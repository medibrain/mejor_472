﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NpgsqlTypes;
using System.Windows.Forms;

namespace Mejor
{
    public enum Koiki_Dtype { 柔整事後 = 1, 鍼灸事後 = 2, 事前 = 3 }

    public class Data_OsakaKoiki
    {
        const int erano = 4;

        public int Did { get; set; }
        public string Iname { get; set; }
        public string Inum { get; set; }
        public string Pnum { get; set; }
        public string Pname { get; set; }
        public string Family { get; set; }
        public string Ratio { get; set; }
        public string YM { get; set; }
        public string Type { get; set; }
        public string Pref { get; set; }
        public string Snum { get; set; }
        public string Sname { get; set; }
        public DateTime StartYMD { get; set; }
        public int Days { get; set; }
        public int Total { get; set; }
        public int Pay { get; set; }
        public string Dnum { get; set; }

        public string Pkana { get; set; }
        public string Pzip { get; set; }
        public string Ppref { get; set; }
        public string Pcity { get; set; }
        public string Paddress { get; set; }
        public DateTime Pbirthday { get; set; }

        public string Qzip { get; set; }
        public string Qpref { get; set; }
        public string Qcity { get; set; }
        public string Qadd { get; set; }
        public string Qname { get; set; }
        public string Qkana { get; set; }

        public string Ssname { get; set; }
        public string Pblc { get; set; }
        public string Gcode { get; set; }

        public string Bnumber { get; set; }
        public string Bkey { get; set; }
        public string Bym { get; set; }
        public int Bsex { get; set; }
        public string Bnote1 { get; set; }
        public string Bnote2 { get; set; }
        public string Bnote3 { get; set; }
        public string Bnote4 { get; set; }
        public string Bnote5 { get; set; }
        public int Bamount { get; set; }
        public string Berrcode1 { get; set; }
        public string Berrname1 { get; set; }
        public string Berrcode2 { get; set; }
        public string Berrname2 { get; set; }
        public string Berrcode3 { get; set; }
        public string Berrname3 { get; set; }
        public string Berrcode4 { get; set; }
        public string Berrname4 { get; set; }
        public string Berrcode5 { get; set; }
        public string Berrname5 { get; set; }
        public string Bpublic { get; set; }
        public string Bpaytype { get; set; }
        public string Bjudge { get; set; }

        public int Merge { get; set; }
        public Koiki_Dtype Dtype { get; set; }

        public int Aid { get; set; }
        public int Aym { get; set; }
        public string Imagefile { get; set; }
        public string Injurename1 { get; set; }
        public string Injurename2 { get; set; }
        public string Injurename3 { get; set; }
        public string Injurename4 { get; set; }
        public string Injurename5 { get; set; }
        public int Visitfee { get; set; }
        public int Returntype { get; set; }
        public DateTime Returndate { get; set; }


        public Data_OsakaKoiki()
        {
            this.Iname = string.Empty;
            this.Inum = string.Empty;
            this.Pnum = string.Empty;
            this.Pname = string.Empty;
            this.Family = string.Empty;
            this.Ratio = string.Empty;
            this.YM = string.Empty;
            this.Type = string.Empty;
            this.Pref = string.Empty;
            this.Snum = string.Empty;
            this.Sname = string.Empty;
            this.Dnum = string.Empty;

            this.Pkana = string.Empty;
            this.Pzip = string.Empty;
            this.Ppref = string.Empty;
            this.Pcity = string.Empty;
            this.Paddress = string.Empty;

            this.Qzip = string.Empty;
            this.Qpref = string.Empty;
            this.Qcity = string.Empty;
            this.Qadd = string.Empty;
            this.Qname = string.Empty;
            this.Qkana = string.Empty;

            this.Ssname = string.Empty;
            this.Pblc = string.Empty;
            this.Gcode = string.Empty;

            this.Bnumber = string.Empty;
            this.Bkey = string.Empty;
            this.Bym = string.Empty;
            this.Bnote1 = string.Empty;
            this.Bnote2 = string.Empty;
            this.Bnote3 = string.Empty;
            this.Bnote4 = string.Empty;
            this.Bnote5 = string.Empty;
            this.Berrcode1 = string.Empty;
            this.Berrname1 = string.Empty;
            this.Berrcode2 = string.Empty;
            this.Berrname2 = string.Empty;
            this.Berrcode3 = string.Empty;
            this.Berrname3 = string.Empty;
            this.Berrcode4 = string.Empty;
            this.Berrname4 = string.Empty;
            this.Berrcode5 = string.Empty;
            this.Berrname5 = string.Empty;
            this.Bpublic = string.Empty;
            this.Bpaytype = string.Empty;
            this.Bjudge = string.Empty;
            this.Imagefile = string.Empty;
            this.Injurename1 = string.Empty;
            this.Injurename2 = string.Empty;
            this.Injurename3 = string.Empty;
            this.Injurename4 = string.Empty;
            this.Injurename5 = string.Empty;
        }

        //画像登録により、あらゆる画像データ1枚につき1レコードをApplicationテーブルに追加する
        public static bool DataEntry(Data_OsakaKoiki dOK, DB.Transaction tran = null)
        {
            var sql = "INSERT INTO koikidata " +
                "(iname, inum, pnum, pname, family, ratio, ym, type, pref, " +
                "snum, sname, startym, days, total, pay, dnum, " +
                "pkana, pzip, ppref, pcity, paddress, pbirthday, " +
                "qzip, qpref, qcity, qadd, qname, qkana, " +
                "ssname, public, gcode, bnumber, bkey, bym, bsex, " +
                "bnote1, bnote2, bnote3, bnote4, bnote5, bamount, " +
                "berrcode1, berrname1, berrcode2, berrname2, berrcode3, berrname3, " +
                "berrcode4, berrname4, berrcode5, berrname5, " +
                "bpublic, bpaytype, bjudge, merge, dtype, aid, aym, imagefile, " +
                "injurename1, injurename2, injurename3, injurename4, injurename5, " +
                "visitfee, returntype, returndate) " +
                "VALUES(:iname, :inum, :pnum, :pname, :family, :ratio, :ym, :type, :pref, " +
                ":snum, :sname, :startym, :days, :total, :pay, :dnum, " +
                ":pkana, :pzip, :ppref, :pcity, :paddress, :pbirthday, " +
                ":qzip, :qpref, :qcity, :qadd, :qname, :qkana, " +
                ":ssname, :public, :gcode, :bnumber, :bkey, :bym, :bsex, " +
                ":bnote1, :bnote2, :bnote3, :bnote4, :bnote5, :bamount, " +
                ":berrcode1, :berrname1, :berrcode2, :berrname2, :berrcode3, :berrname3, " +
                ":berrcode4, :berrname4, :berrcode5, :berrname5, " +
                ":bpublic, :bpaytype, :bjudge, :merge, :dtype, :aid, :aym, :imagefile, " +
                ":injurename1, :injurename2, :injurename3, :injurename4, :injurename5, " +
                ":visitfee, :returntype, :returndate)";
            using(var cmd = tran == null ? DB.Main.CreateCmd(sql) : DB.Main.CreateCmd(sql,tran))
            {
                try
                {
                    //cmd.Parameters.Add("did", NpgsqlDbType.Integer).Value = dOK.Did;
                    cmd.Parameters.Add("iname", NpgsqlDbType.Text).Value = dOK.Iname;
                    cmd.Parameters.Add("inum", NpgsqlDbType.Text).Value = dOK.Inum;
                    cmd.Parameters.Add("pnum", NpgsqlDbType.Text).Value = dOK.Pnum;
                    cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = dOK.Pname;
                    cmd.Parameters.Add("family", NpgsqlDbType.Text).Value = dOK.Family;
                    cmd.Parameters.Add("ratio", NpgsqlDbType.Text).Value = dOK.Ratio;
                    cmd.Parameters.Add("ym", NpgsqlDbType.Text).Value = dOK.YM;
                    cmd.Parameters.Add("type", NpgsqlDbType.Text).Value = dOK.Type;
                    cmd.Parameters.Add("pref", NpgsqlDbType.Text).Value = dOK.Pref;
                    cmd.Parameters.Add("snum", NpgsqlDbType.Text).Value = dOK.Snum;
                    cmd.Parameters.Add("sname", NpgsqlDbType.Text).Value = dOK.Sname;
                    cmd.Parameters.Add("startym", NpgsqlDbType.Date).Value = dOK.StartYMD;
                    cmd.Parameters.Add("days", NpgsqlDbType.Integer).Value = dOK.Days;
                    cmd.Parameters.Add("total", NpgsqlDbType.Integer).Value = dOK.Total;
                    cmd.Parameters.Add("pay", NpgsqlDbType.Integer).Value = dOK.Pay;
                    cmd.Parameters.Add("dnum", NpgsqlDbType.Text).Value = dOK.Dnum;

                    cmd.Parameters.Add("pkana", NpgsqlDbType.Text).Value = dOK.Pkana;
                    cmd.Parameters.Add("pzip", NpgsqlDbType.Text).Value = dOK.Pzip;
                    cmd.Parameters.Add("ppref", NpgsqlDbType.Text).Value = dOK.Ppref;
                    cmd.Parameters.Add("pcity", NpgsqlDbType.Text).Value = dOK.Pcity;
                    cmd.Parameters.Add("paddress", NpgsqlDbType.Text).Value = dOK.Paddress;
                    cmd.Parameters.Add("pbirthday", NpgsqlDbType.Date).Value = dOK.Pbirthday;
                    cmd.Parameters.Add("qzip", NpgsqlDbType.Text).Value = dOK.Qzip;
                    cmd.Parameters.Add("qpref", NpgsqlDbType.Text).Value = dOK.Qpref;
                    cmd.Parameters.Add("qcity", NpgsqlDbType.Text).Value = dOK.Qcity;
                    cmd.Parameters.Add("qadd", NpgsqlDbType.Text).Value = dOK.Qadd;
                    cmd.Parameters.Add("qname", NpgsqlDbType.Text).Value = dOK.Qname;
                    cmd.Parameters.Add("qkana", NpgsqlDbType.Text).Value = dOK.Qkana;

                    cmd.Parameters.Add("ssname", NpgsqlDbType.Text).Value = dOK.Ssname;
                    cmd.Parameters.Add("public", NpgsqlDbType.Text).Value = dOK.Pblc;
                    cmd.Parameters.Add("gcode", NpgsqlDbType.Text).Value = dOK.Gcode;
                    cmd.Parameters.Add("bnumber", NpgsqlDbType.Text).Value = dOK.Bnumber;
                    cmd.Parameters.Add("bkey", NpgsqlDbType.Text).Value = dOK.Bkey;
                    cmd.Parameters.Add("bym", NpgsqlDbType.Text).Value = dOK.Bym;
                    cmd.Parameters.Add("bsex", NpgsqlDbType.Integer).Value = dOK.Bsex;
                    cmd.Parameters.Add("bnote1", NpgsqlDbType.Text).Value = dOK.Bnote1;
                    cmd.Parameters.Add("bnote2", NpgsqlDbType.Text).Value = dOK.Bnote2;
                    cmd.Parameters.Add("bnote3", NpgsqlDbType.Text).Value = dOK.Bnote3;
                    cmd.Parameters.Add("bnote4", NpgsqlDbType.Text).Value = dOK.Bnote4;
                    cmd.Parameters.Add("bnote5", NpgsqlDbType.Text).Value = dOK.Bnote5;
                    cmd.Parameters.Add("bamount", NpgsqlDbType.Integer).Value = dOK.Bamount;
                    cmd.Parameters.Add("berrcode1", NpgsqlDbType.Text).Value = dOK.Berrcode1;
                    cmd.Parameters.Add("berrname1", NpgsqlDbType.Text).Value = dOK.Berrname1;
                    cmd.Parameters.Add("berrcode2", NpgsqlDbType.Text).Value = dOK.Berrcode2;
                    cmd.Parameters.Add("berrname2", NpgsqlDbType.Text).Value = dOK.Berrname2;
                    cmd.Parameters.Add("berrcode3", NpgsqlDbType.Text).Value = dOK.Berrcode3;
                    cmd.Parameters.Add("berrname3", NpgsqlDbType.Text).Value = dOK.Berrname3;
                    cmd.Parameters.Add("berrcode4", NpgsqlDbType.Text).Value = dOK.Berrcode4;
                    cmd.Parameters.Add("berrname4", NpgsqlDbType.Text).Value = dOK.Berrname4;
                    cmd.Parameters.Add("berrcode5", NpgsqlDbType.Text).Value = dOK.Berrcode5;
                    cmd.Parameters.Add("berrname5", NpgsqlDbType.Text).Value = dOK.Berrname5;
                    cmd.Parameters.Add("bpublic", NpgsqlDbType.Text).Value = dOK.Bpublic;
                    cmd.Parameters.Add("bpaytype", NpgsqlDbType.Text).Value = dOK.Bpaytype;
                    cmd.Parameters.Add("bjudge", NpgsqlDbType.Text).Value = dOK.Bjudge;

                    cmd.Parameters.Add("merge", NpgsqlDbType.Integer).Value = dOK.Merge;
                    cmd.Parameters.Add("dtype", NpgsqlDbType.Integer).Value = dOK.Dtype;
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = dOK.Aid;
                    cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = dOK.Aym;
                    cmd.Parameters.Add("imagefile", NpgsqlDbType.Text).Value = dOK.Imagefile;
                    cmd.Parameters.Add("injurename1", NpgsqlDbType.Text).Value = dOK.Injurename1;
                    cmd.Parameters.Add("injurename2", NpgsqlDbType.Text).Value = dOK.Injurename2;
                    cmd.Parameters.Add("injurename3", NpgsqlDbType.Text).Value = dOK.Injurename3;
                    cmd.Parameters.Add("injurename4", NpgsqlDbType.Text).Value = dOK.Injurename4;
                    cmd.Parameters.Add("injurename5", NpgsqlDbType.Text).Value = dOK.Injurename5;
                    cmd.Parameters.Add("visitfee", NpgsqlDbType.Integer).Value = dOK.Visitfee;
                    cmd.Parameters.Add("returntype", NpgsqlDbType.Integer).Value = dOK.Returntype;
                    cmd.Parameters.Add("returndate", NpgsqlDbType.Date).Value = dOK.Returndate;

                    return cmd.TryExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
        }

        //dIdで一枚の申請書を抽出
        public static Data_OsakaKoiki GetDataRecord(int did)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT "+
                "did, iname, inum, pnum, pname, family, ratio, ym, type, pref, " +
                "snum, sname, startym, days, total, pay, dnum, " +
                "pkana, pzip, ppref, pcity, paddress, pbirthday, " +
                "qzip, qpref, qcity, qadd, qname, qkana, " +
                "ssname, public, gcode, bnumber, bkey, bym, bsex, " +
                "bnote1, bnote2, bnote3, bnote4, bnote5, bamount, " +
                "berrcode1, berrname1, berrcode2, berrname2, berrcode3, berrname3, " +
                "berrcode4, berrname4, berrcode5, berrname5, " +
                "bpublic, bpaytype, bjudge, merge, dtype, aid, aym, imagefile, " +
                "injurename1, injurename2, injurename3, injurename4, injurename5, " +
                "visitfee, returntype, returndate " +
                "FROM koikidata " +
                "WHERE did=:did"))
            {
                cmd.Parameters.Add("did", NpgsqlDbType.Integer).Value = did;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                return CreateDatFromRecord(res[0]);
            }
        }

        public static List<Data_OsakaKoiki> SelectAll(int cyyyymm)
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            var aym = DateTimeEx.GetHsYearFromAd(cyyyymm / 100, cyyyymm % 100);
            //var aym = DateTimeEx.GetHsYearFromAd(cyyyymm / 100);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            aym = aym * 100 + cyyyymm % 100;
            return GetDataRecordYM(aym);
        }

        //処理年月で複数の申請書を抽出
        public static List<Data_OsakaKoiki> GetDataRecordYM(int aym)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "did, iname, inum, pnum, pname, family, ratio, ym, type, pref, " +
                "snum, sname, startym, days, total, pay, dnum, " +
                "pkana, pzip, ppref, pcity, paddress, pbirthday, " +
                "qzip, qpref, qcity, qadd, qname, qkana, " +
                "ssname, public, gcode, bnumber, bkey, bym, bsex, " +
                "bnote1, bnote2, bnote3, bnote4, bnote5, bamount, " +
                "berrcode1, berrname1, berrcode2, berrname2, berrcode3, berrname3, " +
                "berrcode4, berrname4, berrcode5, berrname5, " +
                "bpublic, bpaytype, bjudge, merge, dtype, aid, aym, imagefile, " +
                "injurename1, injurename2, injurename3, injurename4, injurename5, " +
                "visitfee, returntype, returndate " +
                "FROM koikidata " +
                "WHERE aym=:aym " +
                "ORDER BY did"))
            {
                cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = aym;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var list = new List<Data_OsakaKoiki>();

                foreach (var item in res)
                {
                    var c = CreateDatFromRecord(item);
                    list.Add(c);
                }
                return list;
            }
        }

        /// <summary>
        /// 指定された請求年月で合計額、もしくは被保番が同じものを取得します
        /// </summary>
        /// <param name="aym"></param>
        /// <param name="pnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<Data_OsakaKoiki> GetDataRecordsOr(int aym, string pnum, int total)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "did, iname, inum, pnum, pname, family, ratio, ym, type, pref, " +
                "snum, sname, startym, days, total, pay, dnum, " +
                "pkana, pzip, ppref, pcity, paddress, pbirthday, " +
                "qzip, qpref, qcity, qadd, qname, qkana, " +
                "ssname, public, gcode, bnumber, bkey, bym, bsex, " +
                "bnote1, bnote2, bnote3, bnote4, bnote5, bamount, " +
                "berrcode1, berrname1, berrcode2, berrname2, berrcode3, berrname3, " +
                "berrcode4, berrname4, berrcode5, berrname5, " +
                "bpublic, bpaytype, bjudge, merge, dtype, aid, aym, imagefile, " +
                "injurename1, injurename2, injurename3, injurename4, injurename5, " +
                "visitfee, returntype, returndate " +
                "FROM koikidata " +
                "WHERE aym=:aym " +
                "AND (pnum=:pnum OR total=:total) " +
                "ORDER BY did"))
            {
                cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = aym;
                cmd.Parameters.Add("pnum", NpgsqlDbType.Text).Value = pnum;
                cmd.Parameters.Add("total", NpgsqlDbType.Integer).Value = total;
                var res = cmd.TryExecuteReaderList();
                if (res == null)return null;

                var list = new List<Data_OsakaKoiki>();
                foreach (var item in res)
                {
                    var c = CreateDatFromRecord(item);
                    list.Add(c);
                }
                return list;
            }
        }

        //被保番と診療年月で複数の申請書を抽出
        public static List<Data_OsakaKoiki> GetDataRecord(string pnum, string ym)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "did, iname, inum, pnum, pname, family, ratio, ym, type, pref, " +
                "snum, sname, startym, days, total, pay, dnum, " +
                "pkana, pzip, ppref, pcity, paddress, pbirthday, " +
                "qzip, qpref, qcity, qadd, qname, qkana, " +
                "ssname, public, gcode, bnumber, bkey, bym, bsex, " +
                "bnote1, bnote2, bnote3, bnote4, bnote5, bamount, " +
                "berrcode1, berrname1, berrcode2, berrname2, berrcode3, berrname3, " +
                "berrcode4, berrname4, berrcode5, berrname5, " +
                "bpublic, bpaytype, bjudge, merge, dtype, aid, aym, imagefile, " +
                "injurename1, injurename2, injurename3, injurename4, injurename5, " +
                "visitfee, returntype, returndate " +
                "FROM koikidata " +
                "WHERE pnum=:pnum AND ym=:ym " +
                "ORDER BY did"))
            {
                cmd.Parameters.Add("pnum", NpgsqlDbType.Text).Value = pnum;
                cmd.Parameters.Add("ym", NpgsqlDbType.Text).Value = ym;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var list = new List<Data_OsakaKoiki>();

                foreach (var item in res)
                {
                    var c = CreateDatFromRecord(item);
                    list.Add(c);
                }
                return list;
            }
        }

        /// <summary>
        /// SQLが吐き出したobjectを元に、Datインスタンスを作成します
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Data_OsakaKoiki CreateDatFromRecord(object[] obj)
        {
            var c = new Data_OsakaKoiki();

            c.Did = (int)obj[0];
            c.Iname = (string)obj[1];
            c.Inum = (string)obj[2];
            c.Pnum = (string)obj[3];
            c.Pname = (string)obj[4];
            c.Family = (string)obj[5];
            c.Ratio = (string)obj[6];
            c.YM = (string)obj[7];
            c.Type = (string)obj[8];
            c.Pref = (string)obj[9];
            c.Snum = (string)obj[10];
            c.Sname = (string)obj[11];
            c.StartYMD = (DateTime)obj[12];
            c.Days = (int)obj[13];
            c.Total = (int)obj[14];
            c.Pay = (int)obj[15];
            c.Dnum = (string)obj[16];
            c.Pkana = (string)obj[17];
            c.Pzip = (string)obj[18];
            c.Ppref = (string)obj[19];
            c.Pcity = (string)obj[20];
            c.Paddress = (string)obj[21];
            c.Pbirthday = (DateTime)obj[22];
            c.Qzip = (string)obj[23];
            c.Qpref = (string)obj[24];
            c.Qcity = (string)obj[25];
            c.Qadd = (string)obj[26];
            c.Qname = (string)obj[27];
            c.Qkana = (string)obj[28];
            c.Ssname = (string)obj[29];
            c.Pblc = (string)obj[30];
            c.Gcode = (string)obj[31];
            c.Bnumber = (string)obj[32];
            c.Bkey = (string)obj[33];
            c.Bym = (string)obj[34];
            c.Bsex = (int)obj[35];
            c.Bnote1 = (string)obj[36];
            c.Bnote2 = (string)obj[37];
            c.Bnote3 = (string)obj[38];
            c.Bnote4 = (string)obj[39];
            c.Bnote5 = (string)obj[40];
            c.Bamount = (int)obj[41];
            c.Berrcode1 = (string)obj[42];
            c.Berrname1 = (string)obj[43];
            c.Berrcode2 = (string)obj[44];
            c.Berrname2 = (string)obj[45];
            c.Berrcode3 = (string)obj[46];
            c.Berrname3 = (string)obj[47];
            c.Berrcode4 = (string)obj[48];
            c.Berrname4 = (string)obj[49];
            c.Berrcode5 = (string)obj[50];
            c.Berrname5 = (string)obj[51];
            c.Bpublic = (string)obj[52];
            c.Bpaytype = (string)obj[53];
            c.Bjudge = (string)obj[54];
            c.Merge = (int)obj[55];
            c.Dtype = (Koiki_Dtype)(int)obj[56];
            c.Aid = (int)obj[57];
            c.Aym = (int)obj[58];
            c.Imagefile = (string)obj[59];
            c.Injurename1 = (string)obj[60];
            c.Injurename2 = (string)obj[61];
            c.Injurename3 = (string)obj[62];
            c.Injurename4 = (string)obj[63];
            c.Injurename5 = (string)obj[64];
            c.Visitfee = (int)obj[65];
            c.Returntype = (int)obj[66];
            c.Returndate = (DateTime)obj[67];

            return c;
        }


        /// <summary>
        /// 柔整事後データをもとに、Datインスタンスを作成します
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Data_OsakaKoiki CreateDatFromJJigo(object[] obj, int py, int pm)
        {
            var c = new Data_OsakaKoiki();

            //c.Did = (int)obj[0];
            c.Iname = (string)obj[0];
            c.Inum = (string)obj[1];
            c.Pnum = (string)obj[2];
            c.Pname = (string)obj[3];
            c.Family = (string)obj[4];
            c.Ratio = (string)obj[5];
            c.YM = (string)obj[6];
            //c.Type = (string)obj[8];
            c.Pref = (string)obj[8];
            c.Snum = (string)obj[9];
            //c.Sname = (string)obj[11];
            string jy = erano + obj[12].ToString();//.Substring(1);
            DateTime wy;
            DateTimeEx.TryGetDateTimeFromJdate(jy, out wy);
            c.StartYMD = wy;
            int days;
            int.TryParse(obj[13].ToString(), out days);
            c.Days = days;// (int)obj[13];
            int total;
            int.TryParse(obj[14].ToString(), out total);
            c.Total = total;// (int)obj[14];
            int pay;
            int.TryParse(obj[15].ToString(), out pay);
            c.Pay = pay;// (int)obj[15];
            //c.Dnum = (string)obj[16];
            c.Pkana = (string)obj[16];
            c.Pzip = (string)obj[17];
            c.Ppref = (string)obj[18];
            c.Pcity = (string)obj[19];
            c.Paddress = (string)obj[20];
            DateTime bd;
            DateTimeEx.TryGetDateTimeFromJdate(obj[21].ToString(), out bd);
            c.Pbirthday = bd;
            c.Qzip = (string)obj[22];
            c.Qpref = (string)obj[23];
            c.Qcity = (string)obj[24];
            c.Qadd = (string)obj[25];
            c.Qname = (string)obj[26];
            c.Qkana = (string)obj[27];
            c.Ssname = (string)obj[11];
            c.Pblc = (string)obj[7];
            c.Gcode = (string)obj[10];
            //c.Bnumber = (string)obj[32];
            //c.Bkey = (string)obj[33];
            //c.Bym = (string)obj[34];
            //c.Bsex = (int)obj[35];
            //c.Bnote1 = (string)obj[36];
            //c.Bnote2 = (string)obj[37];
            //c.Bnote3 = (string)obj[38];
            //c.Bnote4 = (string)obj[39];
            //c.Bnote5 = (string)obj[40];
            //c.Bamount = (int)obj[41];
            //c.Berrcode1 = (string)obj[42];
            //c.Berrname1 = (string)obj[43];
            //c.Berrcode2 = (string)obj[44];
            //c.Berrname2 = (string)obj[45];
            //c.Berrcode3 = (string)obj[46];
            //c.Berrname3 = (string)obj[47];
            //c.Berrcode4 = (string)obj[48];
            //c.Berrname4 = (string)obj[49];
            //c.Berrcode5 = (string)obj[50];
            //c.Berrname5 = (string)obj[51];
            //c.Bpublic = (string)obj[52];
            //c.Bpaytype = (string)obj[53];
            //c.Bjudge = (string)obj[54];
            //c.Merge = (int)obj[55];
            c.Dtype = Koiki_Dtype.柔整事後;
            //c.Aid = (int)obj[57];
            c.Aym = py * 100 + pm;
            //c.Imagefile = (string)obj[59];
            //c.Injurename1 = (string)obj[60];
            //c.Injurename2 = (string)obj[61];
            //c.Injurename3 = (string)obj[62];
            //c.Injurename4 = (string)obj[63];
            //c.Injurename5 = (string)obj[64];
            //c.Visitfee = (int)obj[65];
            //c.Returntype = (int)obj[66];
            //c.Returndate = (DateTime)obj[67];

            return c;
        }

        /// <summary>
        /// 鍼灸事後データをもとに、Datインスタンスを作成します
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Data_OsakaKoiki CreateDatFromHJigo(object[] obj, int py, int pm)
        {
            var c = new Data_OsakaKoiki();

            //c.Did = (int)obj[0];
            c.Iname = (string)obj[0];
            c.Inum = (string)obj[1];
            c.Pnum = (string)obj[2];
            c.Pname = (string)obj[3];
            c.Family = (string)obj[4];
            c.Ratio = (string)obj[5];
            c.YM = (string)obj[6];
            c.Type = (string)obj[7];
            c.Pref = (string)obj[8];
            c.Snum = (string)obj[9];
            c.Sname = (string)obj[10];
            string jy = erano + obj[11].ToString();
            DateTime wy;
            DateTimeEx.TryGetDateTimeFromJdate(jy, out wy);
            c.StartYMD = wy;
            int days;
            int.TryParse(obj[12].ToString(), out days);
            c.Days = days;
            int total;
            int.TryParse(obj[13].ToString(), out total);
            c.Total = total;
            int pay;
            int.TryParse(obj[14].ToString(), out pay);
            c.Pay = pay;
            c.Dnum = (string)obj[15];
            c.Pkana = (string)obj[16];
            c.Pzip = (string)obj[17];
            c.Ppref = (string)obj[18];
            c.Pcity = (string)obj[19];
            c.Paddress = (string)obj[20];
            DateTime bd;
            DateTimeEx.TryGetDateTimeFromJdate(obj[21].ToString(), out bd);
            c.Pbirthday = bd;
            c.Qzip = (string)obj[22];
            c.Qpref = (string)obj[23];
            c.Qcity = (string)obj[24];
            c.Qadd = (string)obj[25];
            c.Qname = (string)obj[26];
            c.Qkana = (string)obj[27];
            c.Ssname = (string)obj[28];
            //c.Pblc = (string)obj[30];
            //c.Gcode = (string)obj[31];
            //c.Bnumber = (string)obj[32];
            //c.Bkey = (string)obj[33];
            //c.Bym = (string)obj[34];
            //c.Bsex = (int)obj[35];
            //c.Bnote1 = (string)obj[36];
            //c.Bnote2 = (string)obj[37];
            //c.Bnote3 = (string)obj[38];
            //c.Bnote4 = (string)obj[39];
            //c.Bnote5 = (string)obj[40];
            //c.Bamount = (int)obj[41];
            //c.Berrcode1 = (string)obj[42];
            //c.Berrname1 = (string)obj[43];
            //c.Berrcode2 = (string)obj[44];
            //c.Berrname2 = (string)obj[45];
            //c.Berrcode3 = (string)obj[46];
            //c.Berrname3 = (string)obj[47];
            //c.Berrcode4 = (string)obj[48];
            //c.Berrname4 = (string)obj[49];
            //c.Berrcode5 = (string)obj[50];
            //c.Berrname5 = (string)obj[51];
            //c.Bpublic = (string)obj[52];
            //c.Bpaytype = (string)obj[53];
            //c.Bjudge = (string)obj[54];
            //c.Merge = (int)obj[55];
            c.Dtype = Koiki_Dtype.鍼灸事後;
            //c.Aid = (int)obj[57];
            c.Aym = py * 100 + pm;
            //c.Imagefile = (string)obj[59];
            //c.Injurename1 = (string)obj[60];
            //c.Injurename2 = (string)obj[61];
            //c.Injurename3 = (string)obj[62];
            //c.Injurename4 = (string)obj[63];
            //c.Injurename5 = (string)obj[64];
            //c.Visitfee = (int)obj[65];
            //c.Returntype = (int)obj[66];
            //c.Returndate = (DateTime)obj[67];

            return c;
        }

        /// <summary>
        /// 柔整事前データをもとに、Datインスタンスを作成します
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Data_OsakaKoiki CreateDatFromJJizen(object[] obj, int py, int pm)
        {
            var c = new Data_OsakaKoiki();

            //c.Did = (int)obj[0];
            //c.Iname = (string)obj[1];
            c.Inum = (string)obj[2];
            int pn;
            int.TryParse(obj[6].ToString(), out pn); 
            c.Pnum = pn.ToString("00000000");
            c.Pname = (string)obj[9];
            //c.Family = (string)obj[5];
            //c.Ratio = (string)obj[6];
            string ym = (string)obj[4];
            ym = ym.Length == 5 ? ym.Remove(0, 1) : ym; 
            c.YM = ym;
            //c.Type = (string)obj[8];
            //c.Pref = (string)obj[9];
            c.Snum = (string)obj[5];
            //c.Sname = (string)obj[11];
            //c.StartYMD = (DateTime)obj[12];
            int d;
            int.TryParse(obj[10].ToString(), out d);
            c.Days = d;
            int t;
            int.TryParse(obj[16].ToString(), out t);
            c.Total = t;
            //c.Pay = (int)obj[15];
            //c.Dnum = (string)obj[16];
            //c.Pkana = (string)obj[17];
            //c.Pzip = (string)obj[18];
            //c.Ppref = (string)obj[19];
            //c.Pcity = (string)obj[20];
            //c.Paddress = (string)obj[21];
            DateTime bd;
            DateTimeEx.TryGetDateTimeFromJdate(obj[7].ToString(), out bd);
            c.Pbirthday = bd;
            //c.Qzip = (string)obj[23];
            //c.Qpref = (string)obj[24];
            //c.Qcity = (string)obj[25];
            //c.Qadd = (string)obj[26];
            //c.Qname = (string)obj[27];
            //c.Qkana = (string)obj[28];
            //c.Ssname = (string)obj[29];
            //c.Pblc = (string)obj[30];
            //c.Gcode = (string)obj[31];
            c.Bnumber = (string)obj[0];
            c.Bkey = (string)obj[1];
            c.Bym = (string)obj[3];
            int sx;
            int.TryParse(obj[8].ToString(), out sx);
            c.Bsex = sx;
            c.Bnote1 = (string)obj[11];
            c.Bnote2 = (string)obj[12];
            c.Bnote3 = (string)obj[13];
            c.Bnote4 = (string)obj[14];
            c.Bnote5 = (string)obj[15];
            int amt;
            int.TryParse(obj[17].ToString(), out amt);
            c.Bamount = amt;
            c.Berrcode1 = (string)obj[18];
            c.Berrname1 = (string)obj[19];
            c.Berrcode2 = (string)obj[20];
            c.Berrname2 = (string)obj[21];
            c.Berrcode3 = (string)obj[22];
            c.Berrname3 = (string)obj[23];
            c.Berrcode4 = (string)obj[24];
            c.Berrname4 = (string)obj[25];
            c.Berrcode5 = (string)obj[26];
            c.Berrname5 = (string)obj[27];
            c.Bpublic = (string)obj[28];
            c.Bpaytype = (string)obj[29];
            c.Bjudge = (string)obj[30];
            c.Merge = 1;    //事前データは、全件文書照会対象
            c.Dtype = Koiki_Dtype.事前;
            //c.Aid = (int)obj[57];
            c.Aym = py * 100 + pm;
            //c.Imagefile = (string)obj[59];
            //c.Injurename1 = (string)obj[60];
            //c.Injurename2 = (string)obj[61];
            //c.Injurename3 = (string)obj[62];
            //c.Injurename4 = (string)obj[63];
            //c.Injurename5 = (string)obj[64];
            //c.Visitfee = (int)obj[65];
            //c.Returntype = (int)obj[66];
            //c.Returndate = (DateTime)obj[67];

            return c;
        }

        //申請書レコードの更新
        public bool Update(int updateUserID)
        {
            using (var cmd = DB.Main.CreateCmd("UPDATE koikidata SET " +
                "iname:=iname, inum=:inum, pnum=:pnum, pname=:pname, " +
                "family=:family, ratio=:ratio, ym=:ym, type=:type, pref=:pref, " +
                "snum=:snum, sname=:sname, startym=:startym, days=:days, total=:total, pay=:pay, dnum=:dnum, " +
                "pkana=:pkana, pzip=:pzip, ppref=:ppref, pcity=:pcity, paddress=:paddress, pbirthday=:pbirthday, " +
                "qzip=:qzip, qpref=:qpref, qcity=:qcity, qadd=:qadd, qname=:qname, qkana=:qkana, " +
                "ssname=:ssname, public=:public, gcode=:gcode, bnumber=:bnumber, bkey=:bkey, bym=:bym, bsex=:bsex, " +
                "bnote1=:bnote1, bnote2=:bnote2, bnote3=:bnote3, bnote4=:bnote4, bnote5=:bnote5, bamount=:bamount, " +
                "berrcode1=:berrcode1, berrname1=:berrname1, berrcode2=:berrcode2, berrname2=:berrname2, " +
                "berrcode3=:berrcode3, berrname3=:berrname3, berrcode4=:berrcode4, berrname4=:berrname4, " +
                "berrcode5=:berrcode5, berrname5=:berrcode5, " +
                "bpublic=:bpublic, bpaytype=:bpaytype, bjudge=:bjudge, merge=:merge, dtype=:dtype, aid=:aid, aym=:aym, imagefile=:imagefile, " +
                "injurename1=:injurename1, injurename2=:injurename2, injurename3=:injurename3, " +
                "injurename4=:injurename4, injurename5=injurename5, " +
                "visitfee=:visitfee, returntype=:returntype, returndate=:returndate " +
                "WHERE did=:did"))
            {
                var c = this;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = c.Aid;

                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// 申請書タイプと処理年月に該当するデータの件数を取得します
        /// </summary>
        /// <param name="type"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static int dataCount(int type, int y, int m)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "COUNT(did) " +
                "FROM koikidata " +
                "WHERE aym=:aym AND dtype=:dtype;"))
            {
                cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = y * 100 + m;
                cmd.Parameters.Add("dtype", NpgsqlDbType.Integer).Value = type;

                var l = cmd.TryExecuteScalar();
                if (l == null || l == DBNull.Value)
                {
                    return 0;
                }

                return (int)(long)l;
            }         
        }

        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<Data_OsakaKoiki> SerchByInput(int cy, int cm, int y, int m, string hnum, int total)
        {
            string ym = (y * 100 + m).ToString();

            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "did, ym, pnum, pname, startym, pbirthday, snum, total, aid, dtype, " +
                "sname, ssname, days " +
                "FROM koikidata " +
                "WHERE pnum=:pnum AND ym=:ym AND total=:total AND aym=:aym " +
                "ORDER BY did;"))
            {
                cmd.Parameters.Add("pnum", NpgsqlDbType.Text).Value = hnum;
                cmd.Parameters.Add("ym", NpgsqlDbType.Text).Value = ym;
                cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = cy * 100 + cm;
                cmd.Parameters.Add("total", NpgsqlDbType.Integer).Value = total;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var list = new List<Data_OsakaKoiki>();

                foreach (var item in res)
                {
                    var dat = new Data_OsakaKoiki();
                    dat.Did = (int)item[0];
                    dat.YM = (string)item[1];
                    dat.Pnum = (string)item[2];
                    dat.Pname = (string)item[3];
                    dat.StartYMD = (DateTime)item[4];
                    dat.Pbirthday = (DateTime)item[5];
                    dat.Snum = (string)item[6];
                    dat.Total = (int)item[7];
                    dat.Aid = (int)item[8];
                    dat.Dtype = (Koiki_Dtype)(int)item[9];
                    dat.Sname = (string)item[10];
                    dat.Ssname = (string)item[11];
                    dat.Days = (int)item[12];

                    list.Add(dat);
                }

                return list;
            }
        }


        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<Data_OsakaKoiki> SerchForMatching(int cym, string hnum, int total)
        {
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            //int jYear = DateTimeEx.GetHsYearFromAd(cym / 100);
            int month = cym % 100;
            int jYear = DateTimeEx.GetHsYearFromAd(cym / 100,month);
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "did, ym, pnum, pname, startym, pbirthday, snum, total, aid, dtype, " +
                "sname, ssname, days "+
                "FROM koikidata " +
                "WHERE aym=:aym AND (pnum=:pnum OR total=:total)" +
                "ORDER BY did;"))
            {
                cmd.Parameters.Add("pnum", NpgsqlDbType.Text).Value = hnum;
                cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = jYear * 100 + month;
                cmd.Parameters.Add("total", NpgsqlDbType.Integer).Value = total;

                var list = new List<Data_OsakaKoiki>();
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return list;


                foreach (var item in res)
                {
                    var dat = new Data_OsakaKoiki();
                    dat.Did = (int)item[0];
                    dat.YM = (string)item[1];
                    dat.Pnum = (string)item[2];
                    dat.Pname = (string)item[3];
                    dat.StartYMD = (DateTime)item[4];
                    dat.Pbirthday = (DateTime)item[5];
                    dat.Snum = (string)item[6];
                    dat.Total = (int)item[7];
                    dat.Aid = (int)item[8];
                    dat.Dtype = (Koiki_Dtype)(int)item[9];
                    dat.Sname = (string)item[10];
                    dat.Ssname = (string)item[11];
                    dat.Days = (int)item[12];

                    list.Add(dat);
                }

                return list;
            }         
        }

        /// <summary>
        /// koikidataテーブル中のdidに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int did, int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=0 WHERE aid=:aid;", tran))
            using (var cmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=:aid WHERE did=:did;", tran))
            {
                try
                {
                    dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                    cmd.Parameters.Add("did", NpgsqlDbType.Integer).Value = did;
                    
                    if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return false;
            }
        }

        /// <summary>
        /// koikidataテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// koikidataテーブルから指定されたAIDの情報を取得します。
        /// マッチングが未完了の場合、または複数件ヒットした場合でも取得できません（null）
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static Data_OsakaKoiki AidSelect(int aid)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "did, iname, inum, pnum, pname, family, ratio, ym, type, pref, " +
                "snum, sname, startym, days, total, pay, dnum, " +
                "pkana, pzip, ppref, pcity, paddress, pbirthday, " +
                "qzip, qpref, qcity, qadd, qname, qkana, " +
                "ssname, public, gcode, bnumber, bkey, bym, bsex, " +
                "bnote1, bnote2, bnote3, bnote4, bnote5, bamount, " +
                "berrcode1, berrname1, berrcode2, berrname2, berrcode3, berrname3, " +
                "berrcode4, berrname4, berrcode5, berrname5, " +
                "bpublic, bpaytype, bjudge, merge, dtype, aid, aym, imagefile, " +
                "injurename1, injurename2, injurename3, injurename4, injurename5, " +
                "visitfee, returntype, returndate " +
                "FROM koikidata " +
                "WHERE aid=:aid"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count != 1) return null;

                return CreateDatFromRecord(res[0]);
            }
        }

        /// <summary>
        /// 全件データとマージ済みデータを比較し、突合できたデータのmergeフラグを立てます
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static int SetEnableFrag(string fileName, int y, int m, WaitForm wf)
        {
            int aym = y * 100 + m;
            int count = 0;
            
            HashSet<string> hs = new HashSet<string>();
            var csv = CommonTool.CsvImportShiftJis(fileName);

            foreach (var line in csv)
            {
                if (line.Length < 3) continue;
                var insurerNo = line[2].Trim();
                hs.Add(insurerNo);
            }

            wf.SetMax(hs.Count);

            var sql = "UPDATE koikidata SET merge = 1 "+
                "WHERE pnum=:pnum AND aym=:aym";
            using (var tran = DB.Main.CreateTransaction())
            using (var cmd = DB.Main.CreateCmd(sql, tran))
            {
                cmd.Parameters.Add("pnum", NpgsqlDbType.Text);
                cmd.Parameters.Add("aym", NpgsqlDbType.Integer).Value = aym;

                foreach (var item in hs)
                {
                    cmd.Parameters["pnum"].Value = item;
                    if (!cmd.TryExecuteNonQuery())
                    {
                        tran.Rollback();
                        return -1;
                    }
                    count++;
                    wf.InvokeValue = count;
                }

                tran.Commit();
                return count;
            }
        }
    }
}
