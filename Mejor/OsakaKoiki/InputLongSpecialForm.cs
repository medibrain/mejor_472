﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.OsakaKoiki
{
    public partial class InputLongSpecialForm : InputFormCore
    {
        private BindingSource bsApp;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 0);
        Point posHnum = new Point(800, 0);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1100);
        Point posTotal = new Point(800, 1800);
        Point posBui = new Point(80, 750);

        public InputLongSpecialForm(ScanGroup sGroup)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            scanGroup = sGroup;

            panelInfo.Visible = true;
            panelMatchCheckInfo.Visible = false;

            var preList = App.GetAppsGID(sGroup.GroupID);
            //var preList = App.GetAppsWithWhere($"WHERE a.scanid={scanID}");
            var list = new List<App>();
            
            //続紙と長期のみのリストに整理
            foreach (var item in preList)
            {
                if (item.MediYear != (int)APP_SPECIAL_CODE.続紙 &&
                    item.MediYear != (int)APP_SPECIAL_CODE.長期) continue;
                list.Add(item);
            }

            //フラグ順にソート
            list.Sort((x, y) =>
                x.InputOrderNumber == y.InputOrderNumber ?
                x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            
            //Appリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            throw new Exception("対応外のモードです");
        }

        private void initialize()
        {
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;

            if (app == null) clearApp();
            else setApp(app);
        }

        //フォーム表示時
        private void inputForm_Shown(object sender, EventArgs e)
        {
            //あはき以外のグループを拒否
            if (scanGroup.AppType != APP_TYPE.あんま && scanGroup.AppType != APP_TYPE.鍼灸)
            {
                MessageBox.Show("あはきグループではありません。作業を中止します。");
                this.Close();
                return;
            }

            if (dataGridViewPlist.RowCount == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }
            if (string.IsNullOrWhiteSpace(scanGroup.note2)) scrollPictureControl1.Ratio = 0.6f;
            else scrollPictureControl1.Ratio = 0.4f;
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void inputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;

                app.StatusFlagRemove(StatusFlag.処理2);                
            }
            else if (verifyBoxY.Text == "..")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.長期;
                app.AppType = APP_TYPE.長期;
                app.StatusFlagRemove(StatusFlag.処理2);

                //20190128142918 furukawa st ////////////////////////
                //拡張入力カウントのためフラグ修正                
                app.StatusFlagSet(StatusFlag.拡張入力済);
                //20190128142918 furukawa ed ////////////////////////
            }

            else
            {
                //その他の場合
                MessageBox.Show("この画面では「続紙」または「長期」のみの入力となります。" +
                    "他の入力に関しては通常の入力画面より入力してください。", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //入力ログ
                //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換                
                if (app.Ufirst == 0 && !InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now-dtstart_core, jyuTran))
                    return false;

                //データ記録
                app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput, tran);

                tran.Commit();
                jyuTran.Commit();
            }

            return true;
        }

        private void clearApp()
        {
            //画像クリア
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            //情報クリア
            var clearTB = new Action<TextBox>((tb) =>
            {
                tb.Text = "";
                tb.BackColor = SystemColors.Info;
            });
            
            Action<Control> act = null;
            act = new Action<Control>(c =>
            {
                if (c is TextBox)
                {
                    c.Text = "";
                    c.BackColor = SystemColors.Info;
                }
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
                foreach (Control item in c.Controls) act(item);
            });
            act(panelRight);
        }

        /// <summary>
        /// Appを表示します
        /// </summary>
        /// <param name="r"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //全クリア
            iVerifiableAllClear(panelRight);

            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);

            //OCR 入力者情報をリセット
            labelStatusOCR.Text = string.Empty;
            labelInputUser.Text = string.Empty;

            if (app == null) return;
            verifyBoxY.Focus();

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
            }
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
            }

            //全選択
            if (ActiveControl is TextBox) ((TextBox)ActiveControl).SelectAll();

            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
            }
        }

        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            labelInputUser.Text = "入力：";

            //OCRデータが存在する場合
            if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                try
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    labelStatusOCR.Text = "OCR有り";
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    //OCR情報をリセット
                    labelStatusOCR.Text = "OCRエラー";
                }
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            labelInputUser.Text = $"入力者：{User.GetUserName(app.Ufirst)}";

            if (app.AppType == APP_TYPE.続紙)
            {
                //続紙
                verifyBoxY.Text = "--";
            }
            else if (app.AppType == APP_TYPE.長期)
            {
                //長期
                verifyBoxY.Text = "..";
            }
            else
            {
                throw new Exception("「続紙」または「長期」以外の申請書が指定されました。処理を中断します。");
            }
        }
        
        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            var appsg = scanGroup ?? (app == null ? null : ScanGroup.SelectWithScanData(app.GroupID));

            var setCanInput = new Action<Control, bool>((t, b) =>
                {
                    if (t is TextBox) ((TextBox)t).ReadOnly = !b;
                    else t.Enabled = b;

                    t.TabStop = b;
                    if (!b)
                        t.BackColor = SystemColors.Menu;
                    else
                        t.BackColor = SystemColors.Info;
                });
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            if (sender is TextBox) ((TextBox)sender).SelectAll();

            Point p;

            if (sender == verifyBoxY) p = posYM;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }


        /// <summary>
        /// データエラーチェック後、画面に復帰する際にエラー箇所にフォーカスを移動
        /// </summary>
        private void focusBack()
        {
            var cs = new Control[] { verifyBoxY, };

            foreach (var item in cs)
            {
                if (item.Enabled && item.BackColor != SystemColors.Info)
                {
                    item.Focus();
                    return;
                }
            }
            verifyBoxY.Focus();
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;
            if (ActiveControl == verifyBoxY) posYM = pos;
        }
    }
}
