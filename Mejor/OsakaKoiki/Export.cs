﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;
using System.Windows.Forms;
using NpgsqlTypes;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;


namespace Mejor.OsakaKoiki
{
    class Export
    {
        const string dbName = "osaka_koiki";

        /// <summary>
        /// 未送付リスト作成用に必要な旧式のViewerDataCSVです。
        /// </summary>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="exportFilePath"></param>
        /// <returns></returns>
        public static bool ExportCSVForInquiry(int cym, string exportFilePath)
        {
            var wf = new WaitForm();
            wf.BarStyle = ProgressBarStyle.Continuous;

            var result = true;
            try
            {
                wf.ShowDialogOtherTask();
                string exFilename = exportFilePath;

                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                wf.LogPrint("提供データを取得しています");
                var rrs = Data_OsakaKoiki.SelectAll(cym);
                var dic = new Dictionary<int, Data_OsakaKoiki>();
                rrs.ForEach(rr => { if (rr.Aid != 0) dic.Add(rr.Aid, rr); });

                wf.LogPrint("未送付リスト作成用データを作成しています");
                wf.SetMax(apps.Count);
                wf.InvokeValue = 0;

                var count = 0;
                using (var sw = new System.IO.StreamWriter(exFilename, true, Encoding.GetEncoding("Shift_JIS")))
                {
                    foreach(var app in apps)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("キャンセルしてもよろしいですか？", "キャンセル確認",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                MessageBox.Show("出力がキャンセルされました。出力途中のデータは残されています。ご注意ください。");
                                return false;
                            }
                            wf.Cancel = false;
                        }

                        if (dic.ContainsKey(app.Aid))
                        {
                            if (!ExportCSV(app, dic[app.Aid], sw)) return false;
                        }
                        else
                        {
                            if (app.MediYear != -3 || app.MediYear != -4)
                            {
                                Console.WriteLine($"突合データなし：{app.Aid}");
                            }
                            continue;
                        }
                        wf.InvokeValue = wf.InvokeValue + 1;
                        count++;
                    }
                }

                MessageBox.Show($"出力完了（{count}件）", "Export",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);                
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                wf.LogPrint("出力に失敗しました。");
                result = false;
            }
            finally
            {
                wf.Dispose();
            }

            return result;
        }

        /// <summary>
        /// CSVの吐き出し
        /// </summary>
        /// <param name="item"></param>
        /// <param name="k"></param>
        /// <param name="exFilename"></param>
        /// <returns></returns>
        private static bool ExportCSV(App a, Data_OsakaKoiki k, System.IO.StreamWriter sw)
        {
            var sl = new string[68];
            sl[0] = k.Did.ToString();
            sl[1] = k.Iname;
            sl[2] = k.Inum;
            sl[3] = k.Pnum;
            sl[4] = k.Pname;
            sl[5] = k.Family;
            sl[6] = k.Ratio;
            sl[7] = k.YM;
            sl[8] = k.Type;
            sl[9] = k.Pref;
            sl[10] = k.Snum;
            sl[11] = k.Sname;
            sl[12] = k.StartYMD.ToShortDateString();
            sl[13] = k.Days.ToString();
            sl[14] = k.Total.ToString();
            sl[15] = k.Pay.ToString();
            sl[16] = k.Dnum;
            sl[17] = k.Pkana;
            sl[18] = k.Pzip;
            sl[19] = k.Ppref;
            sl[20] = k.Pcity;
            sl[21] = k.Paddress;
            sl[22] = k.Pbirthday.ToShortDateString();
            sl[23] = k.Qzip;
            sl[24] = k.Qpref;//Strings.StrConv(mark, VbStrConv.Wide);
            sl[25] = k.Qcity;
            sl[26] = k.Qadd;
            sl[27] = k.Qname;
            sl[28] = k.Qkana;
            sl[29] = k.Ssname;
            sl[30] = k.Pblc;
            sl[31] = k.Gcode;
            sl[32] = k.Bnumber;
            sl[33] = k.Bkey;
            sl[34] = k.Bym;
            sl[35] = k.Bsex.ToString();
            sl[36] = k.Bnote1;
            sl[37] = k.Bnote2;
            sl[38] = k.Bnote3;
            sl[39] = k.Bnote4;
            sl[40] = k.Bnote5;
            sl[41] = k.Bamount.ToString();
            sl[42] = k.Berrcode1;
            sl[43] = k.Berrname1;
            sl[44] = k.Berrcode2;
            sl[45] = k.Berrname2;
            sl[46] = k.Berrcode3;
            sl[47] = k.Berrname3;
            sl[48] = k.Berrcode4;
            sl[49] = k.Berrname4;
            sl[50] = k.Berrcode5;
            sl[51] = k.Berrname5;
            sl[52] = k.Bpublic;
            sl[53] = k.Bpaytype;
            sl[54] = k.Bjudge;
            sl[55] = k.Merge.ToString();
            sl[56] = k.Dtype.ToString();
            sl[57] = k.Aid.ToString();
            sl[58] = k.Aym.ToString();
            sl[59] = k.Did + ".tif"; //a.Aimagefile;
            sl[60] = a.FushoName1;
            sl[61] = a.FushoName2;
            sl[62] = a.FushoName3;
            sl[63] = a.FushoName4;
            sl[64] = a.FushoName5;
            sl[65] = a.Distance == 999 ? "1" : "0"; //a.Fvisitfee.ToString();
            sl[66] = k.Returntype.ToString();
            sl[67] = k.Returndate.ToShortDateString();

            try
            {
                sw.WriteLine(string.Join(",", sl));
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\nCSVデータの書き込みに失敗しました");
                return false;
            }

            return true;
        }

        /// <summary>
        /// SyokaiFormから出力するCSVです。
        /// </summary>
        /// <param name="list"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName)
        {
            try
            {
                using (var wf = new WaitForm())
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var rex = new string[22];
                    //先頭行は見出し
                    rex[0] = "ID";
                    rex[1] = "処理年";
                    rex[2] = "処理月";
                    rex[3] = "診療年";
                    rex[4] = "診療月";
                    rex[5] = "保険者番号";
                    rex[6] = "被保険者番号";
                    rex[7] = "住所";
                    rex[8] = "氏名";
                    rex[9] = "性別";
                    rex[10] = "生年月日";
                    rex[11] = "請求区分";
                    rex[12] = "往療料";
                    rex[13] = "施術所記号";
                    rex[14] = "合計金額";
                    rex[15] = "請求金額";
                    rex[16] = "診療日数";
                    rex[17] = "ナンバリング";
                    rex[18] = "点検結果";
                    rex[19] = "点検詳細";
                    rex[21] = "施術所名";

                    sw.WriteLine(string.Join(",", rex));

                    foreach (var item in list)
                    {
                        rex[0] = item.Aid.ToString();
                        rex[1] = item.ChargeYear.ToString();
                        rex[2] = item.ChargeMonth.ToString();
                        rex[3] = item.MediYear.ToString();
                        rex[4] = item.MediMonth.ToString();
                        rex[5] = item.InsNum;
                        rex[6] = item.HihoNum;
                        rex[7] = item.HihoAdd;
                        rex[8] = item.PersonName;
                        rex[9] = item.Sex == 1 ? "男" : "女";
                        rex[10] = item.Birthday.ToShortDateString();
                        rex[11] = item.NewContType == NEW_CONT.新規 ? "新規" : "継続";
                        rex[12] = item.Distance == 999 ? "あり" : "";
                        rex[13] = item.DrNum;
                        rex[14] = item.Total.ToString();
                        rex[15] = item.Charge.ToString();
                        rex[16] = item.CountedDays.ToString();
                        rex[17] = item.Numbering;
                        rex[18] = item.InspectInfo;
                        rex[19] = item.InspectDescription;

                        //[20170105]施術所名(kubo)
                        //var datOsa = Data_OsakaKoiki.AidSelect(item.Aid);
                        //rex[21] = datOsa?.Ssname;

                        //20180614(松本)
                        //参考データ形式変更(data_osakakoiki → refrece)に伴う調整
                        rex[21] = item.ClinicName;


                        sw.WriteLine(string.Join(",", rex));

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CSV出力に失敗しました。\r\n" + ex.Message);
                Log.ErrorWrite(ex);
                return false;
            }

            MessageBox.Show("CSV出力が完了しました。");
            return true;
        }

        
        public static bool ExportViewerData(int cym)
        {
            string infoFileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                infoFileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(infoFileName);
            var wf = new WaitForm();

            try
            {
                wf.ShowDialogOtherTask();

                //申請書取得
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                //提供データ取得
                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.SelectAll(cym);
                var dic = new Dictionary<int, RefRece>();
                rrs.ForEach(rr => { if (rr.AID != 0) dic.Add(rr.AID, rr); });

                //マッチング確認
                wf.LogPrint("マッチングを確認しています");
                foreach (var app in apps)
                {
                    if (app.YM < 0) continue;
                    if (!dic.ContainsKey(app.Aid))
                    {
                        wf.LogPrint($"突合データがない申請書があります AID:{app.Aid}");
                        continue;
                    }
                }


                //ビューアデータ
                var vdCount = ViewerData.Export(cym, apps, infoFileName, wf);
                if (vdCount < 0) return false;

                //更新データ
                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }




                //ログ
                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nビューアデータ出力数   :{vdCount}";
                wf.LogPrint(lastMsg);
              




            }
            catch (Exception ex)
            {
                wf.LogPrint("データの出力に失敗しました。");
                wf.LogPrint(ex.Message);
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                wf.LogSave(logFn);
                wf.Dispose();
            }
            return true;
        }
    }
}
