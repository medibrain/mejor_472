﻿namespace Mejor.OsakaKoiki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelAppStatus = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.labelMacthCheck = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.buttonReInput = new System.Windows.Forms.Button();
            this.pTotal = new System.Windows.Forms.Panel();
            this.pF1 = new System.Windows.Forms.Panel();
            this.verifyBoxF1FirstM = new Mejor.VerifyBox();
            this.labelF1FirstM = new System.Windows.Forms.Label();
            this.labelF1FirstY = new System.Windows.Forms.Label();
            this.labelF1First = new System.Windows.Forms.Label();
            this.verifyBoxF1FirstY = new Mejor.VerifyBox();
            this.pDoui = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.vbDouiG = new Mejor.VerifyBox();
            this.lblDoui = new System.Windows.Forms.Label();
            this.vbDouiD = new Mejor.VerifyBox();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.pSejutu = new System.Windows.Forms.Panel();
            this.vbSejutuG = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSejutu = new System.Windows.Forms.Label();
            this.lblSejutuM = new System.Windows.Forms.Label();
            this.lblSejutuY = new System.Windows.Forms.Label();
            this.vbSejutuM = new Mejor.VerifyBox();
            this.vbSejutuY = new Mejor.VerifyBox();
            this.vcSejutu = new Mejor.VerifyCheckBox();
            this.buttonDistance = new System.Windows.Forms.Button();
            this.labelInputUser = new System.Windows.Forms.Label();
            this.panelMatchWhere = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxTotal = new System.Windows.Forms.CheckBox();
            this.checkBoxNumber = new System.Windows.Forms.CheckBox();
            this.labelKasanCost = new System.Windows.Forms.Label();
            this.labelOryoCost = new System.Windows.Forms.Label();
            this.verifyBoxKasanCost = new Mejor.VerifyBox();
            this.verifyBoxOryoCost = new Mejor.VerifyBox();
            this.labelOryoKasan = new System.Windows.Forms.Label();
            this.labelOryoKm = new System.Windows.Forms.Label();
            this.verifyBoxKasanKm = new Mejor.VerifyBox();
            this.verifyCheckBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxLeftLower = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxRightLower = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxLeftUpper = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxRightUpper = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxBody = new Mejor.VerifyCheckBox();
            this.verifyCheckBoxVisit = new Mejor.VerifyCheckBox();
            this.labelF1 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxBuiCount = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.labelBui = new System.Windows.Forms.Label();
            this.labelBuiCount = new System.Windows.Forms.Label();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.labelStatusOCR = new System.Windows.Forms.Label();
            this.labelF2 = new System.Windows.Forms.Label();
            this.labelF3 = new System.Windows.Forms.Label();
            this.dataGridRefRece = new System.Windows.Forms.DataGridView();
            this.labelF4 = new System.Windows.Forms.Label();
            this.labelF5 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.pTotal.SuspendLayout();
            this.pF1.SuspendLayout();
            this.pDoui.SuspendLayout();
            this.pSejutu.SuspendLayout();
            this.panelMatchWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRefRece)).BeginInit();
            this.SuspendLayout();
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(239, 11);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(38, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(3, 3);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(38, 23);
            this.verifyBoxM.TabIndex = 4;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextV = "";
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(143, 3);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxHnum.TabIndex = 7;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonUpdate.Location = new System.Drawing.Point(577, 728);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(90, 25);
            this.buttonUpdate.TabIndex = 80;
            this.buttonUpdate.Text = "登録 (PgUp)";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(283, 22);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(48, 15);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(94, 15);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(43, 13);
            this.labelHnum.TabIndex = 6;
            this.labelHnum.Text = "被保番";
            // 
            // labelAppStatus
            // 
            this.labelAppStatus.AutoSize = true;
            this.labelAppStatus.Location = new System.Drawing.Point(679, 16);
            this.labelAppStatus.Name = "labelAppStatus";
            this.labelAppStatus.Size = new System.Drawing.Size(56, 13);
            this.labelAppStatus.TabIndex = 10;
            this.labelAppStatus.Text = "AppStatus";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label6.Location = new System.Drawing.Point(2, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(203, 60);
            this.label6.TabIndex = 0;
            this.label6.Text = "続紙    : \"--\"     不要    : \"++\"\r\n施術同意書  : \"901\"\r\n施術同意書裏：\"902\"\r\n施術報告書  : \"911\"\r\n状態記入" +
    "書  : \"921\"\r\n";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            this.label8.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(293, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "往療";
            // 
            // labelMacthCheck
            // 
            this.labelMacthCheck.AutoSize = true;
            this.labelMacthCheck.Location = new System.Drawing.Point(748, 16);
            this.labelMacthCheck.Name = "labelMacthCheck";
            this.labelMacthCheck.Size = new System.Drawing.Size(86, 13);
            this.labelMacthCheck.TabIndex = 11;
            this.labelMacthCheck.Text = "マッチング未判定";
            this.labelMacthCheck.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(243, 15);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(31, 13);
            this.labelTotal.TabIndex = 8;
            this.labelTotal.Text = "合計";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(277, 3);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxTotal.TabIndex = 9;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBox_Enter);
            this.verifyBoxTotal.Leave += new System.EventHandler(this.verifyBoxTotal_Leave);
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(0, 757);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(0, 757);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(76, 712);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 9;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(4, 712);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 8;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(40, 712);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 7;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-531, 712);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(4);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(0, 757);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.buttonReInput);
            this.panelRight.Controls.Add(this.pTotal);
            this.panelRight.Controls.Add(this.pF1);
            this.panelRight.Controls.Add(this.pDoui);
            this.panelRight.Controls.Add(this.pSejutu);
            this.panelRight.Controls.Add(this.vcSejutu);
            this.panelRight.Controls.Add(this.buttonDistance);
            this.panelRight.Controls.Add(this.labelInputUser);
            this.panelRight.Controls.Add(this.panelMatchWhere);
            this.panelRight.Controls.Add(this.labelKasanCost);
            this.panelRight.Controls.Add(this.labelOryoCost);
            this.panelRight.Controls.Add(this.verifyBoxKasanCost);
            this.panelRight.Controls.Add(this.verifyBoxOryoCost);
            this.panelRight.Controls.Add(this.labelOryoKasan);
            this.panelRight.Controls.Add(this.labelOryoKm);
            this.panelRight.Controls.Add(this.verifyBoxKasanKm);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisitKasan);
            this.panelRight.Controls.Add(this.verifyCheckBoxLeftLower);
            this.panelRight.Controls.Add(this.verifyCheckBoxRightLower);
            this.panelRight.Controls.Add(this.verifyCheckBoxLeftUpper);
            this.panelRight.Controls.Add(this.verifyCheckBoxRightUpper);
            this.panelRight.Controls.Add(this.verifyCheckBoxBody);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisit);
            this.panelRight.Controls.Add(this.label18);
            this.panelRight.Controls.Add(this.labelF1);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.verifyBoxF2);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.labelMacthCheck);
            this.panelRight.Controls.Add(this.verifyBoxF3);
            this.panelRight.Controls.Add(this.labelAppStatus);
            this.panelRight.Controls.Add(this.verifyBoxBuiCount);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.labelBui);
            this.panelRight.Controls.Add(this.labelBuiCount);
            this.panelRight.Controls.Add(this.verifyBoxF4);
            this.panelRight.Controls.Add(this.label6);
            this.panelRight.Controls.Add(this.verifyBoxF5);
            this.panelRight.Controls.Add(this.labelStatusOCR);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelF2);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.labelF3);
            this.panelRight.Controls.Add(this.dataGridRefRece);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.labelF4);
            this.panelRight.Controls.Add(this.labelF5);
            this.panelRight.Controls.Add(this.buttonUpdate);
            this.panelRight.Controls.Add(this.label26);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(57, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 757);
            this.panelRight.TabIndex = 0;
            // 
            // buttonReInput
            // 
            this.buttonReInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonReInput.Location = new System.Drawing.Point(707, 727);
            this.buttonReInput.Name = "buttonReInput";
            this.buttonReInput.Size = new System.Drawing.Size(90, 25);
            this.buttonReInput.TabIndex = 81;
            this.buttonReInput.TabStop = false;
            this.buttonReInput.Text = "入力し直し";
            this.buttonReInput.UseVisualStyleBackColor = true;
            this.buttonReInput.Click += new System.EventHandler(this.buttonReInput_Click);
            // 
            // pTotal
            // 
            this.pTotal.Controls.Add(this.verifyBoxM);
            this.pTotal.Controls.Add(this.labelM);
            this.pTotal.Controls.Add(this.labelTotal);
            this.pTotal.Controls.Add(this.verifyBoxHnum);
            this.pTotal.Controls.Add(this.labelHnum);
            this.pTotal.Controls.Add(this.verifyBoxTotal);
            this.pTotal.Location = new System.Drawing.Point(308, 8);
            this.pTotal.Name = "pTotal";
            this.pTotal.Size = new System.Drawing.Size(360, 34);
            this.pTotal.TabIndex = 4;
            // 
            // pF1
            // 
            this.pF1.Controls.Add(this.verifyBoxF1FirstM);
            this.pF1.Controls.Add(this.labelF1FirstM);
            this.pF1.Controls.Add(this.labelF1FirstY);
            this.pF1.Controls.Add(this.labelF1First);
            this.pF1.Controls.Add(this.verifyBoxF1FirstY);
            this.pF1.Location = new System.Drawing.Point(137, 55);
            this.pF1.Name = "pF1";
            this.pF1.Size = new System.Drawing.Size(150, 40);
            this.pF1.TabIndex = 12;
            // 
            // verifyBoxF1FirstM
            // 
            this.verifyBoxF1FirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstM.Location = new System.Drawing.Point(99, 7);
            this.verifyBoxF1FirstM.MaxLength = 2;
            this.verifyBoxF1FirstM.Name = "verifyBoxF1FirstM";
            this.verifyBoxF1FirstM.NewLine = false;
            this.verifyBoxF1FirstM.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstM.TabIndex = 15;
            this.verifyBoxF1FirstM.TextV = "";
            this.verifyBoxF1FirstM.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelF1FirstM
            // 
            this.labelF1FirstM.AutoSize = true;
            this.labelF1FirstM.Location = new System.Drawing.Point(130, 14);
            this.labelF1FirstM.Name = "labelF1FirstM";
            this.labelF1FirstM.Size = new System.Drawing.Size(19, 13);
            this.labelF1FirstM.TabIndex = 16;
            this.labelF1FirstM.Text = "月";
            // 
            // labelF1FirstY
            // 
            this.labelF1FirstY.AutoSize = true;
            this.labelF1FirstY.Location = new System.Drawing.Point(77, 16);
            this.labelF1FirstY.Name = "labelF1FirstY";
            this.labelF1FirstY.Size = new System.Drawing.Size(19, 13);
            this.labelF1FirstY.TabIndex = 14;
            this.labelF1FirstY.Text = "年";
            // 
            // labelF1First
            // 
            this.labelF1First.AutoSize = true;
            this.labelF1First.Location = new System.Drawing.Point(3, 16);
            this.labelF1First.Name = "labelF1First";
            this.labelF1First.Size = new System.Drawing.Size(43, 13);
            this.labelF1First.TabIndex = 12;
            this.labelF1First.Text = "初検日";
            // 
            // verifyBoxF1FirstY
            // 
            this.verifyBoxF1FirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1FirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1FirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1FirstY.Location = new System.Drawing.Point(47, 7);
            this.verifyBoxF1FirstY.MaxLength = 2;
            this.verifyBoxF1FirstY.Name = "verifyBoxF1FirstY";
            this.verifyBoxF1FirstY.NewLine = false;
            this.verifyBoxF1FirstY.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1FirstY.TabIndex = 13;
            this.verifyBoxF1FirstY.TextV = "";
            this.verifyBoxF1FirstY.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // pDoui
            // 
            this.pDoui.Controls.Add(this.label3);
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Location = new System.Drawing.Point(662, 199);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(350, 39);
            this.pDoui.TabIndex = 60;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 26);
            this.label3.TabIndex = 84;
            this.label3.Text = "4:平成\r\n5:令和";
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(107, 8);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Enabled = false;
            this.lblDoui.Location = new System.Drawing.Point(5, 8);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(43, 26);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(258, 8);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 64;
            this.vbDouiD.TextV = "";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(238, 18);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(19, 13);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(289, 18);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(19, 13);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(186, 18);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(19, 13);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(208, 8);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(157, 8);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // pSejutu
            // 
            this.pSejutu.Controls.Add(this.vbSejutuG);
            this.pSejutu.Controls.Add(this.label1);
            this.pSejutu.Controls.Add(this.lblSejutu);
            this.pSejutu.Controls.Add(this.lblSejutuM);
            this.pSejutu.Controls.Add(this.lblSejutuY);
            this.pSejutu.Controls.Add(this.vbSejutuM);
            this.pSejutu.Controls.Add(this.vbSejutuY);
            this.pSejutu.Location = new System.Drawing.Point(662, 158);
            this.pSejutu.Name = "pSejutu";
            this.pSejutu.Size = new System.Drawing.Size(350, 39);
            this.pSejutu.TabIndex = 55;
            // 
            // vbSejutuG
            // 
            this.vbSejutuG.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuG.Location = new System.Drawing.Point(107, 8);
            this.vbSejutuG.MaxLength = 2;
            this.vbSejutuG.Name = "vbSejutuG";
            this.vbSejutuG.NewLine = false;
            this.vbSejutuG.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuG.TabIndex = 51;
            this.vbSejutuG.TextV = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 26);
            this.label1.TabIndex = 83;
            this.label1.Text = "4:平成\r\n5:令和";
            // 
            // lblSejutu
            // 
            this.lblSejutu.AutoSize = true;
            this.lblSejutu.Enabled = false;
            this.lblSejutu.Location = new System.Drawing.Point(4, 13);
            this.lblSejutu.Name = "lblSejutu";
            this.lblSejutu.Size = new System.Drawing.Size(55, 13);
            this.lblSejutu.TabIndex = 39;
            this.lblSejutu.Text = "前回支給";
            // 
            // lblSejutuM
            // 
            this.lblSejutuM.AutoSize = true;
            this.lblSejutuM.Location = new System.Drawing.Point(237, 18);
            this.lblSejutuM.Name = "lblSejutuM";
            this.lblSejutuM.Size = new System.Drawing.Size(19, 13);
            this.lblSejutuM.TabIndex = 41;
            this.lblSejutuM.Text = "月";
            // 
            // lblSejutuY
            // 
            this.lblSejutuY.AutoSize = true;
            this.lblSejutuY.Location = new System.Drawing.Point(183, 18);
            this.lblSejutuY.Name = "lblSejutuY";
            this.lblSejutuY.Size = new System.Drawing.Size(19, 13);
            this.lblSejutuY.TabIndex = 40;
            this.lblSejutuY.Text = "年";
            // 
            // vbSejutuM
            // 
            this.vbSejutuM.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuM.Location = new System.Drawing.Point(206, 8);
            this.vbSejutuM.MaxLength = 2;
            this.vbSejutuM.Name = "vbSejutuM";
            this.vbSejutuM.NewLine = false;
            this.vbSejutuM.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuM.TabIndex = 53;
            this.vbSejutuM.TextV = "";
            // 
            // vbSejutuY
            // 
            this.vbSejutuY.BackColor = System.Drawing.SystemColors.Info;
            this.vbSejutuY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbSejutuY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbSejutuY.Location = new System.Drawing.Point(155, 8);
            this.vbSejutuY.MaxLength = 2;
            this.vbSejutuY.Name = "vbSejutuY";
            this.vbSejutuY.NewLine = false;
            this.vbSejutuY.Size = new System.Drawing.Size(28, 23);
            this.vbSejutuY.TabIndex = 52;
            this.vbSejutuY.TextV = "";
            // 
            // vcSejutu
            // 
            this.vcSejutu.BackColor = System.Drawing.SystemColors.Info;
            this.vcSejutu.CheckedV = false;
            this.vcSejutu.Enabled = false;
            this.vcSejutu.Location = new System.Drawing.Point(559, 161);
            this.vcSejutu.Name = "vcSejutu";
            this.vcSejutu.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.vcSejutu.Size = new System.Drawing.Size(95, 27);
            this.vcSejutu.TabIndex = 50;
            this.vcSejutu.Text = "交付料あり";
            this.vcSejutu.UseVisualStyleBackColor = false;
            this.vcSejutu.CheckedChanged += new System.EventHandler(this.vcSejutu_CheckedChanged);
            // 
            // buttonDistance
            // 
            this.buttonDistance.Location = new System.Drawing.Point(821, 55);
            this.buttonDistance.Name = "buttonDistance";
            this.buttonDistance.Size = new System.Drawing.Size(90, 25);
            this.buttonDistance.TabIndex = 53;
            this.buttonDistance.TabStop = false;
            this.buttonDistance.Text = "距離確認";
            this.buttonDistance.UseVisualStyleBackColor = true;
            this.buttonDistance.Visible = false;
            this.buttonDistance.Click += new System.EventHandler(this.buttonDistance_Click);
            // 
            // labelInputUser
            // 
            this.labelInputUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputUser.AutoSize = true;
            this.labelInputUser.Location = new System.Drawing.Point(93, 735);
            this.labelInputUser.Name = "labelInputUser";
            this.labelInputUser.Size = new System.Drawing.Size(49, 13);
            this.labelInputUser.TabIndex = 51;
            this.labelInputUser.Text = "入力者：";
            // 
            // panelMatchWhere
            // 
            this.panelMatchWhere.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelMatchWhere.Controls.Add(this.label4);
            this.panelMatchWhere.Controls.Add(this.checkBoxTotal);
            this.panelMatchWhere.Controls.Add(this.checkBoxNumber);
            this.panelMatchWhere.Location = new System.Drawing.Point(238, 728);
            this.panelMatchWhere.Name = "panelMatchWhere";
            this.panelMatchWhere.Size = new System.Drawing.Size(242, 25);
            this.panelMatchWhere.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "突合候補条件";
            // 
            // checkBoxTotal
            // 
            this.checkBoxTotal.AutoSize = true;
            this.checkBoxTotal.Checked = true;
            this.checkBoxTotal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTotal.Location = new System.Drawing.Point(160, 4);
            this.checkBoxTotal.Name = "checkBoxTotal";
            this.checkBoxTotal.Size = new System.Drawing.Size(74, 17);
            this.checkBoxTotal.TabIndex = 38;
            this.checkBoxTotal.TabStop = false;
            this.checkBoxTotal.Text = "合計金額";
            this.checkBoxTotal.UseVisualStyleBackColor = true;
            this.checkBoxTotal.CheckedChanged += new System.EventHandler(this.checkBoxMatch_CheckedChanged);
            // 
            // checkBoxNumber
            // 
            this.checkBoxNumber.AutoSize = true;
            this.checkBoxNumber.Checked = true;
            this.checkBoxNumber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxNumber.Location = new System.Drawing.Point(94, 4);
            this.checkBoxNumber.Name = "checkBoxNumber";
            this.checkBoxNumber.Size = new System.Drawing.Size(62, 17);
            this.checkBoxNumber.TabIndex = 37;
            this.checkBoxNumber.TabStop = false;
            this.checkBoxNumber.Text = "被保番";
            this.checkBoxNumber.UseVisualStyleBackColor = true;
            this.checkBoxNumber.CheckedChanged += new System.EventHandler(this.checkBoxMatch_CheckedChanged);
            // 
            // labelKasanCost
            // 
            this.labelKasanCost.AutoSize = true;
            this.labelKasanCost.Enabled = false;
            this.labelKasanCost.Location = new System.Drawing.Point(708, 54);
            this.labelKasanCost.Name = "labelKasanCost";
            this.labelKasanCost.Size = new System.Drawing.Size(43, 26);
            this.labelKasanCost.TabIndex = 25;
            this.labelKasanCost.Text = "加算料\r\n　合計";
            this.labelKasanCost.Visible = false;
            // 
            // labelOryoCost
            // 
            this.labelOryoCost.AutoSize = true;
            this.labelOryoCost.Enabled = false;
            this.labelOryoCost.Location = new System.Drawing.Point(598, 54);
            this.labelOryoCost.Name = "labelOryoCost";
            this.labelOryoCost.Size = new System.Drawing.Size(43, 26);
            this.labelOryoCost.TabIndex = 23;
            this.labelOryoCost.Text = "往療料\r\n　合計";
            this.labelOryoCost.Visible = false;
            // 
            // verifyBoxKasanCost
            // 
            this.verifyBoxKasanCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKasanCost.Enabled = false;
            this.verifyBoxKasanCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKasanCost.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxKasanCost.Location = new System.Drawing.Point(754, 55);
            this.verifyBoxKasanCost.Name = "verifyBoxKasanCost";
            this.verifyBoxKasanCost.NewLine = false;
            this.verifyBoxKasanCost.Size = new System.Drawing.Size(61, 23);
            this.verifyBoxKasanCost.TabIndex = 26;
            this.verifyBoxKasanCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKasanCost.TextV = "";
            this.verifyBoxKasanCost.Visible = false;
            // 
            // verifyBoxOryoCost
            // 
            this.verifyBoxOryoCost.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxOryoCost.Enabled = false;
            this.verifyBoxOryoCost.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxOryoCost.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxOryoCost.Location = new System.Drawing.Point(642, 55);
            this.verifyBoxOryoCost.Name = "verifyBoxOryoCost";
            this.verifyBoxOryoCost.NewLine = false;
            this.verifyBoxOryoCost.Size = new System.Drawing.Size(61, 23);
            this.verifyBoxOryoCost.TabIndex = 24;
            this.verifyBoxOryoCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxOryoCost.TextV = "";
            this.verifyBoxOryoCost.Visible = false;
            // 
            // labelOryoKasan
            // 
            this.labelOryoKasan.AutoSize = true;
            this.labelOryoKasan.Enabled = false;
            this.labelOryoKasan.Location = new System.Drawing.Point(499, 68);
            this.labelOryoKasan.Name = "labelOryoKasan";
            this.labelOryoKasan.Size = new System.Drawing.Size(31, 13);
            this.labelOryoKasan.TabIndex = 20;
            this.labelOryoKasan.Text = "加算";
            this.labelOryoKasan.Visible = false;
            // 
            // labelOryoKm
            // 
            this.labelOryoKm.AutoSize = true;
            this.labelOryoKm.Enabled = false;
            this.labelOryoKm.Location = new System.Drawing.Point(579, 68);
            this.labelOryoKm.Name = "labelOryoKm";
            this.labelOryoKm.Size = new System.Drawing.Size(21, 13);
            this.labelOryoKm.TabIndex = 22;
            this.labelOryoKm.Text = "km";
            this.labelOryoKm.Visible = false;
            // 
            // verifyBoxKasanKm
            // 
            this.verifyBoxKasanKm.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKasanKm.Enabled = false;
            this.verifyBoxKasanKm.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKasanKm.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxKasanKm.Location = new System.Drawing.Point(533, 55);
            this.verifyBoxKasanKm.Name = "verifyBoxKasanKm";
            this.verifyBoxKasanKm.NewLine = false;
            this.verifyBoxKasanKm.Size = new System.Drawing.Size(46, 23);
            this.verifyBoxKasanKm.TabIndex = 21;
            this.verifyBoxKasanKm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKasanKm.TextV = "";
            this.verifyBoxKasanKm.Visible = false;
            this.verifyBoxKasanKm.TextChanged += new System.EventHandler(this.verifyBoxKasanKm_TextChanged);
            // 
            // verifyCheckBoxVisitKasan
            // 
            this.verifyCheckBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisitKasan.CheckedV = false;
            this.verifyCheckBoxVisitKasan.Enabled = false;
            this.verifyCheckBoxVisitKasan.Location = new System.Drawing.Point(412, 55);
            this.verifyCheckBoxVisitKasan.Name = "verifyCheckBoxVisitKasan";
            this.verifyCheckBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxVisitKasan.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxVisitKasan.TabIndex = 19;
            this.verifyCheckBoxVisitKasan.Text = "加算あり";
            this.verifyCheckBoxVisitKasan.UseVisualStyleBackColor = false;
            this.verifyCheckBoxVisitKasan.CheckedChanged += new System.EventHandler(this.checkBoxVisitControls_CheckedChanged);
            this.verifyCheckBoxVisitKasan.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyCheckBoxLeftLower
            // 
            this.verifyCheckBoxLeftLower.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxLeftLower.CheckedV = false;
            this.verifyCheckBoxLeftLower.Location = new System.Drawing.Point(464, 221);
            this.verifyCheckBoxLeftLower.Name = "verifyCheckBoxLeftLower";
            this.verifyCheckBoxLeftLower.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxLeftLower.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxLeftLower.TabIndex = 45;
            this.verifyCheckBoxLeftLower.Text = "左下肢";
            this.verifyCheckBoxLeftLower.UseVisualStyleBackColor = false;
            this.verifyCheckBoxLeftLower.Visible = false;
            this.verifyCheckBoxLeftLower.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyCheckBoxRightLower
            // 
            this.verifyCheckBoxRightLower.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxRightLower.CheckedV = false;
            this.verifyCheckBoxRightLower.Location = new System.Drawing.Point(382, 221);
            this.verifyCheckBoxRightLower.Name = "verifyCheckBoxRightLower";
            this.verifyCheckBoxRightLower.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxRightLower.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxRightLower.TabIndex = 44;
            this.verifyCheckBoxRightLower.Text = "右下肢";
            this.verifyCheckBoxRightLower.UseVisualStyleBackColor = false;
            this.verifyCheckBoxRightLower.Visible = false;
            this.verifyCheckBoxRightLower.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyCheckBoxLeftUpper
            // 
            this.verifyCheckBoxLeftUpper.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxLeftUpper.CheckedV = false;
            this.verifyCheckBoxLeftUpper.Location = new System.Drawing.Point(300, 221);
            this.verifyCheckBoxLeftUpper.Name = "verifyCheckBoxLeftUpper";
            this.verifyCheckBoxLeftUpper.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxLeftUpper.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxLeftUpper.TabIndex = 43;
            this.verifyCheckBoxLeftUpper.Text = "左上肢";
            this.verifyCheckBoxLeftUpper.UseVisualStyleBackColor = false;
            this.verifyCheckBoxLeftUpper.Visible = false;
            this.verifyCheckBoxLeftUpper.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyCheckBoxRightUpper
            // 
            this.verifyCheckBoxRightUpper.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxRightUpper.CheckedV = false;
            this.verifyCheckBoxRightUpper.Location = new System.Drawing.Point(218, 221);
            this.verifyCheckBoxRightUpper.Name = "verifyCheckBoxRightUpper";
            this.verifyCheckBoxRightUpper.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxRightUpper.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxRightUpper.TabIndex = 42;
            this.verifyCheckBoxRightUpper.Text = "右上肢";
            this.verifyCheckBoxRightUpper.UseVisualStyleBackColor = false;
            this.verifyCheckBoxRightUpper.Visible = false;
            this.verifyCheckBoxRightUpper.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyCheckBoxBody
            // 
            this.verifyCheckBoxBody.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxBody.CheckedV = false;
            this.verifyCheckBoxBody.Location = new System.Drawing.Point(136, 221);
            this.verifyCheckBoxBody.Name = "verifyCheckBoxBody";
            this.verifyCheckBoxBody.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxBody.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxBody.TabIndex = 41;
            this.verifyCheckBoxBody.Text = "体幹";
            this.verifyCheckBoxBody.UseVisualStyleBackColor = false;
            this.verifyCheckBoxBody.Visible = false;
            this.verifyCheckBoxBody.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyCheckBoxVisit
            // 
            this.verifyCheckBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisit.CheckedV = false;
            this.verifyCheckBoxVisit.Location = new System.Drawing.Point(328, 55);
            this.verifyCheckBoxVisit.Name = "verifyCheckBoxVisit";
            this.verifyCheckBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.verifyCheckBoxVisit.Size = new System.Drawing.Size(76, 27);
            this.verifyCheckBoxVisit.TabIndex = 18;
            this.verifyCheckBoxVisit.Text = "往療あり";
            this.verifyCheckBoxVisit.UseVisualStyleBackColor = false;
            this.verifyCheckBoxVisit.CheckedChanged += new System.EventHandler(this.checkBoxVisitControls_CheckedChanged);
            this.verifyCheckBoxVisit.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelF1
            // 
            this.labelF1.AutoSize = true;
            this.labelF1.Location = new System.Drawing.Point(6, 96);
            this.labelF1.Name = "labelF1";
            this.labelF1.Size = new System.Drawing.Size(49, 13);
            this.labelF1.TabIndex = 27;
            this.labelF1.Text = "負傷名1";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 252);
            this.scrollPictureControl1.Margin = new System.Windows.Forms.Padding(4);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 108);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1019, 345);
            this.scrollPictureControl1.TabIndex = 46;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(228, 113);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF2.TabIndex = 30;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF2.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(481, 728);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 75;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(450, 113);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF3.TabIndex = 32;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF3.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxBuiCount
            // 
            this.verifyBoxBuiCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuiCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuiCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBuiCount.Location = new System.Drawing.Point(47, 222);
            this.verifyBoxBuiCount.Name = "verifyBoxBuiCount";
            this.verifyBoxBuiCount.NewLine = false;
            this.verifyBoxBuiCount.Size = new System.Drawing.Size(48, 23);
            this.verifyBoxBuiCount.TabIndex = 40;
            this.verifyBoxBuiCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBuiCount.TextV = "";
            this.verifyBoxBuiCount.Visible = false;
            this.verifyBoxBuiCount.TextChanged += new System.EventHandler(this.verifyBoxBuiCount_TextChanged);
            this.verifyBoxBuiCount.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(6, 113);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF1.TabIndex = 28;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelBui
            // 
            this.labelBui.AutoSize = true;
            this.labelBui.Location = new System.Drawing.Point(105, 234);
            this.labelBui.Name = "labelBui";
            this.labelBui.Size = new System.Drawing.Size(31, 13);
            this.labelBui.TabIndex = 40;
            this.labelBui.Text = "部位";
            this.labelBui.Visible = false;
            // 
            // labelBuiCount
            // 
            this.labelBuiCount.AutoSize = true;
            this.labelBuiCount.Location = new System.Drawing.Point(6, 234);
            this.labelBuiCount.Name = "labelBuiCount";
            this.labelBuiCount.Size = new System.Drawing.Size(43, 13);
            this.labelBuiCount.TabIndex = 38;
            this.labelBuiCount.Text = "部位数";
            this.labelBuiCount.Visible = false;
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(6, 163);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF4.TabIndex = 34;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            this.verifyBoxF4.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(228, 163);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(216, 23);
            this.verifyBoxF5.TabIndex = 36;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.Enter += new System.EventHandler(this.verifyBox_Enter);
            // 
            // labelStatusOCR
            // 
            this.labelStatusOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStatusOCR.AutoSize = true;
            this.labelStatusOCR.Location = new System.Drawing.Point(6, 735);
            this.labelStatusOCR.Name = "labelStatusOCR";
            this.labelStatusOCR.Size = new System.Drawing.Size(30, 13);
            this.labelStatusOCR.TabIndex = 50;
            this.labelStatusOCR.Text = "OCR";
            // 
            // labelF2
            // 
            this.labelF2.AutoSize = true;
            this.labelF2.Location = new System.Drawing.Point(226, 96);
            this.labelF2.Name = "labelF2";
            this.labelF2.Size = new System.Drawing.Size(49, 13);
            this.labelF2.TabIndex = 29;
            this.labelF2.Text = "負傷名2";
            // 
            // labelF3
            // 
            this.labelF3.AutoSize = true;
            this.labelF3.Location = new System.Drawing.Point(448, 96);
            this.labelF3.Name = "labelF3";
            this.labelF3.Size = new System.Drawing.Size(49, 13);
            this.labelF3.TabIndex = 31;
            this.labelF3.Text = "負傷名3";
            // 
            // dataGridRefRece
            // 
            this.dataGridRefRece.AllowUserToAddRows = false;
            this.dataGridRefRece.AllowUserToDeleteRows = false;
            this.dataGridRefRece.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridRefRece.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridRefRece.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridRefRece.Location = new System.Drawing.Point(-1, 597);
            this.dataGridRefRece.Name = "dataGridRefRece";
            this.dataGridRefRece.ReadOnly = true;
            this.dataGridRefRece.RowHeadersVisible = false;
            this.dataGridRefRece.RowTemplate.Height = 21;
            this.dataGridRefRece.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridRefRece.Size = new System.Drawing.Size(674, 127);
            this.dataGridRefRece.StandardTab = true;
            this.dataGridRefRece.TabIndex = 50;
            this.dataGridRefRece.TabStop = false;
            // 
            // labelF4
            // 
            this.labelF4.AutoSize = true;
            this.labelF4.Location = new System.Drawing.Point(6, 146);
            this.labelF4.Name = "labelF4";
            this.labelF4.Size = new System.Drawing.Size(49, 13);
            this.labelF4.TabIndex = 33;
            this.labelF4.Text = "負傷名4";
            // 
            // labelF5
            // 
            this.labelF5.AutoSize = true;
            this.labelF5.Location = new System.Drawing.Point(226, 146);
            this.labelF5.Name = "labelF5";
            this.labelF5.Size = new System.Drawing.Size(49, 13);
            this.labelF5.TabIndex = 35;
            this.labelF5.Text = "負傷名5";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label26.Location = new System.Drawing.Point(6, 195);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(493, 13);
            this.label26.TabIndex = 37;
            this.label26.Text = "鍼灸時：　　1.神経痛　2.リウマチ　3.頚腕症候群　4.五十肩　5.腰痛症　6.頸椎捻挫後遺症　7.その他";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(54, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 757);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1077, 757);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1082, 748);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.inputForm_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.pTotal.ResumeLayout(false);
            this.pTotal.PerformLayout();
            this.pF1.ResumeLayout(false);
            this.pF1.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            this.pSejutu.ResumeLayout(false);
            this.pSejutu.PerformLayout();
            this.panelMatchWhere.ResumeLayout(false);
            this.panelMatchWhere.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRefRece)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelF1;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Button buttonImageFill;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Label labelMacthCheck;
        private System.Windows.Forms.Label labelAppStatus;
        private System.Windows.Forms.DataGridView dataGridRefRece;
        private System.Windows.Forms.Label labelStatusOCR;
        private System.Windows.Forms.Label label18;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Label labelF5;
        private System.Windows.Forms.Label labelF4;
        private System.Windows.Forms.Label labelF3;
        private System.Windows.Forms.Label labelF2;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyCheckBox verifyCheckBoxVisit;
        private System.Windows.Forms.Label labelKasanCost;
        private System.Windows.Forms.Label labelOryoCost;
        private VerifyBox verifyBoxKasanCost;
        private VerifyBox verifyBoxOryoCost;
        private System.Windows.Forms.Label labelOryoKasan;
        private System.Windows.Forms.Label labelOryoKm;
        private VerifyBox verifyBoxKasanKm;
        private VerifyCheckBox verifyCheckBoxVisitKasan;
        private VerifyCheckBox verifyCheckBoxLeftLower;
        private VerifyCheckBox verifyCheckBoxRightLower;
        private VerifyCheckBox verifyCheckBoxLeftUpper;
        private VerifyCheckBox verifyCheckBoxRightUpper;
        private VerifyCheckBox verifyCheckBoxBody;
        private VerifyBox verifyBoxBuiCount;
        private System.Windows.Forms.Label labelBui;
        private System.Windows.Forms.Label labelBuiCount;
        private System.Windows.Forms.Panel panelMatchWhere;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxTotal;
        private System.Windows.Forms.CheckBox checkBoxNumber;
        private System.Windows.Forms.Label labelInputUser;
        private System.Windows.Forms.Button buttonDistance;
        private VerifyBox verifyBoxF1FirstY;
        private VerifyBox verifyBoxF1FirstM;
        private System.Windows.Forms.Label labelF1First;
        private System.Windows.Forms.Label labelF1FirstY;
        private System.Windows.Forms.Label labelF1FirstM;
        private System.Windows.Forms.Label lblSejutu;
        private VerifyBox vbSejutuY;
        private VerifyBox vbSejutuM;
        private System.Windows.Forms.Label lblSejutuY;
        private System.Windows.Forms.Label lblSejutuM;
        private VerifyCheckBox vcSejutu;
        private VerifyBox vbDouiD;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDoui;
        private VerifyBox vbDouiY;
        private VerifyBox vbDouiM;
        private System.Windows.Forms.Label lblDouiY;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Panel pDoui;
        private System.Windows.Forms.Panel pSejutu;
        private VerifyBox vbDouiG;
        private VerifyBox vbSejutuG;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pF1;
        private System.Windows.Forms.Panel pTotal;
        private System.Windows.Forms.Button buttonReInput;
    }
}