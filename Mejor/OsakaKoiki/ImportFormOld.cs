﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NpgsqlTypes;

namespace Mejor.OsakaKoiki
{
    public partial class ImportFormOld : Form
    {
        public ImportFormOld(int cy, int cm)
        {
            InitializeComponent();
            textBoxY.Text = cy.ToString();
            textBoxM.Text = cm.ToString();
            var bs = new BindingSource();

            bs.DataSource = dataCount.GetDataCount();
            bs.ResetBindings(false);

            dataGridView1.DataSource = bs;
            dataGridView1.Columns[nameof(dataCount.aym)].Width = 40;
            dataGridView1.Columns[nameof(dataCount.dtype)].Width = 40;
            dataGridView1.Columns[nameof(dataCount.merge)].Width = 40;
            dataGridView1.Columns[nameof(dataCount.count)].Width = 60;
        }

        class dataCount
        {
            public int aym { get; private set; }
            public int dtype { get; private set; }
            public int merge { get; private set; }
            public long count { get; private set; }

            public static List<dataCount> GetDataCount()
            {
                var list = new List<dataCount>();
                using (var cmd = DB.Main.CreateCmd("SELECT " +
                    "aym, dtype, merge, COUNT(did) FROM koikidata " +
                    "GROUP BY aym, dtype, merge " +
                    "ORDER BY aym, dtype, merge;"))
                {
                    var l = cmd.TryExecuteReaderList();
                    //var dic = new Dictionary<int, ScanGroup>();
                    //var i = 1;
                    foreach (var item in l)
                    {
                        var dc = new dataCount();
                        dc.aym = (int)(item[0] == DBNull.Value ? 0 : item[0]);
                        dc.dtype = (int)(item[1] == DBNull.Value ? 0 : item[1]);
                        dc.merge = (int)(item[2] == DBNull.Value ? 0 : item[2]);
                        dc.count = (long)(item[3] == DBNull.Value ? 0 : item[3]);
                        list.Add(dc);
                    }
                }
                return list;
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonFselect_Click(object sender, EventArgs e)
        {
            int y, m;
            string fn = "";
            int tp = 0;
            int dtp = 0;

            if (!int.TryParse(textBoxY.Text, out y))
            {
                MessageBox.Show("請求年を指定してください");
                return;
            }
            else if (y < DateTimeEx.GetJpYear(DateTime.Today) - 2 || y > DateTimeEx.GetJpYear(DateTime.Today))
            {
                MessageBox.Show("請求年度の指定が不正です\r\n(" + DateTimeEx.GetJpYear(DateTime.Today) + "±1)");
                return;
            }

            if (!int.TryParse(textBoxM.Text, out m))
            {
                MessageBox.Show("請求月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }

            if (radioB_JJigoAll.Checked == true)
            {
                tp = 1;
                dtp = 1;
            }
            else if (radioB_JJigoPart.Checked == true)
            {
                tp = 2;
                dtp = 1;
            }
            else if (radioB_HJigoAll.Checked == true)
            {
                tp = 3;
                dtp = 2;
            }
            else if (radioB_HJigoPart.Checked == true)
            {
                tp = 4;
                dtp = 2;
            }
            else if (radioB_JJizen.Checked == true)
            {
                tp = 5;
                dtp = 3;
            }
            else
            {
                MessageBox.Show("インポートするファイルの種類を指定してください");
                return;
            }

            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == DialogResult.OK) fn = ofd.FileName;
                else return;
            }

            var dat = CommonTool.CsvImportShiftJis(fn);

            int csvCount = dat.Count();
            int datCount = Data_OsakaKoiki.dataCount(dtp, y, m);
            var res = MessageBox.Show("CSVファイル内に " + csvCount + " 件のデータを検出しました。\r\n" +
                datCount + " 件のデータがすでにデータベースに存在します。\r\n\r\n" +
                "この作業はデータベースに大きな負荷がかかるため、\r\n" +
                "他の方が作業していないことを確認してから開始してください。\r\n\r\n" +
                "インポートを実行しますか？", "",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (res == System.Windows.Forms.DialogResult.Cancel) return;


            var d = new Data_OsakaKoiki();
            bool b = false;
            int i = 0;

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.BarStyle = ProgressBarStyle.Continuous;

                wf.SetMax(dat.Count);
                wf.LogPrint("処理を開始しました");

                //todo:SetEnableFlagの返り値Countが正確でない(pnum重複分がカウントされない)
                switch (tp)
                {
                    case 1:
                        using (var tran = DB.Main.CreateTransaction())
                        {
                            foreach (var item in dat)
                            {
                                //最終行でデータが無いエラーを回避のため
                                if (item.Length < 5) continue;

                                d = Data_OsakaKoiki.CreateDatFromJJigo(item, y, m);
                                b = Data_OsakaKoiki.DataEntry(d, tran);

                                if (!b)
                                {
                                    tran.Rollback();
                                    return;
                                }
                                i++;
                                wf.InvokeValue = i;
                            }
                            tran.Commit();
                        }
                        break;

                    case 2:
                        i = Data_OsakaKoiki.SetEnableFrag(fn, y, m, wf);
                        break;

                    case 3:
                        using (var tran = DB.Main.CreateTransaction())
                        {
                            foreach (var item in dat)
                            {
                                //最終行でデータが無いエラーを回避のため
                                if (item.Length < 5) continue;

                                d = Data_OsakaKoiki.CreateDatFromHJigo(item, y, m);
                                b = Data_OsakaKoiki.DataEntry(d, tran);

                                if (!b)
                                {
                                    tran.Rollback();
                                    return;
                                }
                                i++;
                                wf.InvokeValue = i;
                            }
                            tran.Commit();
                        }
                        break;

                    case 4:
                        i = Data_OsakaKoiki.SetEnableFrag(fn, y, m, wf);
                        break;

                    case 5:
                        using (var tran = DB.Main.CreateTransaction())
                        {
                            foreach (var item in dat)
                            {
                                //最終行でデータが無いエラーを回避のため
                                if (item.Length < 5) continue;

                                //事前点検データのみ、先頭2行に不要行が存在するため、
                                //日付欄を数値変換できるかでデータ行かどうかを確認
                                int checknum;
                                if (!int.TryParse(item[3], out checknum)) continue;
                                if (!int.TryParse(item[4], out checknum)) continue;

                                d = Data_OsakaKoiki.CreateDatFromJJizen(item, y, m);
                                b = Data_OsakaKoiki.DataEntry(d);//, tran);

                                if (!b)
                                {
                                    tran.Rollback();
                                    return;
                                }
                                i++;
                                wf.InvokeValue = i;
                            }
                            tran.Commit();
                        }
                        break;
                }

                MessageBox.Show(i + " 件のデータインポートが完了しました");
                this.Close();
            }
        }
    }
}
