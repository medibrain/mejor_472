﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.OsakaKoiki
{
    /// <summary>
    /// 提供データあはきのcsvを扱う(元々のExcelファイルからcsvファイルを柔整部に作って貰う)
    /// </summary>
    public class tdata_ahk
    {

        #region テーブル
        [DB.DbAttribute.Serial]
        public int tdataahkid { get;set; }=0;                                    //プライマリキー メホール管理用;
        public int cym { get; set; } = 0;                                        //メホール上の処理年月 メホール管理用 yyyymm;
        public int ym { get; set; } = 0;                                         //診療年月 メホール管理用 yyyymm;

        public int sortno { get; set; } = 0;                                     //No.;
        public string insnum { get; set; } = string.Empty;                       //保険者番号;
        public string pubexpenditure { get; set; } = string.Empty;               //公費負担者番号;
        public string hnum { get; set; } = string.Empty;                         //被保険者証番号;
        public string pubfunding { get; set; } = string.Empty;                   //公費受給者番号;
        public string pbirthday { get; set; } = string.Empty;                    //生年月日;
        public int pgender { get; set; } = 0;                                    //性別;
        public string pname { get; set; } = string.Empty;                        //氏名;
        public string sejutsuym { get; set; } = string.Empty;                    //施術年月;
        public string clinicnum { get; set; } = string.Empty;                    //医療機関コード;
        public string drnum { get; set; } = string.Empty;                        //登録記号番号;
        public string shinsaym { get; set; } = string.Empty;                     //審査年月;
        public string detailnum2 { get; set; } = string.Empty;                   //明細番号２;
        public string shikyukbn { get; set; } = string.Empty;                    //支給区分;
        public string henreireason { get; set; } = string.Empty;                 //返戻理由;
        public string dispname { get; set; } = string.Empty;                     //通知用氏名;
        public string gigireason { get; set; } = string.Empty;                   //疑義（広域連合）理由;
        public string shisakaicomment { get; set; } = string.Empty;              //審査会コメント;
        public string shisaresult { get; set; } = string.Empty;                  //審査結果;
        public string errorumu { get; set; } = string.Empty;                     //エラー有無区分;
        public string skkerrcode1 { get; set; } = string.Empty;                  //資格エラー情報コード1;
        public string skkerrname1 { get; set; } = string.Empty;                  //資格エラー情報名称1;
        public string skkerrcode2 { get; set; } = string.Empty;                  //資格エラー情報コード2;
        public string skkerrname2 { get; set; } = string.Empty;                  //資格エラー情報名称2;
        public string skkerrcode3 { get; set; } = string.Empty;                  //資格エラー情報コード3;
        public string skkerrname3 { get; set; } = string.Empty;                  //資格エラー情報名称3;
        public string skkerrcode4 { get; set; } = string.Empty;                  //資格エラー情報コード4;
        public string skkerrname4 { get; set; } = string.Empty;                  //資格エラー情報名称4;
        public string skkerrcode5 { get; set; } = string.Empty;                  //資格エラー情報コード5;
        public string skkerrname5 { get; set; } = string.Empty;                  //資格エラー情報名称5;
        public string ouran { get; set; } = string.Empty;                        //重複エラー横覧;
        public string juuran { get; set; } = string.Empty;                       //重複エラー縦覧;
        public string ryouyouhi_shubetsu { get; set; } = string.Empty;           //療養費種別;
        public string hoken_shubetsu { get; set; } = string.Empty;               //保険種別;
        public string honkenyugai { get; set; } = string.Empty;                  //本家入外;
        public string daisaansha { get; set; } = string.Empty;                   //業務上・外、第三者行為の有無;
        public string seikyukbn { get; set; } = string.Empty;                    //請求区分;
        public string firstdate1 { get; set; } = string.Empty;                   //初療年月日;
        public string startdate1 { get; set; } = string.Empty;                   //施術期間自;
        public string finishdate1 { get; set; } = string.Empty;                  //施術期間至;
        public int counteddays { get; set; } = 0;                                //実日数;
        public string f1name { get; set; } = string.Empty;                       //傷病名(コード);
        public string shokenryokbn { get; set; } = string.Empty;                 //初検料区分;
        public string shokenryo { get; set; } = string.Empty;                    //初検料;
        public string until4km { get; set; } = string.Empty;                     //往療料回数4kmまで;
        public string over4km { get; set; } = string.Empty;                      //往療料回数4km超;
        public int total { get; set; } = 0;                                      //合計額;
        public int partial { get; set; } = 0;                                    //一部負担金額;
        public int charge { get; set; } = 0;                                     //請求額;
        public int sejutsum { get; set; } = 0;                                   //施術日月;
        public string sejutsud { get; set; } = string.Empty;                     //施術日日;
        public string sejutsuddisp { get; set; } = string.Empty;                 //施術日表示;
        public string sejutsu_shomei_ymd { get; set; } = string.Empty;           //施術証明年月日;
        public string hokenshokbn { get; set; } = string.Empty;                  //保健所登録区分;
        public string shinseiymd { get; set; } = string.Empty;                   //申請年月日;
        public string accnumber { get; set; } = string.Empty;                    //支払口座番号;
        public string douiymd { get; set; } = string.Empty;                      //同意年月日;
        public string ininymd { get; set; } = string.Empty;                      //委任年月日;
        public string detailnum1 { get; set; } = string.Empty;                   //明細番号;
        public string yobi01 { get; set; } = string.Empty;                       //予備01;
        public string yobi02 { get; set; } = string.Empty;                       //予備02;
        public string yobi03 { get; set; } = string.Empty;                       //予備03;
        public string yobi04 { get; set; } = string.Empty;                       //予備04;
        public string yobi05 { get; set; } = string.Empty;                       //予備05;
        public string yobi06 { get; set; } = string.Empty;                       //予備06;
        public string yobi07 { get; set; } = string.Empty;                       //予備07;
        public string yobi08 { get; set; } = string.Empty;                       //予備08;
        public string yobi09 { get; set; } = string.Empty;                       //予備09;
        public string yobi10 { get; set; } = string.Empty;                       //予備10;
        public string biko { get; set; } = string.Empty;                         //備考;

        public DateTime pbirthdayad { get; set; } = DateTime.MinValue;           //生年月日西暦;
        public string sejutsuymad { get; set; } = string.Empty;                  //施術年月西暦yyyymm;
        public string shinsaymad { get; set; } = string.Empty;                   //審査年月西暦yyyymm;
        public DateTime firstdate1ad { get; set; } = DateTime.MinValue;          //初療年月日（初検日）西暦;
        public DateTime startdate1ad { get; set; } = DateTime.MinValue;          //施術期間自西暦;
        public DateTime finishdate1ad { get; set; } = DateTime.MinValue;         //施術期間至西暦;
        public DateTime sejutsu_shomei_ymdad { get; set; } = DateTime.MinValue;  //施術証明年月日西暦;
        public DateTime shinseiymdad { get; set; } = DateTime.MinValue;          //申請年月日西暦;
        public DateTime douiymdad { get; set; } = DateTime.MinValue;             //同意年月日西暦;
        public DateTime ininymdad { get; set; } = DateTime.MinValue;             //委任年月日西暦;
        public string hihomark_nr { get; set; } = string.Empty;                  //証記号半角;
        public string hihonum_nr { get; set; } = string.Empty;                   //証番号半角;



        #endregion


        private static int intcym;
        private static string strFileName;
        

        public static bool dataimport_ahk(string _strFileName ,int _cym)
        {
                        
            intcym = _cym;
            strFileName = _strFileName;

            if(!System.IO.File.Exists(strFileName))
            {
                System.Windows.Forms.MessageBox.Show("ファイルが見つかりません"
                    ,""
                    ,System.Windows.Forms.MessageBoxButtons.OK
                    ,System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();


            //構想中
            //エクセル→CSVへの加工、そのCSVをcopyでpostgresに取り込んだ方が時間的に早い
            //CreateFileForImport(strFileName);

            //copyToPostgres(strFileName);

            //csvインポート
            if (!import(wf))
            {
                wf.Dispose();
                return false;
            }

            wf.LogPrint("appあはきレコードとマージ");


            //20211221152637 furukawa st ////////////////////////
            //CYM指定にしないと全部回る
            
            if (!Merge(wf,_cym))
            //      if (!Merge(wf))
            //20211221152637 furukawa ed ////////////////////////

            {
                wf.Dispose();
                return false;
            }

            wf.LogPrint("正常終了");

            wf.Dispose();
            return true;

        }



        /// <summary>
        /// 令和3年4月審査分　あはき療養費点検入力シート（後期分）.xlsm等のエクセルをCSVに変換
        /// </summary>
        /// <param name="strFileNameFormImport"></param>
        /// <returns></returns>
        private static bool CreateFileForImport(string strFileNameFormImport)
        {

            NPOI.XSSF.UserModel.XSSFWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileNameFormImport);
            NPOI.SS.UserModel.ISheet ws=wb.GetSheet("点検データ入力シート（後期）");

            NPOI.SS.UserModel.IRow row = ws.GetRow(3);
            NPOI.SS.UserModel.ICell cell = row.GetCell(70);
            if (cell.StringCellValue != "備考") return false;

            System.IO.StreamWriter sw = new System.IO.StreamWriter("tdata_ahk.csv",false,System.Text.Encoding.GetEncoding("shift-jis"));

            string strLine = string.Empty;
            try
            {
                List<string> lst = new List<string>();
                for (int rowcnt=5;rowcnt<ws.LastRowNum;rowcnt++) 
                {
                    NPOI.SS.UserModel.IRow r = ws.GetRow(rowcnt);
                    strLine = string.Empty;
                    for (int col = 0; col < 70; col++)
                    {
                        strLine+= $"\"{CommonTool.getCellValueToString(r.Cells[col]).Trim()}\",";
                    }
                    strLine = strLine.Substring(0, strLine.Length - 1);
                    lst.Add(strLine);
                }

                foreach(string s in lst)
                {
                    sw.WriteLine(s);
                }
                return true;
            }catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(
                    $"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{strLine}\r\n{ex.Message}");
                return false;
            }
            finally
            {
                wb.Close();
                sw.Close();
            }



        }


        /// <summary>
        /// 変換したCSVのチェック
        /// </summary>
        /// <param name="strCSVFilename"></param>
        /// <returns></returns>
        private static bool checkCSVFile(string strCSVFilename)
        {
            List<string[]> lstline = CommonTool.CsvImportMultiCode(strFileName);

            string[] tmp = lstline[0];

            if (tmp.Length != 70)
            {
                System.Windows.Forms.MessageBox.Show("70列ありません。ファイルの内容を確認お願いします");
                return false;
            }
          

            return true;
        }



        ///// <summary>
        ///// 変換したCSVをpostgresのcopyで一気に取り込む
        ///// </summary>
        ///// <param name="strCSVFileName"></param>
        ///// <returns></returns>
        //private static bool copyToPostgres(string strCSVFileName)
        //{

        //    if (!checkCSVFile(strCSVFileName)) return false;


        //    System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
        //    DB.clsDBServer cls = new DB.clsDBServer();
        //    si.Arguments = $"{DB.getDBHost()} {cls.intDBPort} {DB.GetMainDBName()} {strCSVFileName}";
        //    si.FileName = "OsakaKoiki\\import.bat";
        //    si.UseShellExecute = true;
        //    si.CreateNoWindow = false;

        //    System.Diagnostics.Process p = new System.Diagnostics.Process();
        //    p.StartInfo = si;
           
        //    p.Start();
        //    p.WaitForExit();
        //    return true;
        //}


        /// <summary>
        /// csvファイルのインポート
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool import(WaitForm wf)
        {

            List<string[]> lstline=CommonTool.CsvImportMultiCode(strFileName);

            wf.LogPrint("csvインポート");
            wf.SetMax(lstline.Count-1);
         
            DB.Transaction tran = DB.Main.CreateTransaction();
            DB.Command cmd = DB.Main.CreateCmd($"delete from tdata_ahk where cym={intcym}",tran);
            cmd.TryExecuteNonQuery();

            int c = 0;//列番号
            try
            {
                foreach (string[] tmp in lstline)
                {
                    if(CommonTool.WaitFormCancelProcess(wf,tran))
                    {
                        tran.Rollback();
                        return false;
                    }

                    if (tmp.Length != 70)
                    {
                        System.Windows.Forms.MessageBox.Show("70列ありません。ファイルの内容を確認お願いします");
                        tran.Rollback();
                        return false;
                    }

                    c = 0;

                    if (!int.TryParse(tmp[0].ToString(), out int inttmp)) continue;//1列目が数字でない場合飛ばす
                        

                    tdata_ahk ahk = new tdata_ahk();

                    ahk.cym = intcym;             //メホール上の処理年月 メホール管理用 yyyymm;
                    ahk.ym = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(tmp[8].ToString().Trim()));              //診療年月 メホール管理用 yyyymm;

                    ahk.sortno = int.Parse(tmp[c++].ToString().Trim());          //No.;      
                    ahk.insnum = tmp[c++].ToString().Trim();                     //保険者番号;
                    ahk.pubexpenditure = tmp[c++].ToString().Trim();             //公費負担者番号;
                    ahk.hnum = tmp[c++].ToString().Trim();                       //被保険者証番号;
                    ahk.pubfunding = tmp[c++].ToString().Trim();                 //公費受給者番号;
                    ahk.pbirthday = tmp[c++].ToString().Trim();                  //生年月日;
                    ahk.pgender = int.Parse(tmp[c++].ToString().Trim());         //性別;
                    ahk.pname = tmp[c++].ToString().Trim();                      //氏名;
                    ahk.sejutsuym = tmp[c++].ToString().Trim();                  //施術年月;
                    ahk.clinicnum = tmp[c++].ToString().Trim();                  //医療機関コード;
                    ahk.drnum = tmp[c++].ToString().Trim();                      //登録記号番号;
                    ahk.shinsaym = tmp[c++].ToString().Trim();                   //審査年月;
                    ahk.detailnum2 = tmp[c++].ToString().Trim();                 //明細番号２;
                    ahk.shikyukbn = tmp[c++].ToString().Trim();                  //支給区分;
                    ahk.henreireason = tmp[c++].ToString().Trim();               //返戻理由;
                    ahk.dispname = tmp[c++].ToString().Trim();                   //通知用氏名;
                    ahk.gigireason = tmp[c++].ToString().Trim();                 //疑義（広域連合）理由;
                    ahk.shisakaicomment = tmp[c++].ToString().Trim();            //審査会コメント;
                    ahk.shisaresult = tmp[c++].ToString().Trim();                //審査結果;
                    ahk.errorumu = tmp[c++].ToString().Trim();                   //エラー有無区分;
                    ahk.skkerrcode1 = tmp[c++].ToString().Trim();                //資格エラー情報コード1;
                    ahk.skkerrname1 = tmp[c++].ToString().Trim();                //資格エラー情報名称1;
                    ahk.skkerrcode2 = tmp[c++].ToString().Trim();                //資格エラー情報コード2;
                    ahk.skkerrname2 = tmp[c++].ToString().Trim();                //資格エラー情報名称2;
                    ahk.skkerrcode3 = tmp[c++].ToString().Trim();                //資格エラー情報コード3;
                    ahk.skkerrname3 = tmp[c++].ToString().Trim();                //資格エラー情報名称3;
                    ahk.skkerrcode4 = tmp[c++].ToString().Trim();                //資格エラー情報コード4;
                    ahk.skkerrname4 = tmp[c++].ToString().Trim();                //資格エラー情報名称4;
                    ahk.skkerrcode5 = tmp[c++].ToString().Trim();                //資格エラー情報コード5;
                    ahk.skkerrname5 = tmp[c++].ToString().Trim();                //資格エラー情報名称5;
                    ahk.ouran = tmp[c++].ToString().Trim();                      //重複エラー横覧;
                    ahk.juuran = tmp[c++].ToString().Trim();                     //重複エラー縦覧;
                    ahk.ryouyouhi_shubetsu = tmp[c++].ToString().Trim();         //療養費種別;
                    ahk.hoken_shubetsu = tmp[c++].ToString().Trim();             //保険種別;
                    ahk.honkenyugai = tmp[c++].ToString().Trim();                //本家入外;
                    ahk.daisaansha = tmp[c++].ToString().Trim();                 //業務上・外、第三者行為の有無;
                    ahk.seikyukbn = tmp[c++].ToString().Trim();                  //請求区分;
                    ahk.firstdate1 = tmp[c++].ToString().Trim();                 //初療年月日;
                    ahk.startdate1 = tmp[c++].ToString().Trim();                 //施術期間自;
                    ahk.finishdate1 = tmp[c++].ToString().Trim();                //施術期間至;
                    ahk.counteddays = int.Parse(tmp[c++].ToString().Trim());     //実日数;
                    ahk.f1name = tmp[c++].ToString().Trim();                     //傷病名(コード);
                    ahk.shokenryokbn = tmp[c++].ToString().Trim();               //初検料区分;                    
                    ahk.shokenryo = tmp[c++].ToString().Trim();                  //初検料;

                    //20210713154437 furukawa st ////////////////////////
                    //往療0は削除しないと後の判定sqlが煩雑になる

                    string until4km = tmp[c++].ToString().Trim();
                    string over4km = tmp[c++].ToString().Trim();
                    ahk.until4km = until4km == "0" ? string.Empty : until4km;                  //往療料回数4kmまで;                    
                    ahk.over4km = over4km == "0" ? string.Empty : over4km;                    //往療料回数4km超;
                    //ahk.until4km = tmp[c++].ToString().Trim();                   //往療料回数4kmまで;
                    //ahk.over4km = tmp[c++].ToString().Trim();                    //往療料回数4km超;
                    //20210713154437 furukawa ed ////////////////////////

                    ahk.total = int.Parse(tmp[c++].ToString().Trim());           //合計額;
                    ahk.partial = int.Parse(tmp[c++].ToString().Trim());         //一部負担金額;
                    ahk.charge = int.Parse(tmp[c++].ToString().Trim());          //請求額;
                    ahk.sejutsum = int.Parse(tmp[c++].ToString().Trim());        //施術日月;
                    ahk.sejutsud = tmp[c++].ToString().Trim();                   //施術日日;
                    ahk.sejutsuddisp = tmp[c++].ToString().Trim();               //施術日表示;
                    ahk.sejutsu_shomei_ymd = tmp[c++].ToString().Trim();         //施術証明年月日;
                    ahk.hokenshokbn = tmp[c++].ToString().Trim();                //保健所登録区分;
                    ahk.shinseiymd = tmp[c++].ToString().Trim();                 //申請年月日;
                    ahk.accnumber = tmp[c++].ToString().Trim();                  //支払口座番号;
                    ahk.douiymd = tmp[c++].ToString().Trim();                    //同意年月日;
                    ahk.ininymd = tmp[c++].ToString().Trim();                    //委任年月日;
                    ahk.detailnum1 = tmp[c++].ToString().Trim();                 //明細番号;
                    ahk.yobi01 = tmp[c++].ToString().Trim();                     //予備01;
                    ahk.yobi02 = tmp[c++].ToString().Trim();                     //予備02;
                    ahk.yobi03 = tmp[c++].ToString().Trim();                     //予備03;
                    ahk.yobi04 = tmp[c++].ToString().Trim();                     //予備04;
                    ahk.yobi05 = tmp[c++].ToString().Trim();                     //予備05;
                    ahk.yobi06 = tmp[c++].ToString().Trim();                     //予備06;
                    ahk.yobi07 = tmp[c++].ToString().Trim();                     //予備07;
                    ahk.yobi08 = tmp[c++].ToString().Trim();                     //予備08;
                    ahk.yobi09 = tmp[c++].ToString().Trim();                     //予備09;
                    ahk.yobi10 = tmp[c++].ToString().Trim();                     //予備10;
                    ahk.biko = tmp[c++].ToString().Trim();                       //備考;

                    ahk.pbirthdayad = DateTimeEx.GetDateFromJstr7(ahk.pbirthday);                                   //生年月日西暦;
                    ahk.sejutsuymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(ahk.sejutsuym)).ToString();      //施術年月西暦yyyymm;
                    ahk.shinsaymad = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(ahk.shinsaym)).ToString();        //審査年月西暦yyyymm;
                    ahk.firstdate1ad = DateTimeEx.GetDateFromJstr7(ahk.firstdate1);                                 //初療年月日（初検日）西暦;
                    ahk.startdate1ad = DateTimeEx.GetDateFromJstr7(ahk.startdate1);                                 //施術期間自西暦;
                    ahk.finishdate1ad = DateTimeEx.GetDateFromJstr7(ahk.finishdate1);                               //施術期間至西暦;
                    ahk.sejutsu_shomei_ymdad = DateTimeEx.GetDateFromJstr7(ahk.sejutsu_shomei_ymd);                 //施術証明年月日西暦;
                    ahk.shinseiymdad = DateTimeEx.GetDateFromJstr7(ahk.shinseiymd);                                 //申請年月日西暦;
                    ahk.douiymdad = DateTimeEx.GetDateFromJstr7(ahk.douiymd);                                       //同意年月日西暦;
                    ahk.ininymdad = DateTimeEx.GetDateFromJstr7(ahk.ininymd);                                       //委任年月日西暦;

                    ahk.hihonum_nr = Microsoft.VisualBasic.Strings.StrConv(ahk.hnum, Microsoft.VisualBasic.VbStrConv.Narrow);//被保険者証番号半角



                    if (!DB.Main.Insert(ahk, tran)) tran.Rollback();
                    wf.InvokeValue++;
                }

                tran.Commit();                
                return true;
            }
            catch(Exception ex)
            {
                tran.Rollback();
                System.Windows.Forms.MessageBox.Show(
                    $"{System.Reflection.MethodBase.GetCurrentMethod().Name}列番号{c}\r\n{ex.Message}");
                return false;
            }
            finally
            {
                
            }
            
        }

        private static bool Merge(WaitForm wf,int cym)
        {

            //20211221152457 furukawa st ////////////////////////
            //遅いのでストアドMergewith_ahkに移行

            #region 旧コード
            ////csvの明細番号２とファイル名(application.emptytext3)で紐づける
            ////application.emptytext3はappクラスから取得できないのでsqlでやる

            //System.Text.StringBuilder sbsql = new StringBuilder();

            //sbsql.AppendLine("update application set ");
            //sbsql.AppendLine(" psex = d.pgender ");
            //sbsql.AppendLine(",pbirthday = cast(d.pbirthdayad as date) ");
            //sbsql.AppendLine(",pname = d.pname");

            //sbsql.AppendLine(",hnum = d.hihonum_nr");//半角の方が都合よさそう
            //                                         //sbsql.AppendLine(",hnum = d.hnum");

            ////20201113061944 furukawa st ////////////////////////
            ////ayearは放置しないと続紙かどうか判別がつかなくなる

            ////sbsql.AppendLine(",ayear = cast(substring(d.sejutsuym,2,2) as int) ");
            ////20201113061944 furukawa ed ////////////////////////

            //sbsql.AppendLine(",amonth = cast(substring(d.sejutsuym,4,2) as int)");
            //sbsql.AppendLine(",ym = cast(d.ym as int)");

            //sbsql.AppendLine(",sid = d.clinicnum");
            //sbsql.AppendLine(",sregnumber = d.drnum");
            //sbsql.AppendLine(",inum = d.insnum");


            ////20210702110150 furukawa st ////////////////////////
            ////電算管理番号=ファイル名が納品データに必要になったのでcomnumに

            //sbsql.AppendLine(",comnum = d.detailnum2 ");
            ////20210702110150 furukawa ed ////////////////////////




            ////家族区分 0.高一 8.高
            //sbsql.AppendLine(", afamily = ");
            //sbsql.AppendLine("case d.honkenyugai ");
            //sbsql.AppendLine(" when  '00' then  200 ");
            //sbsql.AppendLine(" when  '08' then  208 ");
            //sbsql.AppendLine(" end ");

            ////給付割合　８で高一８と高一９があり判断できないので入力を優先させる
            ////sbsql.AppendLine(",aratio = ");
            ////sbsql.AppendLine("case d.honkenyugai ");
            ////sbsql.AppendLine(" when '00' then 8 ");
            ////sbsql.AppendLine(" when '08' then 9 ");
            ////sbsql.AppendLine(" end");

            //sbsql.AppendLine(",fchargetype = cast(d.seikyukbn as int)");

            ////日付は西暦のやつ
            //sbsql.AppendLine(",ifirstdate1 = d.firstdate1ad");
            //sbsql.AppendLine(",istartdate1 = d.startdate1ad");
            //sbsql.AppendLine(",ifinishdate1 = d.finishdate1ad");

            //sbsql.AppendLine(",acounteddays = d.counteddays");


            ////20211220115044 furukawa st ////////////////////////
            ////傷病名が既に入っている場合は入れない

            //sbsql.AppendLine(",iname1 = case when iname1 is null then d.f1name ");
            //sbsql.AppendLine("               when trim(iname1)='' then d.f1name ");
            //sbsql.AppendLine("               else iname1 ");
            //sbsql.AppendLine(" end ");
            ////      sbsql.AppendLine(",iname1 = d.f1name");
            ////20211220115044 furukawa ed ////////////////////////


            ////20211019142525 furukawa st ////////////////////////
            ////往療は距離でなく回数なのでマージしない


            ////          20210510103925 furukawa st ////////////////////////
            ////          往療加算がある場合は、提供データのuntil4kmが0でも入れないと、加算だけしか無いことになる

            ////          提供データに加算しか値がない場合も、往療料には999を入れてフラグを上げる

            ////          4キロ未満に値がない＋4キロ超に値がある＝4キロ未満に999、4キロ超に999
            ////          4キロ超に値がある場合は999を入れる
            ////          無い場合0
            ////          4キロ未満に値がある場合は999
            ////          無い場合0




            ////                20200911172506 furukawa st ////////////////////////
            ////                距離も入れないと入力画面のフラグが建てれない

            ////                sbsql.AppendLine(",fdistance = case when d.until4km<>'' then cast(d.until4km as int) else 0 end ");



            ////                           20201113161654 furukawa st ////////////////////////
            ////                           往療料、往療料追加分には値を入れない。提供データ側は回数だった

            ////                           sbsql.AppendLine(",fvisitfee = case when d.until4km<>'' then cast(d.until4km as int) else 0 end ");
            ////                           sbsql.AppendLine(",fvisitadd = case when d.over4km<>'' then cast(d.over4km as int) else 0 end ");
            ////                           20201113161654 furukawa ed ////////////////////////

            ////                20200911172506 furukawa ed ////////////////////////



            ////          sbsql.AppendLine(",  fdistance= ");
            ////          sbsql.AppendLine("  case ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) = '' and trim(d.over4km) =''  then 0 ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) = '' and trim(d.over4km) <>'' then 999 ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) <>'' and trim(d.over4km) <>'' then 999 ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) <>'' and trim(d.over4km) =''  then 999 ");
            ////          sbsql.AppendLine("  	else 0 ");                          
            ////          sbsql.AppendLine("  end ");

            ////          sbsql.AppendLine(",  fvisitadd= ");
            ////          sbsql.AppendLine("  case ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) = '' and trim(d.over4km) =''  then 0 ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) = '' and trim(d.over4km) <>'' then 999 ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) <>'' and trim(d.over4km) <>'' then 999 ");
            ////          sbsql.AppendLine("  	when trim(d.until4km) <>'' and trim(d.over4km) =''  then 0");
            ////          sbsql.AppendLine("  	else 0 ");
            ////          sbsql.AppendLine("  end  ");



            ////          20210510103925 furukawa ed ////////////////////////


            ////20211019142525 furukawa ed ////////////////////////

            //sbsql.AppendLine(",atotal = d.total");
            //sbsql.AppendLine(",apartial = d.partial");
            //sbsql.AppendLine(",acharge = d.charge");
            //sbsql.AppendLine(",baccnumber = d.accnumber ");


            ////同意日どうする？→sqlでは面倒なので画面入力にしてもらう
            ////やっぱり画面自動入力のためにはここで入れないと
            //sbsql.AppendLine(",taggeddatas= ");


            ////1項目ずつ有無を調べて無かったら追加

            ////空の場合は｜を入れない
            //sbsql.AppendLine("case when taggeddatas <>'' then ");
            //sbsql.AppendLine("taggeddatas || '|'");
            //sbsql.AppendLine("else '' end || ");            

            //sbsql.AppendLine("case when taggeddatas !~'DouiDate:\"[0-9]{4}.[0-9]{2}.[0-9]{2}\"' then ");
            //sbsql.AppendLine("'DouiDate:\"' || d.douiymdad || '\"'");
            //sbsql.AppendLine("else '' end || ");

            //sbsql.AppendLine("case when taggeddatas !~'flgSejutuDouiUmu:\"true|false\"' then ");
            //sbsql.AppendLine("'|flgSejutuDouiUmu:\"' || case when d.douiymdad ='-infinity' then false else true end || '\"'");
            //sbsql.AppendLine("else '' end || ");

            //sbsql.AppendLine("case when taggeddatas !~'GeneralString1:\"データ取得済\"' then ");
            //sbsql.AppendLine("'|GeneralString1:\"データ取得済\"'");
            //sbsql.AppendLine("else '' end  ");





            ////sbsql.AppendLine("case when taggeddatas <>'' then taggeddatas '|'");
            ////sbsql.AppendLine("else end || ");

            ////sbsql.AppendLine("'DouiDate:\"' || d.douiymdad || '\"'");
            ////sbsql.AppendLine("'|flgSejutuDouiUmu:\"' || case when d.douiymdad ='-infinity' then false else true end || '\"'");
            ////sbsql.AppendLine("'|GeneralString1:\"データ取得済\"'");



            //sbsql.AppendLine("from tdata_ahk d ");
            //sbsql.AppendLine("where ");
            //sbsql.AppendLine("d.detailnum2 = replace(application.emptytext3,'.tif','') ");

            ////20201116184937 furukawa st ////////////////////////
            ////申請書限定で更新する。申請書はemptytext2=0

            //sbsql.AppendLine("and application.emptytext2='0' ");
            //sbsql.AppendLine("and application.cym=d.cym ");
            ////20201116184937 furukawa ed ////////////////////////


            #endregion

            //20211221152457 furukawa ed ////////////////////////


            System.Text.StringBuilder sbsql = new StringBuilder();
            sbsql.AppendLine($"select mergewith_ahk({cym})");

            DB.Transaction tran = new DB.Transaction(DB.Main);

            //20211221152555 furukawa st ////////////////////////
            //あまりに長いときタイムアウトさせるようにした
            
            DB.Command cmd = DB.Main.CreateCmd(sbsql.ToString(),tran);
            //  DB.Command cmd = DB.Main.CreateLongTimeoutCmd(sbsql.ToString());
            //20211221152555 furukawa ed ////////////////////////


            try
            {
                wf.LogPrint($"更新開始");
                cmd.TryExecuteNonQuery();
                wf.LogPrint($"更新終了");
                System.Windows.Forms.MessageBox.Show("更新完了");
                return true;
            }
            catch(Exception ex)
            {
                
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                tran.Rollback();
                return false;

            }
            finally
            {
                tran.Commit();
                cmd.Dispose();
                
            }
            

        }
    }
}
