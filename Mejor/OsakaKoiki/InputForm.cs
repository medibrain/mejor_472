﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.OsakaKoiki
{

    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp;
        private BindingSource bsRefReceData;
        private InputMode inputMode;
        protected override Control inputPanel => panelRight;
        int cym;

      

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 0);
        Point posHnum = new Point(800, 0);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1100);


        //20200908170529 furukawa st ////////////////////////
        //あはき用合計金額、初検日位置
        
        Point posTotalAHK = new Point(350, 900);
        Point posNewAHK = new Point(350, 500);
        //20200908170529 furukawa ed ////////////////////////




        //20190505111235 furukawa st ////////////////////////
        //posTotalの座標を下げた
        Point posTotal = new Point(800, 2200);
        //Point posTotal = new Point(800, 1800);
        //20190505111235 furukawa ed ////////////////////////

        Point posBui = new Point(80, 750);

        APP_TYPE current_apptype;


        //20200220100605 furukawa st ////////////////////////
        //オートマッチング＋data.mdbインポート状態の場合のフラグ。スコープをクラス全体にし、App引数を取らなくていいようにする
        
        /// <summary>
        /// オートマッチング＋data.mdbインポート状態の場合True
        /// </summary>
        bool flgAutoMatchDataImport = false;
        //20200220100605 furukawa ed ////////////////////////





        public InputForm(InputMode mode, ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode == InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            panelInfo.Visible = true;
            panelMatchWhere.Visible = false;
            panelMatchCheckInfo.Visible = false;

            this.scanGroup = sGroup;
            var list = App.GetAppsGID(scanGroup.GroupID);

            //柔整200dpi
            if (string.IsNullOrWhiteSpace(scanGroup.note2))
            {
                posYM = new Point(80, 0);
                posHnum = new Point(800, 0);
                posNew = new Point(800, 500);
                posVisit = new Point(350, 500);
                posTotal = new Point(800, 1250);
                posBui = new Point(80, 500);
            }

            //フラグ順にソート
            list.Sort((x, y) =>
                x.InputOrderNumber == y.InputOrderNumber ?
                x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            
            //あんまの場合、部位入力部分を表示
            bool buiInput = scanGroup.AppType == APP_TYPE.あんま;
            labelBuiCount.Visible = buiInput;
            verifyBoxBuiCount.Visible = buiInput;
            labelBui.Visible = buiInput;
            verifyCheckBoxBody.Visible = buiInput;
            verifyCheckBoxRightUpper.Visible = buiInput;
            verifyCheckBoxLeftUpper.Visible = buiInput;
            verifyCheckBoxRightLower.Visible = buiInput;
            verifyCheckBoxLeftLower.Visible = buiInput;



            //Appリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //20220623103829 furukawa st ////////////////////////
            //外部業者には個人情報グリッドを見せない
            if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者)
            {
                dataGridRefRece.Visible = false;
                //20220716112426 furukawa st ////////////////////////
                //外部業者の場合グリッドにフォーカスしない
                
                dataGridRefRece.TabStop = false;
                //20220716112426 furukawa ed ////////////////////////
            }
            //20220623103829 furukawa ed ////////////////////////


            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
        }




        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //表示調整
            initialize();

            //20220623103957 furukawa st ////////////////////////
            //外部業者には個人情報グリッドを見せない
            if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者) dataGridRefRece.Visible = false;
            //20220623103957 furukawa ed ////////////////////////


            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            
            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }




        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }

        private void initialize()
        {
            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            //広域データ欄
            bsRefReceData = new BindingSource();
            bsRefReceData.DataSource = new List<RefRece>();
            dataGridRefRece.DataSource = bsRefReceData;

            foreach (DataGridViewColumn c in dataGridRefRece.Columns) c.Visible = false;
            dataGridRefRece.Columns[nameof(RefRece.RrID)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.RrID)].Width = 60;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].Width = 40;
            dataGridRefRece.Columns[nameof(RefRece.MYM)].HeaderText = "年月";
            dataGridRefRece.Columns[nameof(RefRece.Num)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Num)].Width = 70;
            dataGridRefRece.Columns[nameof(RefRece.Num)].HeaderText = "被保番";
            dataGridRefRece.Columns[nameof(RefRece.StartDate)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.StartDate)].Width = 95;
            dataGridRefRece.Columns[nameof(RefRece.StartDate)].HeaderText = "開始日";
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].Width = 80;
            dataGridRefRece.Columns[nameof(RefRece.ClinicNum)].HeaderText = "施術師番号";
            dataGridRefRece.Columns[nameof(RefRece.DrName)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.DrName)].Width = 100;
            dataGridRefRece.Columns[nameof(RefRece.DrName)].HeaderText = "代表者";
            dataGridRefRece.Columns[nameof(RefRece.ClinicName)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.ClinicName)].Width = 100;
            dataGridRefRece.Columns[nameof(RefRece.ClinicName)].HeaderText = "施術所";
            dataGridRefRece.Columns[nameof(RefRece.Total)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.Total)].Width = 50;
            dataGridRefRece.Columns[nameof(RefRece.Total)].HeaderText = "合計";
            dataGridRefRece.Columns[nameof(RefRece.AID)].Visible = true;
            dataGridRefRece.Columns[nameof(RefRece.AID)].Width = 60;

            //pgupで選択行が変わるのを防ぐ
            dataGridRefRece.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;

            //20190505110452 furukawa st ////////////////////////
            //Enter時イベント関連づけ
            eventhandle();
            //20190505110452 furukawa ed ////////////////////////


        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;

            if (app == null) clearApp();
            else setApp(app);
        }

        #region 20190505110542 furukawa Enter時イベント関連づけ

        private void eventhandle()
        {
            vcSejutu.Enter += verifyBox_Enter;
            vbSejutuY.Enter += verifyBox_Enter;
            vbSejutuM.Enter += verifyBox_Enter;

            vbDouiG.Enter += verifyBox_Enter;
            vbDouiY.Enter += verifyBox_Enter;
            vbDouiM.Enter += verifyBox_Enter;
            vbDouiD.Enter += verifyBox_Enter;


        }
        #endregion



        /// <summary>
        /// フォーム表示後
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void inputForm_Shown(object sender, EventArgs e)
        {
            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }

                //20200909121309 furukawa st ////////////////////////
                //柔整あはきとも200dpiになったので倍率統一
                
                scrollPictureControl1.Ratio = 0.6f;


                //if (string.IsNullOrWhiteSpace(scanGroup.note2)) scrollPictureControl1.Ratio = 0.6f;
                //else scrollPictureControl1.Ratio = 0.4f;
                //20200909121309 furukawa ed ////////////////////////
            }


            //20200220101158 furukawa st ////////////////////////
            //初回表示時、フォーカス制御が適用されないので追加

            //自動マッチング済の場合、フォーカスを初検年にする
            if (flgAutoMatchDataImport)
            {
                //20200911131142 furukawa st ////////////////////////
                //鍼灸あんまの場合は施術年にフォーカス
                
                if (current_apptype == APP_TYPE.鍼灸 || current_apptype == APP_TYPE.あんま) verifyBoxY.Focus();
                else
                {
                    //20200911131142 furukawa ed ////////////////////////

                    if (verifyBoxF1FirstY.Enabled) verifyBoxF1FirstY.Focus();
                    else verifyCheckBoxVisit.Focus();
                }

            }
            //自動マッチング済以外の場合はフォーカスを施術年
            else verifyBoxY.Focus();

            //20200220101158 furukawa ed ////////////////////////


        }


        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //登録ボタン
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }
        //ショートカット
        private void inputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {

            //20190313184720 furukawa st ////////////////////////
            //再入力用チェック外し解除

            if (dataChanged && !updateDbApp()) return;


            //20190228201936 furukawa st ////////////////////////
            //再入力のため更新チェックを一旦外す
            //if (dataChanged && !updateDbApp()) return;
            //if (!updateDbApp()) return;
            //20190228201936 furukawa ed ////////////////////////


            //20190313184720 furukawa ed ////////////////////////

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectKoikiData(app);
        }


        private void selectKoikiData(App app)
        {
            bsRefReceData.Clear();

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsRefReceData.ResetBindings(false);
                return;
            }

            List<RefRece> l;
            if (inputMode == InputMode.MatchCheck)
            {
                //マッチチェック時
                var num = verifyBoxHnum.Text.Trim();
                l = RefRece.SerchForMatching(cym, num, total);
                if (checkBoxNumber.Checked) l = l.FindAll(a => a.Num == num);
                if (checkBoxTotal.Checked) l = l.FindAll(a => a.Total == total);
            }
            else
            {
                //入力,ベリファイ時
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                int cym = DateTimeEx.GetAdYearFromHs(scanGroup.cyear * 100 + scanGroup.cmonth) * 100 + scanGroup.cmonth;
                int ym = DateTimeEx.GetAdYearFromHs(y * 100 + m) * 100 + m;

                //int cym = DateTimeEx.GetAdYearFromHs(scanGroup.cyear) * 100 + scanGroup.cmonth;
                //int ym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;

                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                l = RefRece.SerchByInput(cym, ym, verifyBoxHnum.Text.Trim(), total);
            }

            if (l != null) bsRefReceData.DataSource = l;
            bsRefReceData.ResetBindings(false);

            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridRefRece.RowCount; i++)
                {
                    int aid = (int)dataGridRefRece[nameof(RefRece.AID), i].Value;
                    var rrid = (int)dataGridRefRece[nameof(RefRece.RrID), i].Value;

                    if (app.RrID == rrid && app.Aid == aid)
                    {
                        //既に一意の広域データとマッチング済みの場合
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;

                        //20190528105642 furukawa st ////////////////////////
                        //グリッドの現在のセルが不可視の場合選択しない（例外発生する）
                        if ( dataGridRefRece[0, i].Visible) dataGridRefRece.CurrentCell = dataGridRefRece[0, i];

                            //dataGridRefRece.CurrentCell = dataGridRefRece[0, i];
                        //20190528105642 furukawa ed ////////////////////////
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridRefRece.CurrentCell = null;
            }
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;
            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);

            //年
            int year = verifyBoxY.GetIntValue();
            ////setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);


            //20200116100324 furukawa st ////////////////////////
            //空欄と０未満をエラーとする            

            setStatus(verifyBoxY, year < 0 || app.ChargeYear.ToString() == string.Empty);
            //setStatus(verifyBoxY, false);  //イレギュラー年対応用（18/4/3久保）

            //20200116100324 furukawa ed ////////////////////////

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 3文字以上かつ数字に直せること
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length < 2 ||
                !long.TryParse(verifyBoxHnum.Text, out long hnumTemp));

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100);

            

            //初検年月

            //20200205101856 furukawa st ////////////////////////
            //初検日を入力するのは柔整、鍼灸なのでチェックも同様にする

            if (current_apptype != APP_TYPE.あんま)
            {
                //20200116100427 furukawa st ////////////////////////
                //.netのエラーメッセージがそのまま出ていたのでチェックする

                int intfjy = verifyBoxF1FirstY.GetIntValue();
                setStatus(verifyBoxF1FirstY, intfjy.ToString() == string.Empty || intfjy < 0);
                int intfm = verifyBoxF1FirstM.GetIntValue();
                setStatus(verifyBoxF1FirstM, intfm.ToString() == string.Empty || intfm < 0);

                //ここまでのチェックで必須エラーが検出されたらfalse
                if (hasError)
                {
                    showInputErrorMessage();
                    return false;
                }
                //20200116100427 furukawa ed ////////////////////////

            }
            //20200205101856 furukawa ed ////////////////////////




            int fjy = verifyBoxF1FirstY.GetIntValue();
            int fm = verifyBoxF1FirstM.GetIntValue();


            //20190826172413 furukawa st ////////////////////////
            //初検日入力はあんま以外
            int fy = 0;//初検年西暦


            if (appsg.AppType != APP_TYPE.あんま)
            {

                //20190816114617 furukawa st ////////////////////////
                //月がないためGetAdYearMonthFromJyymmに変更

                //int fy = DateTimeEx.GetAdYearFromEraYear(4, fjy);



                //20200218161848 furukawa st ////////////////////////
                //初検年が－９（入ってない）場合はfy設定せずエラーにする
                
                if (fjy >= 0)
                {
                    fy = DateTimeEx.GetAdYearFromHs(int.Parse(fjy.ToString("00") + fm.ToString("00")));
                }
                    //fy = DateTimeEx.GetAdYearFromHs(int.Parse(fjy.ToString("00") + fm.ToString("00")));

                //20200218161848 furukawa ed ////////////////////////


                //20190816114617 furukawa ed ////////////////////////



                var firstDtError = !DateTimeEx.IsDate(fy, fm, 1);

           
                setStatus(verifyBoxF1FirstY, firstDtError);
                setStatus(verifyBoxF1FirstM, firstDtError);
            }

            //20190826172413 furukawa ed ////////////////////////



            //負傷名チェック

            //20220526094811 furukawa st ////////////////////////
            //2022年4月からあんま負傷名入れる

            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //      20211227092813 furukawa st ////////////////////////
            //      2021年度中はあんまの負傷名は入力しない。2022年4月から戻す予定
            //      if (appsg.note2 != "8")
            //      {
                      //fusho1Check(verifyBoxF1);
                      //fushoCheck(verifyBoxF2);
                      //fushoCheck(verifyBoxF3);
                      //fushoCheck(verifyBoxF4);
                      //fushoCheck(verifyBoxF5);
            //      }
            //      20211227092813 furukawa ed ////////////////////////

            //20220526094811 furukawa ed ////////////////////////


            //20190312165217 furukawa st ////////////////////////
            //追加入力項目チェック


            if (vbDouiG.Text.Length + vbDouiY.Text.Length+ vbDouiM.Text.Length + vbDouiD.Text.Length > 0)
            {
                //同意年月日
                setStatus(vbDouiG, (vbDouiG.Text.Length >= 1 && !int.TryParse(vbDouiY.Text, out int tmpG)) || vbDouiG.Text==string.Empty);
                setStatus(vbDouiY, (vbDouiY.Text.Length >= 1 && !int.TryParse(vbDouiY.Text, out int tmpY)) || vbDouiY.Text==string.Empty);
                setStatus(vbDouiM, (vbDouiM.Text.Length >= 1 && !int.TryParse(vbDouiM.Text, out int tmpM)) || vbDouiM.Text==string.Empty);
                setStatus(vbDouiD, (vbDouiD.Text.Length >= 1 && !int.TryParse(vbDouiD.Text, out int tmpD)) || vbDouiD.Text == string.Empty);

            }

            ////同意年月日
            //setStatus(vbDouiY, vbDouiY.Text.Length >= 1 && !int.TryParse(vbDouiY.Text, out int tmpY));
            //setStatus(vbDouiM, vbDouiM.Text.Length >= 1 && !int.TryParse(vbDouiM.Text, out int tmpM));
            //setStatus(vbDouiD, vbDouiD.Text.Length >= 1 && !int.TryParse(vbDouiD.Text, out int tmpD));




            //施術報告書交付料　有無　
            if (vcSejutu.Checked)
            {
                if (vbSejutuY.Text.Length + vbSejutuM.Text.Length > 0)
                {
                    //前回支給年月
                    setStatus(vbSejutuY, (vbSejutuY.Text.Length >= 1 && !int.TryParse(vbSejutuY.Text, out int tmpSY)) || vbSejutuY.Text == string.Empty);
                    setStatus(vbSejutuM, (vbSejutuM.Text.Length >= 1 && !int.TryParse(vbSejutuM.Text, out int tmpSM)) || vbSejutuM.Text==string.Empty);
                    //setStatus(vbSejutuM, vbSejutuM.Text.Length >= 1 && !int.TryParse(vbSejutuM.Text, out int tmpSM));
                }
            }
            //20190312165217 furukawa ed ////////////////////////



            //往療料
            int kihonOryo = 0;
            int kasanOryo = 0;
            double inputDistance = 0;

            double.TryParse(verifyBoxKasanKm.Text, out inputDistance);
            int.TryParse(verifyBoxOryoCost.Text, out kihonOryo);
            int.TryParse(verifyBoxKasanCost.Text, out kasanOryo);

            //20220606114239 furukawa st ////////////////////////
            //距離入力は不要になった2022/5/6瀧井さん
            

            if (verifyCheckBoxVisitKasan.Checked)
            {
                
                    // 加算金額より計算
                    if (!string.IsNullOrEmpty(verifyBoxOryoCost.Text) && kihonOryo == 0)
                    {
                        setStatus(verifyBoxOryoCost, true);
                    }

                    if (!string.IsNullOrEmpty(verifyBoxKasanCost.Text) && kasanOryo == 0)
                    {
                        setStatus(verifyBoxKasanCost, true);
                    }
               
                    // 加算距離算出
                    if (!hasError && inputDistance == 0)
                    {
                        int visittimes = kihonOryo / 1800;
                        int incrementKasan = (visittimes > 0) ? kasanOryo / visittimes / 800 : 0;
                        inputDistance = incrementKasan * 2;
                    }
                
            }

            //if (verifyCheckBoxVisitKasan.Checked)
            //{
            //    if (!string.IsNullOrWhiteSpace(verifyBoxKasanKm.Text) && inputDistance == 0)
            //    {
            //        setStatus(verifyBoxKasanKm, true);
            //    }
            //    else
            //    {
            //        // 加算金額より計算
            //        if (!string.IsNullOrEmpty(verifyBoxOryoCost.Text) && kihonOryo == 0)
            //        {
            //            setStatus(verifyBoxOryoCost, true);
            //        }

            //        if (!string.IsNullOrEmpty(verifyBoxKasanCost.Text) && kasanOryo == 0)
            //        {
            //            setStatus(verifyBoxKasanCost, true);
            //        }

            //        // 加算にチェックがあり、距離・金額ともに入力がない場合
            //        if (inputDistance == 0 && (kihonOryo == 0 || kasanOryo == 0))
            //        {
            //            setStatus(verifyBoxOryoCost, true);
            //            setStatus(verifyBoxKasanCost, true);
            //        }

            //        // 加算距離算出
            //        if (!hasError && inputDistance == 0)
            //        {
            //            int visittimes = kihonOryo / 1800;
            //            int incrementKasan = (visittimes > 0) ? kasanOryo / visittimes / 800 : 0;
            //            inputDistance = incrementKasan * 2;
            //        }
            //    }
            //}


            //20220606114239 furukawa ed ////////////////////////

            //部位 ※あんまのみ
            int bui = 0;
            if (appsg.note2 == "8")
            {
                int.TryParse(verifyBoxBuiCount.Text, out int buiCount);

                int cc = 0;
                cc += verifyCheckBoxBody.Checked ? 1 : 0;
                cc += verifyCheckBoxRightUpper.Checked ? 1 : 0;
                cc += verifyCheckBoxLeftUpper.Checked ? 1 : 0;
                cc += verifyCheckBoxRightLower.Checked ? 1 : 0;
                cc += verifyCheckBoxLeftLower.Checked ? 1 : 0;

                int buiCode = 0;
                buiCode += verifyCheckBoxBody.Checked ? 10000 : 0;
                buiCode += verifyCheckBoxRightUpper.Checked ? 1000 : 0;
                buiCode += verifyCheckBoxLeftUpper.Checked ? 100 : 0;
                buiCode += verifyCheckBoxRightLower.Checked ? 10 : 0;
                buiCode += verifyCheckBoxLeftLower.Checked ? 1 : 0;

                if (cc == 0 && buiCount == 0)
                {
                    //両方に入力なし
                    hasError = true;
                    verifyBoxBuiCount.BackColor = Color.Pink;
                    verifyCheckBoxBody.BackColor = Color.Pink;
                    verifyCheckBoxRightUpper.BackColor = Color.Pink;
                    verifyCheckBoxLeftUpper.BackColor = Color.Pink;
                    verifyCheckBoxRightLower.BackColor = Color.Pink;
                    verifyCheckBoxLeftLower.BackColor = Color.Pink;
                }
                else if (cc == 0)
                {
                    //チェックボックス入力なし
                    bui = buiCount * 100000;
                }
                else if (buiCount == 0)
                {
                    //部位数入力なし
                    bui += verifyCheckBoxBody.Checked ? 10000 : 0;
                    bui += verifyCheckBoxRightUpper.Checked ? 1000 : 0;
                    bui += verifyCheckBoxLeftUpper.Checked ? 100 : 0;
                    bui += verifyCheckBoxRightLower.Checked ? 10 : 0;
                    bui += verifyCheckBoxLeftLower.Checked ? 1 : 0;
                }
                else
                {
                    //両方入力あり
                    if (cc != buiCount)
                    {
                        //チェックボックスと一致しない
                        hasError = true;
                        verifyBoxBuiCount.BackColor = Color.Pink;
                        verifyCheckBoxBody.BackColor = Color.Pink;
                        verifyCheckBoxRightUpper.BackColor = Color.Pink;
                        verifyCheckBoxLeftUpper.BackColor = Color.Pink;
                        verifyCheckBoxRightLower.BackColor = Color.Pink;
                        verifyCheckBoxLeftLower.BackColor = Color.Pink;
                    }
                    else
                    {
                        //部位情報を記録
                        bui += verifyCheckBoxBody.Checked ? 10000 : 0;
                        bui += verifyCheckBoxRightUpper.Checked ? 1000 : 0;
                        bui += verifyCheckBoxLeftUpper.Checked ? 100 : 0;
                        bui += verifyCheckBoxRightLower.Checked ? 10 : 0;
                        bui += verifyCheckBoxLeftLower.Checked ? 1 : 0;
                    }
                }

                if (bui == 11111) bui = 511111;
            }

            //ここまでのチェックで必須エラーが検出されたらfalse
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //診療年月の警告
            int ms = (app.ChargeYear - year) * 12 + app.ChargeMonth - month;
            if (ms > 30)
            {
                if (MessageBox.Show("診療年月が古いですが、このまま登録してよろしいですか？",
                    "診療年月確認",　MessageBoxButtons.OKCancel, MessageBoxIcon.Question,
                     MessageBoxDefaultButton.Button2) == DialogResult.OK)
                {
                    verifyBoxY.BackColor = SystemColors.Info;
                    verifyBoxM.BackColor = SystemColors.Info;
                }
                else
                {
                    verifyBoxY.BackColor = Color.Pink;
                    verifyBoxM.BackColor = Color.Pink;
                    return false;
                }
            }
            
            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text;
            app.Total = total;

            //往療
            app.Distance = verifyCheckBoxVisit.Checked ? 999 : 0;

            //20220606113813 furukawa st ////////////////////////
            //距離入力は不要になったので距離無関係でチェック有無だけで判断            
            app.VisitAdd = verifyCheckBoxVisitKasan.Checked ? 999 : 0;
            // app.VisitAdd = Convert.ToInt32(inputDistance * 1000);
            //20220606113813 furukawa ed ////////////////////////


            app.VisitFee = kihonOryo;
            app.VisitTimes = kasanOryo;

            //初検年月
            app.FushoFirstDate1 = appsg.AppType == APP_TYPE.あんま ?
                DateTimeEx.DateTimeNull : new DateTime(fy, fm, 1);

            //新規・継続
            app.NewContType =
                appsg.AppType == APP_TYPE.あんま ? NEW_CONT.継続 :
                fjy == year && fm == month ? NEW_CONT.新規 : NEW_CONT.継続;

            //申請書種別
            app.AppType = appsg.note2 == "7" ? APP_TYPE.鍼灸 :
                appsg.note2 == "8" ? APP_TYPE.あんま : APP_TYPE.柔整;

            //部位
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            app.Bui = bui;



            //20190312165328 furukawa st ////////////////////////
            //追加入力項目　DB登録
            //app.TaggedDatas.flgSejutuDouiUmu = vcSejutu.Checked; //20190412102941 furukawa 施術同意書有無フラグ追加

            app.TaggedDatas.flgKofuUmu = vcSejutu.Checked;

            //20190412125704 furukawa st ////////////////////////
            //施術同意書チェックボックスがTrueの場合は値を入れる。ない場合は空文字にする
            if (vcSejutu.Checked)
            {
                app.TaggedDatas.PastSupplyYM =
                    DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(vbSejutuG.Text + vbSejutuY.Text.PadLeft(2, '0') + vbSejutuM.Text.PadLeft(2, '0'))).ToString();
            }
            else
            {            
                app.TaggedDatas.PastSupplyYM = string.Empty;
            }
            //20190412125704 furukawa ed ////////////////////////
            
            app.TaggedDatas.DouiDate = 
                DateTimeEx.GetDateFromJstr7(vbDouiG.Text  + vbDouiY.Text.PadLeft(2, '0') + vbDouiM.Text.PadLeft(2, '0') + vbDouiD.Text.PadLeft(2, '0'));

            //20190312165328 furukawa ed ////////////////////////






            //広域データからのデータコピー
            //20220716121839 furukawa st ////////////////////////
            //提供データ行は1行のみとする
            RefRece rr = null;
            if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者)
            {
                rr= (RefRece)dataGridRefRece.Rows[0].DataBoundItem;
            }
            else if (User.checkUserBelong(User.CurrentUser.UserID) != User.UserBelong.外部業者)
            {
                rr = (RefRece)dataGridRefRece.CurrentRow.DataBoundItem;
            }
                
            //var rr = (RefRece)dataGridRefRece.CurrentRow.DataBoundItem;
            //20220716121839 furukawa ed ////////////////////////


            app.PersonName = rr.Name;
            app.FushoStartDate1 = rr.StartDate;
            app.Numbering = rr.SearchNum;
            app.ClinicName = rr.ClinicName;
            app.ClinicNum = rr.ClinicNum;
            app.DrNum = rr.DrNum;
            app.CountedDays = rr.Days;
            app.DrName = rr.DrName;
            app.RrID = rr.RrID;

            app.TaggedDatas.Kana = rr.Kana;
            app.HihoZip = rr.Zip;
            app.HihoAdd = rr.Add;
            app.Birthday = rr.Birthday;
            app.TaggedDatas.DestName = rr.DestName;
            app.TaggedDatas.DestKana = rr.DestKana;
            app.TaggedDatas.DestZip = rr.DestZip;
            app.TaggedDatas.DestAdd = rr.DestAdd;
            app.Family = rr.Family;
            app.Ratio = rr.Ratio;
            app.GroupNum = rr.GroupCode;
            app.InsNum = rr.InsNum;

            //事前点検分は処理2
            if (rr.Advance)app.StatusFlagSet(StatusFlag.処理2);
            else app.StatusFlagRemove(StatusFlag.処理2);

            // マージ有分は処理3
            if (rr.Merge) app.StatusFlagSet(StatusFlag.処理3);
            else app.StatusFlagRemove(StatusFlag.処理3);

            return true;
        }


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;




            //20190312140344 furukawa st ////////////////////////


            #region 旧コード

            /*
            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
                app.StatusFlagRemove(StatusFlag.処理2);
            }
            else if (verifyBoxY.Text == "..")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.長期;
                app.AppType = APP_TYPE.長期;
                app.StatusFlagRemove(StatusFlag.処理2);
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                app.StatusFlagRemove(StatusFlag.処理2);
            }
            else if (verifyBoxY.Text == "**")
            {
                //エラー
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                app.AppType = APP_TYPE.エラー;
                app.StatusFlagRemove(StatusFlag.処理2);
            }

            else
            {   

                //申請書の場合
                if (dataGridRefRece.CurrentCell == null)
                {
                    MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                        "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                        "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
                
                int oldDid;
                int.TryParse(app.Numbering, out oldDid);
                var oldDis = app.Distance;

                if (!checkApp(app))
                {
                    focusBack();
                    return false;
                }
            }*/


            #endregion

            //続紙分類の追加
            switch (verifyBoxY.Text)
            {
                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();//20190421125338 furukawa 一応続紙の親を控えておく

                    break;

                case clsInputKind.長期:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.長期;
                    app.AppType = APP_TYPE.長期;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();//20190421125338 furukawa 一応続紙の親を控えておく
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();//20190421125338 furukawa 一応続紙の親を控えておく
                    break;

                case clsInputKind.エラー:
                    //エラー


                    //20211112133806 furukawa st ////////////////////////
                    //エラー登録させない
                    
                    MessageBox.Show("エラーでは登録できません", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                    //20211112133806 furukawa ed ////////////////////////


                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                    app.AppType = APP_TYPE.エラー;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();//20190421125338 furukawa 一応続紙の親を控えておく
                    break;
                



                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    if (!checkAppFor901(app))
                    {
                        focusBack();
                        return false;
                    }

                    break;


                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    break;

                case clsInputKind.状態記入書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    app.StatusFlagRemove(StatusFlag.処理2);
                    app.TaggedDatas.RelationAID = GetAID();
                    break;

                default:
                    //申請書の場合

                    //20220716121602 furukawa st ////////////////////////
                    //外部業者でマッチ行が1以外の場合エラーとする
                    if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者)
                    {
                        if (dataGridRefRece.Rows.Count != 1)
                        {
                            MessageBox.Show("マッチングデータが適切ではありません。登録できません。",
                                "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return false;
                        }
                    }
                    else
                    {
                    //20220716121602 furukawa ed ////////////////////////

                        if (dataGridRefRece.CurrentCell == null)
                        {
                            //20211112133628 furukawa st ////////////////////////
                            //マッチングデータがない場合エラー登録とせず、登録させない

                            MessageBox.Show("マッチングデータがありません。入力内容を確認してください",
                                "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //              MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                            //                  "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                            //                  "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //20211112133628 furukawa ed ////////////////////////
                            return false;
                        }
                    }
                      
                    int oldDid;
                    int.TryParse(app.Numbering, out oldDid);
                    var oldDis = app.Distance;

                    if (!checkApp(app))
                    {
                        focusBack();
                        return false;
                    }

                    break;
            }

            //20190312140344 furukawa ed ////////////////////////



            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //入力ログ
                //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換                
                if (app.Ufirst == 0 && !InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now-dtstart_core, jyuTran))
                    return false;

                //データ記録
                app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput, tran);

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                //広域データ更新

                //20220716121958 furukawa st ////////////////////////
                //外部業者の場合、提供データ行は1行目のみとする 
                RefRece rr = null;
                if (User.checkUserBelong(User.CurrentUser.UserID) == User.UserBelong.外部業者)
                {
                    //20220721193919 furukawa st ////////////////////////
                    //提供グリッドが0の場合エラー回避
                    
                    if (dataGridRefRece.RowCount > 0)
                    {
                        //20220721193919 furukawa ed ////////////////////////


                        rr = (RefRece)dataGridRefRece.Rows[0]?.DataBoundItem;
                    }
                }
                else
                {
                    rr = (RefRece)dataGridRefRece.CurrentRow?.DataBoundItem;
                }
                //      var rr = (RefRece)dataGridRefRece.CurrentRow?.DataBoundItem;
                //20220716121958 furukawa ed ////////////////////////
                
                
                if (app.RrID == 0)
                {
                    RefRece.AidDelete(app.Aid, tran);
                }
                else
                {
                    if (rr.AID != app.Aid) RefRece.AidUpdate(rr.RrID, app.Aid, tran);
                }

                //往療料データ更新
                //var oldDis = app.Distance;

                //if (rr != null && (rr.AID != app.Aid || oldDis != app.Distance))
                //    Kyori.CheckDistance(app.Aid, app.DrNum, (double)app.VisitAdd / 1000d);

                tran.Commit();
                jyuTran.Commit();
            }

            return true;
        }



        //20190312152922 furukawa st ////////////////////////
        /// <summary>
        /// 続紙に紐付く申請書のAID取得
        /// </summary>
        /// <returns></returns>
        private int GetAID()
        {
            //20190426185809 furukawa st ////////////////////////
            //タイムアウト調査のため一旦外す

            return 0;
            
            //20190426185809 furukawa ed ////////////////////////

            var app = (App)bsApp.Current;
            StringBuilder sb = new StringBuilder();


            //20190407150904 furukawa st ////////////////////////
            //aidはスキャングループが違うと20以上の差になるようなので、最も近いaidを取得に変更

            //自分のAID-1から自分AID-20の間で、AppTypeが0超（申請書だけ）を拾う
            //2019-03-13　申請書の最大枚数は20と想定（伸作さん確認済）
            //sb.AppendFormat("select max(aid) from application where (aid between {0} and {1}) and aapptype>=6",
            // app.Aid-20,app.Aid-1);

            sb.AppendFormat("select max(aid) from application where aid < {0} and aapptype>=6",
                 app.Aid);
            //20190407150904 furukawa ed ////////////////////////


            var cmd = DB.Main.CreateCmd(sb.ToString());
            string strAID=cmd.TryExecuteScalar().ToString();

            
            if (strAID == string.Empty) return 0;
            return int.Parse(strAID);
                        
        }


        private void clearApp()
        {
            //画像クリア
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            //情報クリア
            var clearTB = new Action<TextBox>((tb) =>
            {
                tb.Text = "";
                tb.BackColor = SystemColors.Info;
            });
            
            Action<Control> act = null;
            act = new Action<Control>(c =>
            {
                if (c == panelMatchWhere) return;
                if (c is TextBox)
                {
                    c.Text = "";
                    c.BackColor = SystemColors.Info;
                }
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
                foreach (Control item in c.Controls) act(item);
            });
            act(panelRight);

            bsRefReceData.Clear();
            bsRefReceData.ResetBindings(false);
        }

        #region 20190407162315 furukawa st 施術同意書入力チェック
        /// <summary>
        /// 施術同意書入力チェック
        /// </summary>
        /// <param name="app">appデータ</param>
        /// <returns></returns>
        private bool checkAppFor901(App app)
        {
            //20190509131355 furukawa st ////////////////////////
            //同意年月日入力チェック

            hasError = false;

            //20190510094302 furukawa st ////////////////////////
            //同意年月日はすべて入ってOK

            if (vbDouiY.Text == string.Empty || vbDouiD.Text == string.Empty || vbDouiM.Text == string.Empty) hasError=true;
            //20190510094302 furukawa ed ////////////////////////

            setStatus(vbDouiY,  !int.TryParse(vbDouiY.Text, out int tmpY) || vbDouiY.Text.Length <=0);
            setStatus(vbDouiM,  !int.TryParse(vbDouiM.Text, out int tmpM) || vbDouiM.Text.Length <= 0);
            setStatus(vbDouiD,  !int.TryParse(vbDouiD.Text, out int tmpD) || vbDouiD.Text.Length <= 0);
            //ここまでのチェックで必須エラーが検出されたらfalse
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
            //20190509131355 furukawa ed ////////////////////////




            //20190816173654 furukawa st ////////////////////////
            //同意年月日令和対応
            
            int adYM = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(vbDouiG.Text + vbDouiY.Text.PadLeft(2, '0') + vbDouiM.Text.PadLeft(2, '0')));
            int eraNo=DateTimeEx.GetEraNumberYearFromYYYYMM(adYM);
            app.TaggedDatas.DouiDate = new DateTime(adYM / 100, tmpM, tmpD);



                        //DateTime tmpdt = DateTime.Parse(vbDouiY.Text + '/' + vbDouiM.Text + '/' + vbDouiD.Text);
                        //string nengo = DateTimeEx.GetEraJpYear(tmpdt);
                        //app.TaggedDatas.DouiDate = DateTimeEx.GetDateFromJstr7("4" + vbDouiY.Text.PadLeft(2, '0') + vbDouiM.Text.PadLeft(2, '0') + vbDouiD.Text.PadLeft(2,'0'));
            //20190816173654 furukawa ed ////////////////////////



            return true;

        }
        #endregion



        //20200914183021 furukawa st ////////////////////////
        //コントロール有効化制御
        
        private void Sensitive(App app)
        {

            //施術月　被保険者証番号　合計金額
            pTotal.Enabled = false;
           

            //初検日初期化
            pF1.Enabled = false;
           

            //施術同意日
            //pDoui.Enabled = false;
            //pDoui.ForeColor = Color.LightGray;



            //初検日有効化
            pF1.Enabled = true;
        
            
            flgAutoMatchDataImport =
              app.StatusFlagCheck(StatusFlag.自動マッチ済) || app.TaggedDatas.GeneralString1.Contains("データ取得済");

            //自動マッチング済の場合
            if (flgAutoMatchDataImport)
            {
                //あんま、はりの場合は施術年(続紙判定のため入力してもらう）
                if (current_apptype == APP_TYPE.あんま || current_apptype == APP_TYPE.鍼灸)
                {
                    verifyBoxY.Focus();
                }

                //その他
                else
                {
                    if (verifyBoxF1FirstY.Enabled) verifyBoxF1FirstY.Focus();
                    else verifyCheckBoxVisit.Focus();
                }
            }

            //自動マッチングできない場合は手入力のためコントロール有効化
            else
            {
                //施術月　被保険者証番号　合計金額
                pTotal.Enabled = true;

                verifyBoxY.Focus();
            }

        }
        //20200914183021 furukawa ed ////////////////////////



        /// <summary>
        /// Appを表示します
        /// </summary>
        /// <param name="r"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //全クリア
            iVerifiableAllClear(panelRight);

            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);

            //あはきの場合は常に継続
            //2018年度より入力
            //verifyBoxNewCont.Enabled = appsg.AppType == APP_TYPE.柔整;
            //verifyBoxNewCont.Text = "2";

            //あんまの場合、部位入力部分を表示
            bool buiInput = appsg.AppType == APP_TYPE.あんま;

             

            labelBuiCount.Visible = buiInput;
            verifyBoxBuiCount.Visible = buiInput;
            labelBui.Visible = buiInput;
            verifyCheckBoxBody.Visible = buiInput;
            verifyCheckBoxRightUpper.Visible = buiInput;
            verifyCheckBoxLeftUpper.Visible = buiInput;
            verifyCheckBoxRightLower.Visible = buiInput;
            verifyCheckBoxLeftLower.Visible = buiInput;

            buttonReInput.Visible = true;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //OCR 入力者情報をリセット
            labelStatusOCR.Text = string.Empty;
            labelInputUser.Text = string.Empty;

           //20190517164645 furukawa st ////////////////////////
            //APPTYPE取得
            
            current_apptype = appsg.AppType;
            //20190517164645 furukawa ed ////////////////////////


            if (app == null) return;

            //20200914183103 furukawa st ////////////////////////
            //長いので関数にした
            
            Sensitive(app);

            #region old
            //verifyBoxM.Enabled = false;
            //verifyBoxHnum.Enabled = false;
            //verifyBoxTotal.Enabled = false;
            //verifyBoxF1FirstY.Enabled = false;
            //verifyBoxF1FirstM.Enabled = false;
            //labelM.Enabled = false;
            //labelHnum.Enabled = false;
            //labelTotal.Enabled = false;
            //labelF1First.Enabled = false;
            //labelF1FirstY.Enabled = false;
            //labelF1FirstM.Enabled = false;
            //pDoui.Enabled = false;

            //verifyBoxM.ForeColor = Color.LightGray;
            //verifyBoxHnum.ForeColor = Color.LightGray;
            //verifyBoxTotal.ForeColor = Color.LightGray;
            //verifyBoxF1FirstY.ForeColor = Color.LightGray;
            //verifyBoxF1FirstM.ForeColor = Color.LightGray;
            //labelM.ForeColor = Color.LightGray;
            //labelHnum.ForeColor = Color.LightGray;
            //labelTotal.ForeColor = Color.LightGray;
            //labelF1First.ForeColor = Color.LightGray;
            //labelF1FirstY.ForeColor = Color.LightGray;
            //labelF1FirstM.ForeColor = Color.LightGray;
            //pDoui.ForeColor = Color.LightGray;

            ////20200220100939 furukawa st ////////////////////////
            ////オートマッチング＋data.mdbインポート＆マッチ状態フラグ判定


            ////オートマッチング＋data.mdbインポート＆マッチ状態フラグ判定
            //flgAutoMatchDataImport = 
            //    app.StatusFlagCheck(StatusFlag.自動マッチ済) || app.TaggedDatas.GeneralString1.Contains("データ取得済");
            ////20200220100939 furukawa ed ////////////////////////


            ////20200220092950 furukawa st ////////////////////////
            ////付随情報data.mdb取得済みの場合、施術年がすでに入っているはずなので、初検年にフォーカスする

            ////自動マッチング済の場合、フォーカスを初検年にする
            //if (flgAutoMatchDataImport)      
            //    //if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            ////20200220092950 furukawa ed ////////////////////////

            //{
            //    //20200911131335 furukawa st ////////////////////////
            //    //鍼灸あんまの場合は施術年にフォーカス

            //    if (current_apptype == APP_TYPE.あんま || current_apptype == APP_TYPE.鍼灸)
            //    {
            //        verifyBoxY.Focus();
            //    }
            //    else if (current_apptype == APP_TYPE.柔整)
            //    {
            //        verifyBoxF1FirstY.Enabled = true;
            //        verifyBoxF1FirstM.Enabled = true;
            //        verifyBoxF1FirstY.ForeColor = Color.Black;
            //        verifyBoxF1FirstM.ForeColor = Color.Black;
            //        labelF1First.ForeColor = Color.Black;
            //        labelF1FirstY.ForeColor = Color.Black;
            //        labelF1FirstM.ForeColor = Color.Black;

            //        if (verifyBoxF1FirstY.Enabled) verifyBoxF1FirstY.Focus();
            //        else verifyCheckBoxVisit.Focus();
            //    }
            //    else
            //    {
            //        //20200911131335 furukawa ed ////////////////////////


            //        //verifyBoxF1FirstY.Enabled = true;
            //        //verifyBoxF1FirstM.Enabled = true;
            //        //verifyBoxF1FirstY.ForeColor = Color.Black;
            //        //verifyBoxF1FirstM.ForeColor = Color.Black;

            //        if (verifyBoxF1FirstY.Enabled) verifyBoxF1FirstY.Focus();
            //        else verifyCheckBoxVisit.Focus();
            //    }
            //}
            ////自動マッチング済以外の場合はフォーカスを施術年
            //else verifyBoxY.Focus();
            #endregion

            //20200914183103 furukawa ed ////////////////////////




            //App_Flagのチェック
            //App_Flagの状態によって表示項目を変更
            labelAppStatus.Text = app.InputStatus.ToString();
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
                labelAppStatus.BackColor = Color.Cyan;
            }

            //20200218162052 furukawa st ////////////////////////
            //data.mdb等の付随情報を取り込んだフラグ代わりの文字列があれば、入力欄にデータ表示    
            else if (flgAutoMatchDataImport)
                        //else if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            //20200218162052 furukawa ed ////////////////////////
            {
                //マッチング有のデータについては一部広域からのデータを表示
                setNoInputAppWithOcr(app);
                labelAppStatus.BackColor = Color.Yellow;
            }           
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
                labelAppStatus.BackColor = Color.Red;
            }

            //あはきの場合は常に継続
            //if (appsg.AppType != APP_TYPE.柔整) verifyBoxNewCont.Text = "2";

            //広域データ表示
            selectKoikiData(app);


            //20190314113048 furukawa st ////////////////////////
            //追加項目使用可不可

            //20190517164828 furukawa st ////////////////////////
            //APPTYPEによってコントロール使用可否            
            bool flgEnable = current_apptype != APP_TYPE.柔整;
            //bool flgEnable = scanGroup.AppType != APP_TYPE.柔整;
            //20190517164828 furukawa ed ////////////////////////

            //pDoui.Enabled = flgEnable;
            //pSejutu.Enabled = flgEnable;
            //vcSejutu.Enabled = flgEnable;

            //20190314113048 furukawa ed ////////////////////////


            //全選択
            if (ActiveControl is TextBox) ((TextBox)ActiveControl).SelectAll();

            changedReset(app);

        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                //20221020_2 ito st /////
                //ZenkokuGakko\InputFormRev.cs内　//20220323180452 furukawa　を流用した。たぶん全保険者に摘要すべき。
                //ファイルが見つからない場合はクリアする
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
                //20221020_2 ito end /////

                MessageBox.Show("画像表示でエラーが発生しました");
            }
        }


        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            labelInputUser.Text = "入力：";

            //OCRデータが存在する場合
            if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                try
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    if (string.IsNullOrEmpty(appsg.note2))
                    {
                        var ocr = app.OcrData.Split(',');
                        //OCRデータがあれば、部位のみ挿入
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                            verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                            verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                            verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                            verifyBoxF5.Text = Fusho.GetFusho5(ocr);

                        //新規/継続
                        //2018.5.12 南さんよりOCR結果デフォルト表示を無効化
                        //if (ocr.Length > 127)
                        //{
                        //    if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                        //    if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                        //}
                    }

                    labelStatusOCR.Text = "OCR有り";
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    //OCR情報をリセット
                    labelStatusOCR.Text = "OCRエラー";
                }
            }
        }

        /// <summary>
        /// マッチング有りの場合：広域データとOCRデータの両方から入力欄にフィルします
        /// </summary>
        private void setNoInputAppWithOcr(App app)
        {
            labelInputUser.Text = "入力者：";
            
            try
            {

                //20200907141429 furukawa st ////////////////////////
                //続紙があるのであはきは処理年月を手入力して貰う
                
                switch (current_apptype)
                {
                    case APP_TYPE.柔整:
                        verifyBoxY.Text = app.MediYear.ToString();
                        verifyBoxM.Text = app.MediMonth.ToString();
                        verifyBoxHnum.Text = app.HihoNum;
                        verifyBoxTotal.Text = app.Total.ToString();
                        break;

                    case APP_TYPE.鍼灸:
                    case APP_TYPE.あんま:
                        
                        verifyBoxY.Text = app.MediYear.ToString();
                        verifyBoxM.Text = app.MediMonth.ToString();
                        verifyBoxHnum.Text = app.HihoNum;
                        verifyBoxTotal.Text = app.Total.ToString();


                        setDateValue(app.FushoFirstDate1, true, false, verifyBoxF1FirstY, verifyBoxF1FirstM);
                        verifyCheckBoxVisit.Checked = app.Distance == 0 ? false : true;
                        verifyCheckBoxVisitKasan.Checked = app.VisitAdd == 0 ? false : true;
                        setDateValue(app.TaggedDatas.DouiDate, true, false, vbDouiY, vbDouiM, vbDouiG, vbDouiD);
                        setDateValue(DateTimeEx.ToDateTime6(app.TaggedDatas.PastSupplyYM), true, false, vbSejutuY, vbSejutuM, vbSejutuG);
                        
                        break;
                }


                //verifyBoxY.Text = app.MediYear.ToString();
                //verifyBoxM.Text = app.MediMonth.ToString();
                //verifyBoxHnum.Text = app.HihoNum;
                //verifyBoxTotal.Text = app.Total.ToString();

                //20200907141429 furukawa ed ////////////////////////


                var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                if (string.IsNullOrEmpty(appsg.note2))
                {
                    var ocr = app.OcrData.Split(',');
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }
                }

                labelStatusOCR.Text = "OCRデータ有";
            }
            catch (Exception ex)
            {
                labelStatusOCR.Text = "OCRエラー";
                Log.ErrorWrite(ex);
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            labelInputUser.Text = $"入力者：{User.GetUserName(app.Ufirst)}";

            //20190315173146 furukawa st ////////////////////////
            //続紙入力種類値の代入

            #region 旧コード
            /*
            if (app.MediYear == -3)
            {
                //続紙
                verifyBoxY.Text = "--";
            }
            else if (app.AppType == APP_TYPE.長期)
            {
                //長期
                verifyBoxY.Text = "..";
            }
            else if (app.MediYear == -4)
            {
                //白バッジ、その他
                verifyBoxY.Text = "++";
            }
            else if(app.MediYear == -999)
            {
                //エラー
                verifyBoxY.Text = "**";
            }


            */
            #endregion




            //20190507155313 furukawa st ////////////////////////
            //aYearにAPP_TYPEを入れていたが、間違いだったのでSpecialCodeに修正




            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                //続紙
                verifyBoxY.Text = clsInputKind.続紙;
            }
            else if (app.AppType == APP_TYPE.長期)
            {
                //長期
                verifyBoxY.Text = clsInputKind.長期;
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                //白バッジ、その他
                verifyBoxY.Text = clsInputKind.不要;
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                //エラー
                verifyBoxY.Text = clsInputKind.エラー;
            }


            //同意書、報告書の類もやる

            else if (app.MediYear == (int)APP_SPECIAL_CODE.施術報告書)
            {
                verifyBoxY.Text = clsInputKind.施術報告書;
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.状態記入書)
            {
                verifyBoxY.Text = clsInputKind.状態記入書;
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.同意書裏)
            {
                verifyBoxY.Text = clsInputKind.施術同意書裏;
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.同意書)
            {
                verifyBoxY.Text = clsInputKind.施術同意書;
                string DouiYMD = app.TaggedDatas.DouiDate.ToString();

                if ((DouiYMD != string.Empty) && (DateTime.Parse(DouiYMD) != DateTime.MinValue))
                {

                    DateTime dt;
                    DateTime.TryParse(DouiYMD, out dt);
                    vbDouiG.Text = DateTimeEx.GetEraNumber(dt).ToString();

                    vbDouiY.Text = DateTimeEx.GetJpYear(dt).ToString();
                    vbDouiM.Text = dt.Month.ToString();
                    vbDouiD.Text = dt.Day.ToString();
                }

            }

            /*
                else if (app.MediYear == (int)APP_TYPE.施術報告書)
            {
                verifyBoxY.Text = clsInputKind.施術報告書;
            }
            else if (app.MediYear == (int)APP_TYPE.状態記入書)
            {
                verifyBoxY.Text = clsInputKind.状態記入書;
            }
            else if (app.MediYear == (int)APP_TYPE.同意書裏)
            {
                verifyBoxY.Text = clsInputKind.施術同意書裏;
            }
             

            else if (app.MediYear == (int)APP_TYPE.同意書)
            {
                verifyBoxY.Text = clsInputKind.施術同意書;
                string DouiYMD = app.TaggedDatas.DouiDate.ToString();

                if ((DouiYMD != string.Empty) && (DateTime.Parse(DouiYMD) != DateTime.MinValue))
                {

                    DateTime dt;
                    DateTime.TryParse(DouiYMD, out dt);
                    vbDouiG.Text = DateTimeEx.GetEraNumber(dt).ToString();

                    vbDouiY.Text = DateTimeEx.GetJpYear(dt).ToString();
                    vbDouiM.Text = dt.Month.ToString();
                    vbDouiD.Text = dt.Day.ToString();
                }

            }
             */

            //20190507155313 furukawa ed ////////////////////////






            //20190315173146 furukawa ed ////////////////////////


            else
            {
                //申請書
                //申請書年月
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();

                //被保険者番号
                verifyBoxHnum.Text = app.HihoNum;

                //合計金額
                verifyBoxTotal.Text = app.Total.ToString();

                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;


                //20190312171530 furukawa st ////////////////////////
                //追加入力項目　DBから取得


                string DouiYMD = app.TaggedDatas.DouiDate.ToString();
                
                if ((DouiYMD != string.Empty) && (DateTime.Parse(DouiYMD)!= DateTime.MinValue))
                {

                    DateTime dt = DateTime.Parse(DouiYMD);

                    //20190410165635 furukawa st ////////////////////////
                    //施術同意年月日のセット
                    int jpYear = DateTimeEx.GetJpYearFromYM(int.Parse(dt.Year.ToString("0000") + dt.Month.ToString("00")));                    
                    vbDouiY.Text = jpYear.ToString();

                    //DateTimeEx.TryGetDateTimeFromJdate(DouiYMD, out dt);
                    //vbDouiY.Text =  dt.Year.ToString();
                    //20190410165635 furukawa ed ////////////////////////                    

                    //20190412155357 furukawa st ////////////////////////
                    //年号の番号を入れる
                    vbDouiG.Text = DateTimeEx.GetEraNumber(dt).ToString();
                    //20190412155357 furukawa ed ////////////////////////

                    vbDouiM.Text = dt.Month.ToString("00");
                    vbDouiD.Text = dt.Day.ToString("00");
                }


                string PastYM = app.TaggedDatas.PastSupplyYM;



                //20190425212325 furukawa st ////////////////////////
                //交付料フラグ＆日付なしでも登録

                vcSejutu.Checked = app.TaggedDatas.flgKofuUmu;
                //20190425212325 furukawa ed ////////////////////////



                //20190412095446 furukawa st ////////////////////////
                //前回日が空白または０のときは入れない
                if (PastYM != string.Empty && PastYM!="0")
                //if (PastYM != string.Empty)
                //20190412095446 furukawa ed ////////////////////////

                {
                    //20190410165807 furukawa st ////////////////////////
                    //前回日のセット

                    int jpYearPast = DateTimeEx.GetJpYearFromYM(int.Parse(PastYM));
                    vbSejutuY.Text = jpYearPast.ToString("00");
                    vbSejutuM.Text = PastYM.ToString().Substring(4, 2);


                    //20190425212224 furukawa st ////////////////////////
                    //交付料フラグ＆日付なしでも登録のため外に出した

                    //vcSejutu.Checked = vbSejutuY.Text != string.Empty && vbSejutuM.Text != string.Empty;
                    //20190425212224 furukawa ed ////////////////////////




                    //string dt=DateTimeEx.GetAdYearMonthFromJyymm(int.Parse("4" + PastYM)).ToString();
                    //vbSejutuY.Text = dt.Substring(2, 2);
                    //vbSejutuM.Text = dt.Substring(3, 2);

                    //20190410165807 furukawa ed ////////////////////////


                    //20190412155039 furukawa st ////////////////////////
                    //年号の番号を入れる
                    DateTime tmpdt = new DateTime(int.Parse(PastYM.ToString().Substring(0, 4)),
                                                    int.Parse(PastYM.ToString().Substring(4, 2)),
                                                    1);

                    vbSejutuG.Text = DateTimeEx.GetEraNumber(tmpdt).ToString();
                    //20190412155039 furukawa ed ////////////////////////

                }

                //20190312171530 furukawa ed ////////////////////////


                //初検年月
                if (!app.FushoFirstDate1.IsNullDate())
                {
                    verifyBoxF1FirstY.Text = DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString();
                    verifyBoxF1FirstM.Text = app.FushoFirstDate1.Month.ToString(); 
                }

                //往療
                //20200911172546 furukawa st ////////////////////////
                //往療の値は提供データから取り込むので999にならない
                
                verifyCheckBoxVisit.Checked = app.Distance >0;
                //verifyCheckBoxVisit.Checked = app.Distance == 999;
                //20200911172546 furukawa ed ////////////////////////

                if (app.VisitAdd != 0) verifyBoxKasanKm.Text = (app.VisitAdd / 1000m).ToString();
                if (app.VisitFee != 0) verifyBoxOryoCost.Text = app.VisitFee.ToString(); 
                if (app.VisitTimes != 0) verifyBoxKasanCost.Text = app.VisitTimes.ToString();

                //20220606113947 furukawa st ////////////////////////
                //距離入力は不要になったので距離無関係でチェック有無だけで判断                
                verifyCheckBoxVisitKasan.Checked = app.VisitAdd == 999 ? true : false;
                //verifyCheckBoxVisitKasan.Checked =
                //    0 != app.VisitAdd + app.VisitFee + app.VisitTimes;
                //20220606113947 furukawa ed ////////////////////////


                //あんま時部位
                var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                if (appsg.note2 == "8")
                {
                    if (app.Bui >= 100000) verifyBoxBuiCount.Text = (app.Bui / 100000).ToString();
                    verifyCheckBoxBody.Checked = app.Bui / 10000 % 10 == 1;
                    verifyCheckBoxRightUpper.Checked = app.Bui / 1000 % 10 == 1;
                    verifyCheckBoxLeftUpper.Checked = app.Bui / 100 % 10 == 1;
                    verifyCheckBoxRightLower.Checked = app.Bui / 10 % 10 == 1;
                    verifyCheckBoxLeftLower.Checked = app.Bui % 10 == 1;
                }
                    
                //突合情報
                int oldDid = 0;
                int.TryParse(app.Numbering, out oldDid);
            }
        }
        

      

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            var appsg = scanGroup ?? (app == null ? null : ScanGroup.SelectWithScanData(app.GroupID));


            
            var setCanInput = new Action<Control, bool>((t, b) =>
                {
                    if (t is TextBox) ((TextBox)t).ReadOnly = !b;
                    else t.Enabled = b;

                    t.TabStop = b;
                    if (!b)
                        t.BackColor = SystemColors.Menu;
                    else
                        t.BackColor = SystemColors.Info;
                });



            bool special = false;


            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                case clsInputKind.不要:
                case clsInputKind.続紙:
                case clsInputKind.長期:

                case clsInputKind.施術同意書裏:
                case clsInputKind.施術報告書:
                case clsInputKind.状態記入書:
                case clsInputKind.施術同意書:

                    //続紙、白バッジ、その他の場合、入力項目は無い
                    setCanInput(verifyBoxM, false);
                    setCanInput(verifyBoxHnum, false);
                    setCanInput(verifyBoxTotal, false);
                    setCanInput(verifyBoxF1FirstY, false);
                    setCanInput(verifyBoxF1FirstM, false);
                    setCanInput(verifyBoxF1, false);
                    setCanInput(verifyBoxF2, false);
                    setCanInput(verifyBoxF3, false);
                    setCanInput(verifyBoxF4, false);
                    setCanInput(verifyBoxF5, false);
                    verifyBoxF1FirstY.TabStop = false;
                    verifyBoxF1FirstM.TabStop = false;

                    setCanInput(verifyCheckBoxVisit, false);

                    verifyCheckBoxVisitKasan.Enabled = false;
                    verifyBoxKasanKm.Enabled = false;
                    verifyBoxOryoCost.Enabled = false;
                    verifyBoxKasanCost.Enabled = false;



                    //20190312160906 furukawa st ////////////////////////
                    //入力項目追加

                    //全ての種類で一旦falseにする
                    setCanInput(vcSejutu, false);
                    setCanInput(vbSejutuG, false);
                    setCanInput(vbSejutuY, false);
                    setCanInput(vbSejutuM, false);
                    lblSejutu.Enabled = false;
                    lblSejutuY.Enabled = false;
                    lblSejutuM.Enabled = false;

                    setCanInput(vbDouiG, false);
                    setCanInput(vbDouiY, false);
                    setCanInput(vbDouiM, false);
                    setCanInput(vbDouiD, false);
                    lblDoui.Enabled = false;
                    lblDouiY.Enabled = false;
                    lblDouiM.Enabled = false;
                    lblDouiD.Enabled = false;


                    if (verifyBoxY.Text == clsInputKind.施術同意書)
                    {

                        //20190425191431 furukawa st ////////////////////////
                        //続紙他入力項目を有効にする



                        //20190412162804 furukawa st ////////////////////////
                        //５月まで封印


                        setCanInput(vbDouiG, true);
                        setCanInput(vbDouiY, true);
                        setCanInput(vbDouiM, true);
                        setCanInput(vbDouiD, true);
                        lblDoui.Enabled = true;
                        lblDouiY.Enabled = true;
                        lblDouiM.Enabled = true;
                        lblDouiD.Enabled = true;
                        
                        //20190412162804 furukawa ed ////////////////////////

                        //20190425191431 furukawa ed ////////////////////////

                    }
                    //20190312160906 furukawa ed ////////////////////////

                    special = true;

                    break;

                default:

                    //申請書の場合
                    setCanInput(verifyBoxM, true);
                    setCanInput(verifyBoxHnum, true);
                    setCanInput(verifyBoxTotal, true);



                    //20220526095055 furukawa st ////////////////////////
                    //2022年4月から柔整、鍼、あんま全部負傷名入れる
                    

                    setCanInput(verifyBoxF1, true);
                    setCanInput(verifyBoxF2, true);
                    setCanInput(verifyBoxF3, true);
                    setCanInput(verifyBoxF4, true);
                    setCanInput(verifyBoxF5, true);

                    //          20211227100309 furukawa st ////////////////////////
                    //          2021年度中はあんまの負傷名は入力しない。2022年4月から戻す予定


                    //          if (appsg.AppType != APP_TYPE.あんま)
                    //          {//あんま以外は使える
                    //              setCanInput(verifyBoxF1, true);
                    //              setCanInput(verifyBoxF2, true);
                    //              setCanInput(verifyBoxF3, true);
                    //              setCanInput(verifyBoxF4, true);
                    //              setCanInput(verifyBoxF5, true);

                    //          }
                    //          else
                    //          {//あんまは使わせない
                    //              setCanInput(verifyBoxF1, false);
                    //              setCanInput(verifyBoxF2, false);
                    //              setCanInput(verifyBoxF3, false);
                    //              setCanInput(verifyBoxF4, false);
                    //              setCanInput(verifyBoxF5, false);

                    //          }
                    //          20211227100309 furukawa ed ////////////////////////

                    //20220526095055 furukawa ed ////////////////////////


                    //20190312160731 furukawa st ////////////////////////
                    //入力項目追加


                    //20190425205552 furukawa st ////////////////////////
                    //続紙他入力項目を有効にする



                    //20190412162804 furukawa st ////////////////////////
                    //５月まで封印






                    //20190509113421 furukawa st ////////////////////////
                    //柔整の場合は前回支給、同意年月日は不要


                    bool flgEnable = current_apptype == APP_TYPE.柔整 ? false : true;
                    //bool flgEnable = scan.AppType == APP_TYPE.柔整 ? false : true;


                    setCanInput(vcSejutu, flgEnable);
                    setCanInput(vbSejutuG, false);
                    setCanInput(vbSejutuY, false);
                    setCanInput(vbSejutuM, false);
                    lblSejutu.Enabled = false;
                    lblSejutuY.Enabled = false;
                    lblSejutuM.Enabled = false;
                    
                    setCanInput(vbDouiG, flgEnable);
                    setCanInput(vbDouiY, flgEnable);
                    setCanInput(vbDouiM, flgEnable);
                    setCanInput(vbDouiD, flgEnable);
                    lblDoui.Enabled = flgEnable;
                    lblDouiY.Enabled = flgEnable;
                    lblDouiM.Enabled = flgEnable;
                    lblDouiD.Enabled = flgEnable;

                

                    //setCanInput(vcSejutu, true);
                    //setCanInput(vbSejutuG, false);
                    //setCanInput(vbSejutuY, false);
                    //setCanInput(vbSejutuM, false);
                    //lblSejutu.Enabled = false;
                    //lblSejutuY.Enabled = false;
                    //lblSejutuM.Enabled = false;

                    //setCanInput(vbDouiG, true);
                    //setCanInput(vbDouiY, true);
                    //setCanInput(vbDouiM, true);
                    //setCanInput(vbDouiD, true);
                    //lblDoui.Enabled = true;
                    //lblDouiY.Enabled = true;
                    //lblDouiM.Enabled = true;
                    //lblDouiD.Enabled = true;

                    //20190509113421 furukawa ed ////////////////////////





                    //20190412162804 furukawa ed ////////////////////////


                    //20190425205552 furukawa ed ////////////////////////

                    //20190312160731 furukawa ed ////////////////////////

                    //あんまのみ初検入力なし
                    bool firstDateInput = (appsg?.AppType ?? APP_TYPE.NULL) != APP_TYPE.あんま;
                    setCanInput(verifyBoxF1FirstY, firstDateInput);
                    setCanInput(verifyBoxF1FirstM, firstDateInput);
                    verifyBoxF1FirstY.TabStop = firstDateInput;
                    verifyBoxF1FirstM.TabStop = firstDateInput;

                    setCanInput(verifyCheckBoxVisit, true);
                    oryoControlsAdjust();
                    buiTabStopAdjust();

                    if (verifyBoxTotal.Text != string.Empty && verifyBoxHnum.Text != string.Empty && verifyBoxY.Text != string.Empty) selectKoikiData(app);

                    break;
            }



            #region if長いので変更
            ////20190312160957 furukawa st ////////////////////////
            ////続紙種類追加


            //if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || 
            //    verifyBoxY.Text == "**" || verifyBoxY.Text == ".." ||
            //    verifyBoxY.Text == "001" || verifyBoxY.Text == "002" ||
            //    verifyBoxY.Text == "003" || verifyBoxY.Text == "004" 
            //    )
            ///*
            //if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" ||
            //verifyBoxY.Text == "**" || verifyBoxY.Text == "..")*/
            ////20190312160957 furukawa ed ////////////////////////

            //{
            //    //続紙、白バッジ、その他の場合、入力項目は無い
            //    setCanInput(verifyBoxM, false);
            //    setCanInput(verifyBoxHnum, false);
            //    setCanInput(verifyBoxTotal, false);
            //    setCanInput(verifyBoxF1FirstY, false);
            //    setCanInput(verifyBoxF1FirstM, false);
            //    setCanInput(verifyBoxF1, false);
            //    setCanInput(verifyBoxF2, false);
            //    setCanInput(verifyBoxF3, false);
            //    setCanInput(verifyBoxF4, false);
            //    setCanInput(verifyBoxF5, false);
            //    verifyBoxF1FirstY.TabStop = false;
            //    verifyBoxF1FirstM.TabStop = false;

            //    setCanInput(verifyCheckBoxVisit, false);
            //    verifyCheckBoxVisitKasan.Enabled = false;
            //    verifyBoxKasanKm.Enabled = false;
            //    verifyBoxOryoCost.Enabled = false;
            //    verifyBoxKasanCost.Enabled = false;



            //    //20190312160906 furukawa st ////////////////////////
            //    //入力項目追加

            //    setCanInput(vcSejutu, false);
            //    setCanInput(vbSejutuY, false);
            //    setCanInput(vbSejutuM, false);
                
            //    setCanInput(vbDouiY, false);
            //    setCanInput(vbDouiM, false);
            //    setCanInput(vbDouiD, false);
            //    //20190312160906 furukawa ed ////////////////////////



            //    special = true;
            //}

            ////20190407143125 furukawa st ////////////////////////
            ////施術同意書の入力項目制御
            //else if (verifyBoxY.Text == clsInputKind.施術同意書)
            //{
            //    setCanInput(verifyBoxM, false);
            //    setCanInput(verifyBoxHnum, false);
            //    setCanInput(verifyBoxTotal, false);
            //    setCanInput(verifyBoxF1FirstY, false);
            //    setCanInput(verifyBoxF1FirstM, false);
            //    setCanInput(verifyBoxF1, false);
            //    setCanInput(verifyBoxF2, false);
            //    setCanInput(verifyBoxF3, false);
            //    setCanInput(verifyBoxF4, false);
            //    setCanInput(verifyBoxF5, false);
            //    verifyBoxF1FirstY.TabStop = false;
            //    verifyBoxF1FirstM.TabStop = false;

            //    setCanInput(verifyCheckBoxVisit, false);
            //    verifyCheckBoxVisitKasan.Enabled = false;
            //    verifyBoxKasanKm.Enabled = false;
            //    verifyBoxOryoCost.Enabled = false;
            //    verifyBoxKasanCost.Enabled = false;

            //    setCanInput(vcSejutu, false);
            //    setCanInput(vbSejutuY, false);
            //    setCanInput(vbSejutuM, false);

            //    setCanInput(vbDouiY, true);
            //    setCanInput(vbDouiM, true);
            //    setCanInput(vbDouiD, true);
            //    special = true;
            //}
            ////20190407143125 furukawa ed ////////////////////////


            //else
            //{
            //    //申請書の場合
            //    setCanInput(verifyBoxM, true);
            //    setCanInput(verifyBoxHnum, true);
            //    setCanInput(verifyBoxTotal, true);
            //    setCanInput(verifyBoxF1, true);
            //    setCanInput(verifyBoxF2, true);
            //    setCanInput(verifyBoxF3, true);
            //    setCanInput(verifyBoxF4, true);
            //    setCanInput(verifyBoxF5, true);





            //    //20190312160731 furukawa st ////////////////////////
            //    //入力項目追加

            //    setCanInput(vcSejutu, true);
            //    setCanInput(vbSejutuY, true);
            //    setCanInput(vbSejutuM, true);
            //    lblSejutu.Enabled = true;
            //    lblSejutuY.Enabled = true;
            //    lblSejutuM.Enabled = true;
                

                
            //    setCanInput(vbDouiY, true);
            //    setCanInput(vbDouiM, true);
            //    setCanInput(vbDouiD, true);
            //    lblDoui.Enabled = true;
            //    lblDouiY.Enabled = true;
            //    lblDouiM.Enabled = true;
            //    lblDouiD.Enabled = true;
                
            //    //20190312160731 furukawa ed ////////////////////////





            //    //あんまのみ初検入力なし
            //    bool firstDateInput = (appsg?.AppType ?? APP_TYPE.NULL) != APP_TYPE.あんま;
            //    setCanInput(verifyBoxF1FirstY, firstDateInput);
            //    setCanInput(verifyBoxF1FirstM, firstDateInput);
            //    verifyBoxF1FirstY.TabStop = firstDateInput;
            //    verifyBoxF1FirstM.TabStop = firstDateInput;

            //    setCanInput(verifyCheckBoxVisit, true);
            //    oryoControlsAdjust();
            //    buiTabStopAdjust();
            //}
            #endregion



            //あんまの場合、部位入力部分を表示
            bool buiInput = appsg == null ? false : appsg.note2 == "8" && !special;
            labelBuiCount.Enabled = buiInput;
            setCanInput(verifyBoxBuiCount, buiInput);
            labelBui.Enabled = buiInput;
            setCanInput(verifyCheckBoxBody, buiInput);
            setCanInput(verifyCheckBoxRightUpper, buiInput);
            setCanInput(verifyCheckBoxLeftUpper, buiInput);
            setCanInput(verifyCheckBoxRightLower, buiInput);
            setCanInput(verifyCheckBoxLeftLower, buiInput);

         

        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            if (sender is TextBox) ((TextBox)sender).SelectAll();

            Point p;

            //20200908172024 furukawa st ////////////////////////
            //柔整あはき区別したのでコード整頓
            
            switch (current_apptype)
            {
                case APP_TYPE.柔整:
                    if (sender == verifyBoxY) p = posYM;
                    else if (sender == verifyBoxM) p = posYM;
                    else if (sender == verifyBoxHnum) p = posHnum;

                    else if (sender == verifyBoxTotal) p = posTotal;
                    else if (sender == verifyBoxF1FirstY) p = posNew;
                    else if (sender == verifyBoxF1FirstM) p = posNew;

                    else if (sender == verifyBoxF1) p = posBui;
                    else if (sender == verifyBoxF2) p = posBui;
                    else if (sender == verifyBoxF3) p = posBui;
                    else if (sender == verifyBoxF4) p = posBui;
                    else if (sender == verifyBoxF5) p = posBui;
                    else if (sender == verifyBoxBuiCount) p = posBui;
                    else if (sender == verifyCheckBoxBody) p = posBui;
                    else if (sender == verifyCheckBoxRightUpper) p = posBui;
                    else if (sender == verifyCheckBoxLeftUpper) p = posBui;
                    else if (sender == verifyCheckBoxRightLower) p = posBui;
                    else if (sender == verifyCheckBoxLeftLower) p = posBui;
                    
                    else if (sender == verifyCheckBoxVisit) p = posVisit;
                    else if (sender == verifyCheckBoxVisitKasan) p = posVisit;
                    else if (sender == vcSejutu) p = posVisit;
                    else if (sender == vbSejutuY) p = posVisit;
                    else if (sender == vbSejutuM) p = posVisit;
                    
                    else if (sender == vbDouiG) p = posTotal;
                    else if (sender == vbDouiY) p = posTotal;
                    else if (sender == vbDouiM) p = posTotal;
                    else if (sender == vbDouiD) p = posTotal;
                    else return;

                    
                    break;


                default:
                    
                    if (sender == verifyBoxY) p = posYM;
                    else if (sender == verifyBoxM) p = posYM;
                    else if (sender == verifyBoxHnum) p = posHnum;

                    else if (sender == verifyBoxTotal) p = posTotalAHK;
                    else if (sender == verifyBoxF1FirstY) p = posNewAHK;
                    else if (sender == verifyBoxF1FirstM) p = posNewAHK;

                    else if (sender == verifyBoxF1) p = posHnum;
                    else if (sender == verifyBoxF2) p = posHnum;
                    else if (sender == verifyBoxF3) p = posHnum;
                    else if (sender == verifyBoxF4) p = posHnum;
                    else if (sender == verifyBoxF5) p = posHnum;
                    else if (sender == verifyBoxBuiCount) p = posHnum;
                    else if (sender == verifyCheckBoxBody) p = posHnum;
                    else if (sender == verifyCheckBoxRightUpper) p = posHnum;
                    else if (sender == verifyCheckBoxLeftUpper) p = posHnum;
                    else if (sender == verifyCheckBoxRightLower) p = posHnum;
                    else if (sender == verifyCheckBoxLeftLower) p = posHnum;


                    else if (sender == verifyCheckBoxVisit) p = posNewAHK;
                    else if (sender == verifyCheckBoxVisitKasan) p = posNewAHK;
                    else if (sender == vcSejutu) p = posNewAHK;
                    else if (sender == vbSejutuY) p = posNewAHK;
                    else if (sender == vbSejutuM) p = posNewAHK;
                    
                    else if (sender == vbDouiG) p = posTotal;
                    else if (sender == vbDouiY) p = posTotal;
                    else if (sender == vbDouiM) p = posTotal;
                    else if (sender == vbDouiD) p = posTotal;
                    else return;

                    
                    break;
            }
            scrollPictureControl1.ScrollPosition = p;
            return;

            #region 旧コード
            if (sender == verifyBoxY) p = posYM;
            else if (sender == verifyBoxM) p = posYM;
            else if (sender == verifyBoxHnum) p = posHnum;

            //20190510114039 furukawa st ////////////////////////
            //合計金額の画像座標を柔整とあはきにわける。記載されている位置が違う

            //else if (sender == verifyBoxTotal) p = posTotal;

            
            //20190517165107 furukawa st ////////////////////////
            //APPTYPEによって座標変更
            
            else if (sender == verifyBoxTotal && current_apptype == APP_TYPE.柔整) p = posTotal;
            else if (sender == verifyBoxTotal && current_apptype != APP_TYPE.柔整) p = posTotalAHK;
                //else if (sender == verifyBoxTotal && current_apptype != APP_TYPE.柔整) p = posVisit;

            //else if (sender == verifyBoxTotal && this.scanGroup.AppType == APP_TYPE.柔整) p = posTotal;
            //else if (sender == verifyBoxTotal && this.scanGroup.AppType != APP_TYPE.柔整) p = posVisit;
            //20190517165107 furukawa ed ////////////////////////

            //20190510114039 furukawa ed ////////////////////////



            else if (sender == verifyBoxF1FirstY && current_apptype == APP_TYPE.柔整) p = posNew;
            else if (sender == verifyBoxF1FirstM && current_apptype == APP_TYPE.柔整) p = posNew;
            else if (sender == verifyBoxF1FirstY && current_apptype != APP_TYPE.柔整) p = posNewAHK;
            else if (sender == verifyBoxF1FirstM && current_apptype != APP_TYPE.柔整) p = posNewAHK;
            //else if (sender == verifyBoxF1FirstY) p = posNew;
            //else if (sender == verifyBoxF1FirstM) p = posNew;




            else if (sender == verifyBoxF1) p = posBui;
            else if (sender == verifyBoxF2) p = posBui;
            else if (sender == verifyBoxF3) p = posBui;
            else if (sender == verifyBoxF4) p = posBui;
            else if (sender == verifyBoxF5) p = posBui;

            //else if (sender == verifyCheckBoxVisit) p = posVisit;
            else if (sender == verifyCheckBoxVisit && current_apptype == APP_TYPE.柔整) p = posVisit;
            else if (sender == verifyCheckBoxVisit && current_apptype != APP_TYPE.柔整) p = posNewAHK;



            else if (sender == verifyBoxBuiCount) p = posBui;
            else if (sender == verifyCheckBoxBody) p = posBui;
            else if (sender == verifyCheckBoxRightUpper) p = posBui;
            else if (sender == verifyCheckBoxLeftUpper) p = posBui;
            else if (sender == verifyCheckBoxRightLower) p = posBui;
            else if (sender == verifyCheckBoxLeftLower) p = posBui;

            


            //20190312115615 furukawa st ////////////////////////
            //施術同意書コントロール追加


            //20190510114319 furukawa st ////////////////////////
            //前回支給の画像座標を修正

            else if (sender == vcSejutu) p = posVisit;
            else if (sender == vbSejutuY) p = posVisit;
            else if (sender == vbSejutuM) p = posVisit;


            //else if (sender == vcSejutu) p = posBui;
            //else if (sender == vbSejutuY) p = posBui;
            //else if (sender == vbSejutuM) p = posBui;
            //20190510114319 furukawa ed ////////////////////////




            //20190312115615 furukawa ed ////////////////////////


            //20190508153844 furukawa st ////////////////////////
            //同意年月日の座標調整

            else if (sender == vbDouiG) p = posTotal;
            else if (sender == vbDouiY) p = posTotal;
            else if (sender == vbDouiM) p = posTotal;
            else if (sender == vbDouiD) p = posTotal;

            //20190508153844 furukawa ed ////////////////////////


            else return;

            scrollPictureControl1.ScrollPosition = p;
            #endregion

            //20200908172024 furukawa ed ////////////////////////

        }

        private void VcSejutu_Enter(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// データエラーチェック後、画面に復帰する際にエラー箇所にフォーカスを移動
        /// </summary>
        private void focusBack()
        {

            //20190509093312 furukawa st ////////////////////////
            //同意年月日、前回支給追加
            
            var cs = new Control[] { verifyBoxY, verifyBoxM, verifyBoxHnum,verifyBoxTotal,
                verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5,
                verifyBoxF1FirstY,verifyBoxF1FirstM,
                verifyCheckBoxVisit, verifyCheckBoxVisitKasan, verifyBoxKasanKm,
                verifyBoxOryoCost, verifyBoxKasanCost,
            vcSejutu,vbSejutuG,vbSejutuY,vbSejutuM,
            vbDouiG,vbDouiY,vbDouiM,vbDouiD};


            //var cs = new Control[] { verifyBoxY, verifyBoxM, verifyBoxHnum,verifyBoxTotal,
            //    verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5,
            //    verifyBoxF1FirstY,verifyBoxF1FirstM,
            //    verifyCheckBoxVisit, verifyCheckBoxVisitKasan, verifyBoxKasanKm,
            //    verifyBoxOryoCost, verifyBoxKasanCost, };


            //20190509093312 furukawa ed ////////////////////////


            //コントロールは前から順番に並べないと途中で色違いのコントロールがある場合そこで止まる
            foreach (var item in cs)
            {
                //20190510121103 furukawa st ////////////////////////
                //エラー判定を修正

                if ( item.BackColor != SystemColors.Info)
                    //    if (item.Enabled && item.BackColor != SystemColors.Info)
                    //20190510121103 furukawa ed ////////////////////////
                    {
                    item.Focus();
                    return;
                }
            }
            verifyBoxY.Focus();
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;

            if (ActiveControl == verifyBoxY) posYM = pos;
            else if (ActiveControl == verifyBoxM) posYM = pos;
            else if (ActiveControl == verifyBoxHnum) posHnum = pos;
            else if (ActiveControl == verifyBoxF1FirstY) posNew = pos;
            else if (ActiveControl == verifyBoxF1FirstM) posNew = pos;

            //20190510114201 furukawa st ////////////////////////
            //合計金額の画像座標を柔整とあはきにわける。記載されている位置が違う

            //else if (ActiveControl == verifyBoxTotal ) posTotal = pos;

            //20190517170645 furukawa st ////////////////////////
            //APPTYPEによって座標変更
            
            else if (ActiveControl == verifyBoxTotal && current_apptype == APP_TYPE.柔整) posTotal = pos;
            else if (ActiveControl == verifyBoxTotal && current_apptype != APP_TYPE.柔整) posVisit = pos;

                //else if (ActiveControl == verifyBoxTotal && this.scanGroup.AppType == APP_TYPE.柔整) posTotal = pos;
                //else if (ActiveControl == verifyBoxTotal && this.scanGroup.AppType != APP_TYPE.柔整) posVisit = pos;
            //20190517170645 furukawa ed ////////////////////////


            //20190510114201 furukawa ed ////////////////////////

            else if (ActiveControl == verifyBoxF1) posBui = pos;
            else if (ActiveControl == verifyBoxF2) posBui = pos;
            else if (ActiveControl == verifyBoxF3) posBui = pos;
            else if (ActiveControl == verifyBoxF4) posBui = pos;
            else if (ActiveControl == verifyBoxF5) posBui = pos;

            else if (ActiveControl == verifyBoxBuiCount) posBui = pos;
            else if (ActiveControl == verifyCheckBoxBody) posBui = pos;
            else if (ActiveControl == verifyCheckBoxRightUpper) posBui = pos;
            else if (ActiveControl == verifyCheckBoxLeftUpper) posBui = pos;
            else if (ActiveControl == verifyCheckBoxRightLower) posBui = pos;
            else if (ActiveControl == verifyCheckBoxLeftLower) posBui = pos;


            //20190508152817 furukawa st ////////////////////////
            //同意年月日、前回支給の画像座標
            
            else if (ActiveControl == vbDouiG) posTotal = pos;
            else if (ActiveControl == vbDouiY) posTotal = pos;
            else if (ActiveControl == vbDouiM) posTotal = pos;
            else if (ActiveControl == vbDouiD) posTotal = pos;


            //20190510110618 furukawa st ////////////////////////
            //前回支給の画像座標を修正


            else if (ActiveControl == vbSejutuG) posVisit = pos;
            else if (ActiveControl == vbSejutuY) posVisit = pos;
            else if (ActiveControl == vbSejutuM) posVisit = pos;

                    //else if (ActiveControl == vbSejutuG) posTotal = pos;
                    //else if (ActiveControl == vbSejutuY) posTotal = pos;
                    //else if (ActiveControl == vbSejutuM) posTotal = pos;

                    //20190510110618 furukawa ed ////////////////////////

            //20190508152817 furukawa ed ////////////////////////

            //20191212102913 furukawa st ////////////////////////
            //往療有り、交付料ありチェックも画像座標記憶
            
            else if (ActiveControl == verifyCheckBoxVisit) posVisit = pos;
            else if (ActiveControl == vcSejutu) posVisit = pos;
            //20191212102913 furukawa ed ////////////////////////
        }

        private void checkBoxVisitControls_CheckedChanged(object sender, EventArgs e)
        {
            oryoControlsAdjust();
        }

        private void oryoControlsAdjust()
        {
            var setCanInput = new Action<Control, bool>((t, b) =>
            {
                if (t is TextBox) ((TextBox)t).ReadOnly = !b;
                t.Enabled = b;

                t.TabStop = b;
                if (!b)
                    t.BackColor = SystemColors.Menu;
                else
                    t.BackColor = SystemColors.Info;
            });

            if (!verifyCheckBoxVisit.Checked)
            {
                setCanInput(verifyCheckBoxVisitKasan, false);
                labelOryoKasan.Enabled = false;
                
                //20220605152540 furukawa st ////////////////////////
                //距離入力は不要になった2022/5/6瀧井さん
                //setCanInput(verifyBoxKasanKm, false);
                //20220605152540 furukawa ed ////////////////////////

                labelOryoKm.Enabled = false;
                labelOryoCost.Enabled = false;
                setCanInput(verifyBoxOryoCost, false);
                labelKasanCost.Enabled = false;
                setCanInput(verifyBoxKasanCost, false);
            }
            else
            {
                setCanInput(verifyCheckBoxVisitKasan, true);
                var kc = verifyCheckBoxVisitKasan.Checked;

                labelOryoKasan.Enabled = kc;

                //20220605152655 furukawa st ////////////////////////
                //距離入力は不要になった2022/5/6瀧井さん
                //setCanInput(verifyBoxKasanKm, kc);
                //20220605152655 furukawa ed ////////////////////////

                labelOryoKm.Enabled = kc;
                labelOryoCost.Enabled = kc;
                setCanInput(verifyBoxOryoCost, kc);
                labelKasanCost.Enabled = kc;
                setCanInput(verifyBoxKasanCost, kc);
            }
        }

        private void verifyBoxKasanKm_TextChanged(object sender, EventArgs e)
        {
            var kasan = string.IsNullOrWhiteSpace(verifyBoxKasanKm.Text);
            verifyBoxOryoCost.TabStop = kasan;
            verifyBoxKasanCost.TabStop = kasan;
        }

        private void verifyBoxBuiCount_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxBuiCount.Text.Trim().Length == 0)
            {
                verifyCheckBoxBody.TabStop = true;
                verifyCheckBoxRightUpper.TabStop = true;
                verifyCheckBoxLeftUpper.TabStop = true;
                verifyCheckBoxRightLower.TabStop = true;
                verifyCheckBoxLeftLower.TabStop = true;
            }
            else if(verifyBoxBuiCount.Text.Trim() == "5")
            {
                verifyCheckBoxBody.TabStop = false;
                verifyCheckBoxRightUpper.TabStop = false;
                verifyCheckBoxLeftUpper.TabStop = false;
                verifyCheckBoxRightLower.TabStop = false;
                verifyCheckBoxLeftLower.TabStop = false;
                verifyCheckBoxBody.Checked = true;
                verifyCheckBoxRightUpper.Checked = true;
                verifyCheckBoxLeftUpper.Checked = true;
                verifyCheckBoxRightLower.Checked = true;
                verifyCheckBoxLeftLower.Checked = true;
            }
            else
            {
                verifyCheckBoxBody.TabStop = false;
                verifyCheckBoxRightUpper.TabStop = false;
                verifyCheckBoxLeftUpper.TabStop = false;
                verifyCheckBoxRightLower.TabStop = false;
                verifyCheckBoxLeftLower.TabStop = false;
            }
        }

        private void checkBoxMatch_CheckedChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectKoikiData(app);
        }

        private void buttonDistance_Click(object sender, EventArgs e)
        {
            var rr = (RefRece)bsRefReceData.Current;
            if (rr == null) return;

            var app = (App)bsApp.Current;
            var tempClinicNum = app.ClinicNum;
            var tempHihoAdd = app.HihoAdd;
            app.ClinicNum = rr.ClinicNum;
            app.HihoAdd = rr.Add;
            MessageBox.Show(CalcDistance.CheckAppDistance(app), "距離情報",
                MessageBoxButtons.OK, MessageBoxIcon.Information);

            app.ClinicNum =tempClinicNum;
            app.HihoAdd = tempHihoAdd;

        }


        //20190407215536 furukawa st ////////////////////////
        //施術書交付料チェックボックス制御
        private void vcSejutu_CheckedChanged(object sender, EventArgs e)
        {
            sejutuControlsAdjust();
        }

        private void sejutuControlsAdjust()
        {
            var setCanInput = new Action<Control, bool>((t, b) =>
            {
                if (t is TextBox) ((TextBox)t).ReadOnly = !b;
                t.Enabled = b;

                t.TabStop = b;
                if (!b)
                    t.BackColor = SystemColors.Menu;
                else
                    t.BackColor = SystemColors.Info;
            });

            if (!vcSejutu.Checked)
            {
                            //setCanInput(pSejutu, false);
                setCanInput(vbSejutuY, false);
                lblSejutu.Enabled = false;
                setCanInput(vbSejutuM, false);
                setCanInput(vbSejutuG, false);
                lblSejutuY.Enabled = false;
                lblSejutuM.Enabled = false;
            }
            else
            {
                            //setCanInput(pSejutu, true);
                setCanInput(vbSejutuY, true);
                lblSejutu.Enabled = true;
                setCanInput(vbSejutuM, true);
                setCanInput(vbSejutuG, true);
                lblSejutuY.Enabled = true;
                lblSejutuM.Enabled = true;
            }
        }

        //20190407215536 furukawa ed ////////////////////////



        //20201116113232 furukawa st ////////////////////////
        //入力データ修正ボタン
        //一旦間違って登録すると、申請書以外の場合は提供データから持ってきたデータを削除してしまうので、
        //再度、提供データを持ってきて、入力していないステータスにする
        private void buttonReInput_Click(object sender, EventArgs e)
        {

            if (System.Windows.Forms.MessageBox.Show("再登録処理を実行します。宜しいですか？",
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question, 
                MessageBoxDefaultButton.Button2) == DialogResult.No) return;


            var app = (App)bsApp.Current;
            try
            {
                
                selectKoikiData(app);

                string strsql = string.Empty;
                if (current_apptype != APP_TYPE.あんま && current_apptype != APP_TYPE.鍼灸 && current_apptype != APP_TYPE.柔整)
                {
                    //入力前に戻す
                    strsql =
                        $"update application set statusflags = 0 where aid ={app.Aid};";

                    using (DB.Command cmd = DB.Main.CreateCmd(strsql))
                    {
                        cmd.TryExecuteNonQuery();
                    }
                }
                else if (current_apptype == APP_TYPE.あんま || current_apptype == APP_TYPE.鍼灸)
                {
                    //一括更新した情報を消して登録してしまうので、再度提供データをもってくる
                    strsql =
                    $" update application set " +
                    $" psex = d.pgender " +
                    $" ,pbirthday = cast(d.pbirthdayad as date) " +
                    $" ,pname = d.pname,hnum = d.hihonum_nr" +
                    $" ,amonth = cast(substring(d.sejutsuym,4,2) as int)" +
                    $" ,ym = cast(d.ym as int),sid = d.clinicnum" +
                    $" ,sregnumber = d.drnum" +
                    $" ,inum = d.insnum, afamily = " +
                    $" case d.honkenyugai " +
                    $" when  '00' then  200 " +
                    $" when  '08' then  208 " +
                    $" end " +
                    $" ,fchargetype = cast(d.seikyukbn as int)" +
                    $" ,ifirstdate1 = d.firstdate1ad" +
                    $" ,istartdate1 = d.startdate1ad" +
                    $" ,ifinishdate1 = d.finishdate1ad,acounteddays = d.counteddays" +
                    $" ,iname1 = d.f1name" +
                    $" ,fdistance = case when d.until4km<>'' then cast(d.until4km as int) else 0 end " +
                    $" ,atotal = d.total" +
                    $" ,apartial = d.partial" +
                    $" ,acharge = d.charge" +
                    $" ,baccnumber = d.accnumber " +
                    $" ,taggeddatas= case when taggeddatas <>'' then " +
                    $" taggeddatas || '|'" +
                    $" else '' end ||" +
                    $" case when taggeddatas !~'DouiDate:\"[0-9]{4}.[0-9]{2}.[0-9]{2}\"' then " +
                    $" 'DouiDate:\"' || d.douiymdad || '\"'" +
                    $" else '' end || case when taggeddatas !~'flgSejutuDouiUmu:\"true|false\"' then " +
                    $" '|flgSejutuDouiUmu:\"' || case when d.douiymdad ='-infinity' then false else true end || '\"'" +
                    $" else '' end || case when taggeddatas !~'GeneralString1:\"データ取得済\"' then " +
                    $" '|GeneralString1:\"データ取得済\"'" +
                    $" else '' end  " +
                    $" from tdata_ahk d " +
                    $" where " +
                    $" d.detailnum2 = replace(application.emptytext3,'.tif','')" +
                    $" and application.aid={app.Aid} " +
                    $" and d.cym={app.CYM};";
                    using (DB.Command cmd = DB.Main.CreateCmd(strsql))
                    {
                        cmd.TryExecuteNonQuery();
                    }

                    //入力前に戻す
                    strsql =
                        $"update application set statusflags = 0 where aid ={app.Aid};";

                    using (DB.Command cmd = DB.Main.CreateCmd(strsql))
                    {
                        cmd.TryExecuteNonQuery();
                    }
                }
                else if (current_apptype == APP_TYPE.柔整)
                {
                    strsql =
                    $" update application set " +
                    $"  psex=cast(tsex as integer)  " +
                    $" ,pbirthday=cast(tbirthad as date)  " +
                    $" ,ayear=cast(substr(tyearmonth,2,2) as int) " +
                    $" ,amonth=cast(substr(tyearmonth,4,2) as int) " +
                    $" ,atotal=cast(iAmountValue as int) " +
                    $" ,hnum=tinsurednumber " +

                    $" from tdata  " +

                    //20201215105358 furukawa st ////////////////////////
                    //SQL更新遅いので柔整の更新条件にAIDを入れた


                    //20201215120949 furukawa st ////////////////////////
                    //tImagefilenameの区切り文字が間違ってた                    
                    $" where application.aid={app.Aid} and emptytext3 =split_part(timagefilename,'\\',3)";
                    //$" where application.aid={app.Aid} and emptytext3 =split_part(timagefilename,'\',3)";
                    //20201215120949 furukawa ed ////////////////////////



                    //$" where emptytext3 =split_part(timagefilename,'\',3)";
                    //20201215105358 furukawa ed ////////////////////////


                    using (DB.Command cmd = DB.Main.CreateCmd(strsql))
                    {
                        cmd.TryExecuteNonQuery();
                    }

                    //入力前に戻す

                    //20210719162039 furukawa st ////////////////////////
                    //「データ取得済」を削除しないと再入力できない
                    
                    strsql =
                        $"update application set statusflags = 0 " +
                        $",taggeddatas= case when taggeddatas like '%GeneralString1:\"データ取得済\"%' then replace(taggeddatas, '|GeneralString1:\"データ取得済\"', '') end " +
                        $"where aid ={app.Aid};";

                    //      strsql =
                    //         $"update application set statusflags = 0 where aid ={app.Aid};";

                    //20210719162039 furukawa ed ////////////////////////

                    using (DB.Command cmd = DB.Main.CreateCmd(strsql))
                    {
                        cmd.TryExecuteNonQuery();
                    }
                }

                System.Windows.Forms.MessageBox.Show("成功しました。入力画面を閉じて、再度入力してください");

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
        //20201116113232 furukawa ed ////////////////////////


    }
}
