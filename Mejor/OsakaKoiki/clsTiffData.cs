﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.OsakaKoiki
{

    /// <summary>
    /// 画像データ用mdbを取り込むクラス
    /// </summary>
    class clsTiffData
    {
     
        public static int cym;

        public static bool Merge(string strMDBName)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

            wf.LogPrint("app取得中");
            
            //List<App> lstapp = App.GetApps(cym, InsurerID.OSAKA_KOIKI);
            List<App> lstapp = App.GetAppsWithWhere($"where a.ayear>0 and a.pbirthday='0001/01/01' and a.cym={cym}");

            System.Data.OleDb.OleDbConnection cn = new System.Data.OleDb.OleDbConnection();
            System.Data.OleDb.OleDbCommand cmd = new System.Data.OleDb.OleDbCommand();
            System.Data.DataTable dt = new System.Data.DataTable();
            System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter();

            
            DB.Transaction tran = DB.Main.CreateTransaction();

            try
            {
               
                
                //mdb開く                
                cn.ConnectionString = $"Provider=microsoft.jet.oledb.4.0;data source={strMDBName}";
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = $"select * from tData";
                da.SelectCommand = cmd;
                //ロード
                da.Fill(dt);

                
                #region tdata取り込み
                wf.SetMax(dt.Rows.Count);
                wf.LogPrint("data.mdb取込中");
                
                DB.Command cmdins = DB.Main.CreateCmd("truncate table tdata", tran);
                cmdins.TryExecuteNonQuery();

                foreach (System.Data.DataRow r in dt.Rows)
                {
                    int jyymm = int.Parse(
                        r["tBirthEra"].ToString() +
                        r["tBirthYear"].ToString() +
                        r["tBirthMonth"].ToString());

                    //201910型
                    int yyyymm = DateTimeEx.GetAdYearMonthFromJyymm(jyymm);

                    //2019/10/10型
                    DateTime dtBirth;//= new DateTime(yyyymm / 100, yyyymm % 100, int.Parse(dr[0]["tBirthDay"].ToString()));
                    DateTime.TryParse(
                        (yyyymm / 100).ToString() + '/' +
                        (yyyymm % 100).ToString() + '/' +
                        int.Parse(r["tBirthDay"].ToString())
                        , out dtBirth);


                    string strsex= r["tSex"].ToString();

                    if (!int.TryParse(r["tYearMonth"].ToString(), out int tmp_ymwareki))
                    {
                        wf.InvokeValue++;
                        continue;
                    }
                    int ymAD = DateTimeEx.GetAdYearMonthFromJyymm(tmp_ymwareki);
                    


                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("insert into tdata values(");

                    sb.AppendFormat("'{0}',", r[0].ToString());                             //tcommonkey			
                    sb.AppendFormat("'{0}',", r[1].ToString());                             //tijpnumber			
                    sb.AppendFormat("'{0}',", r[2].ToString());                             //tinsurernumber		
                    sb.AppendFormat("'{0}',", r[3].ToString());                             //tyearmonth			
                    sb.AppendFormat("'{0}',", strsex);                                      //tsex					受療者性別
                    sb.AppendFormat("'{0}',", r[5].ToString());                             //tbirthera				受療者生年月日和暦番号
                    sb.AppendFormat("'{0}',", r[6].ToString());                             //tbirthyear			受療者生年月日和暦年
                    sb.AppendFormat("'{0}',", r[7].ToString());                             //tbirthmonth			受療者生年月日月
                    sb.AppendFormat("'{0}',", r[8].ToString());                             //tbirthday				受療者生年月日日
                    sb.AppendFormat("'{0}',", r[9].ToString());                             //iamountvalue			合計金額
                    sb.AppendFormat("'{0}',", r[10].ToString());                            //tinsurednumber		被保番
                    sb.AppendFormat("'{0}',", r[11].ToString());                            //tphysicianclinicnumber
                    sb.AppendFormat("'{0}',", r[12].ToString());                            //timagefilename		画像ファイル名
                    sb.AppendFormat("'{0}',", r[13].ToString());                            //ttenkenkbn			
                    sb.AppendFormat("'{0}',", dtBirth.ToString("yyyy/MM/dd"));              //tbirthad				受療者生年月日西暦
                    sb.AppendFormat("'{0}')", ymAD);                                        //tymad				施術年月西暦

                    cmdins =DB.Main.CreateCmd(sb.ToString(),tran);
                    cmdins.TryExecuteNonQuery();
                    wf.InvokeValue++;
                }
                
                cn.Close();
                tran.Commit();
                cmdins.Dispose();
                #endregion

                #region 更新
                DB.Command cmdupd;

                wf.LogPrint("data.mdb突合中");
                
           
                //タイムアウトになるので再試験→2020/01/27あきらめてストアドにやらせた
                StringBuilder sbupd = new StringBuilder();
/*
                sbupd.AppendLine("update application set ");
                sbupd.AppendLine(" psex=cast(tsex as integer), ");
                sbupd.AppendLine(" pbirthday=cast(tbirthad as date) ");
                sbupd.AppendLine("from tdata ");
                sbupd.AppendLine(" where ");
                sbupd.AppendLine(" emptytext3 <>'' and ");
                sbupd.AppendLine("split_part(emptytext3, '\',13)=split_part(timagefilename,'\',3)");

                //被保番、施術年月（診療年月）、合計金額、メホール処理年月
                //sbupd.AppendLine(" tInsuredNumber=application.hnum ");                    
                //sbupd.AppendLine(" and ym=cast(tymad as int) ");
                //sbupd.AppendLine(" and cast(iAmountValue as int)=application.aTotal");
                sbupd.AppendFormat(" and cym={0} ", cym);
                */

                sbupd.AppendFormat(" select upd({0}); ", cym);


                try
                {
                    //20200827100639 furukawa st ////////////////////////
                    //ストアド時、どうしてもタイムアウトになるのでLongにした
                    
                    //cmdupd = DB.Main.CreateCmd(sbupd.ToString(), tran);
                    cmdupd = DB.Main.CreateLongTimeoutCmd(sbupd.ToString());
                    //20200827100639 furukawa ed ////////////////////////


                    cmdupd.TryExecuteNonQuery();
               
                }
                catch(Npgsql.NpgsqlException nex)
                {
                    wf.LogPrint( nex.Message);
                }
                #endregion

                //tran.Commit();
                System.Windows.Forms.MessageBox.Show("終了しました");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                wf.Dispose();
                cn.Close();
            }
        }
    }
}
