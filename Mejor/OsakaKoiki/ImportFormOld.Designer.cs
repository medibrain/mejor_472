﻿namespace Mejor.OsakaKoiki
{
    partial class ImportFormOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonFselect = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioB_JJizen = new System.Windows.Forms.RadioButton();
            this.radioB_HJigoPart = new System.Windows.Forms.RadioButton();
            this.radioB_HJigoAll = new System.Windows.Forms.RadioButton();
            this.radioB_JJigoPart = new System.Windows.Forms.RadioButton();
            this.radioB_JJigoAll = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.textBoxY = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(396, 284);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonFselect
            // 
            this.buttonFselect.Location = new System.Drawing.Point(64, 20);
            this.buttonFselect.Name = "buttonFselect";
            this.buttonFselect.Size = new System.Drawing.Size(75, 23);
            this.buttonFselect.TabIndex = 1;
            this.buttonFselect.Text = "ファイル...";
            this.buttonFselect.UseVisualStyleBackColor = true;
            this.buttonFselect.Click += new System.EventHandler(this.buttonFselect_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioB_JJizen);
            this.groupBox1.Controls.Add(this.radioB_HJigoPart);
            this.groupBox1.Controls.Add(this.radioB_HJigoAll);
            this.groupBox1.Controls.Add(this.radioB_JJigoPart);
            this.groupBox1.Controls.Add(this.radioB_JJigoAll);
            this.groupBox1.Location = new System.Drawing.Point(24, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 124);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "2. データの種類を選択してください";
            // 
            // radioB_JJizen
            // 
            this.radioB_JJizen.AutoSize = true;
            this.radioB_JJizen.Location = new System.Drawing.Point(40, 100);
            this.radioB_JJizen.Name = "radioB_JJizen";
            this.radioB_JJizen.Size = new System.Drawing.Size(79, 16);
            this.radioB_JJizen.TabIndex = 4;
            this.radioB_JJizen.TabStop = true;
            this.radioB_JJizen.Text = "柔整(事前)";
            this.radioB_JJizen.UseVisualStyleBackColor = true;
            // 
            // radioB_HJigoPart
            // 
            this.radioB_HJigoPart.AutoSize = true;
            this.radioB_HJigoPart.Location = new System.Drawing.Point(40, 80);
            this.radioB_HJigoPart.Name = "radioB_HJigoPart";
            this.radioB_HJigoPart.Size = new System.Drawing.Size(143, 16);
            this.radioB_HJigoPart.TabIndex = 3;
            this.radioB_HJigoPart.TabStop = true;
            this.radioB_HJigoPart.Text = "鍼灸(事後)抽出後データ";
            this.radioB_HJigoPart.UseVisualStyleBackColor = true;
            // 
            // radioB_HJigoAll
            // 
            this.radioB_HJigoAll.AutoSize = true;
            this.radioB_HJigoAll.Location = new System.Drawing.Point(40, 60);
            this.radioB_HJigoAll.Name = "radioB_HJigoAll";
            this.radioB_HJigoAll.Size = new System.Drawing.Size(103, 16);
            this.radioB_HJigoAll.TabIndex = 2;
            this.radioB_HJigoAll.TabStop = true;
            this.radioB_HJigoAll.Text = "鍼灸(事後)全件";
            this.radioB_HJigoAll.UseVisualStyleBackColor = true;
            // 
            // radioB_JJigoPart
            // 
            this.radioB_JJigoPart.AutoSize = true;
            this.radioB_JJigoPart.Location = new System.Drawing.Point(40, 40);
            this.radioB_JJigoPart.Name = "radioB_JJigoPart";
            this.radioB_JJigoPart.Size = new System.Drawing.Size(143, 16);
            this.radioB_JJigoPart.TabIndex = 1;
            this.radioB_JJigoPart.TabStop = true;
            this.radioB_JJigoPart.Text = "柔整(事後)抽出後データ";
            this.radioB_JJigoPart.UseVisualStyleBackColor = true;
            // 
            // radioB_JJigoAll
            // 
            this.radioB_JJigoAll.AutoSize = true;
            this.radioB_JJigoAll.Location = new System.Drawing.Point(40, 20);
            this.radioB_JJigoAll.Name = "radioB_JJigoAll";
            this.radioB_JJigoAll.Size = new System.Drawing.Size(103, 16);
            this.radioB_JJigoAll.TabIndex = 0;
            this.radioB_JJigoAll.TabStop = true;
            this.radioB_JJigoAll.Text = "柔整(事後)全件";
            this.radioB_JJigoAll.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "大阪広域よりのCSVデータを取り込みます";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonFselect);
            this.groupBox2.Location = new System.Drawing.Point(24, 220);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 56);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "3. ファイルを選択してください";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBoxM);
            this.groupBox3.Controls.Add(this.textBoxY);
            this.groupBox3.Location = new System.Drawing.Point(24, 28);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 52);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "1. 処理年月日を入力してください";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(152, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "月分";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(92, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "年";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "和暦";
            // 
            // textBoxM
            // 
            this.textBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxM.Location = new System.Drawing.Point(120, 20);
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(30, 23);
            this.textBoxM.TabIndex = 7;
            // 
            // textBoxY
            // 
            this.textBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxY.Location = new System.Drawing.Point(60, 20);
            this.textBoxY.Name = "textBoxY";
            this.textBoxY.Size = new System.Drawing.Size(30, 23);
            this.textBoxY.TabIndex = 6;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.dataGridView1);
            this.groupBox4.Location = new System.Drawing.Point(244, 28);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(228, 248);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "既にインポートされたデータ一覧";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 232);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(173, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "dtype = 1(柔整), 2(鍼灸), 3(事前)";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.Size = new System.Drawing.Size(204, 208);
            this.dataGridView1.TabIndex = 0;
            // 
            // FormInport_OsakaKoiki
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 312);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Name = "FormInport_OsakaKoiki";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "データインポート";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonFselect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioB_JJizen;
        private System.Windows.Forms.RadioButton radioB_HJigoPart;
        private System.Windows.Forms.RadioButton radioB_HJigoAll;
        private System.Windows.Forms.RadioButton radioB_JJigoPart;
        private System.Windows.Forms.RadioButton radioB_JJigoAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.TextBox textBoxY;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label5;
    }
}