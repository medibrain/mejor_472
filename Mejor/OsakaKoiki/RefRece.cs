﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NPOI.HSSF;
using NPOI.SS.UserModel;

namespace Mejor.OsakaKoiki
{
    class RefRece : RefReceCore
    {
        //保険者独自拡張項目

        public string Kana { get; set; } = string.Empty;                    //被保険者カナ
        public string Zip { get; set; } = string.Empty;                     //被保険者郵便番号
        public string Add { get; set; } = string.Empty;                     //被保険者住所
        public DateTime Birthday { get; set; }                              //被保険者生年月日
        public string DestName { get; set; } = string.Empty;                //送付先
        public string DestKana { get; set; } = string.Empty;                //送付先かな
        public string DestZip { get; set; } = string.Empty;                 //送付先郵便番号
        public string DestAdd { get; set; } = string.Empty;                 //送付先住所
        public int Family { get; set; }                                     //本人家族入外
        public int Ratio { get; set; }                                      //給付割合
        public string GroupCode { get; set; } = string.Empty;               //？
        public string InsName { get; set; } = string.Empty;                 //保険者名
        public DateTime StartDate { get; set; }                             //初検日

        // マージ有のCSVにあるデータ
        // （以前は照会除外リストに登録する用の除外フラグだったが、
        // 除外対象リストの処理の変更に伴い、マージ有に載っていればtrue）
        public bool Merge { get; set; } = false;
        public bool Advance { get; set; } = false;



        /// <summary>
        /// インポートするファイルをまとめたクラス
        /// </summary>
        class ImportFiles
        {
            public string dir = string.Empty;
            public string jyu = string.Empty;
            public string jyuMerge = string.Empty;
            public string ahk = string.Empty;
            public string ahkMerge = string.Empty;
            public string bfJyu = string.Empty;
            public string hihocsv = string.Empty;//20191107124541 furukawa 被保険者マスタクラス追加                                                 

        }

        /// <summary>
        /// インポートメイン処理
        /// </summary>
        /// <param name="dirName"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool Import(string dirName, int cym)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                var res = importAllFiles(dirName, cym, wf);
                if (res)
                {
                    System.Windows.Forms.MessageBox.Show("インポートが完了しました");
                    return true;
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                    return false;
                }
            }
        }




        #region 被保険者CSV適用
        //20191114174118 furukawa st ////////////////////////
        //一旦Dictionaryに全部入れる
        /// <summary>
        /// 被保険者csvを一旦Dictionaryに全部入れる
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static Dictionary<string,hihomst> getHihoInfoFromCsv(string fileName, int cym)
        {
            //var rrs = new List<hihomst>();
            var rrs = new Dictionary<string, hihomst>();

            var l = CommonTool.CsvImportMultiCode(fileName);

     
            foreach (var item in l)
            {
                if (item.Length < 10) continue;

                var rr = new hihomst();
                if (!int.TryParse(item[0].Trim(), out int tmp)) continue;
              

                rr.HHS_BNG = item[0].Trim();
                rr.HHS_SHMKN = item[1].Trim();
                rr.HHS_TS_NMKN = item[2].Trim();
                rr.HHS_KEY_SHMKN = item[3].Trim();
                rr.HHS_SHMKJ = item[4].Trim();
                rr.HHS_TS_NMKJ = item[5].Trim();
                rr.HHS_BRTH_YMD = item[6].Trim();
                rr.HHS_TDFK_NMKJ = item[7].Trim();
                rr.HHS_STS_NMKJ = item[8].Trim();
                rr.HHS_ADDR_KJ = item[9].Trim();
                rr.HHS_ZIPCD = item[10].Trim();
                rr.HHS_KKKRSY_IDO_JYU_CD = item[11].Trim();

                
                rrs.Add(item[0].Trim(),rr);
            }
            return rrs;
        }
        //20191114174118 furukawa ed ////////////////////////


        //20191114174207 furukawa st ////////////////////////
        //DBでなく、登録前のrefreceのリストを被保険者マスタで更新
        /// <summary>
        /// DBでなく、登録前のrefreceのリストを被保険者マスタで更新
        /// </summary>
        /// <param name="lstRefrece"></param>
        /// <param name="lstHiho"></param>
        /// <returns></returns>
        private static List<RefRece> mergeHihomst(List<RefRece> lstRefrece,Dictionary<string,hihomst> lstHiho)
        {
            foreach(RefRece r in lstRefrece)
            {
                if (lstHiho.ContainsKey(r.Num)) { 
                    
                    string strAddHiho = lstHiho[r.Num].HHS_TDFK_NMKJ + lstHiho[r.Num].HHS_STS_NMKJ + lstHiho[r.Num].HHS_ADDR_KJ;

                    //やっぱ全レコードに適用する
                    //if (lstHiho[r.Num].HHS_BRTH_YMD == r.Birthday.ToString("yyyyMMdd") &&
                    //        strAddHiho == r.Add.Trim() &&
                    //        lstHiho[r.Num].HHS_ZIPCD == r.Zip.Trim())
                    //        continue;


                    r.Add = strAddHiho;
                    r.Name = lstHiho[r.Num].HHS_SHMKJ;
                    System.Diagnostics.Debug.Print(r.Num + ' ' + r.Add + ' ' + strAddHiho);
                }
            }

            return lstRefrece;
        }
        //20191114174207 furukawa ed ////////////////////////
        #endregion


        #region インポート全ファイル
        private static bool importAllFiles(string dirName, int cym, WaitForm wf)
        {
            var iFiles = new ImportFiles();
            List<RefRece>jyuBf, jyul, ahkl;

            //20191107125331 furukawa st ////////////////////////
            //被保険者マスタロード用            
            Dictionary<string, hihomst> dicHiho = new Dictionary<string, hihomst>();
            //20191107125331 furukawa ed ////////////////////////



            try
            {
                //ファイルチェック
                wf.LogPrint("ファイルをチェックしています");
                if (!fileCheck(dirName, wf, iFiles)) return false;



                //20191114174118 furukawa st ////////////////////////
                //一旦Dictionaryに全部入れる
                //ファイル：被保険者.csv
                wf.LogPrint("被保険者を読み込んでいます");
                dicHiho = getHihoInfoFromCsv(iFiles.hihocsv, cym);
                //20191114174118 furukawa ed ////////////////////////

                //EYYMM_柔整点検データ（後期分）.xls
                wf.LogPrint("事前柔整申請書を読み込んでいます");
                jyuBf = getRefReceFromExcel(iFiles.bfJyu, cym);


                //ファイル：柔整CSV(マージ無).csv
                wf.LogPrint("柔整申請書を読み込んでいます");
                jyul = getRefReceFromCsv(iFiles.jyu, cym);

                //ファイル：JKX00X0000114_KX00FB40N.csv
                wf.LogPrint("柔整申請書をマージしています");
                mergeFromCsv(iFiles.jyuMerge, jyul);


                //20191114174302 furukawa st ////////////////////////
                //DBでなく、登録前のrefreceのリストを被保険者マスタで更新
                

                wf.LogPrint("被保険者マスタから被保険者名、住所を上書きしています");
                jyul =mergeHihomst(jyul, dicHiho);
                //20191114174302 furukawa ed ////////////////////////


                //ファイル：あんまはり灸マッサージCSV(マージ無).csv
                wf.LogPrint("あはき申請書を読み込んでいます");
                ahkl = getRefReceFromCsv(iFiles.ahk, cym);

                //ファイル：JKX00X0000114_KX00FB44N.csv
                wf.LogPrint("あはき申請書をマージしています");
                mergeFromCsv(iFiles.ahkMerge, ahkl);


                //20191114174302 furukawa st ////////////////////////
                //DBでなく、登録前のrefreceのリストを被保険者マスタで更新
                
                wf.LogPrint("被保険者マスタから被保険者名、住所を上書きしています");
                ahkl = mergeHihomst(ahkl, dicHiho);
                //20191114174302 furukawa ed ////////////////////////


            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            using (var tran = DB.Main.CreateTransaction())
            {
                wf.LogPrint("インポート情報をDBに登録しています");
                var rri = RefReceImport.GetNextImport();
                rri.FileName = dirName;
                rri.CYM = cym;

                //20191107125507 furukawa st ////////////////////////
                //進捗ダイアログに被保険者マスタカウント追加

                
                // rri.ReceCount = jyuBf.Count + jyul.Count + ahkl.Count+hiho.Count;
                rri.ReceCount = jyuBf.Count + jyul.Count + ahkl.Count;

                //20191107125507 furukawa ed ////////////////////////

                rri.AppType = APP_TYPE.複合;
                rri.Insert(tran);

                jyuBf.ForEach(r => r.ImportID = rri.ImportID);
                jyul.ForEach(r => r.ImportID = rri.ImportID);
                ahkl.ForEach(r => r.ImportID = rri.ImportID);

                

                wf.SetMax(rri.ReceCount);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                wf.LogPrint("事前柔整申請書情報をDBに登録しています");
                foreach (var item in jyuBf)
                {
                    if (!DB.Main.Insert(item, tran)) return false;
                    wf.InvokeValue++;
                }

                wf.LogPrint("柔整申請書情報をDBに登録しています");
                foreach (var item in jyul)
                {
                    if (!DB.Main.Insert(item, tran)) return false;
                    wf.InvokeValue++;
                }

                wf.LogPrint("あはき申請書情報をDBに登録しています");
                foreach (var item in ahkl)
                {
                    if (!DB.Main.Insert(item, tran)) return false;
                    wf.InvokeValue++;
                }

               
                tran.Commit();
            


                return true;
            }
        }
        #endregion

        #region ファイル存在確認
        private static bool fileCheck(string dirName, WaitForm wf, ImportFiles iFiles)
        {
            bool error = false;
            iFiles.dir = dirName;
            var dirs = Directory.GetDirectories(dirName);
            

            #region (8)柔整CSV(入力全件)ファイル名チェック

            var jyusei_All_CSV = Array.FindAll(dirs, dir => dir.Contains("(8)柔整CSV(入力全件)"));
            if (jyusei_All_CSV.Length == 1)
            {
                var files = Directory.GetFiles(jyusei_All_CSV[0]);

                //20201217104444 furukawa st ////////////////////////
                //大文字小文字区分なしにする

                var arrJyuseiFileName = Array.FindAll(files, f => f.Contains(".csv") || f.Contains(".CSV"));
                //var js = Array.FindAll(fs, f => f.Contains(".csv"));
                //20201217104444 furukawa ed ////////////////////////

                if (arrJyuseiFileName.Length == 1) iFiles.jyu = arrJyuseiFileName[0];
                else
                {
                    wf.LogPrint("柔整全件CSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("柔整全件CSVが特定できませんでした。");
                error = true;
            }
            #endregion

            #region (3)柔整CSVファイル名チェック

            var jyuCSV = Array.FindAll(dirs, dir => dir.Contains("(3)柔整CSV"));
            if (jyuCSV.Length == 1)
            {
                var files = Directory.GetFiles(jyuCSV[0]);

                //20201217112722 furukawa st ////////////////////////
                //大文字小文字区分なしにする

                var arrJyuseiFileName = Array.FindAll(files, f => f.Contains(".csv") || f.Contains(".CSV"));
                //var arrJyuseiFileName = Array.FindAll(files, f => f.Contains(".csv"));
                //20201217112722 furukawa ed ////////////////////////


                if (arrJyuseiFileName.Length == 1) iFiles.jyuMerge = arrJyuseiFileName[0];
                else
                {
                    wf.LogPrint("柔整マージCSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("柔整マージCSVが特定できませんでした。");
                error = true;
            }

            #endregion

            #region (7)あんま・はり灸・マッサージCSV(入力全件)ファイル名チェック
            var ahkALLCSV = Array.FindAll(dirs, dir => dir.Contains("(7)あんま・はり灸・マッサージCSV(入力全件)"));
            if (ahkALLCSV.Length == 1)
            {
                var files = Directory.GetFiles(ahkALLCSV[0]);

                //20201217112801 furukawa st ////////////////////////
                //大文字小文字区分なしにする
                
                var arrAHKALLFileName = Array.FindAll(files, f => f.Contains(".csv") || f.Contains(".CSV"));
                //var arrAHKALLFileName = Array.FindAll(files, f => f.Contains(".csv"));
                //20201217112801 furukawa ed ////////////////////////


                if (arrAHKALLFileName.Length == 1) iFiles.ahk = arrAHKALLFileName[0];
                else
                {
                    wf.LogPrint("あはき全件CSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("あはき全件CSVが特定できませんでした。");
                error = true;
            }
            #endregion

            #region (5)あんま・はり灸・マッサージCSV(マージあり)ファイル名チェック
            var ahkMCSV = Array.FindAll(dirs, dir => dir.Contains("(5)あんま・はり灸・マッサージCSV(マージあり)"));
            if (ahkMCSV.Length == 1)
            {
                var files = Directory.GetFiles(ahkMCSV[0]);
                //20201217112858 furukawa st ////////////////////////
                //大文字小文字区分なしにする
                
                var arrAHKMergeFileName = Array.FindAll(files, f => f.Contains(".csv") || f.Contains(".CSV"));
                //var arrAHKMergeFileName = Array.FindAll(files, f => f.Contains(".csv"));
                //20201217112858 furukawa ed ////////////////////////


                if (arrAHKMergeFileName.Length == 1) iFiles.ahkMerge = arrAHKMergeFileName[0];
                else
                {
                    wf.LogPrint("あはきマージCSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("あはきマージCSVが特定できませんでした。");
                error = true;
            }
            #endregion

            #region 柔整点検データ（後期分）ファイル名チェック
            var dirFs = Directory.GetFiles(dirName);

            //20201217113756 furukawa st ////////////////////////
            //トラブルの元なのでファイル名のカッコは無視する
            
            var arrJyuseiKoukiFileName = Array.FindAll(dirFs, dir => dir.Contains("柔整点検データ") && dir.Contains("後期分"));
            //var arrJyuseiKoukiFileName = Array.FindAll(dirFs, dir => dir.Contains("_柔整点検データ（後期分）"));
            //20201217113756 furukawa ed ////////////////////////


            if (arrJyuseiKoukiFileName.Length == 1) iFiles.bfJyu = arrJyuseiKoukiFileName[0];
            else
            {
                wf.LogPrint("柔整事前Excelが特定できませんでした。");
                error = true;
            }
            #endregion

            #region 被保険者.csvファイル名チェック
            //20191106142718 furukawa st ////////////////////////
            //被保険者csv存在確認

            var hihocsv = Array.FindAll(dirFs, dir => dir.Contains("被保険者"));
            if (hihocsv.Length == 1)
            {
                //dirFs = Directory.GetFiles(hihocsv[0]);



                //20201217113015 furukawa st ////////////////////////
                //大文字小文字区分なしにする
                
                var arrHihoFileName = Array.FindAll(dirFs, dir=> dir.Contains("被保険者.csv")||dir.Contains("被保険者.CSV"));
                //var arrHihoFileName = Array.FindAll(dirFs, dir => dir.Contains("被保険者.csv"));
                //20201217113015 furukawa ed ////////////////////////


                if (arrHihoFileName.Length == 1) iFiles.hihocsv = arrHihoFileName[0];
                else
                {
                    wf.LogPrint("被保険者CSVが特定できませんでした。");
                    error = true;
                }
            }
            else
            {
                wf.LogPrint("被保険者CSVが特定できませんでした。");
                error = true;
            }
            //20191106142718 furukawa ed ////////////////////////
            #endregion

            return !error;
        }
        #endregion
        

        #region refrece csvインポート
        private static List<RefRece> getRefReceFromCsv(string fileName, int cym)
        {
            var rrs = new List<RefRece>();

            //20190201115213 furukawa st ////////////////////////
            //インポートデータがUTF8に変わるため

            //var l = CommonTool.CsvImportShiftJis(fileName);

            //var l = CommonTool.CsvImportUTF8(fileName);
            var l = CommonTool.CsvImportMultiCode(fileName);

            //20190201115213 furukawa ed ////////////////////////


            foreach (var item in l)
            {
                if (item.Length < 20) continue;

                int.TryParse(item[4], out int family);      //世非
                int.TryParse(item[5], out int ratio);       //割合
                int.TryParse(item[6], out int mym);         //診療年月
                int.TryParse(item[21], out int birth);      //被保険者生年月日

                var rr = new RefRece();



                //申請書タイプを　あはきの8番目　診療区分から取得してる
                //柔整のファイルには柔整しか載っておらず、診療区分の項目がないため
                rr.AppType =
                    item[7] == "8" ? APP_TYPE.鍼灸 :
                    item[7] == "9" ? APP_TYPE.あんま : 
                    APP_TYPE.柔整;





                //20201217140051 furukawa st ////////////////////////
                //csvファイルの年月に和暦番号追加された

                mym = DateTimeEx.GetAdYearMonthFromJyymm(mym);

                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        //mym = DateTimeEx.GetAdYearFromHs(mym) * 100 + mym % 100;
                                    //mym = DateTimeEx.GetAdYearFromHs(mym / 100) * 100 + mym % 100;
                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                //20201217140051 furukawa ed ////////////////////////


                rr.CYM = cym;                                                                   //メホール処理年月
                rr.InsName = item[0].Replace("　", " ").Replace(" ", "");                       //保険者名
                rr.InsNum = item[1].Trim();                                                     //保険者番号
                rr.Num = item[2].Trim();                                                        //被保険者証番号
                rr.Name = item[3].Trim();                                                       //被保険者氏名（漢字）
                rr.Family = family;                                                             //世非
                rr.Ratio = ratio;                                                               //割合
                rr.MYM = mym;                                                                   //診療年月
                                                                                                
                rr.Kana = item[16].Trim();                                                      //被保険者検索カナ
                rr.Zip = item[17].Trim();                                                       //被保険者郵便番号
                rr.Add = item[18].Trim() + item[19].Trim() + item[20].Trim();                   //被保険者都道府県 ＋被保険者市区町村＋ 被保険者住所
                rr.Birthday = item[21].Trim().ToDateTime();                                     //被保険者生年月日
                rr.DestZip = item[22].Trim();                                                   //送付先郵便番号
                rr.DestAdd = item[23].Trim() + item[24].Trim() + item[25].Trim();               //送付先都道府県＋ 送付先市区町村＋ 送付先住所
                rr.DestName = item[26].Trim();                                                  //送付先氏名
                rr.DestKana = item[27].Trim();                                                  //送付先氏名カナ

                if (rr.AppType == APP_TYPE.柔整)
                {
                    //柔整だけの項目
                    int.TryParse(item[12], out int sDate);                      //施術開始年月日
                    int.TryParse(item[13], out int days);                       //日数
                    int.TryParse(item[14], out int total);                      //費用額
                    int.TryParse(item[15], out int charge);                     //支給金額
                    rr.ClinicNum = item[8].Trim() + "7" + item[9].Trim();       //都道府県 ＋"7"(柔整) ＋施術師番号
                    rr.GroupCode = item[10].Trim();                             //団体コード
                    rr.ClinicName = item[11].Trim();                            //施術所名
                    rr.Days = days;
                    rr.Total = total;
                    rr.Charge = charge;


                    //20201217140957 furukawa st ////////////////////////
                    //仕様変更により年号が入ったので直接西暦変換へ                    
                    //5020916＝＞2020/09/16
                    rr.StartDate = DateTimeEx.GetDateFromJInt7(sDate);
                  

                            ////20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                            //int y = DateTimeEx.GetAdYearFromHs(sDate / 100);

                            ////int y = DateTimeEx.GetAdYearFromHs(sDate / 10000);
                            ////20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                            //int m = sDate / 100 % 100;
                            //int d = sDate % 100;
                            //rr.StartDate = new DateTime(y, m, d);
                    //20201217140957 furukawa ed ////////////////////////

                }
                else
                {
                    //あはき

                    int.TryParse(item[11], out int sDate);              //初療年月
                    int.TryParse(item[12], out int days);               //日数
                    int.TryParse(item[13], out int total);              //費用額
                    int.TryParse(item[14], out int charge);             //支給金額
                    rr.DrNum = item[9].Trim();                          //施術師番号
                    rr.DrName = item[10].Trim();                        //施術師氏名
                    rr.SearchNum = item[15].Trim();                     //データ取扱番号
                    rr.ClinicNum = rr.DrNum;                            //施術所番号＝施術師番号
                    rr.ClinicName = item[28].Trim();                    //施術所名
                    rr.Days = days;
                    rr.Total = total;
                    rr.Charge = charge;



                    //20201217141933 furukawa st ////////////////////////
                    //仕様変更により年号が入ったので直接西暦変換へ


                    rr.StartDate = DateTimeEx.GetDateFromJInt7(sDate);


                        ////20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        //int y = DateTimeEx.GetAdYearFromHs(sDate / 100);

                                ////int y = DateTimeEx.GetAdYearFromHs(sDate / 10000);                    
                        ////20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                        //int m = sDate / 100 % 100;
                        //int d = sDate % 100;
                        //rr.StartDate = new DateTime(y, m, d);


                    //20201217141933 furukawa ed ////////////////////////
                }

                rrs.Add(rr);
            }
            return rrs;
        }
        #endregion

        #region refrece csvマージ
        /// <summary>
        /// 被保険者証番号があって、医療機関コードと診療年月？が合致し、マージしていない場合、マージフラグ立てる
        /// </summary>
        /// <param name="fileName">JKX00X0000114_KX00FB40N.csv</param>
        /// <param name="rrs">柔整CSV(マージ無).csv</param>
        private static void mergeFromCsv(string fileName, List<RefRece> rrs)
        {

            //20190201115309 furukawa st ////////////////////////
            //インポートデータがUTF8に変わるため
            //var l = CommonTool.CsvImportShiftJis(fileName);
            
            //var l = CommonTool.CsvImportUTF8(fileName);
            var l = CommonTool.CsvImportMultiCode(fileName);

            //20190201115309 furukawa ed ////////////////////////

            var dic = new Dictionary<string, List<RefRece>>();

            foreach (var item in rrs)
            {
                if (!dic.ContainsKey(item.Num)) dic.Add(item.Num, new List<RefRece>());
                dic[item.Num].Add(item);
            }

            foreach (var item in l)
            {
                if (item.Length < 20) continue;
                int.TryParse(item[6], out int mym);


                //20201217135017 furukawa st ////////////////////////
                //csvファイルの年月に和暦番号追加された
                
                mym = DateTimeEx.GetAdYearMonthFromJyymm(mym);

                            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                            // mym = DateTimeEx.GetAdYearFromHs(mym) * 100 + mym % 100;
                            //mym = DateTimeEx.GetAdYearFromHs(mym / 100) * 100 + mym % 100;
                            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                //20201217135017 furukawa ed ////////////////////////

                var num = item[2].Trim();
                
                var appType = 
                    item[7] == "8" ? APP_TYPE.鍼灸 :
                    item[7] == "9" ? APP_TYPE.あんま : APP_TYPE.柔整;

                var clinicNum = 
                    appType == APP_TYPE.柔整 ?
                    item[8].Trim() + "7" + item[9].Trim() : item[9].Trim();

                //被保険者証番号が無い場合は抜ける
                if (!dic.ContainsKey(num)) continue;

                //被保険者証番号があって、医療機関コードと診療年月？が合致し、マージしていない場合
                var t = dic[num].FirstOrDefault(r =>
                     r.ClinicNum == clinicNum && r.MYM == mym && !r.Merge);

                if (t != null)
                {
                    t.Merge = true;//マージしたかフラグたてる
                }
                else
                {

                    //同被保番で、医療機関、診療年月で合致するレコードがない
                    
                
                    //20191128094935 furukawa st ////////////////////////
                    //ループ中にメッセージ出さない

                    
                    //System.Windows.Forms.MessageBox.Show("NULL");
                    //20191128094935 furukawa ed ////////////////////////
                }
            }
        }

        #endregion

        #region refrece excelインポート
        /// <summary>
        /// EYYMM_柔整点検データ（後期分）.xls取込
        /// </summary>
        /// <param name="fileName">EYYMM_柔整点検データ（後期分）.xls</param>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<RefRece> getRefReceFromExcel(string fileName, int cym)
        {
            var rrs = new List<RefRece>();

            using (var fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                var wb = new NPOI.HSSF.UserModel.HSSFWorkbook(fs);
                var sheet = wb.GetSheet("後期点検データ");

                var rowCount = sheet.LastRowNum + 2;

                for (int i = 2; i < rowCount; i++)
                {
                    var r = sheet.GetRow(i);
                    if (r == null) continue;
                    var c = r.GetCell(0);
                    if (c == null || c.CellType != CellType.String) continue;

                    var b = r.GetCell(0).StringCellValue;
                    if (b.Length < 1 || b[0] != 'B') continue;

                    int.TryParse(r.GetCell(4).StringCellValue, out int mym);              //診療年月
                    int.TryParse(r.GetCell(10).StringCellValue, out int days);            //実日数
                    int.TryParse(r.GetCell(16).StringCellValue, out int total);           //合計
                    int.TryParse(r.GetCell(17).StringCellValue, out int partial);         //一部負担金

                    var rr = new RefRece();
                    rr.AppType = APP_TYPE.柔整;
                    rr.CYM = cym;
                    rr.MYM = DateTimeEx.GetAdYearMonthFromJyymm(mym);
                    rr.InsName = r.GetCell(2).StringCellValue;                                      //保険者番号
                    rr.ClinicNum = r.GetCell(5).StringCellValue;                                    //医療機関コード
                    rr.Num = r.GetCell(6).StringCellValue.PadLeft(8, '0'); ;                        //被保険者証番号
                    rr.Birthday = DateTimeEx.GetDateFromJstr7(r.GetCell(7).StringCellValue);        //生年月日
                    rr.Name = r.GetCell(9).StringCellValue;                                         //被保険者氏名
                    rr.Days = days;
                    rr.Total = total;
                    rr.Charge = total - partial;
                    rr.Advance = true;

                    rrs.Add(rr);
                }
            }

            return rrs;
        }
        #endregion



        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="mym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInput(int cym, int mym, string num, int total) =>
            DB.Main.Select<RefRece>(new { num, mym, total, cym }).ToList();

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="num"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cym, string num, int total) =>
            DB.Main.Select<RefRece>(new { cym, num, total }).ToList();

        public static List<RefRece> SelectAll(int cym) =>
            DB.Main.Select<RefRece>(new { cym }).ToList();
    }
}