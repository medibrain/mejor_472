﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.OsakaKoiki
{
    //20191107124438 furukawa 
    //被保険者マスタクラス

    class hihomst
    {
        [DB.DbAttribute.PrimaryKey]
        public string HHS_BNG { get; set; } = string.Empty;             //被保険者番号



        
        public string HHS_SHMKN { get; set; } = string.Empty;                   //被保険者氏名　カナ
        public string HHS_TS_NMKN { get; set; } = string.Empty;
        public string HHS_KEY_SHMKN { get; set; } = string.Empty;               //被保険者カナ　キー
        public string HHS_SHMKJ { get; set; } = string.Empty;                   //被保険者氏名漢字
        public string HHS_TS_NMKJ { get; set; } = string.Empty;//
        public string HHS_BRTH_YMD { get; set; } = string.Empty;                //被保険者生年月日
        public string HHS_TDFK_NMKJ { get; set; } = string.Empty;               //被保険者都道府県名
        public string HHS_STS_NMKJ { get; set; } = string.Empty;                //被保険者市区町村名
        public string HHS_ADDR_KJ { get; set; } = string.Empty;                 //被保険者住所
        public string HHS_ZIPCD { get; set; } = string.Empty;                   //被保険者郵便番号
        public string HHS_KKKRSY_IDO_JYU_CD { get; set; } = string.Empty;


    }
}
