﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Mejor.OsakaKoiki
{
    public partial class ShokaiMenuForm : Form
    {
        Insurer insurer;
        int cym;

        public ShokaiMenuForm(Insurer insurer, int cym)
        {
            InitializeComponent();
            this.insurer = insurer;
            this.cym = cym;

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            this.Text = "照会オプション機能" + $"{ DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100)}年{ cym % 100}月分";
            //this.Text = "照会オプション機能" + $"{ DateTimeEx.GetHsYearFromAd(cym / 100)}年{ cym % 100}月分";
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        }

        private void buttonJigo_Click(object sender, EventArgs e)
        {
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = $"{cym.ToString()}.csv";
                f.Filter = "*.csv|CSVファイル";
                if (f.ShowDialog() != DialogResult.OK) return;
                fileName = f.FileName;
            }
            Export.ExportCSVForInquiry(cym, fileName);
        }
    }
}
