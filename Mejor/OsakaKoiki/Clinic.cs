﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.OsakaKoiki
{
    class Clinic
    {
        [DB.DbAttribute.PrimaryKey]
        public string ClinicNum { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public double Ido { get; set; }
        public double Keido { get; set; }

        private int branchNo = 0;

        public static bool Import()
        {
            string fileName;
            using (var f = new System.Windows.Forms.OpenFileDialog())
            {
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;
                fileName = f.FileName;
            }

            var csv = CommonTool.CsvImportShiftJis(fileName);
            var dic = new Dictionary<string, Clinic>();

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(csv.Count);

                for (int i = 2; i < csv.Count; i++)
                {
                    wf.InvokeValue++;
                    var line = csv[i];
                    if (line.Length < 10) continue;

                    var c = new Clinic();
                    c.ClinicNum = line[0].Trim() + line[1].Trim() + line[2].Trim() + line[3].Trim();
                    int.TryParse(line[4].Trim(), out c.branchNo);
                    c.Name = line[8].Trim();
                    c.Zip = line[9].Trim();
                    c.Address = line[10].Trim();

                    var g = GeoData.GetGeoData(c.Address);
                    if (g != null)
                    {
                        c.Ido = g.Ido;
                        c.Keido = g.Keido;
                    }
                    else
                    {
                        //20200707232224 furukawa st ////////////////////////
                        //表示変更
                        
                        wf.LogPrint($"緯度経度取得できず：{c.Address}");
                        //wf.LogPrint(c.Address);
                        //20200707232224 furukawa ed ////////////////////////
                    }

                    if (dic.ContainsKey(c.ClinicNum))
                    {
                        if (dic[c.ClinicNum].branchNo > c.branchNo) continue;
                        dic[c.ClinicNum] = c;
                    }
                    else
                    {
                        dic.Add(c.ClinicNum, c);
                    }
                }
                wf.LogSave("not geodata addresses.txt");
            }

            using (var tran = DB.Main.CreateTransaction())
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.SetMax(dic.Count);

                //20200708104602 furukawa st ////////////////////////
                //前にあるデータを残して、無い医療機関コードだけを登録する

                //削除しない//
                //wf.LogPrint("全削除");
                //DB.Command cmd = new DB.Command("truncate table clinic", tran);
                //cmd.TryExecuteNonQuery();
                //20200708104602 furukawa ed ////////////////////////


                wf.LogPrint("登録");
                foreach (var item in dic)
                {
                    wf.InvokeValue++;
                    try
                    {

                        //20200707181836 furukawa st ////////////////////////
                        //重複エラー等すっ飛ばして全部登録

                        //チェックしないで全部回す
                        if (GetClinic(item.Key) == null)
                        {
                            DB.Main.Insert(item.Value, tran);
                        }
                        //if (!DB.Main.Insert(item.Value, tran)) return false;
                        //20200707181836 furukawa ed ////////////////////////

                    }
                    catch (Exception ex)
                    {
                        //重複は無視する
                    }

                }
                tran.Commit();
            }

            return true;
        }

        public static Clinic GetClinic(string clinicNum) =>
            DB.Main.Select<Clinic>(new { clinicNum }).FirstOrDefault();
    }
}
