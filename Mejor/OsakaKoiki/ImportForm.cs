﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.OsakaKoiki
{
    public partial class ImportForm : Form
    {
        public ImportForm(int cym)
        {
            InitializeComponent();

            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            textBoxY.Text = DateTimeEx.GetHsYearFromAd(cym / 100, cym % 100).ToString();
            //textBoxY.Text = DateTimeEx.GetHsYearFromAd(cym / 100).ToString();
            //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            textBoxM.Text = (cym % 100).ToString();
        }



        /// <summary>
        /// 年月チェック 2020/08/27
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private bool chkYM(out int cym)
        {
            cym = 0;

            int.TryParse(textBoxY.Text, out int y);

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            int.TryParse(textBoxM.Text, out int m);

            y = DateTimeEx.GetAdYearFromHs(y * 100 + m);
            //y = DateTimeEx.GetAdYearFromHs(y);
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            if (y < DateTime.Today.Year - 2 || DateTime.Today.Year < y)
            {
                MessageBox.Show("請求年度の指定が不正です");
                return false;
            }

            //20190407104206 furukawa st ////////////////////////
            //和暦年取得に関連し月を先に取るため順序入れ替え
            int.TryParse(textBoxM.Text, out m);
            //int.TryParse(textBoxM.Text, out int m);
            //20190407104206 furukawa ed ////////////////////////

            if (m < 1 || 12 < m)
            {
                MessageBox.Show("月の指定が不正です");
                return false ;
            }

            cym = y * 100 + m;
            return true;

        }



        private void buttonStart_Click(object sender, EventArgs e)
        {
            string dirName = textBox1.Text;
            if (dirName == string.Empty)
            {
                MessageBox.Show("対象フォルダを指定してください");
                return;
            }

            //int.TryParse(textBoxY.Text, out int y);

            ////20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //int.TryParse(textBoxM.Text, out int m);

            //y = DateTimeEx.GetAdYearFromHs(y * 100 + m);
            ////y = DateTimeEx.GetAdYearFromHs(y);
            ////20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //if (y < DateTime.Today.Year - 2 || DateTime.Today.Year < y)
            //{
            //    MessageBox.Show("請求年度の指定が不正です");
            //    return;
            //}

            ////20190407104206 furukawa st ////////////////////////
            ////和暦年取得に関連し月を先に取るため順序入れ替え
            //int.TryParse(textBoxM.Text, out m);
            ////int.TryParse(textBoxM.Text, out int m);
            ////20190407104206 furukawa ed ////////////////////////

            //if (m < 1 || 12 < m)
            //{
            //    MessageBox.Show("月の指定が不正です");
            //    return;
            //}

            //int cym = y * 100 + m;

            if (!chkYM(out int cym)) return;
            if (RefRece.Import(dirName, cym)) Close();



            //20191125090614 furukawa st ////////////////////////
            //画像マッチングMDB処理


            //mdb取込処理
            if (txtmdb.Text.Trim() == string.Empty)
            {
                MessageBox.Show("画像マッチングデータが選択されていません。スキップします");
                //return;
            }
            else
            {
                clsTiffData clsTiffData = new clsTiffData();
                clsTiffData.cym = cym;
                if (!clsTiffData.Merge(txtmdb.Text.Trim()))
                {
                    MessageBox.Show("画像マッチングに失敗しました");
                    return;
                }
            }

            //20191125090614 furukawa ed ////////////////////////



            //提供データあはき取込処理
            if (txtAHK.Text.Trim() == string.Empty)
            {
                MessageBox.Show("提供データあはきが選択されていません。スキップします");
                //return;
            }
            else
            {
                tdata_ahk.dataimport_ahk(txtAHK.Text.Trim(), cym);
                return;

            }

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDir_Click(object sender, EventArgs e)
        {
            var d = new OpenDirectoryDiarog();
            if (d.ShowDialog() != DialogResult.OK) return;
            textBox1.Text = d.Name;
        }



        private void btnRef_Click(object sender, EventArgs e)
        {
            var f = new OpenFileDialog();
            f.FileName = "data.mdb";
            f.Filter = "提供データ柔整mdb|*.mdb";
            f.FilterIndex = 0;
            
            f.Title = "提供データ柔整選択";
            if (f.ShowDialog() != DialogResult.OK) return;
            txtmdb.Text = f.FileName;

        }


        private void btnRefAHK_Click(object sender, EventArgs e)
        {
            var f = new OpenFileDialog();            

            //2021/07/13準備段階
            //f.Filter = "提供データあはき|*.xlsm";
            //f.Filter = "提供データあはきcsv|*.csv|提供データあはきExcel|*.xlsm";

            f.Filter = "提供データあはきcsv|*.csv";
            f.FilterIndex = 0;

            f.Title = "提供データあはき選択";
            if (f.ShowDialog() != DialogResult.OK) return;
            txtAHK.Text = f.FileName;
        }

        private void buttonJyuseiOnly_Click(object sender, EventArgs e)
        {
            int.TryParse(textBoxY.Text, out int y);

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            int.TryParse(textBoxM.Text, out int m);

            y = DateTimeEx.GetAdYearFromHs(y * 100 + m);
            //y = DateTimeEx.GetAdYearFromHs(y);
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            if (y < DateTime.Today.Year - 2 || DateTime.Today.Year < y)
            {
                MessageBox.Show("請求年度の指定が不正です");
                return;
            }

            //20190407104206 furukawa st ////////////////////////
            //和暦年取得に関連し月を先に取るため順序入れ替え
            int.TryParse(textBoxM.Text, out m);
            //int.TryParse(textBoxM.Text, out int m);
            //20190407104206 furukawa ed ////////////////////////

            if (m < 1 || 12 < m)
            {
                MessageBox.Show("月の指定が不正です");
                return;
            }

            int cym = y * 100 + m;
            if (txtmdb.Text.Trim() == string.Empty)
            {
                MessageBox.Show("画像マッチングデータが選択されていません。スキップします");
                return;
            }


            clsTiffData clsTiffData = new clsTiffData();
            clsTiffData.cym = cym;
            if (!clsTiffData.Merge(txtmdb.Text.Trim()))
            {
                MessageBox.Show("画像マッチングに失敗しました");
                return;
            }
        }

        //20200706185615 furukawa st ////////////////////////
        //医療機関マスタ取込ボタン設置
        
        private void buttonClinicImp_Click(object sender, EventArgs e)
        {
            if (System.Windows.Forms.MessageBox.Show("かなり時間が掛かります。サーバ上での実行をおすすめしますが、このまま実行しますか？",
                Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if(Clinic.Import()) MessageBox.Show("終了");
            }
        }



        //20200706185615 furukawa ed ////////////////////////

        private void btnAHKOnly_Click(object sender, EventArgs e)
        {
            if (!chkYM(out int cym)) return;

            //提供データあはき取込処理
            if (txtAHK.Text.Trim() == string.Empty)
            {
                MessageBox.Show("提供データあはきが選択されていません。スキップします");
                //return;
            }
            else
            {

                tdata_ahk.dataimport_ahk(txtAHK.Text.Trim(), cym);


            }
        }

    }
}
