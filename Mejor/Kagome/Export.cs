﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Kagome
{
    partial class Export2022
    {
        #region 宣言部


        App app { get; set; }//applicationテーブル格納用クラス変数
        int honkeKubun { get; set; }//本家区分  //5 本家区分 数値 1 ●

        //int chargeYM { get; set; }//処理月
        string chargeYM { get; set; }//処理月

        APP_TYPE appType { get; set; }//給種区分
        string mediYM { get; set; }//診療年月
        string syo_mark { get; set; }//被保険者証記号
        string syo_number { get; set; }//被保険者証番号


        DateTime birth { get; set; }//生年月日
        string birthforOutput { get; set; }//21.生年月日出力用


        int sex { get; set; }//性別

        DateTime startDate { get; set; }//27 施術開始日 数値 8 
        string startDateforOutput { get; set; }//27 施術開始日 数値 8 

        int days { get; set; }
        int total { get; set; }
        int charge { get; set; }//金額
        int partial { get; set; }//一部負担金
        string receNo { get; set; }//16.レセプト番号
        int henrei { get; set; }//
        int jyushinKubun { get; set; }//受信区分
        int numbering { get; set; }//ナンバリング

        
        string NaibuRenban2 { get; set; } = string.Empty;         //2.内部連番
        string SiharaisakiCode { get; set; } = string.Empty;      //18.支払先コード
        string KumiaiCode { get; set; } = string.Empty;           //3.組合コード
        string HonSibuCode { get; set; } = string.Empty;          //4 本支部コード 数値 2 △



        string SeikyukeitaiKBN { get; set; } = string.Empty;      //6 請求形態区分 数値 1 ●
        string SeikyuKeitaiBango { get; set; } = string.Empty;	  //7.請求時形態番号     
        string SeikyuNengetu { get; set; } = string.Empty;        //8.請求年月
        string FukenCode { get; set; } = string.Empty;            //9 府県コード 数値 2 △

        int KyusyuKBN { get; set; }                               //10 給種区分 数値 1 ● ->下に宣言しているが、別の用途なので別宣言
        string SinryoKBN { get; set; } = string.Empty;            //11 診療区分 数値 1 ●

        string Bango { get; set; } = string.Empty;                //12.番号　各処理月毎に発番　　⇒　１２．番号（内部連番）はNO2の上１桁目に１を追加してください。


        string SeikyujiTorimatomeKbn { get; set; } = "1";         //13.請求時取りまとめ区分 1:団体 2:個人 ⇒　両方ともに「１：団体」を設定してください。
        string SiharaijiTorimatomeKbn { get; set; } = "1";        //14.支払時取りまとめ区分 1:団体 2:個人   ⇒　両方ともに「１：団体」を設定してください。
        
        string NaibuRenban15 { get; set; } = string.Empty;        //15.内部連番　1行ごとの連番

        string sickCode1 = string.Empty;  //23 疾病コード1 数値 4 
        string sickCode2 = string.Empty;  //24 疾病コード2 数値 4 
        string sickCode3 = string.Empty;  //25 疾病コード3 数値 4 
        string sickCode4 = string.Empty;  //26 疾病コード4 数値 4 


        DateTime endDate { get; set; }                  //28 施術終了日 数値 8 
        string strEndDate { get; set; } = string.Empty;//日付型だが、文字型にしないとオール０が入らない

        string dtDoui = string.Empty;                   //29 同意年月日 数値 8 
        string dtUketuke = string.Empty;                //30 受付年月日 数値 8 

        string ItibuFutanKin = string.Empty;            //33 一部負担金 数値 8 
        string SejutuKaisu = string.Empty;              //34 施術回数 数値 3 
        string SeikyuGaku = string.Empty;               //35 請求額 数値 8   

        string KohiCode1 { get; set; } = string.Empty;              //36 公費コード1 数値 2 △
        string KohiCode2 { get; set; } = string.Empty;              //37 公費コード2 数値 2 △
        string KohiKingaku1 { get; set; } = string.Empty;           //38 公費金額1 数値 9 △
        string KohiKingaku2 { get; set; } = string.Empty;           //39 公費金額2 数値 9 △
        string Kanjafutangaku_Kohi1 { get; set; } = string.Empty;   //40 患者負担額公費1 数値 8 △
        string Kanjafutangaku_Kohi2 { get; set; } = string.Empty;   //41 患者負担額公費2 数値 8 △
        string MaruFuKBN { get; set; } = string.Empty;              //42 マル不区分 数値 1 △
        string TenkiKBN { get; set; } = string.Empty;               //43 転帰区分 数値 1 △

        string HonkeSyubetuCode { get; set; } = string.Empty;       //44. 本家種別コード 数値 1 ●

        string KozaNo { get; set; } = string.Empty;                 //45 口座番号 数値 8 △
        string KozaSyubetu { get; set; } = string.Empty;            //46 口座種別 数値 1 △

        string DrName { get; set; } = string.Empty;					//47 柔整師名 文字 24 △  柔整レセ：レセプト本家種別コード ※区分の内訳は区分一覧参照

        string BankName { get; set; } = string.Empty;           	//48 銀行名 文字 20 －  柔整レセ：口座番号　※口座種別とセット　※個人払で支払先コードが未設定の場合は必須
        string BankBranch { get; set; } = string.Empty;             //49 支店名 文字 20 －  柔整レセ：口座種別　※口座番号とセット ※区分の内訳は区分一覧参照　※個人払で支払先コードが未設定の場合は必須
        string BankAccountName { get; set; } = string.Empty;        //50 口座名義人 文字 24 －  柔整レセ：柔整師名（漢字）　※全角12文字以内


        //ソート
        string SortKeyNaibuRenban2 { get; set; } = string.Empty;
        string SortKeyData { get; set; } = string.Empty;



        List<string> imagePahts = new List<string>();

        int changedJyushinKubun => jyushinKubun == 8 ? 1 :
            jyushinKubun == 0 ? 6 :
            jyushinKubun == 4 ? 5 : 0;

        int kyushuKubun => appType == APP_TYPE.あんま ? 4 :
            appType == APP_TYPE.鍼灸 ? 5 : 3;


        #endregion




        /// <summary>
        /// データ出力2022年以降
        /// </summary>
        /// <param name="cym">処理年月</param>
        /// <returns></returns>
        public static bool DoExport2022(int cym)
        {
            
            string fileName;            
            string strDIR_image;
            string strOutputPath;


            //以前からの納品データ出力処理を移動してきた
            if (!Kagome.Export.BankAccountExport(cym, out strOutputPath)) return false;

            strOutputPath = $"{strOutputPath}\\HiPROS";           
            strDIR_image = $"{strOutputPath}\\KG{cym}";            
            fileName = $"{strOutputPath}\\KJ.DATA";

            if (!System.IO.Directory.Exists(strDIR_image)) System.IO.Directory.CreateDirectory(strDIR_image);

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データを取得しています");
                var list = App.GetApps(cym);
                list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

                
                //15.内部連番　用変数　1レコードごとの連番7桁
                long tmpNaibuRenban15 = 900000;
                //16.レセプト番号                
                int rezeptNumber = 90000;


                string tmpKeyNaibuRenban2curr = string.Empty;
                string tmpKeyNaibuRenban2 = string.Empty;
         
                var all = new List<Export2022>();
                
                for (int i = 0; i < list.Count; i++)
                {

                    var item = list[i];
                    if (item.MediYear <= 0) continue;

                    //20220618153233 furukawa st ////////////////////////
                    //支払保留は納品しないので飛ばす                    
                    if (item.StatusFlagCheck(StatusFlag.支払保留)) continue;
                    //20220618153233 furukawa ed ////////////////////////


                    //出力用クラス
                    var Export2022 = new Export2022(item, cym, ref rezeptNumber, ref tmpNaibuRenban15);



                    //2.内部連番付与用ソート
                    //45口座番号 
                    //19記号 
                    //20番号
                    //17診療年月

                    Export2022.SortKeyNaibuRenban2 =
                        Export2022.KozaNo +
                        Export2022.syo_mark + Export2022.syo_number +
                        Export2022.mediYM;


                    //データソート用キー
                    Export2022.SortKeyData =
                        Export2022.KozaNo +
                        Export2022.syo_mark + Export2022.syo_number +
                        Export2022.mediYM;


                    ////2.内部連番付与用ソート
                    ////組合コード 
                    ////本支部コード 
                    ////本家区分 
                    ////請求形態区分 
                    ////請求形態番号 
                    ////請求年月 
                    ////府県コード 
                    ////給種区分 
                    ////診療区分 
                    ////レセプト番号 

                    //Export2022.SortKeyNaibuRenban2 = Export2022.KumiaiCode + Export2022.HonSibuCode +
                    //    Export2022.honkeKubun.ToString() + Export2022.SeikyukeitaiKBN +
                    //    Export2022.SeikyuKeitaiBango + Export2022.chargeYM +
                    //    Export2022.FukenCode + Export2022.KyusyuKBN + Export2022.SinryoKBN;


                    ////データソート用キー
                    //Export2022.SortKeyData = Export2022.KumiaiCode + Export2022.HonSibuCode +
                    //   Export2022.honkeKubun.ToString() + Export2022.SeikyukeitaiKBN +
                    //   Export2022.SeikyuKeitaiBango + Export2022.chargeYM +
                    //   Export2022.FukenCode + Export2022.KyusyuKBN + Export2022.SinryoKBN + Export2022.numbering.ToString("000000");

                    all.Add(Export2022);


                }



                //2.内部連番用
                int tmpNaibuRenban2 = 1;

                //2.内部連番付与用ソート
                all.Sort((x, y) => (int)x.SortKeyNaibuRenban2.CompareTo(y.SortKeyNaibuRenban2));
                for (int r = 0; r < all.Count; r++)
                {
                    //キーが違ったら内部連番足す
                    if (r > 0 && (all[r].SortKeyNaibuRenban2 != all[r - 1].SortKeyNaibuRenban2))

                    {
                        tmpNaibuRenban2++; 
                    }

                    tmpNaibuRenban15++;
                    rezeptNumber++;
                    all[r].NaibuRenban15 = tmpNaibuRenban15.ToString().PadLeft(7,'0');
                    all[r].numbering = rezeptNumber;

                    all[r].NaibuRenban2 = tmpNaibuRenban2.ToString("00000");    //2 '内部連番 数値 5  
                    all[r].Bango = "000001";                   //12 番号 数値 6  1固定
                    //all[r].Bango = "1" + all[r].NaibuRenban2;                   //12 番号 数値 6
                }

                //出力データ用最終ソート
                all.Sort((x, y) => (int)x.SortKeyData.CompareTo(y.SortKeyData));



                wf.LogPrint("データを出力しています");
                //データ出力
                StreamWriter sw = new StreamWriter(fileName,false,System.Text.Encoding.GetEncoding("shift-jis"));
                writeLine(sw, all);
                sw.Close();
           
                wf.LogPrint("画像を出力しています");
                //画像出力
                var fc = new TiffUtility.FastCopy();
                

                wf.SetMax(all.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                foreach (var item in all)
                {
                    TiffUtility.MargeOrCopyTiff(fc, item.imagePahts, strDIR_image + "\\000"+item.numbering.ToString("000000") + ".tif");
                    wf.InvokeValue++;
                }
            }

            return true;
        }



        #region データ作成



        //データ作成　
        private Export2022(App app, int cym, ref int rezeptNumber, ref long tmpNaibuRenban15)
        
        {
            this.app = app;

            //20220712112900 furukawa st ////////////////////////
            //処理月＝メホール年月＋１            
            chargeYM = DateTimeEx.Int6YmAddMonth(cym,1).ToString() + "01";    //1 処理月 数値 8   ※ yyyymm01　形式で格納 メホールの処理月+1月が正解　2022/7/8福田
            //  chargeYM = cym.ToString() + "01"; //app.ChargeYear.ToString() + app.ChargeMonth.ToString("00")+"01";      //1 処理月 数値 8   ※ yyyymm01　形式で格納
            //20220712112900 furukawa ed ////////////////////////
            
            
            //2 '内部連番 数値 5   各処理月毎に発番して下さい　
            //⇒　２．内部連番（綴票内部連番）NO３（組合コード）～NO１１（診療区分）単位に発番します。
            //全体レコードソート後につけることにした
            
            
            //20220712112808 furukawa st ////////////////////////
            //組合コード57500            
            KumiaiCode = "57500";                              //3 組合コード 数値 5   組合コード　57500固定　2022/7/8福田
            //KumiaiCode = "";                              //3 組合コード 数値 5   組合コード　何も入れない
            //20220712112808 furukawa ed ////////////////////////

            HonSibuCode = "";                             //4 本支部コード 数値 2 △  何も入れない

            //20220712113118 furukawa st ////////////////////////
            //本家区分間違い
            
            //5 本家区分 数値 1  2022/7/8福田
            if (app.Family > 100) honkeKubun = app.Family % 100;
            else honkeKubun = app.Family;
            //  honkeKubun = app.Family / 100;                //5 本家区分 数値 1   柔整 ：レセプト本家種別コード ※区分の内訳は区分一覧参照
            //20220712113118 furukawa ed ////////////////////////


            SeikyukeitaiKBN = "4";                        //6 請求形態区分 数値 1   柔整 ：請求形態区分　※4固定にして下さい                      
            SeikyuKeitaiBango = "";                       //7 請求形態番号 数値 4 △  柔整 ：請求形態番号　※団体のコードを設定して下さい
                                                          //※支払時取纏区分が団体(1)の場合は必須。個人(2)の場合は入力不要
            SeikyuNengetu = chargeYM;                   //8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　

            //9 府県コード 数値 2 △  柔整 ：府県コード　※省略時は'0'を設定して下さい
            
            //20220712113009 furukawa st ////////////////////////
            //府県コード00            
            FukenCode = "00";         //00固定 2022/7/8福田
            //      FukenCode =  string.Empty ;         //柔整師登録番号の2,3桁目(0始まりなので1と2)
            //      FukenCode = app.DrNum == string.Empty ? "00" : app.DrNum.Trim();         //柔整師登録番号の2,3桁目(0始まりなので1と2)
            //20220712113009 furukawa ed ////////////////////////

            //10 給種区分 数値 1   柔整 ：給種区分　※3=柔整　4=按摩　5=針灸
            switch (app.AppType)
            {
                case APP_TYPE.あんま:
                    KyusyuKBN = 4;
                    break;

                case APP_TYPE.鍼灸:
                    KyusyuKBN = 5;
                    break;

                default:
                    KyusyuKBN = 3;
                    break;
            }


            SinryoKBN = "2"; //11 診療区分 数値 1   柔整 ：診療区分　※2固定にして下さい
            Bango = "1";        //12 番号 数値 6   柔整 ：各処理月毎に発番して下さい　⇒　1固定

            SiharaijiTorimatomeKbn = "2";// SiharaijiTorimatomeKbn.ToString();                 //13 請求時取纏区分 数値 1   柔整 ：請求時取纏区分　※1=団体　2=個人　⇒　両方ともに「１：団体」を設定してください。
            SeikyujiTorimatomeKbn = "2";// SeikyujiTorimatomeKbn.ToString();                   //14 支払時取纏区分 数値 1   柔整 ：支払時取纏区分　※1=団体　2=個人⇒　両方ともに「１：団体」を設定してください。

            //ループの外でやる
            //tmpNaibuRenban15++;
            //NaibuRenban15 = tmpNaibuRenban15.ToString().PadLeft(7, '0');  //15 内部連番 数値 7   パンチデータファイル毎に1から発番して下さい

            //rezeptNumber++;                                               //16 レセプト番号 数値 6   柔整レセ：レセプト番号 → NO１６（レセプト番号）　：「７００００１」から連番でお願いします。            
            //numbering = rezeptNumber;

            mediYM = app.YM.ToString() + "01";                            //17 診療年月 数値 8   柔整レセ：診療年月　※ yyyymm01 形式で格納            



            //省略時は0埋めでなく値をセットしない
            SiharaisakiCode = app.DrNum==string.Empty? string.Empty : app.DrNum.Substring(1,7);
            //SiharaisakiCode = "";                                         //18 支払先コード 数値 7 △  柔整レセ：柔整師コード　※個人払(支払時取纏区分=2)の場合、補筆された7桁の数字をパンチ
            
            
            //支払時取り纏め区分が１固定のため、こちらも省略

            syo_mark = app.HihoNum.Split('-')[0];          //19 記号 数値 5   柔整レセ：記号
            syo_number = app.HihoNum.Split('-')[1];        //20 番号 数値 8   柔整レセ：番号
            birth = app.Birthday;                          //21 生年月日 数値 8   柔整レセ：生年月日　※ yyyymmdd 形式で格納　月日ブランクは0000            
            sex = app.Sex;                                 //22 性別 数値 1   柔整レセ：性別


            //省略時は0埋めでなく値をセットしない
            //疾病コード全省略
            sickCode1 = ""; //23 疾病コード1 数値 4 －  柔整レセ：疾病コード（１）
            sickCode2 = ""; //24 疾病コード2 数値 4 －  柔整レセ：疾病コード（２）
            sickCode3 = ""; //25 疾病コード3 数値 4 －  柔整レセ：疾病コード（３）
            sickCode4 = ""; //26 疾病コード4 数値 4 －  柔整レセ：疾病コード（４）

            startDate = app.FushoStartDate1;   //27 施術開始日 数値 8   柔整レセ：施術開始日　※ yyyymmdd 形式で格納

            //endDate =  app.FushoFinishDate1;                                                 //28 施術終了日 数値 8 △  柔整レセ：施術終了日　※ yyyymmdd 形式で格納            
            strEndDate = "";
            
            //省略
            dtDoui = "";            //29 同意年月日 数値 8 －  柔整レセ：同意年月日　※ yyyymmdd 形式で格納
            dtUketuke = "";         //30 受付年月日 数値 8 －  柔整レセ：35 未定義　※ yyyymmdd 形式で格納

           
            days = app.CountedDays;          //31 施術日数 数値 3   柔整レセ：施術日数
            total = app.Total;               //32 合計金額 数値 9   柔整レセ：合計金額
            
            //省略
            ItibuFutanKin = string.Empty;              //33 一部負担金 数値 8 －  柔整レセ：一部負担金
            SejutuKaisu = string.Empty;                //34 施術回数 数値 3 －  柔整レセ：施術回数
            SeikyuGaku = app.Charge.ToString();                 //35 請求額 数値 8 －  柔整レセ：請求額
            KohiCode1 = string.Empty;		           //36 公費コード1 数値 2 △  柔整レセ：公費コード（１）
            KohiCode2 = string.Empty;                  //37 公費コード2 数値 2 △  柔整レセ：公費コード（２）
            KohiKingaku1 = string.Empty;               //38 公費金額1 数値 9 △  柔整レセ：公費金額（１）
            KohiKingaku2 = string.Empty;               //39 公費金額2 数値 9 △  柔整レセ：公費金額（２）
            Kanjafutangaku_Kohi1 = string.Empty;       //40 患者負担額公費1 数値 8 △  柔整レセ：患者負担額（第１公費分）
            Kanjafutangaku_Kohi2 = string.Empty;       //41 患者負担額公費2 数値 8 △  柔整レセ：患者負担額（第２公費分）
            MaruFuKBN = string.Empty;                  //42 マル不区分 数値 1 △  柔整レセ：マル不区分 ※1=不１：ｼｽﾃﾑ公費:90or98　2=不２：業務上　3=不３：第三
            TenkiKBN = string.Empty;                   //43 転帰区分 数値 1 △  柔整レセ：転帰区分　※1=下記以外　2=治癒　3=死亡　4=中止


            //20220712113200 furukawa st ////////////////////////
            //本家種別コード間違い
            
            HonkeSyubetuCode = app.Family.ToString().Substring(app.Family.ToString().Length-1,1) == "1" ? "2" : "6";   //44 本家種別コード 数値 1  本人＝２，家族＝６                
            //  HonkeSyubetuCode = (app.Family % 100) == 2 ? "2" : "6";   //44 本家種別コード 数値 1  本人＝２，家族＝６      
            //20220712113200 furukawa ed ////////////////////////


            KozaNo = string.Empty;//app.AccountNumber;                         //45 口座番号 数値 8 △  
            KozaSyubetu = string.Empty;// app.BankType == 0 ? "1" : app.BankType.ToString();                         //46 口座種別 数値 1 △    


            DrName = app.DrName;                                //47 柔整師名 文字 24 △ 
            //省略
            BankName = string.Empty;// app.BankName;                            //48 銀行名 文字 20 － 
            BankBranch = string.Empty;// app.BankBranch;                        //49 支店名 文字 20 －  
            //string strBankAccountName = app.BankName == string.Empty ? string.Empty : Microsoft.VisualBasic.Strings.StrConv(app.BankName, Microsoft.VisualBasic.VbStrConv.Wide);
            BankAccountName = string.Empty;// strBankAccountName.Substring(0,12);                  //50 口座名義人 文字 24


            imagePahts.Add(app.GetImageFullPath());                 //画像パス

        }


        /// <summary>
        /// CSV（固定長）ファイル書き込み
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        private static bool writeLine(StreamWriter sw, List<Export2022> l)
        {
            if (l.Count == 0) return true;

            try
            {
                foreach (var item in l)
                {
                    if (!item.writeReceLine(sw)) return false;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        #endregion

        #region テキスト書込
        private bool writeReceLine(StreamWriter sw)
        {
            try
            {
                var sb = new StringBuilder();


                sb.Append(chargeYM + ",");                          //1 処理月 数値 8   ※ yyyymm01　形式で格納                
                sb.Append(NaibuRenban2 + ",");				        //2 '内部連番 数値 5   各処理月毎に発番して下さい
                sb.Append(KumiaiCode + ",");				        //3 組合コード 数値 5   柔整 ：組合コード　※何も入れない
                sb.Append(HonSibuCode + ",");				        //4 本支部コード 数値 2 △  柔整 ：※何も入れない
                sb.Append(honkeKubun + ",");				        //5 本家区分 数値 1   柔整 ：レセプト本家種別コード ※区分の内訳は区分一覧参照
                sb.Append(SeikyukeitaiKBN + ",");			        //6 請求形態区分 数値 1   柔整 ：請求形態区分　※4固定にして下さい
                sb.Append(SeikyuKeitaiBango + ",");                 //7 請求形態番号 数値 4 △  柔整 ：請求形態番号　※団体のコードを設定して下さい　※支払時取纏区分が団体(1)の場合は必須。個人(2)の場合は入力不要                        
                sb.Append(SeikyuNengetu + ",");				        //8 請求年月 数値 8 △  柔整 ：請求年月　※ yyyymm01 形式で格納　※省略時は取込にて自動設定します
                sb.Append(FukenCode + ",");                         //9 府県コード 数値 2 △  柔整 ：府県コード　※省略時は'0'を設定して下さい
                sb.Append(KyusyuKBN + ",");		    		        //10 給種区分 数値 1   柔整 ：給種区分　※3=柔整　4=按摩　5=針灸
                sb.Append(SinryoKBN + ",");                         //11 診療区分 数値 1   柔整 ：診療区分　※2固定にして下さい                
                sb.Append(Bango + ",");					            //12 番号 数値 6   柔整 ：1固定
                sb.Append(SiharaijiTorimatomeKbn + ",");	        //13 請求時取纏区分 数値 1   柔整 ：請求時取纏区分　※1=団体　2=個人
                sb.Append(SeikyujiTorimatomeKbn + ",");             //14 支払時取纏区分 数値 1   柔整 ：支払時取纏区分　※1=団体　2=個人                
                sb.Append(NaibuRenban15 + ",");                     //15 内部連番 数値 7   パンチデータファイル毎に1から発番して下さい                
                sb.Append(numbering.ToString("000000") + ",");		//16 レセプト番号 数値 6   柔整レセ：レセプト番号
                sb.Append(mediYM + ",");					        //17 診療年月 数値 8   柔整レセ：診療年月　※ yyyymm01 形式で格納
                sb.Append(SiharaisakiCode + ",");                   //18 支払先コード 数値 7 △  柔整レセ：柔整師コード　※個人払(支払時取纏区分=2)の場合、補筆された7桁の数字をパンチ
                sb.Append(syo_mark.PadLeft(5, '0') + ",");			//19 記号 数値 5   柔整レセ：記号
                sb.Append(syo_number.PadLeft(8, '0') + ",");		//20 番号 数値 8   柔整レセ：番号                
                sb.Append(birth.ToString("yyyyMMdd") + ",");		//21 生年月日 数値 8   柔整レセ：生年月日　※ yyyymmdd 形式で格納　月日ブランクは0000


                sb.Append(sex + ",");						    //22 性別 数値 1   柔整レセ：性別
                sb.Append(sickCode1 + ",");					//23 疾病コード1 数値 4 －  柔整レセ：疾病コード（１）
                sb.Append(sickCode2 + ",");					//24 疾病コード2 数値 4 －  柔整レセ：疾病コード（２）
                sb.Append(sickCode3 + ",");					//25 疾病コード3 数値 4 －  柔整レセ：疾病コード（３）
                sb.Append(sickCode4 + ",");                    //26 疾病コード4 数値 4 －  柔整レセ：疾病コード（４）

                sb.Append(startDate.ToString("yyyyMMdd") + ",");				//27 施術開始日 数値 8   柔整レセ：施術開始日　※ yyyymmdd 形式で格納
                //sb.Append(endDate.ToString("yyyyMMdd") + ",");                  //28 施術終了日 数値 8 △  柔整レセ：施術終了日　※ yyyymmdd 形式で格納
                sb.Append(strEndDate + ",");                  //28 施術終了日 数値 8 △  柔整レセ：施術終了日　※ yyyymmdd 形式で格納

                sb.Append(dtDoui + ",");					    //29 同意年月日 数値 8 －  柔整レセ：同意年月日　※ yyyymmdd 形式で格納
                sb.Append(dtUketuke + ",");                    //30 受付年月日 数値 8 －  柔整レセ：35 未定義　※ yyyymmdd 形式で格納

                sb.Append(days.ToString("000") + ",");                     //31 施術日数 数値 3   柔整レセ：施術日数
                sb.Append(total.ToString("000000000") + ",");             //32 合計金額 数値 9   柔整レセ：合計金額


                sb.Append(ItibuFutanKin + ",");					//33 一部負担金 数値 8 －  柔整レセ：一部負担金
                sb.Append(SejutuKaisu + ",");					    //34 施術回数 数値 3 －  柔整レセ：施術回数
                sb.Append(SeikyuGaku.PadLeft(8,'0') + ",");                      //35 請求額 数値 8 －  柔整レセ：請求額

                sb.Append(KohiCode1 + ",");				//36 公費コード1 数値 2 △  柔整レセ：公費コード（１）
                sb.Append(KohiCode2 + ",");				//37 公費コード2 数値 2 △  柔整レセ：公費コード（２）
                sb.Append(KohiKingaku1 + ",");				//38 公費金額1 数値 9 △  柔整レセ：公費金額（１）
                sb.Append(KohiKingaku2 + ",");				//39 公費金額2 数値 9 △  柔整レセ：公費金額（２）
                sb.Append(Kanjafutangaku_Kohi1 + ",");		//40 患者負担額公費1 数値 8 △  柔整レセ：患者負担額（第１公費分）
                sb.Append(Kanjafutangaku_Kohi2 + ",");     //41 患者負担額公費2 数値 8 △  柔整レセ：患者負担額（第２公費分）
                sb.Append(MaruFuKBN + ",");				//42 マル不区分 数値 1 △  柔整レセ：マル不区分 ※1=不１：ｼｽﾃﾑ公費:90or98　2=不２：業務上　3=不３：第三
                sb.Append(TenkiKBN + ",");                 //43 転帰区分 数値 1 △  柔整レセ：転帰区分　※1=下記以外　2=治癒　3=死亡　4=中止

                sb.Append(HonkeSyubetuCode + ",");                    //44 本家種別コード 数値 1 
                sb.Append(KozaNo + ",");					        //45 口座番号 数値 8 △ 
                sb.Append(KozaSyubetu.ToString() + ",");	        //46 口座種別 数値 1 △                 
                sb.Append(DrName + ",");					        //47 柔整師名 文字 全角12 △ 
                sb.Append(BankName + ",");					        //48 銀行名 文字 全角10 
                sb.Append(BankBranch + ",");				        //49 支店名 文字 全角10 
                sb.Append(BankAccountName);					        //50 口座名義 全角12



                sw.WriteLine(sb.ToString());

            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
        #endregion


    }

    class Export
    {
        /// <summary>
        /// 照会リスト等、バッチNo入りのリストを作成します
        /// </summary>
        /// <returns></returns>
        public static bool ListExport(List<App> list, string fileName, int cym)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var sdic = new Dictionary<int, Scan>();

            foreach (var item in scans) sdic.Add(item, Scan.Select(item));

            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    sw.WriteLine(
                        "AID,No.,通知番号,処理年,処理月,被保険者番号,被保険者名,受療者名," +
                        "施術年,施術月,合計金額,請求金額,施術日数,負傷数,新規継続,本家," +
                        "受領委任者氏名,施術所名,照会理由,性別,生年月日,年齢,〒,住所,口座ID" +
                        "点検,照会理由,過誤理由,疑義理由");
                    var ss = new List<string>();

                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    //var jcy = DateTimeEx.GetHsYearFromAd(cym / 100).ToString("00");
                    var jcm = (cym % 100).ToString("00");
                    var jcy = DateTimeEx.GetHsYearFromAd(cym / 100, int.Parse(jcm)).ToString("00");
                    //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    var notifyNumber = $"{jcy}{jcm}-";
                    var count = 1;

                    Func<int, string> toJYear = jyy =>
                    {
                        if (jyy > 100) return jyy.ToString().Substring(1, 2);
                        return "00";
                    };

                    Func<string, string> toShowHzip = hzip =>
                    {
                        if (string.IsNullOrWhiteSpace(hzip)) return "";
                        if (hzip.Length != 7) return "";
                        return $"{hzip.Substring(0, 3)}-{hzip.Substring(3, 4)}";
                    };

                    Func<App, string> getFushoCount = app =>
                    {
                        var c = 0;
                        if (app.FushoName1 != "") c++;
                        if (app.FushoName2 != "") c++;
                        if (app.FushoName3 != "") c++;
                        if (app.FushoName4 != "") c++;
                        if (app.FushoName5 != "") c++;
                        return c.ToString();
                    };

                    foreach (var item in list)
                    {
                        //AID,No.,通知番号,処理年,処理月,被保険者番号,被保険者名,受療者名,
                        ss.Add(item.Aid.ToString());
                        ss.Add(count.ToString());
                        ss.Add(notifyNumber + count.ToString("000"));
                        ss.Add(jcy);
                        ss.Add(jcm);
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);

                        //施術年,施術月,合計金額,請求金額,施術日数,負傷数,新規継続,本家,
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(getFushoCount(item));
                        ss.Add(item.NewContType.ToString());
                        ss.Add(sdic[item.ScanID].Note1);                    //note1
                        
                        //受領委任者氏名,施術所名,照会理由,性別,生年月日,年齢,〒,住所,口座ID
                        ss.Add(item.AccountName);
                        ss.Add(item.ClinicName);
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add(((SEX)item.Sex).ToString());
                        ss.Add(item.Birthday.ToString("yyyy/MM/dd"));
                        ss.Add(DateTimeEx.GetAge(item.Birthday, DateTime.Today).ToString());
                        ss.Add(toShowHzip(item.HihoZip));
                        ss.Add(item.HihoAdd.ToString());
                        ss.Add(item.AccountNumber.ToString());

                        //点検,過誤理由,疑義理由
                        ss.Add(item.InspectInfo);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        //20220607183809 furukawa st ////////////////////////
        //出力先を引き継ぐため引数追加        
        public static bool BankAccountExport(int cym, out string strOutputPath)
        //      public static bool BankAccountExport(int cym)
        //20220607183809 furukawa ed ////////////////////////

        {
            string fn = string.Empty;
            string memoHon, memoKaz, logFile;
            
            strOutputPath = string.Empty;

            using (var f = new SaveFileDialog())
            {
                f.FileName = $"AJS{cym}.csv";
                f.Filter = "CSVデータ|*.csv";
                
                if (f.ShowDialog() != DialogResult.OK) return false;
                //if (f.ShowDialog() != DialogResult.OK) return true;

                fn = f.FileName;
                memoHon = $"{System.IO.Path.GetDirectoryName(fn)}\\メモ本人{cym}.csv";
                memoKaz = $"{System.IO.Path.GetDirectoryName(fn)}\\メモ家族{cym}.csv";
                logFile = $"{System.IO.Path.GetDirectoryName(fn)}\\ExportLog{DateTime.Now.ToString("yyyyMMddHHmmss")}.log";

                //20220607183853 furukawa st ////////////////////////
                //出力先を引き継ぐ                
                strOutputPath = System.IO.Path.GetDirectoryName(fn);
                //20220607183853 furukawa ed ////////////////////////

            }

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                wf.LogPrint("スキャン情報を取得中です");
                var ss = Scan.GetScanListYM(cym);
                if(!ss.All(s => s.Note1.Contains("本人") || s.Note1.Contains("家族")))
                {
                    throw new Exception("本人/家族の記述がないスキャンがあります");
                }

                wf.LogPrint("申請書を取得中です");
                var l = App.GetApps(cym);
                l.RemoveAll(x => x.StatusFlagCheck(StatusFlag.支払保留));

                wf.LogPrint("AJSファイルの集計をしています");
                var g = l.FindAll(x => x.YM > 0)
                    .GroupBy(x => new { x.AccountNumber, x.AccountName })
                    .OrderBy(x => x.First().AccountNumber);

                wf.LogPrint("出力中です");
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.SetMax(l.Count);

                //AJS
                using (var sw = new System.IO.StreamWriter(fn, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine("口座番号ID,件数,振込金額,※口座名義");

                    foreach (var item in g)
                    {
                        wf.InvokeValue++;
                        var apps = item.ToList();
                        sw.WriteLine($"{apps.First().AccountNumber},{apps.Count},{apps.Sum(x => x.Charge)},{apps.First().AccountName}");
                    }
                    wf.InvokeValue++;
                }


                //本人
                using (var sw = new System.IO.StreamWriter(memoHon, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    var hl = l.FindAll(x => x.AppType == APP_TYPE.柔整 
                        && ss.Find( s => s.SID == x.ScanID).Note1.Contains("本人"))
                        .GroupBy(x => new { x.AccountNumber, x.AccountName })
                        .OrderBy(x => x.First().AccountNumber);
                    sw.WriteLine("本人-柔整");
                    sw.WriteLine("被保記号,空白(事業所名),被保険者氏名,支給額,受療者氏名,小計,件数");

                    foreach (var item in hl)
                    {
                        var apps = item.ToList();
                        sw.WriteLine($"{apps[0].HihoNum.Split('-')[0]},,{apps[0].HihoName}," +
                            $"{apps[0].Charge},{apps[0].AccountName},{apps.Sum(x => x.Charge)},{apps.Count}");

                        for (int i = 1; i < apps.Count; i++)
                        {
                            sw.WriteLine($"{apps[i].HihoNum.Split('-')[0]},,{apps[i].HihoName},{apps[i].Charge},〃,,");
                        }
                    }

                    hl = l.FindAll(x => (x.AppType == APP_TYPE.鍼灸 || x.AppType == APP_TYPE.あんま) 
                        && ss.Find(s => s.SID == x.ScanID).Note1.Contains("本人"))
                        .GroupBy(x => new { x.AccountNumber, x.AccountName })
                        .OrderBy(x => x.First().AccountNumber);
                    sw.WriteLine("\r\n本人-あはき");
                    sw.WriteLine("被保記号,空白(事業所名),被保険者氏名,支給額,受療者氏名,小計,件数");

                    foreach (var item in hl)
                    {
                        var apps = item.ToList();
                        sw.WriteLine($"{apps[0].HihoNum.Split('-')[0]},,{apps[0].HihoName}," +
                            $"{apps[0].Charge},{apps[0].AccountName},{apps.Sum(x => x.Charge)},{apps.Count}");

                        for (int i = 1; i < apps.Count; i++)
                        {
                            sw.WriteLine($"{apps[i].HihoNum.Split('-')[0]},,{apps[i].HihoName},{apps[i].Charge},〃,,");
                        }
                    }
                }

                //家族
                using (var sw = new System.IO.StreamWriter(memoKaz, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    var hl = l.FindAll(x => x.AppType == APP_TYPE.柔整
                        && ss.Find(s => s.SID == x.ScanID).Note1.Contains("家族"))
                        .GroupBy(x => new { x.AccountNumber, x.AccountName })
                        .OrderBy(x => x.First().AccountNumber);
                    sw.WriteLine("家族-柔整");
                    sw.WriteLine("被保記号,空白(事業所名),被保険者氏名,支給額,受療者氏名,小計,件数");

                    foreach (var item in hl)
                    {
                        var apps = item.ToList();
                        sw.WriteLine($"{apps[0].HihoNum.Split('-')[0]},,{apps[0].HihoName}," +
                            $"{apps[0].Charge},{apps[0].AccountName},{apps.Sum(x => x.Charge)},{apps.Count}");

                        for (int i = 1; i < apps.Count; i++)
                        {
                            sw.WriteLine($"{apps[i].HihoNum.Split('-')[0]},,{apps[i].HihoName},{apps[i].Charge},〃,,");
                        }
                    }

                    hl = l.FindAll(x => (x.AppType == APP_TYPE.鍼灸 || x.AppType == APP_TYPE.あんま)
                        && ss.Find(s => s.SID == x.ScanID).Note1.Contains("家族"))
                        .GroupBy(x => new { x.AccountNumber, x.AccountName })
                        .OrderBy(x => x.First().AccountNumber);
                    sw.WriteLine("\r\n家族-あはき");
                    sw.WriteLine("被保記号,空白(事業所名),被保険者氏名,支給額,受療者氏名,小計,件数");

                    foreach (var item in hl)
                    {
                        var apps = item.ToList();
                        sw.WriteLine($"{apps[0].HihoNum.Split('-')[0]},,{apps[0].HihoName}," +
                            $"{apps[0].Charge},{apps[0].AccountName},{apps.Sum(x => x.Charge)},{apps.Count}");

                        for (int i = 1; i < apps.Count; i++)
                        {
                            sw.WriteLine($"{apps[i].HihoNum.Split('-')[0]},,{apps[i].HihoName},{apps[i].Charge},〃,,");
                        }
                    }
                }
                
                wf.LogPrint("出力が正常に終了しました");
                return true;
            }
            catch(Exception ex)
            {
                wf.LogPrint("エラーが発生しました。:"+ex.Message);
                return false;
            }
            finally
            {
                wf.LogSave(logFile);
                wf.Dispose();
            }
        }
    }
}
