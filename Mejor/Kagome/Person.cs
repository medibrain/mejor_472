﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.Kagome
{
    class Person
    {
        [DB.DbAttribute.Serial]
        [DB.DbAttribute.PrimaryKey]
        public int ID { get; private set; } = 0;
        public string HihoNum { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public bool Verified { get; private set; } = false;
        public int LastAid { get; set; } = 0;

        public static string GetVerifiedName(string hihoNum)
        {
            var p = DB.Main.Select<Person>(new { hihoNum = hihoNum, verified = true });
            return p.SingleOrDefault()?.Name ?? string.Empty;
        }

        public static Person Select(string hihoNum)
        {
            var p = DB.Main.Select<Person>(new { hihoNum = hihoNum, verified = true });
            return p.SingleOrDefault();
        }

        public static bool Upsert(App app, bool forceVeify, DB.Transaction tran)
        {
            var p = DB.Main.Select<Person>(new { hihoNum = app.HihoNum }).SingleOrDefault();
            if (p == null) p = new Person();

            //ベリファイ済み、かつ名前が変わっていないときは更新なし
            if (p.Verified && p.Name == app.HihoName) return true;

            if (forceVeify) p.Verified = true;
            else if (p.Name == app.HihoName && p.LastAid != app.Aid) p.Verified = true;
            else if (p.Name != app.HihoName) p.Verified = false;

            p.Name = app.HihoName;
            p.HihoNum = app.HihoNum;
            p.LastAid = app.Aid;

            if (p.ID == 0) return DB.Main.Insert(p, tran);
            else return DB.Main.Update(p, tran);
        }


    }
}
