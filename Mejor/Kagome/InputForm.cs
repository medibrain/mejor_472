﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Kagome
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(100, 800);
        Point posCost = new Point(800, 1800);
        Point posDays = new Point(400, 800);
        Point posNewCont = new Point(400, 800);
        Point posOryo = new Point(400, 800);
        Point posBank = new Point(800, 2000);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls,newContControls, oryoControls, bankControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hihoNumControls = new Control[] { verifyBoxNumM, verifyBoxNumN,  };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge,  };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            newContControls = new Control[] { verifyBoxNewCont };
            oryoControls = new Control[] { verifyCheckBoxOryo };
            bankControls = new Control[] { verifyBoxAccountNum, verifyBoxAccountName };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (bankControls.Contains(t)) scrollPictureControl1.ScrollPosition = posBank;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号チェック 1文字以上かつ数字に直せること
            int numM = verifyBoxNumM.GetIntValue();
            setStatus(verifyBoxNumM, numM < 1);
            int numN = verifyBoxNumN.GetIntValue();
            setStatus(verifyBoxNumN, numN < 1);


            //20220607114330 furukawa st ////////////////////////
            //納品データ改修に伴う本家区分欄の追加
            
            int juryoType = verifyBoxJuryoType.GetIntValue();
            setStatus(verifyBoxJuryoType, !new int[] { 2, 4, 6, 8, 0 }.Contains(juryoType));
            //20220607114330 furukawa ed ////////////////////////


            //被保険者名
            setStatus(verifyBoxName, verifyBoxName.Text.Trim().Length < 3);

            //性別のチェック
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日のチェック
            var birth = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額
            bool payError =
                ((total * 70 / 100) != charge && (total * 80 / 100) != charge && (total * 90 / 100) != charge);

            //20220607114553 furukawa st ////////////////////////
            //納品データ改修に伴う開始日追加
            
            DateTime dtStart1 = dateCheck(verifyBoxY, verifyBoxM, verifyBoxF1D.GetIntValue());
            setStatus(verifyBoxF1D, dtStart1 == DateTime.MinValue);
            //20220607114553 furukawa ed ////////////////////////

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //新規継続
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //口座番号
            setStatus(verifyBoxAccountNum, verifyBoxAccountNum.Text.Trim().Length < 2);

            //口座名義
            setStatus(verifyBoxAccountName, verifyBoxAccountName.Text.Trim().Length < 3);

            //20220609140850 furukawa st ////////////////////////
            //納品データ仕様変更に伴い柔整師登録記号番号追加
            
            //柔整師登録記号番号
            string strDrCode = verifyBoxDrCode.Text.Trim();
            setStatus(verifyBoxDrCode, strDrCode.Length < 7);
            //20220609140850 furukawa ed ////////////////////////



            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = $"{verifyBoxNumM.Text.Trim()}-{verifyBoxNumN.Text.Trim()}";
            app.HihoName = verifyBoxName.Text.Trim();

            //20220607115208 furukawa st ////////////////////////
            //納品データ改修に伴う本家区分欄の追加による数値化
            
            app.Family = juryoType*100+(scanGroup.note1 == "本人" ? 1 : scanGroup.note1 == "家族" ? 2 : 0);
            //app.Family = scanGroup.note1 == "本人" ? 1 : scanGroup.note1 == "家族" ? 2 : 0;
            //20220607115208 furukawa ed ////////////////////////


            app.Sex = sex;
            app.Birthday = birth;

            //20220607115333 furukawa st ////////////////////////
            //納品データ改修に伴う開始日追加
            
            app.FushoStartDate1 = dtStart1;
            //20220607115333 furukawa ed ////////////////////////


            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;
            app.Partial = total - charge;
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.AccountNumber = verifyBoxAccountNum.Text.Trim();
            app.AccountName = verifyBoxAccountName.Text.Trim();

            app.DrNum = strDrCode;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            
            app.Distance = verifyCheckBoxOryo.Checked ? 999 : 0;
            app.AppType = scan.AppType;

            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if(!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                if (!Person.Upsert(app, !firstTime, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                
                //20211029123444 furukawa st ////////////////////////
                //auxに口座番号を入れておく                
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString(),"",app.AccountNumber.ToString())) return false;
                //  if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;
                //20211029123444 furukawa ed ////////////////////////

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            if (!firstTime)
            {
                setVerify(app);
            }
            else
            {
                //App_Flagのチェック
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setInputedApp(app);
                }
                else if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    //OCRデータがあれば、部位のみ挿入
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setInputedApp(App app)
        {
            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else
            {
                //申請書
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxNumM.Text = app.HihoNum.Split('-')[0];
                verifyBoxNumN.Text = app.HihoNum.Split('-')[1];

                //20220607115948 furukawa st ////////////////////////
                //納品データ改修に伴う本家区分欄
                
                verifyBoxJuryoType.Text = (app.Family / 100).ToString();
                //20220607115948 furukawa ed ////////////////////////


                verifyBoxName.Text = app.HihoName;
                verifyBoxSex.Text = app.Sex.ToString();
                verifyBoxBE.Text = DateTimeEx.GetEraNumber(app.Birthday).ToString();
                verifyBoxBY.Text = DateTimeEx.GetJpYear(app.Birthday).ToString();
                verifyBoxBM.Text = app.Birthday.Month.ToString();
                verifyBoxBD.Text = app.Birthday.Day.ToString();

                //20220607120026 furukawa st ////////////////////////
                //納品データ改修に伴う開始日追加
                
                verifyBoxF1D.Text = app.FushoStartDate1.Day.ToString();
                //20220607120026 furukawa ed ////////////////////////


                verifyBoxDays.Text = app.CountedDays.ToString();
                verifyBoxTotal.Text = app.Total.ToString();
                verifyBoxCharge.Text = app.Charge.ToString();

                verifyBoxAccountNum.Text = app.AccountNumber;
                verifyBoxAccountName.Text = app.AccountName;

                //20220609141104 furukawa st ////////////////////////
                //納品データ仕様変更に伴い柔整師登録記号番号追加
                
                verifyBoxDrCode.Text = app.DrNum;
                //20220609141104 furukawa ed ////////////////////////


                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;

                verifyBoxNewCont.Text = app.NewContType == NEW_CONT.新規 ? "1" : "2";

                verifyCheckBoxOryo.Checked = app.Distance == 999;
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var cs = new Control[] { verifyBoxY, labelYearInfo, labelH, labelY,
                labelInputerName};

            var visible = new Action<bool>(b =>
            {
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is IVerifiable || item is Label)) continue;
                    if (cs.Contains(item)) continue;
                    item.Visible = b;
                }
            });

            if ((verifyBoxY.Text == "--" || verifyBoxY.Text == "++") && verifyBoxY.Text.Length == 2)
            {
                //続紙その他
                visible(false);
            }
            else
            {
                //申請書の場合
                visible(true);
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void verifyBoxAccountNum_Leave(object sender, EventArgs e)
        {
            verifyBoxAccountNum.Text = verifyBoxAccountNum.Text.Trim().PadLeft(7, '0');
            if (verifyBoxAccountName.Text.Length > 0) return;
            int id = verifyBoxAccountNum.GetIntValue();
            verifyBoxAccountName.Text = JuryoMaster.GetName(id);
        }

        private void verifyBoxNumN_Leave(object sender, EventArgs e)
        {
            if (verifyBoxName.Text.Trim() != string.Empty) return;
            var hihoNum = verifyBoxNumM.Text.Trim() + "-" + verifyBoxNumN.Text.Trim();
            verifyBoxName.Text = Person.GetVerifiedName(hihoNum);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (bankControls.Contains(t)) posBank = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setVerify(App app)
        {
            if(!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else
            {
                //申請書
                setVerifyVal(verifyBoxY, app.MediYear, nv);
                setVerifyVal(verifyBoxM, app.MediMonth, nv);
                setVerifyVal(verifyBoxNumM, app.HihoNum.Split('-')[0], nv);
                setVerifyVal(verifyBoxNumN, app.HihoNum.Split('-')[1], nv);

                //20220607185740 furukawa st ////////////////////////
                //納品データ改修に伴う本家区分欄、開始日追加                
                setVerifyVal(verifyBoxJuryoType, app.Family / 100, nv);
                setVerifyVal(verifyBoxF1D, app.FushoStartDate1.Day.ToString(), nv);
                //20220607185740 furukawa ed ////////////////////////

                setVerifyVal(verifyBoxName, app.HihoName, nv);
                setVerifyVal(verifyBoxSex, app.Sex, false);
                setVerifyVal(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), false);
                setVerifyVal(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), false);
                setVerifyVal(verifyBoxBM, app.Birthday.Month, false);
                setVerifyVal(verifyBoxBD, app.Birthday.Day, false);
                setVerifyVal(verifyBoxDays, app.CountedDays, false);
                setVerifyVal(verifyBoxTotal, app.Total, nv);
                setVerifyVal(verifyBoxCharge, app.Charge, nv);
                setVerifyVal(verifyCheckBoxOryo, app.Distance == 999, false);
                setVerifyVal(verifyBoxNewCont, app.NewContType == NEW_CONT.新規 ? "1" : "2", false);
                setVerifyVal(verifyBoxAccountNum, app.AccountNumber, nv);
                setVerifyVal(verifyBoxAccountName, app.AccountName, nv);

                //20220609141041 furukawa st ////////////////////////
                //納品データ仕様変更に伴い柔整師登録記号番号追加
                
                setVerifyVal(verifyBoxDrCode, app.DrNum,nv);
                //20220609141041 furukawa ed ////////////////////////


                setVerifyVal(verifyBoxF1, app.FushoName1, false);
                setVerifyVal(verifyBoxF2, app.FushoName2, false);
                setVerifyVal(verifyBoxF3, app.FushoName3, false);
                setVerifyVal(verifyBoxF4, app.FushoName4, false);
                setVerifyVal(verifyBoxF5, app.FushoName5, false);
            }
            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {

            if (scanGroup.note1 != "本人" && scanGroup.note1 != "家族")
            {
                MessageBox.Show("本人/家族が判別できません。" +
                    "画像取り込みの際「本人」「家族」を明記し、note1に記録されている必要があります。",
                    "本家区分判別エラー",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
