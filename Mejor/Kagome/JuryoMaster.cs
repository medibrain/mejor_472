﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;

namespace Mejor.Kagome
{
    class JuryoMaster
    {
        [DB.DbAttribute.PrimaryKey]
        public int ID { get; private set; }
        [DB.DbAttribute.PrimaryKey]
        public int BankNo { get; private set; }
        [DB.DbAttribute.PrimaryKey]
        public int BranchNo { get; private set; }
        public string Name { get; set; }

        public static bool Import()
        {
            string fileName;
            using (var f = new System.Windows.Forms.OpenFileDialog())
            {
                f.Title = "受療者マスター更新";
                f.Filter = "AJS-xlsファイル|AJS*.xls";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;
                fileName = f.FileName;
            }

            var dbList = DB.Main.SelectAll<JuryoMaster>();
            var dic = new Dictionary<int, List<JuryoMaster>>();
            foreach (var item in dbList)
            {
                if (!dic.ContainsKey(item.ID)) dic.Add(item.ID, new List<JuryoMaster>());
                dic[item.ID].Add(item);
            }


            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            try
            {
                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    var book = new HSSFWorkbook(fs);

                    //20200316160222 furukawa st ////////////////////////
                    //インポートシートの名前を変更
                    
                    var sheet = book.GetSheet("支払先情報");
                    //var sheet = book.GetSheet("マスター登録");
                    //20200316160222 furukawa ed ////////////////////////

                    var rowCount = sheet.LastRowNum;
                    wf.SetMax(rowCount + 2);
                    wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    for (int i = 0; i < rowCount + 2; i++)
                    {
                        wf.InvokeValue++;
                        try
                        {
                            var row = sheet.GetRow(i);
                            if (row.Cells[0].CellType != CellType.Numeric) continue;
                            var j = new JuryoMaster();
                            j.ID = (int)row.Cells[0].NumericCellValue;
                            j.BankNo = (int)row.GetCell(6).NumericCellValue;
                            j.BranchNo = (int)row.GetCell(9).NumericCellValue;
                            j.Name = row.GetCell(1).StringCellValue;

                            if (dic.ContainsKey(j.ID))
                            {
                                var jm = dic[j.ID].FirstOrDefault(x =>
                                    x.BankNo == j.BankNo && x.BranchNo == j.BranchNo);

                                if (jm == null)
                                {
                                    DB.Main.Insert(j);
                                    dic[j.ID].Add(j);
                                }
                                else
                                {
                                    if (jm.Name == j.Name) continue;
                                    jm.Name = j.Name;
                                    DB.Main.Update(j);
                                }
                            }
                            else
                            {
                                DB.Main.Insert(j);
                                dic.Add(j.ID, new List<JuryoMaster>());
                                dic[j.ID].Add(j);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            return true;
        }

        public static string GetName(int accountNum)
        {
            var l = DB.Main.Select<JuryoMaster>(new { id = accountNum });
            return l.Count() == 1 ? l.First().Name : string.Empty;
        }
    }
}
