﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.AichiToshi
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //愛知都市共済用
        const string iNumber = "32230518";

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(0, 750);
        Point posCost = new Point(400, 2000);
        Point posDays = new Point(400, 750);
        Point posNumbering = new Point(300, 0);
        Point posBatch = new Point(200, 600);
        Point posBatchCount = new Point(600, 200);
        Point posBatchDrCode = new Point(200, 900);

        Control[] ymControls, hnumControls, personControls, dayControls, costControls,
            fushoControls, numberControls ;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hnumControls = new Control[] { verifyBoxHnum, verifyBoxHnumM, verifyBoxFamily, };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, };
            fushoControls = new Control[] { verifyBoxF1Y, verifyBoxF1M,
                verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            numberControls = new Control[] { verifyBoxHosNumber, };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //北村さん指示により、鍼灸時、負傷名関係入力不要
            var setEnable = new Action<TextBox, bool>((t, b) =>
                {
                    t.Enabled = b;
                    t.BackColor = b ? SystemColors.Info : SystemColors.Control;
                });
            if(scan.AppType != APP_TYPE.柔整)
            {
                setEnable(verifyBoxF1, false);
                setEnable(verifyBoxF2, false);
                setEnable(verifyBoxF3, false);
                setEnable(verifyBoxF4, false);
                setEnable(verifyBoxF5, false);
            }

            panelHnum.Visible = false;
            panelTotal.Visible = false;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (numberControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();

            //20190505113945 furukawa st ////////////////////////
            //pgdnキーイベントのキーコード間違い

            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            //else if (e.KeyCode == Keys.PageUp) buttonBack.PerformClick();
            //20190505113945 furukawa ed ////////////////////////
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //申請書種別
            int apptype = verifyBoxAppType.GetIntValue();
            setStatus(verifyBoxAppType, apptype != (int)VerifyBox.ERROR_CODE.NULL && apptype != 0 && apptype != 7);

            //都道府県番号
            int pref = verifyBoxPref.GetIntValue();
            setStatus(verifyBoxPref, pref < 1 || 47 < pref);

            //医療機関コード (数字のみ、最大4桁)
            int hosNumber = verifyBoxHosNumber.GetIntValue();
            setStatus(verifyBoxHosNumber, hosNumber < 1 || 9999 < hosNumber);

            //被保険者番号のチェック
            int numM = verifyBoxHnumM.GetIntValue();
            setStatus(verifyBoxHnumM, numM < 1 || 999 < numM);
            int num = verifyBoxHnum.GetIntValue();
            setStatus(verifyBoxHnum, num < 1 || 99999999 < num);
            var hnum = verifyBoxHnumM.Text.Trim() + "-" + verifyBoxHnum.Text.Trim();

            //本家区分
            int family = verifyBoxFamily.GetIntValue();

            //20200212102206 furukawa st ////////////////////////
            //0高一も入力可能とする
            
            setStatus(verifyBoxFamily, !(new int[] {0, 2, 4, 6, 8 }).Contains(family));
            //setStatus(verifyBoxFamily, !(new int[] { 2, 4, 6, 8 }).Contains(family));
            //20200212102206 furukawa ed ////////////////////////


            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //新規継続
            //int newCont = verifyBoxNewCont.GetIntValue();
            //setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //初検年月
            int sY = verifyBoxF1Y.GetIntValue();
            int sM = verifyBoxF1M.GetIntValue();
            var shoken = DateTime.MinValue;
            if (string.IsNullOrWhiteSpace(scanGroup.note2))
            {
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応
                sY = DateTimeEx.GetAdYearFromHs(sY *100 +sM);
                //sY = DateTimeEx.GetAdYearFromHs(sY + sM);
                //sY = DateTimeEx.GetAdYearFromHs(sY);

                if (!DateTimeEx.IsDate(sY, sM, 1))
                {
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                }
                else
                {
                    setStatus(verifyBoxF1Y, false);
                    setStatus(verifyBoxF1M, false);
                    shoken = new DateTime(sY, sM, 1);
                }
            }

            //合計
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //請求
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;
            if ((family == 2 || family == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (family == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (family == 4 && (int)(total * 80 / 100) != charge)
                payError = true;

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxFamily.BackColor = Color.GreenYellow;
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //ナンバリング抜けチェック
            int preNum = 0;
            int index = bsApp.IndexOf(app);
            var l = (List<App>)bsApp.DataSource;
            for (int i = l.Count - 1; i >= 0; i--)
            {
                if (l[i].MediYear > 0)
                {
                    if (!int.TryParse(l[i].Numbering, out preNum)) continue;
                    break;
                }
            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = hnum;
            app.InsNum = iNumber;
            app.HihoPref = pref;
            app.Family = family;
            app.Sex = sex;

            //app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応
            app.NewContType = DateTimeEx.GetAdYearFromHs(year *100 + month) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //app.NewContType = DateTimeEx.GetAdYearFromHs(year) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;

            app.FushoFirstDate1 = shoken;

            app.Birthday = birthDt;
            app.Total = total;
            app.Charge = charge;
            app.CountedDays = days;
            app.DrNum = verifyBoxHosNumber.Text.Trim();
            app.AppType = apptype == 7 ? APP_TYPE.鍼灸 : APP_TYPE.柔整;

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //被保険者番号
                var hn = app.HihoNum.Split('-');
                if (hn.Length == 2)
                {
                    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxHnumM, string.Empty, firstTime, nv);
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                }

                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);
                setValue(verifyBoxAppType, app.AppType == APP_TYPE.鍼灸 ? "7" : "0", firstTime, nv);
                setValue(verifyBoxPref, app.HihoPref.ToString(), firstTime, nv);
                setValue(verifyBoxHosNumber, app.DrNum, firstTime, nv);
                setValue(verifyBoxFamily, app.Family.ToString(), firstTime, nv);
                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
                setValue(verifyBoxTotal, app.Total, firstTime, nv);
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);

                //負傷名
                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);

                //初検日
                if (app.FushoFirstDate1.IsNullDate())
                {
                    setValue(verifyBoxF1Y, string.Empty, firstTime, false);
                    setValue(verifyBoxF1M, string.Empty, firstTime, false);
                }
                else
                {
                    setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, false);
                    setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, false);
                }
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                panelHnum.Visible = true;
                panelTotal.Visible = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (numberControls.Contains(t)) posNumbering = pos;

        }

        private void verifyBoxAppType_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxAppType.Text != "7" && firstTime)
            {
                verifyBoxF1.Enabled = true;
                verifyBoxF2.Enabled = true;
                verifyBoxF3.Enabled = true;
                verifyBoxF4.Enabled = true;
                verifyBoxF5.Enabled = true;
            }
            else
            {
                verifyBoxF1.Enabled = false;
                verifyBoxF2.Enabled = false;
                verifyBoxF3.Enabled = false;
                verifyBoxF4.Enabled = false;
                verifyBoxF5.Enabled = false;
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
