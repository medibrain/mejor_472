﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using NpgsqlTypes;
using System.Windows.Forms;

namespace Mejor.AichiToshi
{
    class Export
    {
        const int ZOKUSHI_CODE = -3;
        const string DEFALT_ERA = "4";
        const int START_NUM = 900001;   //愛知のナンバリングは90万番台から


        /// <summary>
        /// 特定の請求月のAppに対し、連番を付与します
        /// 万が一申請書と続紙以外のデータがあった場合、不正データが作成されてしまいます
        /// また、この関数を呼び出すと、すでに振られている連番は上書きされます
        /// </summary>
        /// <param name="cyear"></param>
        /// <param name="cmonth"></param>
        /// <returns></returns>
        public static bool Numbering(int cym)
        {
            List<object[]> res;

            //続紙以外のAppを取得
            using (var cmd = DB.Main.CreateCmd("SELECT aid, ayear, numbering FROM application " +
                "WHERE cym=:cym AND ayear<>:ay " +
                "ORDER BY aid"))
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                cmd.Parameters.Add("ay", NpgsqlDbType.Integer).Value = ZOKUSHI_CODE;
                res = cmd.TryExecuteReaderList();
            }

            if (res == null) return false;

            //既にナンバリングされているかどうか確認
            int numcheck = 0;
            for (int i = 0; i < res.Count; i++)
            {
                if (res[i][2].ToString().Length != 0) numcheck++;
            }
            
            if (numcheck == res.Count)
            {
                var r = MessageBox.Show("既にナンバリングされています。再度ナンバリングを実行しますか？",
                    "愛知都市共済 提出データ出力", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.No) return true;
            }
            else if (numcheck != 0)
            {
                var r = MessageBox.Show(numcheck + 
                    "件のナンバリングされていないデータがあります。再度ナンバリングを実行しますか？",
                    "愛知都市共済 提出データ出力", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.No) return true;
            }

            //連番をDBのデータへ付与
            using (var tran = DB.Main.CreateTransaction())
            using (var cmd = DB.Main.CreateCmd("UPDATE application " +
                "SET numbering=:num WHERE aid=:aid;", tran))
            {
                cmd.Parameters.Add("num", NpgsqlDbType.Text);
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer);

                for (int i = 0; i < res.Count; i++)
                {
                    cmd.Parameters["num"].Value = (i + START_NUM).ToString();
                    cmd.Parameters["aid"].Value = (int)res[i][0];
                    if (!cmd.TryExecuteNonQuery())
                    {
                        tran.Rollback();
                        return false;
                    }
                }

                tran.Commit();
            }
            return true;
        }

        /// <summary>
        /// 保存先ディレクトリと対象処理月1ヶ月分すべてのAppを指定し、エクスポートを行います。
        /// aidの順番にソートしておく必要があります。
        /// </summary>
        /// <param name="dirName"></param>
        /// <param name="appList"></param>
        /// <returns></returns>
        public static bool DoExport(string dirName, List<App> appList, WaitForm wf)
        {
            wf.SetMax(appList.Count);
            
            var infoDir = dirName + "\\0_COMMON001";
            var imageDir = dirName + "\\9_JUDOTHERAPY";
            var scanDir = Settings.ImageFolder;
            try
            {
                System.IO.Directory.CreateDirectory(infoDir);
                System.IO.Directory.CreateDirectory(imageDir);
            }
            catch
            {
                MessageBox.Show("出力フォルダの作成に失敗しました。");
                return false;
            }

            var jyuFileName = infoDir + "\\99_PECULIARTEXTINFO_JYU.CSV";

            //tiffに含めるファイル名一覧 フルパス
            var images = new List<string>();
            string saveTiffFileName = "";

            //インフォメーションファイル用診療年月
            string infoSinryoYM=string.Empty;

            //レセプト枚数カウント
            int receCount=0;

            //ファイル出力
            using (var sw = new System.IO.StreamWriter(jyuFileName, false, Encoding.GetEncoding("Shift_JIS")))
            {
                foreach (var item in appList)
                {
                    wf.InvokeValue++;

                    #region 画像関連
                    if (item.MediYear == -4)
                    {
                        //不要画像の場合
                        continue;
                    }
                    else if (item.MediYear == ZOKUSHI_CODE)
                    {
                        //続紙の場合
                        images.Add(item.GetImageFullPath());
                        continue;
                    }
                    else if (images.Count == 1)
                    {
                        if (!ImageUtility.SaveOne(images, saveTiffFileName))
                        {
                            throw new Exception("画像ファイルのコピーに失敗しました。ファイル名:" + saveTiffFileName);
                        }

                        images.Clear();
                    }
                    else if (images.Count != 0)
                    {
                        if (!ImageUtility.Save(images, saveTiffFileName))
                        {
                            throw new Exception("画像ファイルのマルチページTiff変換に失敗しました。ファイル名:" + saveTiffFileName);
                        }
                        images.Clear();
                    }
                    images.Add(item.GetImageFullPath());

                    #endregion


                    //20190611192929 furukawa st ////////////////////////
                    //処理年月、診療年月作成ルーチン整頓
                    
                    #region 処理年月作成
                    //処理年月を生成 
                    //請求年月-1としている

                    int chgY = item.ChargeYear;//新元号の年
                    int chgM = item.ChargeMonth;//新元号の月

                    if (chgM == 1)
                    {
                        chgY--;
                        chgM = 12;
                    }
                    else chgM--;


                    int syori_adyyyymm = DateTimeEx.GetAdYearFromHs(chgY * 100 + chgM) * 100 + chgM;
                    string syoriYM = DateTimeEx.GetGyymmFromAdYM(syori_adyyyymm).ToString("00000");

                    #endregion

                    #region 診療年月作成
                    //informationファイル診療年月
                    //informationファイル用は１回のみ作成
                    if (infoSinryoYM == string.Empty)
                    {
                        string y = syoriYM.Substring(1, 2);
                        string m = syoriYM.Substring(3, 2);

                        int intSyoriYM = 0;
                        int.TryParse(syoriYM, out intSyoriYM);

                        //診療年月は処理年月の1か月前
                        if (intSyoriYM == 50105) infoSinryoYM = "43104";
                        else if (m == "01")
                        {
                            infoSinryoYM = intSyoriYM.ToString().Substring(0, 1) + (int.Parse(y) - 1).ToString("00") + "12";
                        }
                        else infoSinryoYM = (intSyoriYM - 1).ToString("00000");
                    }



                    //99_PECULIARTEXTINFO_JYUファイル診療年月
                    int medY = item.MediYear;
                    int medM = item.MediMonth;
                    int sinryo_adyyyymm = DateTimeEx.GetAdYearFromHs(medY * 100 + medM) * 100 + medM;
                    string sinryoYM = DateTimeEx.GetGyymmFromAdYM(sinryo_adyyyymm).ToString("00000");

                    #endregion

                    #region 旧処理
                    /*
                    #region 処理年月作成

                    //処理年月を生成 請求年月-1としている
                    int y = item.ChargeYear;
                    int m = item.ChargeMonth;



                    //20190611161956 furukawa st ////////////////////////
                    //処理年月新元号対応

                    int chgY = 0;//新元号の年
                    int chgM = 0;//新元号の月
                    string chgEra = DEFALT_ERA;//新元号の番号

                    ChangeEra(y, m, chgEra, out chgY, out chgM, out chgEra);
                    //20190611161956 furukawa ed ////////////////////////


                    //20190611164714 furukawa st ////////////////////////
                    //変数修正
                    
                    if (chgM == 1)
                    {
                        chgY--;
                        chgM = 12;
                    }
                    else chgM--;

                           
                            //if (m == 1)
                            //{
                            //    y--;
                            //    m = 12;
                            //}
                            //else
                            //{
                            //    m--;
                            //}
                            
                    //20190611164714 furukawa ed ////////////////////////





                    //20190611162127 furukawa zst ////////////////////////
                    //処理年月新元号対応

                    int adyyyymm = DateTimeEx.GetAdYearFromHs(chgY * 100 + chgM) * 100 + chgM;
                    //string syoriYM = chgEra + chgY.ToString("00") + chgM.ToString("00");
                        // string syoriYM = DEFALT_ERA + y.ToString("00") + m.ToString("00");

                    string syoriYM = DateTimeEx.GetGyymmFromAdYM(adyyyymm).ToString("00000");
                    //20190611162127 furukawa ed ////////////////////////

                    #endregion

                    #region 診療年月作成


                    //20190611162144 furukawa st ////////////////////////
                    //informationファイル診療年月新元号対応
                    
                    ChangeEra(y, m, chgEra, out chgY, out chgM, out chgEra);
                    //20190611162144 furukawa ed ////////////////////////


                    //インフォメーションファイルのための記録
                    if (infoSinryoYM == string.Empty)
                    {
                        //診療年月は処理年月の1か月前


                        //20190611164841 furukawa st ////////////////////////
                        //変数修正

                        if (chgM == 1)
                        {
                            chgY--;
                            chgM = 12;
                        }
                        else
                        {
                            chgM--;
                        }

                           
                             //if (m == 1)
                             //{
                             //    y--;
                             //    m = 12;
                             //}
                             //else
                             //{
                             //    m--;
                             //}
                             

                        //20190611164841 furukawa ed ////////////////////////


                        //20190611162204 furukawa st ////////////////////////
                        //informationファイル診療年月新元号対応
                        
                        infoSinryoYM = chgEra + chgY.ToString("00") + chgM.ToString("00");
                        //infoSinryoYM = DEFALT_ERA + y.ToString("00") + m.ToString("00");
                        //20190611162204 furukawa ed ////////////////////////


                        int intSyoriYM = 0;
                        int.TryParse(syoriYM, out intSyoriYM);

                        if (intSyoriYM == 105) infoSinryoYM = "3104";
                        else infoSinryoYM = (intSyoriYM - 1).ToString("0000");

                    }

                    //診療年月を生成


                    //20190611162518 furukawa st ////////////////////////
                    //99_PECULIARTEXTINFO_JYUファイル診療年月新元号対応

                    int medY = item.MediYear;
                    int medM = item.MediMonth;
                    string medEra = string.Empty;
                    string sinryoYM = DEFALT_ERA + item.MediYear.ToString("00") + item.MediMonth.ToString("00");
                    ChangeEra(item.MediYear, item.MediMonth, DEFALT_ERA, out medY, out medM, out medEra);
                    sinryoYM = medEra + medY.ToString("00") + medM.ToString("00");


                    adyyyymm = DateTimeEx.GetAdYearFromHs(medY * 100 + medM) * 100 + medM;
                    sinryoYM = DateTimeEx.GetGyymmFromAdYM(adyyyymm).ToString("00000");


                    //string sinryoYM = DEFALT_ERA + item.MediYear.ToString("00") + item.MediMonth.ToString("00");


                    //20190611162518 furukawa ed ////////////////////////
                    #endregion

                    */
                    #endregion

                    //20190611192929 furukawa ed ////////////////////////



                    //ID関係を生成
                    int baseID;
                    if (!int.TryParse(item.Numbering, out baseID))
                    {
                        throw new Exception("ナンバリングを数値に変換できませんでした。aid:" + item.Aid.ToString());
                    }
                    string receNo = "00" + baseID.ToString("000000");
                    string searchNo = "000" + syoriYM + "000" + baseID.ToString("000000");

                    //被保記番の設定
                    var mn = item.HihoNum.Split('-');
                    string mark, number;
                    if (mn.Length == 1)
                    {
                        mark = string.Empty;
                        number = mn[0];
                    }
                    else if (mn.Length == 2)
                    {
                        mark = mn[0];
                        number = mn[1];
                    }
                    else
                    {
                        throw new Exception("被保険者記号番号を判別できませんでした。aid:" + item.Aid.ToString());
                    }

                    //生年月日
                    string jbirth = DateTimeEx.GetEraNumber(item.Birthday).ToString() +
                        DateTimeEx.GetJpYear(item.Birthday).ToString("00") +
                        item.Birthday.Month.ToString("00") +
                        item.Birthday.Day.ToString("00");

                    //画像ファイル名を指定
                    saveTiffFileName = imageDir + "\\" +searchNo + ".tif";

                    //家族区分と受診者区分
                    string familyCode = item.Family.ToString() == "2" ? "1" : "2" ;

                    string jyushinshaCode = item.Family.ToString();
                    if (jyushinshaCode == "4") jyushinshaCode = "5";
                    else if (jyushinshaCode == "8") jyushinshaCode = "1";
                    else if (jyushinshaCode == "0") jyushinshaCode = "2";
                    else jyushinshaCode = "";

                    string syubetsu = item.AppType == APP_TYPE.鍼灸 ? "7" : "1";

                    var sl = new string[87];
                    sl[1] = "10";
                    sl[2] = "MN";
                    sl[3] = receNo;
                    sl[4] = sinryoYM;
                    sl[5] = "6";
                    sl[6] = item.HihoPref.ToString("00");
                    sl[7] = "9";
                    sl[8] = syoriYM;
                    sl[9] = "0" + receNo;
                    sl[10] = searchNo;
                    sl[11] = "2";
                    sl[12] = item.DrNum;
                    sl[13] = "";
                    sl[14] = "";
                    sl[15] = "";
                    sl[16] = "20";
                    sl[17] = "SA";
                    sl[18] = syubetsu;
                    sl[19] = "1";
                    sl[20] = familyCode;
                    sl[21] = "";
                    sl[22] = "";
                    sl[23] = item.InsNum;
                    sl[24] = Strings.StrConv(mark, VbStrConv.Wide);
                    sl[25] = Strings.StrConv(number, VbStrConv.Wide);
                    sl[26] = "";
                    sl[27] = "";
                    sl[28] = "";
                    sl[29] = "";
                    sl[30] = "";
                    sl[31] = "";
                    sl[32] = "";
                    sl[33] = "";
                    sl[34] = "";
                    sl[35] = "";
                    sl[36] = "";
                    sl[37] = "";
                    sl[38] = item.Sex == 1 ? "1" : "2";//item.Psex.ToString();
                    sl[39] = jbirth;
                    sl[40] = "";
                    sl[41] = "";
                    sl[42] = "";
                    sl[43] = "";
                    sl[44] = "";
                    sl[45] = "";
                    sl[46] = "";
                    sl[47] = "";
                    sl[48] = "";
                    sl[49] = "";
                    sl[50] = "";
                    sl[51] = item.CountedDays.ToString();
                    sl[52] = "";
                    sl[53] = "";
                    sl[54] = "";
                    sl[55] = "";
                    sl[56] = "";
                    sl[57] = "";
                    sl[58] = "30";
                    sl[59] = "NJ";
                    sl[60] = "";
                    sl[61] = familyCode;
                    sl[62] = item.Total.ToString();
                    sl[63] = "";
                    sl[64] = "";
                    sl[65] = "";
                    sl[66] = "";
                    sl[67] = "";
                    sl[68] = jyushinshaCode;
                    sl[69] = "";
                    sl[70] = "";
                    sl[71] = "";
                    sl[72] = "";
                    sl[73] = "";
                    sl[74] = "";
                    sl[75] = "";
                    sl[76] = "";
                    sl[77] = "";
                    sl[78] = "";
                    sl[79] = "";
                    sl[80] = "";
                    sl[81] = "";
                    sl[82] = "";
                    sl[83] = "";
                    sl[84] = "";
                    sl[85] = "";
                    sl[86] = "";

                    //CSV書き込み
                    if (!createCSVLine(sl, sw))return false;

                    //レセ枚数カウント
                    receCount++;
                }

                //最終回分tiff画像記録
                ImageUtility.Save(images, saveTiffFileName);
            }

            //infomationファイル作成
            var infoFileName = infoDir + "\\00_INFOMATION.CSV";
            try
            {
                using (var sw = new System.IO.StreamWriter(infoFileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine("保険者名," + "愛知県都市職員共済組合");//Settings.HokenshaName);
                    sw.WriteLine("保険者番号," + "32230518");//Settings.HokenshaNo);
                    sw.WriteLine("診療年月," + infoSinryoYM);
                    sw.WriteLine("媒体作成年月日," + DateTime.Today.ToString("yyyy/MM/dd"));
                    sw.WriteLine(string.Empty);
                    sw.WriteLine("医科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("DPC  (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("歯科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("調剤 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("柔整 (レセ電/紙レセ/コード/画像) ,0," + receCount.ToString() + ",0," + receCount.ToString());
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }

        static bool createCSVLine(string[] list, System.IO.StreamWriter sw)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(list[1]);
                for (int i = 2; i < 16; i++)
                {
                    sb.Append("," + list[i]);
                }
                sw.WriteLine(sb.ToString());

                sb.Clear();
                sb.Append(list[16]);
                for (int i = 17; i < 58; i++)
                {
                    sb.Append("," + list[i]);
                }
                sw.WriteLine(sb.ToString());

                sb.Clear();
                sb.Append(list[58]);
                for (int i = 59; i < 87; i++)
                {
                    sb.Append("," + list[i]);
                }
                sw.WriteLine(sb.ToString());
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            using (var wf = new WaitForm())
            {
                try
                {
                    using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                    {
                        wf.Max = list.Count;
                        wf.BarStyle = ProgressBarStyle.Continuous;wf.ShowDialogOtherTask();
                        wf.LogPrint("リストを出力しています…");

                        var l = new List<string>();
                        //先頭行は見出し
                        l.Add("ID");
                        l.Add("処理年");
                        l.Add("処理月");
                        l.Add("診療年");
                        l.Add("診療月");
                        l.Add("保険者番号");
                        l.Add("被保険者番号");
                        l.Add("住所");
                        l.Add("氏名");
                        l.Add("性別");
                        l.Add("生年月日");
                        l.Add("請求区分");
                        l.Add("往療料");
                        l.Add("施術所記号");
                        l.Add("合計金額");
                        l.Add("請求金額");
                        l.Add("診療日数");
                        l.Add("ナンバリング");
                        l.Add("照会理由");
                        l.Add("点検結果");
                        l.Add("点検結果詳細");
                        l.Add("被保険者名");
                        l.Add("受療者名");
                        l.Add("施術所名");
                        sw.WriteLine(string.Join(",", l));

                        var ss = new List<string>();
                        foreach (var item in list)
                        {
                            if (wf.Cancel)
                            {
                                if (MessageBox.Show("中止してもよろしいですか？", "",
                                     MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                                wf.Cancel = false;
                            }

                            ss.Add(item.Aid.ToString());
                            ss.Add(item.ChargeYear.ToString());
                            ss.Add(item.ChargeMonth.ToString());
                            ss.Add(item.MediYear.ToString());
                            ss.Add(item.MediMonth.ToString());
                            ss.Add(item.InsNum);
                            ss.Add(item.HihoNum);
                            ss.Add(item.HihoAdd);
                            ss.Add(item.PersonName);
                            ss.Add(item.Sex == 1 ? "男" : "女");
                            ss.Add(item.Birthday.ToShortDateString());
                            ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                            ss.Add(item.Distance == 999 ? "あり" : "");
                            ss.Add(item.DrNum);
                            ss.Add(item.Total.ToString());
                            ss.Add(item.Charge.ToString());
                            ss.Add(item.CountedDays.ToString());
                            ss.Add(item.Numbering);
                            ss.Add(item.ShokaiReason.ToString().Replace(",", " "));
                            ss.Add(item.InspectInfo);
                            ss.Add(item.InspectDescription);
                            ss.Add(item.HihoName);
                            ss.Add(item.PersonName);
                            ss.Add(item.ClinicName);

                            sw.WriteLine(string.Join(",", ss));
                            ss.Clear();

                            wf.InvokeValue++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    MessageBox.Show("出力に失敗しました");
                    return false;
                }
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }




        //2019-06-11furukawa追加
        static void ChangeEra(int y, int m, string era,
                       out int outy, out int outm, out string outera)
        {
            string res = string.Empty;

            string strBaseYM = y.ToString("00") + m.ToString("00");

            int baseYM = int.Parse(strBaseYM);
            if (baseYM <= 3104 && baseYM>=1912)
            {
                era = "4";
            }
            else
            {
                era = "5";
            }

        




            outy = y;
            outm = m;
            outera = era;

        }


    }
}
