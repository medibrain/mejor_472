﻿namespace Mejor
{
    partial class InputLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSeach = new System.Windows.Forms.Button();
            this.dateBoxS = new Mejor.DateBox();
            this.dateBoxE = new Mejor.DateBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonAll = new System.Windows.Forms.RadioButton();
            this.radioButtonHour = new System.Windows.Forms.RadioButton();
            this.radioButtonDay = new System.Windows.Forms.RadioButton();
            this.radioButtonMonth = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxInsurer = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonToday = new System.Windows.Forms.Button();
            this.buttonLastDay = new System.Windows.Forms.Button();
            this.buttonTodayWeek = new System.Windows.Forms.Button();
            this.buttonLastWeek = new System.Windows.Forms.Button();
            this.buttonTodayMonth = new System.Windows.Forms.Button();
            this.buttoLastMonth = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCSV = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 67);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1504, 437);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            // 
            // buttonSeach
            // 
            this.buttonSeach.Location = new System.Drawing.Point(1336, 33);
            this.buttonSeach.Name = "buttonSeach";
            this.buttonSeach.Size = new System.Drawing.Size(75, 23);
            this.buttonSeach.TabIndex = 1;
            this.buttonSeach.Text = "集計";
            this.buttonSeach.UseVisualStyleBackColor = true;
            this.buttonSeach.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // dateBoxS
            // 
            this.dateBoxS.DateMode = Mejor.DateBox.DATE_MODE.和暦;
            this.dateBoxS.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxS.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxS.Location = new System.Drawing.Point(15, 18);
            this.dateBoxS.Name = "dateBoxS";
            this.dateBoxS.Size = new System.Drawing.Size(103, 19);
            this.dateBoxS.TabIndex = 2;
            // 
            // dateBoxE
            // 
            this.dateBoxE.DateMode = Mejor.DateBox.DATE_MODE.和暦;
            this.dateBoxE.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dateBoxE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dateBoxE.Location = new System.Drawing.Point(135, 18);
            this.dateBoxE.Name = "dateBoxE";
            this.dateBoxE.Size = new System.Drawing.Size(103, 19);
            this.dateBoxE.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonAll);
            this.groupBox1.Controls.Add(this.radioButtonHour);
            this.groupBox1.Controls.Add(this.radioButtonDay);
            this.groupBox1.Controls.Add(this.radioButtonMonth);
            this.groupBox1.Location = new System.Drawing.Point(558, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(224, 44);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "集計単位";
            // 
            // radioButtonAll
            // 
            this.radioButtonAll.AutoSize = true;
            this.radioButtonAll.Location = new System.Drawing.Point(160, 20);
            this.radioButtonAll.Name = "radioButtonAll";
            this.radioButtonAll.Size = new System.Drawing.Size(59, 16);
            this.radioButtonAll.TabIndex = 2;
            this.radioButtonAll.TabStop = true;
            this.radioButtonAll.Text = "全期間";
            this.radioButtonAll.UseVisualStyleBackColor = true;
            // 
            // radioButtonHour
            // 
            this.radioButtonHour.AutoSize = true;
            this.radioButtonHour.Location = new System.Drawing.Point(107, 21);
            this.radioButtonHour.Name = "radioButtonHour";
            this.radioButtonHour.Size = new System.Drawing.Size(47, 16);
            this.radioButtonHour.TabIndex = 2;
            this.radioButtonHour.TabStop = true;
            this.radioButtonHour.Text = "時間";
            this.radioButtonHour.UseVisualStyleBackColor = true;
            // 
            // radioButtonDay
            // 
            this.radioButtonDay.AutoSize = true;
            this.radioButtonDay.Checked = true;
            this.radioButtonDay.Location = new System.Drawing.Point(54, 21);
            this.radioButtonDay.Name = "radioButtonDay";
            this.radioButtonDay.Size = new System.Drawing.Size(47, 16);
            this.radioButtonDay.TabIndex = 1;
            this.radioButtonDay.TabStop = true;
            this.radioButtonDay.Text = "日付";
            this.radioButtonDay.UseVisualStyleBackColor = true;
            // 
            // radioButtonMonth
            // 
            this.radioButtonMonth.AutoSize = true;
            this.radioButtonMonth.Location = new System.Drawing.Point(13, 21);
            this.radioButtonMonth.Name = "radioButtonMonth";
            this.radioButtonMonth.Size = new System.Drawing.Size(35, 16);
            this.radioButtonMonth.TabIndex = 0;
            this.radioButtonMonth.TabStop = true;
            this.radioButtonMonth.Text = "月";
            this.radioButtonMonth.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.buttonCSV);
            this.panel1.Controls.Add(this.buttonSeach);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1504, 67);
            this.panel1.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.comboBoxInsurer);
            this.groupBox3.Controls.Add(this.comboBox1);
            this.groupBox3.Location = new System.Drawing.Point(788, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(443, 44);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "フィルター";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(173, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "保険者";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "種別";
            // 
            // comboBoxInsurer
            // 
            this.comboBoxInsurer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInsurer.FormattingEnabled = true;
            this.comboBoxInsurer.Location = new System.Drawing.Point(220, 18);
            this.comboBoxInsurer.Name = "comboBoxInsurer";
            this.comboBoxInsurer.Size = new System.Drawing.Size(215, 20);
            this.comboBoxInsurer.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "指定なし",
            "国保",
            "社保",
            "広域連合",
            "学校共済"});
            this.comboBox1.Location = new System.Drawing.Point(49, 18);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(108, 20);
            this.comboBox1.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(1241, 33);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(72, 16);
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "保険者別";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonToday);
            this.groupBox2.Controls.Add(this.buttonLastDay);
            this.groupBox2.Controls.Add(this.buttonTodayWeek);
            this.groupBox2.Controls.Add(this.buttonLastWeek);
            this.groupBox2.Controls.Add(this.buttonTodayMonth);
            this.groupBox2.Controls.Add(this.buttoLastMonth);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dateBoxS);
            this.groupBox2.Controls.Add(this.dateBoxE);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(540, 44);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "集計期間";
            // 
            // buttonToday
            // 
            this.buttonToday.Location = new System.Drawing.Point(485, 17);
            this.buttonToday.Name = "buttonToday";
            this.buttonToday.Size = new System.Drawing.Size(48, 21);
            this.buttonToday.TabIndex = 7;
            this.buttonToday.Text = "今日";
            this.buttonToday.UseVisualStyleBackColor = true;
            this.buttonToday.Click += new System.EventHandler(this.buttonToday_Click);
            // 
            // buttonLastDay
            // 
            this.buttonLastDay.Location = new System.Drawing.Point(437, 17);
            this.buttonLastDay.Name = "buttonLastDay";
            this.buttonLastDay.Size = new System.Drawing.Size(48, 21);
            this.buttonLastDay.TabIndex = 7;
            this.buttonLastDay.Text = "昨日";
            this.buttonLastDay.UseVisualStyleBackColor = true;
            this.buttonLastDay.Click += new System.EventHandler(this.buttonLastDay_Click);
            // 
            // buttonTodayWeek
            // 
            this.buttonTodayWeek.Location = new System.Drawing.Point(389, 17);
            this.buttonTodayWeek.Name = "buttonTodayWeek";
            this.buttonTodayWeek.Size = new System.Drawing.Size(48, 21);
            this.buttonTodayWeek.TabIndex = 7;
            this.buttonTodayWeek.Text = "今週";
            this.buttonTodayWeek.UseVisualStyleBackColor = true;
            this.buttonTodayWeek.Click += new System.EventHandler(this.buttonTodayWeek_Click);
            // 
            // buttonLastWeek
            // 
            this.buttonLastWeek.Location = new System.Drawing.Point(341, 17);
            this.buttonLastWeek.Name = "buttonLastWeek";
            this.buttonLastWeek.Size = new System.Drawing.Size(48, 21);
            this.buttonLastWeek.TabIndex = 7;
            this.buttonLastWeek.Text = "先週";
            this.buttonLastWeek.UseVisualStyleBackColor = true;
            this.buttonLastWeek.Click += new System.EventHandler(this.buttonLastWeek_Click);
            // 
            // buttonTodayMonth
            // 
            this.buttonTodayMonth.Location = new System.Drawing.Point(293, 17);
            this.buttonTodayMonth.Name = "buttonTodayMonth";
            this.buttonTodayMonth.Size = new System.Drawing.Size(48, 21);
            this.buttonTodayMonth.TabIndex = 7;
            this.buttonTodayMonth.Text = "今月";
            this.buttonTodayMonth.UseVisualStyleBackColor = true;
            this.buttonTodayMonth.Click += new System.EventHandler(this.buttonTodayMonth_Click);
            // 
            // buttoLastMonth
            // 
            this.buttoLastMonth.Location = new System.Drawing.Point(245, 17);
            this.buttoLastMonth.Name = "buttoLastMonth";
            this.buttoLastMonth.Size = new System.Drawing.Size(48, 21);
            this.buttoLastMonth.TabIndex = 7;
            this.buttoLastMonth.Text = "先月";
            this.buttoLastMonth.UseVisualStyleBackColor = true;
            this.buttoLastMonth.Click += new System.EventHandler(this.buttonLastMonth_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "～";
            // 
            // buttonCSV
            // 
            this.buttonCSV.Location = new System.Drawing.Point(1417, 33);
            this.buttonCSV.Name = "buttonCSV";
            this.buttonCSV.Size = new System.Drawing.Size(75, 23);
            this.buttonCSV.TabIndex = 1;
            this.buttonCSV.Text = "CSV";
            this.buttonCSV.UseVisualStyleBackColor = true;
            this.buttonCSV.Click += new System.EventHandler(this.buttonCSV_Click);
            // 
            // InputLogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1504, 504);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel1);
            this.Name = "InputLogForm";
            this.Text = "入力集計";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSeach;
        private DateBox dateBoxS;
        private DateBox dateBoxE;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonHour;
        private System.Windows.Forms.RadioButton radioButtonDay;
        private System.Windows.Forms.RadioButton radioButtonMonth;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button buttonToday;
        private System.Windows.Forms.Button buttonLastDay;
        private System.Windows.Forms.Button buttonTodayWeek;
        private System.Windows.Forms.Button buttonLastWeek;
        private System.Windows.Forms.Button buttonTodayMonth;
        private System.Windows.Forms.Button buttoLastMonth;
        private System.Windows.Forms.RadioButton radioButtonAll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBoxInsurer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCSV;
    }
}