﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Awajishi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;
      
        /// <summary>
        /// 提供データ
        /// </summary>
        List<dataimport> lstDataImport = new List<dataimport>();

        /// <summary>
        /// ナンバリング登録前確認用
        /// </summary>
        int intPrevNumbering = 0;

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount,verifyBoxCountedDays};

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,verifyBoxCharge,checkBoxVisit,checkBoxVisitKasan };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1,verifyBoxF2,verifyBoxF3,verifyBoxF4,verifyBoxF5 };

           
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion

            

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            
        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            lstDataImport.Clear();
            lstDataImport = dataimport.GetDataByCYM(scan.CYM);

            var app = (App)bsApp.Current;
            if (app != null)
            {
                setApp(app);
            }
            
            
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {      
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };
            
            phnum.Visible = false;
            pDoui.Visible = false;

            switch (verifyBoxY.Text)
            {
                case clsInputKind.長期://ヘッダ
                case clsInputKind.続紙:// "--"://続紙
                case clsInputKind.不要://"++"://不要
                case clsInputKind.エラー:
                case clsInputKind.施術同意書裏:// "902"://施術同意書裏
                case clsInputKind.施術報告書:// "911"://施術報告書/
                case clsInputKind.状態記入書:// "921"://状態記入書
                    panelTotal.Visible = false;
                    break;

                case clsInputKind.施術同意書:// "901"://施術同意書
                    panelTotal.Visible = false;
                    pDoui.Visible = true;
                    break;
                    
                default:
                    panelTotal.Visible = true;
                    phnum.Visible = true;
                    pDoui.Visible = true;
                    break;
            }



            #region old
            //panelHnum.Visible = false;            

            //続紙: --        不要: ++        ヘッダ:**
            //続紙、不要とヘッダの表示項目変更

            if (verifyBoxY.Text == "**")
            {
                //続紙、その他の場合、入力項目は無い
                //act(panelRight, false);                
               // panelHnum.Visible = false;
                
            }
            else if (verifyBoxY.Text == "++" || verifyBoxY.Text == "--" )
            {
                //続紙、不要の場合は何も入力しない
               // panelHnum.Visible = false;
                
            }
            else if(int.TryParse(verifyBoxY.Text,out int tmp))
            {
                //申請書の場合
                //act(panelRight, true);                
               // panelHnum.Visible = true;

            }
            else
            {
              //  panelHnum.Visible = false;
                
            }
            #endregion

        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            //国保データ

            dataimport k = lstDataImport[0];

            dgv.Columns[nameof(k.f000_importid)].Width = 50;
            dgv.Columns[nameof(k.f001_pageins)].Width = 30;
            dgv.Columns[nameof(k.f002_pageall)].Width = 40;
            dgv.Columns[nameof(k.f003_shinsay)].Width = 50;
            dgv.Columns[nameof(k.f004_shinsam)].Width = 50;
            dgv.Columns[nameof(k.f005_menjo)].Width = 50;
            dgv.Columns[nameof(k.f006_insnum)].Width = 70;
            dgv.Columns[nameof(k.f007_insname)].Width = 50;
            dgv.Columns[nameof(k.f008_clinicnum)].Width = 90;
            dgv.Columns[nameof(k.f009_pname)].Width = 70;
            dgv.Columns[nameof(k.f010_knum)].Width = 50;
            dgv.Columns[nameof(k.f011_futan)].Width = 50;
            dgv.Columns[nameof(k.f012_startdate)].Width = 80;
            dgv.Columns[nameof(k.f013_fukusu)].Width = 50;
            dgv.Columns[nameof(k.f014_total)].Width = 50;
            dgv.Columns[nameof(k.f015_biko1)].Width = 50;
            dgv.Columns[nameof(k.f016_shusei)].Width = 50;
            dgv.Columns[nameof(k.f017_examy)].Width = 50;
            dgv.Columns[nameof(k.f018_examm)].Width = 50;
            dgv.Columns[nameof(k.f019_clinicname)].Width = 50;
            dgv.Columns[nameof(k.f020_hnum)].Width = 50;
            dgv.Columns[nameof(k.f021_pbirthe)].Width = 50;
            dgv.Columns[nameof(k.f022_pbirthy)].Width = 50;
            dgv.Columns[nameof(k.f023_pbirthm)].Width = 50;
            dgv.Columns[nameof(k.f024_pbirthd)].Width = 50;
            dgv.Columns[nameof(k.f025_personalcode)].Width = 50;
            dgv.Columns[nameof(k.f026_pgender)].Width =40;
            dgv.Columns[nameof(k.f027_jukyunum)].Width = 50;
            dgv.Columns[nameof(k.f028_ratio)].Width = 40;
            dgv.Columns[nameof(k.f029_firstdate)].Width = 50;
            dgv.Columns[nameof(k.f030_finishdate)].Width = 50;
            dgv.Columns[nameof(k.f031_counteddays)].Width = 40;
            dgv.Columns[nameof(k.f032_partial)].Width = 80;
            dgv.Columns[nameof(k.f033_biko2)].Width = 50;
            dgv.Columns[nameof(k.f034_comnum)].Width =150;
            dgv.Columns[nameof(k.cym)].Width = 80;
            dgv.Columns[nameof(k.shinsaymAD)].Width = 80;
            dgv.Columns[nameof(k.ymAD)].Width = 80;
            dgv.Columns[nameof(k.birthAD)].Width = 80;
            dgv.Columns[nameof(k.firstdateAD)].Width = 80;
            dgv.Columns[nameof(k.startdateAD)].Width = 80;
            dgv.Columns[nameof(k.finishdateAD)].Width = 80;
            dgv.Columns[nameof(k.hihonum_narrow)].Width = 80;



            dgv.Columns[nameof(k.f000_importid)].HeaderText = "ID";
            dgv.Columns[nameof(k.f001_pageins)].HeaderText = "ページ数";
            dgv.Columns[nameof(k.f002_pageall)].HeaderText = "総ページ数";
            dgv.Columns[nameof(k.f003_shinsay)].HeaderText = "審査年";
            dgv.Columns[nameof(k.f004_shinsam)].HeaderText = "審査月";
            dgv.Columns[nameof(k.f005_menjo)].HeaderText = "免除";
            dgv.Columns[nameof(k.f006_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(k.f007_insname)].HeaderText = "保険者名";
            dgv.Columns[nameof(k.f008_clinicnum)].HeaderText = "機関番号";
            dgv.Columns[nameof(k.f009_pname)].HeaderText = "受診者";
            dgv.Columns[nameof(k.f010_knum)].HeaderText = "公費負担者番号";
            dgv.Columns[nameof(k.f011_futan)].HeaderText = "負担区分";
            dgv.Columns[nameof(k.f012_startdate)].HeaderText = "施術開始年月日";
            dgv.Columns[nameof(k.f013_fukusu)].HeaderText = "複数月区分";
            dgv.Columns[nameof(k.f014_total)].HeaderText = "決定金額";
            dgv.Columns[nameof(k.f015_biko1)].HeaderText = "備考1";
            dgv.Columns[nameof(k.f016_shusei)].HeaderText = "修正区分";
            dgv.Columns[nameof(k.f017_examy)].HeaderText = "施療年";
            dgv.Columns[nameof(k.f018_examm)].HeaderText = "施療月";
            dgv.Columns[nameof(k.f019_clinicname)].HeaderText = "機関名";
            dgv.Columns[nameof(k.f020_hnum)].HeaderText = "被保険者番号";
            dgv.Columns[nameof(k.f021_pbirthe)].HeaderText = "生年月日年号";
            dgv.Columns[nameof(k.f022_pbirthy)].HeaderText = "生年月日年";
            dgv.Columns[nameof(k.f023_pbirthm)].HeaderText = "生年月日月";
            dgv.Columns[nameof(k.f024_pbirthd)].HeaderText = "生年月日日";
            dgv.Columns[nameof(k.f025_personalcode)].HeaderText = "住民コード";
            dgv.Columns[nameof(k.f026_pgender)].HeaderText = "性別";
            dgv.Columns[nameof(k.f027_jukyunum)].HeaderText = "受給者番号";
            dgv.Columns[nameof(k.f028_ratio)].HeaderText = "割合";
            dgv.Columns[nameof(k.f029_firstdate)].HeaderText = "初検年月日";
            dgv.Columns[nameof(k.f030_finishdate)].HeaderText = "施術終了年月日";
            dgv.Columns[nameof(k.f031_counteddays)].HeaderText = "実日数";
            dgv.Columns[nameof(k.f032_partial)].HeaderText = "一部負担";
            dgv.Columns[nameof(k.f033_biko2)].HeaderText = "備考2";
            dgv.Columns[nameof(k.f034_comnum)].HeaderText = "全国共通キー";
            dgv.Columns[nameof(k.cym)].HeaderText = "メホール請求年月";
            dgv.Columns[nameof(k.shinsaymAD)].HeaderText = "審査年月";
            dgv.Columns[nameof(k.ymAD)].HeaderText = "施術年月";
            dgv.Columns[nameof(k.birthAD)].HeaderText = "生年月日";
            dgv.Columns[nameof(k.firstdateAD)].HeaderText = "初検年月日";
            dgv.Columns[nameof(k.startdateAD)].HeaderText = "開始年月日";
            dgv.Columns[nameof(k.finishdateAD)].HeaderText = "終了年月日";
            dgv.Columns[nameof(k.hihonum_narrow)].HeaderText = "証番号";



            dgv.Columns[nameof(k.f000_importid)].Visible = true;
            dgv.Columns[nameof(k.f001_pageins)].Visible = false;
            dgv.Columns[nameof(k.f002_pageall)].Visible = false;
            dgv.Columns[nameof(k.f003_shinsay)].Visible = false;
            dgv.Columns[nameof(k.f004_shinsam)].Visible = false;
            dgv.Columns[nameof(k.f005_menjo)].Visible = false;
            dgv.Columns[nameof(k.f006_insnum)].Visible = false;
            dgv.Columns[nameof(k.f007_insname)].Visible = false;
            dgv.Columns[nameof(k.f008_clinicnum)].Visible = true;
            dgv.Columns[nameof(k.f009_pname)].Visible = true;
            dgv.Columns[nameof(k.f010_knum)].Visible = false;
            dgv.Columns[nameof(k.f011_futan)].Visible = false;
            dgv.Columns[nameof(k.f012_startdate)].Visible = false;
            dgv.Columns[nameof(k.f013_fukusu)].Visible = false;
            dgv.Columns[nameof(k.f014_total)].Visible = true;
            dgv.Columns[nameof(k.f015_biko1)].Visible = false;
            dgv.Columns[nameof(k.f016_shusei)].Visible = false;
            dgv.Columns[nameof(k.f017_examy)].Visible = false;
            dgv.Columns[nameof(k.f018_examm)].Visible = false;
            dgv.Columns[nameof(k.f019_clinicname)].Visible = true;
            dgv.Columns[nameof(k.f020_hnum)].Visible = false;
            dgv.Columns[nameof(k.f021_pbirthe)].Visible = false;
            dgv.Columns[nameof(k.f022_pbirthy)].Visible = false;
            dgv.Columns[nameof(k.f023_pbirthm)].Visible = false;
            dgv.Columns[nameof(k.f024_pbirthd)].Visible = false;
            dgv.Columns[nameof(k.f025_personalcode)].Visible = true;
            dgv.Columns[nameof(k.f026_pgender)].Visible = true;
            dgv.Columns[nameof(k.f027_jukyunum)].Visible = false;
            dgv.Columns[nameof(k.f028_ratio)].Visible = true;
            dgv.Columns[nameof(k.f029_firstdate)].Visible = false;
            dgv.Columns[nameof(k.f030_finishdate)].Visible = false;
            dgv.Columns[nameof(k.f031_counteddays)].Visible = true;
            dgv.Columns[nameof(k.f032_partial)].Visible = true;
            dgv.Columns[nameof(k.f033_biko2)].Visible = false;
            dgv.Columns[nameof(k.f034_comnum)].Visible = true;
            dgv.Columns[nameof(k.cym)].Visible = false;
            dgv.Columns[nameof(k.shinsaymAD)].Visible = true;
            dgv.Columns[nameof(k.ymAD)].Visible = true;
            dgv.Columns[nameof(k.birthAD)].Visible = true;
            dgv.Columns[nameof(k.firstdateAD)].Visible = true;
            dgv.Columns[nameof(k.startdateAD)].Visible = true;
            dgv.Columns[nameof(k.finishdateAD)].Visible = true;
            dgv.Columns[nameof(k.hihonum_narrow)].Visible = true;

        }

        /// <summary>
        /// 国保用グリッド
        /// </summary>
        /// <param name="app"></param>
        private void createGrid(App app = null)
        {
            List<dataimport> lstimp = new List<dataimport>();

            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();
            string strpsex = verifyBoxPsex.Text == "1" ? "男":"女";

            //リスト表示は入力項目で合致させる
            foreach (dataimport item in lstDataImport)
            {
                //被保番半角、メホール請求年月、合計金額で探す
                if (item.hihonum_narrow == verifyBoxHnum.Text.Trim() &&
                    item.cym == scan.CYM &&
                    item.f014_total.Replace(",", "") == verifyBoxTotal.Text.ToString().Trim() &&
                    item.f026_pgender==strpsex && 
                    item.ymAD==intymad)

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;
            dgv.DataSource = lstimp;

            if (lstimp.Count == 0) return;

            InitGrid();

            //1行確定の時は自動選択
            if (lstimp.Count == 1 && app.StatusFlagCheck(StatusFlag.未処理))
            {
                dgv.Rows[0].Selected = true;
            }
            else
            {

                //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
                if (lstimp.Count > 1) dgv.ClearSelection();


                //自動選択は確定したrrid
                if (app != null && app.RrID.ToString() != string.Empty)
                {
                    foreach (DataGridViewRow r in dgv.Rows)
                    {
                        //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                        string strcomnum = r.Cells["f034_comnum"].Value.ToString();

                        //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                        if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                        {

                            if (r.Cells["importid"].Value.ToString() == app.RrID.ToString())
                            {
                                r.Selected = true;
                            }

                            //提供データIDと違う場合の処理抜け
                            else
                            {
                                r.Selected = false;
                            }
                        }
                        else
                        {
                            //1回目終了時、2回目終了時は自動選択
                            if (
                                (app.ComNum == strcomnum && app.StatusFlagCheck(StatusFlag.入力済)) ||
                                (app.ComNum == strcomnum && app.StatusFlagCheck(StatusFlag.ベリファイ済))
                                )
                            {
                                //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない

                                if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                                else r.Selected = true;

                            }
                            //提供データIDと違う場合の処理                        
                            else
                            {
                                r.Selected = false;
                            }
                        }

                    }
                }
            }

            if (lstDataImport == null || lstDataImport.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }


            //マッチングOK条件に選択行が1行の場合も追加
            else if (lstDataImport.Count == 1 || dgv.SelectedRows.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }
    
        #endregion


        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //表示初期化
            pZenkai.Enabled = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

        

            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証番号
                    setValue(verifyBoxHnum, Microsoft.VisualBasic.Strings.StrConv(app.HihoNum,VbStrConv.Narrow), firstTime, nv);

                    //受療者性別
                    setValue(verifyBoxPsex, app.Sex, firstTime, nv);

                    //受療者生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);
                    

                    //負傷カウント                   
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);  

                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

                    //負傷1負傷名 本来鍼だけだが、鍼以外入力できないので空欄になるので区別しない
                    setValue(verifyBoxF1, app.FushoName1.ToString(), firstTime, nv);


                    //往療距離
                    setValue(checkBoxVisit, app.Distance == 0 ? false : true, firstTime, nv);
                    //往療加算
                    setValue(checkBoxVisitKasan, app.VisitAdd == 0 ? false : true, firstTime, nv);
                    

                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //請求金額
                    if(app.AppType != APP_TYPE.柔整) setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                    //実日数
                    setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                    //グリッド選択する                    
                    createGrid(app);
                   
                    
                    break;
            }
        }


        #endregion

        #region 各種更新


        private bool CheckNumbering(int intNumbering)
        {
            if (intPrevNumbering == 0) return true;
            if (intPrevNumbering > intNumbering) return true;//戻ったときの対策
            if (Math.Abs(intNumbering - intPrevNumbering) == 1) return true;
            else return false;
            
        }

        /// <summary>
        /// 提供データをAppに登録
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool RegistSupplyData(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {
                    dataimport k = lstDataImport[0];
                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(k.f000_importid)].Value.ToString()); //ID
                    app.ComNum = dgv.CurrentRow.Cells[nameof(k.f034_comnum)].Value.ToString();

                    //aux更新


                    //20211028143852 furukawa st ////////////////////////
                    //auxにcomnumを入れておく
                    

                    Application_AUX.Update(app.Aid, app.AppType, null, app.RrID.ToString(), app.ComNum.ToString());

                    //      20211011131529 furukawa st ////////////////////////
                    //      AUXにもApptype追加                                  

                    //      Application_AUX.Update(app.Aid, app.AppType, null, app.RrID.ToString());
                    //                Application_AUX.Update(app.Aid, null, app.RrID.ToString());
                    //      20211011131529 furukawa ed ////////////////////////

                    //20211028143852 furukawa ed ////////////////////////
                }
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
        }


        /// <summary>
        /// 提供データをappに適用
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        private bool UpdateAppWithSupplyData(App app)
        {
            StringBuilder sb = new StringBuilder();
          
            sb.AppendLine($" update application set");
            sb.AppendLine($"  inum			= d.f006_insnum");
            sb.AppendLine($" ,pname          = d.f009_pname");
            sb.AppendLine($" ,psex           = case d.f026_pgender when '男' then 1 when '女' then 2 else 0 end ");
            sb.AppendLine($" ,pbirthday      = d.birthAD");
            sb.AppendLine($" ,aratio         = case when d.f028_ratio<>'' then cast(d.f028_ratio as int) else 0 end");
            sb.AppendLine($" ,ifirstdate1    = d.firstdateAD");
            sb.AppendLine($" ,istartdate1    = d.startdateAD");
            sb.AppendLine($" ,ifinishdate1   = d.finishdateAD");
            sb.AppendLine($" ,sid            = d.f008_clinicnum");
            sb.AppendLine($" ,sname          = d.f019_clinicname");

            sb.AppendLine($" ,fchargetype    = case when cast(d.ymad as varchar) =replace(substring(cast(d.firstdatead as varchar),1,7) ,'-','') then {(int)NEW_CONT.新規} else {(int)NEW_CONT.継続} end ");

            sb.AppendLine($" ,apartial       = ");
            sb.AppendLine($" 			case when d.f032_partial<>'' then cast(replace(d.f032_partial,',','') as int) ");
            sb.AppendLine($" 			else 0 end");
            sb.AppendLine($" ,acharge		= ");
            sb.AppendLine($" 			case when d.f032_partial<>'' then atotal-cast(replace(d.f032_partial,',','') as int)");
            sb.AppendLine($" 			else 0 end");
            sb.AppendLine($" ,acounteddays                   = case when d.f031_counteddays <>'' then cast(d.f031_counteddays as int) else 0 end");
            sb.AppendLine($" ,comnum                         = d.f034_comnum");

            sb.AppendLine($" ,taggeddatas                    = ");
            sb.AppendLine($" 'count:\"' ||{app.TaggedDatas.count} || '\"|' ||");//画面入力の負傷数が消えるので取ってくる
            sb.AppendLine($" 'KouhiNum:\"' || d.f010_knum || '\"|' ||");
            sb.AppendLine($" 'JukyuNum :\"' || d.f027_jukyunum || '\"|' ||");
            sb.AppendLine($" 'GeneralString1:\"' || d.shinsaymAD || '\"|' ||");
            sb.AppendLine($" 'GeneralString2:\"' || d.f001_pageins || '\"|' ||");
            sb.AppendLine($" 'GeneralString3:\"' || d.f002_pageall || '\"|' ||");            
            sb.AppendLine($" 'GeneralString4:\"' || d.f025_personalcode || '\" '");

            sb.AppendLine($" from dataimport d               ");
            sb.AppendLine($" where                           ");
            sb.AppendLine($"  d.f000_importid={app.RrID} ");
            sb.AppendLine($" and application.aid={app.Aid}  ");
            sb.AppendLine($" and application.cym={app.CYM}  ");


            DB.Command cmd = new DB.Command(DB.Main, sb.ToString());
            //20210729213824 furukawa st ////////////////////////
            //dispose抜けてたからPoolいっぱいになってた
            
            try
            {
                if (!cmd.TryExecuteNonQuery()) return false;
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

            //            return true;
            //20210729213824 furukawa ed ////////////////////////
        }


        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証番号   
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);

                    //受療者生年月日
                    DateTime dtpbirthday = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                    //受療者性別
                    int intverifyBoxPsex = verifyBoxPsex.GetIntValue();
                    setStatus(verifyBoxPsex, intverifyBoxPsex<0 || intverifyBoxPsex>2);

                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);


                    //負傷数
                    int fushoCount = verifyBoxFushoCount.GetIntValue();
                    setStatus(verifyBoxFushoCount, fushoCount <0 || fushoCount>10);

                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }


                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月

                    app.HihoNum =hnumN;                //被保険者証番号                                       

                    //受療者性別
                    app.Sex = intverifyBoxPsex;

                    //受療者生年月日
                    app.Birthday = dtpbirthday;

                    //往療距離
                    app.Distance = checkBoxVisit.Checked ? 999:0;

                    //往療料加算
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;


                    //申請書種別
                    app.AppType = scan.AppType;

                    //負傷数   柔整のみ
                    fushoCount = app.AppType == APP_TYPE.柔整 ? fushoCount : 0;
                    app.TaggedDatas.count = fushoCount;

                    //マッチング用に合計金額入れる
                    app.Total = total;

                 
                    RegistSupplyData(app);


                    #endregion
                    break;
            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;
            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                jyuTran.Commit();
                tran.Commit();

                if (!UpdateAppWithSupplyData(app))
                {
                    MessageBox.Show("App更新失敗");
                    return false;
                }

                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            App app = (App)bsApp.Current;
            createGrid(app);
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }


            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
               
        

        private void checkBoxVisit_CheckedChanged(object sender, EventArgs e)
        {
            //往療加算は、有無がチェックされたときに有効とする　おかしな申請書が入力できなくなるのでやめとく
            //VerifyCheckBox c = (VerifyCheckBox)sender;
            //checkBoxVisitKasan.Enabled = c.Checked;
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
           // else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
