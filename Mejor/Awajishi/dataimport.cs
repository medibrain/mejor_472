﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Awajishi
{
    //20210401160124 furukawa
    /// <summary>
    /// 国保システムからのデータインポート
    /// </summary>
    public class dataimport
    {
        #region メンバ
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get;set; }=0;                                //インポートID;
        public string f001_pageins { get; set; } = string.Empty;                //保険者ページ数;
        public string f002_pageall { get; set; } = string.Empty;                //総ページ数;
        public string f003_shinsay { get; set; } = string.Empty;                //審査年;
        public string f004_shinsam { get; set; } = string.Empty;                //審査月;
        public string f005_menjo { get; set; } = string.Empty;                  //免除;
        public string f006_insnum { get; set; } = string.Empty;                 //保険者番号;
        public string f007_insname { get; set; } = string.Empty;                //保険者名;
        public string f008_clinicnum { get; set; } = string.Empty;              //機関番号;
        public string f009_pname { get; set; } = string.Empty;                  //受診者氏名;
        public string f010_knum { get; set; } = string.Empty;                   //公費負担者番号;
        public string f011_futan { get; set; } = string.Empty;                  //負担区分;
        public string f012_startdate { get; set; } = string.Empty;              //施術開始年月日;
        public string f013_fukusu { get; set; } = string.Empty;                 //複数月区分;
        public string f014_total { get; set; } = string.Empty;                  //決定金額;
        public string f015_biko1 { get; set; } = string.Empty;                  //備考1;
        public string f016_shusei { get; set; } = string.Empty;                 //修正区分;
        public string f017_examy { get; set; } = string.Empty;                  //施療年;
        public string f018_examm { get; set; } = string.Empty;                  //施療月;
        public string f019_clinicname { get; set; } = string.Empty;             //機関名;
        public string f020_hnum { get; set; } = string.Empty;                   //被保険者番号;
        public string f021_pbirthe { get; set; } = string.Empty;                //生年月日年号;
        public string f022_pbirthy { get; set; } = string.Empty;                //生年月日年;
        public string f023_pbirthm { get; set; } = string.Empty;                //生年月日月;
        public string f024_pbirthd { get; set; } = string.Empty;                //生年月日日;
        public string f025_personalcode { get; set; } = string.Empty;           //住民コード;
        public string f026_pgender { get; set; } = string.Empty;                //性別;
        public string f027_jukyunum { get; set; } = string.Empty;               //受給者番号;
        public string f028_ratio { get; set; } = string.Empty;                  //負担割合;
        public string f029_firstdate { get; set; } = string.Empty;              //初検年月日;
        public string f030_finishdate { get; set; } = string.Empty;             //施術終了年月日;
        public string f031_counteddays { get; set; } = string.Empty;            //実日数;
        public string f032_partial { get; set; } = string.Empty;                //一部負担金;
        public string f033_biko2 { get; set; } = string.Empty;                  //備考2;
        public string f034_comnum { get; set; } = string.Empty;                 //全国共通キー;
        public int cym { get; set; } = 0;                                       //メホール請求年月;
        public int shinsaymAD { get; set; } = 0;                                //審査年月西暦;
        public int ymAD { get; set; } = 0;                                      //施術年月西暦_施療年月;
        public DateTime birthAD { get; set; } = DateTime.MinValue;              //生年月日西暦;
        public DateTime firstdateAD { get; set; } = DateTime.MinValue;          //初検年月日西暦;
        public DateTime startdateAD { get; set; } = DateTime.MinValue;          //施術開始年月日西暦;
        public DateTime finishdateAD { get; set; } = DateTime.MinValue;         //施術終了年月日西暦;
        public string hihonum_narrow { get; set; } = string.Empty;              //被保険者証番号半角;



        #endregion

        public static List<dataimport> lstdataimport = new List<dataimport>();


        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        /// <returns></returns>
        public static bool ImportMain(int _cym)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.InvokeValue = 0;

            try
            {
                if (!LoadFile(wf, _cym)) return false;

                //db登録
                if (!EntryDB(wf, _cym)) return false;

                System.Windows.Forms.MessageBox.Show("取込完了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("取込失敗");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }



        /// <summary>
        /// db登録
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryDB(WaitForm wf, int _cym)
        {

            wf.InvokeValue = 0;
            wf.SetMax(lstdataimport.Count);
            
            DB.Transaction tran = new DB.Transaction(DB.Main);

            try
            {
                wf.LogPrint("DB削除");
                DB.Command cmd = new DB.Command($"delete from dataimport where cym={_cym}", tran);
                cmd.TryExecuteNonQuery();

                wf.LogPrint("DB登録");
                foreach (dataimport cls in lstdataimport)
                {
                    if (!DB.Main.Insert<dataimport>(cls, tran)) return false;
                    wf.InvokeValue++;
                }

                tran.Commit();
                cmd.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    $"{wf.Value + 1}行目" + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

            }
        }


        /// <summary>
        /// 西暦に変換
        /// </summary>
        /// <param name="strtmp">yy.mm.dd型</param>
        /// <returns>datetime</returns>
        private static DateTime ChangeToADYM(string strtmp)
        {
            if (strtmp == string.Empty) return DateTime.MinValue;

            if (!strtmp.Contains('.')) return DateTime.MinValue;

            string firstdatey = strtmp.Split('.')[0].ToString();
            string firstdatem = strtmp.Split('.')[1].ToString();
            string firstdated = strtmp.Split('.')[2].ToString();

            int firstdateymad = DateTimeEx.GetAdYearFromHs(int.Parse(firstdatey + firstdatem)) * 100 + int.Parse(firstdatem);

            return DateTimeEx.ToDateTime(firstdateymad * 100 + int.Parse(firstdated));

            
        }


        private static string tryGetValue(string tmp)
        {
            if (tmp == string.Empty) return string.Empty;
            else return tmp;
        }

        /// <summary>
        /// csvをロードする
        /// </summary>
        /// <returns></returns>
        private static bool LoadFile(WaitForm wf, int _cym)
        {

            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();


            int colNum = 0;
            lstdataimport.Clear();

            try
            {
                dlg.Filter = "提供データCSVファイル|*.csv";
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return false;

                string strFileName = dlg.FileName;


                //CSV内容ロード
                //20220208104248 furukawa st ////////////////////////
                //CSVインポート 文字コード自動判定列数制限                
                if (!CommonTool.CsvImportMultiCodeLimitCols(strFileName, out List<string[]> lst, 34)) return false;
                //      List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);
                //20220208104248 furukawa ed ////////////////////////


                wf.LogPrint("CSVロード");
                wf.SetMax(lst.Count);


                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {

                    if (CommonTool.WaitFormCancelProcess(wf)) return false;

                    try
                    {
                        //決定金額で判断→レセプト全国共通キーにした方が確実
                        if (tmp[33].ToString() == string.Empty) throw new ArgumentNullException();

                        System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[0-9]{20}");
                        if (!reg.IsMatch(tmp[33].ToString())) throw new ArgumentException();

                        
                    }
                    catch(Exception ex)
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataimport cls = new dataimport();
                    colNum = 0;

                    //if (!int.TryParse(tmp[0], out int tmp0)) return false; else cls.f001_pageins = int.Parse(tmp0.ToString());

                    cls.f001_pageins =tmp[colNum++];     //保険者ページ数;
                    cls.f002_pageall =tmp[colNum++];     //総ページ数;
                    cls.f003_shinsay =tmp[colNum++];     //審査年;
                    cls.f004_shinsam =tmp[colNum++];     //審査月;                
                    cls.f005_menjo = tmp[colNum++];                                          //免除;
                    cls.f006_insnum = tmp[colNum++];                                         //保険者番号;
                    cls.f007_insname = tmp[colNum++];                                        //保険者名;
                    cls.f008_clinicnum = tmp[colNum++];                                      //機関番号;
                    cls.f009_pname = tmp[colNum++];                                          //受診者氏名;
                    cls.f010_knum = tmp[colNum++];                                           //公費負担者番号;
                    cls.f011_futan = tmp[colNum++];                                          //負担区分;
                    cls.f012_startdate = tmp[colNum++];                                      //施術開始年月日;
                    cls.f013_fukusu = tmp[colNum++];                                         //複数月区分;
                    cls.f014_total = tmp[colNum++];  //決定金額;
                    cls.f015_biko1 = tmp[colNum++];                                          //備考1;
                    cls.f016_shusei = tmp[colNum++];                                         //修正区分;
                    cls.f017_examy = tmp[colNum++]; //施療年;
                    cls.f018_examm = tmp[colNum++]; //施療月;                    
                    cls.f019_clinicname = tmp[colNum++];                                     //機関名;
                    cls.f020_hnum = tmp[colNum++];                                           //被保険者番号;
                    cls.f021_pbirthe = tmp[colNum++];                                        //生年月日年号;
                    cls.f022_pbirthy = tmp[colNum++];       //生年月日年;
                    cls.f023_pbirthm = tmp[colNum++];       //生年月日月;
                    cls.f024_pbirthd = tmp[colNum++];       //生年月日日;
                    cls.f025_personalcode = tmp[colNum++];                                   //住民コード;
                    cls.f026_pgender = tmp[colNum++];                                        //性別;
                    cls.f027_jukyunum = tmp[colNum++];                                       //受給者番号;
                    cls.f028_ratio = tmp[colNum++];//負担割合;
                    cls.f029_firstdate = tmp[colNum++];                                      //初検年月日;
                    cls.f030_finishdate = tmp[colNum++];                                     //施術終了年月日;
                    cls.f031_counteddays = tmp[colNum++];              //実日数;
                    cls.f032_partial = tmp[colNum++]; //一部負担金;


                    cls.f033_biko2 = tmp[colNum++];                                          //備考2;
                    cls.f034_comnum = tmp[colNum++];                                         //全国共通キー;

                    wf.LogPrint($"CSVロード　レセプト全国共通キー : {cls.f034_comnum}");


                    //被保険者証番号半角;
                    cls.hihonum_narrow = 
                        Microsoft.VisualBasic.Strings.StrConv($"{cls.f020_hnum}",Microsoft.VisualBasic.VbStrConv.Narrow);

                    //西暦を控えておく
                    cls.ymAD = DateTimeEx.GetAdYearFromHs(int.Parse(cls.f017_examy.ToString()+cls.f018_examm.ToString()))*100+int.Parse( cls.f018_examm.ToString());            //施術年月西暦_施療年月;
                    cls.shinsaymAD = DateTimeEx.GetAdYearFromHs(int.Parse(cls.f003_shinsay.ToString())*100 + int.Parse(cls.f004_shinsam.ToString()))*100 +
                        int.Parse(cls.f004_shinsam.ToString());//審査年月西暦


                    cls.firstdateAD = ChangeToADYM(cls.f029_firstdate);     //初検年月日西暦;
                    cls.startdateAD = ChangeToADYM(cls.f012_startdate);     //施術開始年月日西暦;
                    cls.finishdateAD = ChangeToADYM(cls.f030_finishdate);   //施術終了年月日西暦;

                    cls.birthAD = warekiToAD(cls.f021_pbirthe, cls.f022_pbirthy, cls.f023_pbirthm, cls.f024_pbirthd);        //生年月日西暦;

                    cls.cym = _cym;//メホール請求年月;


                    lstdataimport.Add(cls);

                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                    $"{wf.Value + 1}行目{colNum}列目" + "\r\n" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 生年月日専用
        /// </summary>
        /// <param name="e">年号漢字1文字</param>
        /// <param name="y">和暦年</param>
        /// <param name="m"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        private static DateTime warekiToAD(string  e,string y ,string m ,string d)
        {
            System.Text.RegularExpressions.Regex regNumeric = new System.Text.RegularExpressions.Regex("[0-9]{2}");
            if (!regNumeric.IsMatch(y)) return DateTime.MinValue;
            if (!regNumeric.IsMatch(m)) return DateTime.MinValue;
            if (!regNumeric.IsMatch(d)) return DateTime.MinValue;
            if (e == string.Empty) return DateTime.MinValue;

            int inty = int.Parse(y);
            int intm = int.Parse(m); 
            int intd = int.Parse(d); 


            DateTime ad = new DateTime();
            int era=DateTimeEx.GetEraNumber(e);
            int ymad=DateTimeEx.GetAdYearMonthFromJyymm(era * 10000 + inty * 100 + intm);
            ad=DateTimeEx.ToDateTime(ymad * 100 + intd);
            return ad;
        }
     
        /// <summary>
        /// データをクラスに入れる
        /// </summary>
        /// <param name="tmp"></param>
        /// <returns></returns>
        private static dataimport GetData(object[] tmp)
        {
            int colNum = 0;
            dataimport cls = new dataimport();

            cls.f000_importid = int.Parse(tmp[colNum++].ToString());
            cls.f001_pageins = tmp[colNum++].ToString();           //保険者ページ数;
            cls.f002_pageall = tmp[colNum++].ToString();           //総ページ数;
            cls.f003_shinsay = tmp[colNum++].ToString();           //審査年;
            cls.f004_shinsam = tmp[colNum++].ToString();           //審査月;                
            cls.f005_menjo = tmp[colNum++].ToString();             //免除;
            cls.f006_insnum = tmp[colNum++].ToString();            //保険者番号;
            cls.f007_insname = tmp[colNum++].ToString();           //保険者名;
            cls.f008_clinicnum = tmp[colNum++].ToString();         //機関番号;
            cls.f009_pname = tmp[colNum++].ToString();             //受診者氏名;
            cls.f010_knum = tmp[colNum++].ToString();              //公費負担者番号;
            cls.f011_futan = tmp[colNum++].ToString();             //負担区分;
            cls.f012_startdate = tmp[colNum++].ToString();         //施術開始年月日;
            cls.f013_fukusu = tmp[colNum++].ToString();            //複数月区分;
            cls.f014_total = tmp[colNum++].ToString();             //決定金額;
            cls.f015_biko1 = tmp[colNum++].ToString();             //備考1;
            cls.f016_shusei = tmp[colNum++].ToString();            //修正区分;
            cls.f017_examy = tmp[colNum++].ToString();             //施療年;
            cls.f018_examm = tmp[colNum++].ToString();             //施療月;                    
            cls.f019_clinicname = tmp[colNum++].ToString();        //機関名;
            cls.f020_hnum = tmp[colNum++].ToString();              //被保険者番号;
            cls.f021_pbirthe = tmp[colNum++].ToString();           //生年月日年号;
            cls.f022_pbirthy = tmp[colNum++].ToString();           //生年月日年;
            cls.f023_pbirthm = tmp[colNum++].ToString();           //生年月日月;
            cls.f024_pbirthd = tmp[colNum++].ToString();           //生年月日日;
            cls.f025_personalcode = tmp[colNum++].ToString();      //住民コード;
            cls.f026_pgender = tmp[colNum++].ToString();           //性別;
            cls.f027_jukyunum = tmp[colNum++].ToString();          //受給者番号;
            cls.f028_ratio = tmp[colNum++].ToString();             //負担割合;
            cls.f029_firstdate = tmp[colNum++].ToString();         //初検年月日;
            cls.f030_finishdate = tmp[colNum++].ToString();        //施術終了年月日;
            cls.f031_counteddays = tmp[colNum++].ToString();       //実日数;
            cls.f032_partial = tmp[colNum++].ToString();           //一部負担金;


            cls.f033_biko2 = tmp[colNum++].ToString();             //備考2;
            cls.f034_comnum = tmp[colNum++].ToString();            //全国共通キー;


            cls.cym = int.Parse(tmp[colNum++].ToString());                               //メホール請求年月
            cls.shinsaymAD = int.Parse(tmp[colNum++].ToString());                       //審査年月西暦
            cls.ymAD = int.Parse(tmp[colNum++].ToString());                             //施術年月西暦_施療年月
            cls.birthAD = DateTime.Parse(tmp[colNum++].ToString());                      //生年月日西暦
            cls.firstdateAD = DateTime.Parse(tmp[colNum++].ToString());                  //初検年月日西暦
            cls.startdateAD = DateTime.Parse(tmp[colNum++].ToString());                  //施術開始年月日西暦
            cls.finishdateAD = DateTime.Parse(tmp[colNum++].ToString());                 //施術終了年月日西暦
            cls.hihonum_narrow = tmp[colNum++].ToString();                               //被保険者証番号半角

            return cls;
        }

        /// <summary>
        /// importidで提供データ取得
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<dataimport> GetDataByID(int id)
        {
            lstdataimport.Clear();

            DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport where f000_importid={id}");
            List<object[]> lst = cmd.TryExecuteReaderList();
            

            //クラスのリストに登録
            foreach (object[] tmp in lst)
            {
                try
                {
                    if (tmp[34].ToString() == string.Empty) throw new ArgumentNullException();

                    System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[0-9]{20}");
                    if (!reg.IsMatch(tmp[34].ToString())) throw new ArgumentException();

                }
                catch
                {
                    continue;
                }
              
                lstdataimport.Add(GetData(tmp));


            }


            return lstdataimport;
        }

        /// <summary>
        /// 処理年月単位で取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<dataimport> GetDataByCYM(int cym)
        {
            lstdataimport.Clear();

            //データ取得
            DB.Command cmd = new DB.Command(DB.Main, $"select * from dataimport where cym={cym}");
            List<object[]> lst = cmd.TryExecuteReaderList();
            

            try
            {


                //クラスのリストに登録
                foreach (object[] tmp in lst)
                {
                    try
                    {
                        if (tmp[34].ToString() == string.Empty) throw new ArgumentNullException();

                        System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("[0-9]{20}");
                        if (!reg.IsMatch(tmp[34].ToString())) throw new ArgumentException();

                    }
                    catch
                    {
                        continue;
                    }

                   

                    lstdataimport.Add(GetData(tmp));

                    
                }

           
                return lstdataimport;

            }


            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +
                     ex.Message);
                return lstdataimport;
            }
        }
    

        /// <summary>
        /// 該当年月の件数を返す
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static int GetCountCYM(int _cym)
        {
            
            DB.Command cmd = new DB.Command(DB.Main, $"select count(*) from dataimport where cym={_cym} group by cym");
            List<object[]> lst = cmd.TryExecuteReaderList();
            if (lst.Count == 0) return 0;

            return int.Parse(lst[0].GetValue(0).ToString());

        }

        /// <summary>
        /// 件数表示用datatableを返す
        /// </summary>
        /// <returns></returns>
        public static System.Data.DataTable GetDispCount()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            
            DB.Command cmd = new DB.Command(DB.Main, $"select cym,count(*) from dataimport group by cym order by cym desc");
            List<object[]> lst=cmd.TryExecuteReaderList();

            dt.Columns.Add("cym");
            dt.Columns.Add("count");
            try
            {
                foreach (object[] obj in lst)
                {
                    System.Data.DataRow dr = dt.NewRow();
                    dr[0] = obj[0].ToString();
                    dr[1] = obj[1].ToString();
                    dt.Rows.Add(dr);
                }
                dt.AcceptChanges();
                return dt;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" +ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
