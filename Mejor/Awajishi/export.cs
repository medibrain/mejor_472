﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Awajishi

{
    /// <summary>
    /// エクスポートデータ
    /// </summary>
    class export
    {

        #region テーブル構造
        [DB.DbAttribute.Serial]
        public string id { get;set; }=string.Empty;                      //ID=aidといっているが、変更があるかも知れんので、下にも追加してある
        public string numbering { get; set; } = string.Empty;            //ナンバリング;
        public string shinsaYM { get; set; } = string.Empty;             //審査年月;
        public string shoriYM { get; set; } = string.Empty;              //処理年月;
        public string hnum { get; set; } = string.Empty;                 //被保険者番号;
        public string honke { get; set; } = string.Empty;                //本人/家族;
        public string pname { get; set; } = string.Empty;                //氏名;
        public string psex { get; set; } = string.Empty;                 //性別;
        public string pbirthday { get; set; } = string.Empty;            //生年月日;
        public string haddress { get; set; } = string.Empty;             //住所;
        public string ym { get; set; } = string.Empty;                   //診療年月;
        public string counteddays { get; set; } = string.Empty;          //診療日数;
        public string fushocount { get; set; } = string.Empty;           //負傷数;
        public string total { get; set; } = string.Empty;                //合計金額;
        public string charge { get; set; } = string.Empty;               //請求金額;
        public string sid { get; set; } = string.Empty;                  //機関コード;
        public string sname { get; set; } = string.Empty;                //医療機関名;
        public string shokaireason { get; set; } = string.Empty;         //照会理由;
        public string result { get; set; } = string.Empty;               //点検結果;
        public string memo { get; set; } = string.Empty;                 //メモ;
        public int cym { get; set; } = 0;                                //メホール請求年月;
        public int aid { get; set; } = 0;                                //aid;




        #endregion


        /// <summary>
        /// マルチTIFF候補リスト（tiffファイルのパスが入る）
        /// </summary>
        public static List<string> lstImageFilePath = new List<string>();



        /// <summary>
        /// エクスポート
        /// </summary>
        /// <returns></returns>
        public static bool dataexport_main(int cym)
        {
            
            //保存場所選択
            OpenDirectoryDiarog dlg = new OpenDirectoryDiarog();
            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return false;
            string strBaseDir = dlg.Name;


            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
               
                wf.LogPrint("出力テーブル作成");

                //出力データ用テーブルに登録
                if (!InsertExportTable( cym, wf)) return false;


                //データ出力フォルダ作成
                string strNohinDir = strBaseDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd_HHmmss")}出力";
                if (!System.IO.Directory.Exists(strNohinDir)) System.IO.Directory.CreateDirectory(strNohinDir);


                //全出力データ取得
                List<export> lstExport = DB.Main.Select<export>($"cym={cym}").ToList();
                lstExport.Sort((x, y) => x.aid.CompareTo(y.aid));


                //csv出力
                if (!Export(lstExport, strNohinDir, wf)) return false;

                ////画像出力
                //if (!ExportImage(lstExport, strNohinDir, wf)) return false;

                wf.LogPrint("終了");
                System.Windows.Forms.MessageBox.Show("終了");
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                wf.Dispose();
            }

        }


        /// <summary>
        /// 出力用テーブルに登録
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// {<param name="wf">waitform</param>
        /// <returns></returns>
        private static bool InsertExportTable(int cym ,WaitForm wf)
            //private static bool InsertExportTable(List<App> lstApp, int cym, WaitForm wf)
        {
            
            DB.Transaction tran;
            tran = DB.Main.CreateTransaction();
                        
            //今月分を削除
            DB.Command cmddel = new DB.Command($"delete from export where cym='{cym}'", tran);
            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

            try
            {
                cmddel.TryExecuteNonQuery();
                int c = 0;
                string tmpShisaym = string.Empty;//続紙用

                List<App> lstapp = App.GetApps(cym);
                wf.SetMax(lstapp.Count);

                foreach (App a in lstapp)
                {
                    export exp = new export();
                    exp.aid = a.Aid;                                                //ID=aidといっているが、変更があるかも知れんので、下にも追加してある
                    exp.numbering = a.TaggedDatas.GeneralString1;                   //ナンバリング;
                    exp.shinsaYM = a.TaggedDatas.GeneralString3;                    //審査年月;
                    exp.shoriYM = DateTimeEx.GetEraJpYearMonth(a.CYM);              //処理年月;
                    exp.hnum = a.HihoNum;                                           //被保険者番号;
                    exp.honke = a.Family == 2 ? "本人" : "家族";                    //本人/家族;
                    exp.pname = a.PersonName;                                       //氏名;
                    exp.psex = a.Sex == 1 ? "男" : "女";                            //性別;
                    exp.pbirthday = DateTimeEx.GetJpDateStr(a.Birthday);            //生年月日;
                    exp.haddress = a.HihoAdd;                                       //住所;

                    exp.ym = a.YM > 0 ? DateTimeEx.GetEraJpYearMonth(a.YM).ToString() : "";         //診療年月;

                    exp.counteddays = a.CountedDays == 0 ? "": a.CountedDays.ToString();                     //診療日数;
                    exp.fushocount =a.TaggedDatas.count==0? "": a.TaggedDatas.count.ToString();                //負傷数;
                    exp.total = a.Total == 0 ? "" : a.Total.ToString();                                 //合計金額;
                    exp.charge = a.Charge == 0 ? "" : a.Charge.ToString();                               //請求金額;
                    exp.sid = a.ClinicNum;                                          //機関コード;
                    exp.sname = a.ClinicName;                                       //医療機関名;
                    exp.shokaireason = a.ShokaiReasonStr;                           //照会理由;
                    exp.result = a.TenkenResult;                                    //点検結果;
                    exp.memo = a.OutMemo;                                           //メモ;
                    exp.cym = a.CYM;                                                //メホール請求年月;
                    exp.aid = a.Aid;                                                //aid;

               
                    if (!DB.Main.Insert<export>(exp, tran)) return false;


                    wf.LogPrint($"aid:{exp.aid}");
                    wf.InvokeValue++;

                }
              

                tran.Commit();
                wf.LogPrint($"出力テーブル登録完了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +"\r\n"+ ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
              
                cmddel.Dispose();
            }

               
        }


        /// <summary>
        /// 不要以外画像出力
        /// </summary>
        /// <param name="lstExport">出力するリスト</param>
        /// <param name="strDir">出力先</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool ExportImage(List<export> lstExport , string strDir, WaitForm wf)
        {
            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}\\Images";           //tifコピー先フォルダ
       
            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();
            wf.InvokeValue = 0;
            wf.SetMax(lstExport.Count);

            try
            {
                //出力先パス
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";


                //0番目のファイルパス
             //   imageName = $"{strImageDir}\\{lstExport[0].imagefilename}";

                for (int r = 0; r < lstExport.Count(); r++)
                {
                    
                    //レセプト全国共通キーが20桁の場合、申請書と見なす

                    //if ((lstExport[r].comnum.Length==20)  && (lstImageFilePath.Count != 0))
                    //{
                        //申請書レコード　かつ　マルチTIFFにするリストが1件超の場合＝2件目以降のレコード
                        //まず　マルチTIFF候補リストにあるファイルを、マルチTIFFファイルとして保存し、マルチTIFF候補リストをクリアする
                        //その上で、この申請書をマルチTIFF候補リストに追加
                        wf.LogPrint($"申請書出力中:{imageName}");
                        if (!TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName))
                        {
                            System.Windows.Forms.MessageBox.Show(
                                System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n画像出力に失敗しました");
                            return false;
                        }

                        lstImageFilePath.Clear();                        
                        lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
                   //     imageName = $"{strImageDir}\\{lstExport[r].imagefilename}";
                    //}                   
                    //else if ((lstExport[r].comnum.Length==20) && (lstImageFilePath.Count == 0))
                    //{
                    //    //申請書レコード　かつ　マルチTIFF候補リストが0件の場合＝1件目のレコード
                    //    lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
                    //    wf.LogPrint($"申請書出力中:{imageName}");
                    //}

                    //else if (new string[] { "-1", "-3", "-5", "-6", "-9", "-11", "-13", "-14"}.Contains(lstExport[r].comnum))
                    //{
                    //    //申請書以外の、不要ファイル以外をマルチTIFF候補ととして追加
                    //    lstImageFilePath.Add(App.GetApp(lstExport[r].aid).GetImageFullPath());
                    //    wf.LogPrint($"申請書以外:{imageName}");
                    //}
               
                    
                    wf.InvokeValue++;

                }


                //最終画像出力
                //ループの最後の画像を出力
                if (lstImageFilePath.Count != 0 && !string.IsNullOrWhiteSpace(imageName))
                    TiffUtility.MargeOrCopyTiff(fc, lstImageFilePath, imageName);


                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            
        }


        /// <summary>
        /// DBからデータを取得してCSV出力まで
        /// <paramref name="lstExport"/>出力リスト</param>
        /// <param name="strDir">出力フォルダ名</param>
        /// </summary>
        /// <returns></returns>
        private static bool Export(List<export> lstExport ,string strDir,WaitForm wf)            
        {

            //出力csv名
            string strFileName = $"{strDir}\\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_茨木市国保.xlsx";


            string imageName = string.Empty;                    //tifコピー先の名前
            string strImageDir = $"{strDir}\\Images";           //tifコピー先フォルダ

      
            System.IO.Directory.CreateDirectory(strDir);            //納品フォルダ
            System.IO.Directory.CreateDirectory(strImageDir);       //納品フォルダ\image                       

            wf.LogPrint("ファイル作成中");
            wf.InvokeValue = 0;
            wf.SetMax(lstExport.Count<export>());

            #region NPOIコード
            System.IO.FileStream fs = new System.IO.FileStream(strFileName,System.IO.FileMode.Create,System.IO.FileAccess.Write);           
            NPOI.SS.UserModel.IWorkbook wb = new NPOI.XSSF.UserModel.XSSFWorkbook();
            NPOI.SS.UserModel.ISheet ws = wb.CreateSheet();
            #endregion

            TiffUtility.FastCopy fc = new TiffUtility.FastCopy();

            try
            {
                string strNohinDir = strDir + $"\\{DateTime.Now.ToString("yyyy-MM-dd")}出力";

                #region ヘッダ行
                NPOI.SS.UserModel.IFont font = wb.CreateFont();
                font.FontName = "ＭＳ ゴシック";
                font.FontHeightInPoints = 9;
                NPOI.SS.UserModel.ICellStyle csHeader = wb.CreateCellStyle();
                csHeader.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                csHeader.FillForegroundColor = NPOI.SS.UserModel.IndexedColors.PaleBlue.Index;
                csHeader.FillPattern = NPOI.SS.UserModel.FillPattern.SolidForeground;
                csHeader.SetFont(font);

                NPOI.SS.UserModel.IRow row = ws.CreateRow(0);

                for (int r = 0; r < 20; r++)
                {
                    NPOI.SS.UserModel.ICell hcell = row.CreateCell(r);
                    hcell.CellStyle = csHeader;

                   if(r==0) hcell.SetCellValue("ID");
                   if (r == 1) hcell.SetCellValue("ナンバリング");
                   if(r==2) hcell.SetCellValue("審査年月");
                   if(r==3) hcell.SetCellValue("処理年月");
                   if(r==4) hcell.SetCellValue("被保険者番号");
                   if(r==5) hcell.SetCellValue("本人/家族");
                   if(r==6) hcell.SetCellValue("氏名");
                   if(r==7) hcell.SetCellValue("性別");
                   if(r==8) hcell.SetCellValue("生年月日");
                   if(r==9) hcell.SetCellValue("住所");
                   if(r==10)hcell.SetCellValue("診療年月");
                   if(r==11) hcell.SetCellValue("診療日数");
                   if(r==12) hcell.SetCellValue("負傷数");
                   if(r==13) hcell.SetCellValue("合計金額");
                   if(r==14) hcell.SetCellValue("請求金額");
                   if(r==15) hcell.SetCellValue("機関コード");
                   if(r==16) hcell.SetCellValue("医療機関名");
                   if(r==17) hcell.SetCellValue("照会理由");
                   if(r==18) hcell.SetCellValue("点検結果");
                   if(r==19) hcell.SetCellValue("メモ");
                   
                }
                #endregion


                for (int r = 0; r < lstExport.Count(); r++)
                {
                    row = ws.CreateRow(r+1);

                    string strres = string.Empty;
                    imageName = $"{strImageDir}\\{lstExport[r].numbering}.tif";
                    
                    for (int colcnt = 0; colcnt < 20; colcnt++)
                    {
                        NPOI.SS.UserModel.ICell cell = row.CreateCell(colcnt);


                        if (colcnt == 0) cell.SetCellValue(lstExport[r].aid);
                        if (colcnt == 1) cell.SetCellValue(lstExport[r].numbering);
                        if (colcnt == 2) cell.SetCellValue(lstExport[r].shinsaYM);
                        if (colcnt == 3) cell.SetCellValue(lstExport[r].shoriYM);
                        if (colcnt == 4) cell.SetCellValue(lstExport[r].hnum);
                        if (colcnt == 5) cell.SetCellValue(lstExport[r].honke);
                        if (colcnt == 6) cell.SetCellValue(lstExport[r].pname);
                        if (colcnt == 7) cell.SetCellValue(lstExport[r].psex);
                        if (colcnt == 8) cell.SetCellValue(lstExport[r].pbirthday);
                        if (colcnt == 9) cell.SetCellValue(lstExport[r].haddress);
                        if (colcnt == 10) cell.SetCellValue(lstExport[r].ym);
                        if (colcnt == 11) cell.SetCellValue(lstExport[r].counteddays);
                        if (colcnt == 12) cell.SetCellValue(lstExport[r].fushocount);
                        if (colcnt == 13) cell.SetCellValue(lstExport[r].total);
                        if (colcnt == 14) cell.SetCellValue(lstExport[r].charge);
                        if (colcnt == 15) cell.SetCellValue(lstExport[r].sid);
                        if (colcnt == 16) cell.SetCellValue(lstExport[r].sname);
                        if (colcnt == 17) cell.SetCellValue(lstExport[r].shokaireason);
                        if (colcnt == 18) cell.SetCellValue(lstExport[r].result);
                        if (colcnt == 19) cell.SetCellValue(lstExport[r].memo);

                        row.Cells.Add(cell);
                    }
                    


                    App a = App.GetApp(lstExport[r].aid);
                    string strImageFileName = a.GetImageFullPath(DB.GetMainDBName());
                    System.Diagnostics.Debug.Print(strImageFileName);

                    //被保番があるレコードのみ画像をコピー
                    if (lstExport[r].hnum != string.Empty)
                    {
                        wf.LogPrint($"画像ファイルコピー中:{strImageFileName}");
                        fc.FileCopy(strImageFileName, imageName);
                    }


                    
                    wf.InvokeValue++;
                }

               
                for (int c = 0; c < 20; c++) ws.AutoSizeColumn(c);

                wb.Write(fs);
                
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n"+ ex.Message);
                return false;
            }
            finally
            {
                #region npoiコード
                wb.Close();
                fs.Close();
                //cmd.Dispose();
                //sw.Close();            
                #endregion

            }

        }

    }
}
