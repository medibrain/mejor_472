﻿namespace Mejor.SharpKenpo
{
    partial class ImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnImp02 = new System.Windows.Forms.Button();
            this.btnImp01 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnImp03 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(32, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(10, 309);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Visible = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCancel.Location = new System.Drawing.Point(65, 276);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(157, 25);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(64, 177);
            this.btnImport.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(157, 27);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "柔整";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Visible = false;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnImp02
            // 
            this.btnImp02.Location = new System.Drawing.Point(66, 76);
            this.btnImp02.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnImp02.Name = "btnImp02";
            this.btnImp02.Size = new System.Drawing.Size(157, 27);
            this.btnImp02.TabIndex = 5;
            this.btnImp02.Text = "資格適用データ";
            this.btnImp02.UseVisualStyleBackColor = true;
            this.btnImp02.Click += new System.EventHandler(this.btnImp2_Click);
            // 
            // btnImp01
            // 
            this.btnImp01.Location = new System.Drawing.Point(66, 43);
            this.btnImp01.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnImp01.Name = "btnImp01";
            this.btnImp01.Size = new System.Drawing.Size(157, 27);
            this.btnImp01.TabIndex = 6;
            this.btnImp01.Text = "外部機関";
            this.btnImp01.UseVisualStyleBackColor = true;
            this.btnImp01.Click += new System.EventHandler(this.btnImp01_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(64, 210);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(157, 27);
            this.button3.TabIndex = 7;
            this.button3.Text = "送付先";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(65, 243);
            this.button4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(157, 27);
            this.button4.TabIndex = 8;
            this.button4.Text = "被保険者";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnImp03
            // 
            this.btnImp03.Location = new System.Drawing.Point(65, 134);
            this.btnImp03.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnImp03.Name = "btnImp03";
            this.btnImp03.Size = new System.Drawing.Size(157, 27);
            this.btnImp03.TabIndex = 9;
            this.btnImp03.Text = "医科レセデータ";
            this.btnImp03.UseVisualStyleBackColor = true;
            this.btnImp03.Click += new System.EventHandler(this.btnImp03_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "資格適用データは本人、家族両方選択ください";
            // 
            // ImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 331);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnImp03);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnImp01);
            this.Controls.Add(this.btnImp02);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.dataGridView1);
            this.MaximizeBox = false;
            this.Name = "ImportForm";
            this.Text = "シャープ健保データインポート";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnImp02;
        private System.Windows.Forms.Button btnImp01;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnImp03;
        private System.Windows.Forms.Label label1;
    }
}