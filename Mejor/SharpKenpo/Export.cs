﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using NpgsqlTypes;
using System.Windows.Forms;

namespace Mejor.SharpKenpo
{
    //20190205115835 furukawa シャープ健保用モジュール群

    class Export
    {

        //20190215152630 furukawa st ////////////////////////
        //一般とくたいの区別

        static class Type_Ippan
        {
            public static string dir { get; set; } ="\\06";
            public static string HokensyaNo { get; } = "06271779";          
        }
        
        static class Type_Tokutai
        {
            public static string dir { get; set; } = "\\63";
            public static string HokensyaNo { get; } = "63271779";

        }

        //20190215152630 furukawa ed ////////////////////////


        

        const int ZOKUSHI_CODE = -3;

        const string DEFALT_ERA = "4";



        //シャープ健保　も90万から
        const int START_NUM = 900001;   //愛知のナンバリングは90万番台から


        //20190214140511 furukawa 納品フォルダ名
        private static string cymforFolder = string.Empty;

        /// <summary>
        /// 特定の請求月のAppに対し、連番を付与します
        /// 万が一申請書と続紙以外のデータがあった場合、不正データが作成されてしまいます
        /// また、この関数を呼び出すと、すでに振られている連番は上書きされます
        /// </summary>
        /// <param name="cyear"></param>
        /// <param name="cmonth"></param>
        /// <returns></returns>
        public static bool Numbering(int cym)
        {
            List<object[]> res;


            //20190214140432 furukawa st ////////////////////////
            //納品フォルダ名
            cymforFolder = "\\" + cym.ToString();
            //20190214140432 furukawa ed ////////////////////////


            //続紙以外のAppを取得
            using (var cmd = DB.Main.CreateCmd("SELECT aid, ayear, numbering FROM application " +
                "WHERE cym=:cym AND ayear<>:ay " +
                "ORDER BY aid"))
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                cmd.Parameters.Add("ay", NpgsqlDbType.Integer).Value = ZOKUSHI_CODE;
                res = cmd.TryExecuteReaderList();
            }

            if (res == null) return false;

            //既にナンバリングされているかどうか確認
            int numcheck = 0;
            for (int i = 0; i < res.Count; i++)
            {
                if (res[i][2].ToString().Length != 0) numcheck++;
            }
            
            if (numcheck == res.Count)
            {
                var r = MessageBox.Show("既にナンバリングされています。再度ナンバリングを実行しますか？",
                    "シャープ健保 提出データ出力", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.No) return true;
            }
            else if (numcheck != 0)
            {
                var r = MessageBox.Show(numcheck + 
                    "件のナンバリングされていないデータがあります。再度ナンバリングを実行しますか？",
                    "シャープ健保 提出データ出力", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (r == DialogResult.No) return true;
            }

            //連番をDBのデータへ付与
            using (var tran = DB.Main.CreateTransaction())
            using (var cmd = DB.Main.CreateCmd("UPDATE application " +
                "SET numbering=:num WHERE aid=:aid;", tran))
            {
                cmd.Parameters.Add("num", NpgsqlDbType.Text);
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer);

                for (int i = 0; i < res.Count; i++)
                {
                    cmd.Parameters["num"].Value = (i + START_NUM).ToString();
                    cmd.Parameters["aid"].Value = (int)res[i][0];
                    if (!cmd.TryExecuteNonQuery())
                    {
                        tran.Rollback();
                        return false;
                    }
                }

                tran.Commit();
            }
            return true;
        }



        //20190214140551 furukawa 納品ファイル作成
        /// <summary>
        /// 納品データ作成
        /// </summary>
        /// <param name="dirName"></param>
        /// <param name="appList"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool DoExport(string dirName, List<App> appList, WaitForm wf)
        {
                        
            if (!DoExport(dirName + cymforFolder + Type_Ippan.dir, Type_Ippan.HokensyaNo, appList, wf)) return false;
            if (!DoExport(dirName + cymforFolder + Type_Tokutai.dir,Type_Tokutai.HokensyaNo, appList, wf)) return false;

            return true;
        }


        /// <summary>
        /// 保存先ディレクトリと対象処理月1ヶ月分すべてのAppを指定し、エクスポートを行います。
        /// aidの順番にソートしておく必要があります。
        /// </summary>
        /// <param name="dirName"></param>
        /// <param name="appList"></param>
        /// <returns></returns>
         
            //20190221091145 furukawa st ////////////////////////
        //一回のエクスポートでは無理なのでもとのDoExportをラップした
        private static bool DoExport(string dirName , string strHokensyaNo, List<App> appList, WaitForm wf)
        //public static bool DoExport(string dirName, List<App> appList, WaitForm wf)
        //20190221091145 furukawa ed ////////////////////////

        {
            wf.SetMax(appList.Count);
            
            var infoDir = dirName + "\\0_COMMON001";

            //20190308113850 furukawa st ////////////////////////
            //保存フォルダ名修正

            var imageDir = dirName + "\\8_ZYUUSEI001";
            //var imageDir = dirName + "\\9_JUDOTHERAPY";
            //20190308113850 furukawa ed ////////////////////////

            var scanDir = Settings.ImageFolder;
            try
            {
                System.IO.Directory.CreateDirectory(infoDir);
                System.IO.Directory.CreateDirectory(imageDir);
            }
            catch
            {
                MessageBox.Show("出力フォルダの作成に失敗しました。");
                return false;
            }

            var jyuFileName = infoDir + "\\99_PECULIARTEXTINFO_JYU.CSV";

            //tiffに含めるファイル名一覧 フルパス
            var images = new List<string>();
            string saveTiffFileName = "";

            //インフォメーションファイル用診療年月
            string infoSinryoYM=string.Empty;

            //レセプト枚数カウント
            int receCount=0;

            //レセプト管理番号の後ろ5桁
            int intItirenBango = 1;

            //一般とくたいの区別
            string tmpStrType = string.Empty;



            //ファイル出力
            using (var sw = new System.IO.StreamWriter(jyuFileName, false, Encoding.GetEncoding("Shift_JIS")))
            {
                foreach (var item in appList)
                {


                    //20190531170540 furukawa st ////////////////////////
                    //支払保留は納品データに出さない                    
                    if (item.StatusFlagCheck(StatusFlag.支払保留)) continue;
                    //20190531170540 furukawa ed ////////////////////////


                    //一般の場合、とくたいを飛ばす
                    if (strHokensyaNo == Type_Ippan.HokensyaNo)
                        if (item.HihoType != 6) continue;
                    
                    //とくたいの場合一般を飛ばす
                    if(strHokensyaNo== Type_Tokutai.HokensyaNo)
                        if (item.HihoType != 63) continue;


                    wf.InvokeValue++;

                    #region 画像コピー関連



                    if (item.MediYear == -4)
                    {
                        //不要画像の場合
                        continue;
                    }
                    else if (item.MediYear == ZOKUSHI_CODE)
                    {
                        //続紙の場合
                        images.Add(item.GetImageFullPath());
                        continue;
                    }
                    else if (images.Count == 1)
                    {

                        if (!ImageUtility.SaveOne(images, saveTiffFileName))
                        {

                            throw new Exception("画像ファイルのコピーに失敗しました。ファイル名:" + saveTiffFileName);
                        }

                        images.Clear();
                    }
                    else if (images.Count != 0)
                    {

                        if (!ImageUtility.Save(images, saveTiffFileName))
                        {

                            throw new Exception("画像ファイルのマルチページTiff変換に失敗しました。ファイル名:" + saveTiffFileName);
                        }

                        images.Clear();
                    }
                    images.Add(item.GetImageFullPath());

                    #endregion

                    #region コード整頓
                    //20190611202430 furukawa st ////////////////////////
                    //処理年月、診療年月作成ルーチン整頓


                    #region 処理年月作成
                    //処理年月を生成 
                    //メホール画像登録の請求年月 ＋ 1ヶ月 = 8.処理年月

                    int chgY = item.ChargeYear;//新元号の年
                    int chgM = item.ChargeMonth;//新元号の月

                    if (chgM == 12)
                    {
                        chgY++;
                        chgM = 1;
                    }
                    else chgM++;


                    int syori_adyyyymm = DateTimeEx.GetAdYearFromHs(chgY * 100 + chgM) * 100 + chgM;
                    string syoriYM = DateTimeEx.GetGyymmFromAdYM(syori_adyyyymm).ToString("00000");

                    #endregion

                    #region 診療年月作成
                    //informationファイル診療年月
                    //informationファイル用は１回のみ作成
                    if (infoSinryoYM == string.Empty)
                    {
                        string y = syoriYM.Substring(1, 2);
                        string m = syoriYM.Substring(3, 2);
                        int intSyoriYM = 0;
                        int.TryParse(syoriYM, out intSyoriYM);


                        //メホール画像登録の請求年月 ＋ 1ヶ月 = 8.処理年月intSyoriYM



                        //仕様変更2019/07/04
                        //20190704173531 furukawa st ////////////////////////
                        //00_INFORMATION.CSVの診療年月は画像登録と同じ月
                        
                        if (intSyoriYM == 50105) infoSinryoYM = (intSyoriYM-1).ToString("00000");
                        else if (m == "01") infoSinryoYM = intSyoriYM.ToString().Substring(0, 1) + (int.Parse(y) - 1).ToString("00") + "12";
                        else infoSinryoYM = (intSyoriYM - 1).ToString("00000");





                        //診療年月は処理年月の3か月前
                        //if (intSyoriYM == 50105) infoSinryoYM = "43102";
                        //else if (intSyoriYM == 50106) infoSinryoYM = "43103";
                        //else if (intSyoriYM == 50107) infoSinryoYM = "43104";
                        //else if (m == "01") infoSinryoYM = intSyoriYM.ToString().Substring(0, 1) + (int.Parse(y) - 1).ToString("00") + "10";
                        //else if (m == "02") infoSinryoYM = intSyoriYM.ToString().Substring(0, 1) + (int.Parse(y) - 1).ToString("00") + "11";
                        //else if (m == "03") infoSinryoYM = intSyoriYM.ToString().Substring(0, 1) + (int.Parse(y) - 1).ToString("00") + "12";
                        //else infoSinryoYM = (intSyoriYM - 3).ToString("00000");

                        //20190704173531 furukawa ed ////////////////////////

                    }



                    //99_PECULIARTEXTINFO_JYUファイル診療年月
                    int medY = item.MediYear;
                    int medM = item.MediMonth;
                    int sinryo_adyyyymm = DateTimeEx.GetAdYearFromHs(medY * 100 + medM) * 100 + medM;
                    string sinryoYM = DateTimeEx.GetGyymmFromAdYM(sinryo_adyyyymm).ToString("00000");

                    #endregion




                    #region 旧処理
                    /*
                    //処理年月を生成 請求年月-1としている
                    int y = item.ChargeYear;
                    int m = item.ChargeMonth;


                    #region 20190327161637 furukawa 診療年月、処理年月を新元号対応


                    //20190327161637 furukawa st ////////////////////////
                    //診療年月、処理年月を新元号対応


                    #region 処理年月作成
                    int chgY = 0;//新元号の年
                    int chgM = 0;//新元号の月
                    string chgEra = DEFALT_ERA;//新元号の番号
                 
                    ChangeEra(y, m, chgEra, out chgY, out chgM, out chgEra);


                    //20190329200843 furukawa st ////////////////////////
                    //仕様間違い　メホール画像登録の請求年月 ＋ 1ヶ月 = 8.処理年月
                    
                    if (chgM == 12)
                    {
                        chgY++;
                        chgM = 1;
                    }
                    else
                    {
                        chgM++;
                    }

                        
                          //処理年月 = 請求年月 -1ヶ月
                        //if (chgM == 1)
                        //{
                        //    chgY--;
                        //    chgM = 12;
                        //}
                        //else
                        //{
                        //    chgM--;
                        //}
                        

                    //20190329200843 furukawa ed ////////////////////////


                    int adyyyymm=DateTimeEx.GetAdYearFromHs(chgY * 100 + chgM) * 100 + chgM;
                    
                    string syoriYM = chgEra + chgY.ToString("00") + chgM.ToString("00");
                    syoriYM= DateTimeEx.GetGyymmFromAdYM(adyyyymm).ToString();

                    #endregion

                    #region 診療年月作成
                    ChangeEra(y, m, chgEra, out chgY, out chgM, out chgEra);
                 

                    //インフォメーションファイルのための記録
                    if (infoSinryoYM == string.Empty)
                    {

                        //20190329195818 furukawa st ////////////////////////                   

                        //仕様間違い メホール画像登録の請求年月 -2ヶ月 = 診療年月


                        if (chgM == 1)
                        {
                            //1月の2ヶ月前
                            chgY--;
                            chgM = 11;
                        }
                        else if(chgM == 2)
                        {
                            //2月の2ヶ月前
                            chgY--;
                            chgM = 12;
                        }
                        else
                        {
                            chgM-=2;
                        }


                            
                            //診療年月は処理年月の1か月前
                            //if (chgM == 1)
                            //{
                            //    chgY--;
                            //    chgM = 12;
                            //}
                            //else
                            //{
                            //    chgM--;
                            //}
                            

                        //20190329195818 furukawa ed ////////////////////////


                        infoSinryoYM = chgEra + chgY.ToString("00") + chgM.ToString("00");
                    }



                    //診療年月生成


                    //20190531170657 furukawa st ////////////////////////
                    //診療年月用変数
                    
                    int medY = item.MediYear;
                    int medM = item.MediMonth;
                    string medEra = string.Empty;
                    //20190531170657 furukawa ed ////////////////////////


                    string sinryoYM = DEFALT_ERA + item.MediYear.ToString("00") + item.MediMonth.ToString("00");


                    //20190531170747 furukawa st ////////////////////////
                    //診療年月年号調整
                    
                    ChangeEra(item.MediYear, item.MediMonth, DEFALT_ERA, out medY, out medM, out medEra);
                    sinryoYM = medEra + medY.ToString("00") + medM.ToString("00");
                    //20190531170747 furukawa ed ////////////////////////

                    */
                    #endregion

                    #region 元のコード

                    //if (m == 1)
                    //{
                    //    y--;
                    //    m = 12;
                    //}
                    //else
                    //{
                    //    m--;
                    //}
                    //string syoriYM = DEFALT_ERA + y.ToString("00") + m.ToString("00");



                    ////インフォメーションファイルのための記録
                    //if (infoSinryoYM == string.Empty)
                    //{
                    //    //診療年月は処理年月の1か月前
                    //    if (m == 1)
                    //    {
                    //        y--;
                    //        m = 12;
                    //    }
                    //    else
                    //    {
                    //        m--;
                    //    }
                    //    infoSinryoYM = DEFALT_ERA + y.ToString("00") + m.ToString("00");
                    //}

                    ////診療年月を生成
                    //string sinryoYM = DEFALT_ERA + item.MediYear.ToString("00") + item.MediMonth.ToString("00");



                    //20190327161637 furukawa ed ////////////////////////
                    #endregion

                    //20190611202430 furukawa ed ////////////////////////
                    #endregion




                    //ID関係を生成
                    int baseID;
                    if (!int.TryParse(item.Numbering, out baseID))
                    {
                        throw new Exception("ナンバリングを数値に変換できませんでした。aid:" + item.Aid.ToString());
                    }



                    //20190208143152 furukawa st ////////////////////////
                    //各番号の作成

                    //一般・とくたいの区別
                    string strType = string.Empty;


                    //20190220152356 furukawa st ////////////////////////
                    //familyフィールドの構成は本人家族＋受診者区分（3桁）

                    if (item.HihoType == 6 && item.Family.ToString().Substring(0,1) == "2") strType = "0071";
                    if (item.HihoType == 6 && item.Family.ToString().Substring(0, 1) != "2") strType = "0072";
                    if (item.HihoType == 63 && item.Family.ToString().Substring(0, 1) == "2") strType = "0171";
                    if (item.HihoType == 63 && item.Family.ToString().Substring(0, 1) != "2") strType = "0172";

                        /*
                        if (item.HihoType == 6 && item.Family == 2) strType = "0071";
                        if (item.HihoType == 6 && item.Family != 2) strType = "0072";
                        if (item.HihoType == 63 && item.Family == 2) strType = "0171";
                        if (item.HihoType == 63 && item.Family != 2) strType = "0172";
                        */

                    //20190220152356 furukawa ed ////////////////////////




                    //一般とくたいの区別が前のレコードと違ったらレセプト管理番号の後ろ5桁を1にリセット
                    if (tmpStrType != strType) intItirenBango = 1;


                    //3.一連番号


                    //20190616123334 furukawa st ////////////////////////
                    //3.一連番号＝出力したレコードのにくっつく単なる連番
                    
                            //string receNo = "00" + intItirenBango.ToString("00000");
                    string receNo = "00" + baseID.ToString("000000");
                    //20190616123334 furukawa ed ////////////////////////


                    //9.レセプト管理番号

                    //20190616123450 furukawa st ////////////////////////
                    //9.レセプト管理番号　スタンプされた番号　なので、入力データをそのまま出す
                    
                    string receKanriNo = item.ComNum;// strType + intItirenBango.ToString("00000");
                                                     //string receKanriNo = strType + baseID.ToString("000000").Substring(1,5);
                                                     //20190616123450 furukawa ed ////////////////////////


                    //10.検索番号　


                    //20190616123545 furukawa st ////////////////////////
                    //10.検索番号　処理年月＋9．レセプト管理番号
                    
                    string searchNo = "000" + syoriYM + receKanriNo;
                            //string searchNo = "000" + syoriYM + "000" + baseID.ToString("000000");
                    //20190616123545 furukawa ed ////////////////////////



                    //20190208143152 furukawa ed ////////////////////////


                    //被保記番の設定
                    var mn = item.HihoNum.Split('-');
                    string mark, number;
                    if (mn.Length == 1)
                    {
                        mark = string.Empty;
                        number = mn[0];
                    }
                    else if (mn.Length == 2)
                    {
                        mark = mn[0];
                        number = mn[1];
                    }
                    else
                    {
                        throw new Exception("被保険者記号番号を判別できませんでした。aid:" + item.Aid.ToString());
                    }

                    //生年月日
                    string jbirth = DateTimeEx.GetEraNumber(item.Birthday).ToString() +
                        DateTimeEx.GetJpYear(item.Birthday).ToString("00") +
                        item.Birthday.Month.ToString("00") +
                        item.Birthday.Day.ToString("00");


                    //画像ファイル名を指定
                    //20190219133608 furukawa st ////////////////////////


                    //20190220151631 furukawa st ////////////////////////
                    //画像ファイル名は検索番号にする（仕様どおり）
                    saveTiffFileName = imageDir + "\\" +searchNo + ".tif";

                        //画像ファイル名はレセプト管理番号にする
                        //saveTiffFileName = imageDir + "\\" + receKanriNo + ".tif";
                        //saveTiffFileName = imageDir + "\\" +searchNo + ".tif";

                    //20190220151631 furukawa ed ////////////////////////


                    //20190219133608 furukawa ed ////////////////////////


                    //家族区分と受診者区分
                    string familyCode = item.Family.ToString().Substring(0,1) == "2" ? "1" : "2" ;
                         //string familyCode = item.Family.ToString() == "2" ? "1" : "2";

                    //受診者区分
                    string jyushinshaCode = (item.Family % 100).ToString();
                        //string jyushinshaCode = item.Family.ToString();

                    if (jyushinshaCode == "4") jyushinshaCode = "5";
                    else if (jyushinshaCode == "8") jyushinshaCode = "1";
                    else if (jyushinshaCode == "0") jyushinshaCode = "2";
                    else jyushinshaCode = "";

                    //受診者区分
                    //0：高七　→　2：高齢者（一定以上）
                    //2：本人
                    //4：六歳　→　5：未就学
                    //6：家族
                    //8：高一　→　1：高齢者（一般）

                    //本家区分
                    //2：本人　→　1:本人
                    //6：家族　→　2:家族

                        //familyCode = item.Family % 100 == 2 ? "本人" : item.Family % 100 == 6 ? "家族" : "";


                    string syubetsu = item.AppType == APP_TYPE.鍼灸 ? "7" : "1";

                    var sl = new string[87];

                    #region MNレコード
                    sl[1] = "10";                                           //1	行番号	数字	5	可変	レセプト内通し番号を記録する。「１０」固定
                    sl[2] = "MN";                                           //2	レコード識別情報	英数	2	固定	"MN"を記録する。
                    sl[3] = receNo;                                         //3	一連番号	数字	8	固定	提供データ（レセ電・紙レセ分）全体の通し番号を記録する。固有テキスト情報以外分は50万以降の通し番号を記録する。
                    sl[4] = sinryoYM;                                       //4	診療年月	数字	5	固定	1　診療年月を和暦で元号区別コード（別表2）を含めた形で記録する。2　数字"GYYMM"の形式で記録する。
                    sl[5] = "6";                                            //5	レセ電・紙レセ種別コード	数字	1	固定	レセ電、紙レセ種別コード（別表3）を記録する。
                    sl[6] = item.HihoPref.ToString("00");                   //6	都道府県コード	数字	2	固定	医療機関の所在する都道府県コードを記録する。
                    sl[7] = "9";                                            //7	点数表コード	数字	1	固定	柔整閲覧用として"9"を固定で記録する。
                    sl[8] = syoriYM;                                        //8	処理年月	数字	5	固定	1　「請求年月－１ヶ月」を和暦で元号区別コード（別表2）を含めた形で記録する。2　数字"GYYMM"の形式で記録する。
                                                                            
                    //レセプト管理番号=0071+一連番号5桁                     
                    sl[9] = receKanriNo;                                    //9	レセプト管理番号	数字	9	固定	検索番号の下９桁を設定
                        //sl[9] = "0" + receNo;

                    sl[10] = searchNo;                                      //10	検索番号	数字	17	固定	レセプト画像のｔｉｆｆファイル名を設定する。"000"+処理年月(5)+"000"+一連番号(6)形式で付与する。
                    sl[11] = "2";                                           //11	入外別種別コード	数字	1	可変	２：入院外を固定で記録する

                    

                    //これであってる →全フィールドリストにこの値は出ないので、確認しようがない
                    sl[12] = item.ClinicNum;                                 //12	医療機関コード　　　　　　	数字	5	固定	柔整師コード（師会コード）を記録する。	×
                        //sl[12] = item.DrNum;                                    //12	医療機関コード　　　　　　	数字	5	固定	柔整師コード（師会コード）を記録する。	×




                    //20190308131845 furukawa st ////////////////////////
                    //項番１３仕様変更



                    //項番13については、仕様書上 "省略可" になっていましたが、これは誤りです。
                    //柔整の場合は "1" をセットしてください。もし将来的に鍼灸を委託することになった場合は "2" も発生します。
                    //2019/03/08シャープ健保大西様より

                    switch (item.AppType)
                    {
                        case APP_TYPE.柔整:
                            sl[13] = "1";
                            break;
                        case APP_TYPE.鍼灸:
                            sl[13] = "2";
                            break;
                        default:
                            sl[13] = "3";
                            break;
                    }
                    
                    //sl[13] = "";                                            //13	柔整・鍼灸コード　　　　　	数字	8	可変	１：柔整　２：鍼灸　３：あんま・マッサージ	○
                    
                    //20190308131845 furukawa ed ////////////////////////




                    sl[14] = "";                                            //14	医療機関名称　　　　　　　	漢字	40	固定	記録を省略する。	○
                    sl[15] = "";                                            //15	画像品位注意コード　　　　	数字	1	固定	記録を省略する。	○

                    #endregion

                    #region SAレコード

                    sl[16] = "20";//16	行番号　　　　　　	数字	5	可変	レセプト内通し番号を記録する。「２０」固定	×	
                    sl[17] = "SA";//17	レコード識別情報　	英数	2	固定	"SA"を記録する。	×	



                    //20190308133037 furukawa st ////////////////////////
                    //項番18仕様変更
                    
                    sl[18] = "1";       //2019/03/08仕様変更 18	保険種別1	数字	1	固定	"1" （医保） 固定	×	
                                        //sl[18] = "";      //18	保険種別1　	数字	1	固定	記録を省略する。	○	
                                        //sl[18] = syubetsu;

                    //20190308133037 furukawa ed ////////////////////////                    			


                    sl[19] = "1";                                         //19	保険種別2	数字	1	固定	"1"(単独)を記録する	×	レセプト情報管理システム側で必要。
                    sl[20] = familyCode;                                  //20	本人・家族	数字	1	固定	本家区分コード(別表6)を入力する。	×	レセプト情報管理システム側で必要。
                    sl[21] = "";                                          //21	市町村番号	英数	8	固定	記録を省略する。	○	
                    sl[22] = "";                                          //22	老人受給者番号	英数	7	固定	記録を省略する。	○	
                    sl[23] = item.InsNum;                                 //23	保険者番号	英数	8	可変	保険者番号を記録する。	×	レセプト情報管理システム側で必要。
                    sl[24] = Strings.StrConv(mark, VbStrConv.Wide);       //24	被保険者証記号	漢字	40	可変	1医療保険及び老人医療の場合、健康保険被保険者証、船員保険被保険者証、船員保険費扶養者証及び受給資格者票等の「記号及び番号」欄の記号を左づめに記録する。2記録する記号は番号の記録バイト数と合わせて38バイト（19桁）を超えない。3記録する番号が38バイトに満たない場合、後続する"スペース"の記録は省略する。4番号のみ設定されている場
                    sl[25] = Strings.StrConv(number, VbStrConv.Wide);     //25	被保険者証番号	漢字	40	可変	1医療保険及び老人医療の場合、健康保険被保険者証、船員保険被保険者証、船員保険費扶養者証及び受給資格者票等の「記号及び番号」欄の記号を左づめに記録する。2記録する記号は番号の記録バイト数と合わせて38バイト（19桁）を超えない。3記録する番号が38バイトに満たない場合、後続する"スペース"の記録は省略する。	×	
                    sl[26] = "";                                          //26	給付割合（%）	数字	3	固定	記録を省略する。	○	
                    sl[27] = "";                                          //27	第一公費負担者番号	英数	8	固定	記録を省略する。	○	
                    sl[28] = "";                                          //28	第一公費受給者番号	英数	7	固定	記録を省略する。	○	
                    sl[29] = "";                                          //29	第二公費負担者番号	英数	8	固定	記録を省略する。	○	
                    sl[30] = "";                                          //30	第二公費受給者番号	英数	7	固定	記録を省略する。	○	
                    sl[31] = "";                                          //31	第三公費負担者番号	英数	8	固定	記録を省略する。	○	
                    sl[32] = "";                                          //32	第三公費受給者番号	英数	7	固定	記録を省略する。	○	
                    sl[33] = "";                                          //33	第四公費負担者番号	英数	8	固定	記録を省略する。	○	
                    sl[34] = "";                                          //34	第四公費受給者番号	英数	7	固定	記録を省略する。	○	
                    sl[35] = "";                                          //35	第五公費負担者番号	英数	8	固定	記録を省略する。	○	
                    sl[36] = "";                                          //36	第五公費受給者番号	英数	7	固定	記録を省略する。	○	
                    sl[37] = "";                                          //37	予備	英数又は漢字	40	固定	記録を省略する。	○	
                    sl[38] = item.Sex == 1 ? "1" : "2";//item.Psex.ToString();                  //38	性別	数字	1	固定	性別コード（別表1）を記録する。	×	
                    sl[39] = jbirth;                                                            //39	生年月日	数字	7	固定	1生年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3生年月日の"月日"が設定されていない場合は"GY0000"の形式で記録する。また、生年月日の"日"が設定されていない場合は、"GYMM00"の形式で記録する。	×	※注4
                    sl[40] = "";                                  //40	職務上の事由	数字	1	固定	記録を省略する。	○	
                    sl[41] = "";                                  //41	特記事項1	英数	2	固定	記録を省略する。	○	
                    sl[42] = "";                                  //42	特記事項2	英数	2	固定		○	
                    sl[43] = "";                                  //43	特記事項3	英数	2	固定		○	
                    sl[44] = "";                                  //44	特記事項4	英数	2	固定		○	
                    sl[45] = "";                                  //45	特記事項5	英数	2	固定		○	
                    sl[46] = "";                                  //46	保険医療機関の所在地及び名称	漢字	80	固定	記録を省略する。	○	
                    sl[47] = "";                                  //47		漢字	40	固定	記録を省略する。	○	


                    //20190523180809 furukawa st ////////////////////////
                    //４８は支払先コード入力ですでに入っているはず
                    

                        /*

                        //20190308141447 furukawa st ////////////////////////
                        //項番48仕様変更


                        //48  保険医療機関の所在地及び名称 英数	15	固定	"下記 15桁 を記録
                        //・団体：  ""0090000"" ＋ スペース 8桁
                        //・個人：  外部機関コードの下 7桁 ＋ スペース 8桁"	


                        //支払先コードの後ろ２桁で判断しているのでそれで判断
                        int lng = item.PayCode.ToString().Length;


                        //20190329175429 furukawa st ////////////////////////
                        //支払先コードがない場合回避
                        if (lng != 0)
                        //20190329175429 furukawa ed ////////////////////////
                        {

                            string strDantai = item.PayCode.ToString().Substring(lng - 2, 2);
                            switch (strDantai)
                            {
                                case "00"://団体
                                    sl[48] = ("0090000").PadRight(8);
                                    break;

                                case "01"://個人                            
                                    sl[48] = item.DrNum.PadRight(8);
                                    break;

                            }
                        }

                        //sl[48] = "";                                  //48	保険医療機関の所在地及び名称	英数数字	157	固定	記録を省略する。柔整師個人コードを記録する。（前0埋め）	○×	

                        */


                    sl[48] = item.DrNum;

                        //20190308141447 furukawa ed ////////////////////////

                    //20190523180809 furukawa ed ////////////////////////


                    sl[49] = "";                                  //49		漢字	20	固定	記録を省略する。	○	
                    sl[50] = "";                                  //50		漢字	14	固定	記録を省略する。	○	
                    sl[51] = item.CountedDays.ToString();         //51	診療実日数（保）	数字	2	可変	1医療保険又は老人医療の場合、医療保険又は老人医療の診療実日数を"Z9"の形式で記録する。2その他の場合は、記録を省略する。	×	
                    sl[52] = "";                                  //52	診療実日数（①）	数字	2	固定	記録を省略する。	○	
                    sl[53] = "";                                  //53	診療実日数（②）	数字	2	固定	記録を省略する。	○	
                    sl[54] = "";                                  //54	診療実日数（③）	数字	2	固定	記録を省略する。	○	
                    sl[55] = "";                                  //55	診療実日数（④）	数字	2	固定	記録を省略する。	○	
                    sl[56] = "";                                  //56	診療実日数（⑤）	数字	2	固定	記録を省略する。	○	
                    sl[57] = "";                                  //57	予備	数字	7	固定	記録を省略する。	○	

                    #endregion

                    #region NJレコード
                                       
                    sl[58] = "30";                                  //58	行番号　　　　　　　　　　　　	数字	5	可変	レセプト内通し番号を記録する。「３０」固定	省略不可能
                    sl[59] = "NJ";                                  //59	レコード識別情報　　　　　　　	英数	2	固定	"ＮTＪ"を記録する。"ＮＪ"を記録する。	省略不可能
                    sl[60] = "";                                    //60	続柄　　　　　　　　　　　　　	英数	2	固定	記録を省略する。	省略可能
                    sl[61] = familyCode;                            //61	本家区分　　　　　　　　　　　	英数	1	固定	本家区分コード(別表6)を入力する。	省略不可能
                    sl[62] = item.Total.ToString();                 //62	施術金額　　　　　　　　　　　	英数	7	可変	柔整師より請求された合計金額（自己負担金を含む）を"ZZZZZZ9"の形式で記録する。	省略不可能




                    //20190308145835 furukawa st ////////////////////////
                    //項番62-67仕様変更
                    
                    //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                    string dt = DateTimeEx.GetAdYearFromHs(item.MediYear * 100 + item.MediMonth).ToString();
                    //string dt = DateTimeEx.GetAdYearFromHs(int.Parse(item.MediYear.ToString())).ToString();
                    //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                    //string strFirstDate = dt + item.MediMonth + item.FushoStartDate1.Day.ToString("00");


                    //20190523191234 furukawa st ////////////////////////
                    //FushoFirstDate1初検日１　FushoStartDate1施術開始年月日１
                    

                    sl[63] = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoFirstDate1).ToString();
                        //sl[63] = sinryoYM + item.FushoStartDate1.Day.ToString("00"); //2019/03/08省略不可　63	初検年月日１　　　　　　　　　	英数	7	可変	1初検年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    
                    //20190523191234 furukawa ed ////////////////////////


                    sl[64] = item.Total.ToString();                 //64	支給決定金額施術（決定）金額　	英数	7	可変	決定された金額を"ZZZZZZ9"の形式で記録する。組合への請求金額（組合負担分の金額）。柔整師より請求された合計決定金額（自己負担金を含む）を"ZZZZZZ9"の形式で記録する。	省略可能                                                                      
                                                                    //2019/03/08
                                                                    //柔整師より請求された合計決定金額（自己負担金を含む）を"ZZZZZZ9"の形式で記録する。
                                                                    //柔整師より請求された合計金額（自己負担金を含む）に誤りがあり、修正を行った場合は修正後の金額（自己負担金を含む）を記録する。省略された場合は施術金額と同じ値が自動でシステムに取り込まれる。
                    sl[65] = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoStartDate1).ToString();   //2019/03/08省略不可    65	施術開始年月日１    英数	7	可変	1施術を開始した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[66] = DateTimeEx.GetIntJpDateWithEraNumber(item.FushoFinishDate1).ToString();  //2019/03/08省略不可    66	施術終了年月日１    英数	7	可変	1施術を終了した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[67] = "1905";                                //67	疾病コード１　　　　　　　　　	2019/03/08　"1905" 固定



                    /*
                    sl[63] = "";                                    //63	初検年月日１　　　　　　　　　	英数	7	可変	1初検年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[64] = "";                                    //64	支給決定金額施術（決定）金額　	英数	7	可変	決定された金額を"ZZZZZZ9"の形式で記録する。組合への請求金額（組合負担分の金額）。柔整師より請求された合計決定金額（自己負担金を含む）を"ZZZZZZ9"の形式で記録する。	省略可能
                    sl[65] = "";                                    //65	施術開始年月日１　　　　　　　	英数	7	可変	1施術を開始した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[66] = "";                                    //66	施術終了年月日１　　　　　　　	英数	7	可変	1施術を終了した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[67] = "";                                    //67	疾病コード１　　　　　　　　　	数字	4	可変	疾病コードを入力する。	省略可能
                    */


                    //20190308145835 furukawa ed ////////////////////////

                    sl[68] = jyushinshaCode;                        //68	受診者区分　　　　　　　　　　	数字	1	可変	受診者区分(別表7)を入力する。	省略不可能
                    sl[69] = "";                                    //69	受診者管理NO　　　　　　　　	数字	3	可変	受診者管理NOを入力する。	省略可能
                    sl[70] = "";                                    //70	調査NO　　　　　　　　　　　	数字	5	可変	調査NOを入力する。	省略可能
                    sl[71] = "";                                    //71	疾病コード２　　　　　　　　　	数字	4	可変	疾病コードを入力する。	省略可能
                    sl[72] = "";                                    //72	疾病コード３　　　　　　　　　	数字	4	可変	疾病コードを入力する。	省略可能
                    sl[73] = "";                                    //73	疾病コード４　　　　　　　　　	数字	4	可変	疾病コードを入力する。	省略可能
                    sl[74] = "";                                    //74	疾病コード５　　　　　　　　　	数字	4	可変	疾病コードを入力する。	省略可能
                    sl[75] = "";                                    //75	初検年月日２　　　　　　　　　	英数	7	可変	1初検年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[76] = "";                                    //76	初検年月日３　　　　　　　　　	英数	7	可変	1初検年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[77] = "";                                    //77	初検年月日４　　　　　　　　　	英数	7	可変	1初検年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[78] = "";                                    //78	初検年月日５　　　　　　　　　	英数	7	可変	1初検年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[79] = "";                                    //79	施術開始年月日２　　　　　　　	英数	7	可変	1施術を開始した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[80] = "";                                    //80	施術開始年月日３　　　　　　　	英数	7	可変	1施術を開始した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[81] = "";                                    //81	施術開始年月日４　　　　　　　	英数	7	可変	1施術を開始した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[82] = "";                                    //82	施術開始年月日５　　　　　　　	英数	7	可変	1施術を開始した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[83] = "";                                    //83	施術終了年月日２　　　　　　　	英数	7	可変	1施術を終了した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[84] = "";                                    //84	施術終了年月日３　　　　　　　	英数	7	可変	1施術を終了した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[85] = "";                                    //85	施術終了年月日４　　　　　　　	英数	7	可変	1施術を終了した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能
                    sl[86] = "";                                    //86	施術終了年月日５　　　　　　　	英数	7	可変	1施術を終了した年月日を和暦で元号区分コードを含めた形で記録する。2数字"GYYMMDD"の形式で記録する。3年月日の設定が省略される場合は、記録を省略する。	省略可能

                    #endregion


                    //CSV書き込み
                    if (!createCSVLine(sl, sw))return false;

                    //レセ枚数カウント
                    receCount++;

                    //レセプト管理番号の後ろ5桁
                    intItirenBango++;

                    //一般とくたいの区別を次レコード比較用に取得
                    tmpStrType = strType;

                }

                //最終回分tiff画像記録
                ImageUtility.Save(images, saveTiffFileName);
            }


            #region informationファイル作成
            //infomationファイル作成

            //20190308114037 furukawa st ////////////////////////
            //INFORMATION.CSVファイル名修正

            var infoFileName = infoDir + "\\00_INFORMATION.CSV";
            //var infoFileName = infoDir + "\\00_INFOMATION.CSV";
            //20190308114037 furukawa ed ////////////////////////

            try
            {
                using (var sw = new System.IO.StreamWriter(infoFileName, false, Encoding.GetEncoding("Shift_JIS")))
                {

                    //20190308114146 furukawa st ////////////////////////
                    //INFORMATION.CSVカンマ数修正

                    sw.WriteLine("保険者名," + "シャープ健康保険組合" + ",,,");//Settings.HokenshaName);

                    //フォルダ体系によって保険者番号が変わるはず
                    //sw.WriteLine("保険者番号," + "32230518");//Settings.HokenshaNo);                    
                    sw.WriteLine("保険者番号," + strHokensyaNo + ",,,");
                    sw.WriteLine("診療年月," + infoSinryoYM + ",,,");
                    sw.WriteLine("媒体作成年月日," + DateTime.Today.ToString("yyyy/MM/dd") + ",,,");


                    
                    /*
                    sw.WriteLine("保険者名," + "シャープ健康保険組合");//Settings.HokenshaName);

                    //フォルダ体系によって保険者番号が変わるはず
                    //sw.WriteLine("保険者番号," + "32230518");//Settings.HokenshaNo);                    
                    sw.WriteLine("保険者番号," + strHokensyaNo);
                    sw.WriteLine("診療年月," + infoSinryoYM);
                    sw.WriteLine("媒体作成年月日," + DateTime.Today.ToString("yyyy/MM/dd"));
                    */
                    
                    //20190308114146 furukawa ed ////////////////////////


                    sw.WriteLine(string.Empty);
                    sw.WriteLine("医科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("DPC  (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("歯科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("調剤 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("柔整 (レセ電/紙レセ/コード/画像) ,0," + receCount.ToString() + ",0," + receCount.ToString());
                }
            }
            #endregion

            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }

        static bool createCSVLine(string[] list, System.IO.StreamWriter sw)
        {
            try
            {
                var sb = new StringBuilder();
                sb.Append(list[1]);
                for (int i = 2; i < 16; i++)
                {
                    sb.Append("," + list[i]);
                }
                sw.WriteLine(sb.ToString());

                sb.Clear();
                sb.Append(list[16]);
                for (int i = 17; i < 58; i++)
                {
                    sb.Append("," + list[i]);
                }
                sw.WriteLine(sb.ToString());

                sb.Clear();
                sb.Append(list[58]);
                for (int i = 59; i < 87; i++)
                {
                    sb.Append("," + list[i]);
                }
                sw.WriteLine(sb.ToString());
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }

            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            using (var wf = new WaitForm())
            {
                try
                {
                    using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                    {
                        wf.Max = list.Count;
                        wf.BarStyle = ProgressBarStyle.Continuous;wf.ShowDialogOtherTask();
                        wf.LogPrint("リストを出力しています…");

                        var l = new List<string>();
                        //先頭行は見出し
                        l.Add("ID");
                        l.Add("処理年");
                        l.Add("処理月");
                        l.Add("診療年");
                        l.Add("診療月");
                        l.Add("保険者番号");
                        l.Add("被保険者番号");
                        l.Add("住所");
                        l.Add("氏名");
                        l.Add("性別");
                        l.Add("生年月日");
                        l.Add("請求区分");
                        l.Add("往療料");
                        l.Add("施術所記号");
                        l.Add("合計金額");
                        l.Add("請求金額");
                        l.Add("診療日数");
                        l.Add("ナンバリング");
                        l.Add("照会理由");
                        l.Add("点検結果");
                        l.Add("点検結果詳細");
                        l.Add("被保険者名");
                        l.Add("受療者名");
                        l.Add("施術所名");
                        sw.WriteLine(string.Join(",", l));

                        var ss = new List<string>();
                        foreach (var item in list)
                        {
                            if (wf.Cancel)
                            {
                                if (MessageBox.Show("中止してもよろしいですか？", "",
                                     MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                                wf.Cancel = false;
                            }

                            ss.Add(item.Aid.ToString());
                            ss.Add(item.ChargeYear.ToString());
                            ss.Add(item.ChargeMonth.ToString());
                            ss.Add(item.MediYear.ToString());
                            ss.Add(item.MediMonth.ToString());
                            ss.Add(item.InsNum);
                            ss.Add(item.HihoNum);
                            ss.Add(item.HihoAdd);
                            ss.Add(item.PersonName);
                            ss.Add(item.Sex == 1 ? "男" : "女");
                            ss.Add(item.Birthday.ToShortDateString());
                            ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                            ss.Add(item.Distance == 999 ? "あり" : "");
                            ss.Add(item.DrNum);
                            ss.Add(item.Total.ToString());
                            ss.Add(item.Charge.ToString());
                            ss.Add(item.CountedDays.ToString());
                            ss.Add(item.Numbering);
                            ss.Add(item.ShokaiReason.ToString().Replace(",", " "));
                            ss.Add(item.InspectInfo);
                            ss.Add(item.InspectDescription);
                            ss.Add(item.HihoName);
                            ss.Add(item.PersonName);
                            ss.Add(item.ClinicName);

                            sw.WriteLine(string.Join(",", ss));
                            ss.Clear();

                            wf.InvokeValue++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    MessageBox.Show("出力に失敗しました");
                    return false;
                }
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }







        static void ChangeEra(int y,int m,string era,
                            out int outy,out int outm,out string outera)
        {
            string res=string.Empty;

            string strBaseYM = y.ToString("00") + m.ToString("00");
            
            

            if (y >= 31 && m>=5)
            {
                y -= 30;                        
            }
            else if (y==1 && m<=4)
            {
                y += 30;                             
            }

            //20190531170358 furukawa st ////////////////////////
            //修正

            else if (y <= 31)
                //if (y <= 31)

            //20190531170358 furukawa ed ////////////////////////
            {
                if (m <= 4)
                {
                    era = "4";
                }
                else
                {
                    era = "5";
                }
            }



            outy = y;
            outm = m;
            outera = era;

        }
    }
}
