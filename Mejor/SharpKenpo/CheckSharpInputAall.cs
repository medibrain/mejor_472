﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Mejor
{
            
    //20190205115734 furukawa シャープ健保用モジュール群

    class CheckSharpInputAall
    {
        static Regex re = new Regex(@"[^0-9]");

        public App app { get; private set; }
        public bool ErrInum { get; private set; }
        public bool ErrHnum { get; private set; }
        public bool ErrHpref { get; private set; }
        public bool ErrAtotal { get; private set; }
        public bool ErrAcharge { get; private set; }
        public bool ErrAyear { get; private set; }
        public bool ErrAmonth { get; private set; }
        public bool ErrAcounteddays { get; private set; }
        public bool ErrEra { get; private set; }
        public bool ErrBY { get; private set; }
        public bool ErrBM { get; private set; }
        public bool ErrBD { get; private set; }

        public bool ErrBirth
        {
            get
            {
                return ErrEra || ErrBY || ErrBM || ErrBD;
            }
        }

        public bool HasError
        {
            get
            {
                return ErrInum || ErrHnum || ErrHpref || ErrHpref ||
                    ErrAtotal || ErrAcharge || ErrAyear || ErrAmonth ||
                    ErrAcounteddays || ErrEra || ErrBY || ErrBM || ErrBD;
            }
        }

        public CheckSharpInputAall(App app, System.Windows.Forms.Label label)
        {
            this.app = app;
            var ocr = app.OcrData.Split(',');
            if (ocr.Length < 200)
            {
                ErrAyear = true;
                ErrAmonth = true;
                ErrInum = true;
                ErrHnum = true;
                ErrHpref = true;
                ErrAtotal = true;
                ErrAcharge = true;
                ErrAcounteddays = true;
                ErrEra = true;
                ErrBY = true;
                ErrBM = true;
                ErrBD = true;
                label.Text = "OCRデータなし";
                return;
            }

            ErrInum = false;
            ErrHnum = false;
            ErrHpref = false;
            ErrAtotal = false;
            ErrAcharge = false;
            ErrAyear = false;
            ErrAmonth = false;
            ErrAcounteddays = false;
            ErrEra = false;
            ErrBY = false;
            ErrBM = false;
            ErrBD = false;

            string labelText = "Aall   ";
            
            if (ocr.Length < 50) return;
            int temp = 0;

            //保険者番号
            //if (app.Inum != "32230518")
            //    ErrInum = true;
            ErrInum = app.InsNum != "32230518";

            //都道府県
            labelText += "pref:" + ocr[5] + "   ";
            int.TryParse(re.Replace(ocr[5], ""), out temp);
            if (temp == 0 || app.HihoPref != temp)
                ErrHpref = true;

            //被保険者番号
            labelText += "hnum:" + ocr[7] + "   ";
            var ahn = re.Replace(ocr[7], "");
            var ihn = re.Replace(app.HihoNum, "");
            if (ahn != ihn) 
                ErrHnum = true;

            //生年月日 年号
            string era = "";
            if (ocr[32] != "0") era = "明治";
            if (ocr[33] != "0") era += "大正";
            if (ocr[34] != "0") era += "昭和";
            if (ocr[35] != "0") era += "平成";
            if (DateTimeEx.GetEra(app.Birthday) != era) 
                ErrEra = true;
            labelText += "era:" + era + "   ";

            //生年月日 年
            labelText += "Y:" + ocr[36] + "   ";
            int.TryParse(re.Replace(ocr[36], ""), out temp);
            if (temp == 0 || DateTimeEx.GetJpYear(app.Birthday) != temp)
                ErrBY = true;

            //生年月日 月
            labelText += "M:" + ocr[37] + "   ";
            int.TryParse(re.Replace(ocr[37], ""), out temp);
            if (temp == 0 || app.Birthday.Month != temp)
                ErrBM = true;

            //生年月日 日
            labelText += "D:" + ocr[38] + "\r\n";
            int.TryParse(re.Replace(ocr[38], ""), out temp);
            if (temp == 0 || app.Birthday.Day != temp)
                ErrBD = true;
            
            //診療年
            labelText += "AY:" + ocr[3] + "   ";
            int.TryParse(re.Replace(ocr[3], ""), out temp);
            if (temp == 0 || app.MediYear != temp)
                ErrAyear = true;

            //診療月
            labelText += "AM:" + ocr[4] + "   ";
            int.TryParse(re.Replace(ocr[4], ""), out temp);
            if (temp == 0 || app.MediMonth != temp)
                ErrAmonth = true;

            //合計
            labelText += "total:" + ocr[193] + "   ";
            int.TryParse(re.Replace(ocr[193], ""), out temp);
            if (temp == 0 || app.Total != temp)
                ErrAtotal = true;

            //請求
            labelText += "charge:" + ocr[195] + "   ";
            int.TryParse(re.Replace(ocr[195], ""), out temp);
            if (temp == 0 || app.Charge != temp) 
                ErrAcharge = true;

            //実日数
            labelText += "days:" + ocr[53] + "," + ocr[70] + "," + ocr[87] + "," + ocr[104] + "   ";
            var idays = new List<int>();
            int iday;
            string sday;
            sday = re.Replace(ocr[53], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[70], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[87], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[104], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[121], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            iday = idays.Max();
            if (app.CountedDays != iday) 
                ErrAcounteddays = true;

            label.Text = "OCRデータあり";
        }

    }
}
