﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.SharpKenpo
{
    public partial class OtherForm : Form
    {
        int cym;
        public OtherForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
        }



        private void buttonNothing_Click(object sender, EventArgs e)
        {
   /*         var l = AhakiMatching.GetNothingMatch(cym);
            if (l.Count == 0)
            {
                MessageBox.Show("すべての申請書は関連付けが終了しています");
            }
            else
            {
                MessageBox.Show($"広域データが関連付けられていない申請書が{l.Count}枚あります");
            }*/
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
  /*          using (var f = new InputForm(cym))
            {
                f.ShowDialog();
            }*/
        }

        private void buttonAuto_Click(object sender, EventArgs e)
        {
       /*     if (AhakiMatching.AutoMatching(cym))
            {
                MessageBox.Show("自動マッチング処理が完了しました");
            }
            else
            {
                MessageBox.Show("自動マッチングに失敗しました");
            }*/
        }






        //20190501170735 furukawa st ////////////////////////
        //医科レセに存在する人に処理4フラグたてる
        private void btnIkaTotu_Click(object sender, EventArgs e)
        {


            StringBuilder sb = new StringBuilder();

            //20190510175030 furukawa st ////////////////////////            
            //フラグの更新方法変更（未処理フラグ関連）


            /*
            sb.AppendLine("update application a ");
            sb.AppendFormat("set statusflags={0} ", (int)StatusFlag.処理4);
            sb.AppendLine("from ikarezeptdata i ");
            sb.AppendLine("where a.hnum = i.F014");                              //被保番
            sb.AppendLine("and a.pbirthday = cast(i.F013 as date) ");            //生年月日西暦
            sb.AppendLine("and a.psex = cast(i.F006 as int) ");           //性別
            sb.AppendLine("and a.ym = cast(i.F012 as int) ");             //診療年月西暦
            sb.AppendFormat("and a.cym = {0}", cym);

            
            if (!DB.Main.Excute(sb.ToString()))
            {
            MessageBox.Show("自動マッチングに失敗しました");
            }
            else
            {
            var l=App.GetAppsByStatusFlag(cym, StatusFlag.処理4);                
            MessageBox.Show("自動マッチング処理が完了しました\n" + l.Count + '件');
            }*/


            sb.AppendLine("where exists (");
            sb.AppendLine(" select * from ikarezeptdata i ");

            sb.AppendLine("where a.hnum = i.F014");                              //被保番
            sb.AppendLine("and a.pbirthday = cast(i.F013 as date) ");            //生年月日西暦
            sb.AppendLine("and a.psex = cast(i.F006 as int) ");           //性別
            sb.AppendLine("and a.ym = cast(i.F012 as int) ");             //診療年月西暦

            //20190517221250 furukawa st //////////////////////
            //診療年月＝請求年月-2?

            //sb.AppendFormat("and a.cym = {0}", cym);

            sb.AppendLine(")");


            //20200127111601 furukawa st ////////////////////////
            //診療年月＝請求年月-2か月だが、翌年１，２月は単純に引き算すると月の値が99とかになる(202001-2=201999)ので
            //DateTimeExの関数で処理する            

            int tmpcym = DateTimeEx.Int6YmAddMonth(cym, -2);           
            sb.AppendFormat("and a.ym = {0}", tmpcym);
            //sb.AppendFormat("and a.ym = {0}", cym - 2);//診療年月＝請求年月-2?
            //20200127111601 furukawa ed ////////////////////////



            //20190517221250 furukawa ed ////////////////////////

            List<App> app = App.GetAppsWithWhere(sb.ToString());
            app.ForEach(a => a.Update(999, App.UPDATE_TYPE.Ikatotu));
            var l = App.GetAppsByStatusFlag(cym, StatusFlag.処理4);
            MessageBox.Show("自動マッチング処理が完了しました\n" + l.Count + '件');

            //20190510175030 furukawa ed ////////////////////////
        }
    }
}
