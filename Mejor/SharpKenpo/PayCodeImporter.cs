﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using Mejor.Pay;


namespace Mejor.SharpKenpo
{
    //20190212095344 furukawa シャープ健保データ取り込み用
    //中央ラテのコピー

    class PayCodeImporter
    {

        //20190531120517 furukawa st ////////////////////////
        //個人団体区別
        
        public enum PayKind
        {
            団体,
            個人,
            不明
        }


        public static PayKind getDK(string strPayCode)
        {
            if(strPayCode.Substring(9, 2) == "00")
            {
                return PayKind.団体;
            }
            else if(strPayCode.Substring(9, 2) == "01")
            {
                return PayKind.個人;
            }
            else
            {
                return PayKind.不明;
            }
        }
        //20190531120517 furukawa ed ////////////////////////


        public static bool Import()
        {
            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "外部機関マスタ|外部機関マスタ*.xlsx";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("データベースの情報を取り込んでいます");
                var dic = new Dictionary<string, PayCode>();
                var l = DB.Main.SelectAll<PayCode>();
                foreach (var item in l) dic.Add(item.Key, item);

                wf.LogPrint("Excelファイルを読み込んでいます");


                #region Excel読込


                //20190524105524 furukawa st ////////////////////////
                //excel使用中の例外処理
                
                System.IO.FileStream fs;
                try
                {
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open, FileAccess.Read);

                }
                catch(Exception exc)
                {
                    MessageBox.Show(exc.Message);
                    return false;
                }

                    //using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open,FileAccess.Read))
                    //{
                //20190524105524 furukawa ed ////////////////////////


                var ex = System.IO.Path.GetExtension(fileName);

                if (ex == ".xlsx")
                {
                    var xls = new XSSFWorkbook(fs);

                    var sheet = xls.GetSheet("Sheet1");

                    //var rowCount = sheet.LastRowNum ;
                    var rowCount = sheet.LastRowNum + 1;

                    for (int i = 0; i < rowCount; i++)
                    {
                        if (i == 0) continue;

                        try
                        {
                            var row = sheet.GetRow(i);
                            var prevlow = sheet.GetRow(i - 1);

                            var update = row.GetCell(1).StringCellValue.Contains("更新");

                            if (row.GetCell(1).CellType == CellType.Blank)
                                continue;

                            var p = new Pay.PayCode();


                            //20190213173848 furukawa st ////////////////////////
                            //外部機関CD後ろに団体個人コードをつけて、画面から取得できるように

                            p.Code = row.GetCell(1).CellType == CellType.String ?
                                row.GetCell(1).StringCellValue.Trim() :
                                row.GetCell(1).NumericCellValue.ToString();

                            p.Code += row.GetCell(0).StringCellValue.ToString();

                                    //p.Code = row.GetCell(1).CellType == CellType.String ?
                                    //    row.GetCell(1).StringCellValue.Trim() :
                                    //    row.GetCell(1).NumericCellValue.ToString();

                            //20190213173848 furukawa ed ////////////////////////

                            p.UpdateDate = 20000101;//更新日付を持っていないので、固定で持たせる

                            //20190524105625 furukawa st ////////////////////////
                            //銀行番号取得

                            p.BankNo = row.GetCell(6).StringCellValue;
                                    //p.BankNo = row.GetCell(7).StringCellValue;
                            //20190524105625 furukawa ed ////////////////////////

                            p.BankName = "銀行名なし";// row.GetCell(999).StringCellValue.Substring(5).Trim();


                            //20190524105728 furukawa st ////////////////////////
                            //支店番号取得
                            
                            p.BranchNo = row.GetCell(7).StringCellValue.Trim();
                                    //p.BranchNo = row.GetCell(8).StringCellValue.Trim();
                            //20190524105728 furukawa ed ////////////////////////

                            p.BranchName = "支店名なし";// row.GetCell(999).StringCellValue.Trim().Substring(5).Trim();



                            //20190524105803 furukawa st ////////////////////////
                            //口座番号取得
                            
                            p.AccountNo = row.GetCell(9).CellType == CellType.String ?
                                row.GetCell(9).StringCellValue.Trim().PadLeft(7, '0') :
                                row.GetCell(9).NumericCellValue.ToString("0000000");

                                    //p.AccountNo = row.GetCell(10).CellType == CellType.String ?
                                    //    row.GetCell(10).StringCellValue.Trim().PadLeft(7, '0') :
                                    //    row.GetCell(10).NumericCellValue.ToString("0000000");
                            //20190524105803 furukawa ed ////////////////////////


                            //20190717150929 furukawa st ////////////////////////
                            //口座番号が0の場合は飛ばす
                            
                            if (int.Parse(p.AccountNo) == 0) continue;
                            //20190717150929 furukawa ed ////////////////////////


                            p.AccountName = row.GetCell(2).StringCellValue.Trim();
                            p.AccountKana = Utility.ToNarrowKatakana(row.GetCell(3).StringCellValue.Trim());

                            


                            //テーブルとインポートファイル比較
                            if (dic.ContainsKey(p.Key))
                            {

                                //20190825173542 furukawa st ////////////////////////
                                //支払先コード、銀行コード、支店番号、口座番号、口座名、口座名カナが同じ場合は同じレコードとみなす
                                
                                if (dic[p.Key].Code.Trim() == p.Code.Trim() && 
                                    dic[p.Key].BankNo.Trim() == p.BankNo.Trim() &&
                                    dic[p.Key].BranchNo.Trim() == p.BranchNo.Trim() &&
                                    dic[p.Key].AccountNo.Trim() == p.AccountNo.Trim() &&
                                    dic[p.Key].AccountKana.Trim() == p.AccountKana.Trim() &&
                                    dic[p.Key].AccountName.Trim() == p.AccountName.Trim() 
                                    )
                                    continue;

                                            //20190719150328 furukawa st ////////////////////////
                                            //支払先コード、銀行コード、支店番号、口座番号が同じ場合は同じレコードとみなし、飛ばす                                

                                            //if (dic[p.Key].Code.Trim() == p.Code.Trim() &&
                                            //    dic[p.Key].BankNo.Trim() == p.BankNo.Trim() &&
                                            //    dic[p.Key].BranchNo.Trim() == p.BranchNo.Trim() &&
                                            //    dic[p.Key].AccountNo.Trim() == p.AccountNo.Trim())
                                            //    continue;

                                            //20190719150328 furukawa ed ////////////////////////
                                //20190825173542 furukawa ed ////////////////////////


                                //更新日を持っていないので無効化
                                //テーブルにある更新日が最近の場合、飛ばす
                                //if (dic[p.Key].UpdateDate > p.UpdateDate) continue;

                                //上記以外は更新

                                //20190719151423 furukawa st ////////////////////////
                                //追加更新レコードは今日の日付

                                p.UpdateDate = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                                //20190719151423 furukawa ed ////////////////////////
                                dic[p.Key] = p;
                            }
                            else
                            {
                                //新規レコードは追加
                                //20190719151348 furukawa st ////////////////////////
                                //追加更新レコードは今日の日付
                                
                                p.UpdateDate = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                                //20190719151348 furukawa ed ////////////////////////
                                dic.Add(p.Key, p);
                            }
                        }
                        catch
                        {
                            var res = MessageBox.Show($"{i + 1}行目のデータが読み込めませんでした。処理を続けますか？",
                                "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                            if (res != DialogResult.Yes) return false;
                            continue;
                        }
                    }
                }
                else
                {
                    throw new Exception("対応しているのは新Excelファイル形式(.xlsx)です。");
                }
                //                }
                #endregion

                #region db書込

                wf.SetMax(dic.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");

                using (var tran = DB.Main.CreateTransaction())
                {

                    if (!DB.Main.Excute("DELETE FROM paycode", null, tran))
                    {
                        MessageBox.Show("インポートに失敗しました");
                        return false;
                    }

                    //dicに入っているレコードをinsert
                    var g = dic.Values
                        .Select((p, i) => new { index = i, PayCode = p })                        
                        .GroupBy(x => x.index / 10)
                        .Select(gr => gr.Select(x => x.PayCode));


                    

                    foreach (var item in g)
                    {
                        wf.InvokeValue += item.Count();
                        
                        if (!DB.Main.Inserts(item, tran))
                        {                                                        
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                    

                }
                MessageBox.Show("インポートが正常に終了しました");
            }

            #endregion

            return true;
        }

        public static List<PayCode> Select(string accountNo)
        {
            if (accountNo.Length < 7) return new List<PayCode>();
            return DB.Main.Select<PayCode>($"accountno='{accountNo}'").ToList();
        }
    }
}
