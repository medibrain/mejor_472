﻿namespace Mejor.SharpKenpo
{
    partial class OtherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAuto = new System.Windows.Forms.Button();
            this.buttonNothing = new System.Windows.Forms.Button();
            this.buttonCheck = new System.Windows.Forms.Button();
            this.btnIkaTotu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonAuto
            // 
            this.buttonAuto.Location = new System.Drawing.Point(36, 193);
            this.buttonAuto.Name = "buttonAuto";
            this.buttonAuto.Size = new System.Drawing.Size(179, 49);
            this.buttonAuto.TabIndex = 0;
            this.buttonAuto.Text = "あはき自動マッチング";
            this.buttonAuto.UseVisualStyleBackColor = true;
            this.buttonAuto.Visible = false;
            this.buttonAuto.Click += new System.EventHandler(this.buttonAuto_Click);
            // 
            // buttonNothing
            // 
            this.buttonNothing.Location = new System.Drawing.Point(36, 138);
            this.buttonNothing.Name = "buttonNothing";
            this.buttonNothing.Size = new System.Drawing.Size(179, 49);
            this.buttonNothing.TabIndex = 0;
            this.buttonNothing.Text = "あはきマッチング確認";
            this.buttonNothing.UseVisualStyleBackColor = true;
            this.buttonNothing.Visible = false;
            this.buttonNothing.Click += new System.EventHandler(this.buttonNothing_Click);
            // 
            // buttonCheck
            // 
            this.buttonCheck.Location = new System.Drawing.Point(36, 223);
            this.buttonCheck.Name = "buttonCheck";
            this.buttonCheck.Size = new System.Drawing.Size(179, 49);
            this.buttonCheck.TabIndex = 0;
            this.buttonCheck.Text = "あはき確定処理";
            this.buttonCheck.UseVisualStyleBackColor = true;
            this.buttonCheck.Visible = false;
            this.buttonCheck.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // btnIkaTotu
            // 
            this.btnIkaTotu.Location = new System.Drawing.Point(36, 61);
            this.btnIkaTotu.Name = "btnIkaTotu";
            this.btnIkaTotu.Size = new System.Drawing.Size(179, 49);
            this.btnIkaTotu.TabIndex = 0;
            this.btnIkaTotu.Text = "医科レセ突合処理";
            this.btnIkaTotu.UseVisualStyleBackColor = true;
            this.btnIkaTotu.Click += new System.EventHandler(this.btnIkaTotu_Click);
            // 
            // OtherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 284);
            this.Controls.Add(this.btnIkaTotu);
            this.Controls.Add(this.buttonNothing);
            this.Controls.Add(this.buttonCheck);
            this.Controls.Add(this.buttonAuto);
            this.Name = "OtherForm";
            this.Text = "OtherForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAuto;
        private System.Windows.Forms.Button buttonNothing;
        private System.Windows.Forms.Button buttonCheck;
        private System.Windows.Forms.Button btnIkaTotu;
    }
}