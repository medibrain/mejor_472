﻿namespace Mejor.SharpKenpo
{
    partial class InputForm_Tekiyo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.pnlTekiyo = new System.Windows.Forms.Panel();
            this.lblJoken = new System.Windows.Forms.Label();
            this.dataGridTekiyo = new System.Windows.Forms.DataGridView();
            this.pnlSign = new System.Windows.Forms.Panel();
            this.lblRange = new System.Windows.Forms.Label();
            this.lblRatio = new System.Windows.Forms.Label();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblBirth = new System.Windows.Forms.Label();
            this.lblNone = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lblget = new System.Windows.Forms.Label();
            this.lblloss = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelOCR = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.label28 = new System.Windows.Forms.Label();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxF1D = new Mejor.VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.verifyBoxKyufuRate = new Mejor.VerifyBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.verifyBoxF1Start = new Mejor.VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxF1Finish = new Mejor.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxJuryoType = new Mejor.VerifyBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.verifyBoxHKigo = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.verifyBoxKanriBango2 = new Mejor.VerifyBox();
            this.verifyBoxKanriBango1 = new Mejor.VerifyBox();
            this.verifyBoxPref = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelPref = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.pnlTekiyo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTekiyo)).BeginInit();
            this.pnlSign.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 753);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 25);
            this.buttonRegist.TabIndex = 13;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Enabled = false;
            this.labelY.Location = new System.Drawing.Point(132, 24);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Enabled = false;
            this.labelM.Location = new System.Drawing.Point(189, 24);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Enabled = false;
            this.labelHnum.Location = new System.Drawing.Point(562, 12);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(55, 26);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保険者\r\n番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Location = new System.Drawing.Point(70, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            this.label8.Visible = false;
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 759);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 13);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(44, 781);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(44, 781);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 754);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 754);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 754);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-39, 754);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(44, 781);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.pnlTekiyo);
            this.panelRight.Controls.Add(this.pnlSign);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(264, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 781);
            this.panelRight.TabIndex = 2;
            // 
            // pnlTekiyo
            // 
            this.pnlTekiyo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlTekiyo.Controls.Add(this.lblJoken);
            this.pnlTekiyo.Controls.Add(this.dataGridTekiyo);
            this.pnlTekiyo.Location = new System.Drawing.Point(6, 572);
            this.pnlTekiyo.Name = "pnlTekiyo";
            this.pnlTekiyo.Size = new System.Drawing.Size(850, 180);
            this.pnlTekiyo.TabIndex = 62;
            // 
            // lblJoken
            // 
            this.lblJoken.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblJoken.Location = new System.Drawing.Point(7, 10);
            this.lblJoken.Name = "lblJoken";
            this.lblJoken.Size = new System.Drawing.Size(500, 20);
            this.lblJoken.TabIndex = 52;
            // 
            // dataGridTekiyo
            // 
            this.dataGridTekiyo.AllowUserToAddRows = false;
            this.dataGridTekiyo.AllowUserToDeleteRows = false;
            this.dataGridTekiyo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridTekiyo.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridTekiyo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridTekiyo.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridTekiyo.Location = new System.Drawing.Point(3, 36);
            this.dataGridTekiyo.Name = "dataGridTekiyo";
            this.dataGridTekiyo.ReadOnly = true;
            this.dataGridTekiyo.RowHeadersVisible = false;
            this.dataGridTekiyo.RowTemplate.Height = 21;
            this.dataGridTekiyo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridTekiyo.Size = new System.Drawing.Size(840, 130);
            this.dataGridTekiyo.StandardTab = true;
            this.dataGridTekiyo.TabIndex = 51;
            this.dataGridTekiyo.TabStop = false;
            // 
            // pnlSign
            // 
            this.pnlSign.Controls.Add(this.lblRange);
            this.pnlSign.Controls.Add(this.lblRatio);
            this.pnlSign.Controls.Add(this.lblGender);
            this.pnlSign.Controls.Add(this.lblBirth);
            this.pnlSign.Controls.Add(this.lblNone);
            this.pnlSign.Controls.Add(this.label31);
            this.pnlSign.Controls.Add(this.lblget);
            this.pnlSign.Controls.Add(this.lblloss);
            this.pnlSign.Controls.Add(this.label30);
            this.pnlSign.Location = new System.Drawing.Point(5, 116);
            this.pnlSign.Name = "pnlSign";
            this.pnlSign.Size = new System.Drawing.Size(900, 30);
            this.pnlSign.TabIndex = 61;
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRange.ForeColor = System.Drawing.Color.Gray;
            this.lblRange.Location = new System.Drawing.Point(513, 6);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(115, 23);
            this.lblRange.TabIndex = 63;
            this.lblRange.Text = "直近資格範囲外";
            // 
            // lblRatio
            // 
            this.lblRatio.AutoSize = true;
            this.lblRatio.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRatio.ForeColor = System.Drawing.Color.Gray;
            this.lblRatio.Location = new System.Drawing.Point(406, 6);
            this.lblRatio.Name = "lblRatio";
            this.lblRatio.Size = new System.Drawing.Size(100, 23);
            this.lblRatio.TabIndex = 63;
            this.lblRatio.Text = "給付割合相違";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGender.ForeColor = System.Drawing.Color.Gray;
            this.lblGender.Location = new System.Drawing.Point(325, 6);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(70, 23);
            this.lblGender.TabIndex = 62;
            this.lblGender.Text = "性別相違";
            // 
            // lblBirth
            // 
            this.lblBirth.AutoSize = true;
            this.lblBirth.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBirth.ForeColor = System.Drawing.Color.Gray;
            this.lblBirth.Location = new System.Drawing.Point(219, 6);
            this.lblBirth.Name = "lblBirth";
            this.lblBirth.Size = new System.Drawing.Size(100, 23);
            this.lblBirth.TabIndex = 61;
            this.lblBirth.Text = "生年月日相違";
            // 
            // lblNone
            // 
            this.lblNone.AutoSize = true;
            this.lblNone.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNone.ForeColor = System.Drawing.Color.Gray;
            this.lblNone.Location = new System.Drawing.Point(8, 6);
            this.lblNone.Name = "lblNone";
            this.lblNone.Size = new System.Drawing.Size(205, 23);
            this.lblNone.TabIndex = 60;
            this.lblNone.Text = "資格適用データに存在しない";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label31.Location = new System.Drawing.Point(642, 6);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 23);
            this.label31.TabIndex = 60;
            this.label31.Text = "資格喪失日";
            this.label31.Visible = false;
            // 
            // lblget
            // 
            this.lblget.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblget.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblget.Location = new System.Drawing.Point(731, 32);
            this.lblget.Name = "lblget";
            this.lblget.Size = new System.Drawing.Size(120, 24);
            this.lblget.TabIndex = 57;
            this.lblget.Visible = false;
            // 
            // lblloss
            // 
            this.lblloss.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblloss.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblloss.Location = new System.Drawing.Point(731, 5);
            this.lblloss.Name = "lblloss";
            this.lblloss.Size = new System.Drawing.Size(120, 24);
            this.lblloss.TabIndex = 58;
            this.lblloss.Visible = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label30.Location = new System.Drawing.Point(642, 31);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(85, 23);
            this.label30.TabIndex = 59;
            this.label30.Text = "資格取得日";
            this.label30.Visible = false;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 753);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 25);
            this.buttonBack.TabIndex = 14;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 753);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(30, 13);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Enabled = false;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(99, 12);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.label28);
            this.panelTotal.Controls.Add(this.verifyBoxF5);
            this.panelTotal.Controls.Add(this.verifyBoxF4);
            this.panelTotal.Controls.Add(this.verifyBoxF3);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.verifyBoxF2);
            this.panelTotal.Controls.Add(this.verifyBoxF1);
            this.panelTotal.Controls.Add(this.label22);
            this.panelTotal.Controls.Add(this.label21);
            this.panelTotal.Controls.Add(this.label20);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Controls.Add(this.label9);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Enabled = false;
            this.panelTotal.Location = new System.Drawing.Point(5, 476);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(980, 90);
            this.panelTotal.TabIndex = 10;
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(101, 22);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxCharge.TabIndex = 34;
            this.verifyBoxCharge.TextV = "";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(98, 5);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "請求金額";
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(452, 63);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(212, 23);
            this.verifyBoxF5.TabIndex = 44;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(234, 63);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(212, 23);
            this.verifyBoxF4.TabIndex = 43;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(11, 63);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(212, 23);
            this.verifyBoxF3.TabIndex = 42;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(11, 22);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxTotal.TabIndex = 33;
            this.verifyBoxTotal.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(421, 21);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(212, 23);
            this.verifyBoxF2.TabIndex = 41;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoTextBox_TextChanged);
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(203, 21);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(212, 23);
            this.verifyBoxF1.TabIndex = 40;
            this.verifyBoxF1.TextV = "";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(448, 47);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 13);
            this.label22.TabIndex = 8;
            this.label22.Text = "負傷名5";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(230, 47);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 47);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "負傷名3";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(420, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "負傷名2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(203, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "負傷名1";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(8, 5);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(55, 13);
            this.labelTotal.TabIndex = 12;
            this.labelTotal.Text = "合計金額";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 753);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 25);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(17, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxDays);
            this.panelHnum.Controls.Add(this.labelDays);
            this.panelHnum.Controls.Add(this.verifyBoxF1D);
            this.panelHnum.Controls.Add(this.label27);
            this.panelHnum.Controls.Add(this.verifyBoxKyufuRate);
            this.panelHnum.Controls.Add(this.label25);
            this.panelHnum.Controls.Add(this.label26);
            this.panelHnum.Controls.Add(this.verifyBoxF1Start);
            this.panelHnum.Controls.Add(this.label6);
            this.panelHnum.Controls.Add(this.verifyBoxF1Finish);
            this.panelHnum.Controls.Add(this.label15);
            this.panelHnum.Controls.Add(this.label19);
            this.panelHnum.Controls.Add(this.label23);
            this.panelHnum.Controls.Add(this.verifyBoxFamily);
            this.panelHnum.Controls.Add(this.verifyBoxJuryoType);
            this.panelHnum.Controls.Add(this.label24);
            this.panelHnum.Controls.Add(this.label13);
            this.panelHnum.Controls.Add(this.label1);
            this.panelHnum.Controls.Add(this.label14);
            this.panelHnum.Controls.Add(this.verifyBoxF1M);
            this.panelHnum.Controls.Add(this.verifyBoxF1Y);
            this.panelHnum.Controls.Add(this.label10);
            this.panelHnum.Controls.Add(this.label2);
            this.panelHnum.Controls.Add(this.label7);
            this.panelHnum.Controls.Add(this.verifyBoxHKigo);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.verifyBoxKanriBango2);
            this.panelHnum.Controls.Add(this.verifyBoxKanriBango1);
            this.panelHnum.Controls.Add(this.verifyBoxPref);
            this.panelHnum.Controls.Add(this.verifyBoxM);
            this.panelHnum.Controls.Add(this.verifyBoxBD);
            this.panelHnum.Controls.Add(this.verifyBoxBM);
            this.panelHnum.Controls.Add(this.verifyBoxBY);
            this.panelHnum.Controls.Add(this.verifyBoxBE);
            this.panelHnum.Controls.Add(this.verifyBoxSex);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Controls.Add(this.labelM);
            this.panelHnum.Controls.Add(this.labelBirthday);
            this.panelHnum.Controls.Add(this.labelSex);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.label4);
            this.panelHnum.Controls.Add(this.label16);
            this.panelHnum.Controls.Add(this.label17);
            this.panelHnum.Controls.Add(this.label18);
            this.panelHnum.Controls.Add(this.label12);
            this.panelHnum.Controls.Add(this.labelPref);
            this.panelHnum.Enabled = false;
            this.panelHnum.Location = new System.Drawing.Point(0, 0);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(900, 110);
            this.panelHnum.TabIndex = 7;
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Enabled = false;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(830, 68);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxDays.TabIndex = 29;
            this.verifyBoxDays.TextV = "";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Enabled = false;
            this.labelDays.Location = new System.Drawing.Point(830, 51);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(55, 13);
            this.labelDays.TabIndex = 56;
            this.labelDays.Text = "診療日数";
            // 
            // verifyBoxF1D
            // 
            this.verifyBoxF1D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1D.Enabled = false;
            this.verifyBoxF1D.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1D.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1D.Location = new System.Drawing.Point(651, 69);
            this.verifyBoxF1D.Name = "verifyBoxF1D";
            this.verifyBoxF1D.NewLine = false;
            this.verifyBoxF1D.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxF1D.TabIndex = 26;
            this.verifyBoxF1D.TextV = "";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Enabled = false;
            this.label27.Location = new System.Drawing.Point(677, 81);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 13);
            this.label27.TabIndex = 55;
            this.label27.Text = "日";
            // 
            // verifyBoxKyufuRate
            // 
            this.verifyBoxKyufuRate.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyufuRate.Enabled = false;
            this.verifyBoxKyufuRate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyufuRate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyufuRate.Location = new System.Drawing.Point(231, 69);
            this.verifyBoxKyufuRate.Name = "verifyBoxKyufuRate";
            this.verifyBoxKyufuRate.NewLine = false;
            this.verifyBoxKyufuRate.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKyufuRate.TabIndex = 18;
            this.verifyBoxKyufuRate.TextV = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Enabled = false;
            this.label25.Location = new System.Drawing.Point(229, 46);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 13);
            this.label25.TabIndex = 53;
            this.label25.Text = "給付割合";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Enabled = false;
            this.label26.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label26.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label26.Location = new System.Drawing.Point(260, 75);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 12);
            this.label26.TabIndex = 52;
            this.label26.Text = "7～10";
            // 
            // verifyBoxF1Start
            // 
            this.verifyBoxF1Start.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Start.Enabled = false;
            this.verifyBoxF1Start.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Start.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Start.Location = new System.Drawing.Point(707, 69);
            this.verifyBoxF1Start.MaxLength = 2;
            this.verifyBoxF1Start.Name = "verifyBoxF1Start";
            this.verifyBoxF1Start.NewLine = false;
            this.verifyBoxF1Start.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Start.TabIndex = 27;
            this.verifyBoxF1Start.TextV = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(764, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 48;
            this.label6.Text = "終了日";
            // 
            // verifyBoxF1Finish
            // 
            this.verifyBoxF1Finish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Finish.Enabled = false;
            this.verifyBoxF1Finish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Finish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Finish.Location = new System.Drawing.Point(766, 69);
            this.verifyBoxF1Finish.MaxLength = 2;
            this.verifyBoxF1Finish.Name = "verifyBoxF1Finish";
            this.verifyBoxF1Finish.NewLine = false;
            this.verifyBoxF1Finish.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxF1Finish.TabIndex = 28;
            this.verifyBoxF1Finish.TextV = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Enabled = false;
            this.label15.Location = new System.Drawing.Point(705, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "開始日";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Location = new System.Drawing.Point(794, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 50;
            this.label19.Text = "日";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Enabled = false;
            this.label23.Location = new System.Drawing.Point(735, 81);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 13);
            this.label23.TabIndex = 47;
            this.label23.Text = "日";
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Enabled = false;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(155, 69);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxFamily.TabIndex = 16;
            this.verifyBoxFamily.TextV = "";
            // 
            // verifyBoxJuryoType
            // 
            this.verifyBoxJuryoType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxJuryoType.Enabled = false;
            this.verifyBoxJuryoType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxJuryoType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxJuryoType.Location = new System.Drawing.Point(46, 69);
            this.verifyBoxJuryoType.Name = "verifyBoxJuryoType";
            this.verifyBoxJuryoType.NewLine = false;
            this.verifyBoxJuryoType.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxJuryoType.TabIndex = 13;
            this.verifyBoxJuryoType.TextV = "";
            this.verifyBoxJuryoType.TextChanged += new System.EventHandler(this.verifyBoxJuryoType_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Enabled = false;
            this.label24.Location = new System.Drawing.Point(151, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 13);
            this.label24.TabIndex = 42;
            this.label24.Text = "本人家族";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Enabled = false;
            this.label13.Location = new System.Drawing.Point(44, 46);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "本家区分";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(182, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 26);
            this.label1.TabIndex = 44;
            this.label1.Text = "2.本人\r\n6.家族";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label14.Location = new System.Drawing.Point(74, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 36);
            this.label14.TabIndex = 41;
            this.label14.Text = "2.本人 8.高一 \r\n4.六歳\r\n6.家族 0.高７";
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Enabled = false;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(602, 69);
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1M.TabIndex = 25;
            this.verifyBoxF1M.TextV = "";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Enabled = false;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(558, 69);
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1Y.TabIndex = 24;
            this.verifyBoxF1Y.TextV = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Location = new System.Drawing.Point(556, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "初検日";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(628, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "月";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(585, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "年";
            // 
            // verifyBoxHKigo
            // 
            this.verifyBoxHKigo.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHKigo.Enabled = false;
            this.verifyBoxHKigo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHKigo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHKigo.Location = new System.Drawing.Point(623, 11);
            this.verifyBoxHKigo.Name = "verifyBoxHKigo";
            this.verifyBoxHKigo.NewLine = false;
            this.verifyBoxHKigo.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHKigo.TabIndex = 10;
            this.verifyBoxHKigo.TextV = "";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Enabled = false;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(729, 11);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(111, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextV = "";
            // 
            // verifyBoxKanriBango2
            // 
            this.verifyBoxKanriBango2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKanriBango2.Enabled = false;
            this.verifyBoxKanriBango2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKanriBango2.Location = new System.Drawing.Point(355, 13);
            this.verifyBoxKanriBango2.MaxLength = 5;
            this.verifyBoxKanriBango2.Name = "verifyBoxKanriBango2";
            this.verifyBoxKanriBango2.NewLine = false;
            this.verifyBoxKanriBango2.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxKanriBango2.TabIndex = 2;
            this.verifyBoxKanriBango2.TextV = "";
            this.verifyBoxKanriBango2.Validated += new System.EventHandler(this.verifyBoxKanriBango2_Validated);
            // 
            // verifyBoxKanriBango1
            // 
            this.verifyBoxKanriBango1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKanriBango1.Enabled = false;
            this.verifyBoxKanriBango1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKanriBango1.Location = new System.Drawing.Point(290, 13);
            this.verifyBoxKanriBango1.MaxLength = 4;
            this.verifyBoxKanriBango1.Name = "verifyBoxKanriBango1";
            this.verifyBoxKanriBango1.NewLine = false;
            this.verifyBoxKanriBango1.Size = new System.Drawing.Size(60, 23);
            this.verifyBoxKanriBango1.TabIndex = 1;
            this.verifyBoxKanriBango1.TextV = "";
            // 
            // verifyBoxPref
            // 
            this.verifyBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPref.Enabled = false;
            this.verifyBoxPref.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPref.Location = new System.Drawing.Point(513, 12);
            this.verifyBoxPref.Name = "verifyBoxPref";
            this.verifyBoxPref.NewLine = false;
            this.verifyBoxPref.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxPref.TabIndex = 6;
            this.verifyBoxPref.TextV = "";
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Enabled = false;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(154, 12);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 0;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Enabled = false;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(503, 69);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBD.TabIndex = 23;
            this.verifyBoxBD.TextV = "";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Enabled = false;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(460, 69);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBM.TabIndex = 22;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Enabled = false;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(417, 69);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBY.TabIndex = 21;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Enabled = false;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(366, 69);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxBE.TabIndex = 20;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Enabled = false;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(299, 69);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxSex.TabIndex = 19;
            this.verifyBoxSex.TextV = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Enabled = false;
            this.labelBirthday.Location = new System.Drawing.Point(362, 46);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(55, 13);
            this.labelBirthday.TabIndex = 18;
            this.labelBirthday.Text = "生年月日";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Enabled = false;
            this.labelSex.Location = new System.Drawing.Point(295, 46);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 15;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(392, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 39);
            this.label5.TabIndex = 13;
            this.label5.Text = "3.昭\r\n4.平\r\n5.令";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(326, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 26);
            this.label4.TabIndex = 17;
            this.label4.Text = "1.男\r\n2.女";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Enabled = false;
            this.label16.Location = new System.Drawing.Point(529, 81);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "日";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Location = new System.Drawing.Point(486, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Location = new System.Drawing.Point(443, 81);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "年";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Enabled = false;
            this.label12.Location = new System.Drawing.Point(229, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "ナンバリング";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelPref
            // 
            this.labelPref.AutoSize = true;
            this.labelPref.Enabled = false;
            this.labelPref.Location = new System.Drawing.Point(460, 14);
            this.labelPref.Name = "labelPref";
            this.labelPref.Size = new System.Drawing.Size(55, 26);
            this.labelPref.TabIndex = 5;
            this.labelPref.Text = "都道府県\r\n番号";
            this.labelPref.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 170);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 137);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 300);
            this.scrollPictureControl1.TabIndex = 19;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(261, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 781);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm_Tekiyo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1284, 781);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm_Tekiyo";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.pnlTekiyo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTekiyo)).EndInit();
            this.pnlSign.ResumeLayout(false);
            this.pnlSign.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Panel panelHnum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private VerifyBox verifyBoxPref;
        private System.Windows.Forms.Label labelPref;
        private VerifyBox verifyBoxHKigo;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private VerifyBox verifyBoxF1M;
        private VerifyBox verifyBoxF1Y;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private VerifyBox verifyBoxKanriBango1;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxKanriBango2;
        private VerifyBox verifyBoxFamily;
        private VerifyBox verifyBoxJuryoType;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxF1Start;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxF1Finish;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxF1D;
        private System.Windows.Forms.Label label27;
        private VerifyBox verifyBoxKyufuRate;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label label28;
        private VerifyBox verifyBoxDays;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblloss;
        private System.Windows.Forms.Label lblget;
        private System.Windows.Forms.DataGridView dataGridTekiyo;
        private System.Windows.Forms.Panel pnlSign;
        private System.Windows.Forms.Label lblRange;
        private System.Windows.Forms.Label lblRatio;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblBirth;
        private System.Windows.Forms.Label lblNone;
        private System.Windows.Forms.Panel pnlTekiyo;
        private System.Windows.Forms.Label lblJoken;
    }
}