﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using Mejor.Pay;

//20190426155225 furukawa st ////////////////////////
//資格適用データインポートクラス

namespace Mejor.SharpKenpo
{
    //シャープ健保資格適用データレイアウト仕様より続柄
    public enum flgZokugara
    {
        本人 = 0, 夫 = 11, 妻 = 12, 夫未届 = 13, 妻未届 = 14, 妻の子男 = 15, 妻の子女 = 16, 再婚子男 = 17,
        再婚子女 = 18, 長男 = 21, 次男 = 22, 三男 = 23, 四男 = 24, 五男 = 25, 六男 = 26,
        七男 = 27, 子の夫 = 28, 養子 = 29, 長女 = 31, 次女 = 32, 三女 = 33, 四女 = 34,
        五女 = 35, 六女 = 36, 七女 = 37, 子の妻 = 38, 養女 = 39, 父 = 41, 養父 = 42, 義父 = 43,
        祖父 = 44, 曾祖父 = 45, 母 = 51, 養母 = 52, 義母 = 53, 祖母 = 54, 曾祖母 = 55, 兄 = 61, 弟 = 62,
        義兄 = 63, 義弟 = 64, 姉 = 71, 妹 = 72, 義姉 = 73, 義妹 = 74, 孫 = 81, ひ孫 = 82, 叔父 = 83,
        叔母 = 84, 甥 = 85, 姪 = 86, 従兄弟 = 87, 従姉妹 = 88, 伯父 = 89, 伯母 = 90,
    }


    //適用データ用テーブル
    class TekiyouData
    {
        //[DB.DbAttribute.PrimaryKey]
        public string Gno { get; set; } = string.Empty;
        public string SyoNo { get; set; } = string.Empty;
        public string Branch { get; set; } = string.Empty;
        public string Branch2 { get; set; } = string.Empty;
        public string famNo { get; set; } = string.Empty;
        public string hKana { get; set; } = string.Empty;
        public string hName { get; set; } = string.Empty;
        public string hGender { get; set; } = string.Empty;
        public string Zokugara { get; set; } = string.Empty;
        public string BirthG { get; set; } = string.Empty;
        public string BirthYMD { get; set; } = string.Empty;
        public string GetG { get; set; } = string.Empty;
        public string GetYMD { get; set; } = string.Empty;
        public string LossG { get; set; } = string.Empty;
        public string LossYMD { get; set; } = string.Empty;
        public int Ratio { get; set; }
        public string issueDate { get; set; } = string.Empty;
        public string zip { get; set; } = string.Empty;
        public string add { get; set; } = string.Empty;
        public DateTime BirthADYMD { get; set; } = DateTime.MinValue;
        public DateTime GetADYMD { get; set; } = DateTime.MinValue;
        public DateTime LossADYMD { get; set; } = DateTime.MinValue;        
        public string etc01 { get; set; } = string.Empty;
        public string hihoNum { get; set; } = string.Empty;//20190430220841 furukawa hihonum追加
        public DateTime issueADYMD { get; set; } = DateTime.MinValue;//20190504161501 furukawa issueADYMD追加


        /// <summary>
        /// 証番号で資格適用データ抽出
        /// </summary>
        /// <param name="SyoNo"></param>
        /// <returns></returns>
        public static List<TekiyouData> SerchForMatchingSyoNo(string SyoNo)
        {
            List<TekiyouData> res = new List<TekiyouData>();
            res = DB.Main.Select<TekiyouData>(new { SyoNo }).ToList();
            foreach (TekiyouData t in res) cng(t);
            return res;

        }

        /// <summary>
        /// 証番号　生年月日　性別　割合で資格適用データ抽出
        /// </summary>
        /// <param name="SyoNo"></param>
        /// <param name="BirthADYMD"></param>
        /// <param name="hGender"></param>
        /// <param name="Ratio"></param>
        /// <param name="Zokugara"></param>
        /// <returns></returns>
        public static List<TekiyouData> SerchForMatchingAll(string SyoNo, DateTime BirthADYMD, string hGender, int Ratio)
        {
            List<TekiyouData> res = new List<TekiyouData>();
            res = DB.Main.Select<TekiyouData>(new { SyoNo, BirthADYMD, hGender, Ratio }).ToList();
            foreach (TekiyouData t in res) cng(t);            
            return res;

        }

        /// <summary>
        /// 証番号で資格適用データ抽出
        /// </summary>
        /// <param name="BirthADYMD"></param>
        /// <param name="hGender"></param>
        /// <param name="Ratio"></param>
        /// <returns></returns>
        public static List<TekiyouData> SerchForMatchingBGR(DateTime BirthADYMD, string hGender, int Ratio)
        {
           
            List<TekiyouData> res = new List<TekiyouData>();
            res = DB.Main.Select<TekiyouData>(new { BirthADYMD, hGender, Ratio }).ToList();
            foreach (TekiyouData t in res) cng(t);
            return res;
        }
         

        public static List<TekiyouData> SerchForMatchingBG(DateTime BirthADYMD, string hGender)
        {
            List<TekiyouData> res = new List<TekiyouData>();
            res = DB.Main.Select<TekiyouData>(new { BirthADYMD, hGender }).ToList();
            foreach (TekiyouData t in res) cng(t);
            return res;
        }
          
        /// <summary>
        /// 文字に変換
        /// </summary>
        /// <param name="t"></param>
        private static void cng(TekiyouData t)
        {
            t.hGender = t.hGender == "1" ? "男" : "女";
            t.Zokugara = t.Zokugara == "" ? "本人" : ((flgZokugara)Enum.Parse(typeof(flgZokugara), t.Zokugara)).ToString();
        }
    }

    class TekiyoImporter
    {

        public static bool Import()
        {
            List<string> fileNames = new List<string>();

            using (var f = new OpenFileDialog())
            {
                f.Multiselect = true;
                f.Title = "シャープ健保 資格適用情報取込";
                f.Filter = "CSVファイル|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileNames.AddRange(f.FileNames);

            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                //  wf.LogPrint("すでに登録された情報を取得しています");
                //  var pl = DB.Main.SelectAll<TekiyoImporter>();
                //  var dic = new Dictionary<string, TekiyoImporter>();

                //var pl = DB.Main.SelectAll<TekiyouData>();
                var dic = new Dictionary<string, TekiyouData>();


                // foreach (var item in pl) dic.Add(item.InsdNum, item);

                //       wf.LogPrint($"{pl.Count()}件の登録済み情報を取得しました");

                var l = new List<TekiyouData>();

                #region CSVロード
                for (int cnt = 0; cnt < fileNames.Count; cnt++)
                {
                    #region CSVロード

                    wf.LogPrint("CSVを読み込んでいます");
                    //var l = new List<TekiyouData>();
                    //var csv = CommonTool.CsvImportMultiCode(fileName);
                    var csv = CommonTool.CsvImportMultiCode(fileNames[cnt]);

                    for (int i = 1; i < csv.Count; i++)
                    {
                        if (wf.Cancel)
                        {
                            var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Asterisk);
                            if (cres == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }
                        var item = csv[i];

                        if (item.Length < 18) continue;
                        var p = new TekiyouData();


                        if (item[7].ToString().Length == 2)
                        {
                            //家族データ

                            p.Gno = item[0].Trim();                       //1	事業所No                
                            p.SyoNo = item[1].Trim();                     //2	証No
                            p.Branch = item[2].Trim();                    //3	枝番
                            p.famNo = item[3].Trim();                     //4	家族No
                            p.hKana = item[4].Trim();                     //5	カナ氏名 
                            p.hName = item[5].Trim();                     //6	氏名
                            p.hGender = item[6].Trim();                   //7	性別
                            p.Zokugara = item[7].Trim();                  //8 	続柄CD
                            p.BirthG = item[8].Trim();                    //9   生年月日）　元号


                            //20190826110820 furukawa st ////////////////////////
                            //年月日は6桁0埋めにする
                            
                            p.BirthYMD = item[9].Trim().PadLeft(6, '0');        //10	生年月日）　年月日
                            p.GetG = item[10].Trim();                           //11	資格取得日）　元号
                            p.GetYMD = item[11].Trim().PadLeft(6,'0');          //12	資格取得日）　年月日
                            p.LossG = item[12].Trim();                          //13	資格喪失日）　元号
                            p.LossYMD = item[13].Trim().PadLeft(6, '0');        //14	資格喪失日）　年月日

                                //p.BirthYMD = item[9].Trim();                  //10	生年月日）　年月日
                                //p.GetG = item[10].Trim();                     //11	資格取得日）　元号
                                //p.GetYMD = item[11].Trim();                   //12	資格取得日）　年月日
                                //p.LossG = item[12].Trim();                    //13	資格喪失日）　元号                  
                                //p.LossYMD = item[13].Trim();                  //14	資格喪失日）　年月日
                            //20190826110820 furukawa ed ////////////////////////


                            p.Ratio = item[14].Trim() == string.Empty ? 3:int.Parse(item[14].Trim());//15	自己負担割合 空欄は7割給付（3割負担）
                            //p.Ratio = item[14].Trim() == string.Empty ? 0 : int.Parse(item[14].Trim());//15	自己負担割合
                            p.issueDate = item[15].Trim();                //16	発効日                            
                            p.zip = item[16].Trim();                      //17	郵便番号
                            p.add = item[17].Trim();                      //18	住所
                            p.etc01 = item[18].Trim();                   //19	氏名コード



                        }
                        else
                        {

                            //本人データ
                            p.Gno = item[0].Trim();              //1	事業所No
                            p.SyoNo = item[1].Trim();            //2	証No
                            p.Branch = item[2].Trim();           //3	枝番
                            p.Branch2 = item[3].Trim();          //4	最終枝番
                            p.hKana = item[4].Trim();            //5	カナ氏名
                            p.hName = item[5].Trim();            //6	氏名
                            p.hGender = item[6].Trim();          //7	性別
                            p.BirthG = item[7].Trim();           //8	生年月日）　元号


                            //20190826110713 furukawa st ////////////////////////
                            //年月日は6桁0埋めにする
                            
                            p.BirthYMD = item[8].Trim();         //9	生年月日）　年月日
                            p.GetG = item[9].Trim();             //10	資格取得日）　元号
                            p.GetYMD = item[10].Trim();          //11	資格取得日）　年月日
                            p.LossG = item[11].Trim();           //12	資格喪失日）　元号
                            p.LossYMD = item[12].Trim();         //13	資格喪失日）　年月日

                                //p.BirthYMD = item[8].Trim();         //9	生年月日）　年月日
                                //p.GetG = item[9].Trim();             //10	資格取得日）　元号
                                //p.GetYMD = item[10].Trim();          //11	資格取得日）　年月日
                                //p.LossG = item[11].Trim();           //12	資格喪失日）　元号
                                //p.LossYMD = item[12].Trim();         //13	資格喪失日）　年月日
                            //20190826110713 furukawa ed ////////////////////////



                            p.Ratio = item[13].Trim() == string.Empty ? 3 : int.Parse(item[13].Trim());//14	自己負担割合 空欄は7割給付（3割負担）
                            //p.Ratio = item[13].Trim() == string.Empty ? 0 : int.Parse(item[13].Trim());//14	自己負担割合
                            p.issueDate = item[14].Trim();       //15	発効日                            
                            p.zip = item[15].Trim();             //16	郵便番号
                            p.add = item[16].Trim();             //17	住所
                            p.etc01 = item[17].Trim();           //18	氏名コード

                        }


                        //計算用に西暦を持たせる

                        p.BirthADYMD = DateTimeEx.GetDateFromJstr7(p.BirthG + p.BirthYMD.PadLeft(6,'0'));
                        p.GetADYMD = DateTimeEx.GetDateFromJstr7(p.GetG +  p.GetYMD.PadLeft(6,'0'));
                        p.LossADYMD = DateTimeEx.GetDateFromJstr7(p.LossG + p.LossYMD.PadLeft(6,'0'));

                        p.issueADYMD = DateTimeEx.GetDateFromJstr7(p.issueDate);//発効日も西暦持たせる

                        p.hihoNum = p.Gno +'-'+ p.SyoNo;//20190430220740 furukawa hihonum追加

                        l.Add(p);
                    }
                    #endregion

                }

                #endregion

                #region 登録
                wf.LogPrint($"CSVより{l.Count}件の情報を取得しました");

                wf.LogPrint("データベースへ登録を行ないます");
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                int updateCount = 0;
                int insertCount = 0;

                using (var tran = DB.Main.CreateTransaction())
                {
                    //先に全削除
                    DB.Main.Excute("truncate table TekiyouData", tran);

                    foreach (var item in l)
                    {
                        if (wf.Cancel)
                        {
                            var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Asterisk);
                            if (cres == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }



                        wf.InvokeValue++;
                        if (dic.ContainsKey(item.SyoNo))
                        {
                            /*
                            if (dic[item.InsdNum].InsNum == item.InsNum &&
                               dic[item.InsdNum].Name == item.Name &&
                               dic[item.InsdNum].Zip == item.Zip &&
                               dic[item.InsdNum].Address == item.Address &&
                               dic[item.InsdNum].DestName == item.DestName)
                                continue;*/

                            if (!DB.Main.Update(item, tran))
                            {
                                MessageBox.Show("情報のインポートに失敗しました");
                                return false;
                            }
                            updateCount++;
                        }
                        else
                        {
                            if (!DB.Main.Insert(item, tran))
                            {
                                MessageBox.Show("情報のインポートに失敗しました");
                                return false;
                            }
                            insertCount++;
                        }


                    }


                    //20190620162109 furukawa st ////////////////////////
                    //何故か作成されないためテーブル作成タイミング修正
                    //using外に出した

                    ////資格取得日、喪失日をまとめたテーブルを作成
                    //if (!CreateGetLossTable(tran))
                    //{

                    //    MessageBox.Show("テーブル作成に失敗しました");
                    //    return false;

                    //}

                    //20190620162109 furukawa ed ////////////////////////


                    tran.Commit();
                }


                //20190620162246 furukawa st ////////////////////////
                //何故か作成されないためテーブル作成タイミング修正
                
                using (var tran = DB.Main.CreateTransaction())
                {

                    //資格取得日、喪失日をまとめたテーブルを作成
                    if (!CreateGetLossTable(tran))
                    {
                        MessageBox.Show("テーブル作成に失敗しました");
                        return false;
                    }
                    tran.Commit();
                }
                //20190620162246 furukawa ed ////////////////////////

                MessageBox.Show(
                    $"新規登録：{insertCount}件\r\n" +
                    $"更新：{updateCount}件\r\n\r\n" +
                    $"情報のインポートが完了しました");
                return true;

                #endregion

            }

        }


        #region 取得日喪失日まとめテーブル作成
        /// <summary>
        /// 取得日喪失日まとめテーブル
        /// </summary>
        /// <param name="tran"></param>
        /// <returns>成功=true</returns>
        private static bool CreateGetLossTable(DB.Transaction tran)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(" DROP TABLE TEKIYOUDATA_GETLOSS;");
           
            sb.AppendLine(" CREATE TABLE TEKIYOUDATA_GETLOSS AS ");

            sb.AppendLine(" SELECT syono,BRANCH,BRANCH2,ZOKUGARA,");
            sb.AppendLine(" MIN(GETADYMD) AS GET,");
            sb.AppendLine(" MAX(");
            sb.AppendLine(" 	CASE LOSSADYMD ");
            sb.AppendLine(" 		WHEN '-INFINITY' THEN '2099-12-31' ");
            sb.AppendLine(" 		ELSE LOSSADYMD END ");//-infinityの場合は未来を固定で入れる
            sb.AppendLine(" ) AS LOSS, ");

            sb.AppendLine(" MAX(");
            sb.AppendLine(" 	CASE issueadymd ");
            sb.AppendLine(" 		WHEN '-INFINITY' THEN '2099-12-31' ");
            sb.AppendLine(" 		ELSE issueadymd END ");//-infinityの場合は未来を固定で入れる
            sb.AppendLine(" ) AS issue ");

            sb.AppendLine("  ");
            sb.AppendLine(" FROM TEKIYOUDATA");

            sb.AppendLine(" WHERE BRANCH='0'");//最新レコード（仕様より）

            sb.AppendLine(" GROUP BY ");
            sb.AppendLine(" syono,BRANCH,BRANCH2,ZOKUGARA");

            return DB.Main.Excute(sb.ToString(), tran);
        }
        #endregion


        /// <summary>
        /// 本人、家族データの判断を8列目の桁数で行う
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        private static string ChkHonninKazoku(List<string> csv)
        {

            bool flg=false;

            for (int r = 0; r < csv.Count; r++)
            {
                string tmp = csv[r].ToString();
                if (tmp[8].ToString().Length == 2)
                {
                    flg |= true;
                }
                else
                {
                    flg |= false;
                }

            }
            if (!flg) return "honnin";
            if (flg) return "kazoku";

            return string.Empty;

        }

    }
}
