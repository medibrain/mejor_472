﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;


/// <summary>
/// シャープ健保資格適用フォーム
/// </summary>
namespace Mejor.SharpKenpo
{
    public partial class InputForm_Tekiyo : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsTekiyo = new BindingSource();
        protected override Control inputPanel => panelRight;

        //シャープ健保用 保険者番号
        string iNumber = "";

        /// <summary>
        /// レセプト管理番号
        /// </summary>
        string strRezeKanriBango = string.Empty;

        /// <summary>
        /// 検索番号
        /// </summary>
        string strKensakuBango = string.Empty;


        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(0, 750);
        Point posCost = new Point(400, 2000);
        Point posDays = new Point(400, 750);
        Point posNumbering = new Point(300, 0);
        Point posBatch = new Point(200, 600);
        Point posBatchCount = new Point(600, 200);
        Point posBatchDrCode = new Point(200, 900);

        Control[] ymControls, hnumControls, personControls, dayControls, costControls, fushoControls;


        //適用データとappを比較して疑いのあるリスト
/*        class doubtData
        {
            public int Aid { get; set; } = 0;
            public string Groupid { get; set; } = String.Empty;
            public string Ayear { get; set; } = String.Empty;
            public string Amonth { get; set; } = String.Empty;
            public string Hnum { get; set; } = String.Empty;
            public string Pbirthday { get; set; } = String.Empty;
            public string gender { get; set; } = String.Empty;
            public string istartdate1 { get; set; } = String.Empty;
            public string ifinishdate1 { get; set; } = String.Empty;
            public string gno { get; set; } = String.Empty;
            public string syono { get; set; } = String.Empty;
            public string branch { get; set; } = String.Empty;
            public string branch2 { get; set; } = String.Empty;
            public DateTime getadymd { get; set; } = DateTime.MinValue;
            public DateTime lossadymd { get; set; } = DateTime.MinValue;

            public static List<doubtData> GetApps()
            {

                System.Text.StringBuilder sb = new StringBuilder();
                sb.AppendLine("select ");
                sb.AppendLine(" a.aid");
                sb.AppendLine(",a.groupid");
                sb.AppendLine(",a.ayear");
                sb.AppendLine(",a.amonth");
                sb.AppendLine(",a.hnum 被保番");
                sb.AppendLine(",a.pbirthday 生年月日");
                sb.AppendLine(",case a.psex when '1' then '男' else '女' end 性別");
                sb.AppendLine(",a.istartdate1 開始");
                sb.AppendLine(",a.ifinishdate1 終了");
                sb.AppendLine(",t.gno 事業所No");
                sb.AppendLine(",t.syono 証番号");
                sb.AppendLine(",t.branch");
                sb.AppendLine(",t.branch2");
                sb.AppendLine(",t.getadymd 取得日");
                sb.AppendLine(",max(t.lossadymd) 失効日");
                sb.AppendLine(" from application a ");
                sb.AppendLine(" inner join TekiyouData t");
                sb.AppendLine(" on a.hnum=t.gno || '-' || t.syono");
                sb.AppendLine(" and t.branch='0'");
                sb.AppendLine(" WHERE");
                sb.AppendLine(" (NOT(A.ISTARTDATE1 BETWEEN T.GETADYMD  AND  T.LOSSADYMD )");
                sb.AppendLine(" or ");
                sb.AppendLine(" NOT(A.ifinishdate1 BETWEEN T.GETADYMD  AND  T.LOSSADYMD ))");
                sb.AppendLine(" AND T.LOSSADYMD<>'-INFINITY'");

                sb.AppendLine(" group by");
                sb.AppendLine(" a.aid");
                sb.AppendLine(",a.groupid");
                sb.AppendLine(",a.ayear");
                sb.AppendLine(",a.amonth");
                sb.AppendLine(",a.hnum ,a.pbirthday ");
                sb.AppendLine(",a.psex");
                sb.AppendLine(",a.istartdate1 ");
                sb.AppendLine(",a.ifinishdate1 ");
                sb.AppendLine(",t.gno ");
                sb.AppendLine(",t.syono ");
                sb.AppendLine(",t.branch");
                sb.AppendLine(",t.branch2");

                sb.AppendLine(",t.getadymd ");
                sb.AppendLine(" order by a.aid,a.hnum,a.ayear,a.amonth,a.istartdate1");

                var z=DB.Main.SelectAll<doubtData>();
                
                var l= DB.Main.Query<doubtData>(sb.ToString());
                
                return l.ToList();

            }
        }

        */

        int cym = 0;
               
        /// <summary>
        /// 資格適用データで不備があるデータの作成
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        private List<App> CreateData(WaitForm wf)
        {

            List<App> lstRes = new List<App>();
            Dictionary<string, App> dic = new Dictionary<string, App>();
            List<App> appList = App.GetApps(cym, InsurerID.SHARP_KENPO);


            StringBuilder sbWhereHon = new StringBuilder();
            sbWhereHon.AppendLine(" AND substr(cast(a.afamily as text),1,1)='2' ");     //本家区分より頭2は本人
            sbWhereHon.AppendLine(" AND a.cym=" + cym);                                 //処理年月
            sbWhereHon.AppendLine(" AND a.aapptype=" + (int)APP_TYPE.柔整);             //柔整レセ

            StringBuilder sbSyoNo = new StringBuilder();
            sbSyoNo.AppendLine(" AND split_part(trim(A.HNUM),'-',2)=trim(Syono) ");            //証番号で結合

            StringBuilder sbWhereKaz = new StringBuilder();
            sbWhereKaz.AppendLine(" AND substr(cast(a.afamily as text),1,1)='6' ");     //本家区分より頭6は家族
            sbWhereKaz.AppendLine(" AND a.cym=" + cym);                                 //処理年月
            sbWhereKaz.AppendLine(" AND a.aapptype=" + (int)APP_TYPE.柔整);             //柔整レセ


            StringBuilder sbSQL = new StringBuilder();


            //existsにいない人をSelectし、そのレコードの各エラーフラグをあげる
            //最後にどれかフラグが上がっているレコードをリストとして画面に表示

            #region 本人

            wf.LogPrint("資格適用データにいない証番号を抽出");
            //App処理年月リスト
            

            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");
            sbSQL.AppendLine(" SELECT hihonum FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE 1=1 ");      
            sbSQL.AppendLine(sbSyoNo.ToString());       //証番号で結合
            sbSQL.AppendLine(" AND zokugara=''");       //続柄なし＝本人
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード

            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereHon.ToString());    //本人、処理年月、柔整レセ
           

            //資格適用データにいない人
            List<App> appListSikaku_NotMatch = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListSikaku_NotMatch)
            {
                appList.ForEach(al =>{if( al.Aid == a.Aid)
                    al.TaggedDatas.flgSikaku_SikakuNotMatch = true;
                });
                
            }
            wf.LogPrint("資格適用データにいない証番号を抽出：" + appListSikaku_NotMatch.Count.ToString());




            wf.LogPrint("資格適用データに存在、生年月日違い(本人)を抽出");
            //資格適用データに存在して 生年月日違い
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");

            sbSQL.AppendLine(" SELECT HIHONUM,birthadymd FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE A.pBirthday=birthadymd ");
            sbSQL.AppendLine(sbSyoNo.ToString());                                //証番号で結合
            sbSQL.AppendLine(" AND zokugara=''");                                //続柄なし＝本人
            sbSQL.AppendLine(" AND branch='0'");                                 //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereHon.ToString());                             //本人、処理年月、柔整レセ

            List<App> appListBirth_NotMatch = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListBirth_NotMatch)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_SikakuNotMatch = true;
                });

            }
            wf.LogPrint("資格適用データに存在、生年月日違い(本人)を抽出：" + appListBirth_NotMatch.Count.ToString());




            wf.LogPrint("資格適用データに存在、性別違いを抽出");
            //本人　資格適用データに存在して 性別違い
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");

            sbSQL.AppendLine(" SELECT HIHONUM,hgender FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE A.psex=cast(hgender as int) ");
            sbSQL.AppendLine(sbSyoNo.ToString());                                //証番号で結合
            sbSQL.AppendLine(" AND zokugara=''");                              //続柄なし＝本人
            sbSQL.AppendLine(" AND branch='0'");                                 //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereHon.ToString());                             //本人、処理年月、柔整レセ

            List<App> appListGender_NotMatch = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListGender_NotMatch)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_GenderNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データに存在、性別違い(本人)を抽出：" + appListGender_NotMatch.Count.ToString());




            wf.LogPrint("資格適用データに存在、割合違いを抽出");
            //本人　資格適用データに存在して 割合違い
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");
            sbSQL.AppendLine(" SELECT HIHONUM,hgender FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE 10- A.aratio=cast(ratio as int) and 10-A.aratio>0 ");
            sbSQL.AppendLine(sbSyoNo.ToString());                                //証番号で結合
            sbSQL.AppendLine(" AND zokugara=''");                              //続柄なし＝本人
            sbSQL.AppendLine(" AND branch='0'");                                 //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereHon.ToString());                             //本人、処理年月、柔整レセ

            List<App> appListratio_NotMatch = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListratio_NotMatch)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_RatioNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データに存在、割合違い(本人)を抽出：" + appListratio_NotMatch.Count.ToString());


            wf.LogPrint("資格適用データに存在、資格喪失を抽出");
            //本人　資格適用データに存在して 資格喪失
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");
            sbSQL.AppendLine(" SELECT SyoNo,get,loss FROM tekiyoudata_getloss ");
            sbSQL.AppendLine(" WHERE ");

            sbSQL.AppendLine(" ((A.ISTARTDATE1 BETWEEN GET  AND  LOSS) ");
            sbSQL.AppendLine(" and ");
            sbSQL.AppendLine(" (A.ifinishdate1 BETWEEN GET  AND  LOSS))");
            sbSQL.AppendLine(sbSyoNo.ToString());                                //証番号で結合
            sbSQL.AppendLine(" AND zokugara=''");                              //続柄なし＝本人
            sbSQL.AppendLine(" AND branch='0'");                                 //直近レコード
            
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereHon.ToString());                             //本人、処理年月、柔整レセ

            List<App> appListRange_NotMatch = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListRange_NotMatch)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_RangeNotMatch = true;
                });
            }

            wf.LogPrint("資格適用データに存在、資格喪失(本人)を抽出：" + appListRange_NotMatch.Count.ToString());



            /*
            wf.LogPrint("資格適用データに存在、診療年月が発効日より前を抽出");
            //本人　資格適用データに存在して 資格喪失
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");

            sbSQL.AppendLine(" SELECT SyoNo,get,loss FROM tekiyoudata_getloss ");
            sbSQL.AppendLine(" WHERE ");

            sbSQL.AppendLine(" to_timestamp(cast(A.ym || '01' as int)) >= issue ");
            sbSQL.AppendLine(sbSyoNo.ToString());                                //証番号で結合
            sbSQL.AppendLine(" AND zokugara=''");                              //続柄なし＝本人
            sbSQL.AppendLine(" AND branch='0'");                                 //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereHon.ToString());                             //本人、処理年月、柔整レセ

            List<App> appListSinryoYM_BeforeIssue = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListSinryoYM_BeforeIssue)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_SinryoBeforeIssue = true;
                });
            }

            wf.LogPrint("資格適用データに存在、診療年月が発効日より前を抽出(本人)：" + appListSinryoYM_BeforeIssue.Count.ToString());

            */
            #endregion

            
            #region 家族



            wf.LogPrint("資格適用データにいない家族を抽出");
            //App処理年月リスト
            //List<App> appList = App.GetApps(cym, InsurerID.SHARP_KENPO);

            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");
            sbSQL.AppendLine(" SELECT hihonum FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE 1=1 ");
            sbSQL.AppendLine(sbSyoNo.ToString());
            sbSQL.AppendLine(" AND zokugara<>''");      //本人でない
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereKaz.ToString());
            
            //資格適用データにいない人
            List<App> appListIka_NotMatch_kazoku = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListIka_NotMatch_kazoku)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_SikakuNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データにいない家族を抽出" + appListIka_NotMatch_kazoku.Count.ToString());



            wf.LogPrint("資格適用データに存在、生年月日違いの家族を抽出");
            //本人　資格適用データに存在して 生年月日違い
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" not EXISTS( ");
            sbSQL.AppendLine(" SELECT HIHONUM,birthadymd FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE A.pBirthday = birthadymd ");

            //20190709113700 furukawa st ////////////////////////
            //家族なので性別も判断材料とする            
            sbSQL.AppendLine(" and A.psex = cast(hgender as int) ");
            //20190709113700 furukawa ed ////////////////////////


            sbSQL.AppendLine(sbSyoNo.ToString());
            sbSQL.AppendLine(" AND zokugara<>''");      //本人でない
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereKaz.ToString());


            List<App> appListBirth_NotMatch_kazoku = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListBirth_NotMatch_kazoku)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_BirthNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データに存在、生年月日違いの家族を抽出" + appListBirth_NotMatch_kazoku.Count().ToString());


            wf.LogPrint("資格適用データに存在、性別違いの家族を抽出");
            //本人　資格適用データに存在して 性別違い
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" not EXISTS( ");
            sbSQL.AppendLine(" SELECT HIHONUM,hgender FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE A.psex = cast(hgender as int) ");

            //20190709113826 furukawa st ////////////////////////
            //家族なので生年月日も判断材料とする            
            sbSQL.AppendLine(" and A.pBirthday = birthadymd ");
            //20190709113826 furukawa ed ////////////////////////


            sbSQL.AppendLine(sbSyoNo.ToString());
            sbSQL.AppendLine(" AND zokugara<>''");      //本人でない
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereKaz.ToString());

            List<App> appListGender_NotMatch_kazoku = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListGender_NotMatch_kazoku)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_GenderNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データに存在、性別違いの家族を抽出" + appListGender_NotMatch_kazoku.Count().ToString());



            wf.LogPrint("資格適用データに存在、割合違いの家族を抽出");
            //本人　資格適用データに存在して 割合違い
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" not EXISTS( ");
            sbSQL.AppendLine(" SELECT HIHONUM,hgender FROM TEKIYOUDATA ");
            sbSQL.AppendLine(" WHERE 10- A.aratio = cast(ratio as int) and 10-a.aratio>0 ");
            sbSQL.AppendLine(sbSyoNo.ToString());
            sbSQL.AppendLine(" AND zokugara<>''");      //本人でない
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereKaz.ToString());


            List<App> appListratio_NotMatch_kazoku = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListratio_NotMatch_kazoku)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_RatioNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データに存在、割合違いの家族を抽出" + appListratio_NotMatch_kazoku.Count().ToString());


            wf.LogPrint("資格適用データに存在、資格喪失の家族を抽出");
            //本人　資格適用データに存在して 資格喪失
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");

            sbSQL.AppendLine(" SELECT Syono,get,loss FROM tekiyoudata_getloss ");
            sbSQL.AppendLine(" WHERE ");

            sbSQL.AppendLine(" ((A.ISTARTDATE1 BETWEEN GET  AND  LOSS) ");
            sbSQL.AppendLine(" and ");
            sbSQL.AppendLine(" (A.ifinishdate1 BETWEEN GET  AND  LOSS))");
            sbSQL.AppendLine(sbSyoNo.ToString());
            sbSQL.AppendLine(" AND zokugara<>''");      //本人でない
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereKaz.ToString());


            List<App> appListRange_NotMatch_kazoku = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListRange_NotMatch_kazoku)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_RangeNotMatch = true;
                });
            }
            wf.LogPrint("資格適用データに存在、資格喪失の家族を抽出" + appListRange_NotMatch_kazoku.Count().ToString());




            /*
            wf.LogPrint("資格適用データに存在、診療年月が発効日より前を抽出");
            //本人　資格適用データに存在して 資格喪失
            sbSQL.Remove(0, sbSQL.ToString().Length);
            sbSQL.AppendLine(" WHERE ");
            sbSQL.AppendLine(" NOT EXISTS( ");

            sbSQL.AppendLine(" SELECT SyoNo,get,loss FROM tekiyoudata_getloss ");
            sbSQL.AppendLine(" WHERE ");

            sbSQL.AppendLine(" to_timestamp(cast(A.ym || '01' as int)) >= issue ");
            sbSQL.AppendLine(sbSyoNo.ToString());                                //証番号で結合
            sbSQL.AppendLine(" AND zokugara<>''");      //本人でない
            sbSQL.AppendLine(" AND branch='0'");        //直近レコード
            sbSQL.AppendLine(")");
            sbSQL.AppendLine(sbWhereKaz.ToString());

            List<App> appListSinryoYM_BeforeIssue_kazoku = App.GetAppsWithWhere(sbSQL.ToString());

            foreach (App a in appListSinryoYM_BeforeIssue_kazoku)
            {
                appList.ForEach(al => {
                    if (al.Aid == a.Aid)
                        al.TaggedDatas.flgSikaku_SinryoBeforeIssue = true;
                });
            }

            wf.LogPrint("資格適用データに存在、診療年月が発効日より前を抽出(家族人)：" + appListSinryoYM_BeforeIssue_kazoku.Count.ToString());

            */

            //lstRes.AddRange(appListSikaku_NotMatch);
            //lstRes.AddRange(appListIka_NotMatch_kazoku);
            //lstRes.AddRange(appListBirth_NotMatch);
            //lstRes.AddRange(appListBirth_NotMatch_kazoku);
            //lstRes.AddRange(appListGender_NotMatch);
            //lstRes.AddRange(appListGender_NotMatch_kazoku);

            //lstRes.AddRange(appListratio_NotMatch);
            //lstRes.AddRange(appListratio_NotMatch_kazoku);

            //lstRes.AddRange(appListRange_NotMatch);
            //lstRes.AddRange(appListRange_NotMatch_kazoku);
            #endregion



            foreach (App a in appList)
            {
                               
                if ((a.TaggedDatas.flgSikaku_BirthNotMatch ||
                    a.TaggedDatas.flgSikaku_GenderNotMatch ||
                    a.TaggedDatas.flgSikaku_RangeNotMatch ||
                    a.TaggedDatas.flgSikaku_RatioNotMatch ||
                    a.TaggedDatas.flgSikaku_SikakuNotMatch ||
                    a.TaggedDatas.flgSikaku_SinryoBeforeIssue) && a.HihoNum != "")
                    lstRes.Add(a);
                
                    
            }

            

            return lstRes;

            //return appList;
        }

        public InputForm_Tekiyo(int _cym,ScanGroup sGroup=null, bool firstTime=false, int aid = 0)
        {
            InitializeComponent();
            cym = _cym;

            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hnumControls = new Control[] { verifyBoxHnum, verifyBoxHKigo, verifyBoxFamily, verifyBoxKyufuRate , };
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD,  };
            dayControls = new Control[] { verifyBoxF1Y, verifyBoxF1M, verifyBoxF1D, verifyBoxF1Start, verifyBoxF1Finish, verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, };
            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            //抽出リスト作成
            List < App > appList= new List<App>();
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                appList=CreateData(wf);                
            }
          
            /*
            int cnt =0;
            List<string> l=new List<string >();
            foreach(App a in appList)
            {
                 l.Add(a.Aid.ToString()+','+
                     a.TaggedDatas.flgSikaku_SikakuNotMatch.ToString()+ ',' +
                     a.TaggedDatas.flgSikaku_BirthNotMatch.ToString()+ ',' +
                     a.TaggedDatas.flgSikaku_GenderNotMatch.ToString() + ',' +
                     a.TaggedDatas.flgSikaku_RatioNotMatch.ToString() + ',' +
                     a.TaggedDatas.flgSikaku_RangeNotMatch.ToString() 
                     );
                    
            }

            
            foreach (string a in l)
            {

                System.Console.WriteLine(a);
            }
            */



            bsApp = new BindingSource();
            bsApp.DataSource = appList;
            dataGridViewPlist.DataSource = bsApp;



            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "資格点検";

            

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = appList.FindIndex(x => x.Aid == aid);
            
            //pgupで選択行が変わるのを防ぐ
            dataGridTekiyo.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;

      
           // bsTekiyo = new BindingSource();
           // bsTekiyo.DataSource = new List<TekiyouData>();
            //dataGridTekiyo.DataSource = bsTekiyo;
            //InitTekiyoGrid();

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);



            


        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
          
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            //else if (numberControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
        }


        /// <summary>
        /// 資格適用データグリッド初期化
        /// </summary>
        private void InitTekiyoGrid()
        {

            foreach (DataGridViewColumn c in dataGridTekiyo.Columns) c.Visible = false;

            dataGridTekiyo.Columns[nameof(TekiyouData.SyoNo)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.SyoNo)].HeaderText = "証番号";
            dataGridTekiyo.Columns[nameof(TekiyouData.SyoNo)].Width = 60;

            dataGridTekiyo.Columns[nameof(TekiyouData.Branch)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.Branch)].HeaderText = "枝番";
            dataGridTekiyo.Columns[nameof(TekiyouData.Branch)].Width = 30;

            dataGridTekiyo.Columns[nameof(TekiyouData.Branch2)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.Branch2)].HeaderText = "最終枝番";
            dataGridTekiyo.Columns[nameof(TekiyouData.Branch2)].Width = 30;

            dataGridTekiyo.Columns[nameof(TekiyouData.Zokugara)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.Zokugara)].HeaderText = "続柄";
            dataGridTekiyo.Columns[nameof(TekiyouData.Zokugara)].Width = 40;


            dataGridTekiyo.Columns[nameof(TekiyouData.hKana)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.hKana)].Width = 120;
            dataGridTekiyo.Columns[nameof(TekiyouData.hKana)].HeaderText = "カナ";

            dataGridTekiyo.Columns[nameof(TekiyouData.hName)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.hName)].Width = 120;
            dataGridTekiyo.Columns[nameof(TekiyouData.hName)].HeaderText = "氏名";

            dataGridTekiyo.Columns[nameof(TekiyouData.hGender)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.hGender)].Width =40;
            dataGridTekiyo.Columns[nameof(TekiyouData.hGender)].HeaderText = "性別";

            dataGridTekiyo.Columns[nameof(TekiyouData.Ratio)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.Ratio)].Width =40;
            dataGridTekiyo.Columns[nameof(TekiyouData.Ratio)].HeaderText = "割合";

            dataGridTekiyo.Columns[nameof(TekiyouData.BirthADYMD)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.BirthADYMD)].Width = 100;
            dataGridTekiyo.Columns[nameof(TekiyouData.BirthADYMD)].HeaderText = "生年月日";
                       
            dataGridTekiyo.Columns[nameof(TekiyouData.GetADYMD)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.GetADYMD)].Width = 100;
            dataGridTekiyo.Columns[nameof(TekiyouData.GetADYMD)].HeaderText = "取得日";

            dataGridTekiyo.Columns[nameof(TekiyouData.LossADYMD)].Visible = true;
            dataGridTekiyo.Columns[nameof(TekiyouData.LossADYMD)].Width = 100;
            dataGridTekiyo.Columns[nameof(TekiyouData.LossADYMD)].HeaderText = "喪失日";
            
            dataGridTekiyo.Columns[nameof(TekiyouData.issueDate)].Visible = false;
            dataGridTekiyo.Columns[nameof(TekiyouData.issueDate)].Width = 100;
            dataGridTekiyo.Columns[nameof(TekiyouData.issueDate)].HeaderText = "発効日";

        }


        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();        
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;


            #region エラーチェック

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //申請書種別
            //int apptype = verifyBoxAppType.GetIntValue();
            //setStatus(verifyBoxAppType, apptype != (int)VerifyBox.ERROR_CODE.NULL && apptype != 0 && apptype != 7);



            //20190208173706 furukawa st ////////////////////////
            //レセプト管理番号の作成


            //0071一般本人
            //0072一般家族
            //0171特退本人
            //0172特退家族
            

            string strKanriBango1 = verifyBoxKanriBango1.Text.Trim();
            setStatus(verifyBoxKanriBango1, 
                verifyBoxKanriBango1.Text == "0071" ||
                verifyBoxKanriBango1.Text == "0072" ||
                verifyBoxKanriBango1.Text == "0171" ||
                verifyBoxKanriBango1.Text == "0172" );

            string strKanriBango2 = verifyBoxKanriBango2.Text.Trim().PadLeft(5,'0');
            setStatus(verifyBoxKanriBango2, int.Parse(strKanriBango2) <1 || int.Parse(strKanriBango2) > 99999);

            strRezeKanriBango = strKanriBango1 + strKanriBango2;
            //20190208173706 furukawa ed ////////////////////////


            //都道府県番号 医療機関コードからとる？→そのまま入力
            int pref = verifyBoxPref.GetIntValue();            
            setStatus(verifyBoxPref, (pref < 1 || pref > 47) && pref != 99);


            //拡張入力で入力するため不要

            //医療機関コード (数字のみ、最大4桁)
            //int hosNumber = verifyBoxHosNumber.GetIntValue();
            //setStatus(verifyBoxHosNumber, hosNumber < 1 || 9999 < hosNumber);
            //医療機関コードは柔整師コードを入力 仕様書より　 テキストで取らないと前ゼロ落ちる
            //string strHosNumber = verifyBoxClinicNum.Text.Substring(3,5);
            //setStatus(verifyBoxClinicNum, int.Parse(strHosNumber) < 1 || 99999 < int.Parse(strHosNumber));


            //被保険者番号のチェック
            /*int numM = verifyBoxHKigo.GetIntValue();
            setStatus(verifyBoxHKigo, numM < 1 || 999 < numM);
            int num = verifyBoxHnum.GetIntValue();
            setStatus(verifyBoxHnum, num < 1 || 99999999 < num);
            var hnum = verifyBoxHKigo.Text.Trim() + "-" + verifyBoxHnum.Text.Trim();
            */

            //記号番号合わせて40バイト
            bool KigoNumErr = true;
            string strKigo = verifyBoxHKigo.Text;
            string strNum = verifyBoxHnum.Text;
            string strHnum = strKigo + '-' + strNum;
            int KigoNumLength = strKigo.GetByteLength() + strNum.GetByteLength();

            if (KigoNumLength > 40) KigoNumErr = false;

            setStatus(verifyBoxHKigo, strKigo.GetByteLength() > KigoNumLength);
            setStatus(verifyBoxHnum, strNum.GetByteLength() > KigoNumLength);


            //20190325173538 furukawa st ////////////////////////
            //給付割合エラーチェック

            //給付割合 7～10
            int rate = 0;
            int.TryParse(verifyBoxKyufuRate.Text, out rate);
            setStatus(verifyBoxKyufuRate, rate <= 6 || rate>=11);
            //20190325173538 furukawa ed ////////////////////////






            //2019/02/19更新****************************************************************************************************************************


            //受診者区分
            //0：高七　→　2：高齢者（一定以上）
            //2：本人
            //4：六歳　→　5：未就学
            //6：家族
            //8：高一　→　1：高齢者（一般）


            int jusinsyaKubun = verifyBoxJuryoType.GetIntValue();
            setStatus(verifyBoxJuryoType, !new[] { 0, 2, 4, 6, 8 }.Contains(jusinsyaKubun));
            //本家区分


            //2：本人　→　1:本人
            //6：家族　→　2:家族
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !new[] { 2, 6 }.Contains(family));




            //20190325175838 furukawa st ////////////////////////
            //管理番号と本人家族整合性チェック


            //0071一般本人
            //0072一般家族
            //0171特退本人
            //0172特退家族

            switch (verifyBoxKanriBango1.Text.Substring(3, 1)) {
                case "1":
                    if(family != 2) setStatus(verifyBoxFamily, true);
                    break;
                case "2":
                    if (family != 6) setStatus(verifyBoxFamily, true);
                    break;
                default:
                    setStatus(verifyBoxFamily, true);
                    break;
            }

            //20190325175838 furukawa ed ////////////////////////            



            /*
            //本家区分
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !(new int[] { 2, 4, 6, 8 }).Contains(family));
            */



            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //新規継続
            //int newCont = verifyBoxNewCont.GetIntValue();
            //setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);



            //初検年月
            int sY = verifyBoxF1Y.GetIntValue();
            int sM = verifyBoxF1M.GetIntValue();

            int sD = verifyBoxF1D.GetIntValue(); //20190322120331 furukawa 初検日の日を追加


            var shoken = DateTime.MinValue;
            if (string.IsNullOrWhiteSpace(scanGroup.note2))
            {
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                sY = DateTimeEx.GetAdYearFromHs(sY * 100 + sM);
                //sY = DateTimeEx.GetAdYearFromHs(sY);
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                //20190322120500 furukawa st ////////////////////////
                //初検日の日の値を取得して日付を作成

                if (!DateTimeEx.IsDate(sY, sM, sD))
                {
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                    setStatus(verifyBoxF1D, true);
                }
                else
                {
                    setStatus(verifyBoxF1Y, false);
                    setStatus(verifyBoxF1M, false);
                    setStatus(verifyBoxF1D, false);
                    shoken = new DateTime(sY, sM, sD);
                }



                /*
                if (!DateTimeEx.IsDate(sY, sM, 1))
                {
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                }
                else
                {
                    setStatus(verifyBoxF1Y, false);
                    setStatus(verifyBoxF1M, false);
                    shoken = new DateTime(sY, sM, 1);
                }
                */



                //20190322120500 furukawa ed ////////////////////////

            }



            //合計
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);


            //受診者区分
            //0：高七　→　2：高齢者（一定以上）
            //2：本人
            //4：六歳　→　5：未就学
            //6：家族
            //8：高一　→　1：高齢者（一般）

            
            //20190322175116 furukawa st ////////////////////////
            //合計・請求金額・本家区分チェック


            //請求
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;

            /*
            if ((family == 2 || family == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (family == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (family == 4 && (int)(total * 80 / 100) != charge)
                payError = true;
                */



            //本家区分	給付割合
            //2:本人		7
            //4:六歳		8
            //6:家族		7
            //8:高一		8
            //0:高7			7
            //受診者区分
            switch (jusinsyaKubun)
            {
                case 2:
                case 6:
                case 0:
                    if (rate != 7) payError = true;
                    break;

                case 4:
                case 8:
                    if (rate != 8) payError = true;
                    break;


                default:
                    break;
            }

            //20190322175116 furukawa ed ////////////////////////




            //20190325173636 furukawa st ////////////////////////
            //請求金額と合計金額×給付割合の確認

            int ans = 0;
            ans = (int)(total * (rate*0.1));
            if (ans != charge) payError = true;

            //20190325173636 furukawa ed ////////////////////////







            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);




            //20191105165148 furukawa st ////////////////////////
            //開始・終了日は診療年月から取得した方が正確
            
            var startDate = dateCheck(verifyBoxY, verifyBoxM, verifyBoxF1Start.GetIntValue());
            var finishDate = dateCheck(verifyBoxY, verifyBoxM, verifyBoxF1Finish.GetIntValue());

                        //20190304135648 furukawa st ////////////////////////
                        //施術開始日、終了日の追加
                        //開始日
                        //var startDate = dateCheck(4, year, month, verifyBoxF1Start);
                        //終了日
                        //var finishDate = dateCheck(4, year, month, verifyBoxF1Finish);
                        //20190304135648 furukawa ed ////////////////////////

            //20191105165148 furukawa ed ////////////////////////

            /*
            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);
            */


            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
            


            //20190322175309 furukawa st ////////////////////////
            //金額チェック機能


            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxFamily.BackColor = Color.GreenYellow;
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                verifyBoxKyufuRate.BackColor = Color.GreenYellow;//20190326133029 furukawa エラーチェック箇所に給付割合追加
                verifyBoxJuryoType.BackColor = Color.GreenYellow;//20190326133723 furukawa エラーチェック箇所に受診者区分追加


                var res = MessageBox.Show("本家区分・給付割合・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }
            //20190322175309 furukawa ed ////////////////////////



            //ナンバリング抜けチェック
            int preNum = 0;
            int index = bsApp.IndexOf(app);
            var l = (List<App>)bsApp.DataSource;
            for (int i = l.Count - 1; i >= 0; i--)
            {
                if (l[i].MediYear > 0)
                {
                    if (!int.TryParse(l[i].Numbering, out preNum)) continue;
                    break;
                }
            }


            #endregion

            #region appに入力値を反映


            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            
            //app.HihoNum = hnum;
            app.HihoNum = strHnum;//被保番
            
            
            //拡張入力で入力するので不要
            //どっちでいく？→ClinicNumじゃないかも
            //app.ClinicNum = strHosNumber;//医療機関コード＝柔整師コード仕様書より   
            //app.ClinicNum = verifyBoxClinicNum.Text.Trim(); //医療機関コード＝柔整師コード仕様書より   

            //拡張入力で選択
            app.InsNum = iNumber;//保険者番号


            app.HihoPref = pref;//都道府県 医療機関コード＝柔整師コードの都道府県


            //20190325180750 furukawa st ////////////////////////
            //給付割合、請求金額登録
            app.Ratio = rate;
            app.Charge = charge;
            //20190325180750 furukawa ed ////////////////////////


            //2019/02/19更新****************************************************************************************************************************
            //200 202 204 206 208 600 602 604 606 608

            //受診者区分
            //0：高七　→　2：高齢者（一定以上）
            //2：本人
            //4：六歳　→　5：未就学
            //6：家族
            //8：高一　→　1：高齢者（一般）

            //本家区分
            //2：本人　→　1:本人
            //6：家族　→　2:家族


            //本家区分を前にしないと、受診者区分は０の場合があり３桁にならないのでわかりにくい
            app.Family = family * 100 + jusinsyaKubun;   //受診者区分と本人家族をまとめて記録
            //app.Family = jusinsyaKubun * 100 + family;   //受診者区分と本人家族をまとめて記/録
            
            //app.Family = family;//本人家族






            app.Sex = sex;//性別

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.NewContType = DateTimeEx.GetAdYearFromHs(year * 100 + month) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //app.NewContType = DateTimeEx.GetAdYearFromHs(year) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            app.FushoFirstDate1 = shoken;//初検

            app.Birthday = birthDt;
            app.Total = total;

            //htypeフィールドに一般、とくたいの区別をもたせる
            //保険者番号 一般は　06271779　特退は　63271779 北村さんより
            app.HihoType = int.Parse(iNumber.ToString().Substring(0, 2));

            //レセプト管理番号
            app.ComNum = strRezeKanriBango;

            //app.Charge = charge;            
            app.CountedDays = days;

            //医療機関コード＝柔整師コードの３～８桁目仕様書より
            //app.DrNum = verifyBoxHosNumber.Text.Trim();
            //app.DrNum = strHosNumber;
            //app.DrNum = verifyBoxClinicNum.Text.Trim() ;

            //app.AppType = apptype == 7 ? APP_TYPE.鍼灸 : APP_TYPE.柔整;


            //20190304135745 furukawa st ////////////////////////
            //施術開始日、終了日の追加            
            app.FushoStartDate1 = startDate;
            app.FushoFinishDate1 = finishDate;
            //20190304135745 furukawa ed ////////////////////////


            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();



            #endregion

            return true;

        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 資格適用データ相違箇所リセット
        /// </summary>
        private void ResetSign()
        {
            lblBirth.ForeColor = Color.Gray;
            lblRatio.ForeColor = Color.Gray;
            lblRange.ForeColor = Color.Gray;
            lblNone.ForeColor = Color.Gray;
            lblGender.ForeColor = Color.Gray;
        }

        /// <summary>
        /// 資格適用データ相違箇所表示
        /// </summary>
        /// <param name="a"></param>
        private void SetSign(App a)
        {
            if (a.TaggedDatas.flgSikaku_BirthNotMatch) lblBirth.ForeColor = Color.Red;
            if (a.TaggedDatas.flgSikaku_GenderNotMatch) lblGender.ForeColor = Color.Red;
            if (a.TaggedDatas.flgSikaku_RangeNotMatch) lblRange.ForeColor = Color.Red;
            if (a.TaggedDatas.flgSikaku_RatioNotMatch) lblRatio.ForeColor = Color.Red;
            if (a.TaggedDatas.flgSikaku_SikakuNotMatch) lblNone.ForeColor = Color.Red;

        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {


            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);


            //InitTekiyoGrid();

            if(app!=null)setValuesTekiyo(app);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //if (!GetiNumber_family()) return;
            

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);

            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }

           
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

        

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {

                



                //被保険者番号
                var hn = app.HihoNum.Split('-');
                if (hn.Length == 2)
                {
                    setValue(verifyBoxHKigo, hn[0], firstTime, nv);
                    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxHKigo, string.Empty, firstTime, nv);
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                }

                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);


                //都道府県番号
                setValue(verifyBoxPref, app.HihoPref.ToString(), firstTime, nv);


                //管理番号頭４桁を管理番号頭４桁を変更不可に                
                if (app.ComNum != string.Empty) setValue(verifyBoxKanriBango1, app.ComNum.Substring(0, 4).ToString(), false, false);
                if (app.ComNum != string.Empty) setValue(verifyBoxKanriBango2, app.ComNum.Substring(4, 5).ToString(), firstTime, nv);

                //受診者区分と本家区分コードをわけてそれぞれの入力欄にいれる
                if (app.Family.ToString().Length == 3) setValue(verifyBoxJuryoType, app.Family.ToString().Substring(2, 1), firstTime, nv);
                setValue(verifyBoxFamily, app.Family.ToString().Substring(0, 1), firstTime, nv);

                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);

                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
                setValue(verifyBoxTotal, app.Total, firstTime, nv);

                setValue(verifyBoxF1Start, app.FushoStartDate1.Day, firstTime, nv);
                setValue(verifyBoxF1Finish, app.FushoFinishDate1.Day, firstTime, nv);

                //請求金額
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);


                //給付割合
                setValue(verifyBoxKyufuRate, app.Ratio, firstTime, nv);


                //医療機関コード＝柔整師コード

                //負傷名
                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);

                //初検日
                if (app.FushoFirstDate1.IsNullDate())
                {
                    setValue(verifyBoxF1Y, string.Empty, firstTime, false);
                    setValue(verifyBoxF1M, string.Empty, firstTime, false);
                    setValue(verifyBoxF1D, string.Empty, firstTime, false); 

                }
                else
                {
                    setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, false);
                    setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, false);
                    setValue(verifyBoxF1D, app.FushoFirstDate1.Day.ToString(), firstTime, false);
                }
            }
        }

        private void setValuesTekiyo(App app)
        {
            DateTime dtbirth = app.Birthday;
            List<TekiyouData> l = new List<TekiyouData>();

            ResetSign();

            if (app.InputStatus == StatusFlag.未処理)
            {
                pnlTekiyo.Visible = false;
                return;
            }

            else
            {
                pnlTekiyo.Visible = true;
                SetSign(app);
            }
                

            if (app.HihoNum != string.Empty)
            {
                //続柄もあるが、
                //Applicationでは2が本人 適用データでは0が本人（実際は空欄）それ以外は入っているが種類が多すぎるのでとらない                

                l = TekiyouData.SerchForMatchingAll(app.HihoNum.Split('-')[1].ToString(), 
                    dtbirth, app.Sex.ToString(),10-app.Ratio);
                if (l.Count != 0)
                {                    
                    lblJoken.Text = "証番号、生年月日、性別、割合で抽出";
                }
                else
                {                                       
                    l = TekiyouData.SerchForMatchingSyoNo(app.HihoNum.Split('-')[1].ToString());
                    lblJoken.Text = "証番号で抽出";                                       
                }
            }
            else
            {

                l = TekiyouData.SerchForMatchingBGR(dtbirth, app.Sex.ToString(), app.Ratio);
                lblJoken.Text = "生年月日、性別で抽出、割合";
            }

            l.Sort((x,y) => y.GetADYMD.CompareTo(x.GetADYMD));
            l.Sort((x, y) => y.Zokugara.CompareTo(x.Zokugara));
                        
            bsTekiyo.DataSource = l;


            if (l.Count > 0)
            {
                dataGridTekiyo.DataSource = bsTekiyo;
                InitTekiyoGrid();                
            }

        }

        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                panelHnum.Visible = true;
                panelTotal.Visible = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void verifyBoxJuryoType_TextChanged(object sender, EventArgs e)
        {
            //受診者区分が2.本人の場合は本家区分も2.本人とする
            if (verifyBoxJuryoType.Text.ToString() == "2") {
                verifyBoxFamily.Text = "2";
            }
            //受診者区分が6.家族の場合は本家区分も6.家族とする
            else if (verifyBoxJuryoType.Text.ToString() == "6")
            {
                verifyBoxFamily.Text = "6";
            }

        }


        //管理番号の下５桁を０埋め
        private void verifyBoxKanriBango2_Validated(object sender, EventArgs e)
        {          
            verifyBoxKanriBango2.Text = verifyBoxKanriBango2.Text.PadLeft(5, '0');
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;

        }

        private void verifyBoxAppType_TextChanged(object sender, EventArgs e)
        {

            //申請区分がないのでずっと使える
            verifyBoxF1.Enabled = true;
            verifyBoxF2.Enabled = true;
            verifyBoxF3.Enabled = true;
            verifyBoxF4.Enabled = true;
            verifyBoxF5.Enabled = true;

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        //フォルダ名より保険者番号取得
        private bool GetiNumber_family()
        {
            
            if (scan.Note1.IndexOf("071") > 0)
            {
                verifyBoxKanriBango1.Text = "0071";
                iNumber = "06271779";                
                return true;
            }
            if (scan.Note1.IndexOf("072") > 0)
            {
                verifyBoxKanriBango1.Text = "0072";
                iNumber = "06271779";               
                return true;
            }
            if (scan.Note1.IndexOf("171") > 0)
            {
                verifyBoxKanriBango1.Text = "0171";
                iNumber = "63271779";                
                return true;
            }
            if (scan.Note1.IndexOf("172") > 0)
            {
                verifyBoxKanriBango1.Text = "0172";
                iNumber = "63271779";
                return true;
            }

            MessageBox.Show("Note1に 0071,0072,0171,0172 が含まれていません"
                ,Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            return false;

        }


    }
}
