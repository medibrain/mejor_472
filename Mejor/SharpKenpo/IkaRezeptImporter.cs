﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using Mejor.Pay;


//20190429180039 furukawa st ////////////////////////
//医科レセデータインポートクラス

namespace Mejor.SharpKenpo
{
    //医科レセデータ用テーブル
    class IkaRezeptData
    {
  


        public string F003 { get; set; } = string.Empty;            //検索番号	MN10番目
        public string F004 { get; set; } = string.Empty;            //氏名漢字	SA22
        public string F005 { get; set; } = string.Empty;            //生年月日	SA24
        public string F006 { get; set; } = string.Empty;            //性別	SA23
        public string F007 { get; set; } = string.Empty;            //被保険者証記号	SA9
        public string F008 { get; set; } = string.Empty;            //被保険者証番号	SA10
        public string F009 { get; set; } = string.Empty;            //診療年月	MN4番目
        public string F010 { get; set; } = string.Empty;            //医療機関番号	MN12
        public string F011 { get; set; } = string.Empty;            //医療機関名	MN14

        public int F012 { get; set; } = 0;                          //診療年月西暦で持たせておく
        public DateTime  F013 { get; set; } = DateTime.MinValue;    //生年月日西暦の生年月日
        public string F014 { get; set; } = string.Empty;            //記号番号-連結

        [DB.DbAttribute.PrimaryKey]
        public int F001 { get; set; } = 0;//明細行数
        public int F002 { get; set; } = 0;//人件数

    }

    class IkaRezeptImporter
    {

        public static bool Import()
        {
            List<string> fileNames = new List<string>();
       
            using (var f = new OpenFileDialog())
            {
                f.Multiselect = true;
                f.Title = "シャープ健保 医科レセ取り込み";
                f.Filter = "PECULIARTEXTINFOファイル|01_PECULIARTEXTINFO_MED.csv";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileNames.AddRange(f.FileNames);
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                //追記前提だが、何件取り込んだかを取得し、IDに足す

                wf.LogPrint("すでに登録された情報を取得しています");
                
                //   var pl = DB.Main.SelectAll<IkaRezeptImporter>();
                //  var dic = new Dictionary<string, IkaRezeptImporter>();

                var pl = DB.Main.SelectAll<IkaRezeptData>();
                var dic = new Dictionary<string, IkaRezeptData>();
                string strkey=string.Empty ;

                //20190919160231 furukawa st ////////////////////////
                //行番号最大を取得
                                
                var v = DB.Main.Query<int>("select f001 from ikarezeptdata where f001=(select max(f001) from ikarezeptdata);");                
                int maxcnt = v.ToList<int>()[0];
                //20190919160231 furukawa ed ////////////////////////

                // foreach (var item in pl) dic.Add(item.InsdNum, item);
                foreach (var item in pl)
                {
                    strkey = item.F003;
                    //同じ検索番号は飛ばす
                    if (dic.ContainsKey(strkey))
                    {
                        //dic.Remove(strkey);
                        continue;
                    }
                    dic.Add(strkey, item);
                }

                wf.LogPrint($"{pl.Count()}件の登録済み情報を取得しました");

                var l = new List<IkaRezeptData>();//CSVからロードしたリスト

                for (int cnt = 0; cnt < fileNames.Count; cnt++)
                {

                    #region CSVロード

                    wf.LogPrint("CSVを読み込んでいます");

                    var csv = CommonTool.CsvImportMultiCode(fileNames[cnt]);
                    var p = new IkaRezeptData();
                    int pcnt = 1;//人カウンタ
                   

                    for (int r = 0; r < csv.Count; r++)
                    {
                        if (wf.Cancel)
                        {
                            var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Asterisk);
                            if (cres == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }
                        var item = csv[r];

                        if (item.Length < 5) continue;
                    
                        switch (item[1].Trim())
                        {
                            //peculiartextinfoのMNとSAのみ取得する
                            case "MN":


                                //20190517105025 furukawa st ////////////////////////
                                //一人1行なので行番号だけでいい


                                //20190919160714 furukawa st ////////////////////////
                                //行数でなく、最大を足す
                                
                                p.F001 = pcnt + maxcnt;//行連番
                                //p.F001 = pcnt + dic.Count;//行連番
                                //20190919160714 furukawa ed ////////////////////////


                                p.F002 = 0;//人連番

                                //p.F001 = r + dic.Count;//行連番
                                //p.F002 = pcnt + dic.Count;//人連番
                                //20190517105025 furukawa ed ////////////////////////

                                p.F003 = item[10-1].Trim();   //検索番号	MN10番目
                                p.F009 = item[4-1].Trim();    //診療年月	MN4番目
                                p.F010 = item[12-1].Trim();   //医療機関番号	MN12
                                p.F011 = item[14-1].Trim();   //医療機関名	MN14


                                //20200626182203 furukawa st ////////////////////////
                                //診療年月が和暦から西暦に変わったことで、西暦への変換ミスが起きた

                                p.F012 = int.Parse(p.F009);

                                            //DateTime tmp = DateTimeEx.GetDateFromJstr7(p.F009.ToString().PadLeft(5, '0') + "01");
                                            //p.F012 = int.Parse(tmp.Year.ToString() + tmp.Month.ToString().PadLeft(2, '0'));

                                //20200626182203 furukawa ed ////////////////////////

                                break;

                            case "SA":
                                p.F004 = item[22-1].Trim();                 // SA22氏名漢字
                                p.F005 = item[24-1].Trim();                  // SA24生年月日
                                p.F006 = item[23-1].Trim();                  // SA23性別
                                p.F007 = Microsoft.VisualBasic.Strings.StrConv(item[9-1].Trim(),Microsoft.VisualBasic.VbStrConv.Narrow);// SA9 被保険者証記号
                                p.F008 = Microsoft.VisualBasic.Strings.StrConv(item[10-1].Trim(), Microsoft.VisualBasic.VbStrConv.Narrow);// SA10被保険者証番号

                                //20200626182609 furukawa st ////////////////////////
                                //診療年月が和暦から西暦に変わったことで、西暦への変換ミスが起きた
                                
                                p.F013 = DateTime.Parse($"{p.F005.Substring(0,4)}-{p.F005.Substring(4, 2)}-{p.F005.Substring(6, 2)}");
                                //p.F013 = DateTimeEx.GetDateFromJstr7(p.F005.ToString().PadLeft(0, '7'));
                                //20200626182609 furukawa ed ////////////////////////


                                p.F014 = p.F007 + '-' + p.F008;//記号-番号
                                pcnt++;
                                l.Add(p);
                                p = new IkaRezeptData();
                                break;
                            default:
                                continue;
                        }

                    }
                    #endregion

                }

                #region 登録
                wf.LogPrint($"CSVより{l.Count}件の情報を取得しました");

                wf.LogPrint("データベースへ登録を行ないます");
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                int updateCount = 0;
                int insertCount = 0;

                using (var tran = DB.Main.CreateTransaction())
                {
                    //先に全削除
                    //DB.Main.Excute("truncate table IkaRezeptData", tran);

                    foreach (var item in l)
                    {
                        if (wf.Cancel)
                        {
                            var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Asterisk);
                            if (cres == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }



                        wf.InvokeValue++;

                        // 医科レセは追記のみだという想定の下、重複時の更新を外す
                        //        if (dic.ContainsKey(item.F003))
                        //       {
                        //被保番と検索番号が同じレコードはとばす
                        //if (dic[strkey].F003 == item.F003) {


                        //20190919161025 furukawa st ////////////////////////
                        //検索番号
                        strkey = item.F003;

                        //既にある検索番号の記号番号が同じレコードは更新とする
                        if (dic.ContainsKey(strkey))
                        {
                            if (dic[strkey].F007 == item.F007 && dic[strkey].F008 == item.F008)
                            {
                                if (!DB.Main.Update(item, tran))
                                {
                                    MessageBox.Show("情報のインポートに失敗しました");
                                    return false;
                                }
                                updateCount++;
                            }
                        }
                        else
                        {
                            if (!DB.Main.Insert(item, tran))
                            {
                                MessageBox.Show("情報のインポートに失敗しました");
                                return false;
                            }
                            insertCount++;

                        }



                    }
                    tran.Commit();
                }


                MessageBox.Show(
                    $"新規登録：{insertCount}件\r\n" +
                    $"更新：{updateCount}件\r\n\r\n" +
                    $"情報のインポートが完了しました");
                return true;

                #endregion

            }
        }
    }
}
