﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.SharpKenpo
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //シャープ健保用 保険者番号
        string iNumber = "";

        //本家区分
        int family = 0;

        /// <summary>
        /// レセプト管理番号
        /// </summary>
        string strRezeKanriBango = string.Empty;

        /// <summary>
        /// 検索番号
        /// </summary>
        string strKensakuBango = string.Empty;

        /// <summary>
        /// 一連番号
        /// </summary>
        int strItirenBango = 900000;



        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHnum = new Point(400, 0);
        Point posPerson = new Point(0, 0);
        Point posFusho = new Point(0, 750);
        Point posCost = new Point(400, 2000);
        Point posDays = new Point(400, 750);
        Point posNumbering = new Point(300, 0);
        Point posBatch = new Point(200, 600);
        Point posBatchCount = new Point(600, 200);
        Point posBatchDrCode = new Point(200, 900);

        Control[] ymControls, hnumControls, personControls, dayControls, costControls, fushoControls;
        //Control[] ymControls, hnumControls, personControls, dayControls, costControls;

        //        Control[] ymControls, hnumControls, personControls, dayControls, costControls,
        //          fushoControls, numberControls;


        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();
            
            ymControls = new Control[] { verifyBoxY, verifyBoxM };

            //20190322120041 furukawa st ////////////////////////
            //給付割合追加                    番号　記号　本人家族　給付割合
            hnumControls = new Control[] { verifyBoxHnum, verifyBoxHKigo, verifyBoxFamily, verifyBoxKyufuRate , };
            //hnumControls = new Control[] { verifyBoxHnum, verifyBoxHKigo, verifyBoxFamily, };
            //20190322120041 furukawa ed ////////////////////////


            //                                   性別　生年月日年号、年 月 日　
            personControls = new Control[] { verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD,  };

            //                                 初検 年 月 日    開始日　終了日 診療日数
            dayControls = new Control[] { verifyBoxF1Y, verifyBoxF1M, verifyBoxF1D, verifyBoxF1Start, verifyBoxF1Finish, verifyBoxDays, };


            //20190322120115 furukawa st ////////////////////////
            //合計金額、請求金額に変更

            //costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, verifyBoxClinicNum,};
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge, };
            //20190322120115 furukawa ed ////////////////////////




            //20190322115649 furukawa st ////////////////////////
            //初検日を移動

            fushoControls = new Control[] { verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            /*fushoControls = new Control[] { verifyBoxF1Y, verifyBoxF1M,
                verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
                */
            //20190322115649 furukawa ed ////////////////////////


            //numberControls = new Control[] { verifyBoxHosNumber, };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";
            
            //北村さん指示により、鍼灸時、負傷名関係入力不要
            var setEnable = new Action<TextBox, bool>((t, b) =>
                {
                    t.Enabled = b;
                    t.BackColor = b ? SystemColors.Info : SystemColors.Control;
                });
            if(scan.AppType != APP_TYPE.柔整)
            {
                setEnable(verifyBoxF1, false);
                setEnable(verifyBoxF2, false);
                setEnable(verifyBoxF3, false);
                setEnable(verifyBoxF4, false);
                setEnable(verifyBoxF5, false);
            }

            


            //panelHnum.Visible = false;
            //panelTotal.Visible = false;

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            //else if (numberControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();



            //20190328131329 furukawa st ////////////////////////
            //PageDownで一つ前のレセに戻す

            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            //else if (e.KeyCode == Keys.PageUp) buttonBack.PerformClick();
            //20190328131329 furukawa ed ////////////////////////

        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;


            #region エラーチェック

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //申請書種別
            //int apptype = verifyBoxAppType.GetIntValue();
            //setStatus(verifyBoxAppType, apptype != (int)VerifyBox.ERROR_CODE.NULL && apptype != 0 && apptype != 7);



            //20190208173706 furukawa st ////////////////////////
            //レセプト管理番号の作成


            //0071一般本人
            //0072一般家族
            //0171特退本人
            //0172特退家族
            

            string strKanriBango1 = verifyBoxKanriBango1.Text.Trim();

            //20190513153133 furukawa st ////////////////////////
            //エラーチェック修正

            setStatus(verifyBoxKanriBango1, 
                verifyBoxKanriBango1.Text != "0071" &&
                verifyBoxKanriBango1.Text != "0072" &&
                verifyBoxKanriBango1.Text != "0171" &&
                verifyBoxKanriBango1.Text != "0172" );

            //setStatus(verifyBoxKanriBango1,
            //    verifyBoxKanriBango1.Text == "0071" ||
            //    verifyBoxKanriBango1.Text == "0072" ||
            //    verifyBoxKanriBango1.Text == "0171" ||
            //    verifyBoxKanriBango1.Text == "0172");
            
            //20190513153133 furukawa ed ////////////////////////

            string strKanriBango2 = verifyBoxKanriBango2.Text.Trim().PadLeft(5,'0');
            setStatus(verifyBoxKanriBango2, int.Parse(strKanriBango2) <1 || int.Parse(strKanriBango2) > 99999);

            strRezeKanriBango = strKanriBango1 + strKanriBango2;
            //20190208173706 furukawa ed ////////////////////////


            //20190612171008 furukawa st ////////////////////////
            //2019-06-12仕様変更　不要
            
                        //都道府県番号 医療機関コードからとる？→そのまま入力
                        //int pref = verifyBoxPref.GetIntValue();            
                        //setStatus(verifyBoxPref, (pref < 1 || pref > 47) && pref != 99);
            //20190612171008 furukawa ed ////////////////////////


            //拡張入力で入力するため不要

            //医療機関コード (数字のみ、最大4桁)
            //int hosNumber = verifyBoxHosNumber.GetIntValue();
            //setStatus(verifyBoxHosNumber, hosNumber < 1 || 9999 < hosNumber);
            //医療機関コードは柔整師コードを入力 仕様書より　 テキストで取らないと前ゼロ落ちる
            //string strHosNumber = verifyBoxClinicNum.Text.Substring(3,5);
            //setStatus(verifyBoxClinicNum, int.Parse(strHosNumber) < 1 || 99999 < int.Parse(strHosNumber));


            //被保険者番号のチェック
            /*int numM = verifyBoxHKigo.GetIntValue();
            setStatus(verifyBoxHKigo, numM < 1 || 999 < numM);
            int num = verifyBoxHnum.GetIntValue();
            setStatus(verifyBoxHnum, num < 1 || 99999999 < num);
            var hnum = verifyBoxHKigo.Text.Trim() + "-" + verifyBoxHnum.Text.Trim();
            */

            //記号番号合わせて40バイト
            bool KigoNumErr = true;
            string strKigo = verifyBoxHKigo.Text;
            string strNum = verifyBoxHnum.Text;
            string strHnum = strKigo + '-' + strNum;
            int KigoNumLength = strKigo.GetByteLength() + strNum.GetByteLength();

            if (KigoNumLength > 40) KigoNumErr = false;

            setStatus(verifyBoxHKigo, strKigo.GetByteLength() > KigoNumLength);
            setStatus(verifyBoxHnum, strNum.GetByteLength() > KigoNumLength);


            //20190325173538 furukawa st ////////////////////////
            //給付割合エラーチェック

            //給付割合 7～10
            int rate = 0;
            int.TryParse(verifyBoxKyufuRate.Text, out rate);
            setStatus(verifyBoxKyufuRate, rate <= 6 || rate>=11);
            //20190325173538 furukawa ed ////////////////////////






            //2019/02/19更新****************************************************************************************************************************


            //受診者区分
            //0：高七　→　2：高齢者（一定以上）
            //2：本人
            //4：六歳　→　5：未就学
            //6：家族
            //8：高一　→　1：高齢者（一般）


            int jusinsyaKubun = verifyBoxJuryoType.GetIntValue();
            setStatus(verifyBoxJuryoType, !new[] { 0, 2, 4, 6, 8 }.Contains(jusinsyaKubun));
            //本家区分


            //2：本人　→　1:本人
            //6：家族　→　2:家族
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !new[] { 2, 6 }.Contains(family));




            //20190325175838 furukawa st ////////////////////////
            //管理番号と本人家族整合性チェック


            //0071一般本人
            //0072一般家族
            //0171特退本人
            //0172特退家族

            switch (verifyBoxKanriBango1.Text.Substring(3, 1)) {
                case "1":
                    if(family != 2) setStatus(verifyBoxFamily, true);
                    break;
                case "2":
                    if (family != 6) setStatus(verifyBoxFamily, true);
                    break;
                default:
                    setStatus(verifyBoxFamily, true);
                    break;
            }

            //20190325175838 furukawa ed ////////////////////////            



            /*
            //本家区分
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !(new int[] { 2, 4, 6, 8 }).Contains(family));
            */



            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //新規継続
            //int newCont = verifyBoxNewCont.GetIntValue();
            //setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);



            //初検年月
            int sY = verifyBoxF1Y.GetIntValue();
            int sM = verifyBoxF1M.GetIntValue();

            int sD = verifyBoxF1D.GetIntValue(); //20190322120331 furukawa 初検日の日を追加


            var shoken = DateTime.MinValue;
            if (string.IsNullOrWhiteSpace(scanGroup.note2))
            {
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                sY = DateTimeEx.GetAdYearFromHs(sY * 100 + sM);
                //sY = DateTimeEx.GetAdYearFromHs(sY);
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                //20190322120500 furukawa st ////////////////////////
                //初検日の日の値を取得して日付を作成

                if (!DateTimeEx.IsDate(sY, sM, sD))
                {
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                    setStatus(verifyBoxF1D, true);
                }
                else
                {
                    setStatus(verifyBoxF1Y, false);
                    setStatus(verifyBoxF1M, false);
                    setStatus(verifyBoxF1D, false);
                    shoken = new DateTime(sY, sM, sD);
                }



                /*
                if (!DateTimeEx.IsDate(sY, sM, 1))
                {
                    setStatus(verifyBoxF1Y, true);
                    setStatus(verifyBoxF1M, true);
                }
                else
                {
                    setStatus(verifyBoxF1Y, false);
                    setStatus(verifyBoxF1M, false);
                    shoken = new DateTime(sY, sM, 1);
                }
                */



                //20190322120500 furukawa ed ////////////////////////

            }



            //合計
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);


            //受診者区分
            //0：高七　→　2：高齢者（一定以上）
            //2：本人
            //4：六歳　→　5：未就学
            //6：家族
            //8：高一　→　1：高齢者（一般）

            
            //20190322175116 furukawa st ////////////////////////
            //合計・請求金額・本家区分チェック


            //請求
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;

            /*
            if ((family == 2 || family == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (family == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (family == 4 && (int)(total * 80 / 100) != charge)
                payError = true;
                */



            //本家区分	給付割合
            //2:本人		7
            //4:六歳		8
            //6:家族		7
            //8:高一		8
            //0:高7			7
            //受診者区分
            switch (jusinsyaKubun)
            {
                case 2:
                case 6:
                case 0:
                    if (rate != 7) payError = true;
                    break;

                case 4:
                case 8:
                    if (rate != 8) payError = true;
                    break;


                default:
                    break;
            }

            //20190322175116 furukawa ed ////////////////////////




            //20190325173636 furukawa st ////////////////////////
            //請求金額と合計金額×給付割合の確認

            int ans = 0;
            ans = (int)(total * (rate*0.1));
            if (ans != charge) payError = true;

            //20190325173636 furukawa ed ////////////////////////







            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);




            //20190808145316 furukawa st ////////////////////////
            //施術開始年月日、施術終了年月日の年月を施術年月に修正



                        //20190722180906 furukawa st ////////////////////////
                        //診療開始終了日の令和対応

                        //20190304135648 furukawa st ////////////////////////
                        //施術開始日、終了日の追加
                        //開始日
                        //var startDate = dateCheck(4, year, month, verifyBoxF1Start);
                        //終了日
                        // var finishDate = dateCheck(4, year, month, verifyBoxF1Finish);

                        ////開始日 初検日の年、月と診療日をあわせる
                        //var startDate = new DateTime(sY, sM, int.Parse(verifyBoxF1Start.Text));          
                        ////終了日        
                        //var finishDate = new DateTime(sY, sM, int.Parse(verifyBoxF1Finish.Text));

            

                        //開始日 初検日の年、月と診療日をあわせる

                        //var startDate = new DateTime(year, month, int.Parse(verifyBoxF1Start.Text));
                        //var finishDate = new DateTime(year, month, int.Parse(verifyBoxF1Finish.Text));

            

            int sejutuYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            int sejutuMonth = month;


            if (verifyBoxF1Start.Text == string.Empty) hasError = true;
            if (verifyBoxF1Finish.Text == string.Empty) hasError = true;

            //20190914230314 furukawa st ////////////////////////
            //施術開始、終了日NULLチェック
            
            DateTime startDate = DateTime.MinValue ;
            if (verifyBoxF1Start.Text != string.Empty)
            {
                //施術開始日
                startDate = new DateTime(sejutuYear, sejutuMonth, int.Parse(verifyBoxF1Start.Text));
                //var startDate = new DateTime(sejutuYear, sejutuMonth, int.Parse(verifyBoxF1Start.Text));
            }
            DateTime finishDate=DateTime.MinValue;
            if (verifyBoxF1Finish.Text != string.Empty)
            {

                //施術終了日   
                finishDate = new DateTime(sejutuYear, sejutuMonth, int.Parse(verifyBoxF1Finish.Text));
                //var finishDate = new DateTime(sejutuYear, sejutuMonth, int.Parse(verifyBoxF1Finish.Text));
            }
            //20190914230314 furukawa ed ////////////////////////



            //20190304135648 furukawa ed ////////////////////////   

            //20190722180906 furukawa ed ////////////////////////

            //20190808145316 furukawa ed ////////////////////////



            /*
            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);
            */


            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
            


            //20190322175309 furukawa st ////////////////////////
            //金額チェック機能


            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxFamily.BackColor = Color.GreenYellow;
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                verifyBoxKyufuRate.BackColor = Color.GreenYellow;//20190326133029 furukawa エラーチェック箇所に給付割合追加
                verifyBoxJuryoType.BackColor = Color.GreenYellow;//20190326133723 furukawa エラーチェック箇所に受診者区分追加


                var res = MessageBox.Show("本家区分・給付割合・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }
            //20190322175309 furukawa ed ////////////////////////



            //ナンバリング抜けチェック
            int preNum = 0;
            int index = bsApp.IndexOf(app);
            var l = (List<App>)bsApp.DataSource;
            for (int i = l.Count - 1; i >= 0; i--)
            {
                if (l[i].MediYear > 0)
                {
                    if (!int.TryParse(l[i].Numbering, out preNum)) continue;
                    break;
                }
            }


            #endregion

            #region appに入力値を反映


            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            
            //app.HihoNum = hnum;
            app.HihoNum = strHnum;//被保番
            
            
            //拡張入力で入力するので不要
            //どっちでいく？→ClinicNumじゃないかも
            //app.ClinicNum = strHosNumber;//医療機関コード＝柔整師コード仕様書より   
            //app.ClinicNum = verifyBoxClinicNum.Text.Trim(); //医療機関コード＝柔整師コード仕様書より   

            //拡張入力で選択
            app.InsNum = iNumber;//保険者番号

            //20190612171127 furukawa st ////////////////////////
            //2019-06-12仕様変更　不要
            
                    //app.HihoPref = pref;//都道府県 医療機関コード＝柔整師コードの都道府県
            //20190612171127 furukawa ed ////////////////////////

            //20190325180750 furukawa st ////////////////////////
            //給付割合、請求金額登録
            app.Ratio = rate;
            app.Charge = charge;
            //20190325180750 furukawa ed ////////////////////////


            //2019/02/19更新****************************************************************************************************************************
            //200 202 204 206 208 600 602 604 606 608

            //受診者区分
            //0：高七　→　2：高齢者（一定以上）
            //2：本人
            //4：六歳　→　5：未就学
            //6：家族
            //8：高一　→　1：高齢者（一般）

            //本家区分
            //2：本人　→　1:本人
            //6：家族　→　2:家族


            //本家区分を前にしないと、受診者区分は０の場合があり３桁にならないのでわかりにくい
            app.Family = family * 100 + jusinsyaKubun;   //受診者区分と本人家族をまとめて記録
            //app.Family = jusinsyaKubun * 100 + family;   //受診者区分と本人家族をまとめて記/録
            
            //app.Family = family;//本人家族






            app.Sex = sex;//性別

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.NewContType = DateTimeEx.GetAdYearFromHs(year * 100 + month) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;
            //app.NewContType = DateTimeEx.GetAdYearFromHs(year) == shoken.Year && shoken.Month == month ? NEW_CONT.新規 : NEW_CONT.継続;

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            app.FushoFirstDate1 = shoken;//初検

            app.Birthday = birthDt;
            app.Total = total;

            //htypeフィールドに一般、とくたいの区別をもたせる
            //保険者番号 一般は　06271779　特退は　63271779 北村さんより
            app.HihoType = int.Parse(iNumber.ToString().Substring(0, 2));

            //レセプト管理番号
            app.ComNum = strRezeKanriBango;

            //app.Charge = charge;            
            app.CountedDays = days;

            //医療機関コード＝柔整師コードの３～８桁目仕様書より
            //app.DrNum = verifyBoxHosNumber.Text.Trim();
            //app.DrNum = strHosNumber;
            //app.DrNum = verifyBoxClinicNum.Text.Trim() ;


            //20190501142425 furukawa st ////////////////////////
            //appTypeが入ってなかったので入れる
            app.AppType = (int)scan.AppType == 7 ? APP_TYPE.鍼灸 : APP_TYPE.柔整;
            //20190501142425 furukawa ed ////////////////////////

            
            //20190304135745 furukawa st ////////////////////////
            //施術開始日、終了日の追加            
            app.FushoStartDate1 = startDate;
            app.FushoFinishDate1 = finishDate;
            //20190304135745 furukawa ed ////////////////////////


            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();


            setPersonalInfoFromTekiyou(app);

            #endregion

            return true;

        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private bool setPersonalInfoFromTekiyou(App app)
        {
            string strSyoNo = verifyBoxHnum.Text!= string.Empty ? verifyBoxHnum.Text.Trim() : string.Empty;
            if (strSyoNo == string.Empty) return false;

            List<TekiyouData> lst;

            StringBuilder sbWhere = new StringBuilder();
            if (verifyBoxFamily.Text == "2")
            {
                sbWhere.AppendLine(" Syono = '" + strSyoNo + "'");
                sbWhere.AppendLine(" and branch = '0'");
                sbWhere.AppendLine(" and birthadymd = '" + app.Birthday.ToString("yyyy/MM/dd") + "'");
                sbWhere.AppendLine(" and zokugara = ''");
                

                lst= DB.Main.Select<TekiyouData>(sbWhere.ToString()).ToList();
                if (lst.Count == 0) return false;

                app.HihoZip = lst[0].zip;
                app.HihoAdd = lst[0].add;
                app.HihoName = lst[0].hName;
                app.PersonName = lst[0].hName;

            }

            if (verifyBoxFamily.Text != "2")
            {
                sbWhere.AppendLine(" Syono = '" + strSyoNo + "'");
                sbWhere.AppendLine(" and branch = '0'");
                sbWhere.AppendLine(" and zokugara=''");

                lst = DB.Main.Select<TekiyouData>(sbWhere.ToString()).ToList();
                if (lst.Count == 0) return false;

                app.HihoZip = lst[0].zip;
                app.HihoAdd = lst[0].add;
                app.HihoName = lst[0].hName;

                sbWhere.Remove(0, sbWhere.ToString().Length);
                sbWhere.AppendLine(" Syono='" + strSyoNo + "'");
                sbWhere.AppendLine(" and branch='0'");
                sbWhere.AppendLine(" and birthadymd='" + app.Birthday.ToString("yyyy/MM/dd") + "'");
                sbWhere.AppendLine(" and zokugara<>''");

                lst = DB.Main.Select<TekiyouData>(sbWhere.ToString()).ToList();
                if (lst.Count == 0) return false;                

                app.PersonName = lst[0].hName;
            }

            return true;
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //20190208130636 furukawa st ////////////////////////
            //フォルダ名より保険者番号、本人家族取得
            if (!GetiNumber_family()) return;
            //20190208130636 furukawa ed ////////////////////////


            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }
            }
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            if (!System.IO.File.Exists(fn))
            {
                labelImageName.Text = "画像表示でエラーが発生しました";
                return;
            }

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //被保険者番号
                var hn = app.HihoNum.Split('-');
                if (hn.Length == 2)
                {
                    setValue(verifyBoxHKigo, hn[0], firstTime, nv);
                    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxHKigo, string.Empty, firstTime, nv);
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                }

                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);

                //setValue(verifyBoxAppType, app.AppType == APP_TYPE.鍼灸 ? "7" : "0", firstTime, nv);


                //20190612171054 furukawa st ////////////////////////
                //2019-06-12仕様変更　不要
                
                        //都道府県番号
                        //setValue(verifyBoxPref, app.HihoPref.ToString(), firstTime, nv);
                        //setValue(verifyBoxHosNumber, app.DrNum, firstTime, nv);
                //20190612171054 furukawa ed ////////////////////////

                //20190212171243 furukawa st ////////////////////////
                //ナンバリング入れる (レセプト管理番号)
                //一フィールドをわける



                //20190326123225 furukawa st ////////////////////////
                //管理番号頭４桁を管理番号頭４桁を変更不可に                
                //if(app.ComNum != string.Empty) setValue(verifyBoxKanriBango1, app.ComNum.Substring(0, 4).ToString(), firstTime, nv);
                if (app.ComNum != string.Empty) setValue(verifyBoxKanriBango1, app.ComNum.Substring(0, 4).ToString(), false, false);
                //20190326123225 furukawa ed ////////////////////////


                if (app.ComNum != string.Empty) setValue(verifyBoxKanriBango2, app.ComNum.Substring(4, 5).ToString(), firstTime, nv);
                //20190212171243 furukawa ed ////////////////////////


                //受診者区分と本家区分コードをわけてそれぞれの入力欄にいれる
                if (app.Family.ToString().Length == 3) setValue(verifyBoxJuryoType, app.Family.ToString().Substring(2, 1), firstTime, nv);
                setValue(verifyBoxFamily, app.Family.ToString().Substring(0, 1), firstTime, nv);
                //setValue(verifyBoxFamily, app.Family.ToString(), firstTime, nv);

                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);

                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
                setValue(verifyBoxTotal, app.Total, firstTime, nv);


                //20190304135038 furukawa st ////////////////////////
                //施術開始日、終了日の追加

                setValue(verifyBoxF1Start, app.FushoStartDate1.Day, firstTime, nv);
                setValue(verifyBoxF1Finish, app.FushoFinishDate1.Day, firstTime, nv);
                //20190304135038 furukawa ed ////////////////////////



                //請求金額
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);


                //20190325181207 furukawa st ////////////////////////
                //給付割合
                setValue(verifyBoxKyufuRate, app.Ratio, firstTime, nv);
                //20190325181207 furukawa ed ////////////////////////


                //医療機関コード＝柔整師コード
                //setValue(verifyBoxClinicNum, app.ClinicNum, firstTime, nv);


                //負傷名
                setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                setValue(verifyBoxF5, app.FushoName5, firstTime, false);

                
                //初検日
                if (app.FushoFirstDate1.IsNullDate())
                {
                    setValue(verifyBoxF1Y, string.Empty, firstTime, false);
                    setValue(verifyBoxF1M, string.Empty, firstTime, false);
                    setValue(verifyBoxF1D, string.Empty, firstTime, false); //20190325181651 furukawa 初検日の日

                }
                else
                {
                    //20190914230426 furukawa st ////////////////////////
                    //初検日ベリファイ対象に

                    setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, nv);
                    setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, nv);
                    setValue(verifyBoxF1D, app.FushoFirstDate1.Day.ToString(), firstTime, nv); //20190325181730 furukawa 初検日の日

                    //setValue(verifyBoxF1Y, DateTimeEx.GetJpYear(app.FushoFirstDate1).ToString(), firstTime, false);
                    //setValue(verifyBoxF1M, app.FushoFirstDate1.Month.ToString(), firstTime, false);
                    //setValue(verifyBoxF1D, app.FushoFirstDate1.Day.ToString(), firstTime, false); //20190325181730 furukawa 初検日の日

                    //20190914230426 furukawa ed ////////////////////////
                }
            }
        }

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                panelHnum.Visible = true;
                panelTotal.Visible = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }


        //負傷名未入力のときは次の負傷名欄を飛ばす
        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void verifyBoxJuryoType_TextChanged(object sender, EventArgs e)
        {
            //20190220121806 furukawa st ////////////////////////
            //入力内容によって自動入力する

            //受診者区分が2.本人の場合は本家区分も2.本人とする
            if (verifyBoxJuryoType.Text.ToString() == "2") {
                verifyBoxFamily.Text = "2";
            }
            //受診者区分が6.家族の場合は本家区分も6.家族とする
            else if (verifyBoxJuryoType.Text.ToString() == "6")
            {
                verifyBoxFamily.Text = "6";
            }

            //要望があれば消す
            /*
            else
            {
                verifyBoxFamily.Text = string.Empty;
            }
            */
            //20190220121806 furukawa ed ////////////////////////

        }


        //20190213105204 furukawa st ////////////////////////
        //管理番号の下５桁を０埋め
        private void verifyBoxKanriBango2_Validated(object sender, EventArgs e)
        {          
            verifyBoxKanriBango2.Text = verifyBoxKanriBango2.Text.PadLeft(5, '0');
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
          //  else if (fushoControls.Contains(t)) posFusho = pos;
           // else if (numberControls.Contains(t)) posNumbering = pos;

        }

        private void verifyBoxAppType_TextChanged(object sender, EventArgs e)
        {

            //20190213112238 furukawa st ////////////////////////
            //申請区分がないのでずっと使える

            verifyBoxF1.Enabled = true;
            verifyBoxF2.Enabled = true;
            verifyBoxF3.Enabled = true;
            verifyBoxF4.Enabled = true;
            verifyBoxF5.Enabled = true;

            /*
                        if (verifyBoxAppType.Text != "7" && firstTime)
                        {
                            verifyBoxF1.Enabled = true;
                            verifyBoxF2.Enabled = true;
                            verifyBoxF3.Enabled = true;
                            verifyBoxF4.Enabled = true;
                            verifyBoxF5.Enabled = true;
                        }
                        else
                        {
                            verifyBoxF1.Enabled = false;
                            verifyBoxF2.Enabled = false;
                            verifyBoxF3.Enabled = false;
                            verifyBoxF4.Enabled = false;
                            verifyBoxF5.Enabled = false;
                        }*/

            //20190213112238 furukawa ed ////////////////////////

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        //20190208130636 furukawa ////////////////////////
        //フォルダ名より保険者番号取得
        private bool GetiNumber_family()
        {
            if (scan.Note1.IndexOf("071") > 0)
            {
                verifyBoxKanriBango1.Text = "0071";
                iNumber = "06271779";                
                return true;
            }
            if (scan.Note1.IndexOf("072") > 0)
            {
                verifyBoxKanriBango1.Text = "0072";
                iNumber = "06271779";               
                return true;
            }
            if (scan.Note1.IndexOf("171") > 0)
            {
                verifyBoxKanriBango1.Text = "0171";
                iNumber = "63271779";                
                return true;
            }
            if (scan.Note1.IndexOf("172") > 0)
            {
                verifyBoxKanriBango1.Text = "0172";
                iNumber = "63271779";
                return true;
            }

            MessageBox.Show("Note1に 0071,0072,0171,0172 が含まれていません"
                ,Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            return false;

        }


    }
}
