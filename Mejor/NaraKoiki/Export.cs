﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.NaraKoiki
{
    class Export
    {
        /// <summary>
        /// CSVの吐き出し
        /// </summary>
        /// <param name="item"></param>
        /// <param name="k"></param>
        /// <param name="exFilename"></param>
        /// <returns></returns>
        private static bool ExportCSV(App a, KoikiData k, Shokai.ShokaiData s, System.IO.StreamWriter sw)
        {
            var sl = new string[29];
            sl[0] = k.Did.ToString();
            sl[1] = k.Num;
            sl[2] = k.Name;
            sl[3] = k.Kana;
            sl[4] = k.DrNum;
            sl[5] = string.Empty;
            sl[6] = string.Empty;
            sl[7] = k.MediHYM.ToString();
            sl[8] = a.ChargeYear.ToString("00") + a.ChargeMonth.ToString("00");
            sl[9] = a.FushoName1;
            sl[10] = a.FushoName2;
            sl[11] = a.FushoName3;
            sl[12] = a.FushoName4;
            sl[13] = a.FushoName5;
            sl[14] = k.Days.ToString();
            sl[15] = k.Total.ToString();
            sl[16] = k.Aid.ToString();
            sl[17] = a.Distance == 999 ? "1" : "0";
            sl[18] = k.ReceNum;
            sl[19] = k.ReceNum + ".tif"; //a.Aimagefile;
            sl[20] = k.Zip;    //郵便番号
            sl[21] = k.Add;    //住所1
            sl[22] = k.DestName;   //送付先宛名
            sl[23] = k.DestZip;    //送付先郵便番号
            sl[24] = k.DestAdd;    //送付先住所
            sl[25] = s == null ? "0" : ((int)s.Reason).ToString();  //照会理由
            sl[26] = "0";       //返戻理由
            sl[27] = s == null ? "0" : ((int)s.Status).ToString();  //照会状況
            sl[28] = (a.AppType == APP_TYPE.鍼灸 ? 2 : a.AppType == APP_TYPE.あんま ? 4 : 1).ToString();    //2=鍼灸、3=あんま

            try
            {
                sw.WriteLine(string.Join(",", sl));
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\nCSVデータの書き込みに失敗しました");
                return false;
            }

            return true;
        }

        private static bool createNotMatchCsv(KoikiData k, System.IO.StreamWriter sw)
        {
            var sl = new string[16];
            
            sl[0] = k.Num;
            sl[1] = k.Name;
            sl[2] = k.Kana;
            sl[3] = k.Zip;
            sl[4] = k.Add;
            sl[5] = k.DestZip;
            sl[6] = k.DestAdd;
            sl[7] = k.DestName;
            sl[8] = k.DrNum;
            sl[9] = string.Empty;
            sl[10] = string.Empty;
            sl[11] = k.MediYM.ToString("000000");
            sl[12] = k.AppYM.ToString("000000");
            sl[13] = k.Days.ToString();
            sl[14] = k.Total.ToString();
            sl[15] = k.ReceNum;

            try
            {
                sw.WriteLine("\"" + string.Join("\",\"", sl) + "\"");
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\nCSVデータの書き込みに失敗しました");
                return false;
            }

            return true;
        }

        private static bool createStatusUpdateCsv(string fileName)
        {
            var dir = System.IO.Path.GetDirectoryName(fileName);
            dir = dir + "\\s";
            System.IO.Directory.CreateDirectory(dir);

            var l = Shokai.ShokaiResultData.SelectUpdated(DateTime.Today.AddMonths(-3), DateTime.Today);
            using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
            {
                var sl = new string[7];

                sw.WriteLine("\"aid\",\"reason\",\"result\",\"status\",\"henrei\",\"note\",\"note\",\"filename\"");
                foreach (var item in l)
                {
                    sl[0] = item.AID.ToString();
                    sl[1] = ((int)item.Reason).ToString();
                    sl[2] = ((int)item.Result).ToString();
                    sl[3] = ((int)item.Status).ToString();
                    sl[4] = item.Henrei.ToString();
                    sl[5] = item.Note;
                    sl[6] = item.FileName;

                    try
                    {
                        sw.WriteLine("\"" + string.Join("\",\"", sl) + "\"");
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex + "\r\n\r\nCSVデータの書き込みに失敗しました");
                        return false;
                    }

                    //画像ファイルのコピー
                    if (item.FileName == string.Empty) continue;
                    System.IO.File.Copy(item.GetFullPath(), dir + "\\" + item.FileName);
                }
            }

            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            var wf = new WaitForm();
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,処理年,処理月,No.,照会ID,記号番号,被保険者名,施術年,施術月,実日数,合計,施術所番号,宛名氏名,郵便番号,住所,電算番号,照会理由,メモ";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    int no = 0;
                    string insNoStr = ((int)InsurerID.HIROSHIMA_KOIKI).ToString();
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        int kid = 0;
                        int.TryParse(item.Numbering, out kid);
                        var kd = KoikiData.Select(kid);

                        //2016.8.24 松本
                        //広域データがない時でもリスト作成要調整
                        if (kd == null) kd = new KoikiData();

                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(insNoStr + ymStr + no.ToString("0000"));
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(kd.Name);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.DrNum.ToString());
                        ss.Add(kd.DestName == string.Empty ? kd.Name : kd.DestName);
                        ss.Add(kd.DestZip == string.Empty ? kd.Zip : kd.DestZip);
                        ss.Add(kd.DestAdd == string.Empty ? kd.Add : kd.DestAdd);
                        ss.Add(kd.ReceNum);
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add(item.Memo);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }

        public static bool ListExportForMedivery(List<App> list, string fileName)
        {
            var wf = new WaitForm();
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,処理年,処理月,No.,通知番号奈良-,照会ID," +
                        "記号番号,被保険者名,施術年,施術月,実日数,往療回数,合計," +
                        "施術院,宛名氏名,郵便番号,住所1,住所2,電算番号,照会理由,メモ";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    int no = 0;
                    string insNoStr = ((int)InsurerID.HIROSHIMA_KOIKI).ToString();
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        string zip;
                        if (item.HihoZip.Length == 7) zip = item.HihoZip.Insert(3, "-");
                        else zip = item.HihoZip;

                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(string.Empty);
                        ss.Add(insNoStr + ymStr + no.ToString("0000"));
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.VisitTimes.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.ClinicName.ToString());
                        ss.Add(item.HihoName.ToString());
                        ss.Add(zip);
                        ss.Add(item.HihoAdd.ToString());
                        ss.Add(string.Empty);
                        ss.Add(string.Empty);
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add(item.Memo);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }

    }
}
