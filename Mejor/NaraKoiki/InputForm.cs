﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.NaraKoiki
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp;
        private BindingSource bsKoikiData;
        bool isAhakiKoikiMatchnigMode = false;
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 60);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1200);
        Point posTotal = new Point(800, 1800);
        Point posBui = new Point(80, 750);
        Point posDays = new Point(400, 800);

        public InputForm(ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();

            isAhakiKoikiMatchnigMode = false;
            this.scanGroup = sGroup;
            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //データリストを作成
            var list = App.GetAppsGID(sGroup.GroupID);

            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            initialize();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);
        }

        /// <summary>
        /// あはきマッチング用
        /// </summary>
        /// <param name="cym"></param>
        public InputForm(int cym)
        {
            InitializeComponent();

            isAhakiKoikiMatchnigMode = true;
            this.scanGroup = new ScanGroup(cym);
            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            //データリストを作成
            var list = AhakiMatching.GetNothingMatch(cym);

            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            initialize();
        }

        private void initialize()
        {
            dataGridKoikiData.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridKoikiData.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;

            //あはきの場合は常に継続 実日数も入力
            if (!string.IsNullOrWhiteSpace(scanGroup.note2) || isAhakiKoikiMatchnigMode)
            {
                verifyBoxDays.Enabled = true;
                verifyBoxNewCont.Enabled = true;//[20170130(kubo)]選ぶようになりました
            }
            else
            {
                verifyBoxDays.Enabled = false;
                verifyBoxNewCont.Enabled = true;
            }

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            //広域データ欄
            bsKoikiData = new BindingSource();
            bsKoikiData.DataSource = new List<KoikiData>();
            dataGridKoikiData.DataSource = bsKoikiData;

            foreach (DataGridViewColumn c in dataGridKoikiData.Columns) c.Visible = false;
            dataGridKoikiData.Columns[nameof(KoikiData.Did)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.Did)].Width = 60;
            dataGridKoikiData.Columns[nameof(KoikiData.MediYM)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.MediYM)].Width = 40;
            dataGridKoikiData.Columns[nameof(KoikiData.MediYM)].HeaderText = "年月";
            dataGridKoikiData.Columns[nameof(KoikiData.Num)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.Num)].Width = 70;
            dataGridKoikiData.Columns[nameof(KoikiData.Num)].HeaderText = "被保番";
            dataGridKoikiData.Columns[nameof(KoikiData.Name)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.Name)].Width = 95;
            dataGridKoikiData.Columns[nameof(KoikiData.Name)].HeaderText = "氏名";
            dataGridKoikiData.Columns[nameof(KoikiData.DrNum)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.DrNum)].Width = 80;
            dataGridKoikiData.Columns[nameof(KoikiData.DrNum)].HeaderText = "施術師番号";
            dataGridKoikiData.Columns[nameof(KoikiData.Total)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.Total)].Width = 50;
            dataGridKoikiData.Columns[nameof(KoikiData.Total)].HeaderText = "合計";
            dataGridKoikiData.Columns[nameof(KoikiData.Aid)].Visible = true;
            dataGridKoikiData.Columns[nameof(KoikiData.Aid)].Width = 60;

            //pgupで選択行が変わるのを防ぐ
            dataGridKoikiData.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);

            if (app.StatusFlagCheck(StatusFlag.自動マッチ済)) verifyBoxNewCont.Focus();
            else verifyBoxY.Focus();
        }

        //フォーム表示時
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (dataGridViewPlist.RowCount == 0)
            {
                MessageBox.Show("表示すべきデータがありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                return;
            }

            verifyBoxY.Focus();
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            selectKoikiData();
        }

        private void selectKoikiData()
        {
            bsKoikiData.Clear();
            if (!isAhakiKoikiMatchnigMode && !string.IsNullOrWhiteSpace(scanGroup.note2)) return;
            var app = (App)bsApp.Current;

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsKoikiData.ResetBindings(false);
                return;
            }


            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            var cy = DateTimeEx.GetAdYearFromHs(scanGroup.cyear * 100 + scanGroup.cmonth);
            y = DateTimeEx.GetAdYearFromHs(y * 100 + m);

            //var cy = DateTimeEx.GetAdYearFromHs(scanGroup.cyear);
            //y = DateTimeEx.GetAdYearFromHs(y);
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊


            var l = KoikiData.SerchByInput(cy, scanGroup.cmonth, y, m, verifyBoxHnum.Text.Trim(), total);
            if (l != null) bsKoikiData.DataSource = l;
            bsKoikiData.ResetBindings(false);

            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridKoikiData.RowCount; i++)
                {
                    string did = dataGridKoikiData["Did", i].Value.ToString();
                    if (app.Numbering == did)
                    {
                        //既に一意の広域データとマッチング済みの場合。
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        dataGridKoikiData.CurrentCell = dataGridKoikiData[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridKoikiData.CurrentCell = null;
            }
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);


            //20200116094547 furukawa st ////////////////////////
            //エラーチェックはなかったが、空欄をエラーとする            
            setStatus(verifyBoxY, year < 0 );
            //20200116094547 furukawa ed ////////////////////////




            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 3文字以上かつ数字に直せること
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length < 2 ||
                !long.TryParse(verifyBoxHnum.Text, out long hnumTemp));

            //合計
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //新規・継続の抜けチェック
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);
            
            //往療料
            bool visit = verifyCheckBoxVisit.Checked;

            //実日数 あはき時のみ
            int days = 0;
            if (verifyBoxDays.Enabled)
            {
                days = verifyBoxDays.GetIntValue();
                setStatus(verifyBoxDays, days < 1 || 31 < days);
            }

            //ここまでのチェックで必須エラーが検出されたらfalse
            

            if (hasError)
            {
                showInputErrorMessage();
                return false;             
            }
            //if (hasError) return false;



            //ここから値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text;
            app.Total = total;
            app.Distance = visit ? 999 : 0;

            //新規・継続
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //申請書種別
            if (!isAhakiKoikiMatchnigMode) app.AppType = scan.AppType;

            //部位
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            //広域データからのデータコピー
            if (app.AppType == APP_TYPE.柔整 || isAhakiKoikiMatchnigMode)
            {
                var k = (KoikiData)dataGridKoikiData.CurrentRow.DataBoundItem;
                app.InsNum = k.InsNum;
                app.PersonName = k.Name;
                app.DrNum = k.DrNum;
                app.CountedDays = k.Days;
                app.FushoFirstDate1 = k.StartDate;
                app.Charge = k.Seikyu;
                app.Partial = k.Futan;
                app.HihoZip = k.Zip;
                app.HihoAdd = k.Add;
                app.TaggedDatas.DestName = k.DestName;
                app.TaggedDatas.DestZip = k.DestZip;
                app.TaggedDatas.DestAdd = k.DestAdd;
                app.Numbering = k.Did.ToString();
                app.RrID = k.Did;
                app.ComNum = k.ReceNum;
            }
            else
            {
                app.CountedDays = days;
            }

            //チェックOKならデータを格納したインスタンスを返す
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            if (verifyBoxY.Text == "--")
            {
                //続紙
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else if (verifyBoxY.Text == "**")
            {
                //エラー
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                app.AppType = APP_TYPE.エラー;
            }
            else
            {
                //申請書の場合
                if (dataGridKoikiData.CurrentCell == null && (isAhakiKoikiMatchnigMode || string.IsNullOrWhiteSpace(scanGroup.note2)))
                {
                    //20220511095259 furukawa st ////////////////////////
                    //メッセージ変更
                    
                    if (MessageBox.Show("マッチングデータがありません。登録しますか？",
                                 Application.ProductName,
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Exclamation,
                                 MessageBoxDefaultButton.Button2) != DialogResult.Yes) return false;

                    //      MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                    //          "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                    //          "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //      return false;
                    //20220511095259 furukawa ed ////////////////////////
                }

                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //入力ログ
                //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                if (app.Ufirst == 0 && !InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now-dtstart_core, jyuTran))
                    return false;

                //データ記録
                app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput);

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                //広域データ更新
                if (app.AppType == APP_TYPE.柔整 || isAhakiKoikiMatchnigMode)
                {
                    if (app.RrID == 0)
                    {
                        KoikiData.AidDelete(app.Aid, tran);
                    }
                    else
                    {
                        var kd = (KoikiData)dataGridKoikiData.CurrentRow.DataBoundItem;
                        if (kd.Aid != app.Aid) KoikiData.AidUpdate(kd.Did, app.Aid, tran);
                    }
                }

                jyuTran.Commit();
                tran.Commit();
            }

            return true;
        }

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //まずは現在の表示をリセット
            iVerifiableAllClear(panelRight);

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力者情報をリセット
            labelInputUser.Text = "入力者：";

            //App_Flagのチェック
            labelAppStatus.Text = app.InputStatus.ToString();
            if (app == null) return;
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
                labelAppStatus.BackColor = Color.Cyan;
            }
            else if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            {
                //マッチング有のデータについては一部広域からのデータを表示
                setNoInputAppWithOcr(app);
                labelAppStatus.BackColor = Color.Yellow;
            }
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
                labelAppStatus.BackColor = Color.Red;
            }

            //あはきの場合は常に継続
            //if (!string.IsNullOrWhiteSpace(this.scanGroup.note2) || isAhakiKoikiMatchnigMode) verifyBoxNewCont.Text = "2";

            //広域データ表示
            selectKoikiData();

            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
                
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            if (string.IsNullOrEmpty(scanGroup.note2))
            {
                var ocr = app.OcrData.Split(',');
                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                    verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                    verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                    verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                    verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                }

                //新規/継続
                if (ocr.Length > 127)
                {
                    if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                    if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                }
            }
        }

        /// <summary>
        /// マッチング有りの場合：広域データとOCRデータの両方から入力欄にフィルします
        /// </summary>
        private void setNoInputAppWithOcr(App app)
        {
            try
            {
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxHnum.Text = app.HihoNum;
                verifyBoxTotal.Text = app.Total.ToString();

                if (!isAhakiKoikiMatchnigMode && string.IsNullOrEmpty(scanGroup.note2))
                {
                    var ocr = app.OcrData.Split(',');
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }

                    //新規/継続
                    if (ocr.Length > 127)
                    {
                        if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                        if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            labelInputUser.Text = $"入力者：{User.GetUserName(app.Ufirst)}";

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                //続紙
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                //白バッジ、その他
                verifyBoxY.Text = "++";
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.エラー)
            {
                //エラー
                verifyBoxY.Text = "**";
            }
            else
            {
                //申請書年月
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();

                //被保険者番号
                verifyBoxHnum.Text = app.HihoNum;

                //合計金額
                verifyBoxTotal.Text = app.Total.ToString();

                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;

                //新規/継続
                verifyBoxNewCont.Text =
                    app.NewContType == NEW_CONT.新規 ? "1" :
                    app.NewContType == NEW_CONT.継続 ? "2" : "";

                //往療
                verifyCheckBoxVisit.Checked = app.Distance == 999;

                //実日数
                verifyBoxDays.Text = app.CountedDays == 0 ? "" : app.CountedDays.ToString();

                //突合情報
                int oldDid = 0;
                int.TryParse(app.Numbering, out oldDid);
            }
        }


        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var setCanInput = new Action<TextBox, bool>((t, b) =>
                {
                    t.ReadOnly = !b;
                    t.TabStop = b;
                    if (!b)
                        t.BackColor = SystemColors.Menu;
                    else
                        t.BackColor = SystemColors.Info;
                });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                setCanInput(verifyBoxM, false);
                setCanInput(verifyBoxHnum, false);
                setCanInput(verifyBoxTotal, false);
                setCanInput(verifyBoxNewCont, false);
                setCanInput(verifyBoxDays, false);
                setCanInput(verifyBoxF1, false);
                setCanInput(verifyBoxF2, false);
                setCanInput(verifyBoxF3, false);
                setCanInput(verifyBoxF4, false);
                setCanInput(verifyBoxF5, false);
                verifyBoxNewCont.TabStop = false;

                verifyCheckBoxVisit.BackColor = SystemColors.Menu;
                verifyCheckBoxVisit.Enabled = false;
            }
            else
            {
                //申請書の場合
                setCanInput(verifyBoxM, true);
                setCanInput(verifyBoxHnum, true);
                setCanInput(verifyBoxTotal, true);
                setCanInput(verifyBoxNewCont, true);
                setCanInput(verifyBoxDays, true);
                setCanInput(verifyBoxF1, true);
                setCanInput(verifyBoxF2, true);
                setCanInput(verifyBoxF3, true);
                setCanInput(verifyBoxF4, true);
                setCanInput(verifyBoxF5, true);
                verifyBoxNewCont.TabStop = true;

                verifyCheckBoxVisit.BackColor = SystemColors.Info;
                verifyCheckBoxVisit.Enabled = true;
                buiTabStopAdjust();
            }
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (sender == verifyBoxY) p = posYM;
            else if (sender == verifyBoxM) p = posYM;
            else if (sender == verifyBoxHnum) p = posHnum;
            else if (sender == verifyBoxTotal) p = posTotal;
            else if (sender == verifyBoxNewCont) p = posNew;
            else if (sender == verifyBoxDays) p = posDays;
            else if (sender == verifyBoxF1) p = posBui;
            else if (sender == verifyBoxF2) p = posBui;
            else if (sender == verifyBoxF3) p = posBui;
            else if (sender == verifyBoxF4) p = posBui;
            else if (sender == verifyBoxF5) p = posBui;
            else if (sender == verifyCheckBoxVisit) p = posVisit;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;
            if (verifyBoxY.Focused == true) posYM = pos;
            else if (verifyBoxM.Focused == true) posYM = pos;
            else if (verifyBoxHnum.Focused == true) posHnum = pos;
            else if (verifyBoxNewCont.Focused == true) posNew = pos;
            else if (verifyBoxDays.Focused == true) posDays = pos;
            else if (verifyBoxTotal.Focused == true) posTotal = pos;
            else if (verifyBoxF1 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF2 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF3 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF4 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF5 == this.ActiveControl) posBui = pos;
        }
    }      
}
