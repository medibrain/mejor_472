﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.NaraKoiki
{
    public partial class ImportForm : Form
    {
        public ImportForm(int cym)
        {
            InitializeComponent();
            textBoxY.Text = DateTimeEx.GetJpYearFromYM(cym).ToString();
            textBoxM.Text = (cym % 100).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int y, m;
            int.TryParse(textBoxY.Text, out y);
            int.TryParse(textBoxM.Text, out m);

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            y = DateTimeEx.GetAdYearFromHs(y * 100 + m);
            //y = DateTimeEx.GetAdYearFromHs(y);
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            if (y < DateTime.Today.Year-2 || DateTime.Today.Year + 2 < y || m < 1 || 12 < m)
            {
                MessageBox.Show("年月を正しく指定して下さい");
                return;
            }

            using (var f = new OpenFileDialog())
            {
                f.Filter = "CSVファイル|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                KoikiData.Import(f.FileName, y, m);
            }
        }
    }
}
