﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;

namespace Mejor.NaraKoiki
{
    class ViewerDataExport
    {
        public static bool ExportViewerData(int cym)
        {
            string log = string.Empty;
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(fileName);
            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            fileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                wf.LogPrint("提供データを取得しています");
                var rrs = KoikiData.SelectAym(cym);
                var dic = new Dictionary<int, KoikiData>();
                rrs.ForEach(rr => { if (rr.Aid != 0) dic.Add(rr.Aid, rr); });

                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                int receCount = 0;
                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerData.Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];
                        if (!dic.ContainsKey(app.Aid))
                        {
                            log += $"突合データがない申請書があります AID:{app.Aid}\r\n";
                            for (i++; i < apps.Count; i++) if (apps[i].YM > 0) break;
                            continue;
                        }

                        var rr = dic[app.Aid];
                        var vd = createViewData(app, rr);

                        sw.WriteLine(vd.CreateCsvLine());
                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                if (!ViewerData.CreateInfo(fileName, Insurer.CurrrentInsurer.InsurerName, cym, receCount))
                    return false;

                //突合なしデータ一覧を出力
                wf.LogPrint("突合なしデータを抽出しています");
                string notMatchCSV = dir + $"\\突合なし広域データ{cym}.csv";
                var notList = KoikiData.GetNotMatchDatas(cym);
                using (var sw = new StreamWriter(notMatchCSV, false, Encoding.UTF8))
                {
                    sw.WriteLine("\"被保険者番号\",\"被保険者氏名\"," +
                        "\"被保険者氏名（カナ）\",\"郵便番号\",\"住所\",\"送付先郵便番号\"," +
                        "\"送付先住所\",\"送付先氏名\",\"医療機関番号\",\"医療機関名（漢字）\"," +
                        "\"代表者名（漢字）\",\"診療年月\",\"請求年月\",\"診療実日数\",\"費用額\"," +
                        "\"電算管理番号\"");

                    foreach (var item in notList) createNotMatchCsv(item, sw);
                }

                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nマッチングなしデータ数 :{notList.Count()}" +
                    $"\r\nビューアデータ出力数   :{receCount}";
                log += lastMsg;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                using (var sw = new StreamWriter(logFn, false, Encoding.UTF8)) sw.Write(log);
                wf.Dispose();
            }
            return true;
        }

        private static ViewerData createViewData(App app, KoikiData rr)
        {
            var vd = new ViewerData();

            vd.AID = app.Aid;
            vd.RrID = rr.Did;
            vd.CYM = app.CYM;
            vd.YM = app.YM;
            vd.AppType = app.AppType;
            vd.InsNum = rr.InsNum;
            vd.Num = rr.Num;
            vd.Name = rr.Name;
            vd.Kana = rr.Kana;
            vd.Sex = SEX.Null;
            vd.Birth = 0;
            vd.ShokenDate = 0;
            vd.StartDate = 0;
            vd.Family = 0;
            if (app.AppType == APP_TYPE.鍼灸)
            {
                var fsFunc = new Func<string, string>(s =>
                {
                    int hid;
                    int.TryParse(s, out hid);
                    return (0 < hid && hid < 8) ? ((HARI_BYOMEI)hid).ToString() : s;
                });
                vd.Fusho1 = fsFunc(app.FushoName1);
                vd.Fusho2 = fsFunc(app.FushoName2);
                vd.Fusho3 = fsFunc(app.FushoName3);
                vd.Fusho4 = fsFunc(app.FushoName4);
                vd.Fusho5 = fsFunc(app.FushoName5);
            }
            else
            {
                vd.Fusho1 = app.FushoName1;
                vd.Fusho2 = app.FushoName2;
                vd.Fusho3 = app.FushoName3;
                vd.Fusho4 = app.FushoName4;
                vd.Fusho5 = app.FushoName5;
            }

            if (app.AppType == APP_TYPE.あんま)
            {
                int bc = app.Bui / 100000;
                if (bc == 5)
                {
                    vd.AnmaBody = true;
                    vd.AnmaRightUpper = true;
                    vd.AnmaLeftUpper = true;
                    vd.AnmaRightLower = true;
                    vd.AnmaLeftLower = true;
                }
                else
                {
                    vd.AnmaBody = app.Bui / 10000 % 10 == 1;
                    vd.AnmaRightUpper = app.Bui / 1000 % 10 == 1;
                    vd.AnmaLeftUpper = app.Bui / 100 % 10 == 1;
                    vd.AnmaRightLower = app.Bui / 10 % 10 == 1;
                    vd.AnmaLeftLower = app.Bui % 10 == 1;
                }
            }
            else
            {
                vd.AnmaBody = false;
                vd.AnmaRightUpper = false;
                vd.AnmaLeftUpper = false;
                vd.AnmaRightLower = false;
                vd.AnmaLeftLower = false;
            }
            vd.AnmaCount = (vd.AnmaBody ? 1 : 0) +
                (vd.AnmaRightUpper ? 1 : 0) +
                (vd.AnmaLeftUpper ? 1 : 0) +
                (vd.AnmaRightLower ? 1 : 0) +
                (vd.AnmaLeftLower ? 1 : 0);

            vd.NewCont = app.NewContType;
            vd.Total = rr.Total;
            vd.Ratio = rr.Ratio / 10;
            vd.Partial = 0;
            vd.Charge = 0;
            vd.Days = rr.Days;
            vd.VisitFee = app.Distance == 999;
            vd.DrNum = rr.DrNum;
            vd.DrName = string.Empty;
            vd.ClinicNum = string.Empty;
            vd.ClinicName = string.Empty;
            vd.GroupNum = string.Empty;
            vd.GroupName = string.Empty;
            vd.Zip = rr.Zip;
            vd.Adds = rr.Add;
            vd.DestName = rr.DestName;
            vd.DestKana = rr.DestKana;
            vd.DestZip = rr.DestZip;
            vd.DestAdds = rr.DestAdd;
            vd.Numbering = string.Empty;
            vd.ComNum = string.Empty;
            vd.ImageFile = app.Aid.ToString() + ".tif";

            return vd;
        }

        private static bool createNotMatchCsv(KoikiData k, StreamWriter sw)
        {
            var sl = new string[16];

            sl[0] = k.Num;
            sl[1] = k.Name;
            sl[2] = k.Kana;
            sl[3] = k.Zip;
            sl[4] = k.Add;
            sl[5] = k.DestZip;
            sl[6] = k.DestAdd;
            sl[7] = k.DestName;
            sl[8] = k.DrNum;
            sl[9] = string.Empty;
            sl[10] = string.Empty;
            sl[11] = k.MediYM.ToString("000000");
            sl[12] = k.AppYM.ToString("000000");
            sl[13] = k.Days.ToString();
            sl[14] = k.Total.ToString();
            sl[15] = k.ReceNum;

            try
            {
                sw.WriteLine("\"" + string.Join("\",\"", sl) + "\"");
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\nCSVデータの書き込みに失敗しました");
                return false;
            }

            return true;
        }
    }
}
