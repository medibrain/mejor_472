﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.NaraKoiki
{
    class RefRece
    {
        [DB.DbAttribute.Serial]
        [DB.DbAttribute.PrimaryKey]
        public int RrID { get; set; }
        public int ImportID { get; set; }
        public int AID { get; set; }
        public int YM { get; set; }
        public int CYM { get; set; }
        public string InsNum { get; set; } = string.Empty;
        public string Num { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Kana { get; set; } = string.Empty;
        public int AppType { get; set; }
        public DateTime StartDate { get; set; } = DateTimeEx.DateTimeNull;
        public int Days { get; set; }
        public int Total { get; set; }
        public int Ratio { get; set; }
        public int Charge { get; set; }
        public int Partial { get; set; }
        public string DrNum { get; set; } = string.Empty;
        public string ComNum { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Add { get; set; } = string.Empty;
        public string DestName { get; set; } = string.Empty;
        public string DestKana { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdd { get; set; } = string.Empty;

        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        [DB.DbAttribute.Ignore]
        public int MediHYM => DateTimeEx.GetHsYearFromAd(YM / 100, YM % 100) * 100 + YM % 100;
        [DB.DbAttribute.Ignore]
        public int MediHY => DateTimeEx.GetHsYearFromAd(YM / 100, YM % 100);
        /*
        [DB.DbAttribute.Ignore]
        public int MediHYM => DateTimeEx.GetHsYearFromAd(YM / 100) * 100 + YM % 100;
        [DB.DbAttribute.Ignore]
        public int MediHY => DateTimeEx.GetHsYearFromAd(YM / 100);
        */
        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

        [DB.DbAttribute.Ignore]
        public int MediM => YM % 100;

        public bool Insert(DB.Transaction tran)
        {
            return DB.Main.Insert(this, tran);
        }

        public static bool Import(string fileName, int ady, int m)
        {
            using (var wf = new WaitForm())
            {
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.LogPrint("インポートを開始します");

                wf.LogPrint("CSVファイルを読み込み中です…");
                var l = CommonTool.CsvImportUTF8(fileName);

                wf.LogPrint("データベースへ書き込み中です…");
                wf.SetMax(l.Count - 1);

                using (var tran = DB.Main.CreateTransaction())
                {
                    try
                    {
                        //取込開始 ヘッダなし
                        for (int i = 0; i < l.Count; i++)
                        {
                            wf.InvokeValue = i;
                            if (l[i].Length < 11) continue;

                            int mym, atype, days, total, ratio, seikyu, futan;
                            int.TryParse(l[i][6], out mym);
                            int.TryParse(l[i][3], out atype);
                            int.TryParse(l[i][8], out days);
                            int.TryParse(l[i][9], out ratio);
                            int.TryParse(l[i][10], out total);
                            int.TryParse(l[i][11], out seikyu);
                            int.TryParse(l[i][12], out futan);
                            mym = DateTimeEx.GetAdYearMonthFromJyymm(mym);
                            DateTime sdt = DateTimeEx.GetDateFromJstr7(l[i][7].Trim());

                            var rr = new RefRece();
                            rr.CYM = ady * 100 + m;
                            rr.YM = mym;
                            rr.InsNum = l[i][0].Trim();
                            rr.Num = l[i][1].Trim();
                            rr.Name = l[i][13].Trim();
                            rr.Kana = l[i][14].Trim();
                            rr.AppType = atype;
                            rr.StartDate = sdt;
                            rr.Days = days;
                            rr.Total = total;
                            rr.Ratio = ratio;
                            rr.Charge = seikyu;
                            rr.Partial = futan;
                            rr.DrNum = l[i][2] + l[i][4];
                            rr.ComNum = l[i][5].Trim();
                            rr.Zip = l[i][16].Trim();
                            rr.Add = l[i][17].Trim() + l[i][18].Trim() + l[i][19].Trim();
                            rr.DestName = l[i][24].Trim();
                            rr.DestKana = l[i][25].Trim();
                            rr.DestZip = l[i][20].Trim();
                            rr.DestAdd = l[i][21].Trim() + l[i][22].Trim() + l[i][23].Trim();
                            rr.AID = 0;

                            if (!rr.Insert(tran)) return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                        tran.Rollback();
                        return false;
                    }

                    tran.Commit();
                }

                System.Windows.Forms.MessageBox.Show("インポートが完了しました");
                return true;
            }
        }

        public static List<RefRece> SerchByInput(int cym, int ym, string hnum, int total)
        {
            var l = DB.Main.Select<RefRece>(new { cym = cym, ym = ym });
            return l.ToList();
        }

        /// <summary>
        /// koikidataテーブル中のdidに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int rrid, int aid, DB.Transaction tran = null)
        {
            bool needCommit = false;
            if (tran == null)
            {
                tran = DB.Main.CreateTransaction();
                needCommit = true;
            }

            var delSql = $"UPDATE refrece SET aid=0 WHERE rrid={rrid};";
            var insSql = $"UPDATE refrece SET aid={aid} WHERE rrid={rrid};";

            try
            {
                var res = DB.Main.Excute(delSql, tran);
                res &= DB.Main.Excute(insSql, tran);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                if (needCommit) tran.Rollback();
                return false;
            }

            if (needCommit) tran.Commit();
            return true;
        }

        /// <summary>
        /// koikidataテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            var sql = $"UPDATE refrece SET aid=0 WHERE aid={aid};";
            return DB.Main.Excute(sql, tran);
        }

        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        public static List<RefRece> GetNotMatchDatas(int cym)
        {
            return DB.Main.Select<RefRece>($"cym={cym} AND aid=0").ToList();
        }

        public static RefRece Select(int rrid)
        {
            return DB.Main.Select<RefRece>(new { rrid = rrid }).FirstOrDefault();
        }

        public static List<RefRece> SelectAym(int cym)
        {
            return DB.Main.Select<RefRece>(new { cym= cym }).ToList();
        }

        public static List<RefRece> SelectAll()
        {
            return DB.Main.SelectAll<RefRece>().ToList();
        }

    }
}
