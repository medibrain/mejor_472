﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.NaraKoiki
{
    public partial class OtherForm : Form
    {
        int cym;
        public OtherForm(int cym)
        {
            InitializeComponent();
            this.cym = cym;
        }

        private void buttonNothing_Click(object sender, EventArgs e)
        {
            var l = AhakiMatching.GetNothingMatch(cym);
            if (l.Count == 0)
            {
                MessageBox.Show("すべての申請書は関連付けが終了しています");
            }
            else
            {
                MessageBox.Show($"広域データが関連付けられていない申請書が{l.Count}枚あります");
            }
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            using (var f = new InputForm(cym))
            {
                f.ShowDialog();
            }
        }

        private void buttonAuto_Click(object sender, EventArgs e)
        {
            if (AhakiMatching.AutoMatching(cym))
            {
                MessageBox.Show("自動マッチング処理が完了しました");
            }
            else
            {
                MessageBox.Show("自動マッチングに失敗しました");
            }
        }
    }
}
