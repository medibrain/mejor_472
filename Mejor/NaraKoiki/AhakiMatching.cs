﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.NaraKoiki
{
    class AhakiMatching
    {
        public static List<App> GetNothingMatch(int cym)
        {
            var where = $"WHERE a.cym={cym} " +
                "AND a.aapptype in (7,8) AND a.ayear>0 AND a.numbering=''";
            return App.GetAppsWithWhere(where);
        }

        public static bool AutoMatching(int cym)
        {
            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("マッチングがないあはきデータを取得しています");
                var l = GetNothingMatch(cym);

                wf.LogPrint($"マッチングがない{l.Count}件のあはきデータを取得しました");
                wf.SetMax(l.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.LogPrint($"自動マッチングを開始します");

                int matchCount = 0;
                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        if (wf.Cancel)
                        {
                            var res = System.Windows.Forms.MessageBox.Show("キャンセルししてもよろしいですか？",
                                "キャンセル確認", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                            if (res == System.Windows.Forms.DialogResult.Yes)
                            {
                                tran.Rollback();
                                return false;
                            }

                            wf.Cancel = false;
                        }

                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                        int appAdY = DateTimeEx.GetAdYearFromHs(item.ChargeYear * 100 + item.ChargeMonth);

                        //20190515143854 furukawa st ////////////////////////
                        //引数計算間違い                        
                        int adY = DateTimeEx.GetAdYearFromHs(item.MediYear * 100 + item.MediMonth);
                            //int adY = DateTimeEx.GetAdYearFromHs(item.MediYear * 100 * item.MediMonth);
                        //20190515143854 furukawa ed ////////////////////////


                                //int appAdY = DateTimeEx.GetAdYearFromHs(item.ChargeYear);
                                //int adY = DateTimeEx.GetAdYearFromHs(item.MediYear);
                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                        var kList = KoikiData.SerchByInput(appAdY, item.ChargeMonth, adY, item.MediMonth, item.HihoNum, item.Total);
                        if (kList?.Count == 1 && kList[0].AppType != 9)
                        {
                            var k = kList[0];
                            item.InsNum = k.InsNum;
                            item.PersonName = k.Name;
                            item.DrNum = k.DrNum;
                            item.CountedDays = k.Days;
                            item.FushoFirstDate1 = k.StartDate;
                            item.Charge = k.Seikyu;
                            item.Partial = k.Futan;
                            item.HihoZip = k.Zip;
                            item.HihoAdd = k.Add;
                            item.TaggedDatas.DestName = k.DestName;
                            item.TaggedDatas.DestZip = k.DestZip;
                            item.TaggedDatas.DestAdd = k.DestAdd;
                            item.Numbering = k.Did.ToString();
                            item.RrID = k.Did;
                            item.ComNum = k.ReceNum;
                            
                            //データ更新
                            var up = item.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.SecondInput, tran);
                            up &= KoikiData.AidUpdate(k.Did, item.Aid, tran);
                            if (!up)
                            {
                                tran.Rollback();
                                return false;
                            }

                            matchCount++;
                        }
                        wf.InvokeValue++;
                    }

                    tran.Commit();
                    System.Windows.Forms.MessageBox.Show($"{matchCount}件のマッチングを行ないました");
                    return true;
                }
            }
        }
    }
}
