﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Mejor.Musashino
{
            
    class CheckInputAall
    {
        static Regex re = new Regex(@"[^0-9]");

        public App app { get; private set; }
        public bool ErrAyear { get; private set; }
        public bool ErrAmonth { get; private set; }
        public bool ErrHnum { get; private set; }
        public bool ErrEra { get; private set; }
        public bool ErrBY { get; private set; }
        public bool ErrBM { get; private set; }
        public bool ErrBD { get; private set; }

        public bool ErrFusho { get; private set; }
        public bool ErrShokenY { get; private set; }
        public bool ErrShokenM { get; private set; }
        public bool ErrShokenD { get; private set; }
        public bool ErrStart { get; private set; }
        public bool ErrEnd { get; private set; }
        public bool ErrAcountedDays { get; private set; }
        public bool ErrAtotal { get; private set; }
        public bool ErrAcharge { get; private set; }
        public bool ErrDrCode { get; private set; }

        public bool ErrBirth
        {
            get
            {
                return ErrEra || ErrBY || ErrBM || ErrBD;
            }
        }

        public bool ErrShokenDate
        {
            get
            {
                return ErrShokenY || ErrShokenM || ErrShokenD;
            }
        }

        public CheckInputAall(App app)
        {
            this.app = app;
            var ocr = app.OcrData.Split(',');
            if (ocr.Length < 200)
            {
                ErrAyear = true;
                ErrAmonth = true;
                ErrHnum = true;
                ErrEra = true;
                ErrBY = true;
                ErrBM = true;
                ErrBD = true;

                ErrFusho = true;
                ErrShokenY = true;
                ErrShokenM = true;
                ErrShokenD = true;
                ErrStart = true;
                ErrEnd = true;
                ErrAcountedDays = true;
                ErrAtotal = true;
                ErrAcharge = true;
                ErrDrCode = true;

                return;
            }

            ErrAyear = false;
            ErrAmonth = false;
            ErrHnum = false;
            ErrEra = false;
            ErrBY = false;
            ErrBM = false;
            ErrBD = false;

            ErrFusho = false;
            ErrShokenY = false;
            ErrShokenM = false;
            ErrShokenD = false;
            ErrStart = false;
            ErrEnd = false;
            ErrAcountedDays = false;
            ErrAtotal = false;
            ErrAcharge = false;
            ErrDrCode = false;

            if (ocr.Length < 50) return;
            int temp = 0;

            //被保険者番号
            var ahn = re.Replace(ocr[7], "");
            if (ahn.Length < 7)
            {
                ErrHnum = true;
            }
            else
            {
                ahn = ahn.Substring(ahn.Length - 6, 6);
                ahn = ahn.Remove(2) + "-" + ahn.Substring(2);
                if (ahn != app.HihoNum) ErrHnum = true;
            }

            //生年月日 年号
            string era = "";
            if (ocr[32] != "0") era = "明治";
            if (ocr[33] != "0") era += "大正";
            if (ocr[34] != "0") era += "昭和";
            if (ocr[35] != "0") era += "平成";
            if (DateTimeEx.GetEra(app.Birthday) != era) 
                ErrEra = true;


            //診療年
            int.TryParse(re.Replace(ocr[3], ""), out temp);
            if (temp == 0 || app.MediYear != temp)
                ErrAyear = true;

            //診療月
            int.TryParse(re.Replace(ocr[4], ""), out temp);
            if (temp == 0 || app.MediMonth != temp)
                ErrAmonth = true;

            //生年月日 年
            int.TryParse(re.Replace(ocr[36], ""), out temp);
            if (temp == 0 || DateTimeEx.GetJpYear(app.Birthday) != temp)
                ErrBY = true;

            //生年月日 月
            int.TryParse(re.Replace(ocr[37], ""), out temp);
            if (temp == 0 || app.Birthday.Month != temp)
                ErrBM = true;

            //生年月日 日
            int.TryParse(re.Replace(ocr[38], ""), out temp);
            if (temp == 0 || app.Birthday.Day != temp)
                ErrBD = true;

            //負傷名
            var f = Fusho.Change(ocr[40]);
            if (app.FushoName1 != f) ErrFusho = true;

            //初検日 年
            int.TryParse(re.Replace(ocr[44], ""), out temp);
            if (temp == 0 || DateTimeEx.GetJpYear(app.FushoFirstDate1) != temp)
                ErrShokenY = true;

            //初検日 月
            int.TryParse(re.Replace(ocr[45], ""), out temp);
            if (temp == 0 || app.FushoFirstDate1.Month != temp)
                ErrShokenM = true;

            //初検日 日
            int.TryParse(re.Replace(ocr[46], ""), out temp);
            if (temp == 0 || app.FushoFirstDate1.Day != temp)
                ErrShokenD = true;

            //開始
            int.TryParse(re.Replace(ocr[49], ""), out temp);
            if (temp == 0 || app.FushoStartDate1.Day != temp)
                ErrStart = true;

            //終了
            int.TryParse(re.Replace(ocr[52], ""), out temp);
            if (temp == 0 || app.FushoFinishDate1.Day != temp)
                ErrEnd = true;

            //実日数
            var idays = new List<int>();
            int iday;
            string sday;
            sday = re.Replace(ocr[53], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[70], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[87], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[104], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            sday = re.Replace(ocr[121], "");
            int.TryParse(sday, out iday);
            idays.Add(iday);
            iday = idays.Max();
            if (app.CountedDays != iday)
                ErrAcountedDays = true;

            //合計
            int.TryParse(re.Replace(ocr[193], ""), out temp);
            if (temp == 0 || app.Total != temp)
                ErrAtotal = true;

            //請求
            int.TryParse(re.Replace(ocr[195], ""), out temp);
            if (temp == 0 || app.Charge != temp)
                ErrAcharge = true;
        }
    }
}
