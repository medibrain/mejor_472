﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.Musashino
{
    class Doctor
    {
        public string Code { get; private set; }
        public string DrName { get; private set; }
        public string HosName { get; private set; }
        public string GroupName { get; private set; }
        public bool Verified { get; private set; }
        public int LastAID { get; private set; }

        private Doctor(string code)
        {
            this.Code = code;
            this.Verified = false;
        }

        public Doctor(App app)
        {
            this.Code = app.DrNum;
            this.DrName = app.DrName;
            this.HosName = app.ClinicName;
            this.GroupName = app.AccountName;
            this.LastAID = app.Aid;
            this.Verified = false;
        }

        /// <summary>
        /// 施術師情報を比較します。
        /// 施術師コードが違うものを比較した場合、例外が発生します。
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public bool IsEqual(App app)
        {
            if (this.Code != app.DrNum) throw new Exception("施術師Noが違う情報を比較しようとしました");
            return this.DrName == app.DrName &&
            this.HosName == app.ClinicName &&
            this.GroupName == app.AccountName;
        }

        public static Doctor GetDoctor(string code)
        {
            var sql = "SELECT code, drname, hosname, groupname, verified " +
                "FROM doctor " +
                "WHERE code=:code";
            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("code", NpgsqlDbType.Text).Value = code;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var d = new Doctor(code);
                d.DrName = (string)res[0][1];
                d.HosName = (string)res[0][2];
                d.GroupName = (string)res[0][3];
                d.Verified = (bool)res[0][4];

                return d;
            }
        }

        public bool Update(App app, bool forceVerify)
        {
            if (this.Code != app.DrNum) throw new Exception("施術師Noが違う情報を更新しようとしました");

            if (IsEqual(app))
            {
                if (this.Verified) return true;

                //情報が一致し、情報元aidが違う場合にベリファイ済みとみなす
                this.Verified = forceVerify ? true : this.LastAID != app.Aid;
                this.LastAID = app.Aid;
            }
            else
            {
                this.DrName = app.DrName;
                this.HosName = app.ClinicName;
                this.GroupName = app.AccountName;
                this.LastAID = app.Aid;
                this.Verified = forceVerify ? true : false;
            }

            var sql = "UPDATE doctor SET " +
                "drname=:drname, hosname=:hosname, groupname=:groupname, " +
                "verified=:verified, update_uid=:upuid, lastaid=:aid " +
                "WHERE code = :code";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("drname", NpgsqlDbType.Text).Value = this.DrName;
                cmd.Parameters.Add("hosname", NpgsqlDbType.Text).Value = this.HosName;
                cmd.Parameters.Add("groupname", NpgsqlDbType.Text).Value = this.GroupName;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("upuid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.LastAID;
                cmd.Parameters.Add("code", NpgsqlDbType.Text).Value = this.Code;

                return cmd.TryExecuteNonQuery();
            }
        }

        public bool Insert()
        {
            var sql = "INSERT INTO doctor " +
                "(code, drname, hosname, groupname, verified, insert_uid) " +
                "VALUES (:code, :drname, :hosname, :groupname, :verified, :inuid)";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("code", NpgsqlDbType.Text).Value = this.Code;
                cmd.Parameters.Add("drname", NpgsqlDbType.Text).Value = this.DrName;
                cmd.Parameters.Add("hosname", NpgsqlDbType.Text).Value = this.HosName;
                cmd.Parameters.Add("groupname", NpgsqlDbType.Text).Value = this.GroupName;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("inuid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;

                return cmd.TryExecuteNonQuery();
            }
        }
    }
}
