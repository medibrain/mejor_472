﻿namespace Mejor.Musashino
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verifyBoxY = new VerifyBox();
            this.verifyBoxM = new VerifyBox();
            this.verifyBoxHnum = new VerifyBox();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonBack = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.verifyBoxHosName = new VerifyBox();
            this.label32 = new System.Windows.Forms.Label();
            this.verifyBoxGroupName = new VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxDrName = new VerifyBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.verifyBoxEnd = new VerifyBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.verifyBoxStart = new VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.verifyBoxShokenY = new VerifyBox();
            this.label24 = new System.Windows.Forms.Label();
            this.verifyBoxShokenM = new VerifyBox();
            this.verifyBoxShokenD = new VerifyBox();
            this.label25 = new System.Windows.Forms.Label();
            this.verifyBoxBuiCount = new VerifyBox();
            this.label21 = new System.Windows.Forms.Label();
            this.labelNumbering = new System.Windows.Forms.Label();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.verifyBoxDrCode = new VerifyBox();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.verifyBoxNewCont = new VerifyBox();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.verifyBoxDays = new VerifyBox();
            this.verifyBoxTotal = new VerifyBox();
            this.labelImageName = new System.Windows.Forms.Label();
            this.verifyBoxNumbering = new VerifyBox();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxFusho = new VerifyBox();
            this.verifyBoxCharge = new VerifyBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.verifyBoxName = new VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.verifyBoxFname = new VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.verifyBoxHnumM = new VerifyBox();
            this.verifyBoxBirthday = new VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.verifyBoxSex = new VerifyBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.verifyBoxBY = new VerifyBox();
            this.label17 = new System.Windows.Forms.Label();
            this.verifyBoxBM = new VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxBD = new VerifyBox();
            this.label16 = new System.Windows.Forms.Label();
            this.verifyBoxRatio = new VerifyBox();
            this.panelRight.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxY.Location = new System.Drawing.Point(93, 7);
            this.verifyBoxY.MaxLength = 2;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            this.verifyBoxY.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxM.Location = new System.Drawing.Point(143, 7);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxM.TabIndex = 5;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(322, 7);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.Size = new System.Drawing.Size(110, 26);
            this.verifyBoxHnum.TabIndex = 15;
            this.verifyBoxHnum.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            this.verifyBoxHnum.Leave += new System.EventHandler(this.verifyBoxHnum_Leave);
            // 
            // buttonRegist
            // 
            this.buttonRegist.Location = new System.Drawing.Point(992, 157);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 58;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(126, 21);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 4;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(176, 21);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 7;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(230, 9);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(53, 24);
            this.labelHnum.TabIndex = 12;
            this.labelHnum.Text = "被保険者\r\n番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(64, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(18, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.panel2);
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(221, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1123, 722);
            this.panelRight.TabIndex = 2;
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 126);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1123, 412);
            this.scrollPictureControl1.TabIndex = 3;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonBack);
            this.panel2.Controls.Add(this.label33);
            this.panel2.Controls.Add(this.verifyBoxHosName);
            this.panel2.Controls.Add(this.label32);
            this.panel2.Controls.Add(this.verifyBoxGroupName);
            this.panel2.Controls.Add(this.label31);
            this.panel2.Controls.Add(this.verifyBoxDrName);
            this.panel2.Controls.Add(this.label30);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.verifyBoxEnd);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.verifyBoxStart);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.verifyBoxShokenY);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.verifyBoxShokenM);
            this.panel2.Controls.Add(this.verifyBoxShokenD);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.verifyBoxBuiCount);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.labelNumbering);
            this.panel2.Controls.Add(this.buttonImageChange);
            this.panel2.Controls.Add(this.buttonRegist);
            this.panel2.Controls.Add(this.verifyBoxDrCode);
            this.panel2.Controls.Add(this.buttonImageRotateL);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.verifyBoxNewCont);
            this.panel2.Controls.Add(this.buttonImageRotateR);
            this.panel2.Controls.Add(this.verifyBoxDays);
            this.panel2.Controls.Add(this.verifyBoxTotal);
            this.panel2.Controls.Add(this.labelImageName);
            this.panel2.Controls.Add(this.verifyBoxNumbering);
            this.panel2.Controls.Add(this.labelDays);
            this.panel2.Controls.Add(this.verifyBoxFusho);
            this.panel2.Controls.Add(this.verifyBoxCharge);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.labelInputerName);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.labelCharge);
            this.panel2.Controls.Add(this.labelTotal);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.panel2.Location = new System.Drawing.Point(0, 538);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1123, 184);
            this.panel2.TabIndex = 2;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(902, 157);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 59;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(371, 77);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 24);
            this.label33.TabIndex = 43;
            this.label33.Text = "施術\r\n所名";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHosName
            // 
            this.verifyBoxHosName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHosName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHosName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHosName.Location = new System.Drawing.Point(400, 75);
            this.verifyBoxHosName.Name = "verifyBoxHosName";
            this.verifyBoxHosName.Size = new System.Drawing.Size(238, 26);
            this.verifyBoxHosName.TabIndex = 44;
            this.verifyBoxHosName.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(644, 89);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 12);
            this.label32.TabIndex = 46;
            this.label32.Text = "団体名";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxGroupName
            // 
            this.verifyBoxGroupName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxGroupName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxGroupName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxGroupName.Location = new System.Drawing.Point(686, 75);
            this.verifyBoxGroupName.Name = "verifyBoxGroupName";
            this.verifyBoxGroupName.Size = new System.Drawing.Size(301, 26);
            this.verifyBoxGroupName.TabIndex = 47;
            this.verifyBoxGroupName.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(176, 77);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 24);
            this.label31.TabIndex = 40;
            this.label31.Text = "施術\r\n師名";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxDrName
            // 
            this.verifyBoxDrName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxDrName.Location = new System.Drawing.Point(205, 75);
            this.verifyBoxDrName.Name = "verifyBoxDrName";
            this.verifyBoxDrName.Size = new System.Drawing.Size(160, 26);
            this.verifyBoxDrName.TabIndex = 41;
            this.verifyBoxDrName.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(589, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 20;
            this.label30.Text = "終了日";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(493, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 16;
            this.label29.Text = "開始日";
            // 
            // verifyBoxEnd
            // 
            this.verifyBoxEnd.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxEnd.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxEnd.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxEnd.Location = new System.Drawing.Point(630, 7);
            this.verifyBoxEnd.MaxLength = 2;
            this.verifyBoxEnd.Name = "verifyBoxEnd";
            this.verifyBoxEnd.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxEnd.TabIndex = 21;
            this.verifyBoxEnd.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(662, 21);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 23;
            this.label28.Text = "日";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label13.Location = new System.Drawing.Point(811, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 24);
            this.label13.TabIndex = 30;
            this.label13.Text = "新規：1\r\n継続：2";
            // 
            // verifyBoxStart
            // 
            this.verifyBoxStart.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxStart.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxStart.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxStart.Location = new System.Drawing.Point(534, 7);
            this.verifyBoxStart.MaxLength = 2;
            this.verifyBoxStart.Name = "verifyBoxStart";
            this.verifyBoxStart.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxStart.TabIndex = 17;
            this.verifyBoxStart.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(566, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 12);
            this.label27.TabIndex = 19;
            this.label27.Text = "日";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(299, 21);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 12);
            this.label26.TabIndex = 6;
            this.label26.Text = "初検日";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(372, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 9;
            this.label23.Text = "年";
            // 
            // verifyBoxShokenY
            // 
            this.verifyBoxShokenY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxShokenY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxShokenY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxShokenY.Location = new System.Drawing.Point(340, 7);
            this.verifyBoxShokenY.MaxLength = 2;
            this.verifyBoxShokenY.Name = "verifyBoxShokenY";
            this.verifyBoxShokenY.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxShokenY.TabIndex = 7;
            this.verifyBoxShokenY.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(421, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 12;
            this.label24.Text = "月";
            // 
            // verifyBoxShokenM
            // 
            this.verifyBoxShokenM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxShokenM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxShokenM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxShokenM.Location = new System.Drawing.Point(389, 7);
            this.verifyBoxShokenM.MaxLength = 2;
            this.verifyBoxShokenM.Name = "verifyBoxShokenM";
            this.verifyBoxShokenM.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxShokenM.TabIndex = 10;
            this.verifyBoxShokenM.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // verifyBoxShokenD
            // 
            this.verifyBoxShokenD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxShokenD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxShokenD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxShokenD.Location = new System.Drawing.Point(438, 7);
            this.verifyBoxShokenD.MaxLength = 2;
            this.verifyBoxShokenD.Name = "verifyBoxShokenD";
            this.verifyBoxShokenD.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxShokenD.TabIndex = 13;
            this.verifyBoxShokenD.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(470, 21);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 15;
            this.label25.Text = "日";
            // 
            // verifyBoxBuiCount
            // 
            this.verifyBoxBuiCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBuiCount.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBuiCount.Location = new System.Drawing.Point(59, 7);
            this.verifyBoxBuiCount.MaxLength = 2;
            this.verifyBoxBuiCount.Name = "verifyBoxBuiCount";
            this.verifyBoxBuiCount.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBuiCount.TabIndex = 1;
            this.verifyBoxBuiCount.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 0;
            this.label21.Text = "部位数";
            // 
            // labelNumbering
            // 
            this.labelNumbering.AutoSize = true;
            this.labelNumbering.Location = new System.Drawing.Point(18, 75);
            this.labelNumbering.Name = "labelNumbering";
            this.labelNumbering.Size = new System.Drawing.Size(41, 24);
            this.labelNumbering.TabIndex = 37;
            this.labelNumbering.Text = "施術師\r\nコード";
            this.labelNumbering.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Location = new System.Drawing.Point(93, 157);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 54;
            this.buttonImageChange.TabStop = false;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Visible = false;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.Location = new System.Drawing.Point(60, 73);
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.Size = new System.Drawing.Size(110, 26);
            this.verifyBoxDrCode.TabIndex = 38;
            this.verifyBoxDrCode.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            this.verifyBoxDrCode.Leave += new System.EventHandler(this.verifyBoxDrCode_Leave);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(20, 157);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 52;
            this.buttonImageRotateL.TabStop = false;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(754, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 24);
            this.label12.TabIndex = 27;
            this.label12.Text = "新規\r\n継続";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(783, 7);
            this.verifyBoxNewCont.MaxLength = 1;
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxNewCont.TabIndex = 28;
            this.verifyBoxNewCont.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(56, 157);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 53;
            this.buttonImageRotateR.TabStop = false;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(716, 7);
            this.verifyBoxDays.MaxLength = 2;
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxDays.TabIndex = 25;
            this.verifyBoxDays.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(886, 9);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.Size = new System.Drawing.Size(80, 26);
            this.verifyBoxTotal.TabIndex = 32;
            this.verifyBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTotal.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // labelImageName
            // 
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(18, 142);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 55;
            this.labelImageName.Text = "ImageName";
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.Location = new System.Drawing.Point(1022, 75);
            this.verifyBoxNumbering.MaxLength = 4;
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.Size = new System.Drawing.Size(60, 26);
            this.verifyBoxNumbering.TabIndex = 50;
            this.verifyBoxNumbering.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxNumbering.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(685, 9);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(29, 24);
            this.labelDays.TabIndex = 24;
            this.labelDays.Text = "診療\r\n日数";
            // 
            // verifyBoxFusho
            // 
            this.verifyBoxFusho.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFusho.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFusho.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxFusho.Location = new System.Drawing.Point(138, 7);
            this.verifyBoxFusho.Name = "verifyBoxFusho";
            this.verifyBoxFusho.Size = new System.Drawing.Size(155, 26);
            this.verifyBoxFusho.TabIndex = 4;
            this.verifyBoxFusho.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(1002, 7);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.Size = new System.Drawing.Size(80, 26);
            this.verifyBoxCharge.TabIndex = 35;
            this.verifyBoxCharge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxCharge.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(993, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 24);
            this.label6.TabIndex = 49;
            this.label6.Text = "整理\r\n番号";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(589, 163);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(298, 15);
            this.labelInputerName.TabIndex = 57;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(97, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 3;
            this.label22.Text = "負傷名";
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(972, 9);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(29, 24);
            this.labelCharge.TabIndex = 34;
            this.labelCharge.Text = "請求\r\n金額";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(858, 9);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(29, 24);
            this.labelTotal.TabIndex = 31;
            this.labelTotal.Text = "合計\r\n金額";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.verifyBoxName);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.verifyBoxFname);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.labelM);
            this.panel1.Controls.Add(this.verifyBoxM);
            this.panel1.Controls.Add(this.labelY);
            this.panel1.Controls.Add(this.verifyBoxY);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.labelHnum);
            this.panel1.Controls.Add(this.verifyBoxHnum);
            this.panel1.Controls.Add(this.labelBirthday);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.verifyBoxHnumM);
            this.panel1.Controls.Add(this.verifyBoxBirthday);
            this.panel1.Controls.Add(this.labelSex);
            this.panel1.Controls.Add(this.verifyBoxSex);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.verifyBoxBY);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.verifyBoxBM);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.verifyBoxBD);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.verifyBoxRatio);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1123, 126);
            this.panel1.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(393, 71);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 24);
            this.label20.TabIndex = 44;
            this.label20.Text = "療養者\r\n氏名";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxName
            // 
            this.verifyBoxName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxName.Location = new System.Drawing.Point(434, 69);
            this.verifyBoxName.Name = "verifyBoxName";
            this.verifyBoxName.Size = new System.Drawing.Size(120, 26);
            this.verifyBoxName.TabIndex = 45;
            this.verifyBoxName.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(520, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 24);
            this.label19.TabIndex = 20;
            this.label19.Text = "被保険者\r\n氏名";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxFname
            // 
            this.verifyBoxFname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFname.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxFname.Location = new System.Drawing.Point(573, 7);
            this.verifyBoxFname.Name = "verifyBoxFname";
            this.verifyBoxFname.Size = new System.Drawing.Size(120, 26);
            this.verifyBoxFname.TabIndex = 21;
            this.verifyBoxFname.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(147, 71);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(29, 24);
            this.labelBirthday.TabIndex = 27;
            this.labelBirthday.Text = "生年\r\n月日";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(203, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 24);
            this.label5.TabIndex = 30;
            this.label5.Text = "昭：3\r\n平：4";
            // 
            // verifyBoxHnumM
            // 
            this.verifyBoxHnumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumM.Location = new System.Drawing.Point(283, 7);
            this.verifyBoxHnumM.MaxLength = 2;
            this.verifyBoxHnumM.Name = "verifyBoxHnumM";
            this.verifyBoxHnumM.Size = new System.Drawing.Size(39, 26);
            this.verifyBoxHnumM.TabIndex = 13;
            this.verifyBoxHnumM.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // verifyBoxBirthday
            // 
            this.verifyBoxBirthday.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthday.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBirthday.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBirthday.Location = new System.Drawing.Point(177, 69);
            this.verifyBoxBirthday.Name = "verifyBoxBirthday";
            this.verifyBoxBirthday.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxBirthday.TabIndex = 28;
            this.verifyBoxBirthday.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(18, 83);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(29, 12);
            this.labelSex.TabIndex = 23;
            this.labelSex.Text = "性別";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(47, 69);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxSex.TabIndex = 24;
            this.verifyBoxSex.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(74, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 24);
            this.label4.TabIndex = 26;
            this.label4.Text = "男 : 1\r\n女 : 2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(264, 83);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 33;
            this.label18.Text = "年";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(232, 69);
            this.verifyBoxBY.MaxLength = 2;
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBY.TabIndex = 31;
            this.verifyBoxBY.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(313, 83);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 36;
            this.label17.Text = "月";
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(281, 69);
            this.verifyBoxBM.MaxLength = 2;
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBM.TabIndex = 34;
            this.verifyBoxBM.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(451, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 24);
            this.label15.TabIndex = 17;
            this.label15.Text = "給付\r\n割合";
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(330, 69);
            this.verifyBoxBD.MaxLength = 2;
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxBD.TabIndex = 37;
            this.verifyBoxBD.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            this.verifyBoxBD.Leave += new System.EventHandler(this.verifyBoxBD_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(362, 83);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 39;
            this.label16.Text = "日";
            // 
            // verifyBoxRatio
            // 
            this.verifyBoxRatio.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxRatio.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxRatio.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxRatio.Location = new System.Drawing.Point(480, 7);
            this.verifyBoxRatio.MaxLength = 1;
            this.verifyBoxRatio.Name = "verifyBoxRatio";
            this.verifyBoxRatio.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxRatio.TabIndex = 18;
            this.verifyBoxRatio.Enter += new System.EventHandler(this.verifyBoxs_Enter);
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 722);
            this.Controls.Add(this.panelRight);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.panelRight.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Panel panelRight;
        private VerifyBox verifyBoxSex;
        private System.Windows.Forms.Label labelSex;
        private VerifyBox verifyBoxCharge;
        private System.Windows.Forms.Label labelCharge;
        private VerifyBox verifyBoxTotal;
        private System.Windows.Forms.Label labelTotal;
        private VerifyBox verifyBoxBirthday;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelNumbering;
        private VerifyBox verifyBoxDrCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private System.Windows.Forms.Label labelDays;
        private VerifyBox verifyBoxDays;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private VerifyBox verifyBoxNewCont;
        private VerifyBox verifyBoxNumbering;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxRatio;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelImageName;
        private VerifyBox verifyBoxHnumM;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private VerifyBox verifyBoxEnd;
        private System.Windows.Forms.Label label28;
        private VerifyBox verifyBoxStart;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label23;
        private VerifyBox verifyBoxShokenY;
        private System.Windows.Forms.Label label24;
        private VerifyBox verifyBoxShokenM;
        private VerifyBox verifyBoxShokenD;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxBuiCount;
        private System.Windows.Forms.Label label21;
        private VerifyBox verifyBoxFusho;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private VerifyBox verifyBoxName;
        private System.Windows.Forms.Label label19;
        private VerifyBox verifyBoxFname;
        private System.Windows.Forms.Label label33;
        private VerifyBox verifyBoxHosName;
        private System.Windows.Forms.Label label32;
        private VerifyBox verifyBoxGroupName;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxDrName;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
    }
}