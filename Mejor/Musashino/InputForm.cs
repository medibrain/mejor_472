﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;
using System.Drawing.Imaging;

namespace Mejor.Musashino
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp;
        protected override Control inputPanel => panelRight;

        private int cYear, cMonth;

        public enum a2tp { 未チェック = 0, First = 1, Other = 2 };

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(0, 50);
        Point posPerson = new Point(0, 50);
        Point posFusho = new Point(0, 400);
        Point posTotal = new Point(0, 400);
        Point posDr = new Point(0, 1800);
        Point posNo = new Point(0, 1800);

        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            this.scanGroup = sGroup;
            cYear = scanGroup.cyear;
            cMonth = scanGroup.cmonth;
            this.firstTime = firstTime;

            //GIDで検索
            int gid = scanGroup.GroupID;
            var list = App.GetAppsGID(gid);
            
            //データリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = false;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            focusBack(false);
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //Nextボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        //キー入力監視
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowindex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);


            /*
            //年
            int year = verifyBoxY.GetIntValue();
            int adYear = DateTimeEx.GetAdYearFromHs(year);
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);
            */

            //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

            //被保険者番号
            int hnumS = verifyBoxHnumM.GetIntValue();
            setStatus(verifyBoxHnumM, hnumS < 1 || verifyBoxHnumM.Text.Trim().Length != 2);
            int hnumN = verifyBoxHnum.GetIntValue();
            setStatus(verifyBoxHnum, hnumN < 1 || verifyBoxHnum.Text.Trim().Length != 4);
            string hnum = verifyBoxHnumM.Text.Trim() + "-" + verifyBoxHnum.Text.Trim();

            //割合
            int ratio = verifyBoxRatio.GetIntValue();
            setStatus(verifyBoxRatio, ratio < 7 || 10 < ratio);

            //被保険者氏名
            setStatus(verifyBoxFname, verifyBoxFname.Text.Length < 3);

            //性別
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日
            var birthDt = dateCheck(verifyBoxBirthday, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //新規継続チェック
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //療養者氏名
            setStatus(verifyBoxName, verifyBoxName.Text.Length < 3);

            //部位数
            int buiCount = verifyBoxBuiCount.GetIntValue();
            setStatus(verifyBoxBuiCount, buiCount < 1 || 5 < buiCount);

            //負傷名
            fusho1Check(verifyBoxFusho);

            //初検日
            var firstDate = dateCheck(4, verifyBoxShokenY, verifyBoxShokenM, verifyBoxShokenD);

            //開始日
            var startDate = dateCheck(4, year, month, verifyBoxStart);

            //終了日
            var endDate = dateCheck(4, year, month, verifyBoxEnd);

            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 <total);

            //請求
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //施術師コード
            int drNo = verifyBoxDrCode.GetIntValue();
            setStatus(verifyBoxDrCode, drNo < 10000000 || 480000000 < drNo);

            //施術師名
            setStatus(verifyBoxDrName, verifyBoxDrName.Text.Length < 3);

            //施術所名
            setStatus(verifyBoxHosName, verifyBoxHosName.Text.Length < 3);

            //団体名
            setStatus(verifyBoxGroupName, verifyBoxGroupName.Text.Length < 3);

            //ナンバリング
            int numbering = verifyBoxNumbering.GetIntValue();
            setStatus(verifyBoxNumbering, numbering < 1 || 10000 < numbering);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあればいったん登録中断
            bool ratioError = (int)(total * ratio / 10) != charge;
            if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != charge;

            if (ratioError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = hnum;
            app.Ratio = ratio;
            app.HihoName = verifyBoxFname.Text.Trim();
            app.Sex = sex;

            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;
            app.PersonName = verifyBoxName.Text.Trim();
            app.Birthday = birthDt;

            app.FushoName1 = verifyBoxFusho.Text.Trim();
            app.FushoName2 = buiCount >= 2 ? "Exist" : string.Empty;
            app.FushoName3 = buiCount >= 3 ? "Exist" : string.Empty;
            app.FushoName4 = buiCount >= 4 ? "Exist" : string.Empty;
            app.FushoName5 = buiCount >= 5 ? "Exist" : string.Empty;

            app.FushoFirstDate1 = firstDate;
            app.FushoStartDate1 = startDate;
            app.FushoFinishDate1 = endDate;

            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;

            app.DrNum = verifyBoxDrCode.Text.Trim();
            app.DrName = verifyBoxDrName.Text.Trim();
            app.ClinicName = verifyBoxHosName.Text.Trim();
            app.AccountName = verifyBoxGroupName.Text.Trim();
            app.Numbering = verifyBoxNumbering.Text.Trim();
            app.AppType = scan.AppType;

            return true;
        }

        /// <summary>
        /// Applicationの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //その他
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }

                //被保険者情報の管理
                var p = Person.GetPerson(app.HihoNum, app.Birthday);
                if (p == null)
                {
                    p = new Person(app);
                    p.Insert();
                }
                else
                {
                    if (!p.IsEqual(app))
                    {
                        if (MessageBox.Show("過去に入力された被保険者データと一致しません。" +
                            "被保険者データを更新し、入力データを登録してもよろしいですか？",
                            "被保険者情報確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2)
                            != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    p.Update(app, !firstTime);
                }

                //施術師情報の管理
                var d = Doctor.GetDoctor(app.DrNum);
                if (d == null)
                {
                    d = new Doctor(app);
                    d.Insert();
                }
                else
                {
                    if (!d.IsEqual(app))
                    {
                        if (MessageBox.Show("過去に入力された施術師データと一致しません。" +
                            "施術師データを更新し、入力データを登録してもよろしいですか？",
                            "施術師情報確認",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button2)
                            != System.Windows.Forms.DialogResult.OK) return false;
                    }
                    d.Update(app, !firstTime);
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のApplicationを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:" + User.GetUserName(app.Ufirst) +
                "  2:" + User.GetUserName(app.Usecond);

            if (!firstTime)
            {
                //ベリファイ入力時
                var cai = new CheckInputAall(app);
                setVerify(cai);
            }
            else
            {
                //App_Flagのチェック
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    //既にチェック済みの画像はデータベースからデータ表示
                    setInputedApp(app);
                }
                else
                {
                    //一度もチェックしていない画像
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                        verifyBoxFusho.Text = Fusho.GetFusho1(ocr);
                    }
                }
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                //通常表示
                scrollPictureControl1.Ratio = 0.4f;
                scrollPictureControl1.SetPictureFile(fn);
                scrollPictureControl1.ScrollPosition = posYM;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        /// <param name="r"></param>
        private void setInputedApp(App r)
        {
            //OCRチェックが済んだ画像の場合
            if (r.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                verifyBoxY.Text = "--";
            }
            else if (r.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                verifyBoxY.Text = "++";
            }
            else
            {
                verifyBoxY.Text = r.MediYear.ToString();
                verifyBoxM.Text = r.MediMonth.ToString();
                
                var hn = r.HihoNum.Split('-');
                if (hn.Length == 2)
                {
                    verifyBoxHnumM.Text = hn[0];
                    verifyBoxHnum.Text = hn[1];
                }
                else
                {
                    verifyBoxHnumM.Clear();
                    verifyBoxHnum.Text = r.HihoNum;
                }

                verifyBoxRatio.Text = r.Ratio.ToString();
                verifyBoxFname.Text = r.HihoName;
                verifyBoxSex.Text = r.Sex.ToString();
                int jj = DateTimeEx.GetEraNumber(r.Birthday);
                verifyBoxBirthday.Text = jj.ToString();
                verifyBoxBY.Text = DateTimeEx.GetJpYear(r.Birthday).ToString();
                verifyBoxBM.Text = r.Birthday.Month.ToString();
                verifyBoxBD.Text = r.Birthday.Day.ToString();
                verifyBoxNewCont.Text = ((int)r.NewContType).ToString();
                verifyBoxName.Text = r.PersonName;

                verifyBoxBuiCount.Text = getBuiCount(r).ToString();
                verifyBoxFusho.Text = r.FushoName1;
                verifyBoxShokenY.Text = DateTimeEx.GetJpYear(r.FushoFirstDate1).ToString();
                verifyBoxShokenM.Text = r.FushoFirstDate1.Month.ToString();
                verifyBoxShokenD.Text = r.FushoFirstDate1.Day.ToString();
                verifyBoxStart.Text = r.FushoStartDate1.Day.ToString();
                verifyBoxEnd.Text = r.FushoFinishDate1.Day.ToString();
                verifyBoxDays.Text = r.CountedDays.ToString();
                verifyBoxTotal.Text = r.Total.ToString();
                verifyBoxCharge.Text = r.Charge.ToString();

                verifyBoxDrCode.Text = r.DrNum;
                verifyBoxDrName.Text = r.DrName;
                verifyBoxHosName.Text = r.ClinicName;
                verifyBoxGroupName.Text = r.AccountName;
                verifyBoxNumbering.Text = r.Numbering;
            }
        }

        private int getBuiCount(App r)
        {
            int c = 1;
            if (r.FushoName2 != string.Empty) c++;
            if (r.FushoName3 != string.Empty) c++;
            if (r.FushoName4 != string.Empty) c++;
            if (r.FushoName5 != string.Empty) c++;
            return c;
        }

        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var setCanInput = new Action<TextBox, bool>((t, b) =>
            {
                t.ReadOnly = !b;
                t.TabStop = b;
                if (!b)
                    t.BackColor = SystemColors.Menu;
                else if (!t.Enabled)
                    t.BackColor = SystemColors.Control;
                else
                    t.BackColor = SystemColors.Info;
            });

            var normalInput = verifyBoxY.Text != "--" && verifyBoxY.Text != "++";

            setCanInput(verifyBoxM, normalInput);
            setCanInput(verifyBoxHnumM, normalInput);
            setCanInput(verifyBoxHnum, normalInput);
            setCanInput(verifyBoxRatio, normalInput);
            setCanInput(verifyBoxFname, normalInput);
            setCanInput(verifyBoxSex, normalInput);
            setCanInput(verifyBoxBirthday, normalInput);
            setCanInput(verifyBoxBY, normalInput);
            setCanInput(verifyBoxBM, normalInput);
            setCanInput(verifyBoxBD, normalInput);
            setCanInput(verifyBoxName, normalInput);

            setCanInput(verifyBoxBuiCount, normalInput);
            setCanInput(verifyBoxFusho, normalInput);
            setCanInput(verifyBoxShokenY, normalInput);
            setCanInput(verifyBoxShokenM, normalInput);
            setCanInput(verifyBoxShokenD, normalInput);
            setCanInput(verifyBoxStart, normalInput);
            setCanInput(verifyBoxEnd, normalInput);
            setCanInput(verifyBoxDays, normalInput);
            setCanInput(verifyBoxNewCont, normalInput);
            setCanInput(verifyBoxTotal, normalInput);
            setCanInput(verifyBoxCharge, normalInput);
            setCanInput(verifyBoxDrCode, normalInput);
            setCanInput(verifyBoxDrName, normalInput);
            setCanInput(verifyBoxHosName, normalInput);
            setCanInput(verifyBoxGroupName, normalInput);
            setCanInput(verifyBoxNumbering, normalInput);
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            ImageUtility.ImageRotate(app.GetImageFullPath(), true);
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            ImageUtility.ImageRotate(app.GetImageFullPath(), false);
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        private void verifyBoxs_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            var pos = t == verifyBoxY ? posYM :
                t == verifyBoxM ? posYM :
                t == verifyBoxHnum ? posYM :
                t == verifyBoxHnumM ? posYM :
                t == verifyBoxRatio ? posYM :
                t == verifyBoxFname ? posPerson :
                t == verifyBoxSex ? posPerson :
                t == verifyBoxBirthday ? posPerson :
                t == verifyBoxBY ? posPerson :
                t == verifyBoxBM ? posPerson :
                t == verifyBoxBD ? posPerson :
                t == verifyBoxName ? posPerson :
                t == verifyBoxBuiCount ? posFusho :
                t == verifyBoxFusho ? posFusho :
                t == verifyBoxShokenY ? posFusho :
                t == verifyBoxShokenM ? posFusho :
                t == verifyBoxShokenD ? posFusho :
                t == verifyBoxStart ? posFusho :
                t == verifyBoxEnd ? posFusho :
                t == verifyBoxDays ? posFusho :
                t == verifyBoxNewCont ? posFusho :
                t == verifyBoxTotal ? posTotal :
                t == verifyBoxCharge ? posTotal :
                t == verifyBoxDrCode ? posDr :
                t == verifyBoxDrName ? posDr :
                t == verifyBoxHosName ? posDr :
                t == verifyBoxGroupName ? posDr :
                t == verifyBoxNumbering ? posNo :
                posYM;

            scrollPictureControl1.ScrollPosition = pos;
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setVerify(CheckInputAall ca)
        {
            //エラーに応じてテキストボックスを有効化、値設定
            var app = ca.app;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setVerifyVal(verifyBoxY, "--", nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setVerifyVal(verifyBoxY, "++", nv);
            }
            else
            {
                setVerifyVal(verifyBoxY, app.MediYear, nv);

                //これらの項目はかならず入力、ベリファイ済みの時のみ表示
                setVerifyVal(verifyBoxRatio, app.Ratio, nv);
                setVerifyVal(verifyBoxSex, app.Sex, nv);
                setVerifyVal(verifyBoxBuiCount, getBuiCount(app), nv);
                setVerifyVal(verifyBoxNewCont, ((int)app.NewContType).ToString(), nv);
                setVerifyVal(verifyBoxNumbering, app.Numbering, nv);

                var fn = Person.GetVerifiedFamilyName(app.HihoNum);
                var p = Person.GetPerson(app.HihoNum, app.Birthday);
                var d = Doctor.GetDoctor(app.DrNum);

                setVerifyVal(verifyBoxM, app.MediMonth, nv && ca.ErrAmonth);
                var mn = app.HihoNum.Split('-');
                if (mn.Length == 2)
                {
                    setVerifyVal(verifyBoxHnumM, mn[0], nv && ca.ErrHnum);
                    setVerifyVal(verifyBoxHnum, mn[1], nv && ca.ErrHnum);
                }
                else
                {
                    setVerifyVal(verifyBoxHnumM, "", nv);
                    setVerifyVal(verifyBoxHnum, "", nv);
                }
                setVerifyVal(verifyBoxBirthday, DateTimeEx.GetEraNumber(app.Birthday), nv && ca.ErrBirth);
                setVerifyVal(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), nv && ca.ErrBirth);
                setVerifyVal(verifyBoxBM, app.Birthday.Month, nv && ca.ErrBirth);
                setVerifyVal(verifyBoxBD, app.Birthday.Day, nv && ca.ErrBirth);
                var familyNeedCheck = ca.ErrHnum || fn == null;
                setVerifyVal(verifyBoxFname, app.HihoName, nv && familyNeedCheck);
                var personNeedCheck = ca.ErrHnum || ca.ErrBirth || p == null || !p.Verified;
                setVerifyVal(verifyBoxName, app.PersonName, nv && personNeedCheck);

                verifyBoxFusho.Text = Fusho.ToRegularName(verifyBoxFusho.Text);
                var fname1 = Fusho.ToRegularName(app.FushoName1);
                setVerifyVal(verifyBoxFusho, fname1, nv && ca.ErrFusho);

                setVerifyVal(verifyBoxShokenY, DateTimeEx.GetJpYear(app.FushoFirstDate1), nv && ca.ErrShokenDate);
                setVerifyVal(verifyBoxShokenM, app.FushoFirstDate1.Month, nv && ca.ErrShokenDate);
                setVerifyVal(verifyBoxShokenD, app.FushoFirstDate1.Day, nv && ca.ErrShokenDate);
                setVerifyVal(verifyBoxStart, app.FushoStartDate1.Day, nv && ca.ErrStart);
                setVerifyVal(verifyBoxEnd, app.FushoFinishDate1.Day, nv && ca.ErrEnd);
                setVerifyVal(verifyBoxDays, app.CountedDays, nv && ca.ErrAcountedDays);
                setVerifyVal(verifyBoxTotal, app.Total, nv && ca.ErrAtotal);
                setVerifyVal(verifyBoxCharge, app.Charge, nv && ca.ErrAcharge);

                setVerifyVal(verifyBoxDrCode, app.DrNum, nv && ca.ErrDrCode);
                var drNeedCheck = ca.ErrDrCode || !d.Verified;
                setVerifyVal(verifyBoxDrName, app.DrName, nv && drNeedCheck);
                setVerifyVal(verifyBoxHosName, app.ClinicName, nv && drNeedCheck);
                setVerifyVal(verifyBoxGroupName, app.AccountName, nv && drNeedCheck);
            }
            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {

        }

        private void verifyBoxDrCode_Leave(object sender, EventArgs e)
        {
            var code = verifyBoxDrCode.Text.Trim();
            var d = Doctor.GetDoctor(code);
            if (d == null || !d.Verified) return;

            if (verifyBoxDrName.Text == string.Empty) verifyBoxDrName.Text = d.DrName;
            if (verifyBoxHosName.Text == string.Empty) verifyBoxHosName.Text = d.HosName;
            if (verifyBoxGroupName.Text == string.Empty) verifyBoxGroupName.Text = d.GroupName;
            if (ActiveControl == verifyBoxDrName) verifyBoxNumbering.Focus();
        }

        private void verifyBoxHnum_Leave(object sender, EventArgs e)
        {
            if (verifyBoxFname.Text != string.Empty) return;
            var hnum = verifyBoxHnumM.Text.Trim() + "-" + verifyBoxHnum.Text.Trim();
            var fn = Person.GetVerifiedFamilyName(hnum);
            if (fn == null) return;
            verifyBoxFname.Text = fn;
        }

        private void verifyBoxBD_Leave(object sender, EventArgs e)
        {
            if (verifyBoxName.Text != string.Empty) return;
            var hnum = verifyBoxHnumM.Text.Trim() + "-" + verifyBoxHnum.Text.Trim();

            int j, y, m, d;
            DateTime dt;
            if (!int.TryParse(verifyBoxBirthday.Text, out j)) return;
            if (!int.TryParse(verifyBoxBY.Text, out y)) return;
            if (!int.TryParse(verifyBoxBM.Text, out m)) return;
            if (!int.TryParse(verifyBoxBD.Text, out d)) return;

            var dts = j == 1 ? "M" : j == 2 ? "T" : j == 3 ? "S" : j == 4 ? "H" : "";
            dts += verifyBoxBY.Text + "/" + verifyBoxBM.Text + "/" + verifyBoxBD.Text;
            DateTimeEx.TryGetDateTimeFromJdate(dts, out dt);

            var p = Person.GetPerson(hnum, dt);
            if (p == null || !p.Verified) return;
            verifyBoxName.Text = p.Pname;
            if (ActiveControl == verifyBoxName) verifyBoxBuiCount.Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (t == verifyBoxY) posYM = pos;
            else if (t == verifyBoxM) posYM = pos;
            else if (t == verifyBoxHnum) posYM = pos;
            else if (t == verifyBoxHnumM) posYM = pos;
            else if (t == verifyBoxRatio) posYM = pos;
            else if (t == verifyBoxFname) posPerson = pos;
            else if (t == verifyBoxSex) posPerson = pos;
            else if (t == verifyBoxBirthday) posPerson = pos;
            else if (t == verifyBoxBY) posPerson = pos;
            else if (t == verifyBoxBM) posPerson = pos;
            else if (t == verifyBoxBD) posPerson = pos;
            else if (t == verifyBoxName) posPerson = pos;
            else if (t == verifyBoxBuiCount) posFusho = pos;
            else if (t == verifyBoxFusho) posFusho = pos;
            else if (t == verifyBoxShokenY) posFusho = pos;
            else if (t == verifyBoxShokenM) posFusho = pos;
            else if (t == verifyBoxShokenD) posFusho = pos;
            else if (t == verifyBoxStart) posFusho = pos;
            else if (t == verifyBoxEnd) posFusho = pos;
            else if (t == verifyBoxDays) posFusho = pos;
            else if (t == verifyBoxNewCont) posFusho = pos;
            else if (t == verifyBoxTotal) posTotal = pos;
            else if (t == verifyBoxCharge) posTotal = pos;
            else if (t == verifyBoxDrCode) posDr = pos;
            else if (t == verifyBoxDrName) posDr = pos;
            else if (t == verifyBoxHosName) posDr = pos;
            else if (t == verifyBoxGroupName) posDr = pos;
            else if (t == verifyBoxNumbering) posNo = pos;
        }
    }
}
