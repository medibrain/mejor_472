﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.Musashino
{
    class Person
    {
        public int PID { get; private set; }
        public string Hnum { get; private set; }
        public DateTime Pbirth { get; private set; }
        public int Psex { get; private set; }
        public string Hname { get; private set; }
        public string Pname { get; private set; }
        public bool Verified { get; private set; }
        public int LastAID { get; private set; }

        private Person()
        {
            PID = 0;
        }

        public Person(App app)
        {
            PID = 0;
            this.Hnum = app.HihoNum;
            this.Pbirth = app.Birthday;
            this.Hname = app.HihoName;
            this.Psex = app.Sex;
            this.Pname = app.PersonName;
            this.LastAID = app.Aid;
        }

        /// <summary>
        /// 確定された被保険者氏名を返します　ない場合nullが返ります
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static string GetVerifiedFamilyName(string number)
        {
            var sql = "SELECT hname FROM person " +
                "WHERE hnum=:hnum AND verified LIMIT 1";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("hnum", NpgsqlTypes.NpgsqlDbType.Text).Value = number;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count != 1) return null;

                return (string)res[0][0];
            }
        }

        public bool IsEqual(App app)
        {
            if (PID == 0) throw new Exception("データベースにない情報と比較しようとしました");
            if (this.Hnum != app.HihoNum) throw new Exception("被保険者番号が違う情報を比較しようとしました");
            if (this.Pbirth != app.Birthday) throw new Exception("生年月日が違う情報を比較しようとしました");

            return this.Hname == app.HihoName &&
                this.Psex == app.Sex &&
                this.Pname == app.PersonName;
        }

        public static Person GetPerson(string number, DateTime birth)
        {
            var sql = "SELECT pid, hnum, pbirth, psex, hname, pname, verified, lastaid " +
                "FROM person " +
                "WHERE hnum=:hnum AND pbirth=:pbirth";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("hnum", NpgsqlTypes.NpgsqlDbType.Text).Value = number;
                cmd.Parameters.Add("pbirth", NpgsqlTypes.NpgsqlDbType.Date).Value = birth;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var p = new Person();
                p.PID = (int)res[0][0];
                p.Hnum = (string)res[0][1];
                p.Pbirth = (DateTime)res[0][2];
                p.Psex = (int)res[0][3];
                p.Hname = (string)res[0][4];
                p.Pname = (string)res[0][5];
                p.Verified = (bool)res[0][6];
                p.LastAID = (int)res[0][7];
                return p;
            }
        }


        public bool Insert()
        {
            if (PID != 0) return false;

            var sql = "INSERT INTO person " +
                "(hnum, pbirth, psex, hname, pname, verified, insert_uid, lastaid) " +
                "VALUES (:hnum, :pbirth, :psex, :hname, :pname, :verified, :inuid, :aid);";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = this.Hnum;
                cmd.Parameters.Add("pbirth", NpgsqlDbType.Date).Value = this.Pbirth;
                cmd.Parameters.Add("psex", NpgsqlDbType.Integer).Value = this.Psex;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = this.Hname;
                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = this.Pname;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("inuid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.LastAID;
                return cmd.TryExecuteNonQuery();
            }
        }

        public bool Update(App app, bool forceVerify)
        {
            if (PID == 0) return false;
            if (this.Hnum != app.HihoNum) return false;
            if (this.Pbirth != app.Birthday) return false;

            if (IsEqual(app))
            {
                if (this.Verified) return true;

                //情報が一致し、情報元aidが違う場合にベリファイ済みとみなす
                this.Verified = forceVerify ? true : this.LastAID != app.Aid;
                this.LastAID = app.Aid;
            }
            else
            {
                this.Hname = app.HihoName;
                this.Psex = app.Sex;
                this.Pname = app.PersonName;
                this.LastAID = app.Aid;
                this.Verified = forceVerify ? true : false;
            }

            var sql = "UPDATE person " +
                "SET psex=:psex, hname=:hname, pname=:pname, verified=:verified, " +
                "update_uid=:upuid, lastaid=:aid " +
                "WHERE pid=:pid";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("psex", NpgsqlDbType.Integer).Value = this.Psex;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = this.Hname;
                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = this.Pname;
                cmd.Parameters.Add("verified", NpgsqlDbType.Boolean).Value = this.Verified;
                cmd.Parameters.Add("upuid", NpgsqlDbType.Integer).Value = User.CurrentUser.UserID;
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.LastAID;
                cmd.Parameters.Add("pid", NpgsqlDbType.Integer).Value = this.PID;
                return cmd.TryExecuteNonQuery();
            }
        }

    }
}
