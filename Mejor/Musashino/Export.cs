﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using NpgsqlTypes;
using System.Windows.Forms;

namespace Mejor.Musashino
{
    /// <summary>
    /// 武蔵野市国保のデータ出力クラス
    /// 2015.11.20　松本直也
    /// </summary>
    class Export
    {
        const int ZOKUSHI_CODE = -3;
        const int FUYOU_CODE = -4;
        const string DEFALT_ERA = "4";
        const string dbName = "musashino";

        class ExportData
        {
            private App sourceApp;
            public string numberinig { get; private set; }
            private string mark;
            private string number;
            private string familyName;
            private string name;
            private string sex;
            private string birth;
            private string mediYM;
            private string days;
            private string buiCount;
            private string total;
            private string charge;
            private string firstDate;
            private string startDay;
            private string endDay;
            private string fushoName;
            private string ratio;
            private string groupName;
            private string hosName;
            private string drNo;
            private string drName;
            private List<App> subPagesApp = new List<App>();

            public ExportData(App app)
            {
                sourceApp = app;
                numberinig = app.Numbering.PadLeft(4, '0');
                var mn = app.HihoNum.Split('-');
                mark = mn[0];
                number = mn[1];
                familyName = app.HihoName;
                name = app.PersonName;
                sex = app.Sex.ToString();
                birth = DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday).ToString();
                mediYM = "4" + app.MediYear.ToString("00") + app.MediMonth.ToString("00");
                days = app.CountedDays.ToString();
                buiCount = getBuiCount(app).ToString();
                total = app.Total.ToString();
                charge = app.Charge.ToString();
                firstDate = DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFirstDate1).ToString();
                startDay = DateTimeEx.GetIntJpDateWithEraNumber(app.FushoStartDate1).ToString();
                endDay = DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFinishDate1).ToString();
                fushoName = app.FushoName1;
                ratio = app.Ratio.ToString();
                groupName = app.AccountName;
                hosName = app.ClinicName;
                drNo = app.DrNum;
                drName = app.DrName;
            }

            public void AddSubPage(App app)
            {
                subPagesApp.Add(app);
            }

            public string CreateCsvLine()
            {
                var ss = new string[21];
                ss[0] = numberinig;
                ss[1] = mark;
                ss[2] = number;
                ss[3] = familyName;
                ss[4] = name;
                ss[5] = sex;
                ss[6] = birth;
                ss[7] = mediYM;
                ss[8] = days;
                ss[9] = buiCount;
                ss[10] = total;
                ss[11] = charge;
                ss[12] = firstDate;
                ss[13] = startDay;
                ss[14] = endDay;
                ss[15] = fushoName;
                ss[16] = ratio;
                ss[17] = groupName;
                ss[18] = hosName;
                ss[19] = drNo;
                ss[20] = drName;

                return string.Join(",", ss);
            }

            public bool CreateTiff(string saveFileName)
            {
                var l = new List<string>();
                l.Add(sourceApp.GetImageFullPath(dbName));

                if (subPagesApp.Count == 0)
                {
                    return ImageUtility.SaveOne(l, saveFileName);
                }

                foreach (var item in subPagesApp)
                {
                    l.Add(item.GetImageFullPath(dbName));
                }
                return ImageUtility.Save(l, saveFileName);
            }

            private int getBuiCount(App r)
            {
                int c = 1;
                if (r.FushoName2 != string.Empty) c++;
                if (r.FushoName3 != string.Empty) c++;
                if (r.FushoName4 != string.Empty) c++;
                if (r.FushoName5 != string.Empty) c++;
                return c;
            }

        }

        /// <summary>
        /// 保存先ディレクトリと対象処理月1ヶ月分すべてのAppを指定し、エクスポートを行います。
        /// </summary>
        /// <param name="dirName"></param>
        /// <param name="appList"></param>
        /// <returns></returns>
        public static bool Exporting(string dirName, int cym, WaitForm wf)
        {
            wf.LogPrint("対象データの抽出中です…");
            var appList = App.GetApps(cym);
            wf.SetMax(appList.Count);

            wf.LogPrint("データ変換中です…");
            var exportList = new List<ExportData>();
            ExportData export = null;

            //エクスポートデータに変換
            foreach (var item in appList)
            {
                if (item.MediYear == FUYOU_CODE) continue;

                if (item.MediYear == ZOKUSHI_CODE)
                {
                    if (export == null)
                    {
                        MessageBox.Show("先頭の画像に対し続紙コードが指定されています。再度入力データを確認してください。");
                        return false;
                    }

                    export.AddSubPage(item);
                    continue;
                }

                if (export != null) exportList.Add(export);
                export = new ExportData(item);
            }
            if (export != null) exportList.Add(export);

            //ナンバリング順にソート
            exportList.Sort((ex1, ex2) => ex1.numberinig.CompareTo(ex2.numberinig));

            var imageDir = dirName + "\\image";
            var scanDir = Settings.ImageFolder;
            try
            {
                System.IO.Directory.CreateDirectory(imageDir);
            }
            catch
            {
                MessageBox.Show("出力フォルダの作成に失敗しました。");
                return false;
            }

            wf.LogPrint("データ及び画像の出力中です…");
            int receCount = 0;
            var jyuFileName = dirName + "\\柔整" + cym.ToString("0000年00月") + "分.csv";

            try
            {
                using (var sw = new System.IO.StreamWriter(jyuFileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine("整理番号,記号,番号,被保険者氏名,療養者氏名,性別,生年," +
                        "診療月,日数,部位数,合計金額,請求金額,初検日,施術開始日,施術終了日," +
                        "負傷名,給付割合,団体名称,施術所名称,柔整師No,柔整師氏名");

                    foreach (var item in exportList)
                    {
                        receCount++;
                        wf.InvokeValue = receCount;
                        sw.WriteLine(item.CreateCsvLine());
                        item.CreateTiff(imageDir + "\\" + item.numberinig + ".tiff");

                        if (wf.Cancel)
                        {
                            if(MessageBox.Show("出力を中止してもよろしいですか？",
                                "出力中止確認",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) ==
                                DialogResult.Yes)
                            return false;

                            wf.Cancel = false;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("出力に失敗しました。\r\n" + ex.Message);
                return false;
            }

            MessageBox.Show("出力が終了しました。");
            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            var wf = new WaitForm();
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var rex = new string[25];
                    //先頭行は見出し
                    rex[0] = "ID";
                    rex[1] = "処理年";
                    rex[2] = "処理月";
                    rex[3] = "診療年";
                    rex[4] = "診療月";
                    rex[5] = "保険者番号";
                    rex[6] = "被保険者番号";
                    rex[7] = "住所";
                    rex[8] = "氏名";
                    rex[9] = "性別";
                    rex[10] = "生年月日";
                    rex[11] = "請求区分";
                    rex[12] = "往療料";
                    rex[13] = "施術所記号";
                    rex[14] = "合計金額";
                    rex[15] = "請求金額";
                    rex[16] = "診療日数";
                    rex[17] = "ナンバリング";
                    rex[18] = "点検結果";
                    rex[19] = "点検詳細";
                    rex[20] = "被保険者名";
                    rex[21] = "受療者名";
                    rex[22] = "郵便番号";
                    rex[23] = "住所";
                    rex[24] = "施術所名";

                    sw.WriteLine(string.Join(",", rex));

                    var ss = new List<string>();
                    foreach (var item in list)
                    {
                        if (wf.Cancel)
                        {
                            if (MessageBox.Show("中止してもよろしいですか？", "",
                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.InsNum);
                        ss.Add(item.HihoNum);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.PersonName);
                        ss.Add(item.Sex == 1 ? "男" : "女");
                        ss.Add(item.Birthday.ToShortDateString());
                        ss.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                        ss.Add(item.Distance == 999 ? "あり" : "");
                        ss.Add(item.DrNum);
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Numbering);
                        ss.Add(item.InspectInfo);
                        ss.Add(item.InspectDescription);
                        ss.Add(item.HihoName);
                        ss.Add(item.PersonName);
                        ss.Add(item.HihoZip);
                        ss.Add(item.HihoAdd);
                        ss.Add(item.ClinicName);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }

            MessageBox.Show("出力が終了しました");
            return true;
        }
    }
}

