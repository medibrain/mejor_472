﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.Verify
{
    class VerifyMissCounter
    {
        List<Counter> list = new List<Counter>();
        bool tempSaved = false;

        public int firstMissCount { get; private set; }
        public int secondMissCount { get; private set; }

        class Counter
        {
            IVerifiable iv;
            string firstTemp;
            string secondTemp;

            public bool firstMiss = false;
            public bool secondMiss = false;

            public Counter(IVerifiable v)
            {
                iv = v;
            }

            public void TempSave()
            {
                firstTemp = iv.VerifyValue;
                secondTemp = iv.Value;
            }

            public void Reset()
            {
                firstTemp = string.Empty;
                secondTemp = string.Empty;
                firstMiss = false;
                secondMiss = false;
            }

            /// <summary>
            /// チェックを行ないます
            /// </summary>
            /// <param name="aid"></param>
            public void Check()
            {
                if (!iv.Enabled) return;
                if (!iv.Visible) return;
                firstMiss = iv.VerifyValue != firstTemp;
                secondMiss = iv.Value != secondTemp;
            }

            /// <summary>
            /// 詳細ログを残します
            /// </summary>
            /// <param name="aid"></param>
            /// <param name="cid"></param>
            public void LogWrite(int aid, int cid)
            { 
                if (!firstMiss && !secondMiss) return;
                var log = new MissData();
                log.aid = aid;
                log.cid = cid;
                log.Name = iv.Name;
                log.Input1 = firstTemp;
                log.Input2 = secondTemp;
                log.Decision = iv.Value;
                log.InputType = firstMiss && secondMiss ? INPUT_TYPE.Both
                    : firstMiss ? INPUT_TYPE.First : INPUT_TYPE.Second;
                log.Insert();
            }

            class MissData
            {
                [DB.DbAttribute.Serial]
                [DB.DbAttribute.PrimaryKey]
                public int id { get; set; }
                public int iid { get; set; } = Insurer.CurrrentInsurer.InsurerID;
                public int aid { get; set; }
                public int cid { get; set; }
                public string Name { get; set; } = string.Empty;
                public string Input1 { get; set; } = string.Empty;
                public string Input2 { get; set; } = string.Empty;
                public string Decision { get; set; } = string.Empty;
                public INPUT_TYPE InputType { get; set; } = INPUT_TYPE.Null;

                private DB db = new DB("jyusei");
                public void Insert() => db.Insert(this);
            }
        }

        public VerifyMissCounter(List<IVerifiable> l)
        {
            foreach (var item in l)
            {
                var c = new Counter(item);
                list.Add(c);
            }
        }

        public void Reset()
        {
            foreach (var item in list)
            {
                item.Reset();
            }
            firstMissCount = 0;
            secondMissCount = 0;
            tempSaved = false;
        }

        /// <summary>
        /// ミスをチェック＆カウントします
        /// </summary>
        /// <param name="aid"></param>
        public void Check()
        {
            if (!tempSaved)
            {
                tempSaved = true;
                list.ForEach(x => x.TempSave());
            }

            firstMissCount = 0;
            secondMissCount = 0;

            foreach (var item in list)
            {
                item.Check();
                if (item.firstMiss) firstMissCount++;
                if (item.secondMiss) secondMissCount++;
            }
        }

        /// <summary>
        /// ミスをチェック＆カウントします
        /// </summary>
        /// <param name="aid"></param>
        public void Check(int aid)
        {
            if (!tempSaved)
            {
                tempSaved = true;
                list.ForEach(x => x.TempSave());
            }

            firstMissCount = 0;
            secondMissCount = 0;

            foreach (var item in list)
            {
                item.Check();
                if (item.firstMiss) firstMissCount++;
                if (item.secondMiss) secondMissCount++;
            }
        }

        /// <summary>
        /// 詳細ログを残します
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="cid"></param>
        public void MissLogWrite(int aid, int cid)
        {
            foreach (var item in list)
            {
                item.LogWrite(aid, cid);
            }
        }
    }
}
