﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Mejor
{
    public class VerifyCheckBox : BorderCheckBox, IVerifiable
    {
        protected MODE mode { get; set; }
        private BorderCheckBox cb = new BorderCheckBox();

        public VerifyCheckBox()
        {
            mode = MODE.Normal;
            Enter += VerifyCheckBox_Enter;
            Leave += VerifyCheckBox_Leave;
            EnabledChanged += VerifyCheckBox_EnabledChanged;
            TextChanged += VerifyCheckBox_TextChanged;

            cb.Visible = false;
            cb.BackColor = Color.LightPink;
            cb.Enter += Cb_Enter;
            cb.Leave += Cb_Leave;
            cb.EnabledChanged += Cb_EnabledChanged;
        }

        private void Cb_Enter(object sender, EventArgs e)
        {
            cb.Visible = true;
            cb.BackColor = Color.HotPink;
            cb.BringToFront();
        }

        private void Cb_Leave(object sender, EventArgs e)
        {
            cb.BackColor = Color.Pink;
            CheckVerify();
            if (!this.Focused) cb.Visible = false;
        }

        private void Cb_EnabledChanged(object sender, EventArgs e)
        {
            cb.BackColor = cb.Enabled ? Color.Pink : SystemColors.Control;
        }

        private void VerifyCheckBox_Enter(object sender, EventArgs e)
        {
            if (this.BackColor == SystemColors.Info) this.BackColor = Color.LightCyan;
            else if (this.BackColor == Color.Pink) this.BackColor = Color.HotPink;

            if (mode == MODE.Verify && !IsEqual)
            {
                if (this.Parent == null) return;
                if (!this.Parent.Controls.Contains(cb))
                {
                    this.Parent.Controls.Add(cb);
                    cb.Location = new Point(this.Location.X, this.Location.Y + this.Height - 1);
                    cb.Size = this.Size;
                    cb.TabIndex = this.TabIndex;
                }
                cb.Visible = true;
                
                //20200731145104 furukawa st ////////////////////////
                //Zオーダーを最前面にしないと他のコントロールに被って見えない
                
                cb.BringToFront();
                //20200731145104 furukawa ed ////////////////////////
            }
        }

        private void VerifyCheckBox_Leave(object sender, EventArgs e)
        {
            if (this.BackColor == Color.LightCyan) this.BackColor = SystemColors.Info;
            else if (this.BackColor == Color.HotPink) this.BackColor = Color.Pink;

            if (!cb.Focused) cb.Visible = false;
        }

        private void VerifyCheckBox_EnabledChanged(object sender, EventArgs e)
        {
            this.BackColor = this.Enabled ?
                this.Focused ? Color.LightCyan : SystemColors.Info :
                SystemColors.Control;
        }

        private void VerifyCheckBox_TextChanged(object sender, EventArgs e)
        {
            cb.Text = Text;
        }

        [System.ComponentModel.Category("ベリファイ項目")]
        [System.ComponentModel.Description("ベリファイボックスのチェックを取得または指定します")]
        [System.ComponentModel.Browsable(false)]
        public bool CheckedV
        {
            get { return cb.Checked; }
            set { cb.Checked = value; }
        }

        public bool IsEqual
        {
            get { return this.Checked == cb.Checked; }
        }

        /// <summary>
        /// 2回の入力が一致するかどうかをチェックし、違う場合は強調表示にします。
        /// エラーがある場合、Trueを返します。
        /// </summary>
        /// <returns>errorがある場合、True</returns>
        public bool CheckVerify()
        {
            if (!Enabled || !Visible) return false;

            mode = MODE.Verify;
            var b = !IsEqual;

            this.BackColor = b ? Color.Pink : SystemColors.Info;
            return b;
        }

        /// <summary>
        /// バックカラーを通常、またはエラーにします
        /// </summary>
        /// <param name="st"></param>
        public void SetErrorView(bool isError)
        {
            this.BackColor = !this.Enabled ? SystemColors.Control :
                isError ?
                this.Focused ? Color.Pink : Color.MistyRose :
                this.Focused ? Color.LightCyan : SystemColors.Info;
        }

        public void AllClear()
        {
            this.Checked = false;
            this.BackColor = Enabled ? SystemColors.Info : SystemColors.Control;
            cb.Checked = false;
            cb.Visible = false;
            this.mode = MODE.Normal;
        }

        public string Value => Checked.ToString();
        public string VerifyValue => cb.Checked.ToString();
    }
}
