﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Mejor
{
    public class VerifyBoxCore : TextBox
    {
        [DllImport("imm32.dll")]
        static extern int ImmGetVirtualKey(int hwnd);

        const int WM_KEYDOWN = 0x100;
        const int VK_PROCESSKEY = 0xE5;

        //event デリゲート型 イベント ハンドラー名;
        public delegate void ProcessKeyDownEventHandrer(Keys key);
        public event ProcessKeyDownEventHandrer ProcessCmdKeyDown;

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (msg.Msg.Equals(WM_KEYDOWN))
            {
                if (msg.WParam.ToInt32().Equals(VK_PROCESSKEY))
                {
                    int key = ImmGetVirtualKey((int)this.Handle);
                    ProcessCmdKeyDown?.Invoke((Keys)key);
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }

    public class VerifyBox : VerifyBoxCore, IVerifiable
    {
        public event EventHandler TextBoxV_Leave;

        public enum VIEW_STATUS { OK, NG }
        public enum ERROR_CODE { 不明 = -2, 桁超 = - 3,NULL = -9 }

        protected MODE mode { get; set; }
        protected VerifyBoxCore tb = new VerifyBoxCore();

        public bool NewLine { get; set; } = false;

        public VerifyBox()
        {
            tb.Visible = false;
            tb.BackColor = Color.Pink;
            this.FontChanged += TextBoxV_FontChanged;
            this.BackColor = SystemColors.Info;
            mode = MODE.Normal;

            this.Enter += VerifyBox_Enter;
            this.Leave += VerifyBox_Leave;
            this.EnabledChanged += TextBoxV_EnabledChanged;
            this.TextAlignChanged += VerifyBox_TextAlignChanged;
            this.ImeModeChanged += (s, e) => tb.ImeMode = this.ImeMode;
            tb.Enter += tb_Enter;
            tb.Leave += tb_Leave;
            tb.EnabledChanged += tb_EnabledChanged;
        }

        private void VerifyBox_TextAlignChanged(object sender, EventArgs e)
        {
            tb.TextAlign = this.TextAlign;
        }

        void tb_EnabledChanged(object sender, EventArgs e)
        {
            tb.BackColor = tb.Enabled ? Color.Pink : SystemColors.Control;
        }

        void TextBoxV_EnabledChanged(object sender, EventArgs e)
        {
            this.BackColor = this.Enabled ? 
                this.Focused ? Color.LightCyan : SystemColors.Info :
                SystemColors.Control;
        }

        void tb_Enter(object sender, EventArgs e)
        {
            tb.Visible = true;
            //BringToFront();
            //tb.TabStop = true;
            //tb.BringToFront();
            tb.BackColor = Color.HotPink;
            if (tb.Multiline)
            {
                //MultiLine許容時、
                //全選択された状態でフォーカスが移ると選択部分が消滅するので対応
                tb.Select(tb.Text.Length, 0);
            }
            else tb.SelectAll();
        }

        void tb_Leave(object sender, EventArgs e)
        {
            TextBoxV_Leave?.Invoke(this, new EventArgs());
            tb.BackColor = Color.Pink;
            CheckVerify();
            if (!this.Focused) tb.Visible = false;

            if (!NewLine)
            {
                tb.Text = tb.Text.Replace("\r", "").Replace("\n", "");
            }
        }

        void VerifyBox_Enter(object sender, EventArgs e)
        {
            if (this.BackColor == SystemColors.Info) this.BackColor = Color.LightCyan;
            else if (this.BackColor == Color.Pink) this.BackColor = Color.HotPink;

            if (mode == MODE.Verify && !IsEqual)
            {
                if (this.Parent == null) return;
                if (!this.Parent.Controls.Contains(tb))
                {
                    this.Parent.Controls.Add(tb);
                    tb.Location = new Point(this.Location.X, this.Location.Y + this.Height - 1);
                    tb.Size = this.Size;
                    tb.Multiline = this.Multiline;
                    tb.TabIndex = this.TabIndex;
                    tb.BringToFront();
                    BringToFront();
                }
                tb.Visible = true;
                tb.BringToFront();
                BringToFront();
                tb.TabStop = true;

            }
            this.SelectAll();
        }

        void VerifyBox_Leave(object sender, EventArgs e)
        {
            if (this.BackColor == Color.LightCyan) this.BackColor = SystemColors.Info;
            else if (this.BackColor == Color.HotPink) this.BackColor = Color.Pink;

            if (!tb.Focused) tb.Visible = false;
        }

        void TextBoxV_FontChanged(object sender, EventArgs e)
        {
            tb.Font = Font;
        }

        [System.ComponentModel.Category("ベリファイ項目")]
        [System.ComponentModel.Description("ベリファイボックスの値を取得または指定します")]
        [System.ComponentModel.Browsable(false)]
        public string TextV
        {
            get { return tb.Text; }
            set { tb.Text = value; }
        }

        public bool IsEqual
        {
            get { return this.Text.Trim() == tb.Text.Trim(); }
        }

        public string Value => this.Text;

        public string VerifyValue => tb.Text;

        public void AllClear()
        {
            this.Clear();
            this.BackColor = Enabled ? SystemColors.Info : SystemColors.Control;
            tb.Clear();
            tb.Visible = false;
            this.mode = MODE.Normal;
        }

        /// <summary>
        /// 2回の入力が一致するかどうかをチェックし、違う場合は強調表示にします。
        /// エラーがある場合、Trueを返します。
        /// </summary>
        /// <returns>errorがある場合、True</returns>
        public bool CheckVerify()
        {
            if (!Enabled || !Visible) return false;

            mode = MODE.Verify;
            var b = !IsEqual;

            this.BackColor = b ? Color.Pink : SystemColors.Info;            
            return b;
        }

        /// <summary>
        /// int型の数値を取得します
        /// エラーの場合、各エラーコードを返します
        /// </summary>
        /// <returns></returns>
        public int GetIntValue()
        {
            var ss = this.Text.Split(" .,/-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (ss.Length > MaxLength) return (int)ERROR_CODE.桁超;
            if (ss.Length == 0) return (int)ERROR_CODE.NULL;

            int temp;
            int number = 0;
            foreach (var item in ss)
            {
                if (!int.TryParse(item, out temp)) return (int)ERROR_CODE.NULL;
                number *= 10;
                number += temp;
            }

            return number;
        }

        /// <summary>
        /// 100倍されたint型の数値を取得します
        /// エラーの場合、各エラーコードを返します
        /// </summary>
        /// <returns></returns>
        public int GetInt100Value()
        {
            decimal temp = 0;
            if (!decimal.TryParse(this.Text, out temp)) return (int)ERROR_CODE.NULL;
            return (int)(temp * 100m);
        }

        /// <summary>
        /// 1000倍されたint型の数値を取得します
        /// エラーの場合、各エラーコードを返します
        /// </summary>
        /// <returns></returns>
        public int GetInt1000Value()
        {
            decimal temp = 0;
            if (!decimal.TryParse(this.Text, out temp)) return (int)ERROR_CODE.NULL;
            return (int)(temp * 1000m);
        }

        /// <summary>
        /// バックカラーを通常、またはエラーにします
        /// </summary>
        /// <param name="st"></param>
        public void SetErrorView(bool isError)
        {
            this.BackColor = !this.Enabled ? SystemColors.Control :
                isError ?
                this.Focused ? Color.Pink : Color.MistyRose :
                this.Focused ? Color.LightCyan : SystemColors.Info;
        }

        /// <summary>
        /// 入力されている数値をチェックします
        /// </summary>
        /// <param name="minValue">最小値</param>
        /// <param name="maxValue">最大値</param>
        /// <returns></returns>
        public bool Check(int minValue, int maxValue)
        {
            if (this.Text.Trim().Length == 0)
            {
                this.BackColor = Color.MistyRose;
                return false;
            }
            if (this.Text.Trim() == "**" || this.Text.Trim() == "--")
            {
                this.BackColor = SystemColors.Info;
                return true;
            }

            int temp;
            if (!int.TryParse(this.Text, out temp)) return false;

            if (temp < minValue || maxValue < temp)
            {
                this.BackColor = Color.MistyRose;
                return false;
            }

            this.BackColor = SystemColors.Info;
            return true;
        }

        /// <summary>
        /// 入力されている数値をチェックします
        /// </summary>
        /// <param name="values">使用可能な数値一覧</param>
        /// <returns></returns>
        public bool Check(int[] values)
        {
            if (this.Text.Trim().Length == 0)
            {
                this.BackColor = Color.MistyRose;
                return false;
            }
            if (this.Text.Trim() == "**" || this.Text.Trim() == "--")
            {
                this.BackColor = SystemColors.Info;
                return true;
            }

            int temp;
            if (!int.TryParse(this.Text, out temp)) return false;

            if (!values.Contains(temp))
            {
                this.BackColor = Color.MistyRose;
                return false;
            }

            this.BackColor = SystemColors.Info;
            return true;
        }
    }
}
