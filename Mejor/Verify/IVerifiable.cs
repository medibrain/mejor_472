﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor
{
    public enum MODE { Normal, Verify }

    public interface IVerifiable
    {
        /// <summary>
        /// 現在の値が一致しているかどうかを返します
        /// </summary>
        bool IsEqual { get; }

        /// <summary>
        /// 現在の値を文字列で返します
        /// </summary>
        string Value { get; }

        /// <summary>
        /// 現在の値を文字列で返します
        /// </summary>
        string VerifyValue { get; }

        /// <summary>
        /// すべての表示を通常に戻し、入力情報をクリアします
        /// </summary>
        void AllClear();

        /// <summary>
        /// 2回の入力が一致するかどうかをチェックし、違う場合は強調表示にします。
        /// エラーがある場合、Trueを返します。
        /// </summary>
        /// <returns>errorがある場合、True</returns>
        bool CheckVerify();

        /// <summary>
        /// バックカラーを通常、またはエラー表示にします
        /// </summary>
        /// <param name="isError"></param>
        void SetErrorView(bool isError);


        bool Enabled { get; }
        bool Visible { get; }
        string Name { get; }
    }
}
