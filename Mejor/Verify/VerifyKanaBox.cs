﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;

namespace Mejor
{
    delegate void ProcessKeyDownHandler(int keyCode);

    class VerifyKanaBox : VerifyBox
    {
        bool charN = false;

        public VerifyKanaBox()
        {
            this.ProcessCmdKeyDown += KanaBox_ProcessKeyDown;
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KanaBox_KeyPress);
            this.Enter += new System.EventHandler(this.KanaBox_Enter);
            this.TextChanged += KanaBox_TextChanged;

            tb.ProcessCmdKeyDown += KanaBox_ProcessKeyDown;
            tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KanaBox_KeyPress);
            tb.Enter += new System.EventHandler(this.KanaBox_Enter);
        }

        void KanaBox_TextChanged(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            var i = t.SelectionStart;
            this.Text = Strings.StrConv(this.Text, VbStrConv.Katakana, 0x411);
            this.Select(i, 0);
        }

        void KanaBox_ProcessKeyDown(Keys key)
        {
            if (key == Keys.A || key == Keys.I || key == Keys.U ||
                key == Keys.E || key == Keys.O || key == Keys.OemMinus)
            {
                charN = false;
                SendKeys.Send("{Enter}");
            }
            else if (key == Keys.N)
            {
                if (charN)
                {
                    SendKeys.Send("{Enter}");
                    charN = false;
                }
                else
                {
                    charN = true;
                }
            }
            else if (key == Keys.Enter)
            {
                this.Text = Strings.StrConv(this.Text, VbStrConv.Katakana, 0x411);
                this.Select(this.Text.Length, 0);
                charN = false;
            }
        }

        private void KanaBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) e.Handled = true;
        }

        private void KanaBox_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            t.ImeMode = System.Windows.Forms.ImeMode.On;
            charN = false;
        }
    }
}

