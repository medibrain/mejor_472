﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor
{
    public partial class PriceForm : Form
    {
        int cym;
        /// <summary>
        /// application#cymとaapptypeが存在する必要があります。
        /// </summary>
        /// <param name="chargeYear"></param>
        /// <param name="chargeMonth"></param>
        public PriceForm(int cym)
        {
            InitializeComponent();
            labelTitle.Text =
                $"【{Insurer.CurrrentInsurer.InsurerName}】 {cym.ToString("0000/00")}分";

            Shown += PriceForm_Shown;
            this.cym = cym;

            dataGridView1.DataSource = new List<BatchPrice>();

            var cellStyle = new DataGridViewCellStyle();
            cellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            cellStyle.Format = "#,0";
            dataGridView1.Columns[nameof(BatchPrice.Count)].DefaultCellStyle = cellStyle;
            dataGridView1.Columns[nameof(BatchPrice.Total)].DefaultCellStyle = cellStyle;
            dataGridView1.Columns[nameof(BatchPrice.Charge)].DefaultCellStyle = cellStyle;
            dataGridView1.Columns[nameof(BatchPrice.Count)].Width = 150;
            dataGridView1.Columns[nameof(BatchPrice.Total)].Width = 150;
            dataGridView1.Columns[nameof(BatchPrice.Charge)].Width = 150;
            dataGridView1.Columns[nameof(BatchPrice.BatchNo)].HeaderText = "バッチNo";
            dataGridView1.Columns[nameof(BatchPrice.Count)].HeaderText = "件数";
            dataGridView1.Columns[nameof(BatchPrice.Total)].HeaderText = "合計額";
            dataGridView1.Columns[nameof(BatchPrice.Charge)].HeaderText = "請求額";

            dataGridView1.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.Black;
        }

        private void PriceForm_Shown(object sender, EventArgs e)
        {
            using(var  f = new WaitForm())
            {
                f.ShowDialogOtherTask();

                f.LogPrint("データを取得しています");
                var l = App.GetApps(cym);

                f.LogPrint("集計しています");
                calcPrices(l);
            }
        }

        class BatchPrice
        {
            public string BatchNo { get; set; }
            public int Count { get; set; }
            public int Total { get; set; }
            public int Charge { get; set; }
        }

        private void calcPrices(List<App> list)
        {
            if (list == null || list.Count == 0)
            {
                textBoxPriceTotal.Text = "0";
                textBoxPriceSeikyu.Text = "0";
                return;
            }

            long total = 0;
            long seikyu = 0;
            int countTotal = 0;
            int countSeikyu = 0;
            //var l = new List<BatchPrice>();
            var batch = new BatchPrice();
            var batchNo = string.Empty;

            var dir = new Dictionary<string, BatchPrice>();

            foreach (var item in list)
            {
                if (item.MediYear == (int)APP_SPECIAL_CODE.バッチ &&
                    !dir.ContainsKey(item.Numbering))
                {
                    batch = new BatchPrice();
                    batch.BatchNo = item.Numbering;
                    dir.Add(item.Numbering, batch);
                    continue;
                }
                else if(dir.ContainsKey(item.Numbering))
                {
                    batch = dir[item.Numbering];
                    continue;
                }

                if (item.MediYear < 1) continue;

                batch.Count++;
                batch.Total += item.Total;
                batch.Charge += item.Charge;
                total += item.Total;
                seikyu += item.Charge;
                if (item.Total > 0) countTotal++;
                if (item.Charge > 0) countSeikyu++;
            }

            var l = dir.Values.ToList().FindAll(x => x.Count > 0);
            l.Sort((x, y) => x.BatchNo.PadLeft(10).CompareTo(y.BatchNo.PadLeft(10)));
            dataGridView1.DataSource = l;
            textBoxPriceTotal.Text = string.Format("{0:#,0}", total);
            textBoxPriceSeikyu.Text = string.Format("{0:#,0}", seikyu);
        }

        private void buttonPriceTotalCheck_Click(object sender, EventArgs e)
        {
            checkPrice(
                textBoxPriceTotal.Text,
                textBoxPriceTotalInput.Text,
                labelPriceTotalResult,
                textBoxPriceTotalInput
                );
        }

        private void buttonPriceSeikyuCheck_Click(object sender, EventArgs e)
        {
            checkPrice(
                textBoxPriceSeikyu.Text,
                textBoxPriceSeikyuInput.Text,
                labelPriceSeikyuResult,
                textBoxPriceSeikyuInput);
        }

        private void checkPrice(string targetPrice, string checkPrice, Label resultL, TextBox resultTB)
        {
            long valLong = 0;
            if (long.TryParse(checkPrice.Replace(",", ""), out valLong))
            {
                var convVal = string.Format("{0:#,0}", valLong);
                if (targetPrice == convVal)
                {
                    resultL.Text = "一致";
                }
                else
                {
                    resultL.Text = "不一致";
                }
                resultTB.Text = convVal;
            }
            else
            {
                resultL.Text = "不一致";
            }
            resultL.Visible = true;
        }

        private void buttonCsv_Click(object sender, EventArgs e)
        {
            var fn = string.Empty;
            using (var f =new SaveFileDialog())
            {
                f.FileName = $"{Insurer.CurrrentInsurer.InsurerName}バッチ別リスト{cym}.csv";
                if (f.ShowDialog() != DialogResult.OK) return;
                fn = f.FileName;
            }

            Utility.ViewToCsv(dataGridView1, fn);
        }
    }
}
