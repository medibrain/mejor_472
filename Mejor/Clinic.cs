﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Mejor
{
    public class Clinic
    {
        public long Code { get; set; }
        public string Name { get; set; }
        public DateTime Insert_Date { get; set; }
        public int Insert_CYM { get; set; }
        public DateTime Update_Date { get; set; }
        public int Update_CYM { get; set; }
        public bool Verified { get; set; }

        /// <summary>
        /// 登録されているすべての施術所（師）を取得します
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static IEnumerable<Clinic> GetClinicList()
        {
            var sql = $"SELECT * FROM clinic;";
            var result = DB.Main.Query<Clinic>(sql);
            if (result == null || result.Count() == 0) return null;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool CreateListCsv(string fileName)
        {
            try
            {
                var l = GetClinicList().ToList();
                l.Sort((x, y) => x.Code.CompareTo(y.Code));

                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine("コード,名称");
                    foreach (var item in l)
                    {
                        sw.WriteLine($"{(item.Code< 10000000 ? item.Code.ToString() : item.Code.ToString("0000000000"))},{item.Name}");
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
        }

        /// <summary>
        /// コードに対応した施術所（師）を取得します。
        /// 取得できなかった場合はnullを返します。
        /// また引数が０以下の場合でもnullを返します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static Clinic GetClinic(long code)
        {
            if (code <= 0) return null;
            var sql = $"SELECT * FROM clinic WHERE code={code}";
            var result = DB.Main.Query<Clinic>(sql);
            if (result == null || result.Count() == 0) return null;
            return result.ToList()[0];
        }

        public static Clinic GetClinic(string code)
        {
            if (code.Length < 1 || !long.TryParse(code, out long v)) return null;
            var sql = $"SELECT * FROM clinic WHERE code={v}";
            var result = DB.Main.Query<Clinic>(sql);
            if (result == null || result.Count() == 0) return null;
            return result.ToList()[0];
        }

        /// <summary>
        /// 施術所（師）情報を登録します。
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Insert(DB.Transaction tran = null)
        {
            var sql = "INSERT INTO clinic (code, name, insert_date, insert_cym) " +
                "VALUES (@code, @name, @insert_date, @insert_cym)";
            if (tran != null) return DB.Main.Excute(sql, this, tran);
            return DB.Main.Excute(sql, this);
        }

        /// <summary>
        /// 施術所（師）情報を更新します。
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Update(DB.Transaction tran = null)
        {
            var sql = "UPDATE clinic " +
                $"SET name=@name, update_date=@update_date, update_cym=@update_cym, verified=@verified " +
                "WHERE code=@code";
            if (tran != null) return DB.Main.Excute(sql, this, tran);
            return DB.Main.Excute(sql, this);
        }

        /// <summary>
        /// 施術所（師）情報の確定フラグを更新します。（更新日時と更新年月を含む）
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool UpdateVerified(DB.Transaction tran = null)
        {
            var sql = "UPDATE clinic " +
                "SET update_date=@update_date, update_cym=@update_cym, verified=@verified " +
                "WHERE code=@code";
            if (tran != null) return DB.Main.Excute(sql, this, tran);
            return DB.Main.Excute(sql, this);
        }

    }
}
