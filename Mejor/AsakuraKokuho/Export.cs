﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.AsakuraKokuho
{
    class Export
    {
        public static bool ImageExport(int cym)
        {
            var d = new OpenDirectoryDiarog();
            if (d.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;
            var dir = d.Name;

            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                wf.LogPrint("申請書を取得中です");
                var fc = new TiffUtility.FastCopy();
                var l = App.GetApps(cym);
                int lastaid = 0;
                int page = 0;

                wf.SetMax(l.Count);
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.LogPrint("画像の保存中です");
                foreach (var item in l)
                {
                    wf.InvokeValue++;
                    if (item.AppType > 0)
                    {
                        lastaid = item.Aid;
                        page = 0;
                        fc.FileCopy(item.GetImageFullPath(),
                            $"{dir}\\{lastaid.ToString().PadLeft(10, '0')}.tif");
                    }
                    else if (item.AppType == APP_TYPE.続紙)
                    {
                        page++;
                        fc.FileCopy(item.GetImageFullPath(),
                            $"{dir}\\{lastaid.ToString().PadLeft(10, '0')}-{page.ToString("000")}.tif");
                    }
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            return true;
        }

        public static bool ListExport(List<App> apps, string fileName)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.UTF8))
                {
                    //先頭行は見出し
                    var h = "ID,処理年,処理月,診療年,診療月,保険者番号,被保険者番号," +
                    "住所,氏名,性別,本人家族,生年月日,年齢,請求区分,負傷名1,負傷名2,負傷名3,負傷名4,負傷名5," +
                    "施術所記号,合計金額,請求金額,診療日数,ナンバリング,照会理由,点検結果";

                    sw.WriteLine(h);
                    var l = new List<string>();

                    foreach (var item in apps)
                    {
                        l.Clear();

                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応
                        var dt = new DateTime(DateTimeEx.GetAdYearFromHs(item.MediYear *100 + item.MediMonth), item.MediMonth, 1);
                        //var dt = new DateTime(DateTimeEx.GetAdYearFromHs(item.MediYear), item.MediMonth, 1);

                        l.Add(item.Aid.ToString());
                        l.Add(item.ChargeYear.ToString());
                        l.Add(item.ChargeMonth.ToString());
                        l.Add(item.MediYear.ToString());
                        l.Add(item.MediMonth.ToString());
                        l.Add(item.InsNum);
                        l.Add(item.HihoNum);
                        l.Add(item.HihoAdd);
                        l.Add(item.PersonName);
                        l.Add(item.Sex == 1 ? "男" : "女");
                        l.Add(item.Family == 2 ? "本人" : item.Family == 6 ? "家族" : "");
                        l.Add(item.Birthday.ToShortDateString());
                        l.Add(DateTimeEx.GetAge(item.Birthday, dt).ToString());
                        l.Add(item.NewContType == NEW_CONT.新規 ? "新規" : "継続");
                        l.Add(item.FushoName1);
                        l.Add(item.FushoName2);
                        l.Add(item.FushoName3);
                        l.Add(item.FushoName4);
                        l.Add(item.FushoName5);
                        l.Add(item.DrNum);
                        l.Add(item.Total.ToString());
                        l.Add(item.Charge.ToString());
                        l.Add(item.CountedDays.ToString());
                        l.Add(item.Numbering);
                        l.Add(item.ShokaiReasonStr);
                        l.Add(item.InspectInfo);

                        sw.WriteLine(string.Join(",", l));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("CSV出力に失敗しました。\r\n" + ex.Message);
                Log.ErrorWrite(ex);
                return false;
            }

            MessageBox.Show("CSV出力が完了しました。");
            return true;
        }
    }
}
