﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ToyohashiKokuho
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 画像座標定義
        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        
        Point posYM = new Point(0, 0);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 400);
        Point posFusho = new Point(100, 800);

        //20190902152014 furukawa st ////////////////////////
        //画像座標調整
        
        Point posCost = new Point(800, 1900);
        Point posDays = new Point(0, 400);
        //Point posCost = new Point(800, 2000);
        //Point posDays = new Point(600, 800);
        //20190902152014 furukawa ed ////////////////////////



        Point posNewCont = new Point(800, 1200);
        Point posOryo = new Point(400, 1200);
        Point posNumbering = new Point(800, 2600);
        Point posAcc = new Point(800, 2600);
#endregion

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls, newContControls, oryoControls, numberingControls,accControls;

        public InputForm(ScanGroup sGroup, bool firstTime, int aid)
        {
            InitializeComponent();


            #region コントロールグループ定義
            ymControls = new Control[] { verifyBoxY, verifyBoxM };
            hihoNumControls = new Control[] {   verifyBoxNumN,  verifyBoxJuryoType,verifyBoxFamily, };

            //hihoNumControls = new Control[] { verifyBoxNumM, verifyBoxNumN, verifyBoxJuryoType, verifyBoxFamily, };
            //hihoNumControls = new Control[] { verifyBoxInsNum2, verifyBoxNumM, verifyBoxNumN, verifyBoxKouhi, verifyBoxJuryoType, verifyBoxFamily, };

            personControls = new Control[] { verifyBoxPName,verifyBoxSex, verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD, };
            dayControls = new Control[] { verifyBoxDays, };
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge,  };

            fushoControls = new Control[] {  verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };
            //fushoControls = new Control[] { verifyBoxStart, verifyBoxFinish, verifyBoxF1, verifyBoxF2, verifyBoxF3, verifyBoxF4, verifyBoxF5, };

            newContControls = new Control[] { verifyBoxNewCont };
            oryoControls = new Control[] { verifyCheckBoxVisit ,verifyCheckBoxVisitKasan ,verifyBoxKasanKm,};
            numberingControls = new Control[] { verifyBoxNumbering };
           
            //accControls = new Control[] { verifyBoxAccNumber ,verifyBoxDrNum};
            accControls = new Control[] { verifyBoxAccNumber, verifyBoxDrNum ,verifyBoxAccName,verifyBoxClinicName};
            #endregion

            #region イベント設定


            //生年月日テキストイベント設定
            verifyBoxBE.Validated += new EventHandler(GetPName);
            verifyBoxBY.Validated += new EventHandler(GetPName);
            verifyBoxBM.Validated += new EventHandler(GetPName);
            verifyBoxBD.Validated += new EventHandler(GetPName);
            verifyBoxSex.Validated += new EventHandler(GetPName);

            //被保番
            verifyBoxNumN.Validated += new EventHandler(GetName);
            //口座番号
            verifyBoxAccNumber.Validated += new EventHandler(GetName);
            //柔整師登録番号
            verifyBoxDrNum.Validated += new EventHandler(GetName);

            //各テキストボックスにEnterイベント設定
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);
            
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;

            #region 申請書リスト設定
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();
        }

        #region 現在の申請書を設定
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);
        }
        #endregion


        /// <summary>
        /// Enterイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (numberingControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
            else if (accControls.Contains(t)) scrollPictureControl1.ScrollPosition = posAcc;

        }

        #region データ登録関連

        /// <summary>
        /// 登録
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }


    
   

        
        /// <summary>
        /// 入力チェック・クラスへの登録：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            #region 入力チェック

            //年
            int year = verifyBoxY.GetIntValue();

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            ////保険者番号上2ケタ
            //var insNum = verifyBoxInsNum2.Text.Trim();
            //setStatus(verifyBoxInsNum2, insNum != "06" && insNum != "63");

            ////公費コード
            //var kouhi = verifyBoxKouhi.GetIntValue();
            //kouhi = kouhi == (int)VerifyBox.ERROR_CODE.NULL ? 0 : kouhi;
            //setStatus(verifyBoxKouhi, (verifyBoxKouhi.Text.Trim().Length > 0 &&
            //    verifyBoxKouhi.Text.Trim().Length != 2) || kouhi < 0);

            //被保険者番号チェック 1文字以上かつ数字に直せること
            // int numM = verifyBoxNumM.GetIntValue();
            //setStatus(verifyBoxNumM, numM < 1);
            //被保険者番号チェック 1文字以上かつ数字に直せる、6桁以下である
            int numN = verifyBoxNumN.GetIntValue();
            setStatus(verifyBoxNumN, numN < 1 || numN.ToString().Length>6);


            //2019/08/14furukawa追加
            //被保険者名チェック　姓名の間は全角スペース
            string strHimoName = verifyBoxFName.Text;
            setStatus(verifyBoxFName, !verifyBoxFName.Text.Contains('　'));

            //2019/08/15furukawa追加
            //受療者名チェック　姓名の間は全角スペース
            string strJuryoName = verifyBoxPName.Text;
            setStatus(verifyBoxPName, !verifyBoxPName.Text.Contains('　'));

            //受診者区分（本家区分）
            int juryoType = verifyBoxJuryoType.GetIntValue();
            setStatus(verifyBoxJuryoType, !new[] { 0, 2, 4, 6, 8 }.Contains(juryoType));

            //本人家族区分
            int family = verifyBoxFamily.GetIntValue();
            setStatus(verifyBoxFamily, !new[] { 2, 6 }.Contains(family));

            //性別のチェック
            int sex = verifyBoxSex.GetIntValue();
            setStatus(verifyBoxSex, sex != 1 && sex != 2);

            //生年月日のチェック
            var birth = dateCheck(verifyBoxBE, verifyBoxBY, verifyBoxBM, verifyBoxBD);

            //初検日
            var firstDt = dateCheck( verifyBoxFirstY, verifyBoxFirstM);
            //var firstDt = dateCheck(4, verifyBoxFirstY, verifyBoxFirstM, verifyBoxFirstD);

            //開始日
            //var startDate = dateCheck(4, year, month, verifyBoxStart);

            //終了日
            //var finishDate = dateCheck(4, year, month, verifyBoxFinish);


            //実日数
            int days = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, days < 1 || 31 < days);

            //新規継続  →自動判定app
            //int newCont = verifyBoxNewCont.GetIntValue();
            //setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 || 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 100 || total < charge);

            //ナンバリング
            //setStatus(verifyBoxNumbering, verifyBoxNumbering.Text.Trim().Length != 7);

            //合計金額：請求金額：本家区分のトリプルチェック
            bool payError = false;
            if ((juryoType == 2 || juryoType == 6) && (int)(total * 70 / 100) != charge)
                payError = true;
            else if (juryoType == 8 && (int)(total * 80 / 100) != charge && (int)(total * 90 / 100) != charge)
                payError = true;
            else if (juryoType == 4 && (int)(total * 80 / 100) != charge)
                payError = true;

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //往料距離
            double inputDistance = 0;
            double.TryParse(verifyBoxKasanKm.Text, out inputDistance);
            //チェックがあって、0の場合はエラー
            if (verifyCheckBoxVisitKasan.Checked)
            {
                if (!string.IsNullOrWhiteSpace(verifyBoxKasanKm.Text) && inputDistance == 0)
                {
                    setStatus(verifyBoxKasanKm, true);
                }
            }



            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("受診者区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != System.Windows.Forms.DialogResult.OK) return false;
            }

            #endregion            

            #region appへの反映


            //値の反映
            app.MediYear = year;
            app.MediMonth = month;
            //app.InsNum = insNum;

            app.HihoNum =  verifyBoxNumN.Text.Trim();
            //app.HihoNum = verifyBoxNumM.Text.Trim() + "-" + verifyBoxNumN.Text.Trim();


            app.HihoName = strHimoName;//被保険者名2019/08/14furukawa追加
            app.PersonName = strJuryoName;//受療者名2019/08/15フルカワ

            app.Family = family * 100 + juryoType;   //受診者区分と本人家族をまとめて記録（受療者区分は０があるため、int登録すると3桁想定が1桁になってしまう
            //app.Family = juryoType * 100 + family;   //受診者区分と本人家族をまとめて記録
            app.Sex = sex;
            app.Birthday = birth;
           //app.PublcExpense = verifyBoxKouhi.Text.Trim();

            app.FushoFirstDate1 = firstDt;
            //app.FushoStartDate1 = startDate;
            //app.FushoFinishDate1 = finishDate;
            
            
   

            //診療年月＝初検日年月が同じ場合は新規とみなす
            app.NewContType = app.YM.ToString() == app.FushoFirstDate1.Year.ToString("00") + app.FushoFirstDate1.Month.ToString("00") ? NEW_CONT.新規 : NEW_CONT.継続;
            //app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //往料
            app.Distance = verifyCheckBoxVisit.Checked ? 999 : 0;
            //往料加算
            app.VisitAdd = Convert.ToInt32(inputDistance * 1000);

            app.CountedDays = days;
            app.Total = total;
            app.Charge = charge;


            //20191010194001 furukawa st ////////////////////////
            //ナンバリング不要



                        //20190827165351 furukawa st ////////////////////////
                        //取得はsetAppに移動

                        //app.Numbering = verifyBoxNumbering.Text.Trim();           

                        ////ナンバリング　yyyyMM12345
                        ////該当処理月で最大を取得する
                        //DB.Command cmd = new DB.Command(DB.Main, $"select max(substr(cast(numbering as varchar),7,5)) as m from application where cym={app.CYM}");
                        //var varnum = cmd.TryExecuteScalar();
                        //int intnum = 0;
                        //if (varnum == null) intnum = 0;
                        //else int.TryParse(varnum.ToString(), out intnum);
                        ////最大に+1
                        //app.Numbering = app.CYM + (intnum + 1).ToString().PadLeft(5, '0');


            
                        //app.Numbering = verifyBoxNumbering.Text.Trim();


                        //20190827165351 furukawa ed ////////////////////////

            //20191010194001 furukawa ed ////////////////////////

            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();
            
            app.AppType = scan.AppType;



            app.AccountName = verifyBoxAccName.Text.Trim();//口座名義
            app.ClinicName = verifyBoxClinicName.Text.Trim();//施術所名

            app.AccountNumber  = verifyBoxAccNumber.Text.Trim();//口座番号
            app.DrNum = verifyBoxDrNum.Text.Trim();//柔整師登録番号

            

            #endregion

            return true;
        }
        


        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }


        #endregion



        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);


            //20191010190450 furukawa st ////////////////////////
            //ナンバリングはスキャン時に登録するので無視

            //20190827165233 furukawa st ////////////////////////
            //ナンバリング取得し、テキストボックスにセット

            ////ナンバリング　グループID6桁0埋め＋ナンバリング
            ////該当処理月、グループIDで最大を取得する
            ///
            //int intnum = 0;
            //if (app.Numbering.ToString() == "0" || app.Numbering.ToString() == string.Empty)
            //{
            //    DB.Command cmd = new DB.Command(DB.Main,
            //        $"select max(cast(numbering as bigint)) as m from application " +
            //        $"where cym={app.CYM} and groupid='{app.GroupID}' and numbering ~'[0-9]'");

            //    var varnum = cmd.TryExecuteScalar();


            //    if (varnum == null) intnum = 0;
            //    else int.TryParse(varnum.ToString(), out intnum);

            //    if (intnum > 0) intnum = int.Parse(intnum.ToString().Substring(6));

            //    //最大に+1

            //    app.Numbering = app.GroupID.ToString("000000") + (intnum + 1).ToString().PadLeft(5, '0');
            //}




            //setValue(verifyBoxNumbering, app.Numbering, firstTime, false);

            //20191010190450 furukawa ed ////////////////////////



            //20190827165233 furukawa ed ////////////////////////



            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                //OCRデータがあれば、部位のみ挿入
                var ocr = app.OcrData.Split(',');
                verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5.Text = Fusho.GetFusho5(ocr);
            }
            //画像の表示
            setImage(app);
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }




        

        /// <summary>
        /// 名称取得（口座番号から口座名義、被保険者番号から被保険者名、柔整師登録番号から施術所名）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetName(object sender, EventArgs e)
        {
            string strFld = string.Empty;
            string strVal = string.Empty;
            string strSQL = string.Empty;
            VerifyBox vIn = new VerifyBox();
            vIn = (VerifyBox)sender;
            List<App> lstName=new List<App>();

            switch(vIn.Name)
            {
                case "verifyBoxAccNumber":
                    strSQL = $" where a.baccnumber = '{verifyBoxAccNumber.Text.Trim()}' ";
                    
                    lstName = App.GetAppsWithLimit(strSQL, 1, false);
                    if (lstName.Count > 0) verifyBoxAccName.Text = lstName[0].AccountName;

                    break;


                case "verifyBoxNumN":
                    strSQL = $" where a.hnum = '{verifyBoxNumN.Text.Trim()}' and a.hname is not null"; 

                    lstName = App.GetAppsWithLimit(strSQL, 1, false);
                    if (lstName.Count > 0) verifyBoxFName.Text = lstName[0].HihoName;

                    break;

                case "verifyBoxDrNum":
                    strSQL = $" where a.sregnumber = '{verifyBoxDrNum.Text.Trim()}'";

                    lstName = App.GetAppsWithLimit(strSQL, 1, false);
                    if (lstName.Count > 0) verifyBoxClinicName.Text = lstName[0].ClinicName;

                    break;


                default:
                    break;
                
                    }
           
        }



        /// <summary>
        /// 生年月日と被保番と性別で受療者名を取得する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetPName(object sender,EventArgs e)
        {
            try
            {
                if (verifyBoxBE.Text != string.Empty &&
                    verifyBoxBY.Text != string.Empty &&
                    verifyBoxBM.Text != string.Empty &&
                    verifyBoxBD.Text != string.Empty &&
                    verifyBoxSex.Text != string.Empty &&
                    verifyBoxNumN.Text != string.Empty)
                {

                    int warekiYM = int.Parse(verifyBoxBE.Text + verifyBoxBY.Text.PadLeft(2,'0') + verifyBoxBM.Text.PadLeft(2,'0'));
                    int adYearMonth = DateTimeEx.GetAdYearMonthFromJyymm (warekiYM);
                    string datetimeAD = adYearMonth + verifyBoxBD.Text.PadLeft(2, '0');

                    DateTime dt = DateTimeEx.ToDateTime(datetimeAD);

                    var lstName = App.GetAppsWithLimit(
                        $" where a.hnum = '{verifyBoxNumN.Text.Trim()}' and pbirthday='{dt.ToString("yyyy-MM-dd")}' and psex='{verifyBoxSex.Text.Trim()}'", 1, false);


                    if (lstName.Count > 0) verifyBoxPName.Text = lstName[0].PersonName ;
                }

            }
            catch (Exception ex)
            {
                //ない場合は無視
            }

        }


        /// <summary>
        /// 本人家族自動判定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxJuryoType_TextChanged(object sender, EventArgs e)
        {

            //20190901180913 furukawa st ////////////////////////
            //例外が多いので自動にしない 8/28伸作さん要望

            //  //受診者区分が2.本人の場合は本家区分も2.本人とする
            //if (verifyBoxJuryoType.Text.ToString() == "2") {
            //    verifyBoxFamily.Text = "2";
            //}
            ////受診者区分が2.本人以外の場合は、基本的に本家区分6.家族とする。入力にて変更可能
            //else if (verifyBoxJuryoType.Text.ToString() != "2" && verifyBoxJuryoType.Text!=string.Empty)
            //{
            //    verifyBoxFamily.Text = "6";
            //}
            //20190901180913 furukawa ed ////////////////////////
        }



        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var cs = new Control[] { verifyBoxY, labelYearInfo, labelH, labelY,
                labelInputerName};

            //隠したままにするコントロール
            var hidecs = new Control[] {
                verifyBoxStart,verifyBoxFinish,verifyBoxNewCont,verifyBoxNumbering,
                lblNumbering,lblStart,lbldayStart,lblFinish,lbldayFinish,
                lblNewCont,lblNewContNO,
                //verifyBoxFamily,lblFamily,lblFamilyNo,    //本人家族入れさせる
                verifyBoxFirstD,lblFirstDay            };


            //visible設定
            var visible = new Action<bool>(b =>
            {
                foreach (Control item in panelRight.Controls)
                {
                    if (!(item is IVerifiable || item is Label)) continue;
                    if (cs.Contains(item)) continue;
                    if (hidecs.Contains(item)) continue;
                    item.Visible = b;
                }
            });

            if ((verifyBoxY.Text == "--" || verifyBoxY.Text == "++") && verifyBoxY.Text.Length == 2)
            {
                //続紙その他
                visible(false);
            }
            else
            {
                //申請書の場合
                visible(true);
            }
        }


        /// <summary>
        /// 口座番号7桁ゼロ埋め
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxAccNumber_Validated(object sender, EventArgs e)
        {
            if (verifyBoxAccNumber.Text != string.Empty)
                verifyBoxAccNumber.Text = verifyBoxAccNumber.Text.PadLeft(7, '0');
            
        }


        #region 画像操作関連
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void verifyBoxPName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\n')//[Ctrl]+[Enter]キー
            {
                if (verifyBoxFName.Text.Trim() != string.Empty)
                {
                    verifyBoxPName.Text = verifyBoxFName.Text.Trim();
                    SelectNextControl(verifyBoxPName, true, true,false,false );
                    return;
                }
              
            }
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (numberingControls.Contains(t)) posNumbering = pos;
        }

      

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }


        /// <summary>
        /// 画像のボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }
        #endregion



        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            if(!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //申請書
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);           
                setValue(verifyBoxNumN,  app.HihoNum , firstTime, nv);               
                setValue(verifyBoxFName, app.HihoName, firstTime, nv);
                setValue(verifyBoxPName, app.PersonName, firstTime, nv);                
                setValue(verifyBoxJuryoType, app.Family / 100, firstTime, nv);

                //家族区分 本家区分には０がないので、3桁のうち前に来ている。受療者区分は０：高７があり、intなので桁が落ちてエラーになるため
                if (app.Family.ToString().Length == 3) setValue(verifyBoxJuryoType, app.Family.ToString().Substring(2, 1), firstTime, nv);
                //本家区分
                setValue(verifyBoxFamily, app.Family.ToString().Substring(0, 1), firstTime, nv);
                

                setValue(verifyBoxSex, app.Sex, firstTime, nv);
                setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv);
                setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv);
                setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv);
                setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv);                
                setValue(verifyBoxFirstY, DateTimeEx.GetJpYear(app.FushoFirstDate1), firstTime, nv);
                setValue(verifyBoxFirstM, app.FushoFirstDate1.Month, firstTime, nv);
                setValue(verifyBoxFirstD, app.FushoFirstDate1.Day, firstTime, nv);
                setValue(verifyBoxStart, app.FushoStartDate1.Day, firstTime, nv);
                setValue(verifyBoxFinish, app.FushoFinishDate1.Day, firstTime, nv);
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
                setValue(verifyBoxNewCont, app.NewContType == NEW_CONT.新規 ? "1" : "2", firstTime, false);



                //以下の項目もベリファイ入力にする
                setValue(verifyCheckBoxVisit, app.Distance == 999, firstTime, nv);
                //setValue(verifyCheckBoxOryo, app.Distance == 999, firstTime, false);


                //大阪広域より visitaddは加算距離が0超であればtrue
                //verifyCheckBoxVisitKasan.Checked = 0 != app.VisitAdd;
                setValue(verifyCheckBoxVisitKasan, app.VisitAdd != 0, firstTime, nv);
                if (app.VisitAdd != 0)
                {
                    //verifyBoxKasanKm.Text = (app.VisitAdd / 1000m).ToString();
                    setValue(verifyBoxKasanKm, (app.VisitAdd / 1000m).ToString(), firstTime, nv);
                }

                setValue(verifyBoxTotal, app.Total, firstTime, nv);
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);



                //以下の項目もベリファイ入力にする
                setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
                setValue(verifyBoxF2, app.FushoName2, firstTime, nv);
                setValue(verifyBoxF3, app.FushoName3, firstTime, nv);
                setValue(verifyBoxF4, app.FushoName4, firstTime, nv);
                setValue(verifyBoxF5, app.FushoName5, firstTime, nv);


                //２回め入力時、1回め入力の値を表示
                if (!firstTime)
                {
                    setValue(verifyBoxF1, app.FushoName1, true, nv);
                    setValue(verifyBoxF2, app.FushoName2, true, nv);
                    setValue(verifyBoxF3, app.FushoName3, true, nv);
                    setValue(verifyBoxF4, app.FushoName4, true, nv);
                    setValue(verifyBoxF5, app.FushoName5, true, nv);
                }


                
                setValue(verifyBoxAccNumber, app.AccountNumber, firstTime, nv);//口座番号


                //20190902162307 furukawa st ////////////////////////
                //DB修正で対応するので削除

                        //20190902125411 furukawa st ////////////////////////
                        //口座番号がない場合、1回め登録時に0が勝手に入るのでベリファイ時もそれに合わせる

                        //if (!firstTime)
                        //{
                        //    int tmpAccountNumber = 0;
                        //    int.TryParse(app.AccountNumber, out tmpAccountNumber);
                        //    if (tmpAccountNumber == 0)
                        //    {
                        //        setValue(verifyBoxAccNumber, 0.ToString("0000000"), !firstTime, nv);//口座番号
                        //    }
                        //    else
                        //    {
                        //        setValue(verifyBoxAccNumber, app.AccountNumber, firstTime, nv);//口座番号
                        //    }
                        //}
                        ////20190902125411 furukawa ed ////////////////////////

                //20190902162307 furukawa ed ////////////////////////

                setValue(verifyBoxAccName, app.AccountName, firstTime, nv);//口座名義
                setValue(verifyBoxDrNum,app.DrNum , firstTime, nv);//柔整師登録番号
                setValue(verifyBoxClinicName, app.ClinicName, firstTime, nv);//施術所名

                //setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                //setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                //setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                //setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                //setValue(verifyBoxF5, app.FushoName5, firstTime, false);

                //setValue(verifyBoxAccNumber, app.AccountNumber, firstTime, false);//口座番号
                //setValue(verifyBoxAccName, app.AccountName, firstTime, false);//口座名義
                //setValue(verifyBoxDrNum, app.DrNum, firstTime, false);//柔整師登録番号
                //setValue(verifyBoxClinicName, app.ClinicName, firstTime, false);//施術所名


            }
            missCounterReset();
        }


        #region フォームイベント
        /// <summary>
        /// フォームのキーイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        #endregion


        private void checkBoxVisitControls_CheckedChanged(object sender, EventArgs e)
        {
            oryoControlsAdjust();
        }

        private void oryoControlsAdjust()
        {
            var app = (App)bsApp.Current;

            var setCanInput = new Action<Control, bool>((t, b) =>
            {
                if (t is TextBox) ((TextBox)t).ReadOnly = !b;
                t.Enabled = b;

                t.TabStop = b;
                if (!b)
                    t.BackColor = SystemColors.Menu;
                else
                    t.BackColor = SystemColors.Info;
            });

            if (!verifyCheckBoxVisit.Checked)
            {
                setCanInput(verifyCheckBoxVisitKasan, false);
                labelOryoKasan.Enabled = false;
                setCanInput(verifyBoxKasanKm, false);
                labelOryoKm.Enabled = false;
                //labelOryoCost.Enabled = false;
                //setCanInput(verifyBoxOryoCost, false);
                //labelKasanCost.Enabled = false;
                //setCanInput(verifyBoxKasanCost, false);
            }
            else
            {
                if (!app.StatusFlagCheck(StatusFlag.ベリファイ済))
                setCanInput(verifyCheckBoxVisitKasan, true);
                var kc = verifyCheckBoxVisitKasan.Checked;

                labelOryoKasan.Enabled = kc;
                setCanInput(verifyBoxKasanKm, kc);
                labelOryoKm.Enabled = kc;
                //labelOryoCost.Enabled = kc;
                //setCanInput(verifyBoxOryoCost, kc);
                //labelKasanCost.Enabled = kc;
                //setCanInput(verifyBoxKasanCost, kc);
            }
        }


        //負傷名未入力のときは次の負傷名欄を飛ばす
        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
