﻿using System;

namespace Mejor.ToyohashiKokuho
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.labelH = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.labelHnameCopy = new System.Windows.Forms.Label();
            this.labelOryoKasan = new System.Windows.Forms.Label();
            this.labelOryoKm = new System.Windows.Forms.Label();
            this.verifyBoxKasanKm = new Mejor.VerifyBox();
            this.verifyCheckBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxAccName = new Mejor.VerifyBox();
            this.label33 = new System.Windows.Forms.Label();
            this.verifyBoxClinicName = new Mejor.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxPName = new Mejor.VerifyBox();
            this.label30 = new System.Windows.Forms.Label();
            this.verifyBoxFName = new Mejor.VerifyBox();
            this.verifyBoxAccNumber = new Mejor.VerifyBox();
            this.label29 = new System.Windows.Forms.Label();
            this.verifyBoxDrNum = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblNewContNO = new System.Windows.Forms.Label();
            this.verifyCheckBoxVisit = new Mejor.VerifyCheckBox();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxNumN = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxFamily = new Mejor.VerifyBox();
            this.verifyBoxJuryoType = new Mejor.VerifyBox();
            this.verifyBoxBD = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.verifyBoxBM = new Mejor.VerifyBox();
            this.verifyBoxNumbering = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxBY = new Mejor.VerifyBox();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxBE = new Mejor.VerifyBox();
            this.verifyBoxNewCont = new Mejor.VerifyBox();
            this.verifyBoxFinish = new Mejor.VerifyBox();
            this.verifyBoxFirstD = new Mejor.VerifyBox();
            this.verifyBoxFirstM = new Mejor.VerifyBox();
            this.verifyBoxFirstY = new Mejor.VerifyBox();
            this.verifyBoxStart = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxSex = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblFamily = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.lblNewCont = new System.Windows.Forms.Label();
            this.lblFinish = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.lblFirstDay = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbldayFinish = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.labelYearInfo = new System.Windows.Forms.Label();
            this.lbldayStart = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelSex = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.lblNumbering = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.lblFamilyNo = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(888, 869);
            this.buttonRegist.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(120, 29);
            this.buttonRegist.TabIndex = 90;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(168, 40);
            this.labelY.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(22, 15);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(235, 40);
            this.labelM.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(37, 15);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(287, 6);
            this.labelHnum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(97, 15);
            this.labelHnum.TabIndex = 12;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Location = new System.Drawing.Point(85, 40);
            this.labelH.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(37, 15);
            this.labelH.TabIndex = 1;
            this.labelH.Text = "和暦";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(160, 876);
            this.labelImageName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(80, 15);
            this.labelImageName.TabIndex = 2;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(289, 0);
            this.panelLeft.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(143, 901);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(143, 901);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(104, 870);
            this.buttonImageChange.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(53, 29);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(7, 870);
            this.buttonImageRotateL.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(47, 29);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(55, 870);
            this.buttonImageRotateR.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(47, 29);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(32, 870);
            this.buttonImageFill.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(100, 29);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(5);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(143, 901);
            this.userControlImage1.TabIndex = 1;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.labelHnameCopy);
            this.panelRight.Controls.Add(this.labelOryoKasan);
            this.panelRight.Controls.Add(this.labelOryoKm);
            this.panelRight.Controls.Add(this.verifyBoxKasanKm);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisitKasan);
            this.panelRight.Controls.Add(this.label2);
            this.panelRight.Controls.Add(this.verifyBoxAccName);
            this.panelRight.Controls.Add(this.label33);
            this.panelRight.Controls.Add(this.verifyBoxClinicName);
            this.panelRight.Controls.Add(this.label31);
            this.panelRight.Controls.Add(this.verifyBoxPName);
            this.panelRight.Controls.Add(this.label30);
            this.panelRight.Controls.Add(this.verifyBoxFName);
            this.panelRight.Controls.Add(this.verifyBoxAccNumber);
            this.panelRight.Controls.Add(this.label29);
            this.panelRight.Controls.Add(this.verifyBoxDrNum);
            this.panelRight.Controls.Add(this.label1);
            this.panelRight.Controls.Add(this.label32);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.lblNewContNO);
            this.panelRight.Controls.Add(this.verifyCheckBoxVisit);
            this.panelRight.Controls.Add(this.verifyBoxF5);
            this.panelRight.Controls.Add(this.verifyBoxF4);
            this.panelRight.Controls.Add(this.verifyBoxNumN);
            this.panelRight.Controls.Add(this.verifyBoxF3);
            this.panelRight.Controls.Add(this.verifyBoxFamily);
            this.panelRight.Controls.Add(this.verifyBoxJuryoType);
            this.panelRight.Controls.Add(this.verifyBoxBD);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.verifyBoxBM);
            this.panelRight.Controls.Add(this.verifyBoxNumbering);
            this.panelRight.Controls.Add(this.verifyBoxCharge);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxTotal);
            this.panelRight.Controls.Add(this.verifyBoxBY);
            this.panelRight.Controls.Add(this.verifyBoxDays);
            this.panelRight.Controls.Add(this.verifyBoxF2);
            this.panelRight.Controls.Add(this.verifyBoxBE);
            this.panelRight.Controls.Add(this.verifyBoxNewCont);
            this.panelRight.Controls.Add(this.verifyBoxFinish);
            this.panelRight.Controls.Add(this.verifyBoxFirstD);
            this.panelRight.Controls.Add(this.verifyBoxFirstM);
            this.panelRight.Controls.Add(this.verifyBoxFirstY);
            this.panelRight.Controls.Add(this.verifyBoxStart);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.verifyBoxSex);
            this.panelRight.Controls.Add(this.verifyBoxF1);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.label22);
            this.panelRight.Controls.Add(this.lblFamily);
            this.panelRight.Controls.Add(this.labelHnum);
            this.panelRight.Controls.Add(this.label13);
            this.panelRight.Controls.Add(this.label21);
            this.panelRight.Controls.Add(this.label20);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label11);
            this.panelRight.Controls.Add(this.labelBirthday);
            this.panelRight.Controls.Add(this.lblNewCont);
            this.panelRight.Controls.Add(this.lblFinish);
            this.panelRight.Controls.Add(this.label26);
            this.panelRight.Controls.Add(this.label14);
            this.panelRight.Controls.Add(this.lblStart);
            this.panelRight.Controls.Add(this.lblFirstDay);
            this.panelRight.Controls.Add(this.label27);
            this.panelRight.Controls.Add(this.lbldayFinish);
            this.panelRight.Controls.Add(this.label25);
            this.panelRight.Controls.Add(this.labelYearInfo);
            this.panelRight.Controls.Add(this.lbldayStart);
            this.panelRight.Controls.Add(this.label9);
            this.panelRight.Controls.Add(this.labelSex);
            this.panelRight.Controls.Add(this.labelH);
            this.panelRight.Controls.Add(this.label5);
            this.panelRight.Controls.Add(this.labelDays);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.label4);
            this.panelRight.Controls.Add(this.labelTotal);
            this.panelRight.Controls.Add(this.lblNumbering);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelCharge);
            this.panelRight.Controls.Add(this.lblFamilyNo);
            this.panelRight.Controls.Add(this.label16);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.label18);
            this.panelRight.Controls.Add(this.label17);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(432, 0);
            this.panelRight.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panelRight.MinimumSize = new System.Drawing.Size(880, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1360, 901);
            this.panelRight.TabIndex = 1;
            // 
            // labelHnameCopy
            // 
            this.labelHnameCopy.AutoSize = true;
            this.labelHnameCopy.Location = new System.Drawing.Point(961, 73);
            this.labelHnameCopy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelHnameCopy.Name = "labelHnameCopy";
            this.labelHnameCopy.Size = new System.Drawing.Size(218, 15);
            this.labelHnameCopy.TabIndex = 96;
            this.labelHnameCopy.Text = "Ctrl+Enter （被保険者氏名 貼付）";
            this.labelHnameCopy.Visible = false;
            // 
            // labelOryoKasan
            // 
            this.labelOryoKasan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOryoKasan.AutoSize = true;
            this.labelOryoKasan.Enabled = false;
            this.labelOryoKasan.Location = new System.Drawing.Point(652, 687);
            this.labelOryoKasan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOryoKasan.Name = "labelOryoKasan";
            this.labelOryoKasan.Size = new System.Drawing.Size(37, 15);
            this.labelOryoKasan.TabIndex = 93;
            this.labelOryoKasan.Text = "加算";
            // 
            // labelOryoKm
            // 
            this.labelOryoKm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOryoKm.AutoSize = true;
            this.labelOryoKm.Enabled = false;
            this.labelOryoKm.Location = new System.Drawing.Point(759, 687);
            this.labelOryoKm.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOryoKm.Name = "labelOryoKm";
            this.labelOryoKm.Size = new System.Drawing.Size(25, 15);
            this.labelOryoKm.TabIndex = 95;
            this.labelOryoKm.Text = "km";
            // 
            // verifyBoxKasanKm
            // 
            this.verifyBoxKasanKm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxKasanKm.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKasanKm.Enabled = false;
            this.verifyBoxKasanKm.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKasanKm.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.verifyBoxKasanKm.Location = new System.Drawing.Point(697, 672);
            this.verifyBoxKasanKm.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxKasanKm.Name = "verifyBoxKasanKm";
            this.verifyBoxKasanKm.NewLine = false;
            this.verifyBoxKasanKm.Size = new System.Drawing.Size(60, 27);
            this.verifyBoxKasanKm.TabIndex = 35;
            this.verifyBoxKasanKm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKasanKm.TextV = "";
            // 
            // verifyCheckBoxVisitKasan
            // 
            this.verifyCheckBoxVisitKasan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisitKasan.CheckedV = false;
            this.verifyCheckBoxVisitKasan.Enabled = false;
            this.verifyCheckBoxVisitKasan.Location = new System.Drawing.Point(547, 672);
            this.verifyCheckBoxVisitKasan.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyCheckBoxVisitKasan.Name = "verifyCheckBoxVisitKasan";
            this.verifyCheckBoxVisitKasan.Padding = new System.Windows.Forms.Padding(9, 3, 0, 0);
            this.verifyCheckBoxVisitKasan.Size = new System.Drawing.Size(101, 31);
            this.verifyCheckBoxVisitKasan.TabIndex = 34;
            this.verifyCheckBoxVisitKasan.Text = "加算あり";
            this.verifyCheckBoxVisitKasan.UseVisualStyleBackColor = false;
            this.verifyCheckBoxVisitKasan.CheckedChanged += new System.EventHandler(this.checkBoxVisitControls_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.Location = new System.Drawing.Point(929, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 45);
            this.label2.TabIndex = 92;
            this.label2.Text = "2.本人 8.高一 \r\n4.六歳\r\n6.家族 0.高７";
            // 
            // verifyBoxAccName
            // 
            this.verifyBoxAccName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxAccName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxAccName.Location = new System.Drawing.Point(419, 743);
            this.verifyBoxAccName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxAccName.Name = "verifyBoxAccName";
            this.verifyBoxAccName.NewLine = false;
            this.verifyBoxAccName.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxAccName.TabIndex = 56;
            this.verifyBoxAccName.TextV = "";
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(421, 722);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 15);
            this.label33.TabIndex = 82;
            this.label33.Text = "口座名義";
            // 
            // verifyBoxClinicName
            // 
            this.verifyBoxClinicName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxClinicName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxClinicName.Location = new System.Drawing.Point(953, 743);
            this.verifyBoxClinicName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxClinicName.Name = "verifyBoxClinicName";
            this.verifyBoxClinicName.NewLine = false;
            this.verifyBoxClinicName.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxClinicName.TabIndex = 61;
            this.verifyBoxClinicName.TextV = "";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(630, 70);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(52, 30);
            this.label31.TabIndex = 80;
            this.label31.Text = "療養者\r\n氏名";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxPName
            // 
            this.verifyBoxPName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxPName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxPName.Location = new System.Drawing.Point(685, 68);
            this.verifyBoxPName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxPName.Name = "verifyBoxPName";
            this.verifyBoxPName.NewLine = false;
            this.verifyBoxPName.Size = new System.Drawing.Size(250, 31);
            this.verifyBoxPName.TabIndex = 24;
            this.verifyBoxPName.TextV = "";
            this.verifyBoxPName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.verifyBoxPName_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(440, 25);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(67, 30);
            this.label30.TabIndex = 78;
            this.label30.Text = "被保険者\r\n氏名";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxFName
            // 
            this.verifyBoxFName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFName.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxFName.Location = new System.Drawing.Point(521, 24);
            this.verifyBoxFName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFName.Name = "verifyBoxFName";
            this.verifyBoxFName.NewLine = false;
            this.verifyBoxFName.Size = new System.Drawing.Size(250, 31);
            this.verifyBoxFName.TabIndex = 4;
            this.verifyBoxFName.TextV = "";
            // 
            // verifyBoxAccNumber
            // 
            this.verifyBoxAccNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxAccNumber.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccNumber.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccNumber.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxAccNumber.Location = new System.Drawing.Point(253, 743);
            this.verifyBoxAccNumber.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxAccNumber.Name = "verifyBoxAccNumber";
            this.verifyBoxAccNumber.NewLine = false;
            this.verifyBoxAccNumber.Size = new System.Drawing.Size(156, 27);
            this.verifyBoxAccNumber.TabIndex = 55;
            this.verifyBoxAccNumber.TextV = "";
            this.verifyBoxAccNumber.Validated += new System.EventHandler(this.verifyBoxAccNumber_Validated);
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(251, 722);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(67, 15);
            this.label29.TabIndex = 76;
            this.label29.Text = "口座番号";
            // 
            // verifyBoxDrNum
            // 
            this.verifyBoxDrNum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDrNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrNum.Location = new System.Drawing.Point(739, 743);
            this.verifyBoxDrNum.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxDrNum.Name = "verifyBoxDrNum";
            this.verifyBoxDrNum.NewLine = false;
            this.verifyBoxDrNum.Size = new System.Drawing.Size(156, 27);
            this.verifyBoxDrNum.TabIndex = 60;
            this.verifyBoxDrNum.TextV = "";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(896, 741);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 30);
            this.label1.TabIndex = 75;
            this.label1.Text = "[協]: 0\r\n[契]: 1";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(956, 722);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(67, 15);
            this.label32.TabIndex = 73;
            this.label32.Text = "施術所名";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(736, 722);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 15);
            this.label3.TabIndex = 73;
            this.label3.Text = "柔整師登録番号";
            // 
            // lblNewContNO
            // 
            this.lblNewContNO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNewContNO.AutoSize = true;
            this.lblNewContNO.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblNewContNO.Location = new System.Drawing.Point(1279, 863);
            this.lblNewContNO.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNewContNO.Name = "lblNewContNO";
            this.lblNewContNO.Size = new System.Drawing.Size(53, 30);
            this.lblNewContNO.TabIndex = 51;
            this.lblNewContNO.Text = "新規：1\r\n継続：2";
            this.lblNewContNO.Visible = false;
            // 
            // verifyCheckBoxVisit
            // 
            this.verifyCheckBoxVisit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyCheckBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.verifyCheckBoxVisit.CheckedV = false;
            this.verifyCheckBoxVisit.Location = new System.Drawing.Point(424, 672);
            this.verifyCheckBoxVisit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyCheckBoxVisit.Name = "verifyCheckBoxVisit";
            this.verifyCheckBoxVisit.Padding = new System.Windows.Forms.Padding(9, 3, 0, 0);
            this.verifyCheckBoxVisit.Size = new System.Drawing.Size(111, 31);
            this.verifyCheckBoxVisit.TabIndex = 33;
            this.verifyCheckBoxVisit.Text = "往療あり";
            this.verifyCheckBoxVisit.UseVisualStyleBackColor = false;
            this.verifyCheckBoxVisit.CheckedChanged += new System.EventHandler(this.checkBoxVisitControls_CheckedChanged);
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(336, 839);
            this.verifyBoxF5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxF5.TabIndex = 75;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(16, 839);
            this.verifyBoxF4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxF4.TabIndex = 74;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxNumN
            // 
            this.verifyBoxNumN.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumN.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumN.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumN.Location = new System.Drawing.Point(284, 27);
            this.verifyBoxNumN.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxNumN.Name = "verifyBoxNumN";
            this.verifyBoxNumN.NewLine = false;
            this.verifyBoxNumN.Size = new System.Drawing.Size(88, 27);
            this.verifyBoxNumN.TabIndex = 3;
            this.verifyBoxNumN.TextV = "";
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(656, 790);
            this.verifyBoxF3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxF3.TabIndex = 73;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxFamily
            // 
            this.verifyBoxFamily.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFamily.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFamily.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFamily.Location = new System.Drawing.Point(1063, 29);
            this.verifyBoxFamily.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFamily.Name = "verifyBoxFamily";
            this.verifyBoxFamily.NewLine = false;
            this.verifyBoxFamily.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxFamily.TabIndex = 6;
            this.verifyBoxFamily.TextV = "";
            // 
            // verifyBoxJuryoType
            // 
            this.verifyBoxJuryoType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxJuryoType.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxJuryoType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxJuryoType.Location = new System.Drawing.Point(873, 27);
            this.verifyBoxJuryoType.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxJuryoType.Name = "verifyBoxJuryoType";
            this.verifyBoxJuryoType.NewLine = false;
            this.verifyBoxJuryoType.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxJuryoType.TabIndex = 5;
            this.verifyBoxJuryoType.TextV = "";
            this.verifyBoxJuryoType.TextChanged += new System.EventHandler(this.verifyBoxJuryoType_TextChanged);
            // 
            // verifyBoxBD
            // 
            this.verifyBoxBD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBD.Location = new System.Drawing.Point(547, 70);
            this.verifyBoxBD.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxBD.Name = "verifyBoxBD";
            this.verifyBoxBD.NewLine = false;
            this.verifyBoxBD.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxBD.TabIndex = 23;
            this.verifyBoxBD.TextV = "";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 120);
            this.scrollPictureControl1.Margin = new System.Windows.Forms.Padding(5);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(266, 157);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1358, 520);
            this.scrollPictureControl1.TabIndex = 33;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // verifyBoxBM
            // 
            this.verifyBoxBM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBM.Location = new System.Drawing.Point(489, 70);
            this.verifyBoxBM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxBM.Name = "verifyBoxBM";
            this.verifyBoxBM.NewLine = false;
            this.verifyBoxBM.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxBM.TabIndex = 22;
            this.verifyBoxBM.TextV = "";
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(1107, 809);
            this.verifyBoxNumbering.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.NewLine = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(95, 27);
            this.verifyBoxNumbering.TabIndex = 59;
            this.verifyBoxNumbering.TabStop = false;
            this.verifyBoxNumbering.TextV = "";
            this.verifyBoxNumbering.Visible = false;
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(139, 743);
            this.verifyBoxCharge.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(95, 27);
            this.verifyBoxCharge.TabIndex = 51;
            this.verifyBoxCharge.TextV = "";
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(760, 869);
            this.buttonBack.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(120, 29);
            this.buttonBack.TabIndex = 91;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(15, 743);
            this.verifyBoxTotal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(95, 27);
            this.verifyBoxTotal.TabIndex = 50;
            this.verifyBoxTotal.TextV = "";
            // 
            // verifyBoxBY
            // 
            this.verifyBoxBY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBY.Location = new System.Drawing.Point(432, 70);
            this.verifyBoxBY.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxBY.Name = "verifyBoxBY";
            this.verifyBoxBY.NewLine = false;
            this.verifyBoxBY.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxBY.TabIndex = 21;
            this.verifyBoxBY.TextV = "";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(223, 675);
            this.verifyBoxDays.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(52, 27);
            this.verifyBoxDays.TabIndex = 30;
            this.verifyBoxDays.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(336, 790);
            this.verifyBoxF2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxF2.TabIndex = 72;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxBE
            // 
            this.verifyBoxBE.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBE.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxBE.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxBE.Location = new System.Drawing.Point(364, 70);
            this.verifyBoxBE.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxBE.Name = "verifyBoxBE";
            this.verifyBoxBE.NewLine = false;
            this.verifyBoxBE.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxBE.TabIndex = 20;
            this.verifyBoxBE.TextV = "";
            // 
            // verifyBoxNewCont
            // 
            this.verifyBoxNewCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxNewCont.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNewCont.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxNewCont.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxNewCont.Location = new System.Drawing.Point(1241, 864);
            this.verifyBoxNewCont.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxNewCont.Name = "verifyBoxNewCont";
            this.verifyBoxNewCont.NewLine = false;
            this.verifyBoxNewCont.Size = new System.Drawing.Size(35, 27);
            this.verifyBoxNewCont.TabIndex = 50;
            this.verifyBoxNewCont.TabStop = false;
            this.verifyBoxNewCont.TextV = "";
            this.verifyBoxNewCont.Visible = false;
            // 
            // verifyBoxFinish
            // 
            this.verifyBoxFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFinish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFinish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFinish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFinish.Location = new System.Drawing.Point(1283, 809);
            this.verifyBoxFinish.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFinish.Name = "verifyBoxFinish";
            this.verifyBoxFinish.NewLine = false;
            this.verifyBoxFinish.Size = new System.Drawing.Size(35, 27);
            this.verifyBoxFinish.TabIndex = 45;
            this.verifyBoxFinish.TabStop = false;
            this.verifyBoxFinish.TextV = "";
            this.verifyBoxFinish.Visible = false;
            // 
            // verifyBoxFirstD
            // 
            this.verifyBoxFirstD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFirstD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstD.Location = new System.Drawing.Point(131, 675);
            this.verifyBoxFirstD.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFirstD.Name = "verifyBoxFirstD";
            this.verifyBoxFirstD.NewLine = false;
            this.verifyBoxFirstD.Size = new System.Drawing.Size(35, 27);
            this.verifyBoxFirstD.TabIndex = 27;
            this.verifyBoxFirstD.TextV = "";
            this.verifyBoxFirstD.Visible = false;
            // 
            // verifyBoxFirstM
            // 
            this.verifyBoxFirstM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFirstM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstM.Location = new System.Drawing.Point(72, 675);
            this.verifyBoxFirstM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFirstM.Name = "verifyBoxFirstM";
            this.verifyBoxFirstM.NewLine = false;
            this.verifyBoxFirstM.Size = new System.Drawing.Size(35, 27);
            this.verifyBoxFirstM.TabIndex = 26;
            this.verifyBoxFirstM.TextV = "";
            // 
            // verifyBoxFirstY
            // 
            this.verifyBoxFirstY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxFirstY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFirstY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFirstY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFirstY.Location = new System.Drawing.Point(13, 675);
            this.verifyBoxFirstY.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxFirstY.Name = "verifyBoxFirstY";
            this.verifyBoxFirstY.NewLine = false;
            this.verifyBoxFirstY.Size = new System.Drawing.Size(35, 27);
            this.verifyBoxFirstY.TabIndex = 25;
            this.verifyBoxFirstY.TextV = "";
            // 
            // verifyBoxStart
            // 
            this.verifyBoxStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxStart.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxStart.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxStart.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxStart.Location = new System.Drawing.Point(1211, 809);
            this.verifyBoxStart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxStart.Name = "verifyBoxStart";
            this.verifyBoxStart.NewLine = false;
            this.verifyBoxStart.Size = new System.Drawing.Size(35, 27);
            this.verifyBoxStart.TabIndex = 42;
            this.verifyBoxStart.TabStop = false;
            this.verifyBoxStart.TextV = "";
            this.verifyBoxStart.Visible = false;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(188, 27);
            this.verifyBoxM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(43, 27);
            this.verifyBoxM.TabIndex = 1;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxSex.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxSex.Location = new System.Drawing.Point(199, 74);
            this.verifyBoxSex.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.NewLine = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(33, 27);
            this.verifyBoxSex.TabIndex = 18;
            this.verifyBoxSex.TextV = "";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(16, 790);
            this.verifyBoxF1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(311, 27);
            this.verifyBoxF1.TabIndex = 71;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(124, 27);
            this.verifyBoxY.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(43, 27);
            this.verifyBoxY.TabIndex = 0;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(335, 820);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(60, 15);
            this.label22.TabIndex = 68;
            this.label22.Text = "負傷名5";
            // 
            // lblFamily
            // 
            this.lblFamily.AutoSize = true;
            this.lblFamily.Location = new System.Drawing.Point(1061, 8);
            this.lblFamily.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFamily.Name = "lblFamily";
            this.lblFamily.Size = new System.Drawing.Size(67, 15);
            this.lblFamily.TabIndex = 18;
            this.lblFamily.Text = "本人家族";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(859, 8);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 15;
            this.label13.Text = "本家区分";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 820);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 15);
            this.label21.TabIndex = 66;
            this.label21.Text = "負傷名4";
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(653, 773);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 15);
            this.label20.TabIndex = 64;
            this.label20.Text = "負傷名3";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(363, 869);
            this.labelInputerName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(248, 29);
            this.labelInputerName.TabIndex = 72;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(335, 773);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 15);
            this.label11.TabIndex = 62;
            this.label11.Text = "負傷名2";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(285, 73);
            this.labelBirthday.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(67, 15);
            this.labelBirthday.TabIndex = 24;
            this.labelBirthday.Text = "生年月日";
            // 
            // lblNewCont
            // 
            this.lblNewCont.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNewCont.AutoSize = true;
            this.lblNewCont.Location = new System.Drawing.Point(1239, 846);
            this.lblNewCont.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNewCont.Name = "lblNewCont";
            this.lblNewCont.Size = new System.Drawing.Size(67, 15);
            this.lblNewCont.TabIndex = 49;
            this.lblNewCont.Text = "新規継続";
            this.lblNewCont.Visible = false;
            // 
            // lblFinish
            // 
            this.lblFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFinish.AutoSize = true;
            this.lblFinish.Location = new System.Drawing.Point(1280, 790);
            this.lblFinish.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFinish.Name = "lblFinish";
            this.lblFinish.Size = new System.Drawing.Size(52, 15);
            this.lblFinish.TabIndex = 44;
            this.lblFinish.Text = "終了日";
            this.lblFinish.Visible = false;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(11, 655);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 15);
            this.label26.TabIndex = 34;
            this.label26.Text = "初検日";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(425, 655);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 15);
            this.label14.TabIndex = 52;
            this.label14.Text = "往療有無";
            // 
            // lblStart
            // 
            this.lblStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(1208, 790);
            this.lblStart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(52, 15);
            this.lblStart.TabIndex = 41;
            this.lblStart.Text = "開始日";
            this.lblStart.Visible = false;
            // 
            // lblFirstDay
            // 
            this.lblFirstDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFirstDay.AutoSize = true;
            this.lblFirstDay.Location = new System.Drawing.Point(167, 688);
            this.lblFirstDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFirstDay.Name = "lblFirstDay";
            this.lblFirstDay.Size = new System.Drawing.Size(22, 15);
            this.lblFirstDay.TabIndex = 21;
            this.lblFirstDay.Text = "日";
            this.lblFirstDay.Visible = false;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(108, 688);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(22, 15);
            this.label27.TabIndex = 20;
            this.label27.Text = "月";
            // 
            // lbldayFinish
            // 
            this.lbldayFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbldayFinish.AutoSize = true;
            this.lbldayFinish.Location = new System.Drawing.Point(1319, 823);
            this.lbldayFinish.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldayFinish.Name = "lbldayFinish";
            this.lbldayFinish.Size = new System.Drawing.Size(22, 15);
            this.lbldayFinish.TabIndex = 46;
            this.lbldayFinish.Text = "日";
            this.lbldayFinish.Visible = false;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(49, 688);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 15);
            this.label25.TabIndex = 19;
            this.label25.Text = "年";
            // 
            // labelYearInfo
            // 
            this.labelYearInfo.AutoSize = true;
            this.labelYearInfo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelYearInfo.Location = new System.Drawing.Point(11, 23);
            this.labelYearInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelYearInfo.Name = "labelYearInfo";
            this.labelYearInfo.Size = new System.Drawing.Size(61, 30);
            this.labelYearInfo.TabIndex = 0;
            this.labelYearInfo.Text = "続紙: --\r\n不要: ++";
            // 
            // lbldayStart
            // 
            this.lbldayStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbldayStart.AutoSize = true;
            this.lbldayStart.Location = new System.Drawing.Point(1247, 823);
            this.lbldayStart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldayStart.Name = "lbldayStart";
            this.lbldayStart.Size = new System.Drawing.Size(22, 15);
            this.lbldayStart.TabIndex = 43;
            this.lbldayStart.Text = "日";
            this.lbldayStart.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 773);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 15);
            this.label9.TabIndex = 60;
            this.label9.Text = "負傷名1";
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(149, 75);
            this.labelSex.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(37, 15);
            this.labelSex.TabIndex = 21;
            this.labelSex.Text = "性別";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(397, 65);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 45);
            this.label5.TabIndex = 26;
            this.label5.Text = "昭:3\r\n平:4\r\n令:5";
            // 
            // labelDays
            // 
            this.labelDays.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(219, 655);
            this.labelDays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(67, 15);
            this.labelDays.TabIndex = 47;
            this.labelDays.Text = "診療日数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(235, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 30);
            this.label4.TabIndex = 23;
            this.label4.Text = "男:1\r\n女:2";
            // 
            // labelTotal
            // 
            this.labelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(15, 723);
            this.labelTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(67, 15);
            this.labelTotal.TabIndex = 54;
            this.labelTotal.Text = "合計金額";
            // 
            // lblNumbering
            // 
            this.lblNumbering.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNumbering.AutoSize = true;
            this.lblNumbering.Location = new System.Drawing.Point(1107, 790);
            this.lblNumbering.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNumbering.Name = "lblNumbering";
            this.lblNumbering.Size = new System.Drawing.Size(75, 15);
            this.lblNumbering.TabIndex = 58;
            this.lblNumbering.Text = "ナンバリング";
            this.lblNumbering.Visible = false;
            // 
            // labelCharge
            // 
            this.labelCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(139, 723);
            this.labelCharge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(67, 15);
            this.labelCharge.TabIndex = 56;
            this.labelCharge.Text = "請求金額";
            // 
            // lblFamilyNo
            // 
            this.lblFamilyNo.AutoSize = true;
            this.lblFamilyNo.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblFamilyNo.Location = new System.Drawing.Point(1099, 29);
            this.lblFamilyNo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFamilyNo.Name = "lblFamilyNo";
            this.lblFamilyNo.Size = new System.Drawing.Size(48, 30);
            this.lblFamilyNo.TabIndex = 20;
            this.lblFamilyNo.Text = "本人:2\r\n家族:6";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(581, 84);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 15);
            this.label16.TabIndex = 32;
            this.label16.Text = "日";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(467, 84);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 15);
            this.label18.TabIndex = 16;
            this.label18.Text = "年";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(524, 84);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(22, 15);
            this.label17.TabIndex = 17;
            this.label17.Text = "月";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(428, 0);
            this.splitter2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(4, 901);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1792, 901);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "InputForm";
            this.Text = "/";
            this.Shown += new System.EventHandler(this.FormOCRCheck_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label labelYearInfo;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBD;
        private VerifyBox verifyBoxBM;
        private VerifyBox verifyBoxBY;
        private VerifyBox verifyBoxBE;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private VerifyBox verifyBoxJuryoType;
        private VerifyBox verifyBoxStart;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbldayStart;
        private VerifyCheckBox verifyCheckBoxVisit;
        private System.Windows.Forms.Label label14;
        private VerifyBox verifyBoxNewCont;
        private System.Windows.Forms.Label lblNewCont;
        private System.Windows.Forms.Label lblNewContNO;
        private VerifyBox verifyBoxFinish;
        private System.Windows.Forms.Label lblFinish;
        private System.Windows.Forms.Label lbldayFinish;
        private VerifyBox verifyBoxNumbering;
        private System.Windows.Forms.Label lblNumbering;
        private VerifyBox verifyBoxNumN;
        private VerifyBox verifyBoxFamily;
        private System.Windows.Forms.Label lblFamily;
        private System.Windows.Forms.Label lblFamilyNo;
        private VerifyBox verifyBoxFirstD;
        private VerifyBox verifyBoxFirstM;
        private VerifyBox verifyBoxFirstY;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblFirstDay;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxAccNumber;
        private System.Windows.Forms.Label label29;
        private VerifyBox verifyBoxDrNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label30;
        private VerifyBox verifyBoxFName;
        private System.Windows.Forms.Label label31;
        private VerifyBox verifyBoxPName;
        private VerifyBox verifyBoxAccName;
        private System.Windows.Forms.Label label33;
        private VerifyBox verifyBoxClinicName;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label2;
        private VerifyCheckBox verifyCheckBoxVisitKasan;
        private System.Windows.Forms.Label labelOryoKasan;
        private System.Windows.Forms.Label labelOryoKm;
        private VerifyBox verifyBoxKasanKm;
        private System.Windows.Forms.Label labelHnameCopy;
    }
}