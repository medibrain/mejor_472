﻿using NpgsqlTypes;
using System.Threading;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

namespace Mejor
{
    public class Kyoris
    {
        private static List<ManualResetEvent> eventList = new List<ManualResetEvent>();
        static int threadCount = 0;

        public enum KyoriKubun
        {
            K_2km,
            K_2to4km,
            K_4to6km,
            K_6to8km,
            K_8to10km,
            K_10to12km,
            K_12to14km,
            K_14to16km,
            K_over16km,
        }

        public enum KyoriErrorCode
        {
            Error_NoError = -1,
            Error_NoData = -2,
            Error_PatientAddr = -3,
            Error_SejutuAddr = -4,
            Error_BothAddr = -5
        }

        public enum StatusCode
        {
            Err_NoErrors = 0,           // 正常
            Err_TooFar = -1,            // 遠すぎる。16km以上申告
            Err_InconsistantData = -2,  // 申請距離と計算距離の区分が異なる
            Err_InsufficientData = -3,  // 住所情報を特定できず
            Err_Over6km = -4            // 6km以上。これ以上は加算料が同じ
        }

        public Kyoris()
        {
        }

        public static string GetStatusMessage(int code)
        {
            string result = "";
            switch (code)
            {
                case 0:
                    result = "異常なし";
                    break;
                case -1:
                    result = "申告距離が16km以上です";
                    break;
                case -2:
                    result = "申請距離区分と計算距離区分が異なります";
                    break;
                case -3:
                    result = "住所情報を算出できませんでした";
                    break;
                default:
                    result = "不明なエラーです";
                    break;
            }
            return result;
        }

        public static void CheckDistance(int aid, string snum, double sDistance)
        {
            KyoriWorker worker = new KyoriWorker();
            worker.aid = aid;

            if (!string.IsNullOrEmpty(snum) && snum.Length == 7)
            {
                // 社団データ対応　都道府県コード27, 点数表ワイルドカードで検索
                snum = "27%" + snum;
            }
            worker.snum = snum;
            // TODO: 現在２を足しているがしばらく入力実績ができたら再考する
            worker.sDistanceKm = (sDistance == -1) ? sDistance : sDistance + 2;

            worker.ThreadFinishedHandler += ThreadFinished;
            WaitCallback waitCallback = new WaitCallback(worker.CalculateDistance);
            // スレッドスタート
            //            Debug.WriteLine("TH[STRT]: " + thread.ManagedThreadId);
            ManualResetEvent mre = new ManualResetEvent(false);
            ThreadPool.QueueUserWorkItem(waitCallback, mre);
            eventList.Add(mre);
            threadCount++;

            // 画面自体は次にすぐに移ってほしいのでここではjoinしない
        }

        public static void GetAddresses(int aid, out string pAddress, out string sAddress)
        {
            string sNum = string.Empty;
            DB db = new DB("osaka_koiki");
            pAddress = string.Empty;
            sAddress = string.Empty;

            using (var cmd = db.CreateCmd("SELECT (koiki.ppref || koiki.pcity || koiki.paddress), koiki.snum FROM " +
                "koikidata koiki WHERE koiki.aid=:aid;"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;

                var res = cmd.TryExecuteReaderList();
                if (res?.Count > 0)
                {
                    // 被保険者の住所を取得する
                    pAddress = (string)res[0][0];
                    sNum = (string)res[0][1];
                }
            }

            if (!string.IsNullOrEmpty(sNum))
            {
                using (var cmd = db.CreateCmd("SELECT \"IRYKKN_AD_KJ\", \"TDFKN_CD\" FROM iryoukikan WHERE " +
                    "CAST(\"TDFKN_CD\" as TEXT) || CAST(\"TNSU_HYO_CD\" as TEXT) || TRIM(to_char(to_number(\"IRYKKN_SKTSN_CD\", '00'), '00')) || TRIM(to_char(to_number(\"IRYKKN_CD\", '00000'), '00000')) LIKE '" + sNum + "'"))
                {
                    var res = cmd.TryExecuteReaderList();
                    if (res?.Count > 0)
                    {
                        sAddress = (string)res[0][0];
                    }
                }
            }
        }

        public static double CalculateDistance(string from, string to)
        {
            KyoriErrorCode eCode = KyoriErrorCode.Error_NoError;
            DB db = new DB("common");
            double idoP = -1, keidoP = -1, idoS = -1, keidoS = -1;

            // 緯度経度情報を取得
            if (string.IsNullOrEmpty(from) || string.IsNullOrEmpty(to))
            {
                Debug.WriteLine("address null or empty from[" + from + "] to[" + to + "]");
                return (double)KyoriErrorCode.Error_NoData;
            }

            // 全角を半角に変換
            Regex re = new Regex("[０-９]");
            var hFrom = re.Replace(from, (Match m) => { return Strings.StrConv(m.Value, VbStrConv.Narrow); });
            var hTo = re.Replace(to, (Match m) => { return Strings.StrConv(m.Value, VbStrConv.Narrow); });

            using (var cmd = db.CreateCmd("SELECT \"ido\",\"keido\",\"MachikufugoChiban\" FROM \"Address\" " +
                "WHERE \"Todofuken\" || \"Sikuchoson\" || \"Chochome\" || \"MachikufugoChiban\" LIKE '" + hFrom + "%' ORDER BY \"MachikufugoChiban\""))
            {
                var res = cmd.TryExecuteReaderList();
                if (res?.Count > 0)
                {
                    idoP = (double)res[0][0];
                    keidoP = (double)res[0][1];
                }
                else
                {
                    eCode = KyoriErrorCode.Error_PatientAddr;
                }
            }

            using (var cmd = db.CreateCmd("SELECT \"ido\",\"keido\",\"MachikufugoChiban\" FROM \"Address\" " +
                "WHERE \"Todofuken\" || \"Sikuchoson\" || \"Chochome\" || \"MachikufugoChiban\" LIKE '" + hTo + "%' ORDER BY \"MachikufugoChiban\""))
            {
                var res = cmd.TryExecuteReaderList();
                if (res?.Count > 0)
                {
                    idoS = (double)res[0][0];
                    keidoS = (double)res[0][1];
                }
                else
                {
                    eCode = KyoriErrorCode.Error_SejutuAddr;
                }
            }

            // 距離計算
            if (idoP != -1 && keidoP != -1 && idoS != -1 && keidoS != -1)
            {
                var distance =  Utility.CalcDistance(idoP, keidoP, idoS, keidoS) / 1000;
                Debug.WriteLine("Distance:" + distance);
                return distance;
            }

            // 両方取れてないか？
            return eCode == KyoriErrorCode.Error_NoError ? (double) KyoriErrorCode.Error_BothAddr : (double) eCode;
        }

        public static bool UpdateAddresses(int aid, string pAddress, string sAddress, double calcDistance)
        {
            DB db = new DB("osaka_koiki");

            using (var cmd = db.CreateCmd("UPDATE kyori_data SET \"calcDistance\"=:calcDistance, " +
                 "\"sejutuAddress\"=:sejutuAddress, \"patientAddress\"=:patientAddress WHERE aid=:aid"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                cmd.Parameters.Add("sejutuAddress", NpgsqlDbType.Text).Value = sAddress;
                cmd.Parameters.Add("patientAddress", NpgsqlDbType.Text).Value = pAddress;
                cmd.Parameters.Add("calcDistance", NpgsqlDbType.Double).Value = calcDistance;

                return cmd.TryExecuteNonQuery();
            }
        }

        public static double GetCalcDistance(int aid)
        {
            DB db = new DB("osaka_koiki");
            double result = -1.0;

            using (var cmd = db.CreateCmd("SELECT \"calcDistance\" FROM kyori_data WHERE aid=:aid"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;

                var res = cmd.TryExecuteReaderList();
                if (res?.Count > 0)
                {
                    result = (double)res[0][0];
                }
            }

            return result;
        }

        static void ThreadFinished(object sender, EventArgs e)
        {
        }

        public static void WaitForThreads()
        {
//            Debug.WriteLine(string.Format("WaitForThreads Wait[{0}]:[{1}] START: ", eventList.Count, threadCount));
            if (eventList.Count > 0)
            {
                WaitHandle[] handleArr = eventList.ToArray();
                WaitHandle.WaitAll(handleArr);
            }
//            Debug.WriteLine("WaitForThreads DONE");
        }

        public static void EndThread(ManualResetEvent arg)
        {
            threadCount--;
            Kyoris.eventList.Remove(arg);
            //Debug.WriteLine("TH[END ]: " + Thread.CurrentThread.ManagedThreadId + " Count[" + eventList.Count + "]:["+threadCount+"]");
        }

        internal static KyoriKubun CalculateKubun(double kyori)
        {
            return KyoriWorker.CalculateKubun(kyori);
        }
    }

    /// <summary>
    /// ワーカースレッドクラス
    /// </summary>
    class KyoriWorker
    {
        const string OSAKAFU = "大阪府";
        const string SAKAISHI = "堺市";
        const string CHOME = "丁目";
        const string CHO = "丁";
        const string BAR = "－";
        const int OSAKAFU_CD = 27;

        public int aid;
        public string snum;
        // 申請書の距離
        public double sDistanceKm = -1;
        private double cDistanceKm = -1;

        public event EventHandler ThreadFinishedHandler;

        protected virtual void OnThreadFinished(EventArgs e)
        {
            ThreadFinishedHandler?.Invoke(this, e);
        }

        /// <summary>
        /// 距離計算用引数
        /// </summary>
        private string addressSejutu;
        private string addressPatient;

        private double idoP = -1;
        private double keidoP = -1;
        private double idoS = -1;
        private double keidoS = -1;

        private DB db = new DB("osaka_koiki");
        private DB commonDB = new DB("common");

        /// <summary>
        /// 申請用紙ごとの距離計算を行なう
        /// </summary>
        public void CalculateDistance(object state)
        {
            try
            {
                // 引数取得
                GetParametersFromDB();

                // 距離計算
                if (idoP != -1 && keidoP != -1 && idoS != -1 && keidoS != -1)
                {
                    this.cDistanceKm = Utility.CalcDistance(idoP, keidoP, idoS, keidoS) / 1000;
                    //Debug.WriteLine("Distance:" + cDistanceKm);
                }

                // 距離データ登録
                RegisterData();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: "+e.Message);
            }
            finally
            {
                ManualResetEvent e = (ManualResetEvent)state;
                Kyoris.EndThread(e);
                e.Set();
            }
        }

        internal static Kyoris.KyoriKubun CalculateKubun(double dist)
        {
            Kyoris.KyoriKubun result;

            if (dist <= 2) result = Kyoris.KyoriKubun.K_2km;
            else if (dist > 2 && dist <= 4) result = Kyoris.KyoriKubun.K_2to4km;
            else if (dist > 4 && dist <= 6) result = Kyoris.KyoriKubun.K_4to6km;
            else if (dist > 6 && dist <= 8) result = Kyoris.KyoriKubun.K_6to8km;
            else if (dist > 8 && dist <= 10) result = Kyoris.KyoriKubun.K_8to10km;
            else if (dist > 10 && dist <= 12) result = Kyoris.KyoriKubun.K_10to12km;
            else if (dist > 12 && dist <= 14) result = Kyoris.KyoriKubun.K_12to14km;
            else if (dist > 14 && dist <= 16) result = Kyoris.KyoriKubun.K_14to16km;
            else result = Kyoris.KyoriKubun.K_over16km;

            return result;
        }

        private Kyoris.StatusCode GetStatus()
        {
            Kyoris.StatusCode result = Kyoris.StatusCode.Err_NoErrors;
            do
            {
                if (sDistanceKm > 16 || cDistanceKm > 16)
                {
                    result = Kyoris.StatusCode.Err_TooFar;
                    break;
                }

                if (sDistanceKm > 6 && cDistanceKm > 6)
                {
                    result = Kyoris.StatusCode.Err_Over6km;
                    break;
                }

                // sDistanceKm,cDistanceKmを2単位で繰り上げて差異があればstatusCode.Err_InconsistantDataを返す
                Kyoris.KyoriKubun sKubun = CalculateKubun(sDistanceKm);
                Kyoris.KyoriKubun cKubun = CalculateKubun(cDistanceKm);

                // 多く申請されていたらエラー通知
                if (sDistanceKm != -1 && cDistanceKm != -1 && sKubun > cKubun)
                {
                    result = Kyoris.StatusCode.Err_InconsistantData;
                    break;
                }

                if (string.IsNullOrEmpty(this.addressPatient) ||
                    string.IsNullOrEmpty(this.addressSejutu) || 
                    cDistanceKm == -1 ||
                    sDistanceKm == -1)
                {
                    result = Kyoris.StatusCode.Err_InsufficientData;
                    break;
                }
            } while (false);

//            Debug.WriteLine("KyoriStatus: " + result);
            return result;
        }

        private void RegisterData()
        {
            bool existRecord = false;
            // 既存チェック
            using (var cmd = db.CreateCmd("SELECT * FROM kyori_data WHERE aid=:aid"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.aid;
                var res = cmd.TryExecuteReaderList();
                existRecord = !(res == null || res.Count == 0);
            }
            if (existRecord)
            {
                using (var cmd = db.CreateCmd("UPDATE kyori_data SET \"isCalcDistNeeded\"=:isCalcDistNeeded, " +
                    "\"isPatiantNotFromHome\"=:isPatiantNotFromHome, snum=:snum, \"calcDistance\"=:calcDistance, " +
                    "\"sDistance\"=:sDistance, \"statusCode\"=:statusCode, \"sejutuAddress\"=:sejutuAddress, " +
                    "\"patientAddress\"=:patientAddress WHERE aid=:aid"))
                {
                    Int64 iSNum;
                    Int64.TryParse(this.snum, out iSNum);
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.aid;
                    cmd.Parameters.Add("isCalcDistNeeded", NpgsqlDbType.Bit).Value = 0;
                    cmd.Parameters.Add("isPatiantNotFromHome", NpgsqlDbType.Bit).Value = 0;
                    cmd.Parameters.Add("snum", NpgsqlDbType.Bigint).Value = iSNum;
                    cmd.Parameters.Add("calcDistance", NpgsqlDbType.Double).Value = this.cDistanceKm;
                    cmd.Parameters.Add("sDistance", NpgsqlDbType.Double).Value = this.sDistanceKm;
                    cmd.Parameters.Add("statusCode", NpgsqlDbType.Integer).Value = this.GetStatus();
                    cmd.Parameters.Add("sejutuAddress", NpgsqlDbType.Text).Value = this.addressSejutu;
                    cmd.Parameters.Add("patientAddress", NpgsqlDbType.Text).Value = this.addressPatient;
                    if (!cmd.TryExecuteNonQuery())
                    {
                        Debug.WriteLine("SQL失敗");
                    }
                }
            }
            else
            {
                using (var cmd = db.CreateCmd("INSERT INTO kyori_data (aid, \"isCalcDistNeeded\", \"isPatiantNotFromHome\", snum, \"calcDistance\", \"sDistance\", \"statusCode\", \"sejutuAddress\", \"patientAddress\") " +
                    "VALUES (:aid, :isCalcDistNeeded, :isPatiantNotFromHome, :snum, :calcDistance, :sDistance, :statusCode, :sejutuAddress, :patientAddress)"))
                {
                    Int64 iSNum;
                    Int64.TryParse(this.snum, out iSNum);
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = this.aid;
                    cmd.Parameters.Add("isCalcDistNeeded", NpgsqlDbType.Bit).Value = 0;
                    cmd.Parameters.Add("isPatiantNotFromHome", NpgsqlDbType.Bit).Value = 0;
                    cmd.Parameters.Add("snum", NpgsqlDbType.Bigint).Value = iSNum;
                    cmd.Parameters.Add("calcDistance", NpgsqlDbType.Double).Value = this.cDistanceKm;
                    cmd.Parameters.Add("sDistance", NpgsqlDbType.Double).Value = this.sDistanceKm;
                    cmd.Parameters.Add("statusCode", NpgsqlDbType.Integer).Value = this.GetStatus();
                    cmd.Parameters.Add("sejutuAddress", NpgsqlDbType.Text).Value = this.addressSejutu;
                    cmd.Parameters.Add("patientAddress", NpgsqlDbType.Text).Value = this.addressPatient;
                    if (!cmd.TryExecuteNonQuery())
                    {
                        Debug.WriteLine("SQL失敗");
                    }
                }
            }
            if (this.GetStatus() != Kyoris.StatusCode.Err_NoErrors)
                Debug.WriteLine(string.Format("REGISTERED: status[{5}] aid[{4}] 施術所[{0}] 被保険者[{1}] s距離[{2}] c距離[{3}]", this.addressSejutu, this.addressPatient, this.sDistanceKm, this.cDistanceKm, this.aid, this.GetStatus().ToString()));
        }

        private void GetParametersFromDB()
        {
            string rawAddr;
            // (OCR)applicationテーブルから取得 snum,paddress
            using (var cmd = db.CreateCmd("SELECT (koiki.ppref || koiki.pcity || koiki.paddress) FROM " +
                "koikidata koiki WHERE koiki.aid=:aid;"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0)
                {
                    // 取得失敗。計算ができないので終了する
                    return;
                }

                // 被保険者の住所を取得する
                rawAddr = (string)res[0][0];
                addressPatient = TruncateAddress((string)res[0][0]);
            }

            //            Debug.WriteLine("RAW:" + rawAddr);
            //            Debug.WriteLine("new:" + addressPatient);
            //            rawAddr = string.Empty;

            bool isOsaka = true;

            // 施術所の住所を取得する
            // 施術者コードが高度なロジックでDBに格納されているので対応するクエリを作成・・・
            using (var cmd = db.CreateCmd("SELECT \"IRYKKN_AD_KJ\", \"TDFKN_CD\" FROM iryoukikan WHERE " +
                "CAST(\"TDFKN_CD\" as TEXT) || CAST(\"TNSU_HYO_CD\" as TEXT) || TRIM(to_char(to_number(\"IRYKKN_SKTSN_CD\", '00'), '00')) || TRIM(to_char(to_number(\"IRYKKN_CD\", '00000'), '00000')) LIKE '" + snum + "'"))
            {
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0)
                {
                    // 取得失敗。計算ができないので終了する
                    return;
                }

                isOsaka = ((int)res[0][1] == OSAKAFU_CD);
                rawAddr = (string)res[0][0];
                addressSejutu = TruncateAddress((string)res[0][0], isOsaka);
            }

            if (!isOsaka)
            {
                Debug.WriteLine("RAW:" + rawAddr);
                Debug.WriteLine("new:" + addressSejutu);
            }

            // 緯度経度情報を取得
            if (string.IsNullOrEmpty(addressPatient) || string.IsNullOrEmpty(addressSejutu))
            {
                Debug.WriteLine("address null or empty patient["+addressPatient+"] sejutu["+addressSejutu+"]");
                return;
            }
            using (var cmd = commonDB.CreateCmd("SELECT \"ido\",\"keido\" FROM \"Address\" " +
                "WHERE \"Todofuken\" || \"Sikuchoson\" || \"Chochome\" LIKE '" + addressPatient + "%'"))
            {
                var res = cmd.TryExecuteReaderList();
                if (res?.Count > 0)
                {
                    idoP = (double)res[0][0];
                    keidoP = (double)res[0][1];
                }
            }

            using (var cmd = commonDB.CreateCmd("SELECT \"ido\",\"keido\" FROM \"Address\" " +
                "WHERE \"Todofuken\" || \"Sikuchoson\" || \"Chochome\" LIKE '" + addressSejutu + "%'"))
            {
                var res = cmd.TryExecuteReaderList();
                if (res?.Count > 0)
                {
                    idoS = (double)res[0][0];
                    keidoS = (double)res[0][1];
                }
            }
        }

        /// <summary>
        /// 住所を検索用に町番前まで切り詰める
        /// </summary>
        /// <param name="rawAddress"></param>
        /// <returns></returns>
        private string TruncateAddress(string rawAddress, bool isOsaka = true)
        {
            string result = rawAddress;
            const string numeric = "0123456789０１２３４５６７８９";
            const string kanji_numeric = "零一二三四五六七八九零一二三四五六七八九";

            //空っぽだったらすぐ終わる
            if (result == null || result == string.Empty)
            {
                return string.Empty;
            }

            // 大阪府が付いていない住所に大阪府を付与する
            if (!string.IsNullOrEmpty(result) && !result.Contains(OSAKAFU) && isOsaka)
            {
                result = OSAKAFU + result;
            }

            // 堺市ロジック：丁目ではなく丁
            bool isSakai = result.Contains(SAKAISHI);

            // 丁目文字を見つけてその前の数字を漢数字に置換する。
            // 対応するのは一つ目だけでよい
            int index = result.IndexOf(isSakai ? CHO : CHOME);
            // 想定しているのは○丁目△番□号か○－△－□のいずれかの表記
            bool isChome = index != -1;
            if (isChome)
            {
                char cOld = result[index - 1];
                int numIdx = numeric.IndexOf(cOld);
                if (numIdx >= 0 && numIdx <= 20)
                {
                    // 数字のみを処理
                    char cNew = kanji_numeric[numeric.IndexOf(cOld)];
                    // 一つ目のみを処理
                    var regex = new Regex(Regex.Escape(cOld.ToString()));
                    result = regex.Replace(result, cNew.ToString(), 1);
                }
            }

            //if (isChome)
            //{
                // ○丁目(あるいは丁)まで抽出、丁目でないと番地前まで抽出
                index = result.IndexOfAny(numeric.ToCharArray());
                if (index > 0)
                {
                    result = result.Substring(0, index);
                }
            //}
            //else
            //{
            //    // ○－△－□の○まで抽出
            //    index = result.IndexOf(BAR);
            //    if (index > 0)
            //    {
            //        result = result.Substring(0, index);
            //    }
            //}

            return result;
        }

        /// <summary>
        /// 送付先住所を使用するかチェック、使用する場合は患者住所を特定する
        /// </summary>
        private void CheckSenderAddress()
        {
            // TODO: ロジック確認
        }
    }
}
