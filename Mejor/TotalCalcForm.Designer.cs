﻿namespace Mejor
{
    partial class TotalCalcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxM1 = new Mejor.VerifyBox();
            this.verifyBoxM2 = new Mejor.VerifyBox();
            this.verifyBoxM3 = new Mejor.VerifyBox();
            this.verifyBoxR1 = new Mejor.VerifyBox();
            this.verifyBoxR2 = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonCalc = new System.Windows.Forms.Button();
            this.verifyBoxR3 = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.labelR1 = new System.Windows.Forms.Label();
            this.labelRatio = new System.Windows.Forms.Label();
            this.labelR2 = new System.Windows.Forms.Label();
            this.labelR3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxR4 = new Mejor.VerifyBox();
            this.verifyBoxM4 = new Mejor.VerifyBox();
            this.labelR4 = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.Location = new System.Drawing.Point(94, 11);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxTotal.TabIndex = 1;
            this.verifyBoxTotal.TextV = "";
            this.verifyBoxTotal.TextChanged += new System.EventHandler(this.verifyBox_TextChanged);
            // 
            // verifyBoxM1
            // 
            this.verifyBoxM1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM1.Location = new System.Drawing.Point(163, 54);
            this.verifyBoxM1.Name = "verifyBoxM1";
            this.verifyBoxM1.NewLine = false;
            this.verifyBoxM1.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxM1.TabIndex = 4;
            this.verifyBoxM1.TextV = "";
            this.verifyBoxM1.TextChanged += new System.EventHandler(this.verifyBox_TextChanged);
            // 
            // verifyBoxM2
            // 
            this.verifyBoxM2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM2.Location = new System.Drawing.Point(163, 89);
            this.verifyBoxM2.Name = "verifyBoxM2";
            this.verifyBoxM2.NewLine = false;
            this.verifyBoxM2.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxM2.TabIndex = 6;
            this.verifyBoxM2.TextV = "";
            this.verifyBoxM2.TextChanged += new System.EventHandler(this.verifyBox_TextChanged);
            // 
            // verifyBoxM3
            // 
            this.verifyBoxM3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM3.Location = new System.Drawing.Point(163, 124);
            this.verifyBoxM3.Name = "verifyBoxM3";
            this.verifyBoxM3.NewLine = false;
            this.verifyBoxM3.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxM3.TabIndex = 8;
            this.verifyBoxM3.TextV = "";
            this.verifyBoxM3.TextChanged += new System.EventHandler(this.verifyBox_TextChanged);
            // 
            // verifyBoxR1
            // 
            this.verifyBoxR1.BackColor = System.Drawing.Color.White;
            this.verifyBoxR1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxR1.Location = new System.Drawing.Point(401, 54);
            this.verifyBoxR1.Name = "verifyBoxR1";
            this.verifyBoxR1.NewLine = false;
            this.verifyBoxR1.ReadOnly = true;
            this.verifyBoxR1.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxR1.TabIndex = 14;
            this.verifyBoxR1.TabStop = false;
            this.verifyBoxR1.TextV = "";
            // 
            // verifyBoxR2
            // 
            this.verifyBoxR2.BackColor = System.Drawing.Color.White;
            this.verifyBoxR2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxR2.Location = new System.Drawing.Point(401, 89);
            this.verifyBoxR2.Name = "verifyBoxR2";
            this.verifyBoxR2.NewLine = false;
            this.verifyBoxR2.ReadOnly = true;
            this.verifyBoxR2.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxR2.TabIndex = 16;
            this.verifyBoxR2.TabStop = false;
            this.verifyBoxR2.TextV = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(42, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "合計額";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(100, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "１ヶ月目";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(6, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "合計請求額";
            // 
            // buttonCalc
            // 
            this.buttonCalc.Location = new System.Drawing.Point(282, 91);
            this.buttonCalc.Name = "buttonCalc";
            this.buttonCalc.Size = new System.Drawing.Size(96, 53);
            this.buttonCalc.TabIndex = 12;
            this.buttonCalc.Text = ">>　計算　>>";
            this.buttonCalc.UseVisualStyleBackColor = true;
            this.buttonCalc.Click += new System.EventHandler(this.buttonCalc_Click);
            // 
            // verifyBoxR3
            // 
            this.verifyBoxR3.BackColor = System.Drawing.Color.White;
            this.verifyBoxR3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxR3.Location = new System.Drawing.Point(401, 124);
            this.verifyBoxR3.Name = "verifyBoxR3";
            this.verifyBoxR3.NewLine = false;
            this.verifyBoxR3.ReadOnly = true;
            this.verifyBoxR3.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxR3.TabIndex = 18;
            this.verifyBoxR3.TabStop = false;
            this.verifyBoxR3.TextV = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(100, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "３ヶ月目";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(100, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "４ヶ月目";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(555, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 22;
            this.button1.Text = "閉じる";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelR1
            // 
            this.labelR1.AutoSize = true;
            this.labelR1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelR1.Location = new System.Drawing.Point(507, 58);
            this.labelR1.Name = "labelR1";
            this.labelR1.Size = new System.Drawing.Size(22, 15);
            this.labelR1.TabIndex = 15;
            this.labelR1.Text = "※";
            // 
            // labelRatio
            // 
            this.labelRatio.AutoSize = true;
            this.labelRatio.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelRatio.Location = new System.Drawing.Point(519, 18);
            this.labelRatio.Name = "labelRatio";
            this.labelRatio.Size = new System.Drawing.Size(75, 15);
            this.labelRatio.TabIndex = 13;
            this.labelRatio.Text = "負担割合：";
            // 
            // labelR2
            // 
            this.labelR2.AutoSize = true;
            this.labelR2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelR2.Location = new System.Drawing.Point(507, 93);
            this.labelR2.Name = "labelR2";
            this.labelR2.Size = new System.Drawing.Size(22, 15);
            this.labelR2.TabIndex = 17;
            this.labelR2.Text = "※";
            // 
            // labelR3
            // 
            this.labelR3.AutoSize = true;
            this.labelR3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelR3.Location = new System.Drawing.Point(507, 128);
            this.labelR3.Name = "labelR3";
            this.labelR3.Size = new System.Drawing.Size(22, 15);
            this.labelR3.TabIndex = 19;
            this.labelR3.Text = "※";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(100, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "２ヶ月目";
            // 
            // verifyBoxR4
            // 
            this.verifyBoxR4.BackColor = System.Drawing.Color.White;
            this.verifyBoxR4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxR4.Location = new System.Drawing.Point(401, 159);
            this.verifyBoxR4.Name = "verifyBoxR4";
            this.verifyBoxR4.NewLine = false;
            this.verifyBoxR4.ReadOnly = true;
            this.verifyBoxR4.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxR4.TabIndex = 20;
            this.verifyBoxR4.TabStop = false;
            this.verifyBoxR4.TextV = "";
            // 
            // verifyBoxM4
            // 
            this.verifyBoxM4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM4.Location = new System.Drawing.Point(163, 159);
            this.verifyBoxM4.Name = "verifyBoxM4";
            this.verifyBoxM4.NewLine = false;
            this.verifyBoxM4.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxM4.TabIndex = 10;
            this.verifyBoxM4.TextV = "";
            this.verifyBoxM4.TextChanged += new System.EventHandler(this.verifyBox_TextChanged);
            // 
            // labelR4
            // 
            this.labelR4.AutoSize = true;
            this.labelR4.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelR4.Location = new System.Drawing.Point(507, 163);
            this.labelR4.Name = "labelR4";
            this.labelR4.Size = new System.Drawing.Size(22, 15);
            this.labelR4.TabIndex = 21;
            this.labelR4.Text = "※";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelTotal.Location = new System.Drawing.Point(132, 200);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(45, 15);
            this.labelTotal.TabIndex = 11;
            this.labelTotal.Text = "合計：";
            // 
            // TotalCalcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 221);
            this.Controls.Add(this.labelRatio);
            this.Controls.Add(this.labelR4);
            this.Controls.Add(this.labelR3);
            this.Controls.Add(this.labelR2);
            this.Controls.Add(this.labelR1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonCalc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.verifyBoxR2);
            this.Controls.Add(this.verifyBoxR1);
            this.Controls.Add(this.verifyBoxM4);
            this.Controls.Add(this.verifyBoxM3);
            this.Controls.Add(this.verifyBoxR4);
            this.Controls.Add(this.verifyBoxM2);
            this.Controls.Add(this.verifyBoxR3);
            this.Controls.Add(this.verifyBoxM1);
            this.Controls.Add(this.verifyBoxTotal);
            this.KeyPreview = true;
            this.Name = "TotalCalcForm";
            this.Text = "合計額計算ツール";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TotalCalcForm_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxM1;
        private VerifyBox verifyBoxM2;
        private VerifyBox verifyBoxM3;
        private VerifyBox verifyBoxR1;
        private VerifyBox verifyBoxR2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonCalc;
        private VerifyBox verifyBoxR3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelR1;
        private System.Windows.Forms.Label labelRatio;
        private System.Windows.Forms.Label labelR2;
        private System.Windows.Forms.Label labelR3;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxR4;
        private VerifyBox verifyBoxM4;
        private System.Windows.Forms.Label labelR4;
        private System.Windows.Forms.Label labelTotal;
    }
}