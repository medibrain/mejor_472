﻿namespace Mejor.Ichiharashi
{
    partial class frmImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxBase = new System.Windows.Forms.TextBox();
            this.gbJ = new System.Windows.Forms.GroupBox();
            this.btnFile2 = new System.Windows.Forms.Button();
            this.btnFile1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxKyufu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImpJyusei = new System.Windows.Forms.Button();
            this.textBoxAHK = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnFile3 = new System.Windows.Forms.Button();
            this.gbA = new System.Windows.Forms.GroupBox();
            this.gbJ.SuspendLayout();
            this.gbA.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxBase
            // 
            this.textBoxBase.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBase.Location = new System.Drawing.Point(180, 29);
            this.textBoxBase.Multiline = true;
            this.textBoxBase.Name = "textBoxBase";
            this.textBoxBase.Size = new System.Drawing.Size(512, 44);
            this.textBoxBase.TabIndex = 0;
            // 
            // gbJ
            // 
            this.gbJ.Controls.Add(this.btnFile2);
            this.gbJ.Controls.Add(this.btnFile1);
            this.gbJ.Controls.Add(this.label2);
            this.gbJ.Controls.Add(this.textBoxKyufu);
            this.gbJ.Controls.Add(this.label1);
            this.gbJ.Controls.Add(this.textBoxBase);
            this.gbJ.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbJ.Location = new System.Drawing.Point(43, 12);
            this.gbJ.Name = "gbJ";
            this.gbJ.Size = new System.Drawing.Size(786, 94);
            this.gbJ.TabIndex = 1;
            this.gbJ.TabStop = false;
            this.gbJ.Text = "柔整";
            // 
            // btnFile2
            // 
            this.btnFile2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile2.Location = new System.Drawing.Point(698, 81);
            this.btnFile2.Name = "btnFile2";
            this.btnFile2.Size = new System.Drawing.Size(48, 15);
            this.btnFile2.TabIndex = 3;
            this.btnFile2.Text = "...";
            this.btnFile2.UseVisualStyleBackColor = true;
            this.btnFile2.Visible = false;
            // 
            // btnFile1
            // 
            this.btnFile1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile1.Location = new System.Drawing.Point(698, 30);
            this.btnFile1.Name = "btnFile1";
            this.btnFile1.Size = new System.Drawing.Size(48, 31);
            this.btnFile1.TabIndex = 3;
            this.btnFile1.Text = "...";
            this.btnFile1.UseVisualStyleBackColor = true;
            this.btnFile1.Click += new System.EventHandler(this.btnFile1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "給付記録データ";
            this.label2.Visible = false;
            // 
            // textBoxKyufu
            // 
            this.textBoxKyufu.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxKyufu.Location = new System.Drawing.Point(180, 81);
            this.textBoxKyufu.Multiline = true;
            this.textBoxKyufu.Name = "textBoxKyufu";
            this.textBoxKyufu.Size = new System.Drawing.Size(512, 28);
            this.textBoxKyufu.TabIndex = 0;
            this.textBoxKyufu.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "住所データ";
            // 
            // btnImpJyusei
            // 
            this.btnImpJyusei.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImpJyusei.Location = new System.Drawing.Point(729, 95);
            this.btnImpJyusei.Name = "btnImpJyusei";
            this.btnImpJyusei.Size = new System.Drawing.Size(115, 34);
            this.btnImpJyusei.TabIndex = 3;
            this.btnImpJyusei.Text = "取込";
            this.btnImpJyusei.UseVisualStyleBackColor = true;
            this.btnImpJyusei.Click += new System.EventHandler(this.btnImpJyusei_Click);
            // 
            // textBoxAHK
            // 
            this.textBoxAHK.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAHK.Location = new System.Drawing.Point(180, 27);
            this.textBoxAHK.Multiline = true;
            this.textBoxAHK.Name = "textBoxAHK";
            this.textBoxAHK.Size = new System.Drawing.Size(512, 45);
            this.textBoxAHK.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "あはき療養費データ";
            // 
            // btnFile3
            // 
            this.btnFile3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFile3.Location = new System.Drawing.Point(698, 29);
            this.btnFile3.Name = "btnFile3";
            this.btnFile3.Size = new System.Drawing.Size(48, 31);
            this.btnFile3.TabIndex = 3;
            this.btnFile3.Text = "...";
            this.btnFile3.UseVisualStyleBackColor = true;
            // 
            // gbA
            // 
            this.gbA.Controls.Add(this.btnFile3);
            this.gbA.Controls.Add(this.label4);
            this.gbA.Controls.Add(this.textBoxAHK);
            this.gbA.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.gbA.Location = new System.Drawing.Point(43, 114);
            this.gbA.Name = "gbA";
            this.gbA.Size = new System.Drawing.Size(786, 27);
            this.gbA.TabIndex = 1;
            this.gbA.TabStop = false;
            this.gbA.Text = "あはき";
            this.gbA.Visible = false;
            // 
            // frmImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 150);
            this.Controls.Add(this.btnImpJyusei);
            this.Controls.Add(this.gbA);
            this.Controls.Add(this.gbJ);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "frmImport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "取込";
            this.gbJ.ResumeLayout(false);
            this.gbJ.PerformLayout();
            this.gbA.ResumeLayout(false);
            this.gbA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxBase;
        private System.Windows.Forms.GroupBox gbJ;
        private System.Windows.Forms.Button btnImpJyusei;
        private System.Windows.Forms.Button btnFile2;
        private System.Windows.Forms.Button btnFile1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxKyufu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAHK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFile3;
        private System.Windows.Forms.GroupBox gbA;
    }
}