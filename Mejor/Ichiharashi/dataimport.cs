﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Ichiharashi

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName = frm.strFileBaseData;
            string strFileName_jyuseikyufu = frm.strFileKyufuData;
            string strFileName_ahk = frm.strFileAHKRyouyouhiData;

            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                if (strFileName != string.Empty)
                {
                    wf.LogPrint("データインポート");
                    if (!dataImport_base.dataImport_main(_cym, wf, strFileName))
                    {
                        wf.LogPrint("データインポート異常終了");
                        System.Windows.Forms.MessageBox.Show("データインポート異常終了","",
                            System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    
                    wf.LogPrint("データインポート終了");
                }
                else
                {
                    wf.LogPrint("データのファイルが指定されていないため処理しない");
                }


                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {

            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    /// <summary>
    /// 柔整データインポート
    /// </summary>
    partial class dataImport_base
    {

        #region テーブル構造
        
        public int cym { get;set; }=0;                                         //メホール請求年月管理用;
        public string f001insnum { get; set; } = string.Empty;                 //保険者番号;

        [DB.DbAttribute.PrimaryKey]
        public string f002shinsaym { get; set; } = string.Empty;               //審査年月;
        public string f003comnum { get; set; } = string.Empty;                 //レセプト全国共通キー;

        public string f004rezenum { get; set; } = string.Empty;                //国保連レセプト番号;
        public string f005sejutsuym { get; set; } = string.Empty;              //施術年月;
        public string f006 { get; set; } = string.Empty;                       //保険種別1;
        public string f007 { get; set; } = string.Empty;                       //保険種別2;
        public string f008family { get; set; } = string.Empty;                 //本人家族入外区分;
        public string f009hihomark { get; set; } = string.Empty;               //被保険者証記号;
        public string f010hihonum { get; set; } = string.Empty;                //証番号;
        public string f011pgender { get; set; } = string.Empty;                //性別;
        public string f012pbirthday { get; set; } = string.Empty;              //生年月日;
        public string f013ratio { get; set; } = string.Empty;                  //給付割合;
        public string f014 { get; set; } = string.Empty;                       //特記事項1;
        public string f015 { get; set; } = string.Empty;                       //特記事項2;
        public string f016 { get; set; } = string.Empty;                       //特記事項3;
        public string f017 { get; set; } = string.Empty;                       //特記事項4;
        public string f018 { get; set; } = string.Empty;                       //特記事項5;
        public string f019 { get; set; } = string.Empty;                       //算定区分1;
        public string f020 { get; set; } = string.Empty;                       //算定区分2;
        public string f021 { get; set; } = string.Empty;                       //算定区分3;
        public string f022clinicnum { get; set; } = string.Empty;              //施術所コード;
        public string f023 { get; set; } = string.Empty;                       //柔整団体コード;
        public string f024 { get; set; } = string.Empty;                       //回数;
        public string f025total { get; set; } = string.Empty;                  //決定金額;
        public string f026partial { get; set; } = string.Empty;                //決定一部負担金;
        public string f027 { get; set; } = string.Empty;                       //費用額;
        public string f028charge { get; set; } = string.Empty;                 //保険者負担額;
        public string f029 { get; set; } = string.Empty;                       //高額療養費;
        public string f030 { get; set; } = string.Empty;                       //患者負担額;
        public string f031 { get; set; } = string.Empty;                       //国保優先公費;
        public string f033firstdate1 { get; set; } = string.Empty;             //初検年月日;
        public string f034 { get; set; } = string.Empty;                       //負傷名数;
        public string f035 { get; set; } = string.Empty;                       //転帰;
        public string f036 { get; set; } = string.Empty;                       //転帰レコード区分;
        public string f037 { get; set; } = string.Empty;                       //転帰グループ番号;
        public string f038 { get; set; } = string.Empty;                       //整形審査年月_1;
        public string f039 { get; set; } = string.Empty;                       //整形レセプト全国共通キー_1;
        public string f040 { get; set; } = string.Empty;                       //整形医療機関コード_1;
        public string f041 { get; set; } = string.Empty;                       //整形医療機関名_1;
        public string f042 { get; set; } = string.Empty;                       //整形診療開始日1_1;
        public string f043 { get; set; } = string.Empty;                       //整形診療開始日2_1;
        public string f044 { get; set; } = string.Empty;                       //整形診療開始日3_1;
        public string f045 { get; set; } = string.Empty;                       //整形疾病コード1_1;
        public string f046 { get; set; } = string.Empty;                       //整形疾病コード2_1;
        public string f047 { get; set; } = string.Empty;                       //整形疾病コード3_1;
        public string f048 { get; set; } = string.Empty;                       //整形疾病コード4_1;
        public string f049 { get; set; } = string.Empty;                       //整形疾病コード5_1;
        public string f050 { get; set; } = string.Empty;                       //整形診療日数_1;
        public string f051 { get; set; } = string.Empty;                       //整形費用額_1;
        public string f052 { get; set; } = string.Empty;                       //整形審査年月_2;
        public string f053 { get; set; } = string.Empty;                       //整形レセプト全国共通キー_2;
        public string f054 { get; set; } = string.Empty;                       //整形医療機関コード_2;
        public string f055 { get; set; } = string.Empty;                       //整形医療機関名_2;
        public string f056 { get; set; } = string.Empty;                       //整形診療開始日1_2;
        public string f057 { get; set; } = string.Empty;                       //整形診療開始日2_2;
        public string f058 { get; set; } = string.Empty;                       //整形診療開始日3_2;
        public string f059 { get; set; } = string.Empty;                       //整形疾病コード1_2;
        public string f060 { get; set; } = string.Empty;                       //整形疾病コード2_2;
        public string f061 { get; set; } = string.Empty;                       //整形疾病コード3_2;
        public string f062 { get; set; } = string.Empty;                       //整形疾病コード4_2;
        public string f063 { get; set; } = string.Empty;                       //整形疾病コード5_2;
        public string f064 { get; set; } = string.Empty;                       //整形診診療日数_2;
        public string f065 { get; set; } = string.Empty;                       //整形費用額_2;
        public string f066 { get; set; } = string.Empty;                       //整形審査年月_3;
        public string f067 { get; set; } = string.Empty;                       //整形レセプト全国共通キー_3;
        public string f068 { get; set; } = string.Empty;                       //整形医療機関コード_3;
        public string f069 { get; set; } = string.Empty;                       //整形医療機関名_3;
        public string f070 { get; set; } = string.Empty;                       //整形診療開始日1_3;
        public string f071 { get; set; } = string.Empty;                       //整形診療開始日2_3;
        public string f072 { get; set; } = string.Empty;                       //整形診療開始日3_3;
        public string f073 { get; set; } = string.Empty;                       //整形疾病コード1_3;
        public string f074 { get; set; } = string.Empty;                       //整形疾病コード2_3;
        public string f075 { get; set; } = string.Empty;                       //整形疾病コード3_3;
        public string f076 { get; set; } = string.Empty;                       //整形疾病コード4_3;
        public string f077 { get; set; } = string.Empty;                       //整形疾病コード5_3;
        public string f078 { get; set; } = string.Empty;                       //整形診療日数_3;
        public string f079 { get; set; } = string.Empty;                       //整形費用額_3;
        public string f080 { get; set; } = string.Empty;                       //宛名番号;
        public string f081zip { get; set; } = string.Empty;                   //郵便番号;
        public string f082address { get; set; } = string.Empty;               //住所;
        public string f083namekanji { get; set; } = string.Empty;             //氏名漢字;

        //Mejor用項目
        public int f100shinsaYMAD { get; set; } = 0;                           //審査年月西暦;
        public int f101sejutsuYMAD { get; set; } = 0;                          //施術年月西暦;
        public DateTime f102birthAD { get; set; } = DateTime.MinValue;         //生年月日西暦;
        public DateTime f103firstdateAD { get; set; } = DateTime.MinValue;                        //初検年月日西暦;
        public int f104seikeishisnaYMAD1 { get; set; } = 0;                    //整形審査年月_1西暦;
        public int f105seikeishisnaYMAD2 { get; set; } = 0;                    //整形審査年月_2西暦;
        public int f106seikeishisnaYMAD3 { get; set; } = 0;                    //整形審査年月_3西暦;
        public string f107hihomark_narrow { get; set; } = string.Empty;        //被保険者証記号半角;
        public string f108hihonum_narrow { get; set; } = string.Empty;         //証番号半角;


        #endregion


        /// <summary>
        /// シート名リスト
        /// </summary>
        static List<string> lstSheet = new List<string>();

        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;

        static int intcym = 0;

        List<dataImport_base> lstImp = new List<dataImport_base>();

        /// <summary>
        /// データインポート
        /// </summary>
        /// <returns></returns>
        public static bool dataImport_main(int _cym,WaitForm wf,string strFileName)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");
                
                //npoiで開きデータ取りだし
                //dataimport_baseに登録
                if(!EntryExcelData(strFileName, wf)) return false;

                
                wf.LogPrint("Appテーブル作成");
                //画像名（国保連レセプト番号）で紐づけてアップデートする
                if (!UpdateApp(intcym)) return false;

                //20211029105052 furukawa st ////////////////////////
                //auxにレセプト全国共通キーを入れておく
                
                wf.LogPrint("AUXテーブル作成");
                if (!UpdateAUX(intcym)) return false;
                //20211029105052 furukawa ed ////////////////////////


                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
           

        }

        //20211029112709 furukawa st ////////////////////////
        //auxにレセプト全国共通キーを入れておく
        
        /// <summary>
        /// AUX更新
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool UpdateAUX(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();

            sb.AppendLine(" update application_aux set    ");
            sb.AppendLine(" matchingid02 = b.f003comnum ");  //レセプト全国共通キー
            sb.AppendLine(" ,matchingid02date = now() ");  //レセプト全国共通キー

            sb.AppendLine(" from application a ,dataimport_base b ");
            sb.AppendLine(" where ");
            sb.AppendLine("  a.aid=application_aux.aid ");
            sb.AppendLine($" and replace(a.emptytext3,'.tif','')=b.f004rezenum and a.cym={cym};");
            

            string strsql = sb.ToString();


            DB.Command cmd = new DB.Command(DB.Main, strsql, true);


            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }
        //20211029112709 furukawa ed ////////////////////////

        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();
             
            sb.AppendLine("update application set    " );
            sb.AppendLine(" inum				      = b.f001insnum" );                                         //保険者番号
            sb.AppendLine(",comnum			          = b.f003comnum" );                                         //レセプト全国共通キー
            sb.AppendLine(",afamily			          = case cast(b.f008family as int) when 2 then 2 else 6 end " );        //本人家族入外区分                 
            sb.AppendLine(",hnum				      = b.f009hihomark || '-' || b.f010hihonum" );               //被保険者証記号番号
                                                           //",hnum				          = b.hihomark || b.hihonum" );                          //被保険者証記号番号
            sb.AppendLine(",psex				      = cast(b.f011pgender as int) " );                          //性別
            sb.AppendLine(",aratio			          = cast(b.f013ratio as int) " );                            //給付割合
            sb.AppendLine(",sid				          = b.f022clinicnum" );                                      //施術所コード
            sb.AppendLine(",acounteddays		      = cast((case when b.f024        ='' then '0' else b.f024        end ) as int) " );         //回数（実日数）
            sb.AppendLine(",atotal			          = cast((case when b.f025total   ='' then '0' else b.f025total   end ) as int) " );         //決定金額
            sb.AppendLine(",acharge			          = cast((case when b.f028charge  ='' then '0' else b.f028charge  end ) as int)" );          //保険者負担額
            sb.AppendLine(",apartial			      = cast((case when b.f030 ='' then '0' else b.f030 end ) as int)" );          //患者負担額
            sb.AppendLine(",taggeddatas	              " );
            sb.AppendLine("       = 'count:\"' || case when b.f034 ='' then '0' else b.f034 end  || '\"'  " );//負傷数、被保険者名カナ
                                                          //"       = taggeddatas || 'count:\"' || b.fushocount || '\"|' || 'GeneralString1:\"' || b.hihokana || '\"' " );//負傷数、被保険者名カナ
            sb.AppendLine(",hzip				          = b.f081zip" );                                     //被保険者郵便番号
            sb.AppendLine(",haddress			          = b.f082address" );                                 //被保険者住所
            sb.AppendLine(",hname				          = b.f083namekanji" );                               //被保険者名

            //20210414103059 furukawa st ////////////////////////                                
            //入力画面がないため、app更新時にymをayear,amonthから取得するので、ここで入れないとym=0となり、診療年月が0になってしまう
            sb.AppendLine(",ayear               = cast(substr(b.f005sejutsuym,2,2) as int)");       //施術年和暦
            sb.AppendLine(",amonth              = cast(substr(b.f005sejutsuym,4,2) as int)");       //施術月和暦            
            //20210414103059 furukawa ed ////////////////////////


            sb.AppendLine(",ym				            = cast(b.f101sejutsuYMAD as int) " );             //診療年月
            sb.AppendLine(",pbirthday				    = cast(b.f102birthad as date) " );                //受療者生年月日
            sb.AppendLine(",ifirstdate1				    = cast(b.f103firstdatead as date)  " );           //初検日1

            //20210415134308 furukawa st ////////////////////////
            //宛名番号追加            
            sb.AppendLine(",numbering                   = b.f080");//宛名番号
            //20210415134308 furukawa ed ////////////////////////


            sb.AppendLine(",fchargetype=  " );
            sb.AppendLine("	case when cast(b.f101sejutsuYMAD as varchar) =substring(replace(cast(ifirstdate1 as varchar),'-',''),0,7) then 1 else 2 end " );//新規継続
            sb.AppendLine(",aapptype=6 " ); //申請書種類は6柔整だろう
            sb.AppendLine("from dataimport_base b " );
            sb.AppendLine("where " );
            sb.AppendLine($"replace(application.emptytext3,'.tif','')=b.f004rezenum and application.cym={cym};");



            string strsql = sb.ToString();
              

            DB.Command cmd = new DB.Command(DB.Main,strsql,true);
           

            try
            {

                cmd.TryExecuteNonQuery();
                return true;

            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }


        /// <summary>
        /// 提供データExcelを開いてテーブル登録
        /// </summary>
        /// <param name="strFileName">ファイル名</param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryExcelData(string strFileName ,WaitForm wf)
        {
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);

            NPOI.SS.UserModel.ISheet ws = wb.GetSheetAt(0);

            DB.Transaction tran = new DB.Transaction(DB.Main);

            DB.Command cmd=new DB.Command($"delete from dataimport_base where cym={intcym}",tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            int c = 0;//インポートファイルの列
            int r = 1;//インポートファイルの行
            try
            {

                wf.InvokeValue = 0;
                wf.SetMax(ws.LastRowNum);
                
                //NPOIのGetRowは空白セルを飛ばすので、データに抜けがあった場合に同じ要素数にならないため、1行目（ヘッダ行）のセル数に固定する
                int cellcnt = ws.GetRow(0).Cells.Count;
                
                for (r = 1; r <= ws.LastRowNum; r++)
                {
                    dataImport_base imp = new dataImport_base();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);

                    imp.cym = intcym;                                                           //	メホール上の処理年月 メホール管理用    

                    for ( c = 0; c < cellcnt; c++)
                    {
                        if(CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        
                        if (c == 0)   imp.f001insnum = getCellValueToString(cell);                //保険者番号;
                        if (c == 1)   imp.f002shinsaym = getCellValueToString(cell);              //審査年月;
                        if (c == 2)   imp.f003comnum = getCellValueToString(cell);                //レセプト全国共通キー;
                        if (c == 3)   imp.f004rezenum = getCellValueToString(cell);               //国保連レセプト番号;
                        if (c == 4)   imp.f005sejutsuym = getCellValueToString(cell);             //施術年月;
                        if (c == 5)   imp.f006 = getCellValueToString(cell);                      //保険種別1;
                        if (c == 6)   imp.f007 = getCellValueToString(cell);                      //保険種別2;
                        if (c == 7)   imp.f008family = getCellValueToString(cell);                //本人家族入外区分;
                        if (c == 8)   imp.f009hihomark = getCellValueToString(cell);              //被保険者証記号;
                        if (c == 9)    imp.f010hihonum = getCellValueToString(cell);              //証番号;
                        if (c == 10)   imp.f011pgender = getCellValueToString(cell);              //性別;
                        if (c == 11)   imp.f012pbirthday = getCellValueToString(cell);            //生年月日;
                        if (c == 12)   imp.f013ratio = getCellValueToString(cell);                //給付割合;
                        if (c == 13)   imp.f014 = getCellValueToString(cell);                     //特記事項1;
                        if (c == 14)   imp.f015 = getCellValueToString(cell);                     //特記事項2;
                        if (c == 15)   imp.f016 = getCellValueToString(cell);                     //特記事項3;
                        if (c == 16)   imp.f017 = getCellValueToString(cell);                     //特記事項4;
                        if (c == 17)   imp.f018 = getCellValueToString(cell);                     //特記事項5;
                        if (c == 18)   imp.f019 = getCellValueToString(cell);                     //算定区分1;
                        if (c == 19)   imp.f020 = getCellValueToString(cell);                     //算定区分2;
                        if (c == 20)   imp.f021 = getCellValueToString(cell);                     //算定区分3;
                        if (c == 21)   imp.f022clinicnum = getCellValueToString(cell);            //施術所コード;
                        if (c == 22)   imp.f023 = getCellValueToString(cell);                     //柔整団体コード;
                        if (c == 23)   imp.f024 = getCellValueToString(cell);                     //回数;
                        if (c == 24)   imp.f025total = getCellValueToString(cell);                //決定金額;
                        if (c == 25)   imp.f026partial = getCellValueToString(cell);              //決定一部負担金;
                        if (c == 26)   imp.f027 = getCellValueToString(cell);                     //費用額;
                        if (c == 27)   imp.f028charge = getCellValueToString(cell);               //保険者負担額;
                        if (c == 28)   imp.f029 = getCellValueToString(cell);                     //高額療養費;
                        if (c == 29)   imp.f030 = getCellValueToString(cell);                     //患者負担額;
                        if (c == 30)   imp.f031 = getCellValueToString(cell);                     //国保優先公費;
                        if (c == 31)   imp.f033firstdate1 = getCellValueToString(cell);           //初検年月日;
                        if (c == 32)   imp.f034 = getCellValueToString(cell);                     //負傷名数;
                        if (c == 33)   imp.f035 = getCellValueToString(cell);                     //転帰;
                        if (c == 34)   imp.f036 = getCellValueToString(cell);                     //転帰レコード区分;
                        if (c == 35)   imp.f037 = getCellValueToString(cell);                     //転帰グループ番号;
                        if (c == 36)   imp.f038 = getCellValueToString(cell);                     //整形審査年月_1;
                        if (c == 37)   imp.f039 = getCellValueToString(cell);                     //整形レセプト全国共通キー_1;
                        if (c == 38)   imp.f040 = getCellValueToString(cell);                     //整形医療機関コード_1;
                        if (c == 39)   imp.f041 = getCellValueToString(cell);                     //整形医療機関名_1;
                        if (c == 40)   imp.f042 = getCellValueToString(cell);                     //整形診療開始日1_1;
                        if (c == 41)   imp.f043 = getCellValueToString(cell);                     //整形診療開始日2_1;
                        if (c == 42)   imp.f044 = getCellValueToString(cell);                     //整形診療開始日3_1;
                        if (c == 43)   imp.f045 = getCellValueToString(cell);                     //整形疾病コード1_1;
                        if (c == 44)   imp.f046 = getCellValueToString(cell);                     //整形疾病コード2_1;
                        if (c == 45)   imp.f047 = getCellValueToString(cell);                     //整形疾病コード3_1;
                        if (c == 46)   imp.f048 = getCellValueToString(cell);                     //整形疾病コード4_1;
                        if (c == 47)   imp.f049 = getCellValueToString(cell);                     //整形疾病コード5_1;
                        if (c == 48)   imp.f050 = getCellValueToString(cell);                     //整形診療日数_1;
                        if (c == 49)   imp.f051 = getCellValueToString(cell);                     //整形費用額_1;
                        if (c == 50)   imp.f052 = getCellValueToString(cell);                     //整形審査年月_2;
                        if (c == 51)   imp.f053 = getCellValueToString(cell);                     //整形レセプト全国共通キー_2;
                        if (c == 52)   imp.f054 = getCellValueToString(cell);                     //整形医療機関コード_2;
                        if (c == 53)   imp.f055 = getCellValueToString(cell);                     //整形医療機関名_2;
                        if (c == 54)   imp.f056 = getCellValueToString(cell);                     //整形診療開始日1_2;
                        if (c == 55)   imp.f057 = getCellValueToString(cell);                     //整形診療開始日2_2;
                        if (c == 56)   imp.f058 = getCellValueToString(cell);                     //整形診療開始日3_2;
                        if (c == 57)   imp.f059 = getCellValueToString(cell);                     //整形疾病コード1_2;
                        if (c == 58)   imp.f060 = getCellValueToString(cell);                     //整形疾病コード2_2;
                        if (c == 59)   imp.f061 = getCellValueToString(cell);                     //整形疾病コード3_2;
                        if (c == 60)   imp.f062 = getCellValueToString(cell);                     //整形疾病コード4_2;
                        if (c == 61)   imp.f063 = getCellValueToString(cell);                     //整形疾病コード5_2;
                        if (c == 62)   imp.f064 = getCellValueToString(cell);                     //整形診診療日数_2;
                        if (c == 63)   imp.f065 = getCellValueToString(cell);                     //整形費用額_2;
                        if (c == 64)   imp.f066 = getCellValueToString(cell);                     //整形審査年月_3;
                        if (c == 65)   imp.f067 = getCellValueToString(cell);                     //整形レセプト全国共通キー_3;
                        if (c == 66)   imp.f068 = getCellValueToString(cell);                     //整形医療機関コード_3;
                        if (c == 67)   imp.f069 = getCellValueToString(cell);                     //整形医療機関名_3;
                        if (c == 68)   imp.f070 = getCellValueToString(cell);                     //整形診療開始日1_3;
                        if (c == 69)   imp.f071 = getCellValueToString(cell);                     //整形診療開始日2_3;
                        if (c == 70)   imp.f072 = getCellValueToString(cell);                     //整形診療開始日3_3;
                        if (c == 71)   imp.f073 = getCellValueToString(cell);                     //整形疾病コード1_3;
                        if (c == 72)   imp.f074 = getCellValueToString(cell);                     //整形疾病コード2_3;
                        if (c == 73)   imp.f075 = getCellValueToString(cell);                     //整形疾病コード3_3;
                        if (c == 74)   imp.f076 = getCellValueToString(cell);                     //整形疾病コード4_3;
                        if (c == 75)   imp.f077 = getCellValueToString(cell);                     //整形疾病コード5_3;
                        if (c == 76)   imp.f078 = getCellValueToString(cell);                     //整形診療日数_3;
                        if (c == 77)   imp.f079 = getCellValueToString(cell);                     //整形費用額_3;
                        if (c == 78)   imp.f080 = getCellValueToString(cell);                     //宛名番号;
                        if (c == 79)   imp.f081zip = getCellValueToString(cell);                 //郵便番号;
                        if (c == 80)   imp.f082address = getCellValueToString(cell);             //住所;
                        if (c == 81)   imp.f083namekanji = getCellValueToString(cell);           //氏名漢字;
                        
                        
                        //管理項目は最終列で処理
                        if (c == 81)
                        {
                            string strErr = string.Empty;
                            
                            imp.cym = intcym;                                           //メホール請求年月管理用;

                            //審査年月西暦;
                            if (DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f002shinsaym)) == 0) strErr += "審査年月 変換に失敗しました";
                            
                            imp.f100shinsaYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f002shinsaym));

                            imp.f101sejutsuYMAD = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(imp.f005sejutsuym));         //施術年月西暦;                            
                            imp.f102birthAD = DateTimeEx.GetDateFromJstr7(imp.f012pbirthday);                               //生年月日西暦;                            
                            imp.f103firstdateAD = DateTimeEx.GetDateFromJstr7(imp.f033firstdate1);//初検年月日西暦

                            if (!int.TryParse(imp.f038==string.Empty ? "0": imp.f038, out int tmpf038)) strErr += "\r\n 整形審査年月_1西暦 変換に失敗しました";
                            imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf038);                          //整形審査年月_1西暦;


                            if (!int.TryParse(imp.f052 == string.Empty ? "0" : imp.f052, out int tmpf052)) strErr += "\r\n 整形審査年月_2西暦 変換に失敗しました";
                            imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf052);                          //整形審査年月_2西暦;


                            if (!int.TryParse(imp.f066 == string.Empty ? "0" : imp.f066, out int tmpf066)) strErr += "\r\n 整形審査年月_3西暦 変換に失敗しました";
                            imp.f104seikeishisnaYMAD1 = DateTimeEx.GetAdYearMonthFromJyymm(tmpf066);                          //整形審査年月_3西暦;



                            //被保険者証記号半角;                                                                                                
                            //記号は漢字の可能性が                                                                          
                            try
                            {
                                imp.f107hihomark_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);
                            }
                            catch
                            {
                                imp.f107hihomark_narrow = imp.f010hihonum;
                            }

                            imp.f108hihonum_narrow = Microsoft.VisualBasic.Strings.StrConv(imp.f010hihonum, Microsoft.VisualBasic.VbStrConv.Narrow);//証番号半角;



                            if (strErr != string.Empty)
                            {
                                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目:{strErr}");
                                tran.Rollback();
                                return false;
                            }
                        }

                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"住所データ  レセプト全国共通キー:{imp.f003comnum}");

                    if (!DB.Main.Insert<dataImport_base>(imp, tran)) return false;
                }

                return true;
            }         
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{r}行目{c}列目{ex.Message}");
                tran.Rollback();
                return false;
            }           
            finally
            {
                tran.Commit();
                wb.Close();
            }
            
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch(cell.CellType)
            {
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                default:
                    return string.Empty;

            }
           
            
        }

    }

 
}
