﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace Mejor.Pay
{
    class PayCode
    {
        [DB.DbAttribute.PrimaryKey]
        public string Code { get; set; }
        public int UpdateDate { get; set; }
        public string BankNo { get; set; }
        public string BankName { get; set; }
        public string BranchNo { get; set; }
        public string BranchName { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string AccountKana { get; set; }

        [DB.DbAttribute.Ignore]
        public string Key => $"{BankNo}-{BranchNo}-{AccountNo}";

        public static List<PayCode> Select(string accountNo)
        {
            if (accountNo.Length < 7) return new List<PayCode>();
            return DB.Main.Select<PayCode>($"accountno='{accountNo}'").ToList();
        }
    }
}
