﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Pay
{
    public partial class PayInputForm : InputFormCore
    {
        private bool firstTime = true;
        private BindingSource bsApp = new BindingSource();
        private BindingSource bsPayCode = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(400, 2000);
        Point posHihoNum = new Point(800, 0);
        Point posPerson = new Point(0, 400);
        Point posFusho = new Point(100, 800);
        Point posCost = new Point(800, 2000);
        Point posDays = new Point(600, 800);
        Point posNewCont = new Point(800, 1200);
        Point posOryo = new Point(400, 1200);
        Point posNumbering = new Point(800, 2600);

        Control[] ymControls, hihoNumControls, personControls, dayControls, costControls,
            fushoControls, newContControls, oryoControls, numberingControls;

        public PayInputForm(ScanGroup sGroup, bool firstTime, int aid)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxAccountNo };
            hihoNumControls = new Control[] { };
            personControls = new Control[] { };
            dayControls = new Control[] { };
            costControls = new Control[] { };
            fushoControls = new Control[] { };
            newContControls = new Control[] { };
            oryoControls = new Control[] { };
            numberingControls = new Control[] { };

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox) item.Enter += item_Enter;
                    func(item);
                }
            });
            func(panelRight);

            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            list = list.FindAll(x => x.YM > 0);
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.PayCode)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.PayCode)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.PayCode)].HeaderText = "支払コード";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].HeaderText = "状態";
            dataGridViewPlist.Columns[nameof(App.InputStatusEx)].DisplayIndex = 1;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            var l = new List<PayCode>();
            bsPayCode.DataSource = l;
            dataGridPayCode.DataSource = bsPayCode;
            dataGridPayCode.Columns[nameof(PayCode.BankName)].Width = 70;
            dataGridPayCode.Columns[nameof(PayCode.BankName)].HeaderText = "銀行名";           
            dataGridPayCode.Columns[nameof(PayCode.BranchName)].Width = 70;
            dataGridPayCode.Columns[nameof(PayCode.BranchName)].HeaderText = "支店名";

            //20190329185714 furukawa st ////////////////////////
            //不要な列を非表示
            dataGridPayCode.Columns[nameof(PayCode.BankName)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.BranchName)].Visible = false;
            //20190329185714 furukawa ed ////////////////////////


            //20190329185618 furukawa st ////////////////////////
            //グリッドの幅を調整
            dataGridPayCode.Columns[nameof(PayCode.Code)].Width = 80;
            //dataGridPayCode.Columns[nameof(PayCode.Code)].Width = 54;

            //20190329185618 furukawa ed ////////////////////////


            dataGridPayCode.Columns[nameof(PayCode.Code)].HeaderText = "コード";
            dataGridPayCode.Columns[nameof(PayCode.AccountName)].Width = 240;
            dataGridPayCode.Columns[nameof(PayCode.AccountName)].HeaderText = "口座名義";
            dataGridPayCode.Columns[nameof(PayCode.AccountKana)].Width = 240;
            dataGridPayCode.Columns[nameof(PayCode.AccountKana)].HeaderText = "口座名義カナ";
            dataGridPayCode.Columns[nameof(PayCode.BankNo)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.BranchNo)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.AccountNo)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.UpdateDate)].Visible = false;
            dataGridPayCode.Columns[nameof(PayCode.Key)].Visible = false;
            dataGridPayCode.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridPayCode.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;
            bsPayCode.CurrentChanged += BsPayCode_CurrentChanged;

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxAccountNo.Focus();
        }

        private void BsPayCode_CurrentChanged(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrWhiteSpace(verifyBoxPayCode.Text)) return;

            if (bsPayCode.Count < 1) return;
            //if (bsPayCode.Count != 1) return;

            //20190510163055 furukawa st ////////////////////////
            //入力エラー時、グリッドの値取得箇所でエラー落ちするのを回避

            try
            {
                //20190510163055 furukawa ed ////////////////////////


                var listCode = ((PayCode)dataGridPayCode.CurrentRow?.DataBoundItem)?.Code ?? string.Empty;
                verifyBoxPayCode.Text = listCode;

                //20190213173447 furukawa st ////////////////////////
                //口座情報グリッドを選択したとき、特定保険者処理に入る
                SpecialProcess();
                //20190213173447 furukawa ed ////////////////////////
            }
            catch
            {

            }


        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
            changedReset(app);

            //20210621182022 furukawa st ////////////////////////
            //setAppしたあとにバインドリセットすると、複数行で選択したのが解除されてしまう

            //bsPayCode.ResetBindings(false);
            //20210621182022 furukawa ed ////////////////////////
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;
            
            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hihoNumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHihoNum;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = posPerson;
            else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (newContControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNewCont;
            else if (oryoControls.Contains(t)) scrollPictureControl1.ScrollPosition = posOryo;
            else if (numberingControls.Contains(t)) scrollPictureControl1.ScrollPosition = posNumbering;
        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            int aNumber = verifyBoxAccountNo.GetIntValue();
            setStatus(verifyBoxAccountNo, aNumber < 1 || verifyBoxAccountNo.Text.Trim().Length != 7);

            int gCode = verifyBoxPayCode.GetIntValue();
            setStatus(verifyBoxPayCode, gCode < 1 || verifyBoxPayCode.Text.Trim().Length < 1);

            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //値の反映
            app.AccountNumber = verifyBoxAccountNo.Text.Trim();
            app.PayCode = verifyBoxPayCode.Text.Trim();


            return true;
        }
        
        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.拡張ベリ済)) return true;

            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var tran = DB.Main.CreateTransaction())
            {                
                var ut = firstTime ? App.UPDATE_TYPE.FirstInputEx : App.UPDATE_TYPE.SecondInputEx;
                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211029095517 furukawa st ////////////////////////
                //支払先コードをAUX matchingid03に入れる
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, "", "", app.PayCode)) return false;
                //20211029095517 furukawa ed ////////////////////////


                tran.Commit();
                return true;
            }
        }


        #region 20190213173130 furukawa 特定健保用特別処理
        /// <summary>
        /// 20190213173130 furukawa 特定健保用特別処理
        /// </summary>
        /// <returns></returns>
        private void SpecialProcess()
        {
            var app = (App)bsApp.Current;
            string strID_PayData = string.Empty;
            
            try
            {
                
                switch (Insurer.CurrrentInsurer.InsurerID)
                {

                  

                    case (int)InsurerID.SHARP_KENPO:

                        //MNレコード12番　医療機関コード（柔整師コード）
                        //団体→外部機関マスタ(SY - GM.txt）の外部機関CD下5桁
                        //個人→29901固定

                        //SAレコード48番　柔整師個人コード
                        //団体→固定で0090000 + スペース8桁
                        //個人→外部機関CDの下7桁＋スペース8桁

                        if (this.dataGridPayCode.Rows.Count <= 0) return;

                        //20190510154534 furukawa st ////////////////////////
                        //支払先コードを選択されている行のコードにする

                        strID_PayData = this.dataGridPayCode.Rows[dataGridPayCode.CurrentRow.Index].Cells[0].Value.ToString();
                        //strID_PayData = this.dataGridPayCode.Rows[0].Cells[0].Value.ToString();
                        //20190510154534 furukawa ed ////////////////////////


                        if (strID_PayData == string.Empty) return;

                        this.verifyBoxPayCode.Text = strID_PayData.Substring(0,9);


                        //20190531120700 furukawa st ////////////////////////
                        //判断処理変更
                        
                        if (SharpKenpo.PayCodeImporter.getDK(strID_PayData) == SharpKenpo.PayCodeImporter.PayKind.団体)
                            //if (strID_PayData.Substring(9, 2) == "00")
                        //20190531120700 furukawa ed ////////////////////////


                        {
                            //団体の場合
                            app.ClinicNum = strID_PayData.Substring(4, 5);
                            app.DrNum = "0090000".PadRight(15, ' ');

                            //20190322104413 furukawa st ////////////////////////
                            //団体の場合は99固定、個人の場合はパンチ入力そのままとする 
                            //→2019/06/12 個人の場合は支払先コードの３，４桁目を入れる

                            app.HihoPref = 99;
                            //20190322104413 furukawa ed ////////////////////////

                        }
                        else
                        {
                            //その他
                            app.ClinicNum = "29901";
                            app.DrNum = this.verifyBoxPayCode.Text.Substring(2, 7).PadRight(15, ' ');

                            //20190612155946 furukawa st ////////////////////////
                            //仕様変更　個人の場合は支払先コードの３、４桁めを入れる
                            //実際には、取り込むexcelファイルはコード頭２桁を団体個人区分とし、残り９桁をコードとして使用しているため
                            //コードの頭２桁となる

                            app.HihoPref = int.Parse(strID_PayData.Substring(0,2));
                            //20190612155946 furukawa ed ////////////////////////

                        }

                        break;

                    default:
                        break;

                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
        #endregion
       

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);
            bsPayCode.Clear();

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.UfirstEx) +
                "\r\n入力2:  " + User.GetUserName(app.UsecondEx);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.拡張入力済))
            {
                setValues(app);
            }
            else
            {
                bsPayCode.ResetBindings(false);
            }

            //画像の表示
            setImage(app);
            changedReset(app);
        }

        private void verifyBoxAccountNo_Leave(object sender, EventArgs e)
        {
            verifyBoxAccountNo.Text = verifyBoxAccountNo.Text.PadLeft(7, '0');
            //引数で指定した値を用いてデータソースを設定する
            bsPayCode.DataSource = PayCode.Select(verifyBoxAccountNo.Text.Trim());
            bsPayCode.ResetBindings(false);
        }

        //20190510141505 furukawa st ////////////////////////
        //グリッド選択時に値を入れる

    
        private void dataGridPayCode_MouseUp(object sender, MouseEventArgs e)
        {
            SpecialProcess();
        }

        private void dataGridPayCode_KeyUp(object sender, KeyEventArgs e)
        {
//            if (e.KeyCode == Keys.Enter) SpecialProcess();
        }

      
        private void dataGridPayCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SpecialProcess();
        }

        //20190510141505 furukawa ed ////////////////////////

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            else if (hihoNumControls.Contains(t)) posHihoNum = pos;
            else if (personControls.Contains(t)) posPerson = pos;
            else if (dayControls.Contains(t)) posDays = pos;
            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls.Contains(t)) posFusho = pos;
            else if (newContControls.Contains(t)) posNewCont = pos;
            else if (oryoControls.Contains(t)) posOryo = pos;
            else if (numberingControls.Contains(t)) posNumbering = pos;
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// ベリファイ入力時、1回目の入力を各項目のサブテキストボックスに当てはめます
        /// </summary>
        private void setValues(App app)
        {
            if(!app.StatusFlagCheck(StatusFlag.拡張入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.拡張ベリ済);

            //申請書
            setValue(verifyBoxAccountNo, app.AccountNumber, firstTime, nv);
            setValue(verifyBoxPayCode, app.PayCode, firstTime, nv);

            //リスト
            var l = PayCode.Select(verifyBoxAccountNo.Text.Trim());
            bsPayCode.DataSource = l;
            bsPayCode.ResetBindings(false);

            //20210621182124 furukawa st ////////////////////////
            //複数行あるときはループで検索
            
            if (l.Count > 1)
            {
                if (verifyBoxPayCode.Text != string.Empty)
                {
                    for(int r = 0; r < l.Count; r++)
                    {
                        if (dataGridPayCode.Rows[r].Cells[0].Value.ToString().Contains(verifyBoxPayCode.Text))
                        {
                            dataGridPayCode.Rows[r].Selected = true;
                            break;
                        }
                        else dataGridPayCode.Rows[r].Selected = false;
                    }
                }
            }
            //if (l.Count > 1) dataGridPayCode.CurrentCell = null;
            //20210621182124 furukawa ed ////////////////////////


            missCounterReset();
        }

        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxAccountNo.Focus();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
