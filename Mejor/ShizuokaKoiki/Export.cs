﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Mejor.ShizuokaKoiki
{
    class Export
    {
        public static bool ExportViewerData(int cyyyymm)
        {
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            var dir = System.IO.Path.GetDirectoryName(fileName);
            var dataFile = dir + "\\" + cyyyymm.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            System.IO.Directory.CreateDirectory(imgDir);

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();

                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cyyyymm);
                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.SelectCym(cyyyymm);
                var dic = new Dictionary<int, RefRece>();
                rrs.ForEach(rr => { if (rr.Aid != 0) dic.Add(rr.Aid, rr); });

                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                int receCount = 0;
                using (var sw = new System.IO.StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerData.Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();
                    
                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];
                        if (!dic.ContainsKey(app.Aid))
                            throw new Exception($"関連データがない申請書があります AID:{app.Aid}");
                        var rr = dic[app.Aid];

                        var vd = new ViewerData();
                        vd.AID = app.Aid;
                        vd.RrID = rr.RrID;
                        vd.CYM = app.CYM;
                        vd.YM = app.YM;
                        vd.AppType = app.AppType;
                        vd.InsNum = rr.InsNum;
                        vd.Num = rr.Num;
                        vd.Name = rr.Name;
                        vd.Kana = rr.Kana;
                        vd.Sex = (SEX)rr.Sex;
                        vd.Birth = DateTimeEx.ToInt(rr.Birth);
                        vd.ShokenDate = 0;
                        vd.StartDate = 0;
                        vd.Family = 0;
                        if (app.AppType == APP_TYPE.鍼灸)
                        {
                            var fsFunc = new Func<string, string>(s =>
                                {
                                    int hid;
                                    int.TryParse(s, out hid);
                                    return (0 < hid && hid < 8) ? ((HARI_BYOMEI)hid).ToString() : s;
                                });
                            vd.Fusho1 = fsFunc(app.FushoName1);
                            vd.Fusho2 = fsFunc(app.FushoName2);
                            vd.Fusho3 = fsFunc(app.FushoName3);
                            vd.Fusho4 = fsFunc(app.FushoName4);
                            vd.Fusho5 = fsFunc(app.FushoName5);
                        }
                        else
                        {
                            vd.Fusho1 = app.FushoName1;
                            vd.Fusho2 = app.FushoName2;
                            vd.Fusho3 = app.FushoName3;
                            vd.Fusho4 = app.FushoName4;
                            vd.Fusho5 = app.FushoName5;
                        }
                        vd.AnmaCount = 0;
                        vd.AnmaBody = false;
                        vd.AnmaRightUpper = false;
                        vd.AnmaLeftUpper = false;
                        vd.AnmaRightLower = false;
                        vd.AnmaLeftLower = false;
                        vd.Total = rr.Total;
                        vd.Ratio = app.Ratio;
                        vd.Partial = rr.Partial;
                        vd.Charge = rr.Charge;
                        vd.Days = rr.Days;
                        vd.VisitFee = app.Distance == 999;
                        vd.DrNum = rr.DrNum;
                        vd.DrName = rr.DrName;
                        vd.ClinicNum = rr.ClinicNum;
                        vd.ClinicName = rr.ClinicName;
                        vd.GroupNum = string.Empty;
                        vd.GroupName = string.Empty;
                        vd.Zip = rr.Zip;
                        vd.Adds = rr.Add;
                        vd.DestName = string.Empty;
                        vd.DestKana = string.Empty;
                        vd.DestZip = rr.DestZip;
                        vd.DestAdds = rr.DestAdd;
                        vd.Numbering = string.Empty;
                        vd.ComNum = string.Empty;
                        vd.ImageFile = app.Aid.ToString() + ".tif";

                        sw.WriteLine(vd.CreateCsvLine());
                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                ViewerData.CreateInfo(fileName, Insurer.CurrrentInsurer.InsurerName, cyyyymm, receCount);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            return true;
        }

        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);
            var wf = new WaitForm();
            try
            {
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,処理年,処理月,No.,種別,保険者番号,保険者名," +
                        "被保険者番号,被保険者氏名,被保険者カナ氏名,生年月日,施術年,施術月,実日数,合計,請求," +
                        "施術所番号,施術所名,施術師番号,施術師名,送付郵便番号,送付住所,コメント";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    int no = 0;
                    //string insNoStr = ((int)InsurerID.HIROSHIMA_KOIKI).ToString();
                    foreach (var item in list)
                    {
                        var rrs = RefRece.SelectRrID(int.Parse(item.Numbering));
                        if (rrs == null || rrs.Count != 1)
                        {
                            throw new Exception("広域からのデータがない、またはOCR確認が済んでいない申請書が照会対象として指定されています" +
                            "\r\nID:" + item.Aid.ToString());
                        }

                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        var rr = rrs.First();
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(item.AppType.ToString());
                        ss.Add(rr.InsNum);
                        ss.Add(rr.InsName);
                        ss.Add(item.HihoNum.ToString());
                        ss.Add(rr.Name);
                        ss.Add(rr.Kana);
                        ss.Add(rr.Birth.ToJDateShortStr());
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(item.Charge.ToString());
                        ss.Add(rr.ClinicNum);
                        ss.Add(rr.ClinicName);
                        ss.Add(rr.DrNum);
                        ss.Add(rr.DrName);
                        ss.Add(rr.DestAdd == string.Empty ? rr.Zip : rr.DestZip);
                        ss.Add(rr.DestAdd == string.Empty ? rr.Add : rr.DestAdd);
                        ss.Add(item.MemoInspect);

                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        /// <summary>
        /// 事前あはき照会リストマッチング用CSVデータを出力します。
        /// cym=yyyyMM
        /// </summary>
        /// <param name="saveFilePath"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool RefReceExportByCYM(string saveFilePath, int cym)
        {
            var list = RefRece.SelectCym(cym);
            if (list == null) return false;
            using (var swRes = new System.IO.StreamWriter(saveFilePath, true, Encoding.GetEncoding("UTF-8")))
            {
                //ヘッダ
                try
                {
                    var csv = new string[25];
                    var i = 0;
                    csv[i++] = "\"rrid\"";
                    csv[i++] = "\"importid\"";
                    csv[i++] = "\"cym\"";
                    csv[i++] = "\"ym\"";
                    csv[i++] = "\"num\"";
                    csv[i++] = "\"name\"";
                    csv[i++] = "\"kana\"";
                    csv[i++] = "\"zip\"";
                    csv[i++] = "\"add\"";
                    csv[i++] = "\"destzip\"";
                    csv[i++] = "\"destadd\"";
                    csv[i++] = "\"sex\"";
                    csv[i++] = "\"birth\"";
                    csv[i++] = "\"drnum\"";
                    csv[i++] = "\"drname\"";
                    csv[i++] = "\"clinicnum\"";
                    csv[i++] = "\"clinicname\"";
                    csv[i++] = "\"days\"";
                    csv[i++] = "\"total\"";
                    csv[i++] = "\"charge\"";
                    csv[i++] = "\"partial\"";
                    csv[i++] = "\"apptype\"";
                    csv[i++] = "\"aid\"";
                    csv[i++] = "\"insnum\"";
                    csv[i++] = "\"insname\"";
                    swRes.WriteLine(string.Join(",", csv));
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex + "\r\n\r\nREX CSVデータの書き込みに失敗しました");
                    return false;
                }

                foreach (var item in list)
                {
                    var csv = new string[25];
                    var i = 0;
                    csv[i++] = $"\"{item.RrID.ToString()}\"";
                    csv[i++] = $"\"{item.ImportID.ToString()}\"";
                    csv[i++] = $"\"{item.CYM.ToString()}\"";
                    csv[i++] = $"\"{item.YM.ToString()}\"";
                    csv[i++] = $"\"{item.Num.ToString()}\"";
                    csv[i++] = $"\"{item.Name.ToString()}\"";
                    csv[i++] = $"\"{item.Kana.ToString()}\"";
                    csv[i++] = $"\"{item.Zip.ToString()}\"";
                    csv[i++] = $"\"{item.Add.ToString()}\"";
                    csv[i++] = $"\"{item.DestZip.ToString()}\"";
                    csv[i++] = $"\"{item.DestAdd.ToString()}\"";
                    csv[i++] = $"\"{((int)item.Sex).ToString()}\"";
                    csv[i++] = $"\"{item.Birth.ToString("yyyy-MM-dd")}\"";
                    csv[i++] = $"\"{item.DrNum.ToString()}\"";
                    csv[i++] = $"\"{item.DrName.ToString()}\"";
                    csv[i++] = $"\"{item.ClinicNum.ToString()}\"";
                    csv[i++] = $"\"{item.ClinicName.ToString()}\"";
                    csv[i++] = $"\"{item.Days.ToString()}\"";
                    csv[i++] = $"\"{item.Total.ToString()}\"";
                    csv[i++] = $"\"{item.Charge.ToString()}\"";
                    csv[i++] = $"\"{item.Partial.ToString()}\"";
                    csv[i++] = $"\"{((int)item.AppType).ToString()}\"";
                    csv[i++] = $"\"{item.Aid.ToString()}\"";
                    csv[i++] = $"\"{item.InsNum.ToString()}\"";
                    csv[i++] = $"\"{item.InsName.ToString()}\"";

                    try
                    {
                        swRes.WriteLine(string.Join(",", csv));
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex + "\r\n\r\nREX CSVデータの書き込みに失敗しました");
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool ImageSelectExport(int cym)
        {
            var f = new OpenDirectoryDiarog();
            if (f.ShowDialog() != DialogResult.OK) return false;
            var dir = f.Name;


            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています。");
                var l = App.InspectSelect($"FROM application AS a WHERE a.cym={cym} AND a.ym>0");
                var c = (l.Count - 5) / 10 + 1;
                wf.LogPrint($"{l.Count}枚の申請書を取得しました。{c}枚の画像を対象として抽出します。");

                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.SetMax(c);
                var fc = new TiffUtility.FastCopy();
                for (int i = 4; i < l.Count; i += 10)
                {
                    wf.InvokeValue++;
                    try
                    {
                        var s = l[i].GetImageFullPath();
                        var d = dir + "\\" + Path.GetFileName(s);
                        if (!fc.FileCopy(s, d))
                        {
                            var res = MessageBox.Show($"AID:{l[i].Aid}  ファイル名:{s}\r\n" +
                                $"ファイルのコピーに失敗しました。処理を続けますか？",
                                "", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                            if (res != DialogResult.Yes) return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        var res = MessageBox.Show(ex.Message + "\r\n" +
                            $"AID:{l[i].Aid}\r\n" +
                            $"ファイルのコピーに失敗しました。処理を続けますか？",
                            "", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                        if (res != DialogResult.Yes) return false;
                    }
                }

                MessageBox.Show("画像の抽出が終了しました。");
                return true;
            }

        }
    }
}
