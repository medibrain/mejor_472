﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace Mejor
{
    /// <summary>
    /// 平成のみを扱う日付テキストボックス
    /// </summary>
    class DateBoxH : TextBox
    {
        KeysConverter kc = new KeysConverter();

        //public bool _setIndexToMonth = false;

        [System.ComponentModel.Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string Text
        {
            get { return base.Text; }
            private set { base.Text = value; }
        }

        [System.ComponentModel.Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new int MaxLength
        {
            get { return base.MaxLength; }
            private set { base.MaxLength = value; }
        }

        /// <summary>
        /// 現在設定されているテキストがデフォルトのものか、設定します。
        /// </summary>
        public bool IsDefault
        {
            get { return this.Text == DefaultText; }
        }

        /// <summary>
        /// 日付をセットします。失敗した場合、またはNULLの場合、デフォルトの表示を行います。
        /// </summary>
        /// <param name="dt"></param>
        public void SetDate(DateTime dt)
        {
            if (dt == DateTimeEx.DateTimeNull) this.Text = DefaultText;
            var t = DateTimeEx.GetJpDateStr(dt);
            this.Text = t == string.Empty ? DefaultText : t.Substring(2);
        }

        public string DefaultText { get; private set; }

        /// <summary>
        /// 指定されている日付を返します。
        /// </summary>
        /// <returns></returns>
        public DateTime GetDate()
        {
            if (this.IsDefault) return DateTimeEx.DateTimeNull;
            var dateText = Text.Replace('_', ' ');
            int y, m, d;
            DateTime dt;

            if (!DateTimeEx.TryGetYear("平成" + dateText.Remove(2), out y)) return DateTimeEx.DateTimeNull;
            if (!int.TryParse(dateText.Substring(3, 2), out m)) return DateTimeEx.DateTimeNull;
            if (!int.TryParse(dateText.Substring(6, 2), out d)) return DateTimeEx.DateTimeNull;
            if (!DateTimeEx.IsDate(y, m, d)) return DateTimeEx.DateTimeNull;
            dt = new DateTime(y, m, d);
            if (DateTimeEx.GetEra(dt) != dateText.Remove(2)) return DateTimeEx.DateTimeNull;

            return dt;
        }

        public DateBoxH()
        {
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.DefaultText = "__年__月__日";
            this.Text = DefaultText;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DateBox_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DateBox_KeyPress);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.DateBox_Validating);
            this.Enter += new System.EventHandler(this.DateBox_Enter);
            this.Click += DateBox_Click;

            MaxLength = 9;
            var yStr = DateTimeEx.GetJpYearStr(Text.Remove(2));

            if (yStr == string.Empty)
            {
                Text = "__年" + Text.Substring(3, 2) + "月" + Text.Substring(6, 2) + "日";
            }
            else
            {
                Text = yStr + "年" + Text.Substring(3, 2) + "月" + Text.Substring(6, 2) + "日";
            }
        }

        

        void DateBox_Click(object sender, EventArgs e)
        {
            int ss = SelectionStart;
            if (ss == 0 || ss == 1 || ss == 2)
            {
                ss = 0;
            }
            else if (ss == 3 || ss == 4 || ss == 5)
            {
                ss = 3;
            }
            else if (ss == 6 || ss == 7 || ss == 8)
            {
                ss = 6;
            }
            SelectionStart = ss;
            SelectionLength = 2;
        }

        protected override void WndProc(ref Message m)
        {
            //切り取り貼り付け無効
            const int WM_PASTE = 0x0302;
            const int WM_CUT = 0x0300;

            if (m.Msg != WM_PASTE && m.Msg != WM_CUT) base.WndProc(ref m);
        }

        private void DateBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ReadOnly) return;
            textAdjustJDate(e);
        }

        private void textAdjustJDate(KeyPressEventArgs e)
        {
            int sIndex = this.SelectionStart;

            //backspaceの場合、別処理
            if (e.KeyChar == (char)Keys.Back)
            {
                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = DefaultText.Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                    SelectionStart = sIndex;
                }
                else if (sIndex == 3 || sIndex == 6 || sIndex == 9)
                {
                    this.Text = Text.Remove(sIndex - 2) + "_" + Text.Substring(sIndex - 1);
                    SelectionStart = sIndex - 2;
                }
                else if (sIndex == 1 || sIndex == 2 || sIndex == 4 || sIndex == 5 || sIndex == 7 || sIndex == 8)
                {
                    this.Text = Text.Remove(sIndex - 1) + "_" + Text.Substring(sIndex);
                    SelectionStart = sIndex - 1;
                }
                e.Handled = true;
            }
            else
            {
                if (!char.IsLetterOrDigit(e.KeyChar))
                {
                    //文字以外の場合処理終了
                    e.Handled = true;
                    return;
                }

                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = DefaultText.Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                }

                if (sIndex == 0)
                {
                    this.Text = Text.Remove(sIndex) + e.KeyChar + Text.Substring(sIndex + 1);
                    SelectionStart = sIndex + 1;
                }
                else if (sIndex == 3)
                {
                    if (e.KeyChar == '0' || e.KeyChar == '1')
                    {
                        this.Text = Text.Remove(3) + e.KeyChar + Text.Substring(sIndex + 1);
                        SelectionStart = 4;
                    }
                    else
                    {
                        this.Text = Text.Remove(3) + "0" + e.KeyChar + Text.Substring(5);
                        SelectionStart = 6;
                    }
                }
                else if (sIndex == 6)
                {
                    if (e.KeyChar == '0' || e.KeyChar == '1' || e.KeyChar == '2' || e.KeyChar == '3')
                    {
                        this.Text = Text.Remove(6) + e.KeyChar + Text.Substring(7);
                        SelectionStart = 7;
                    }
                    else
                    {
                        this.Text = Text.Remove(6) + "0" + e.KeyChar + Text.Substring(9);
                        SelectionStart = 8;
                    }
                }

                else if (sIndex == 1 || sIndex == 4 || sIndex == 7)
                {
                    this.Text = Text.Remove(sIndex) + e.KeyChar + Text.Substring(sIndex + 1);
                    SelectionStart = sIndex + 2;
                }
                e.Handled = true;
            }
        }

        private void DateBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (ReadOnly) return;
            int sIndex = this.SelectionStart;

            if (e.KeyCode == Keys.Delete)
            {
                //文字選択時
                if (SelectionLength > 0)
                {
                    var defStr = DefaultText.Substring(SelectionStart, SelectionLength);
                    Text = Text.Remove(SelectionStart) + defStr + Text.Substring(SelectionStart + SelectionLength);
                }
                else
                {
                    if (sIndex == 0 || sIndex == 1 || sIndex == 3 || sIndex == 4 || sIndex == 6 || sIndex == 7)
                    {
                        this.Text = Text.Remove(sIndex) + "_" + Text.Substring(sIndex + 1);
                        SelectionStart = sIndex;
                    }
                }
                e.Handled = true;
                SelectionStart = sIndex;
            }
            else if (e.KeyCode == Keys.Space && e.Shift)
            {
                e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up)
            {
                e.Handled = true;
                int ss = SelectionStart;
                var dt = GetDate();
                if (dt.IsNullDate()) return;

                if (ss == 0 || ss == 1 || ss == 2 )
                {
                    dt = dt.AddYears(1);
                }
                else if (ss == 3 || ss == 4 || ss == 5)
                {
                    dt = dt.AddMonths(1);
                }
                else
                {
                    dt = dt.AddDays(1);
                }

                SetDate(dt);
                SelectionStart = ss;
            }
            else if (e.KeyCode == Keys.Down)
            {
                e.Handled = true;
                int ss = SelectionStart;
                var dt = GetDate();
                if (dt.IsNullDate()) return;

                if (ss == 0 || ss == 1 || ss == 2)
                {
                    dt = dt.AddYears(-1);
                }
                else if (ss == 3 || ss == 4 || ss == 5)
                {
                    dt = dt.AddMonths(-1);
                }
                else
                {
                    dt = dt.AddDays(-1);
                }

                SetDate(dt);
                SelectionStart = ss;
            }
        }

        private void DateBox_Validating(object sender, CancelEventArgs e)
        {
            var dt = GetDate();
            if (dt != DateTimeEx.DateTimeNull) SetDate(dt);
        }

        private void DateBox_Enter(object sender, EventArgs e)
        {
            this.SelectAll();
        }

        /// <summary>
        /// デフォルトの表示に戻します。("__年__月__日")
        /// </summary>
        public override void ResetText()
        {
            this.Text = DefaultText;
        }
    }
}
