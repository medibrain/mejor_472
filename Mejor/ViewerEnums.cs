﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor
{
    public class ViewerEnums
    {
        [Flags]
        public enum ShokaiReason
        {
            なし = 0,
            部位数 = 0x1,
            施術日数 = 0x2,
            施術期間 = 0x4,
            疑義施術所 = 0x8,
            合計金額 = 0x10,
            その他 = 0x1_0000,
        }


        [Flags]
        public enum ShokaiResult
        {
            なし = 0,

            宛所なし = 0x10_0000,
            相違なし = 0x20_0000,
            相違あり = 0x40_0000
        }

        [Flags]
        public enum KagoReason
        {
            なし = 0,
            署名違い = 0x1, 筆跡違い = 0x2, 家族同一筆跡 = 0x4,
            原因なし = 0x8, 長期理由なし = 0x10, 負傷原因相違 = 0x20,
            その他 = 0x80,
            //原因なし1 = 0x100, 原因なし2 = 0x200, 原因なし3 = 0x400, 原因なし4 = 0x800, 原因なし5 = 0x1000,
            //長期理由なし1 = 0x2000, 長期理由なし2 = 0x4000, 長期理由なし3 = 0x8000, 長期理由なし4 = 0x10000, 長期理由なし5 = 0x20000,
            //負傷原因相違1 = 0x40000, 負傷原因相違2 = 0x80000, 負傷原因相違3 = 0x100000, 負傷原因相違4 = 0x200000, 負傷原因相違5 = 0x400000,
        }

        [Flags]
        public enum SaishinsaReason
        {
            なし = 0,
            初検料 = 0x1, その他 = 0x2,

            負傷1 = 0x10, 負傷2 = 0x20, 負傷3 = 0x40,
            負傷4 = 0x80, 負傷5 = 0x100,

            転帰なし = 0x1000, 中止 = 0x2000,
            同一負傷名 = 0x4000, 別負傷名 = 0x8000,
        }

        [Flags]
        public enum HenreiReason
        {
            なし = 0,
            負傷部位 = 0x1, 負傷原因 = 0x2, 負傷時期 = 0x4,
            けが外 = 0x8, 受療日数 = 0x10, 勤務中 = 0x20,
            過誤 = 0x40, その他 = 0x80,
            //負傷部位1 = 0x100, 負傷部位2 = 0x200, 負傷部位3 = 0x400, 負傷部位4 = 0x800, 負傷部位5 = 0x1000,
            //負傷原因1 = 0x2000, 負傷原因2 = 0x4000, 負傷原因3 = 0x8000, 負傷原因4 = 0x1_0000, 負傷原因5 = 0x2_0000,
        }
    }
}
