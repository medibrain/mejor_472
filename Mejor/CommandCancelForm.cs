﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class CommandCancelForm : Form
    {
        DateTime dt;

        public CommandCancelForm(DateTime dt)
        {
            InitializeComponent();
            this.dt = dt;
            timer1.Start();
            labelTime.Text = "経過時間: " + (DateTime.Now - dt).ToString(@"hh\:mm\:ss");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var res = MessageBox.Show("データベースアクセスを中止します。よろしいですか？",
                "中止確認",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Asterisk,
                MessageBoxDefaultButton.Button2);
            if (res != DialogResult.OK) return;

            res = MessageBox.Show("データベースアクセスを中止します。" +
                "現在の処理は失敗となりますが、本当によろしいですか？", "中止確認",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Asterisk,
                MessageBoxDefaultButton.Button2);
            if (res != DialogResult.OK) return;

            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void CommandCancelForm_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawIcon(SystemIcons.Information, 20, 20);
        }

        public void InvokeDispose()
        {
            if (this.Visible)
            {
                Invoke(new Action(() => this.Dispose()));
            }
            else
            {
                this.Dispose();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            labelTime.Text = "経過時間: " + (DateTime.Now - dt).ToString(@"hh\:mm\:ss");
        }
    }
}
