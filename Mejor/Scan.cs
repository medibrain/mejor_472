﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NpgsqlTypes;

namespace Mejor
{
    public enum SCAN_STATUS { NULL = 0, SCANING = 1, SCANED = 2, OCR済み = 3, MATCHED = 4, INQUIRY = 5 }

    public class Scan
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int SID { get; set; }
        public DateTime ScanDate { get; set; }
        public int Cyear { get; set; }
        public int Cmonth { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public SCAN_STATUS Status { get; set; }
        public APP_TYPE AppType { get; set; }
        public int CYM { get; private set; }


        public static InsurerID insurerForScan = 0;        //20190326141746 furukawa 保険者で処理を分けるため追加
        
        public static int maxNumbering =1;//20191010181947 furukawa ナンバリング用変数

        public static string strOrgFileName = string.Empty;//20191128164124 furukawaオリジナルのフルパス
        

        public string ImageFolder
        {
            get
            {
                if (SID == 0) throw new Exception("スキャンIDが確定していません");//

                //20201119121345 furukawa st ////////////////////////
                //settings.xmlではなくjyusei.insurerから保険者ごとの設定を取得
                
                var ip = App.GetImageFolderPath(DB.GetMainDBName());
                //var ip = Settings.ImageFolder;    //"\\\\192.168.200.100\\scan";
                //20201119121345 furukawa ed ////////////////////////


                ip += "\\" + DB.GetMainDBName();
                ip += "\\" + SID.ToString();
                return ip;
            }
        }

        /// <summary>
        /// //20191023133050 furukawa カウンタテーブル値取得
        /// </summary>
        /// <returns></returns>
        private static int GetCounter(int ins,APP_TYPE apptype,int cmonth)
            //private static int GetCounter(int ins)
        {
            int intret = 0;
            
            //保険者を追加したらテーブルも必要
            if (ins == (int)InsurerID.SHIMANE_KOIKI)
            {                
                intret = DB.Main.QueryFirstOrDefault<int>("select cnt from ahkcounter");

                //20191024173921 furukawa st ////////////////////////               
                //年度リセット あはき AND 4月 AND 値＞3000
                if (apptype != APP_TYPE.柔整 && cmonth == 4 && intret>3000)
                {
                    DB.Main.Excute("update ahkcounter set cnt=1");
                    intret = 1;
                }
                //20191024173921 furukawa ed ////////////////////////           
            }

            return intret;
        }


        /// <summary>
        /// //20191023133123 furukawa カウンタテーブル値更新
        /// </summary>
        /// <param name="ins"></param>
        /// <param name="num"></param>
        private static void UpdCounter(int ins,int num,APP_TYPE apptype)
        {
            //保険者を追加したらテーブルも必要
            if (ins == (int)InsurerID.SHIMANE_KOIKI)
            {
                //業者年間の通しナンバリング
                if (apptype == APP_TYPE.あんま || apptype == APP_TYPE.鍼灸)
                {
                    DB.Main.Excute($"update ahkcounter set cnt={num}");
                }
            }
        }





        /// <summary>
        /// 画像をデータベースに登録します。ファイル数が800を超える場合、複数のScanに分割します。
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="note1"></param>
        /// <param name="note2"></param>
        /// <param name="appType"></param>
        /// <param name="wf"></param>
        /// <param name="fileNameIsNumbeirng">ファイル名をナンバリングとする</param>
        /// <param name="NeedNumbering">ナンバリングが必要</param>
        /// <param name="IgnoreNumberingHyphen">ハイフンを無視するフラグ</param>
        /// <param name="insurerID"></param>
        /// <param name="keepOrgFileName"></param>
        /// <returns></returns>


        //20191128164303 furukawa st ////////////////////////
        //元ファイル名保持フラグ追加

        public static bool DoScan(string dirPath, int cYear, int cMonth, string note1, string note2,
                    APP_TYPE appType, WaitForm wf, bool fileNameIsNumbeirng, bool NeedNumbering, bool IgnoreNumberingHyphen,
                    int insurerID,bool keepOrgFileName)



                //20191010182146 furukawa st ////////////////////////
                //ナンバリング用引数追加

                //public static bool DoScan(string dirPath, int cYear, int cMonth, string note1, string note2,
                //APP_TYPE appType, WaitForm wf, bool fileNameIsNumbeirng, bool NeedNumbering ,bool IgnoreNumberingHyphen, int insurerID)

                //              public static bool DoScan(string dirPath, int cYear, int cMonth, string note1, string note2, 
                //              APP_TYPE appType, WaitForm wf, bool fileNameIsNumbeirng , bool IgnoreNumberingHyphen)
                //20191010182146 furukawa ed ////////////////////////

        //20191128164303 furukawa ed ////////////////////////
        {
            var fc = new TiffUtility.FastCopy();
            var s = new Scan();
            var fl = System.IO.Directory.GetFiles(dirPath);
            int baseGid = 1;
            int acount = 0;
            int nameCount = 0;
            int aid;
            int gid = 0;


            //20191023131203 furukawa st ////////////////////////
            //ナンバリング取得
            int numberingCount = GetCounter(insurerID,appType,cMonth);
            //20191023131203 furukawa ed ////////////////////////


            //頭ゼロなしでも順番にソート
            Array.Sort(fl, ((x, y) => x.PadLeft(20,'0').CompareTo(y.PadLeft(20, '0'))));

            using (var tran = DB.Main.CreateTransaction())
            {
                foreach (var item in fl)
                {
                    //20210129130413 furukawa st ////////////////////////
                    //キャンセルボタン時処理実装
                    
                    if (wf.Cancel)
                    {
                        if (MessageBox.Show("取り込み中止しますか？",
                            Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes)
                            return false;
                    }
                    //20210129130413 furukawa ed ////////////////////////



                    //拡張子判断
                    var extension = System.IO.Path.GetExtension(item).ToLower();
                    if (extension != ".tif" && extension != ".tiff") continue;

                    //ScanIDを追加する
                    if (nameCount == 0 || nameCount % 800 == 0)
                    {
                        //初回以外
                        if (nameCount != 0)
                        {
                            //まず現在のScanの登録済みフラグを上げる
                            s.Status = SCAN_STATUS.SCANED;
                            s.Update(tran);

                            //次のスキャン情報
                            s = new Scan();
                        }

                        s.Cmonth = cMonth;
                        s.Cyear = cYear;
                        s.Note1 = note1;
                        s.Note2 = note2;
                        s.ScanDate = DateTime.Today;
                        s.Status = SCAN_STATUS.SCANING;
                        s.AppType = appType;

                        //20190424191119_2019/04/07 GetAdYearFromHS変更対応
                        //s.CYM = DateTimeEx.GetAdYearFromHs(cYear) * 100 + cMonth;
                        s.CYM = DateTimeEx.GetAdYearFromHs(cYear * 100 + cMonth) * 100 + cMonth;
                        


                        //Scan IDはここでserial値として付与される
                        if (!s.Insert(tran))
                        {
                            Log.ErrorWriteWithMsg("Scan IDの取得に失敗しました");
                            return false;
                        }

                        try
                        {
                            //フォルダを作成
                            System.IO.Directory.CreateDirectory(s.ImageFolder);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("フォルダの作成に失敗しました\r\n" + ex, "", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                            return false;
                        }

                        baseGid = 1;
                        acount = 0;
                        nameCount = 0;

                        //ひとつ目のScanGroupの登録
                        gid = s.SID * 100 + baseGid;


                        //20191010182249 furukawa st ////////////////////////
                        //ナンバリング用変数リセット                        
                        maxNumbering = 1;
                        //20191010182249 furukawa ed ////////////////////////


                        if (!ScanGroup.CreateScanGroup(s, gid, tran)) return false;
                    }

                    //グループID設定
                    if (acount == Settings.GroupCount)
                    {
                        baseGid++;
                        acount = 0;

                        gid = s.SID * 100 + baseGid;

                        //20191010182342 furukawa st ////////////////////////
                        //グループが変わったらナンバリングリセット                        
                        maxNumbering = 1;
                        //20191010182342 furukawa ed ////////////////////////


                        if (!ScanGroup.CreateScanGroup(s, gid, tran)) return false;


                    }

                    acount++;
                    nameCount++;
                    wf.InvokeValue++;

                    //画像ファイルをコピー
                    aid = gid * 1000 + nameCount;
                    var newfn = s.ImageFolder + "\\" + aid.ToString() + ".tif";
                    try
                    {
                        //20200207095715 furukawa st ////////////////////////
                        //更新ストアドwhere句での処理が面倒なのでファイル名だけにする
                        
                        strOrgFileName = System.IO.Path.GetFileName(item);

                        //20191128164507 furukawa st ////////////////////////
                        //オリジナルフルパスを控えておく                   
                        //strOrgFileName = item;
                        //20191128164507 furukawa ed ////////////////////////


                        //20200207095715 furukawa ed ////////////////////////



                        fc.FileCopy(item, newfn);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg("画像ファイルのコピーに失敗しました\r\n" + ex);
                        return false;
                    }

                    try
                    {                      
                        if (fileNameIsNumbeirng)
                        {

                            //20200227095537 furukawa st ////////////////////////
                            //保険者ごとに処理わけルーチン
                            
                            string numbering = string.Empty;


                            switch (insurerID)
                            {


                                //20200228143547 furukawa st 削除 ////////////////////////
                                //協会けんぽ広島支部、Numberingには通常と同じく拡張子ありファイル名を入れ、インポートExcelに合わせる

                                //case (int)InsurerID.KYOKAIKENPO_HIROSHIMA:
                                //    //協会けんぽ広島支部の場合、ファイル名をそのままnumberingに登録
                                //    numbering = System.IO.Path.GetFileName(item);
                                //    break;
                                //20200228143547 furukawa ed ////////////////////////


                                default:
                                    //上記以外は拡張子を除いたファイル名をnumberingとする
                                    numbering = System.IO.Path.GetFileNameWithoutExtension(item);
                                    break;
                            }
                                                //var numbering = System.IO.Path.GetFileNameWithoutExtension(item);

                            //20200227095537 furukawa ed ////////////////////////



                            if (IgnoreNumberingHyphen) numbering = numbering.Replace("-", "");

                            //20210603195057 furukawa st ////////////////////////
                            //画像登録時にauxに元ファイル名を入れるため引数追加                            
                            App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), numbering,strOrgFileName);
                            //      App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), numbering);
                            //20210603195057 furukawa ed ////////////////////////
                        }


                        //20191010182414 furukawa st ////////////////////////
                        //ナンバリングが必要な場合は保険者別に設定
                        else if (NeedNumbering)
                        {
                            
                            string strNumbering = string.Empty;
                            switch(insurerID)
                            {
                                
                                case (int)InsurerID.SHIMANE_KOIKI:
                                    if(s.AppType==APP_TYPE.あんま || s.AppType == APP_TYPE.鍼灸)
                                    {
                                        //RA201908+連番22桁123457890123456789012  
                                        
                                        strNumbering = "RA" + s.CYM + numberingCount++.ToString().PadLeft(22, '0');
                                    }
                                    break;


                                    //case (int)InsurerID.TOYOHASHI_KOKUHO:
                                    //    //豊橋市国保の場合
                                    //    //01234512345  (gid＋連番)
                                    //    //最大に+1

                                    //    strNumbering = gid.ToString("000000") + (maxNumbering++).ToString().PadLeft(5, '0');
                                    //    break;
                            }


                            //20210603195123 furukawa st ////////////////////////
                            //画像登録時にauxに元ファイル名を入れるため引数追加                            
                            App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), strNumbering,strOrgFileName);
                            //App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), strNumbering);
                            //20210603195123 furukawa ed ////////////////////////

                        }
                        //20191010182414 furukawa ed ////////////////////////



                        else
                        {
                            //20210603195143 furukawa st ////////////////////////
                            //画像登録時にauxに元ファイル名を入れるため引数追加
                            
                            App.ImageEntry(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), tran,strOrgFileName);
                            //App.ImageEntry(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), tran);
                            //20210603195143 furukawa ed ////////////////////////
                        }

                        //20191128164546 furukawa st ////////////////////////
                        //フラグが立っている保険者はオリジナルファイル名をemptytext3に更新

                        if (keepOrgFileName)
                        {
                            using (var cmd = DB.Main.CreateCmd($"update application set emptytext3='{strOrgFileName}' where aid={aid}", tran))
                            {
                                cmd.TryExecuteNonQuery();
                            }
                        }
                        //20191128164546 furukawa ed ////////////////////////


                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg("画像ファイルのデータベース登録に失敗しました\r\n" + ex);
                        return false;
                    }
                }

                //20191023114513 furukawa st ////////////////////////
                //最後のナンバリングを更新する
                if (NeedNumbering) UpdCounter(insurerID,numberingCount,s.AppType);                
                //20191023114513 furukawa ed ////////////////////////


                s.Status = SCAN_STATUS.SCANED;
                s.Update(tran);
                tran.Commit();
            }

            return true;
        }

        /// <summary>
        /// マルチページTIFFが含まれる場合に使用します。
        /// ページ単位に画像ファイルを分割し、個別にデータを作成します。
        /// </summary>
        /// <param name="dirPath"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="note1"></param>
        /// <param name="note2"></param>
        /// <param name="appType"></param>
        /// <param name="wf"></param>
        /// <param name="fileNameIsNumbeirng">ファイル名=numbering</param>
        /// <param name="=IgnoreNumberingHyphen">ハイフン無視フラグ</param>
        /// <param name="keepOrgFileName">ファイル名控えるフラグ</param>
        /// <returns></returns>
        /// 

        //20191128163808 furukawa st ////////////////////////
        //元ファイル名保持フラグ追加
        

        public static bool DoScanMultipage(string dirPath, int cYear, int cMonth, string note1, string note2,
                            APP_TYPE appType, WaitForm wf, bool fileNameIsNumbeirng, bool NeedNumbering, 
                            bool IgnoreNumberingHyphen, int insurerID,bool keepOrgFileName)
        #region old
        //20190414190642 furukawa st ////////////////////////
        //ファイル名=numbering、ハイフン無視フラグ追加


        //public static bool DoScanMultipage(string dirPath, int cYear, int cMonth, string note1, string note2,
        //                                APP_TYPE appType, WaitForm wf, bool fileNameIsNumbeirng,  bool NeedNumbering,bool IgnoreNumberingHyphen,int insurerID)

        //20191010183043 furukawa st ////////////////////////
        //ナンバリング用引数追加


        //public static bool DoScanMultipage(string dirPath, int cYear, int cMonth, string note1, string note2,
        //    APP_TYPE appType, WaitForm wf, bool fileNameIsNumbeirng, bool IgnoreNumberingHyphen)

        //20191010183043 furukawa ed ////////////////////////




        //        public static bool DoScanMultipage(string dirPath, int cYear, int cMonth, string note1, string note2,
        //          APP_TYPE appType, WaitForm wf)

        //20190414190642 furukawa ed ////////////////////////
        #endregion

        //20191128163808 furukawa ed ////////////////////////
        {
            var fc = new TiffUtility.FastCopy();
            var s = new Scan();
            var fl = System.IO.Directory.GetFiles(dirPath);
            int baseGid = 1;
            int acount = 0;
            int nameCount = 0;
            int aid;
            int gid = 0;


            maxNumbering = 1;//20191010183226 furukawa ナンバリング用変数リセット

            //20191023141145 furukawa st ////////////////////////
            //カウンタテーブル値取得

            int numberingCount = GetCounter(insurerID, appType, cMonth);

            //20191023141145 furukawa ed ////////////////////////


            //頭ゼロなしでも順番にソート
            Array.Sort(fl, ((x, y) => x.PadLeft(20, '0').CompareTo(y.PadLeft(20, '0'))));

            using (var tran = DB.Main.CreateTransaction())
            {
                foreach (var item in fl)
                {
                    //20210129130500 furukawa st ////////////////////////
                    //キャンセルボタン時処理実装
                    
                    if (wf.Cancel)
                    {
                        if(MessageBox.Show("取り込み中止しますか？",
                            Application.ProductName,MessageBoxButtons.YesNo)==DialogResult.Yes)
                            return false;
                    }
                    //20210129130500 furukawa ed ////////////////////////



     
                    //拡張子判断
                    var extension = System.IO.Path.GetExtension(item).ToLower();
                    if (extension != ".tif" && extension != ".tiff") continue;

                    //TIFFファイルをページ単位に分解
                    List<byte[]> frames;
                    try
                    {
                        frames = TiffUtility.GetFrameStreams(item);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg("TIFFファイルのページ分割に失敗しました\r\n" + ex);
                        return false;
                    }

                    //20201113131726 furukawa st ////////////////////////
                    //マルチtiffを分割する際、ページ番号をつけて申請書だけでも区別する                    
                    int pageno = 0;
                    //20201113131726 furukawa ed ////////////////////////


                    //ページごとに登録＆コピー
                    foreach (var frame in frames)
                    {
                        //ScanIDを追加する
                        if (nameCount == 0 || nameCount % 800 == 0)
                        {
                            //初回以外
                            if (nameCount != 0)
                            {
                                //まず現在のScanの登録済みフラグを上げる
                                s.Status = SCAN_STATUS.SCANED;
                                s.Update(tran);

                                //次のスキャン情報
                                s = new Scan();
                            }

                            s.Cmonth = cMonth;
                            s.Cyear = cYear;
                            s.Note1 = note1;
                            s.Note2 = note2;
                            s.ScanDate = DateTime.Today;
                            s.Status = SCAN_STATUS.SCANING;
                            s.AppType = appType;


                            
                            //20190424191119_2019/04/07 GetAdYearFromHS変更対応
                            s.CYM = DateTimeEx.GetAdYearFromHs(cYear * 100 + cMonth) * 100 + cMonth ;
                            //s.CYM = DateTimeEx.GetAdYearFromHs(cYear) * 100 + cMonth;


                            //Scan IDはここでserial値として付与される
                            if (!s.Insert(tran))
                            {
                                Log.ErrorWriteWithMsg("Scan IDの取得に失敗しました");
                                return false;
                            }

                            try
                            {
                                //フォルダを作成
                                System.IO.Directory.CreateDirectory(s.ImageFolder);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("フォルダの作成に失敗しました\r\n" + ex, "", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                                return false;
                            }

                            baseGid = 1;
                            acount = 0;
                            nameCount = 0;

                            //ひとつ目のScanGroupの登録
                            gid = s.SID * 100 + baseGid;


                            //20191010183314 furukawa st ////////////////////////
                            //ナンバリング用変数リセット
                            maxNumbering = 1;
                            //20191010183314 furukawa ed ////////////////////////
                            
                            if (!ScanGroup.CreateScanGroup(s, gid, tran)) return false;
                            
                           
                        }

                        //グループID設定
                        if (acount == Settings.GroupCount)
                        {
                            baseGid++;
                            acount = 0;

                            gid = s.SID * 100 + baseGid;



                            //20191010183357 furukawa st ////////////////////////
                            //グループが変わったらナンバリングリセット                            
                            maxNumbering = 1;
                            //20191010183357 furukawa ed ////////////////////////

                            if (!ScanGroup.CreateScanGroup(s, gid, tran)) return false;

                        }

                        acount++;
                        nameCount++;
                        

                        //画像ファイルをコピー
                        aid = gid * 1000 + nameCount;
                        var newfn = s.ImageFolder + "\\" + aid.ToString() + ".tif";
                        try
                        {
                            using(var fs = new FileStream(newfn, FileMode.Create))
                            {
                                fs.Write(frame, 0, frame.Length);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorWriteWithMsg("画像ファイルのコピーに失敗しました\r\n" + ex);
                            return false;
                        }

                        //20200207095851 furukawa st ////////////////////////
                        //更新ストアドwhere句での処理が面倒なのでファイル名だけにする
                        
                        strOrgFileName = System.IO.Path.GetFileName(item);


                                //20191128163943 furukawa st ////////////////////////
                                //オリジナルファイル名を控えておく
                                //strOrgFileName = item;
                                //20191128163943 furukawa ed ////////////////////////


                        //20200207095851 furukawa ed ////////////////////////



                        //application登録

                        //20190414190734 furukawa st ////////////////////////
                        //ファイル名=numbering、ハイフン無視フラグで分岐

                        try
                        {
                            #region ファイル名＝ナンバリングの場合
                            if (fileNameIsNumbeirng)
                            {

                                //20200227095146 furukawa st ////////////////////////
                                //保険者ごとに処理わけルーチン
                                
                                string numbering = string.Empty;


                                switch (insurerID) {
                                    //20200228145911 furukawa st 削除////////////////////////
                                    //協会けんぽ広島支部、Numberingには通常と同じく拡張子ありファイル名を入れ、インポートExcelに合わせる

                                    //case (int)InsurerID.KYOKAIKENPO_HIROSHIMA:
                                    //    //協会けんぽ広島支部の場合、ファイル名をそのままnumberingに登録
                                    //    numbering = System.IO.Path.GetFileName(item);
                                    //    break;
                                    //20200228145911 furukawa ed ////////////////////////

                                     default:
                                        //上記以外は拡張子を除いたファイル名をnumberingとする
                                        numbering = System.IO.Path.GetFileNameWithoutExtension(item);
                                        break;
                                }

                                                    //var numbering = System.IO.Path.GetFileNameWithoutExtension(item);

                                //20200227095146 furukawa ed ////////////////////////


                                if (IgnoreNumberingHyphen) numbering = numbering.Replace("-", "");






                                //20210603194830 furukawa st ////////////////////////
                                //画像登録時にauxに元ファイル名を入れるため引数追加
                                
                                App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), numbering, pageno, strOrgFileName);

                                //      20201113155944 furukawa st ////////////////////////
                                //      申請書区別用ページ番号カウント追加
                                //      App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), numbering, pageno);
                                //                App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), numbering);
                                //      20201113155944 furukawa ed ////////////////////////

                                //20210603194830 furukawa ed ////////////////////////



                            }

                            //20191010183544 furukawa st ////////////////////////
                            //ナンバリングが必要な場合は保険者別に設定
                            #endregion

                            #region ナンバリングが必要だがファイル名とイコールではない場合
                            else if (NeedNumbering)
                            {

                                string strNumbering = string.Empty;
                                switch (insurerID)
                                {
                                    //島根広域あはきのみSAMデータ用連番必要
                                    case (int)InsurerID.SHIMANE_KOIKI:
                                        if (s.AppType == APP_TYPE.あんま || s.AppType == APP_TYPE.鍼灸)
                                        {
                                            //RA201908+連番22桁123457890123456789012                                        
                                            strNumbering = "RA" + s.CYM + numberingCount++.ToString().PadLeft(22, '0');
                                        }
                                        break;


                                    //case (int)InsurerID.TOYOHASHI_KOKUHO:
                                        //    //豊橋市国保の場合
                                        //    //01234512345  (gid＋連番)
                                        //    //最大に+1

                                        //    strNumbering = gid.ToString("000000") + (maxNumbering++).ToString().PadLeft(5, '0');
                                        //    break;
                                }


                                //20210603194934 furukawa st ////////////////////////
                                //画像登録時にauxに元ファイル名を入れるため引数追加
                                
                                App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), strNumbering, pageno,strOrgFileName);

                                //      20201113160908 furukawa st ////////////////////////
                                //      申請書区別用ページ番号カウント追加                                
                                //      App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), strNumbering,pageno);
                                //              App.ImageEntryWithNumbering(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), strNumbering);
                                //      20201113160908 furukawa ed ////////////////////////
                                //20210603194934 furukawa ed ////////////////////////


                            }
                            //20191010183544 furukawa ed ////////////////////////
                            #endregion


                            else
                            {

                                //20210603194957 furukawa st ////////////////////////
                                //画像登録時にauxに元ファイル名を入れるため引数追加
                                
                                App.ImageEntry(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), tran, pageno,strOrgFileName);

                                //      20201117165402 furukawa st ////////////////////////
                                //      引数に申請書区別用ページ番号カウントを追加

                                //      App.ImageEntry(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), tran,pageno);
                                //                App.ImageEntry(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), tran);
                                //      20201117165402 furukawa ed ////////////////////////
                                //20210603194957 furukawa ed ////////////////////////
                            }



                            //オリジナルファイル名はapplication_auxに持たせるが完全移行まで待つ

                            //20191128164032 furukawa st ////////////////////////
                            //フラグが立っている保険者はオリジナルファイル名をemptytext3に更新

                            if (keepOrgFileName)
                            {
                                using (var cmd = DB.Main.CreateCmd($"update application set emptytext3='{strOrgFileName}' where aid={aid}", tran))
                                {
                                    cmd.TryExecuteNonQuery();
                                }
                            }
                            //20191128164032 furukawa ed ////////////////////////



                            //20201113131928 furukawa st ////////////////////////
                            //申請書区別用ページ番号カウント                            
                            pageno++;
                            //20201113131928 furukawa ed ////////////////////////



                        }
                        catch (Exception ex)
                        {
                            Log.ErrorWriteWithMsg("画像ファイルのデータベース登録に失敗しました\r\n" + ex);
                            return false;
                        }


                        /*
                        
                        try
                        {
                            App.ImageEntry(aid, s.SID, gid, cYear, cMonth, System.IO.Path.GetFileName(newfn), tran);
                        }
                        catch (Exception ex)
                        {
                            Log.ErrorWriteWithMsg("画像ファイルのデータベース登録に失敗しました\r\n" + ex);
                            return false;
                        }*/


                        //20190414190734 furukawa ed ////////////////////////

                    }

                    wf.InvokeValue++;   //何ページあるのか分からないので元ファイル単位で増加
                }


                //20191023134354 furukawa st ////////////////////////
                //カウンタ更新
                
                if (NeedNumbering) UpdCounter(insurerID, numberingCount, s.AppType);
                //20191023134354 furukawa ed ////////////////////////

                s.Status = SCAN_STATUS.SCANED;
                s.Update(tran);
                tran.Commit();
            }

            return true;
        }

        /// <summary>
        /// scanテーブルにスキャンIDを登録
        /// </summary>
        /// <returns></returns>
        protected bool Insert(DB.Transaction tran)
        {
            using(var cmd =DB.Main.CreateCmd("INSERT INTO scan " +
                "(scandate, cyear, cmonth, note1, note2, status, apptype, cym)" +
                "VALUES(:sdate, :cy, :cm, :n1, :n2, :st, :at, :cym) "+
                "RETURNING sid", tran))
            {
                cmd.Parameters.Add("sdate", NpgsqlDbType.Date).Value =ScanDate;
                cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = Cyear;
                cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = Cmonth;
                cmd.Parameters.Add("n1", NpgsqlDbType.Text).Value = Note1;
                cmd.Parameters.Add("n2", NpgsqlDbType.Text).Value = Note2;
                cmd.Parameters.Add("st", NpgsqlDbType.Integer).Value = (int)Status;
                cmd.Parameters.Add("at", NpgsqlDbType.Integer).Value = (int)AppType;
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = CYM;
                
                try
                {
                    var res = cmd.TryExecuteScalar();
                    if (res == DBNull.Value) return false;
                    this.SID = (int)res;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// Scanレコードの更新
        /// </summary>
        /// <returns></returns>
        public bool Update(DB.Transaction tran)
        {
            using (var cmd = DB.Main.CreateCmd("UPDATE scan SET " +
                "scandate=:sdate, cyear=:cy, cmonth=:cm, " +
                "note1=:n1, note2=:n2, status=:st, apptype=:at, cym=:cym " +
                "WHERE sid=:sid", tran))
            {
                cmd.Parameters.Add("sdate", NpgsqlDbType.Date).Value = ScanDate;
                cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = Cyear;
                cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = Cmonth;
                cmd.Parameters.Add("n1", NpgsqlDbType.Text).Value = Note1;
                cmd.Parameters.Add("n2", NpgsqlDbType.Text).Value = Note2;
                cmd.Parameters.Add("st", NpgsqlDbType.Integer).Value = (int)Status;
                cmd.Parameters.Add("at", NpgsqlDbType.Integer).Value = (int)AppType;
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = CYM;
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = SID;

                try
                {
                    return cmd.TryExecuteNonQuery();
                }
                catch(Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }
            }
        }

        /// <summary>
        /// Sidで単一のScanを選択
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static Scan Select(int sid)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "sid, scandate, cyear, cmonth, note1, note2, status, apptype, cym " +
                "FROM scan WHERE sid =:sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = sid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                var s = new Scan();
                var item = res[0];
                s.SID = (int)item[0];
                s.ScanDate = (DateTime)item[1];
                s.Cyear = (int)item[2];
                s.Cmonth = (int)item[3];
                s.Note1 = (string)item[4];
                s.Note2 = (string)item[5];
                s.Status = (SCAN_STATUS)(int)item[6];
                s.AppType = (APP_TYPE)(int)item[7];
                s.CYM = (int)item[8];
                return s;
            }
        }

        /// <summary>
        /// StatusでScanを抽出
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<Scan> GetScanList(SCAN_STATUS status)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "sid, scandate, cyear, cmonth, note1, note2, status, apptype, cym " +
                "FROM scan WHERE status=:st"))
            {
                cmd.Parameters.Add("st", NpgsqlDbType.Integer).Value = (int)status;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<Scan>();

                foreach (var item in res)
                {
                    var s = new Scan();

                    s.SID = (int)item[0];
                    s.ScanDate = (DateTime)item[1];
                    s.Cyear = (int)item[2];
                    s.Cmonth = (int)item[3];
                    s.Note1 = (string)item[4];
                    s.Note2 = (string)item[5];
                    s.Status = (SCAN_STATUS)(int)item[6];
                    s.AppType = (APP_TYPE)(int)item[7];
                    s.CYM = (int)item[8];
                    l.Add(s);
                }

                return l;
            }
        }

        /// <summary>
        /// 処理年月でScanを抽出
        /// </summary>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <returns></returns>
        public static List<Scan> GetScanListYM(int cym)
        {
            using (var cmd = DB.Main.CreateCmd("SELECT " +
                "sid, scandate, cyear, cmonth, note1, note2, status, apptype, cym " +
                "FROM scan WHERE cym=:cym ORDER BY note1, sid"))
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<Scan>();
                foreach (var item in res)
                {
                    var s = new Scan();

                    s.SID = (int)item[0];
                    s.ScanDate = (DateTime)item[1];
                    s.Cyear = (int)item[2];
                    s.Cmonth = (int)item[3];
                    s.Note1 = (string)item[4];
                    s.Note2 = (string)item[5];
                    s.Status = (SCAN_STATUS)(int)item[6];
                    s.AppType = (APP_TYPE)(int)item[7];
                    s.CYM = (int)item[8];
                    l.Add(s);
                }

                return l;
            }
        }

        //20220704150824 furukawa st ////////////////////////
        //複数のScanIDでScanを抽出
        
        /// <summary>
        /// 複数のScanIDでScanを抽出
        /// </summary>
        /// <returns></returns>
        public static List<Scan> GetScanListByScanIDList(List<int> lstScanID)
        {

            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.AppendLine($" select ");
            sb.AppendLine($" sid, scandate, cyear, cmonth, note1, note2, status, apptype, cym ");
            sb.AppendLine($" FROM scan WHERE ");
            sb.AppendLine($" sid in ( ");
            foreach(int scanid in lstScanID)
            {
                sb.AppendLine($" {scanid}, ");
            }
            sb.Remove(sb.Length - 1, 1);
            sb.AppendLine($" ORDER BY note1, sid );");
            
            using (var cmd = DB.Main.CreateCmd(sb.ToString()))
            {               

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                var l = new List<Scan>();
                foreach (var item in res)
                {
                    var s = new Scan();

                    s.SID = (int)item[0];
                    s.ScanDate = (DateTime)item[1];
                    s.Cyear = (int)item[2];
                    s.Cmonth = (int)item[3];
                    s.Note1 = (string)item[4];
                    s.Note2 = (string)item[5];
                    s.Status = (SCAN_STATUS)(int)item[6];
                    s.AppType = (APP_TYPE)(int)item[7];
                    s.CYM = (int)item[8];
                    l.Add(s);
                }

                return l;
            }
        }
        //20220704150824 furukawa ed ////////////////////////



        /// <summary>
        /// ScanId単位で削除
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static bool DeleteBySID(int sid)
        {
            using (var cmd = DB.Main.CreateCmd("DELETE FROM scan " +
                "WHERE sid=:sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = sid;
                var res = cmd.TryExecuteReaderList();

                return cmd.TryExecuteNonQuery();
            }
        }

    }
}
