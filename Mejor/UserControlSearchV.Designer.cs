﻿namespace Mejor
{
    partial class UserControlSearchV
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSyokai = new System.Windows.Forms.GroupBox();
            this.checkBoxSyokai2 = new System.Windows.Forms.CheckBox();
            this.checkBoxSyokai1 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxSaishinsa = new System.Windows.Forms.GroupBox();
            this.checkBoxGigi2 = new System.Windows.Forms.CheckBox();
            this.checkBoxGigi1 = new System.Windows.Forms.CheckBox();
            this.groupBoxKago = new System.Windows.Forms.GroupBox();
            this.checkBoxKago2 = new System.Windows.Forms.CheckBox();
            this.checkBoxKago1 = new System.Windows.Forms.CheckBox();
            this.textBoxTotalMin = new System.Windows.Forms.TextBox();
            this.groupBoxVisit = new System.Windows.Forms.GroupBox();
            this.checkBoxVisit2 = new System.Windows.Forms.CheckBox();
            this.checkBoxVisit1 = new System.Windows.Forms.CheckBox();
            this.groupBoxShinki = new System.Windows.Forms.GroupBox();
            this.checkBoxNew2 = new System.Windows.Forms.CheckBox();
            this.checkBoxNew1 = new System.Windows.Forms.CheckBox();
            this.textBoxTotalMax = new System.Windows.Forms.TextBox();
            this.textBoxDays = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.checkBoxMassage = new System.Windows.Forms.CheckBox();
            this.checkBoxHari = new System.Windows.Forms.CheckBox();
            this.checkBoxJyu = new System.Windows.Forms.CheckBox();
            this.textBoxClinicNum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxBui = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNum = new System.Windows.Forms.TextBox();
            this.textBoxKako = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxLastM = new System.Windows.Forms.GroupBox();
            this.checkBoxLast2 = new System.Windows.Forms.CheckBox();
            this.checkBoxLast1 = new System.Windows.Forms.CheckBox();
            this.groupBoxOryoGigi = new System.Windows.Forms.GroupBox();
            this.checkBoxOryoGigi2 = new System.Windows.Forms.CheckBox();
            this.checkBoxOryoGigi1 = new System.Windows.Forms.CheckBox();
            this.groupBoxFusho = new System.Windows.Forms.GroupBox();
            this.checkBoxHiza = new System.Windows.Forms.CheckBox();
            this.checkBoxKubi = new System.Windows.Forms.CheckBox();
            this.checkBoxSe = new System.Windows.Forms.CheckBox();
            this.checkBoxKoshi = new System.Windows.Forms.CheckBox();
            this.checkBoxKata = new System.Windows.Forms.CheckBox();
            this.textBoxCont = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxFusho = new System.Windows.Forms.CheckBox();
            this.groupBoxAge = new System.Windows.Forms.GroupBox();
            this.checkBoxMinor = new System.Windows.Forms.CheckBox();
            this.checkBoxAdult = new System.Windows.Forms.CheckBox();
            this.textBoxFirstYM = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxMediYM = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBoxBank = new System.Windows.Forms.GroupBox();
            this.checkBoxBank2 = new System.Windows.Forms.CheckBox();
            this.checkBoxBank1 = new System.Windows.Forms.CheckBox();
            this.groupBoxOryoSai = new System.Windows.Forms.GroupBox();
            this.checkBoxOryoSai2 = new System.Windows.Forms.CheckBox();
            this.checkBoxOryoSai1 = new System.Windows.Forms.CheckBox();
            this.groupBoxTenken = new System.Windows.Forms.GroupBox();
            this.checkBoxTenken2 = new System.Windows.Forms.CheckBox();
            this.checkBoxTenken1 = new System.Windows.Forms.CheckBox();
            this.groupBoxOryoTenken = new System.Windows.Forms.GroupBox();
            this.checkBoxOryoTenken2 = new System.Windows.Forms.CheckBox();
            this.checkBoxOryoTenken1 = new System.Windows.Forms.CheckBox();
            this.groupBoxHenrei = new System.Windows.Forms.GroupBox();
            this.checkBoxHenrei2 = new System.Windows.Forms.CheckBox();
            this.checkBoxHenrei1 = new System.Windows.Forms.CheckBox();
            this.groupBoxPend = new System.Windows.Forms.GroupBox();
            this.checkBoxPend2 = new System.Windows.Forms.CheckBox();
            this.checkBoxPend1 = new System.Windows.Forms.CheckBox();
            this.groupBoxLastYm = new System.Windows.Forms.GroupBox();
            this.checkBoxLastYm2 = new System.Windows.Forms.CheckBox();
            this.checkBoxLastYm1 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxDrNum = new System.Windows.Forms.TextBox();
            this.groupBoxInputError = new System.Windows.Forms.GroupBox();
            this.checkBoxInputError2 = new System.Windows.Forms.CheckBox();
            this.checkBoxInputError1 = new System.Windows.Forms.CheckBox();
            this.groupBoxMemo = new System.Windows.Forms.GroupBox();
            this.checkBoxMemo2 = new System.Windows.Forms.CheckBox();
            this.checkBoxMemo1 = new System.Windows.Forms.CheckBox();
            this.groupBoxTel = new System.Windows.Forms.GroupBox();
            this.checkBoxTel2 = new System.Windows.Forms.CheckBox();
            this.checkBoxTel1 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxNumbering = new System.Windows.Forms.TextBox();
            this.groupBoxShiharai = new System.Windows.Forms.GroupBox();
            this.checkBoxShiharai2 = new System.Windows.Forms.CheckBox();
            this.checkBoxShiharai1 = new System.Windows.Forms.CheckBox();
            this.groupBoxProcess1 = new System.Windows.Forms.GroupBox();
            this.checkBoxProcess12 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess11 = new System.Windows.Forms.CheckBox();
            this.groupBoxProcess2 = new System.Windows.Forms.GroupBox();
            this.checkBoxProcess22 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess21 = new System.Windows.Forms.CheckBox();
            this.groupBoxBlack = new System.Windows.Forms.GroupBox();
            this.checkBoxBlack2 = new System.Windows.Forms.CheckBox();
            this.checkBoxBlack1 = new System.Windows.Forms.CheckBox();
            this.checkBoxOutside = new System.Windows.Forms.CheckBox();
            this.groupBoxShokaiHen = new System.Windows.Forms.GroupBox();
            this.checkBoxShokaiHen2 = new System.Windows.Forms.CheckBox();
            this.checkBoxShokaiHen1 = new System.Windows.Forms.CheckBox();
            this.labelBatch = new System.Windows.Forms.Label();
            this.textBoxBatch = new System.Windows.Forms.TextBox();
            this.labelBankAccount = new System.Windows.Forms.Label();
            this.textBoxBankAccount = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxCymTo = new System.Windows.Forms.TextBox();
            this.textBoxCymFrom = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxProcess32 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess31 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxProcess42 = new System.Windows.Forms.CheckBox();
            this.checkBoxProcess41 = new System.Windows.Forms.CheckBox();
            this.chkInputCount = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxAID = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBoxOutMemo = new System.Windows.Forms.GroupBox();
            this.checkBoxOutMemo2 = new System.Windows.Forms.CheckBox();
            this.checkBoxOutMemo1 = new System.Windows.Forms.CheckBox();
            this.groupBoxVisitAdd = new System.Windows.Forms.GroupBox();
            this.checkBoxVisitAdd2 = new System.Windows.Forms.CheckBox();
            this.checkBoxVisitAdd1 = new System.Windows.Forms.CheckBox();
            this.groupBoxSyokai.SuspendLayout();
            this.groupBoxSaishinsa.SuspendLayout();
            this.groupBoxKago.SuspendLayout();
            this.groupBoxVisit.SuspendLayout();
            this.groupBoxShinki.SuspendLayout();
            this.groupBoxLastM.SuspendLayout();
            this.groupBoxOryoGigi.SuspendLayout();
            this.groupBoxFusho.SuspendLayout();
            this.groupBoxAge.SuspendLayout();
            this.groupBoxBank.SuspendLayout();
            this.groupBoxOryoSai.SuspendLayout();
            this.groupBoxTenken.SuspendLayout();
            this.groupBoxOryoTenken.SuspendLayout();
            this.groupBoxHenrei.SuspendLayout();
            this.groupBoxPend.SuspendLayout();
            this.groupBoxLastYm.SuspendLayout();
            this.groupBoxInputError.SuspendLayout();
            this.groupBoxMemo.SuspendLayout();
            this.groupBoxTel.SuspendLayout();
            this.groupBoxShiharai.SuspendLayout();
            this.groupBoxProcess1.SuspendLayout();
            this.groupBoxProcess2.SuspendLayout();
            this.groupBoxBlack.SuspendLayout();
            this.groupBoxShokaiHen.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxOutMemo.SuspendLayout();
            this.groupBoxVisitAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSyokai
            // 
            this.groupBoxSyokai.Controls.Add(this.checkBoxSyokai2);
            this.groupBoxSyokai.Controls.Add(this.checkBoxSyokai1);
            this.groupBoxSyokai.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxSyokai.Location = new System.Drawing.Point(111, 482);
            this.groupBoxSyokai.Name = "groupBoxSyokai";
            this.groupBoxSyokai.Size = new System.Drawing.Size(102, 38);
            this.groupBoxSyokai.TabIndex = 58;
            this.groupBoxSyokai.TabStop = false;
            this.groupBoxSyokai.Text = "照会対象";
            // 
            // checkBoxSyokai2
            // 
            this.checkBoxSyokai2.AutoSize = true;
            this.checkBoxSyokai2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxSyokai2.Name = "checkBoxSyokai2";
            this.checkBoxSyokai2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxSyokai2.TabIndex = 1;
            this.checkBoxSyokai2.Text = "外";
            this.checkBoxSyokai2.UseVisualStyleBackColor = true;
            // 
            // checkBoxSyokai1
            // 
            this.checkBoxSyokai1.AutoSize = true;
            this.checkBoxSyokai1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxSyokai1.Name = "checkBoxSyokai1";
            this.checkBoxSyokai1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxSyokai1.TabIndex = 0;
            this.checkBoxSyokai1.Text = "対象";
            this.checkBoxSyokai1.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(112, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 11);
            this.label6.TabIndex = 22;
            this.label6.Text = "～";
            // 
            // groupBoxSaishinsa
            // 
            this.groupBoxSaishinsa.Controls.Add(this.checkBoxGigi2);
            this.groupBoxSaishinsa.Controls.Add(this.checkBoxGigi1);
            this.groupBoxSaishinsa.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxSaishinsa.Location = new System.Drawing.Point(111, 441);
            this.groupBoxSaishinsa.Name = "groupBoxSaishinsa";
            this.groupBoxSaishinsa.Size = new System.Drawing.Size(102, 38);
            this.groupBoxSaishinsa.TabIndex = 57;
            this.groupBoxSaishinsa.TabStop = false;
            this.groupBoxSaishinsa.Text = "再審査";
            // 
            // checkBoxGigi2
            // 
            this.checkBoxGigi2.AutoSize = true;
            this.checkBoxGigi2.Location = new System.Drawing.Point(55, 17);
            this.checkBoxGigi2.Name = "checkBoxGigi2";
            this.checkBoxGigi2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxGigi2.TabIndex = 1;
            this.checkBoxGigi2.Text = "なし";
            this.checkBoxGigi2.UseVisualStyleBackColor = true;
            // 
            // checkBoxGigi1
            // 
            this.checkBoxGigi1.AutoSize = true;
            this.checkBoxGigi1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxGigi1.Name = "checkBoxGigi1";
            this.checkBoxGigi1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxGigi1.TabIndex = 0;
            this.checkBoxGigi1.Text = "あり";
            this.checkBoxGigi1.UseVisualStyleBackColor = true;
            // 
            // groupBoxKago
            // 
            this.groupBoxKago.Controls.Add(this.checkBoxKago2);
            this.groupBoxKago.Controls.Add(this.checkBoxKago1);
            this.groupBoxKago.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxKago.Location = new System.Drawing.Point(111, 401);
            this.groupBoxKago.Name = "groupBoxKago";
            this.groupBoxKago.Size = new System.Drawing.Size(102, 38);
            this.groupBoxKago.TabIndex = 56;
            this.groupBoxKago.TabStop = false;
            this.groupBoxKago.Text = "過誤";
            // 
            // checkBoxKago2
            // 
            this.checkBoxKago2.AutoSize = true;
            this.checkBoxKago2.Location = new System.Drawing.Point(55, 17);
            this.checkBoxKago2.Name = "checkBoxKago2";
            this.checkBoxKago2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxKago2.TabIndex = 1;
            this.checkBoxKago2.Text = "なし";
            this.checkBoxKago2.UseVisualStyleBackColor = true;
            // 
            // checkBoxKago1
            // 
            this.checkBoxKago1.AutoSize = true;
            this.checkBoxKago1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxKago1.Name = "checkBoxKago1";
            this.checkBoxKago1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxKago1.TabIndex = 0;
            this.checkBoxKago1.Text = "あり";
            this.checkBoxKago1.UseVisualStyleBackColor = true;
            // 
            // textBoxTotalMin
            // 
            this.textBoxTotalMin.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxTotalMin.Location = new System.Drawing.Point(63, 169);
            this.textBoxTotalMin.MaxLength = 8;
            this.textBoxTotalMin.Name = "textBoxTotalMin";
            this.textBoxTotalMin.Size = new System.Drawing.Size(49, 20);
            this.textBoxTotalMin.TabIndex = 21;
            // 
            // groupBoxVisit
            // 
            this.groupBoxVisit.Controls.Add(this.checkBoxVisit2);
            this.groupBoxVisit.Controls.Add(this.checkBoxVisit1);
            this.groupBoxVisit.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxVisit.Location = new System.Drawing.Point(5, 482);
            this.groupBoxVisit.Name = "groupBoxVisit";
            this.groupBoxVisit.Size = new System.Drawing.Size(102, 38);
            this.groupBoxVisit.TabIndex = 48;
            this.groupBoxVisit.TabStop = false;
            this.groupBoxVisit.Text = "往療料";
            // 
            // checkBoxVisit2
            // 
            this.checkBoxVisit2.AutoSize = true;
            this.checkBoxVisit2.Location = new System.Drawing.Point(53, 17);
            this.checkBoxVisit2.Name = "checkBoxVisit2";
            this.checkBoxVisit2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxVisit2.TabIndex = 1;
            this.checkBoxVisit2.Text = "なし";
            this.checkBoxVisit2.UseVisualStyleBackColor = true;
            // 
            // checkBoxVisit1
            // 
            this.checkBoxVisit1.AutoSize = true;
            this.checkBoxVisit1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxVisit1.Name = "checkBoxVisit1";
            this.checkBoxVisit1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxVisit1.TabIndex = 0;
            this.checkBoxVisit1.Text = "あり";
            this.checkBoxVisit1.UseVisualStyleBackColor = true;
            // 
            // groupBoxShinki
            // 
            this.groupBoxShinki.Controls.Add(this.checkBoxNew2);
            this.groupBoxShinki.Controls.Add(this.checkBoxNew1);
            this.groupBoxShinki.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxShinki.Location = new System.Drawing.Point(5, 401);
            this.groupBoxShinki.Name = "groupBoxShinki";
            this.groupBoxShinki.Size = new System.Drawing.Size(102, 38);
            this.groupBoxShinki.TabIndex = 46;
            this.groupBoxShinki.TabStop = false;
            this.groupBoxShinki.Text = "新規/継続";
            // 
            // checkBoxNew2
            // 
            this.checkBoxNew2.AutoSize = true;
            this.checkBoxNew2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.checkBoxNew2.Location = new System.Drawing.Point(53, 17);
            this.checkBoxNew2.Name = "checkBoxNew2";
            this.checkBoxNew2.Size = new System.Drawing.Size(48, 16);
            this.checkBoxNew2.TabIndex = 1;
            this.checkBoxNew2.Text = "継続";
            this.checkBoxNew2.UseVisualStyleBackColor = true;
            // 
            // checkBoxNew1
            // 
            this.checkBoxNew1.AutoSize = true;
            this.checkBoxNew1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.checkBoxNew1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxNew1.Name = "checkBoxNew1";
            this.checkBoxNew1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxNew1.TabIndex = 0;
            this.checkBoxNew1.Text = "新規";
            this.checkBoxNew1.UseVisualStyleBackColor = true;
            // 
            // textBoxTotalMax
            // 
            this.textBoxTotalMax.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxTotalMax.Location = new System.Drawing.Point(128, 168);
            this.textBoxTotalMax.MaxLength = 8;
            this.textBoxTotalMax.Name = "textBoxTotalMax";
            this.textBoxTotalMax.Size = new System.Drawing.Size(49, 20);
            this.textBoxTotalMax.TabIndex = 23;
            // 
            // textBoxDays
            // 
            this.textBoxDays.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxDays.Location = new System.Drawing.Point(63, 98);
            this.textBoxDays.MaxLength = 2;
            this.textBoxDays.Name = "textBoxDays";
            this.textBoxDays.Size = new System.Drawing.Size(35, 20);
            this.textBoxDays.TabIndex = 14;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "日数";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(99, 102);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "日以上";
            // 
            // checkBoxMassage
            // 
            this.checkBoxMassage.AutoSize = true;
            this.checkBoxMassage.Checked = true;
            this.checkBoxMassage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxMassage.Location = new System.Drawing.Point(117, 2);
            this.checkBoxMassage.Name = "checkBoxMassage";
            this.checkBoxMassage.Size = new System.Drawing.Size(55, 17);
            this.checkBoxMassage.TabIndex = 2;
            this.checkBoxMassage.Text = "あんま";
            this.checkBoxMassage.UseVisualStyleBackColor = true;
            // 
            // checkBoxHari
            // 
            this.checkBoxHari.AutoSize = true;
            this.checkBoxHari.Checked = true;
            this.checkBoxHari.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHari.Location = new System.Drawing.Point(61, 2);
            this.checkBoxHari.Name = "checkBoxHari";
            this.checkBoxHari.Size = new System.Drawing.Size(56, 17);
            this.checkBoxHari.TabIndex = 1;
            this.checkBoxHari.Text = "はり灸";
            this.checkBoxHari.UseVisualStyleBackColor = true;
            // 
            // checkBoxJyu
            // 
            this.checkBoxJyu.AutoSize = true;
            this.checkBoxJyu.Checked = true;
            this.checkBoxJyu.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxJyu.Location = new System.Drawing.Point(11, 2);
            this.checkBoxJyu.Name = "checkBoxJyu";
            this.checkBoxJyu.Size = new System.Drawing.Size(50, 17);
            this.checkBoxJyu.TabIndex = 0;
            this.checkBoxJyu.Text = "柔整";
            this.checkBoxJyu.UseVisualStyleBackColor = true;
            // 
            // textBoxClinicNum
            // 
            this.textBoxClinicNum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxClinicNum.Location = new System.Drawing.Point(63, 121);
            this.textBoxClinicNum.Name = "textBoxClinicNum";
            this.textBoxClinicNum.Size = new System.Drawing.Size(114, 20);
            this.textBoxClinicNum.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(3, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "施術所番";
            // 
            // textBoxBui
            // 
            this.textBoxBui.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxBui.Location = new System.Drawing.Point(63, 74);
            this.textBoxBui.MaxLength = 2;
            this.textBoxBui.Name = "textBoxBui";
            this.textBoxBui.Size = new System.Drawing.Size(25, 20);
            this.textBoxBui.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "部位数";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 173);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "合計金額";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "被保番";
            // 
            // textBoxNum
            // 
            this.textBoxNum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxNum.Location = new System.Drawing.Point(63, 50);
            this.textBoxNum.Name = "textBoxNum";
            this.textBoxNum.Size = new System.Drawing.Size(114, 20);
            this.textBoxNum.TabIndex = 8;
            // 
            // textBoxKako
            // 
            this.textBoxKako.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxKako.Location = new System.Drawing.Point(63, 193);
            this.textBoxKako.MaxLength = 3;
            this.textBoxKako.Name = "textBoxKako";
            this.textBoxKako.Size = new System.Drawing.Size(35, 20);
            this.textBoxKako.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "過去照会";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "ヶ月";
            // 
            // groupBoxLastM
            // 
            this.groupBoxLastM.Controls.Add(this.checkBoxLast2);
            this.groupBoxLastM.Controls.Add(this.checkBoxLast1);
            this.groupBoxLastM.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxLastM.Location = new System.Drawing.Point(5, 441);
            this.groupBoxLastM.Name = "groupBoxLastM";
            this.groupBoxLastM.Size = new System.Drawing.Size(102, 38);
            this.groupBoxLastM.TabIndex = 47;
            this.groupBoxLastM.TabStop = false;
            this.groupBoxLastM.Text = "先月申請書";
            // 
            // checkBoxLast2
            // 
            this.checkBoxLast2.AutoSize = true;
            this.checkBoxLast2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxLast2.Name = "checkBoxLast2";
            this.checkBoxLast2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxLast2.TabIndex = 1;
            this.checkBoxLast2.Text = "なし";
            this.checkBoxLast2.UseVisualStyleBackColor = true;
            // 
            // checkBoxLast1
            // 
            this.checkBoxLast1.AutoSize = true;
            this.checkBoxLast1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxLast1.Name = "checkBoxLast1";
            this.checkBoxLast1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxLast1.TabIndex = 0;
            this.checkBoxLast1.Text = "あり";
            this.checkBoxLast1.UseVisualStyleBackColor = true;
            // 
            // groupBoxOryoGigi
            // 
            this.groupBoxOryoGigi.Controls.Add(this.checkBoxOryoGigi2);
            this.groupBoxOryoGigi.Controls.Add(this.checkBoxOryoGigi1);
            this.groupBoxOryoGigi.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxOryoGigi.Location = new System.Drawing.Point(5, 522);
            this.groupBoxOryoGigi.Name = "groupBoxOryoGigi";
            this.groupBoxOryoGigi.Size = new System.Drawing.Size(102, 38);
            this.groupBoxOryoGigi.TabIndex = 49;
            this.groupBoxOryoGigi.TabStop = false;
            this.groupBoxOryoGigi.Text = "往療疑義";
            // 
            // checkBoxOryoGigi2
            // 
            this.checkBoxOryoGigi2.AutoSize = true;
            this.checkBoxOryoGigi2.Location = new System.Drawing.Point(54, 42);
            this.checkBoxOryoGigi2.Name = "checkBoxOryoGigi2";
            this.checkBoxOryoGigi2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxOryoGigi2.TabIndex = 1;
            this.checkBoxOryoGigi2.Text = "なし";
            this.checkBoxOryoGigi2.UseVisualStyleBackColor = true;
            // 
            // checkBoxOryoGigi1
            // 
            this.checkBoxOryoGigi1.AutoSize = true;
            this.checkBoxOryoGigi1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxOryoGigi1.Name = "checkBoxOryoGigi1";
            this.checkBoxOryoGigi1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxOryoGigi1.TabIndex = 0;
            this.checkBoxOryoGigi1.Text = "あり";
            this.checkBoxOryoGigi1.UseVisualStyleBackColor = true;
            // 
            // groupBoxFusho
            // 
            this.groupBoxFusho.Controls.Add(this.checkBoxHiza);
            this.groupBoxFusho.Controls.Add(this.checkBoxKubi);
            this.groupBoxFusho.Controls.Add(this.checkBoxSe);
            this.groupBoxFusho.Controls.Add(this.checkBoxKoshi);
            this.groupBoxFusho.Controls.Add(this.checkBoxKata);
            this.groupBoxFusho.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxFusho.Location = new System.Drawing.Point(4, 762);
            this.groupBoxFusho.Name = "groupBoxFusho";
            this.groupBoxFusho.Size = new System.Drawing.Size(209, 40);
            this.groupBoxFusho.TabIndex = 55;
            this.groupBoxFusho.TabStop = false;
            this.groupBoxFusho.Text = "負傷名 (or検索)";
            // 
            // checkBoxHiza
            // 
            this.checkBoxHiza.AutoSize = true;
            this.checkBoxHiza.Location = new System.Drawing.Point(168, 17);
            this.checkBoxHiza.Name = "checkBoxHiza";
            this.checkBoxHiza.Size = new System.Drawing.Size(36, 16);
            this.checkBoxHiza.TabIndex = 4;
            this.checkBoxHiza.Text = "膝";
            this.checkBoxHiza.UseVisualStyleBackColor = true;
            // 
            // checkBoxKubi
            // 
            this.checkBoxKubi.AutoSize = true;
            this.checkBoxKubi.Location = new System.Drawing.Point(8, 17);
            this.checkBoxKubi.Name = "checkBoxKubi";
            this.checkBoxKubi.Size = new System.Drawing.Size(36, 16);
            this.checkBoxKubi.TabIndex = 0;
            this.checkBoxKubi.Text = "頚";
            this.checkBoxKubi.UseVisualStyleBackColor = true;
            // 
            // checkBoxSe
            // 
            this.checkBoxSe.AutoSize = true;
            this.checkBoxSe.Location = new System.Drawing.Point(88, 17);
            this.checkBoxSe.Name = "checkBoxSe";
            this.checkBoxSe.Size = new System.Drawing.Size(36, 16);
            this.checkBoxSe.TabIndex = 2;
            this.checkBoxSe.Text = "背";
            this.checkBoxSe.UseVisualStyleBackColor = true;
            // 
            // checkBoxKoshi
            // 
            this.checkBoxKoshi.AutoSize = true;
            this.checkBoxKoshi.Location = new System.Drawing.Point(128, 17);
            this.checkBoxKoshi.Name = "checkBoxKoshi";
            this.checkBoxKoshi.Size = new System.Drawing.Size(36, 16);
            this.checkBoxKoshi.TabIndex = 3;
            this.checkBoxKoshi.Text = "腰";
            this.checkBoxKoshi.UseVisualStyleBackColor = true;
            // 
            // checkBoxKata
            // 
            this.checkBoxKata.AutoSize = true;
            this.checkBoxKata.Location = new System.Drawing.Point(48, 17);
            this.checkBoxKata.Name = "checkBoxKata";
            this.checkBoxKata.Size = new System.Drawing.Size(36, 16);
            this.checkBoxKata.TabIndex = 1;
            this.checkBoxKata.Text = "肩";
            this.checkBoxKata.UseVisualStyleBackColor = true;
            // 
            // textBoxCont
            // 
            this.textBoxCont.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxCont.Location = new System.Drawing.Point(63, 217);
            this.textBoxCont.MaxLength = 2;
            this.textBoxCont.Name = "textBoxCont";
            this.textBoxCont.Size = new System.Drawing.Size(35, 20);
            this.textBoxCont.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "連続受診";
            // 
            // checkBoxFusho
            // 
            this.checkBoxFusho.AutoSize = true;
            this.checkBoxFusho.Location = new System.Drawing.Point(131, 220);
            this.checkBoxFusho.Name = "checkBoxFusho";
            this.checkBoxFusho.Size = new System.Drawing.Size(50, 17);
            this.checkBoxFusho.TabIndex = 31;
            this.checkBoxFusho.Text = "同負";
            this.checkBoxFusho.UseVisualStyleBackColor = true;
            // 
            // groupBoxAge
            // 
            this.groupBoxAge.Controls.Add(this.checkBoxMinor);
            this.groupBoxAge.Controls.Add(this.checkBoxAdult);
            this.groupBoxAge.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxAge.Location = new System.Drawing.Point(5, 682);
            this.groupBoxAge.Name = "groupBoxAge";
            this.groupBoxAge.Size = new System.Drawing.Size(102, 38);
            this.groupBoxAge.TabIndex = 53;
            this.groupBoxAge.TabStop = false;
            this.groupBoxAge.Text = "年齢";
            // 
            // checkBoxMinor
            // 
            this.checkBoxMinor.AutoSize = true;
            this.checkBoxMinor.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.checkBoxMinor.Location = new System.Drawing.Point(53, 17);
            this.checkBoxMinor.Name = "checkBoxMinor";
            this.checkBoxMinor.Size = new System.Drawing.Size(48, 16);
            this.checkBoxMinor.TabIndex = 1;
            this.checkBoxMinor.Text = "未成";
            this.checkBoxMinor.UseVisualStyleBackColor = true;
            // 
            // checkBoxAdult
            // 
            this.checkBoxAdult.AutoSize = true;
            this.checkBoxAdult.Location = new System.Drawing.Point(8, 17);
            this.checkBoxAdult.Name = "checkBoxAdult";
            this.checkBoxAdult.Size = new System.Drawing.Size(48, 16);
            this.checkBoxAdult.TabIndex = 0;
            this.checkBoxAdult.Text = "成年";
            this.checkBoxAdult.UseVisualStyleBackColor = true;
            // 
            // textBoxFirstYM
            // 
            this.textBoxFirstYM.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxFirstYM.Location = new System.Drawing.Point(63, 241);
            this.textBoxFirstYM.MaxLength = 6;
            this.textBoxFirstYM.Name = "textBoxFirstYM";
            this.textBoxFirstYM.Size = new System.Drawing.Size(49, 20);
            this.textBoxFirstYM.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 245);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "初検年月";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(119, 245);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "以前";
            // 
            // textBoxMediYM
            // 
            this.textBoxMediYM.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxMediYM.Location = new System.Drawing.Point(63, 265);
            this.textBoxMediYM.MaxLength = 6;
            this.textBoxMediYM.Name = "textBoxMediYM";
            this.textBoxMediYM.Size = new System.Drawing.Size(49, 20);
            this.textBoxMediYM.TabIndex = 36;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(3, 269);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 12);
            this.label14.TabIndex = 35;
            this.label14.Text = "施術年月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(119, 269);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 37;
            this.label18.Text = "以降";
            // 
            // groupBoxBank
            // 
            this.groupBoxBank.Controls.Add(this.checkBoxBank2);
            this.groupBoxBank.Controls.Add(this.checkBoxBank1);
            this.groupBoxBank.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxBank.Location = new System.Drawing.Point(218, 764);
            this.groupBoxBank.Name = "groupBoxBank";
            this.groupBoxBank.Size = new System.Drawing.Size(102, 38);
            this.groupBoxBank.TabIndex = 73;
            this.groupBoxBank.TabStop = false;
            this.groupBoxBank.Text = "特定口座(学校)";
            // 
            // checkBoxBank2
            // 
            this.checkBoxBank2.AutoSize = true;
            this.checkBoxBank2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxBank2.Name = "checkBoxBank2";
            this.checkBoxBank2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxBank2.TabIndex = 0;
            this.checkBoxBank2.Text = "外";
            this.checkBoxBank2.UseVisualStyleBackColor = true;
            // 
            // checkBoxBank1
            // 
            this.checkBoxBank1.AutoSize = true;
            this.checkBoxBank1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxBank1.Name = "checkBoxBank1";
            this.checkBoxBank1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxBank1.TabIndex = 0;
            this.checkBoxBank1.Text = "対象";
            this.checkBoxBank1.UseVisualStyleBackColor = true;
            // 
            // groupBoxOryoSai
            // 
            this.groupBoxOryoSai.Controls.Add(this.checkBoxOryoSai2);
            this.groupBoxOryoSai.Controls.Add(this.checkBoxOryoSai1);
            this.groupBoxOryoSai.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxOryoSai.Location = new System.Drawing.Point(5, 562);
            this.groupBoxOryoSai.Name = "groupBoxOryoSai";
            this.groupBoxOryoSai.Size = new System.Drawing.Size(102, 38);
            this.groupBoxOryoSai.TabIndex = 50;
            this.groupBoxOryoSai.TabStop = false;
            this.groupBoxOryoSai.Text = "往療計算差異";
            // 
            // checkBoxOryoSai2
            // 
            this.checkBoxOryoSai2.AutoSize = true;
            this.checkBoxOryoSai2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxOryoSai2.Name = "checkBoxOryoSai2";
            this.checkBoxOryoSai2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxOryoSai2.TabIndex = 1;
            this.checkBoxOryoSai2.Text = "なし";
            this.checkBoxOryoSai2.UseVisualStyleBackColor = true;
            // 
            // checkBoxOryoSai1
            // 
            this.checkBoxOryoSai1.AutoSize = true;
            this.checkBoxOryoSai1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxOryoSai1.Name = "checkBoxOryoSai1";
            this.checkBoxOryoSai1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxOryoSai1.TabIndex = 0;
            this.checkBoxOryoSai1.Text = "あり";
            this.checkBoxOryoSai1.UseVisualStyleBackColor = true;
            // 
            // groupBoxTenken
            // 
            this.groupBoxTenken.Controls.Add(this.checkBoxTenken2);
            this.groupBoxTenken.Controls.Add(this.checkBoxTenken1);
            this.groupBoxTenken.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxTenken.Location = new System.Drawing.Point(111, 522);
            this.groupBoxTenken.Name = "groupBoxTenken";
            this.groupBoxTenken.Size = new System.Drawing.Size(102, 38);
            this.groupBoxTenken.TabIndex = 59;
            this.groupBoxTenken.TabStop = false;
            this.groupBoxTenken.Text = "点検対象";
            // 
            // checkBoxTenken2
            // 
            this.checkBoxTenken2.AutoSize = true;
            this.checkBoxTenken2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxTenken2.Name = "checkBoxTenken2";
            this.checkBoxTenken2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxTenken2.TabIndex = 1;
            this.checkBoxTenken2.Text = "外";
            this.checkBoxTenken2.UseVisualStyleBackColor = true;
            // 
            // checkBoxTenken1
            // 
            this.checkBoxTenken1.AutoSize = true;
            this.checkBoxTenken1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxTenken1.Name = "checkBoxTenken1";
            this.checkBoxTenken1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxTenken1.TabIndex = 0;
            this.checkBoxTenken1.Text = "対象";
            this.checkBoxTenken1.UseVisualStyleBackColor = true;
            // 
            // groupBoxOryoTenken
            // 
            this.groupBoxOryoTenken.Controls.Add(this.checkBoxOryoTenken2);
            this.groupBoxOryoTenken.Controls.Add(this.checkBoxOryoTenken1);
            this.groupBoxOryoTenken.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxOryoTenken.Location = new System.Drawing.Point(111, 562);
            this.groupBoxOryoTenken.Name = "groupBoxOryoTenken";
            this.groupBoxOryoTenken.Size = new System.Drawing.Size(102, 38);
            this.groupBoxOryoTenken.TabIndex = 60;
            this.groupBoxOryoTenken.TabStop = false;
            this.groupBoxOryoTenken.Text = "往点対象";
            // 
            // checkBoxOryoTenken2
            // 
            this.checkBoxOryoTenken2.AutoSize = true;
            this.checkBoxOryoTenken2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxOryoTenken2.Name = "checkBoxOryoTenken2";
            this.checkBoxOryoTenken2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxOryoTenken2.TabIndex = 1;
            this.checkBoxOryoTenken2.Text = "外";
            this.checkBoxOryoTenken2.UseVisualStyleBackColor = true;
            // 
            // checkBoxOryoTenken1
            // 
            this.checkBoxOryoTenken1.AutoSize = true;
            this.checkBoxOryoTenken1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxOryoTenken1.Name = "checkBoxOryoTenken1";
            this.checkBoxOryoTenken1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxOryoTenken1.TabIndex = 0;
            this.checkBoxOryoTenken1.Text = "対象";
            this.checkBoxOryoTenken1.UseVisualStyleBackColor = true;
            // 
            // groupBoxHenrei
            // 
            this.groupBoxHenrei.Controls.Add(this.checkBoxHenrei2);
            this.groupBoxHenrei.Controls.Add(this.checkBoxHenrei1);
            this.groupBoxHenrei.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxHenrei.Location = new System.Drawing.Point(111, 602);
            this.groupBoxHenrei.Name = "groupBoxHenrei";
            this.groupBoxHenrei.Size = new System.Drawing.Size(102, 38);
            this.groupBoxHenrei.TabIndex = 61;
            this.groupBoxHenrei.TabStop = false;
            this.groupBoxHenrei.Text = "返戻対象";
            // 
            // checkBoxHenrei2
            // 
            this.checkBoxHenrei2.AutoSize = true;
            this.checkBoxHenrei2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxHenrei2.Name = "checkBoxHenrei2";
            this.checkBoxHenrei2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxHenrei2.TabIndex = 1;
            this.checkBoxHenrei2.Text = "外";
            this.checkBoxHenrei2.UseVisualStyleBackColor = true;
            // 
            // checkBoxHenrei1
            // 
            this.checkBoxHenrei1.AutoSize = true;
            this.checkBoxHenrei1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxHenrei1.Name = "checkBoxHenrei1";
            this.checkBoxHenrei1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxHenrei1.TabIndex = 0;
            this.checkBoxHenrei1.Text = "対象";
            this.checkBoxHenrei1.UseVisualStyleBackColor = true;
            // 
            // groupBoxPend
            // 
            this.groupBoxPend.Controls.Add(this.checkBoxPend2);
            this.groupBoxPend.Controls.Add(this.checkBoxPend1);
            this.groupBoxPend.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxPend.Location = new System.Drawing.Point(111, 642);
            this.groupBoxPend.Name = "groupBoxPend";
            this.groupBoxPend.Size = new System.Drawing.Size(102, 38);
            this.groupBoxPend.TabIndex = 62;
            this.groupBoxPend.TabStop = false;
            this.groupBoxPend.Text = "支払保留";
            // 
            // checkBoxPend2
            // 
            this.checkBoxPend2.AutoSize = true;
            this.checkBoxPend2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxPend2.Name = "checkBoxPend2";
            this.checkBoxPend2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxPend2.TabIndex = 1;
            this.checkBoxPend2.Text = "外";
            this.checkBoxPend2.UseVisualStyleBackColor = true;
            // 
            // checkBoxPend1
            // 
            this.checkBoxPend1.AutoSize = true;
            this.checkBoxPend1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxPend1.Name = "checkBoxPend1";
            this.checkBoxPend1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxPend1.TabIndex = 0;
            this.checkBoxPend1.Text = "保留";
            this.checkBoxPend1.UseVisualStyleBackColor = true;
            // 
            // groupBoxLastYm
            // 
            this.groupBoxLastYm.Controls.Add(this.checkBoxLastYm2);
            this.groupBoxLastYm.Controls.Add(this.checkBoxLastYm1);
            this.groupBoxLastYm.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxLastYm.Location = new System.Drawing.Point(5, 642);
            this.groupBoxLastYm.Name = "groupBoxLastYm";
            this.groupBoxLastYm.Size = new System.Drawing.Size(102, 38);
            this.groupBoxLastYm.TabIndex = 52;
            this.groupBoxLastYm.TabStop = false;
            this.groupBoxLastYm.Text = "最新診療年月";
            // 
            // checkBoxLastYm2
            // 
            this.checkBoxLastYm2.AutoSize = true;
            this.checkBoxLastYm2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxLastYm2.Name = "checkBoxLastYm2";
            this.checkBoxLastYm2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxLastYm2.TabIndex = 1;
            this.checkBoxLastYm2.Text = "外";
            this.checkBoxLastYm2.UseVisualStyleBackColor = true;
            // 
            // checkBoxLastYm1
            // 
            this.checkBoxLastYm1.AutoSize = true;
            this.checkBoxLastYm1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxLastYm1.Name = "checkBoxLastYm1";
            this.checkBoxLastYm1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxLastYm1.TabIndex = 0;
            this.checkBoxLastYm1.Text = "最新";
            this.checkBoxLastYm1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(3, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "施術師番";
            // 
            // textBoxDrNum
            // 
            this.textBoxDrNum.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxDrNum.Location = new System.Drawing.Point(63, 145);
            this.textBoxDrNum.Name = "textBoxDrNum";
            this.textBoxDrNum.Size = new System.Drawing.Size(114, 20);
            this.textBoxDrNum.TabIndex = 19;
            // 
            // groupBoxInputError
            // 
            this.groupBoxInputError.Controls.Add(this.checkBoxInputError2);
            this.groupBoxInputError.Controls.Add(this.checkBoxInputError1);
            this.groupBoxInputError.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxInputError.Location = new System.Drawing.Point(218, 401);
            this.groupBoxInputError.Name = "groupBoxInputError";
            this.groupBoxInputError.Size = new System.Drawing.Size(102, 38);
            this.groupBoxInputError.TabIndex = 64;
            this.groupBoxInputError.TabStop = false;
            this.groupBoxInputError.Text = "入力時エラー";
            // 
            // checkBoxInputError2
            // 
            this.checkBoxInputError2.AutoSize = true;
            this.checkBoxInputError2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxInputError2.Name = "checkBoxInputError2";
            this.checkBoxInputError2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxInputError2.TabIndex = 1;
            this.checkBoxInputError2.Text = "なし";
            this.checkBoxInputError2.UseVisualStyleBackColor = true;
            // 
            // checkBoxInputError1
            // 
            this.checkBoxInputError1.AutoSize = true;
            this.checkBoxInputError1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxInputError1.Name = "checkBoxInputError1";
            this.checkBoxInputError1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxInputError1.TabIndex = 0;
            this.checkBoxInputError1.Text = "あり";
            this.checkBoxInputError1.UseVisualStyleBackColor = true;
            // 
            // groupBoxMemo
            // 
            this.groupBoxMemo.Controls.Add(this.checkBoxMemo2);
            this.groupBoxMemo.Controls.Add(this.checkBoxMemo1);
            this.groupBoxMemo.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxMemo.Location = new System.Drawing.Point(218, 682);
            this.groupBoxMemo.Name = "groupBoxMemo";
            this.groupBoxMemo.Size = new System.Drawing.Size(102, 38);
            this.groupBoxMemo.TabIndex = 71;
            this.groupBoxMemo.TabStop = false;
            this.groupBoxMemo.Text = "メモ";
            // 
            // checkBoxMemo2
            // 
            this.checkBoxMemo2.AutoSize = true;
            this.checkBoxMemo2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxMemo2.Name = "checkBoxMemo2";
            this.checkBoxMemo2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxMemo2.TabIndex = 1;
            this.checkBoxMemo2.Text = "なし";
            this.checkBoxMemo2.UseVisualStyleBackColor = true;
            // 
            // checkBoxMemo1
            // 
            this.checkBoxMemo1.AutoSize = true;
            this.checkBoxMemo1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxMemo1.Name = "checkBoxMemo1";
            this.checkBoxMemo1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxMemo1.TabIndex = 0;
            this.checkBoxMemo1.Text = "あり";
            this.checkBoxMemo1.UseVisualStyleBackColor = true;
            // 
            // groupBoxTel
            // 
            this.groupBoxTel.Controls.Add(this.checkBoxTel2);
            this.groupBoxTel.Controls.Add(this.checkBoxTel1);
            this.groupBoxTel.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxTel.Location = new System.Drawing.Point(218, 441);
            this.groupBoxTel.Name = "groupBoxTel";
            this.groupBoxTel.Size = new System.Drawing.Size(102, 38);
            this.groupBoxTel.TabIndex = 65;
            this.groupBoxTel.TabStop = false;
            this.groupBoxTel.Text = "架電対象";
            // 
            // checkBoxTel2
            // 
            this.checkBoxTel2.AutoSize = true;
            this.checkBoxTel2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxTel2.Name = "checkBoxTel2";
            this.checkBoxTel2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxTel2.TabIndex = 1;
            this.checkBoxTel2.Text = "外";
            this.checkBoxTel2.UseVisualStyleBackColor = true;
            // 
            // checkBoxTel1
            // 
            this.checkBoxTel1.AutoSize = true;
            this.checkBoxTel1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxTel1.Name = "checkBoxTel1";
            this.checkBoxTel1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxTel1.TabIndex = 0;
            this.checkBoxTel1.Text = "対象";
            this.checkBoxTel1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(99, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "ヶ月";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label19.Location = new System.Drawing.Point(3, 294);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 12);
            this.label19.TabIndex = 38;
            this.label19.Text = "ナンバリング";
            // 
            // textBoxNumbering
            // 
            this.textBoxNumbering.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxNumbering.Location = new System.Drawing.Point(63, 290);
            this.textBoxNumbering.Name = "textBoxNumbering";
            this.textBoxNumbering.Size = new System.Drawing.Size(114, 20);
            this.textBoxNumbering.TabIndex = 39;
            // 
            // groupBoxShiharai
            // 
            this.groupBoxShiharai.Controls.Add(this.checkBoxShiharai2);
            this.groupBoxShiharai.Controls.Add(this.checkBoxShiharai1);
            this.groupBoxShiharai.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxShiharai.Location = new System.Drawing.Point(218, 482);
            this.groupBoxShiharai.Name = "groupBoxShiharai";
            this.groupBoxShiharai.Size = new System.Drawing.Size(102, 38);
            this.groupBoxShiharai.TabIndex = 66;
            this.groupBoxShiharai.TabStop = false;
            this.groupBoxShiharai.Text = "支払済み";
            // 
            // checkBoxShiharai2
            // 
            this.checkBoxShiharai2.AutoSize = true;
            this.checkBoxShiharai2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxShiharai2.Name = "checkBoxShiharai2";
            this.checkBoxShiharai2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxShiharai2.TabIndex = 1;
            this.checkBoxShiharai2.Text = "外";
            this.checkBoxShiharai2.UseVisualStyleBackColor = true;
            // 
            // checkBoxShiharai1
            // 
            this.checkBoxShiharai1.AutoSize = true;
            this.checkBoxShiharai1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxShiharai1.Name = "checkBoxShiharai1";
            this.checkBoxShiharai1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxShiharai1.TabIndex = 0;
            this.checkBoxShiharai1.Text = "対象";
            this.checkBoxShiharai1.UseVisualStyleBackColor = true;
            // 
            // groupBoxProcess1
            // 
            this.groupBoxProcess1.Controls.Add(this.checkBoxProcess12);
            this.groupBoxProcess1.Controls.Add(this.checkBoxProcess11);
            this.groupBoxProcess1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxProcess1.Location = new System.Drawing.Point(218, 522);
            this.groupBoxProcess1.Name = "groupBoxProcess1";
            this.groupBoxProcess1.Size = new System.Drawing.Size(102, 38);
            this.groupBoxProcess1.TabIndex = 67;
            this.groupBoxProcess1.TabStop = false;
            this.groupBoxProcess1.Text = "処理1";
            // 
            // checkBoxProcess12
            // 
            this.checkBoxProcess12.AutoSize = true;
            this.checkBoxProcess12.Location = new System.Drawing.Point(54, 17);
            this.checkBoxProcess12.Name = "checkBoxProcess12";
            this.checkBoxProcess12.Size = new System.Drawing.Size(36, 16);
            this.checkBoxProcess12.TabIndex = 1;
            this.checkBoxProcess12.Text = "外";
            this.checkBoxProcess12.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess11
            // 
            this.checkBoxProcess11.AutoSize = true;
            this.checkBoxProcess11.Location = new System.Drawing.Point(8, 17);
            this.checkBoxProcess11.Name = "checkBoxProcess11";
            this.checkBoxProcess11.Size = new System.Drawing.Size(48, 16);
            this.checkBoxProcess11.TabIndex = 0;
            this.checkBoxProcess11.Text = "対象";
            this.checkBoxProcess11.UseVisualStyleBackColor = true;
            // 
            // groupBoxProcess2
            // 
            this.groupBoxProcess2.Controls.Add(this.checkBoxProcess22);
            this.groupBoxProcess2.Controls.Add(this.checkBoxProcess21);
            this.groupBoxProcess2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxProcess2.Location = new System.Drawing.Point(218, 562);
            this.groupBoxProcess2.Name = "groupBoxProcess2";
            this.groupBoxProcess2.Size = new System.Drawing.Size(102, 38);
            this.groupBoxProcess2.TabIndex = 68;
            this.groupBoxProcess2.TabStop = false;
            this.groupBoxProcess2.Text = "処理2";
            // 
            // checkBoxProcess22
            // 
            this.checkBoxProcess22.AutoSize = true;
            this.checkBoxProcess22.Location = new System.Drawing.Point(54, 17);
            this.checkBoxProcess22.Name = "checkBoxProcess22";
            this.checkBoxProcess22.Size = new System.Drawing.Size(36, 16);
            this.checkBoxProcess22.TabIndex = 1;
            this.checkBoxProcess22.Text = "外";
            this.checkBoxProcess22.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess21
            // 
            this.checkBoxProcess21.AutoSize = true;
            this.checkBoxProcess21.Location = new System.Drawing.Point(8, 17);
            this.checkBoxProcess21.Name = "checkBoxProcess21";
            this.checkBoxProcess21.Size = new System.Drawing.Size(48, 16);
            this.checkBoxProcess21.TabIndex = 0;
            this.checkBoxProcess21.Text = "対象";
            this.checkBoxProcess21.UseVisualStyleBackColor = true;
            // 
            // groupBoxBlack
            // 
            this.groupBoxBlack.Controls.Add(this.checkBoxBlack2);
            this.groupBoxBlack.Controls.Add(this.checkBoxBlack1);
            this.groupBoxBlack.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxBlack.Location = new System.Drawing.Point(5, 722);
            this.groupBoxBlack.Name = "groupBoxBlack";
            this.groupBoxBlack.Size = new System.Drawing.Size(102, 38);
            this.groupBoxBlack.TabIndex = 54;
            this.groupBoxBlack.TabStop = false;
            this.groupBoxBlack.Text = "照会除外リスト";
            // 
            // checkBoxBlack2
            // 
            this.checkBoxBlack2.AutoSize = true;
            this.checkBoxBlack2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.checkBoxBlack2.Location = new System.Drawing.Point(53, 17);
            this.checkBoxBlack2.Name = "checkBoxBlack2";
            this.checkBoxBlack2.Size = new System.Drawing.Size(36, 16);
            this.checkBoxBlack2.TabIndex = 1;
            this.checkBoxBlack2.Text = "外";
            this.checkBoxBlack2.UseVisualStyleBackColor = true;
            // 
            // checkBoxBlack1
            // 
            this.checkBoxBlack1.AutoSize = true;
            this.checkBoxBlack1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxBlack1.Name = "checkBoxBlack1";
            this.checkBoxBlack1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxBlack1.TabIndex = 0;
            this.checkBoxBlack1.Text = "対象";
            this.checkBoxBlack1.UseVisualStyleBackColor = true;
            // 
            // checkBoxOutside
            // 
            this.checkBoxOutside.AutoSize = true;
            this.checkBoxOutside.Location = new System.Drawing.Point(131, 195);
            this.checkBoxOutside.Name = "checkBoxOutside";
            this.checkBoxOutside.Size = new System.Drawing.Size(50, 17);
            this.checkBoxOutside.TabIndex = 27;
            this.checkBoxOutside.Text = "外部";
            this.checkBoxOutside.UseVisualStyleBackColor = true;
            // 
            // groupBoxShokaiHen
            // 
            this.groupBoxShokaiHen.Controls.Add(this.checkBoxShokaiHen2);
            this.groupBoxShokaiHen.Controls.Add(this.checkBoxShokaiHen1);
            this.groupBoxShokaiHen.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxShokaiHen.Location = new System.Drawing.Point(111, 683);
            this.groupBoxShokaiHen.Name = "groupBoxShokaiHen";
            this.groupBoxShokaiHen.Size = new System.Drawing.Size(102, 38);
            this.groupBoxShokaiHen.TabIndex = 63;
            this.groupBoxShokaiHen.TabStop = false;
            this.groupBoxShokaiHen.Text = "照会返信";
            // 
            // checkBoxShokaiHen2
            // 
            this.checkBoxShokaiHen2.AutoSize = true;
            this.checkBoxShokaiHen2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.checkBoxShokaiHen2.Location = new System.Drawing.Point(53, 17);
            this.checkBoxShokaiHen2.Name = "checkBoxShokaiHen2";
            this.checkBoxShokaiHen2.Size = new System.Drawing.Size(48, 16);
            this.checkBoxShokaiHen2.TabIndex = 1;
            this.checkBoxShokaiHen2.Text = "未返";
            this.checkBoxShokaiHen2.UseVisualStyleBackColor = true;
            // 
            // checkBoxShokaiHen1
            // 
            this.checkBoxShokaiHen1.AutoSize = true;
            this.checkBoxShokaiHen1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxShokaiHen1.Name = "checkBoxShokaiHen1";
            this.checkBoxShokaiHen1.Size = new System.Drawing.Size(48, 16);
            this.checkBoxShokaiHen1.TabIndex = 0;
            this.checkBoxShokaiHen1.Text = "返済";
            this.checkBoxShokaiHen1.UseVisualStyleBackColor = true;
            // 
            // labelBatch
            // 
            this.labelBatch.AutoSize = true;
            this.labelBatch.Enabled = false;
            this.labelBatch.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelBatch.Location = new System.Drawing.Point(3, 319);
            this.labelBatch.Name = "labelBatch";
            this.labelBatch.Size = new System.Drawing.Size(55, 12);
            this.labelBatch.TabIndex = 40;
            this.labelBatch.Text = "学校バッチ";
            // 
            // textBoxBatch
            // 
            this.textBoxBatch.Enabled = false;
            this.textBoxBatch.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxBatch.Location = new System.Drawing.Point(63, 315);
            this.textBoxBatch.MaxLength = 20;
            this.textBoxBatch.Name = "textBoxBatch";
            this.textBoxBatch.Size = new System.Drawing.Size(114, 20);
            this.textBoxBatch.TabIndex = 41;
            // 
            // labelBankAccount
            // 
            this.labelBankAccount.AutoSize = true;
            this.labelBankAccount.Enabled = false;
            this.labelBankAccount.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelBankAccount.Location = new System.Drawing.Point(3, 343);
            this.labelBankAccount.Name = "labelBankAccount";
            this.labelBankAccount.Size = new System.Drawing.Size(53, 12);
            this.labelBankAccount.TabIndex = 42;
            this.labelBankAccount.Text = "学校口座";
            // 
            // textBoxBankAccount
            // 
            this.textBoxBankAccount.Enabled = false;
            this.textBoxBankAccount.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxBankAccount.Location = new System.Drawing.Point(63, 340);
            this.textBoxBankAccount.MaxLength = 20;
            this.textBoxBankAccount.Name = "textBoxBankAccount";
            this.textBoxBankAccount.Size = new System.Drawing.Size(114, 20);
            this.textBoxBankAccount.TabIndex = 43;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "処理月";
            // 
            // textBoxCymTo
            // 
            this.textBoxCymTo.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxCymTo.Location = new System.Drawing.Point(128, 25);
            this.textBoxCymTo.MaxLength = 8;
            this.textBoxCymTo.Name = "textBoxCymTo";
            this.textBoxCymTo.Size = new System.Drawing.Size(49, 20);
            this.textBoxCymTo.TabIndex = 6;
            // 
            // textBoxCymFrom
            // 
            this.textBoxCymFrom.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxCymFrom.Location = new System.Drawing.Point(63, 26);
            this.textBoxCymFrom.MaxLength = 8;
            this.textBoxCymFrom.Name = "textBoxCymFrom";
            this.textBoxCymFrom.Size = new System.Drawing.Size(49, 20);
            this.textBoxCymFrom.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label20.Location = new System.Drawing.Point(112, 31);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 11);
            this.label20.TabIndex = 5;
            this.label20.Text = "～";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxProcess32);
            this.groupBox1.Controls.Add(this.checkBoxProcess31);
            this.groupBox1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(218, 602);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(102, 38);
            this.groupBox1.TabIndex = 69;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "処理3";
            // 
            // checkBoxProcess32
            // 
            this.checkBoxProcess32.AutoSize = true;
            this.checkBoxProcess32.Location = new System.Drawing.Point(54, 17);
            this.checkBoxProcess32.Name = "checkBoxProcess32";
            this.checkBoxProcess32.Size = new System.Drawing.Size(36, 16);
            this.checkBoxProcess32.TabIndex = 1;
            this.checkBoxProcess32.Text = "外";
            this.checkBoxProcess32.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess31
            // 
            this.checkBoxProcess31.AutoSize = true;
            this.checkBoxProcess31.Location = new System.Drawing.Point(8, 17);
            this.checkBoxProcess31.Name = "checkBoxProcess31";
            this.checkBoxProcess31.Size = new System.Drawing.Size(48, 16);
            this.checkBoxProcess31.TabIndex = 0;
            this.checkBoxProcess31.Text = "対象";
            this.checkBoxProcess31.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxProcess42);
            this.groupBox2.Controls.Add(this.checkBoxProcess41);
            this.groupBox2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(218, 642);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(102, 38);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "処理4";
            // 
            // checkBoxProcess42
            // 
            this.checkBoxProcess42.AutoSize = true;
            this.checkBoxProcess42.Location = new System.Drawing.Point(54, 17);
            this.checkBoxProcess42.Name = "checkBoxProcess42";
            this.checkBoxProcess42.Size = new System.Drawing.Size(36, 16);
            this.checkBoxProcess42.TabIndex = 1;
            this.checkBoxProcess42.Text = "外";
            this.checkBoxProcess42.UseVisualStyleBackColor = true;
            // 
            // checkBoxProcess41
            // 
            this.checkBoxProcess41.AutoSize = true;
            this.checkBoxProcess41.Location = new System.Drawing.Point(8, 17);
            this.checkBoxProcess41.Name = "checkBoxProcess41";
            this.checkBoxProcess41.Size = new System.Drawing.Size(48, 16);
            this.checkBoxProcess41.TabIndex = 0;
            this.checkBoxProcess41.Text = "対象";
            this.checkBoxProcess41.UseVisualStyleBackColor = true;
            // 
            // chkInputCount
            // 
            this.chkInputCount.AutoSize = true;
            this.chkInputCount.Location = new System.Drawing.Point(124, 76);
            this.chkInputCount.Name = "chkInputCount";
            this.chkInputCount.Size = new System.Drawing.Size(62, 17);
            this.chkInputCount.TabIndex = 12;
            this.chkInputCount.Text = "入力数";
            this.chkInputCount.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(92, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "以上";
            // 
            // textBoxAID
            // 
            this.textBoxAID.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textBoxAID.Location = new System.Drawing.Point(63, 366);
            this.textBoxAID.Name = "textBoxAID";
            this.textBoxAID.Size = new System.Drawing.Size(114, 20);
            this.textBoxAID.TabIndex = 45;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label21.Location = new System.Drawing.Point(3, 369);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(16, 12);
            this.label21.TabIndex = 44;
            this.label21.Text = "ID";
            // 
            // groupBoxOutMemo
            // 
            this.groupBoxOutMemo.Controls.Add(this.checkBoxOutMemo2);
            this.groupBoxOutMemo.Controls.Add(this.checkBoxOutMemo1);
            this.groupBoxOutMemo.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxOutMemo.Location = new System.Drawing.Point(219, 723);
            this.groupBoxOutMemo.Name = "groupBoxOutMemo";
            this.groupBoxOutMemo.Size = new System.Drawing.Size(102, 38);
            this.groupBoxOutMemo.TabIndex = 72;
            this.groupBoxOutMemo.TabStop = false;
            this.groupBoxOutMemo.Text = "出力メモ";
            // 
            // checkBoxOutMemo2
            // 
            this.checkBoxOutMemo2.AutoSize = true;
            this.checkBoxOutMemo2.Location = new System.Drawing.Point(54, 17);
            this.checkBoxOutMemo2.Name = "checkBoxOutMemo2";
            this.checkBoxOutMemo2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxOutMemo2.TabIndex = 1;
            this.checkBoxOutMemo2.Text = "なし";
            this.checkBoxOutMemo2.UseVisualStyleBackColor = true;
            // 
            // checkBoxOutMemo1
            // 
            this.checkBoxOutMemo1.AutoSize = true;
            this.checkBoxOutMemo1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxOutMemo1.Name = "checkBoxOutMemo1";
            this.checkBoxOutMemo1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxOutMemo1.TabIndex = 0;
            this.checkBoxOutMemo1.Text = "あり";
            this.checkBoxOutMemo1.UseVisualStyleBackColor = true;
            // 
            // groupBoxVisitAdd
            // 
            this.groupBoxVisitAdd.Controls.Add(this.checkBoxVisitAdd2);
            this.groupBoxVisitAdd.Controls.Add(this.checkBoxVisitAdd1);
            this.groupBoxVisitAdd.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBoxVisitAdd.Location = new System.Drawing.Point(5, 602);
            this.groupBoxVisitAdd.Name = "groupBoxVisitAdd";
            this.groupBoxVisitAdd.Size = new System.Drawing.Size(102, 38);
            this.groupBoxVisitAdd.TabIndex = 51;
            this.groupBoxVisitAdd.TabStop = false;
            this.groupBoxVisitAdd.Text = "加算あり";
            // 
            // checkBoxVisitAdd2
            // 
            this.checkBoxVisitAdd2.AutoSize = true;
            this.checkBoxVisitAdd2.Location = new System.Drawing.Point(53, 17);
            this.checkBoxVisitAdd2.Name = "checkBoxVisitAdd2";
            this.checkBoxVisitAdd2.Size = new System.Drawing.Size(43, 16);
            this.checkBoxVisitAdd2.TabIndex = 1;
            this.checkBoxVisitAdd2.Text = "なし";
            this.checkBoxVisitAdd2.UseVisualStyleBackColor = true;
            // 
            // checkBoxVisitAdd1
            // 
            this.checkBoxVisitAdd1.AutoSize = true;
            this.checkBoxVisitAdd1.Location = new System.Drawing.Point(8, 17);
            this.checkBoxVisitAdd1.Name = "checkBoxVisitAdd1";
            this.checkBoxVisitAdd1.Size = new System.Drawing.Size(42, 16);
            this.checkBoxVisitAdd1.TabIndex = 0;
            this.checkBoxVisitAdd1.Text = "あり";
            this.checkBoxVisitAdd1.UseVisualStyleBackColor = true;
            // 
            // UserControlSearchMini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxVisitAdd);
            this.Controls.Add(this.groupBoxOutMemo);
            this.Controls.Add(this.textBoxAID);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.chkInputCount);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxProcess2);
            this.Controls.Add(this.groupBoxProcess1);
            this.Controls.Add(this.groupBoxShiharai);
            this.Controls.Add(this.groupBoxLastYm);
            this.Controls.Add(this.groupBoxBank);
            this.Controls.Add(this.textBoxBankAccount);
            this.Controls.Add(this.labelBankAccount);
            this.Controls.Add(this.textBoxBatch);
            this.Controls.Add(this.labelBatch);
            this.Controls.Add(this.textBoxNumbering);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBoxMediYM);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textBoxFirstYM);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBoxShokaiHen);
            this.Controls.Add(this.groupBoxBlack);
            this.Controls.Add(this.groupBoxAge);
            this.Controls.Add(this.groupBoxFusho);
            this.Controls.Add(this.checkBoxOutside);
            this.Controls.Add(this.checkBoxFusho);
            this.Controls.Add(this.groupBoxOryoSai);
            this.Controls.Add(this.groupBoxOryoGigi);
            this.Controls.Add(this.groupBoxLastM);
            this.Controls.Add(this.textBoxCont);
            this.Controls.Add(this.textBoxKako);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBoxTel);
            this.Controls.Add(this.groupBoxMemo);
            this.Controls.Add(this.groupBoxInputError);
            this.Controls.Add(this.groupBoxPend);
            this.Controls.Add(this.groupBoxHenrei);
            this.Controls.Add(this.groupBoxOryoTenken);
            this.Controls.Add(this.groupBoxTenken);
            this.Controls.Add(this.groupBoxSyokai);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBoxSaishinsa);
            this.Controls.Add(this.groupBoxKago);
            this.Controls.Add(this.textBoxCymFrom);
            this.Controls.Add(this.textBoxTotalMin);
            this.Controls.Add(this.groupBoxVisit);
            this.Controls.Add(this.textBoxCymTo);
            this.Controls.Add(this.groupBoxShinki);
            this.Controls.Add(this.textBoxTotalMax);
            this.Controls.Add(this.textBoxDays);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.checkBoxMassage);
            this.Controls.Add(this.checkBoxHari);
            this.Controls.Add(this.checkBoxJyu);
            this.Controls.Add(this.textBoxDrNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxClinicNum);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxBui);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxNum);
            this.Name = "UserControlSearchMini";
            this.Size = new System.Drawing.Size(330, 810);
            this.groupBoxSyokai.ResumeLayout(false);
            this.groupBoxSyokai.PerformLayout();
            this.groupBoxSaishinsa.ResumeLayout(false);
            this.groupBoxSaishinsa.PerformLayout();
            this.groupBoxKago.ResumeLayout(false);
            this.groupBoxKago.PerformLayout();
            this.groupBoxVisit.ResumeLayout(false);
            this.groupBoxVisit.PerformLayout();
            this.groupBoxShinki.ResumeLayout(false);
            this.groupBoxShinki.PerformLayout();
            this.groupBoxLastM.ResumeLayout(false);
            this.groupBoxLastM.PerformLayout();
            this.groupBoxOryoGigi.ResumeLayout(false);
            this.groupBoxOryoGigi.PerformLayout();
            this.groupBoxFusho.ResumeLayout(false);
            this.groupBoxFusho.PerformLayout();
            this.groupBoxAge.ResumeLayout(false);
            this.groupBoxAge.PerformLayout();
            this.groupBoxBank.ResumeLayout(false);
            this.groupBoxBank.PerformLayout();
            this.groupBoxOryoSai.ResumeLayout(false);
            this.groupBoxOryoSai.PerformLayout();
            this.groupBoxTenken.ResumeLayout(false);
            this.groupBoxTenken.PerformLayout();
            this.groupBoxOryoTenken.ResumeLayout(false);
            this.groupBoxOryoTenken.PerformLayout();
            this.groupBoxHenrei.ResumeLayout(false);
            this.groupBoxHenrei.PerformLayout();
            this.groupBoxPend.ResumeLayout(false);
            this.groupBoxPend.PerformLayout();
            this.groupBoxLastYm.ResumeLayout(false);
            this.groupBoxLastYm.PerformLayout();
            this.groupBoxInputError.ResumeLayout(false);
            this.groupBoxInputError.PerformLayout();
            this.groupBoxMemo.ResumeLayout(false);
            this.groupBoxMemo.PerformLayout();
            this.groupBoxTel.ResumeLayout(false);
            this.groupBoxTel.PerformLayout();
            this.groupBoxShiharai.ResumeLayout(false);
            this.groupBoxShiharai.PerformLayout();
            this.groupBoxProcess1.ResumeLayout(false);
            this.groupBoxProcess1.PerformLayout();
            this.groupBoxProcess2.ResumeLayout(false);
            this.groupBoxProcess2.PerformLayout();
            this.groupBoxBlack.ResumeLayout(false);
            this.groupBoxBlack.PerformLayout();
            this.groupBoxShokaiHen.ResumeLayout(false);
            this.groupBoxShokaiHen.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxOutMemo.ResumeLayout(false);
            this.groupBoxOutMemo.PerformLayout();
            this.groupBoxVisitAdd.ResumeLayout(false);
            this.groupBoxVisitAdd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSyokai;
        private System.Windows.Forms.CheckBox checkBoxSyokai1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxSaishinsa;
        private System.Windows.Forms.CheckBox checkBoxGigi2;
        private System.Windows.Forms.CheckBox checkBoxGigi1;
        private System.Windows.Forms.GroupBox groupBoxKago;
        private System.Windows.Forms.CheckBox checkBoxKago2;
        private System.Windows.Forms.CheckBox checkBoxKago1;
        private System.Windows.Forms.TextBox textBoxTotalMin;
        private System.Windows.Forms.GroupBox groupBoxVisit;
        private System.Windows.Forms.CheckBox checkBoxVisit2;
        private System.Windows.Forms.CheckBox checkBoxVisit1;
        private System.Windows.Forms.GroupBox groupBoxShinki;
        private System.Windows.Forms.CheckBox checkBoxNew2;
        private System.Windows.Forms.CheckBox checkBoxNew1;
        private System.Windows.Forms.TextBox textBoxTotalMax;
        private System.Windows.Forms.TextBox textBoxDays;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox checkBoxMassage;
        private System.Windows.Forms.CheckBox checkBoxHari;
        private System.Windows.Forms.CheckBox checkBoxJyu;
        private System.Windows.Forms.TextBox textBoxClinicNum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxBui;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNum;
        private System.Windows.Forms.TextBox textBoxKako;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxLastM;
        private System.Windows.Forms.CheckBox checkBoxLast2;
        private System.Windows.Forms.CheckBox checkBoxLast1;
        private System.Windows.Forms.GroupBox groupBoxOryoGigi;
        private System.Windows.Forms.CheckBox checkBoxOryoGigi2;
        private System.Windows.Forms.CheckBox checkBoxOryoGigi1;
        private System.Windows.Forms.GroupBox groupBoxFusho;
        private System.Windows.Forms.CheckBox checkBoxSe;
        private System.Windows.Forms.CheckBox checkBoxKoshi;
        private System.Windows.Forms.CheckBox checkBoxKata;
        private System.Windows.Forms.CheckBox checkBoxHiza;
        private System.Windows.Forms.CheckBox checkBoxKubi;
        private System.Windows.Forms.TextBox textBoxCont;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxFusho;
        private System.Windows.Forms.GroupBox groupBoxAge;
        private System.Windows.Forms.CheckBox checkBoxMinor;
        private System.Windows.Forms.CheckBox checkBoxAdult;
        private System.Windows.Forms.TextBox textBoxFirstYM;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxMediYM;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBoxBank;
        private System.Windows.Forms.CheckBox checkBoxBank1;
        private System.Windows.Forms.GroupBox groupBoxOryoSai;
        private System.Windows.Forms.CheckBox checkBoxOryoSai2;
        private System.Windows.Forms.CheckBox checkBoxOryoSai1;
        private System.Windows.Forms.GroupBox groupBoxTenken;
        private System.Windows.Forms.CheckBox checkBoxTenken1;
        private System.Windows.Forms.GroupBox groupBoxOryoTenken;
        private System.Windows.Forms.CheckBox checkBoxOryoTenken1;
        private System.Windows.Forms.CheckBox checkBoxSyokai2;
        private System.Windows.Forms.CheckBox checkBoxTenken2;
        private System.Windows.Forms.CheckBox checkBoxOryoTenken2;
        private System.Windows.Forms.GroupBox groupBoxHenrei;
        private System.Windows.Forms.CheckBox checkBoxHenrei2;
        private System.Windows.Forms.CheckBox checkBoxHenrei1;
        private System.Windows.Forms.GroupBox groupBoxPend;
        private System.Windows.Forms.CheckBox checkBoxPend2;
        private System.Windows.Forms.CheckBox checkBoxPend1;
        private System.Windows.Forms.GroupBox groupBoxLastYm;
        private System.Windows.Forms.CheckBox checkBoxLastYm1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDrNum;
        private System.Windows.Forms.CheckBox checkBoxLastYm2;
        private System.Windows.Forms.GroupBox groupBoxInputError;
        private System.Windows.Forms.CheckBox checkBoxInputError2;
        private System.Windows.Forms.CheckBox checkBoxInputError1;
        private System.Windows.Forms.GroupBox groupBoxMemo;
        private System.Windows.Forms.CheckBox checkBoxMemo2;
        private System.Windows.Forms.CheckBox checkBoxMemo1;
        private System.Windows.Forms.GroupBox groupBoxTel;
        private System.Windows.Forms.CheckBox checkBoxTel2;
        private System.Windows.Forms.CheckBox checkBoxTel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxNumbering;
        private System.Windows.Forms.GroupBox groupBoxShiharai;
        private System.Windows.Forms.CheckBox checkBoxShiharai2;
        private System.Windows.Forms.CheckBox checkBoxShiharai1;
        private System.Windows.Forms.GroupBox groupBoxProcess1;
        private System.Windows.Forms.CheckBox checkBoxProcess12;
        private System.Windows.Forms.CheckBox checkBoxProcess11;
        private System.Windows.Forms.GroupBox groupBoxProcess2;
        private System.Windows.Forms.CheckBox checkBoxProcess22;
        private System.Windows.Forms.CheckBox checkBoxProcess21;
        private System.Windows.Forms.CheckBox checkBoxBank2;
        private System.Windows.Forms.GroupBox groupBoxBlack;
        private System.Windows.Forms.CheckBox checkBoxBlack2;
        private System.Windows.Forms.CheckBox checkBoxBlack1;
        private System.Windows.Forms.CheckBox checkBoxOutside;
        private System.Windows.Forms.GroupBox groupBoxShokaiHen;
        private System.Windows.Forms.CheckBox checkBoxShokaiHen2;
        private System.Windows.Forms.CheckBox checkBoxShokaiHen1;
        private System.Windows.Forms.Label labelBatch;
        private System.Windows.Forms.TextBox textBoxBatch;
        private System.Windows.Forms.Label labelBankAccount;
        private System.Windows.Forms.TextBox textBoxBankAccount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxCymTo;
        private System.Windows.Forms.TextBox textBoxCymFrom;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxProcess32;
        private System.Windows.Forms.CheckBox checkBoxProcess31;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxProcess42;
        private System.Windows.Forms.CheckBox checkBoxProcess41;
        private System.Windows.Forms.CheckBox chkInputCount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxAID;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBoxOutMemo;
        private System.Windows.Forms.CheckBox checkBoxOutMemo2;
        private System.Windows.Forms.CheckBox checkBoxOutMemo1;
        private System.Windows.Forms.GroupBox groupBoxVisitAdd;
        private System.Windows.Forms.CheckBox checkBoxVisitAdd2;
        private System.Windows.Forms.CheckBox checkBoxVisitAdd1;
    }
}
