﻿using System.Linq;

namespace Mejor
{
    class BatchSelector
    {
        /// <summary>
        /// 指定した申請書が所属するバッチの情報を取得します。
        /// </summary>
        /// <param name="insurerid"></param>
        /// <param name="receAid"></param>
        /// <returns></returns>
        public static App GetBatchApp(Insurer insurer, int receAid)
        {
            switch (insurer.InsurerType)
            {
                case INSURER_TYPE.学校共済:
                    return GetBatchAppFromZenkokuGakko(receAid);
            }

            return null;
        }

        private static App GetBatchAppFromZenkokuGakko(int receAid)
        {
            var sql = "FROM application AS a WHERE a.aid=(SELECT aid FROM application " +
                $"WHERE aapptype={(int)APP_TYPE.バッチ} AND aid<{receAid} " +
                $"ORDER BY aid DESC " +
                $"LIMIT 1) ";

            return App.InspectSelect(sql).FirstOrDefault();
        }

        private static App GetBatchAppFromOsakaGakko(int receAid)
        {
            var sql = "FROM application AS a WHERE a.aid=(SELECT aid FROM application " +
                $"WHERE aapptype={(int)APP_TYPE.バッチ2} AND aid<{receAid} " +
                $"ORDER BY aid DESC " +
                $"LIMIT 1) ";

            return App.InspectSelect(sql).FirstOrDefault();
        }
    }
}
