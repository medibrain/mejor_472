﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NpgsqlTypes;

namespace Mejor
{
    public partial class UserControlOryo : UserControl
    {
        private DataGridViewCellStyle redStyle = new DataGridViewCellStyle();
        private DataGridViewCellStyle aquaStyle = new DataGridViewCellStyle();

        string patientAdd = string.Empty;
        string clinicAdd = string.Empty;
        public double AppDistance_OsakaKoiki { get; private set; } = 0;
        public double CalcDistance_OsakaKoiki { get; private set; } = 0;

        public string PatientCalcAdd =>
            app == null || textBoxPatientAddr == null ? string.Empty :
            app.HihoAdd.Trim() == textBoxPatientAddr.Text.Trim() ? string.Empty :
            textBoxPatientAddr.Text.Trim();

        private App app = null;

        public string Clinic_OsakaKoikiCalcAdd =>
            clinic_OsakaKoiki == null ? string.Empty :
            clinic_OsakaKoiki.Address.Trim() == textBoxSejutushoAddr.Text.Trim() ? string.Empty :
            textBoxSejutushoAddr.Text.Trim();
        
        private OsakaKoiki.Clinic clinic_OsakaKoiki = null;

        //20220601132336 furukawa st ////////////////////////
        //茨城広域用

        public double AppDistance_IbarakiKoiki { get; private set; } = 0;
        public double CalcDistance_IbarakiKoiki { get; private set; } = 0;


        public string Clinic_IbarakiKoikiCalcAdd =>
            clinic_IbarakiKoiki == null ? string.Empty :
            clinic_IbarakiKoiki.f008_clinicadd.Trim() == textBoxSejutushoAddr.Text.Trim() ? string.Empty :
            textBoxSejutushoAddr.Text.Trim();

        private IbarakiKoiki.imp_clinicMaster clinic_IbarakiKoiki = null;
        //20220601132336 furukawa ed ////////////////////////


        public Insurer ins;

        //20210205173124 furukawa st ////////////////////////
        //メモ、出力用メモ保持
        
        public string OutMemo => textBoxOutMemo.Text;
        public string Memo => textBoxMemo.Text;
        //20210205173124 furukawa ed ////////////////////////

        public UserControlOryo()
        {
            InitializeComponent();

            redStyle.BackColor = Color.MistyRose;
            redStyle.SelectionBackColor = Color.MistyRose;
            aquaStyle.BackColor = Color.Aqua;
            aquaStyle.SelectionBackColor = Color.Aqua;

            dataGridViewKubun.Rows.Add(" 6km-    ", " 6km-    ");
            dataGridViewKubun.Rows.Add(" 4km- 6km", " 4km- 6km");
            dataGridViewKubun.Rows.Add(" 2km- 4km", " 2km- 4km");
            dataGridViewKubun.Rows.Add("    - 2km", "    - 2km");
            dataGridViewKubun.DefaultCellStyle.SelectionBackColor = dataGridViewKubun.DefaultCellStyle.BackColor;
            dataGridViewKubun.DefaultCellStyle.SelectionForeColor = dataGridViewKubun.DefaultCellStyle.ForeColor;

            //20211012141340 furukawa st ////////////////////////
            //リスト作成画面表示時には、表示高速化のためロードしない

            //LoadAddressInsurer();
            //20211012141340 furukawa ed ////////////////////////

        }


        //20190723191843 furukawa 住所候補ロード
        /// <summary>
        /// 住所候補ロード
        /// </summary>
        private void LoadGeoData()
        {
            System.Collections.Generic.List<string> acsc = new System.Collections.Generic.List<string>();
            textBoxPatientAddr.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBoxPatientAddr.AutoCompleteSource = AutoCompleteSource.CustomSource;

            textBoxSejutushoAddr.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBoxSejutushoAddr.AutoCompleteSource = AutoCompleteSource.CustomSource;

            acsc = GeoData.GetGeoPref(Insurer.CurrrentInsurer);

            //20211012141307 furukawa st ////////////////////////
            //ロード高速化
            
            var autocompletelist1 = new AutoCompleteStringCollection();
            var autocompletelist2 = new AutoCompleteStringCollection();
            textBoxPatientAddr.AutoCompleteCustomSource = autocompletelist1;
            textBoxSejutushoAddr.AutoCompleteCustomSource = autocompletelist2;
            autocompletelist1.AddRange(acsc.ToArray());
            autocompletelist2.AddRange(acsc.ToArray());

            //foreach (string s in acsc)
            //{
            //    textBoxPatientAddr.AutoCompleteCustomSource.Add(s);
            //    textBoxSejutushoAddr.AutoCompleteCustomSource.Add(s);
            //}

            //20211012141307 furukawa ed ////////////////////////
        }


        //20190726101329 furukawa 住所候補をロードする保険者分岐
        /// <summary>
        /// 往療料計算が必要な保険者で分岐
        /// </summary>
        private void LoadAddressInsurer()
        {
            switch (Insurer.CurrrentInsurer?.InsurerID)
            {
                                
                case (int)InsurerID.OSAKA_KOIKI:
                case (int)InsurerID.IBARAKI_KOIKI://20220516164352 furukawa 茨城広域追加
                    LoadGeoData();
                    break;

                default:
                    break;
            }
           
        }



       

        public void SetApp(App app)
        {
            ResetControl();
            
            this.app = app;
            patientAdd = app.HihoAdd;
            textBoxPatientAddr.Text = string.IsNullOrEmpty(app.TaggedDatas.DistCalcAdd) ? app.HihoAdd : app.TaggedDatas.DistCalcAdd;


            //20210205160634 furukawa st ////////////////////////
            //メモ、出力用メモをロード
            
            textBoxMemo.Text = app.Memo;
            textBoxOutMemo.Text = app.OutMemo;
            //20210205160634 furukawa ed ////////////////////////


            //20190724134735 furukawa st ////////////////////////
            //大阪広域以外でも使うようになったので分岐

            switch (Insurer.CurrrentInsurer.InsurerID)

            {

                case (int)InsurerID.OSAKA_KOIKI:
                
                    //大阪広域用コード

                    clinic_OsakaKoiki = OsakaKoiki.Clinic.GetClinic(app.ClinicNum);
                    clinicAdd = clinic_OsakaKoiki?.Address ?? string.Empty;
                    textBoxSejutushoAddr.Text = string.IsNullOrEmpty(app.TaggedDatas.DistCalcClinicAdd) ? clinicAdd : app.TaggedDatas.DistCalcClinicAdd;

                    AppDistance_OsakaKoiki = app.TaggedDatas.AppDistance != 0 ? app.TaggedDatas.AppDistance :
                        app.Distance == 0 ? 0 :
                        app.VisitAdd / 1000d + 2;
                    CalcDistance_OsakaKoiki = app.TaggedDatas.CalcDistance == 0 ?
                        OsakaKoiki.CalcDistance.GetCalcDistance(app) : app.TaggedDatas.CalcDistance;

                    toolStripStatusLabel.Text = "自動距離計算の結果です";
                    break;

                //20220601132411 furukawa st ////////////////////////
                //茨城広域用
                
                case (int)InsurerID.IBARAKI_KOIKI:
                    //20220601142259 furukawa st ////////////////////////
                    //医療機関コードがない場合抜ける
                    
                    if (app.ClinicNum == string.Empty) break;
                    //20220601142259 furukawa ed ////////////////////////

                    clinic_IbarakiKoiki = IbarakiKoiki.imp_clinicMaster.Select(app.CYM,app.ClinicNum)[0];
                    clinicAdd = clinic_IbarakiKoiki?.f008_clinicadd ?? string.Empty;
                    textBoxSejutushoAddr.Text = string.IsNullOrEmpty(app.TaggedDatas.DistCalcClinicAdd) ? clinicAdd : app.TaggedDatas.DistCalcClinicAdd;

                    AppDistance_IbarakiKoiki = app.TaggedDatas.AppDistance != 0 ? app.TaggedDatas.AppDistance :
                        app.Distance == 0 ? 0 :
                        app.VisitAdd / 1000d + 2;
                    //CalcDistance_IbarakiKoiki = app.TaggedDatas.CalcDistance == 0 ?
                    //    IbarakiKoiki.CalcDistance.GetCalcDistance(app) : app.TaggedDatas.CalcDistance;

                    toolStripStatusLabel.Text = "自動距離計算の結果です";
                    break;

                //20220601132411 furukawa ed ////////////////////////

                case (int)InsurerID.KISHIWADA_KOKUHO:
                   // clinicAdd = app.ClinicAdd;

                    //clinic = KishiwadaKokuho.RefRece.GetClinic(app.ClinicNum);
                    //clinicAdd = clinic?.Address ?? string.Empty;
                    //textBoxSejutushoAddr.Text = string.IsNullOrEmpty(app.TaggedDatas.DistCalcClinicAdd) ? clinicAdd : app.TaggedDatas.DistCalcClinicAdd;

                    //AppDistance = app.TaggedDatas.AppDistance != 0 ? app.TaggedDatas.AppDistance :
                    //    app.Distance == 0 ? 0 :
                    //    app.VisitAdd / 1000d + 2;
                    //CalcDistance = app.TaggedDatas.CalcDistance == 0 ?
                    //    OsakaKoiki.CalcDistance.GetCalcDistance(app) : app.TaggedDatas.CalcDistance;

                    //toolStripStatusLabel.Text = "自動距離計算の結果です";
                    break;

                default:

                    break;

            }

            //20190724134735 furukawa ed ////////////////////////


            showData();
        }

        private void showData()
        {
            textBoxKeisanKyori.Text = string.Format("{0:0.0}", CalcDistance_OsakaKoiki);
            textBoxSinseiKyori.Text = string.Format("{0:0.0}", AppDistance_OsakaKoiki);
            textBoxPOrgAddr.Text = patientAdd;
            textBoxSOrgAddr.Text = clinicAdd;

            rankHilight(CalcDistance_OsakaKoiki, AppDistance_OsakaKoiki);
        }

        public void ResetControl()
        {
            app = null;
            textBoxPOrgAddr.Text = string.Empty;
            textBoxSOrgAddr.Text = string.Empty;
            textBoxKeisanKyori.Text = string.Empty;
            textBoxSinseiKyori.Text = string.Empty;
            textBoxPatientAddr.Text = string.Empty;
            textBoxSejutushoAddr.Text = string.Empty;

            statusStripKeisan.BackColor = Color.Transparent;
            textBoxPatientAddr.BackColor = Color.White;
            textBoxSejutushoAddr.BackColor = Color.White;

            for (int i = 0; i < dataGridViewKubun.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridViewKubun.Rows[i].Cells.Count; j++)
                {
                    dataGridViewKubun.Rows[i].Cells[j].Style = null;
                }
            }
        }

        private void buttonRecalculate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxPatientAddr.Text))
                textBoxPatientAddr.Text = patientAdd;

            if (string.IsNullOrWhiteSpace(textBoxSejutushoAddr.Text))
                textBoxSejutushoAddr.Text = clinicAdd;

            double d;
            if (string.IsNullOrWhiteSpace(textBoxSinseiKyori.Text))
            {
                d = app.Distance == 0 ? 0 : app.VisitAdd / 1000d + 2;
            }
            else if (!double.TryParse(textBoxSinseiKyori.Text, out d))
            {
                textBoxPatientAddr.BackColor = Color.Tomato;
                toolStripStatusLabel.Text = "申請距離を正しく指定して下さい";
                return;
            }
            else
            {
                textBoxPatientAddr.BackColor = Color.White;
            }
            AppDistance_OsakaKoiki = d;

            string status = "再計算を行いました";
            var newDist = GeoData.CalculateDistance(textBoxSejutushoAddr.Text, textBoxPatientAddr.Text);
            if (newDist >= 0)
            {
                CalcDistance_OsakaKoiki = newDist;
                textBoxKeisanKyori.Text = string.Format("{0:0.0}", newDist);
                rankHilight(newDist, CalcDistance_OsakaKoiki);
                statusStripKeisan.BackColor = Color.Aqua;
                textBoxPatientAddr.BackColor = Color.White;
            }
            else
            {
                CalcDistance_OsakaKoiki = 0;
                var eCode = (GeoData.ErrorCode)newDist;
                switch (eCode)
                {
                    case GeoData.ErrorCode.Error_NoData:
                        status = "データが空です";
                        statusStripKeisan.BackColor = Color.Tomato;
                        break;
                    case GeoData.ErrorCode.Error_PatientAddr:
                        status = "対応する住所座標データがありません。";
                        textBoxPatientAddr.BackColor = Color.Tomato;
                        statusStripKeisan.BackColor = Color.Tomato;
                        ActiveControl = textBoxPatientAddr;
                        break;
                    case GeoData.ErrorCode.Error_SejutuAddr:
                        status = "対応する住所座標データがありません。";
                        textBoxSejutushoAddr.BackColor = Color.Tomato;
                        statusStripKeisan.BackColor = Color.Tomato;
                        ActiveControl = textBoxSejutushoAddr;
                        break;
                    case GeoData.ErrorCode.Error_BothAddr:
                        status = "対応する住所座標データがありません。";
                        textBoxPatientAddr.BackColor = Color.Tomato;
                        textBoxSejutushoAddr.BackColor = Color.Tomato;
                        statusStripKeisan.BackColor = Color.Tomato;
                        ActiveControl = textBoxPatientAddr;
                        break;
                }
            }

            toolStripStatusLabel.Text = status;
            showData();
        }

        private void rankHilight(double calc, double input)
        {
            // リセット
            for (int i = 0; i < dataGridViewKubun.Rows.Count; i++)
            {
                for (int j = 0; j < dataGridViewKubun.Rows[i].Cells.Count; j++)
                {
                    dataGridViewKubun.Rows[i].Cells[j].Style = null;
                }
            }

            // 区分をハイライト
            var inputRank = OsakaKoiki.CalcDistance.GetDistanceRank(input);
            var calcRank = OsakaKoiki.CalcDistance.GetDistanceRank(calc);
            var style = (calcRank < inputRank) ? redStyle : aquaStyle;

            dataGridViewKubun[0, (4 - (int)inputRank) >= 0 ? (4 - (int)inputRank) : 0].Style = style;
            dataGridViewKubun[1, (4 - (int)calcRank) >= 0 ? (4 - (int)calcRank) : 0].Style = style;
        }

        private void buttonMemo_Click(object sender, EventArgs e)
        {
            //メモをあちこちに増やそうと言われるかもなので、あえて使わせない
            app.MemoInspect = textBoxMemo.Text;
            app.OutMemo = textBoxOutMemo.Text;
            app.UpdateINQ();
        }

        private void UserControlOryo_Load(object sender, EventArgs e)
        {
            //20211012141425 furukawa st ////////////////////////
            //座標データロード
          //  WaitFormSimple wf = new WaitFormSimple();
            try
            {
                using (WaitFormSimple wf = new WaitFormSimple())
                {
                    System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
                    wf.StartPosition = FormStartPosition.CenterParent;
                    LoadAddressInsurer();
                    wf.InvokeCloseDispose();//閉じる
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
         //       wf.InvokeCloseDispose();
            }
            //20211012141425 furukawa ed ////////////////////////
        }
    }
}
