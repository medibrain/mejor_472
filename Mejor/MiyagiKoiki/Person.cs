﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Mejor.MiyagiKoiki
{
    class Person
    {
        [DB.DbAttribute.PrimaryKey]
        public string InsdNum { get; set; } = string.Empty;
        public string InsNum { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string DestName { get; set; } = string.Empty;

        public static bool Import()
        {
            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Title = "宮城広域提供 被保険者情報取り込み";
                f.Filter = "CSVファイル|*.csv";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();

                wf.LogPrint("すでに登録された被保険者情報を取得しています");
                var pl = DB.Main.SelectAll<Person>();
                var dic = new Dictionary<string, Person>();
                foreach (var item in pl) dic.Add(item.InsdNum, item);
                wf.LogPrint($"{pl.Count()}件の登録済み被保険者情報を取得しました");

                wf.LogPrint("CSVを読み込んでいます");
                var l = new List<Person>();
                var csv = CommonTool.CsvImportUTF8(fileName);

                for (int i = 1; i < csv.Count; i++)
                {
                    if (wf.Cancel)
                    {
                        var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Asterisk);
                        if (cres == DialogResult.Yes) return false;
                        wf.Cancel = false;
                    }
                    var item = csv[i];

                    if (item.Length < 6) continue;
                    var p = new Person();
                    p.InsNum = item[0].Trim();
                    p.InsdNum = item[1].Trim();
                    p.Name = item[2].Trim();
                    p.Zip = item[3].Trim();
                    p.Address = item[4].Trim() + item[5].Trim() + item[6].Trim();
                    p.DestName = item[7].Trim();

                    if (string.IsNullOrEmpty(p.DestName))
                    {
                        p.Name = p.Name.Substring(0, p.Name.Length - 2);
                    }
                    else
                    {
                        p.Name = p.Name.Substring(1, p.Name.Length - 5);
                        p.DestName = p.DestName.Substring(0, p.DestName.Length - 2);
                    }

                    l.Add(p);
                }

                wf.LogPrint($"CSVより{l.Count}件の被保険者情報を取得しました");

                wf.LogPrint("データベースへ登録/更新を行ないます");
                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                int updateCount = 0;
                int insertCount = 0;

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        if (wf.Cancel)
                        {
                            var cres = MessageBox.Show("取り込み中のデータはすべて破棄されます。取り込みを中止してよろしいですか？", "",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Asterisk);
                            if (cres == DialogResult.Yes) return false;
                            wf.Cancel = false;
                        }

                        wf.InvokeValue++;
                        if (dic.ContainsKey(item.InsdNum))
                        {
                            if (dic[item.InsdNum].InsNum == item.InsNum &&
                               dic[item.InsdNum].Name == item.Name &&
                               dic[item.InsdNum].Zip == item.Zip &&
                               dic[item.InsdNum].Address == item.Address &&
                               dic[item.InsdNum].DestName == item.DestName)
                                continue;

                            if (!DB.Main.Update(item, tran))
                            {
                                MessageBox.Show("被保険者情報のインポートに失敗しました");
                                return false;
                            }
                            updateCount++;
                        }
                        else
                        {
                            if (!DB.Main.Insert(item, tran))
                            {
                                MessageBox.Show("被保険者情報のインポートに失敗しました");
                                return false;
                            }
                            insertCount++;
                        }
                    }
                    tran.Commit();
                }

                MessageBox.Show(
                    $"新規登録：{insertCount}件\r\n" +
                    $"更新：{updateCount}件\r\n\r\n" +
                    $"被保険者情報のインポートが完了しました");
                return true;
            }
        }

        public static Person GetPerson(string insdNum) =>
            DB.Main.Select<Person>(new { InsdNum = insdNum }).FirstOrDefault();

    }
}
