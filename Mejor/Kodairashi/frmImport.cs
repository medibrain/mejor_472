﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.Kodairashi
{
    public partial class frmImport : Form
    {

        public  string strFileBaseData { get; set; } = string.Empty;
        public  string strFileKyufuData { get; set; } = string.Empty;
        public  string strFileAHKRyouyouhiData { get; set; } = string.Empty;

        private  static int _cym;

        public frmImport(int cym)
        {
            InitializeComponent();
            _cym = cym;

        }
        
        

        private void btnFile1_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "柔整イメージデータ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;
            textBoxBase.Text= ofd.FileName;
            
        }

        private void btnFile2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "給付記録データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxKyufu.Text= ofd.FileName;
        }

        private void btnFile3_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Excelファイル|*.xlsx";
            ofd.FilterIndex = 0;
            ofd.Title = "あはき療養費データ";
            ofd.ShowDialog();
            if (ofd.FileName == string.Empty) return;

            textBoxAHK.Text = ofd.FileName;
        }

        private void btnImpJyusei_Click(object sender, EventArgs e)
        {
            strFileBaseData = textBoxBase.Text.Trim();
            strFileKyufuData = textBoxKyufu.Text.Trim();
            strFileAHKRyouyouhiData = textBoxAHK.Text.Trim();
                
            this.Close();
        }

    }
}
