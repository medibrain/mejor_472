﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace Mejor.Kodairashi
{
    class RefRece
    {
        [DB.DbAttribute.PrimaryKey]
        public string Numbering { get; set; }
        public string ReceKey { get; set; }
        public int ChargeYM { get; set; }

        [DB.DbAttribute.PrimaryKey]
        public int MediYM { get; set; }
        public string HihoNum { get; set; }
        public int Sex { get; set; }
        public DateTime Birth { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public int Days { get; set; }
        public int Total { get; set; }
        public int Partial { get; set; }
        public int Charge { get; set; }
        public int Ratio { get; set; }
        public NEW_CONT NewContType { get; set; }
        public string DrNum { get; set; }
        public string DrName { get; set; }

        public static bool Import()
        {
            var fileName = string.Empty;
            using (var f = new OpenFileDialog())
            {
                f.Filter = "Mejor紐づけ用データ|Mejor紐づけ用データ.xlsx;Mejor紐づけ用データ.xls";
                if (f.ShowDialog() != DialogResult.OK) return true;
                fileName = f.FileName;
            }

            using (var wf = new WaitForm())
            {
                wf.ShowDialogOtherTask();
                var l = new List<RefRece>();
                wf.LogPrint("Excelファイルを読み込んでいます");

                using (var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    //国保、退職、それぞれのシートから読み込み
                    var ex = System.IO.Path.GetExtension(fileName);

                    if (ex == ".xls")
                    {
                        var xls = new HSSFWorkbook(fs);

                        var sheet = xls.GetSheet("国保");
                        l.AddRange(sheetToRefReces(sheet));

                        sheet = xls.GetSheet("退職");
                        l.AddRange(sheetToRefReces(sheet));
                    }
                    else if (ex == ".xlsx")
                    {
                        var xlsx = new XSSFWorkbook(fs);

                        var sheet = xlsx.GetSheet("国保");
                        l.AddRange(sheetToRefReces(sheet));

                        sheet = xlsx.GetSheet("退職");
                        l.AddRange(sheetToRefReces(sheet));
                    }
                    else
                    {
                        throw new Exception("Excelファイルの形式が拡張子より判別できませんでした");
                    }
                }

                wf.SetMax(l.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.LogPrint("データベースに登録しています");

                using (var tran = DB.Main.CreateTransaction())
                {
                    foreach (var item in l)
                    {
                        wf.InvokeValue++;
                        if (!DB.Main.Insert(item))
                        {
                            MessageBox.Show("インポートに失敗しました");
                            return false;
                        }
                    }
                    tran.Commit();
                }
            }

            MessageBox.Show("インポートが終了しました");
            return true;

        }

        private static List<RefRece> sheetToRefReces(ISheet sheet)
        {
            var l = new List<RefRece>();
            var rowCount = sheet.LastRowNum + 1;

            for (int i = 0; i < rowCount; i++)
            {
                try
                {
                    var row = sheet.GetRow(i);
                    if (row.GetCell(1).CellType != CellType.Numeric) continue;

                    var rr = new RefRece();
                    rr.Numbering = row.GetCell(7).StringCellValue;          //帳票イメージ番号
                    rr.ReceKey = row.GetCell(6).StringCellValue;            //レセプト全国共通キー

                    int.TryParse(row.GetCell(5).StringCellValue, out int cym);  //処理年月
                    rr.ChargeYM = DateTimeEx.GetAdYearMonthFromJyymm(cym);

                    rr.MediYM = DateTimeEx.GetAdYearMonthFromJyymm((int)row.GetCell(8).NumericCellValue);       // 施術年月


                    var mark = Utility.ToHalfWithoutKatakana(row.GetCell(9).StringCellValue);//被保険者証記号
                    var number = Utility.ToHalfWithoutKatakana(row.GetCell(10).StringCellValue);//被保険者証番号
                    rr.HihoNum = (mark + "・" + number);

                    rr.Sex = row.GetCell(11).StringCellValue == "2" ? 2 : 1;                //性別
                    rr.Birth = DateTimeEx.GetDateFromJInt7((int)row.GetCell(12).NumericCellValue);
                    rr.StartDate = DateTimeEx.GetDateFromJInt7((int)row.GetCell(14).NumericCellValue);
                    rr.FinishDate = DateTimeEx.GetDateFromJInt7((int)row.GetCell(15).NumericCellValue);
                    rr.Days = (int)row.GetCell(16).NumericCellValue;
                    rr.Partial = (int)row.GetCell(18).NumericCellValue;
                    rr.Ratio = (int)row.GetCell(19).NumericCellValue / 10;
                    rr.Charge = (int)row.GetCell(20).NumericCellValue;
                    rr.Total = (int)row.GetCell(21).NumericCellValue;
                    rr.NewContType = row.GetCell(22).StringCellValue == "1" ? NEW_CONT.新規 : NEW_CONT.継続;
                    rr.DrNum = row.GetCell(23).StringCellValue;
                    rr.DrName = row.GetCell(24).StringCellValue;

                    l.Add(rr);
                }
                catch
                {
                    continue;
                }
            }
            return l;
        }

        public static RefRece Select(string numbering)
        {
            return DB.Main.Select<RefRece>(new { numbering = numbering }).SingleOrDefault();
        }
    }
}
