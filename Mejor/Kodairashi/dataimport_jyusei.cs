﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.Kodairashi
{
    /// <summary>
    /// 保険者から提供される　給付データテーブルクラス
    /// </summary>
    class dataimport_jyusei
    {

        #region テーブル構造

        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public string f000 { get;set; }=string.Empty;                            //シーケンス　メホール管理用;
        public string f001 { get; set; } = string.Empty;                         //ＮＯ;
        public string f002 { get; set; } = string.Empty;                         //保険者決定区分;
        public string f003 { get; set; } = string.Empty;                         //種類;
        public string f004imagenum { get; set; } = string.Empty;                 //帳票イメージ番号;
        public string f005sejutsuym { get; set; } = string.Empty;                //施術年月;
        public string f006hihomark { get; set; } = string.Empty;                 //被保険者証記号;
        public string f007hihonum { get; set; } = string.Empty;                  //被保険者証番号;
        public string f008pgender { get; set; } = string.Empty;                  //性別;
        public string f009pbirthday { get; set; } = string.Empty;                //生年月日;
        public string f010atenanum { get; set; } = string.Empty;                 //宛名番号;
        public string f011startdate1 { get; set; } = string.Empty;               //施術開始日;
        public string f012finishdate1 { get; set; } = string.Empty;              //施術終了日;
        public string f013counteddays { get; set; } = string.Empty;              //日数;
        public string f014total { get; set; } = string.Empty;                    //費用額;
        public string f015partial { get; set; } = string.Empty;                  //一部負担金;
        public string f016ratio { get; set; } = string.Empty;                    //給付;
        public string f017charge { get; set; } = string.Empty;                   //請求金額;
        public string f018decision { get; set; } = string.Empty;                 //決定金額;
        public string f019 { get; set; } = string.Empty;                         //初・再検加算含む;
        public string f020drnum { get; set; } = string.Empty;                    //施術師番号？;
        public int cym { get; set; } = 0;                                        //メホール請求年月;
        public int aid { get; set; } = 0;                                        //aid;
        public DateTime birthdayad { get; set; } = DateTime.MinValue;            //生年月日西暦;
        public DateTime startdate1ad { get; set; } = DateTime.MinValue;          //施術開始日西暦;
        public DateTime finishdate1ad { get; set; } = DateTime.MinValue;         //施術終了日西暦;
        public int sejutsuymad { get; set; } = 0;                                //施術年月西暦;"
        public string hihonum_narrow { get; set; } = string.Empty;       //被保険者証記号番号半角;

        #endregion 



        /// <summary>
        /// インポート
        /// </summary>
        /// <param name="_cym">処理年月</param>
        /// <returns></returns>
        public static bool Import(int _cym)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "CSVファイル|*.csv|すべてのファイル|*.*";
            ofd.FilterIndex = 0;
            ofd.Title = "小平市柔整 提供データ";
            ofd.ShowDialog();
            string fileName = ofd.FileName;
            if (fileName == string.Empty) return false;

            var l = CommonTool.CsvImportMultiCode(fileName);

            
            if (l == null) return false;

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.SetMax(l.Count-1);

            try
            {

                using (var tran = DB.Main.CreateTransaction())
                {
                    wf.LogPrint("取込中です");

                    foreach (var item in l)
                    {
                        //ヘッダの場合は飛ばす（保険者番号が数字かどうかで判断）
                        if (!int.TryParse(item[3], out int tmp)) continue;

                  
                        var d = new dataimport_jyusei();


                        d.f001 = item[0];                   //ＮＯ;
                        d.f002 = item[1];                   //保険者決定区分;
                        d.f003 = item[2];                   //種類;
                        d.f004imagenum = item[3];           //帳票イメージ番号;
                        d.f005sejutsuym = item[4];          //施術年月;
                        d.f006hihomark = item[5];           //被保険者証記号;
                        d.f007hihonum = item[6];            //被保険者証番号;
                        d.f008pgender = item[7];            //性別;
                        d.f009pbirthday = item[8];          //生年月日;
                        d.f010atenanum = item[9];           //宛名番号;
                        d.f011startdate1 = item[10];        //施術開始日;
                        d.f012finishdate1 = item[11];       //施術終了日;
                        d.f013counteddays = item[12];       //日数;
                        d.f014total = item[13];             //費用額;
                        d.f015partial = item[14];           //一部負担金;
                        d.f016ratio = item[15];             //給付;
                        d.f017charge = item[16];            //請求金額;
                        d.f018decision = item[17];          //決定金額;
                        d.f019 = item[18];                  //初・再検加算含む;
                        d.f020drnum = item[19];             //施術師番号？;


                        d.cym = _cym;
                        d.aid = 0;
                        
                        
                        //d.startdate1ad = item[24];
                        //d.finishdate1ad = item[25];
                        




                        //審査年月西暦;                       
                        //if (System.Text.RegularExpressions.Regex.IsMatch(d.shinsaym, "(令和|平成)[0-9]{2}年[0-9]{2}月"))
                        //{
                        //    string strshinsaymY = d.shinsaym.Substring(0, 4);//令和02年07月→令和02
                        //    string strshinsaymM = d.shinsaym.Substring(5, 2);//令和02年07月→07
                        //    d.shinsaymad = DateTimeEx.GetAdYear(strshinsaymY).ToString() + strshinsaymM;
                        //}
                        //else
                        //{
                        //    d.shinsaymad = "999999";
                        //}

                        //施術年月西暦;
                        if (System.Text.RegularExpressions.Regex.IsMatch(d.f005sejutsuym, "(令和|平成)[0-9]{2}年[0-9]{2}月"))
                        {
                            string strshinryoY = d.f005sejutsuym.Substring(0, 4);//令和02年07月→令和02
                            string strshinryoM = d.f005sejutsuym.Substring(5, 2);//令和02年07月→07
                            d.sejutsuymad = int.Parse(DateTimeEx.GetAdYear(strshinryoY).ToString() + strshinryoM);
                        }
                        else
                        {
                            d.sejutsuymad = 999999;
                        }


                        //受療者生年月日西暦(数字部分が前0ない場合があるので{1,2}にしてる);
                        if (System.Text.RegularExpressions.Regex.IsMatch(d.f009pbirthday, ".+[0-9]{1,2}年[0-9]{1,2}月[0-9]{1,2}日"))
                        {
                            string strPattern = @"(.+)([0-9]{1,2})年([0-9]{1,2})月([0-9]{1,2})日";
                            string strAdYear = System.Text.RegularExpressions.Regex.Replace(d.f009pbirthday, strPattern, @"$1$2").ToString();
                            int yearad = DateTimeEx.GetAdYear(strAdYear);

                            d.birthdayad = new DateTime( yearad,
                                int.Parse(System.Text.RegularExpressions.Regex.Replace(d.f009pbirthday, strPattern, @"$3")),
                                int.Parse(System.Text.RegularExpressions.Regex.Replace(d.f009pbirthday, strPattern, @"$4")));

                        }                        
                        else
                        {
                            d.birthdayad = DateTime.MinValue;
                        }


                        //被保険者証　記号番号半角
                        //記号があったらハイフン繋ぎ
                        d.hihonum_narrow =
                            d.f006hihomark != string.Empty ? 
                            $"{Microsoft.VisualBasic.Strings.StrConv(d.f006hihomark, Microsoft.VisualBasic.VbStrConv.Narrow)}-{Microsoft.VisualBasic.Strings.StrConv(d.f007hihonum, Microsoft.VisualBasic.VbStrConv.Narrow)}" :
                            $"{Microsoft.VisualBasic.Strings.StrConv(d.f007hihonum, Microsoft.VisualBasic.VbStrConv.Narrow)}";

                                

                        wf.LogPrint($"提供データ  被保険者証記号-番号:{d.f006hihomark}-{d.f007hihonum}");
                        wf.InvokeValue++;

                        if (!DB.Main.Insert(d, tran)) return false;
                        
                    }

                    tran.Commit();

                }

           

                System.Windows.Forms.MessageBox.Show("終了しました");
                return true;
            }
          
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(
                    System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;

            }
            finally
            {
                wf.Dispose();
            }
        }


      
        /// <summary>
        /// 給付データ抽出
        /// </summary>
        /// <param name="strwhere">抽出条件(whereいらんsql)</param>
        /// <returns></returns>
        public static List<dataimport_jyusei> select(string strwhere)
        {
            List<dataimport_jyusei> lst = new List<dataimport_jyusei>();
            var l=DB.Main.Select<dataimport_jyusei>(strwhere);
            foreach (var item in l) lst.Add(item);
            return lst;
        }



    }
}
