﻿namespace Mejor.kyokaikenpo_saitama
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelHnum = new System.Windows.Forms.Panel();
            this.verifyBoxHnumM = new Mejor.VerifyBox();
            this.verifyBoxHname = new Mejor.VerifyBox();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelOCR = new System.Windows.Forms.Label();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panelAcc = new System.Windows.Forms.Panel();
            this.buttonZipSearch = new System.Windows.Forms.Button();
            this.verifyBoxClinicAddress = new Mejor.VerifyBox();
            this.verifyBoxClinicZip = new Mejor.VerifyBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxAccountName = new Mejor.VerifyBox();
            this.verifyBoxAccountNum = new Mejor.VerifyBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.toolTipOCR = new System.Windows.Forms.ToolTip(this.components);
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelHnum.SuspendLayout();
            this.panelAcc.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(570, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 13;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(133, 35);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(191, 35);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 1;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(20, 10);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(107, 12);
            this.labelHnum.TabIndex = 9;
            this.labelHnum.Text = "被保険者記号・番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            this.label8.Visible = false;
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(21, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelHnum);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelOCR);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.panelAcc);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // panelHnum
            // 
            this.panelHnum.Controls.Add(this.verifyBoxHnumM);
            this.panelHnum.Controls.Add(this.verifyBoxHname);
            this.panelHnum.Controls.Add(this.verifyBoxHnum);
            this.panelHnum.Controls.Add(this.label5);
            this.panelHnum.Controls.Add(this.labelHnum);
            this.panelHnum.Location = new System.Drawing.Point(225, 3);
            this.panelHnum.Name = "panelHnum";
            this.panelHnum.Size = new System.Drawing.Size(700, 69);
            this.panelHnum.TabIndex = 7;
            // 
            // verifyBoxHnumM
            // 
            this.verifyBoxHnumM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnumM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnumM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnumM.Location = new System.Drawing.Point(22, 25);
            this.verifyBoxHnumM.Name = "verifyBoxHnumM";
            this.verifyBoxHnumM.NewLine = false;
            this.verifyBoxHnumM.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxHnumM.TabIndex = 10;
            this.verifyBoxHnumM.TextV = "";
            // 
            // verifyBoxHname
            // 
            this.verifyBoxHname.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHname.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHname.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxHname.Location = new System.Drawing.Point(266, 25);
            this.verifyBoxHname.Name = "verifyBoxHname";
            this.verifyBoxHname.NewLine = false;
            this.verifyBoxHname.Size = new System.Drawing.Size(260, 23);
            this.verifyBoxHname.TabIndex = 20;
            this.verifyBoxHname.TextV = "";
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(123, 25);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(111, 23);
            this.verifyBoxHnum.TabIndex = 11;
            this.verifyBoxHnum.TextV = "";
            this.verifyBoxHnum.Validated += new System.EventHandler(this.verifyBoxHnum_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(264, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "被保険者氏名";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(480, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 14;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelOCR
            // 
            this.labelOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelOCR.AutoSize = true;
            this.labelOCR.Location = new System.Drawing.Point(6, 695);
            this.labelOCR.Name = "labelOCR";
            this.labelOCR.Size = new System.Drawing.Size(29, 12);
            this.labelOCR.TabIndex = 18;
            this.labelOCR.Text = "OCR";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(100, 24);
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 2;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(156, 24);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 3;
            this.verifyBoxM.TextV = "";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 12;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(17, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // panelAcc
            // 
            this.panelAcc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelAcc.Controls.Add(this.buttonZipSearch);
            this.panelAcc.Controls.Add(this.verifyBoxClinicAddress);
            this.panelAcc.Controls.Add(this.verifyBoxClinicZip);
            this.panelAcc.Controls.Add(this.label1);
            this.panelAcc.Controls.Add(this.label4);
            this.panelAcc.Controls.Add(this.verifyBoxAccountName);
            this.panelAcc.Controls.Add(this.verifyBoxAccountNum);
            this.panelAcc.Controls.Add(this.label7);
            this.panelAcc.Controls.Add(this.label2);
            this.panelAcc.Location = new System.Drawing.Point(3, 561);
            this.panelAcc.Name = "panelAcc";
            this.panelAcc.Size = new System.Drawing.Size(800, 120);
            this.panelAcc.TabIndex = 10;
            // 
            // buttonZipSearch
            // 
            this.buttonZipSearch.Location = new System.Drawing.Point(107, 75);
            this.buttonZipSearch.Name = "buttonZipSearch";
            this.buttonZipSearch.Size = new System.Drawing.Size(84, 23);
            this.buttonZipSearch.TabIndex = 47;
            this.buttonZipSearch.TabStop = false;
            this.buttonZipSearch.Text = "住所帳（F5）";
            this.buttonZipSearch.UseVisualStyleBackColor = true;
            this.buttonZipSearch.Click += new System.EventHandler(this.buttonZipSearch_Click);
            // 
            // verifyBoxClinicAddress
            // 
            this.verifyBoxClinicAddress.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicAddress.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicAddress.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxClinicAddress.Location = new System.Drawing.Point(198, 77);
            this.verifyBoxClinicAddress.Name = "verifyBoxClinicAddress";
            this.verifyBoxClinicAddress.NewLine = false;
            this.verifyBoxClinicAddress.Size = new System.Drawing.Size(550, 23);
            this.verifyBoxClinicAddress.TabIndex = 46;
            this.verifyBoxClinicAddress.TextV = "";
            // 
            // verifyBoxClinicZip
            // 
            this.verifyBoxClinicZip.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxClinicZip.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxClinicZip.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxClinicZip.Location = new System.Drawing.Point(12, 77);
            this.verifyBoxClinicZip.Name = "verifyBoxClinicZip";
            this.verifyBoxClinicZip.NewLine = false;
            this.verifyBoxClinicZip.Size = new System.Drawing.Size(80, 23);
            this.verifyBoxClinicZip.TabIndex = 44;
            this.verifyBoxClinicZip.TextV = "";
            this.verifyBoxClinicZip.Leave += new System.EventHandler(this.verifyBoxClinicZip_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(199, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 45;
            this.label1.Text = "返戻先住所";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 12);
            this.label4.TabIndex = 43;
            this.label4.Text = "返戻先郵便番号";
            // 
            // verifyBoxAccountName
            // 
            this.verifyBoxAccountName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.verifyBoxAccountName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.verifyBoxAccountName.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccountName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccountName.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxAccountName.Location = new System.Drawing.Point(118, 28);
            this.verifyBoxAccountName.Name = "verifyBoxAccountName";
            this.verifyBoxAccountName.NewLine = false;
            this.verifyBoxAccountName.Size = new System.Drawing.Size(400, 23);
            this.verifyBoxAccountName.TabIndex = 42;
            this.verifyBoxAccountName.TextV = "";
            // 
            // verifyBoxAccountNum
            // 
            this.verifyBoxAccountNum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAccountNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxAccountNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxAccountNum.Location = new System.Drawing.Point(12, 28);
            this.verifyBoxAccountNum.Name = "verifyBoxAccountNum";
            this.verifyBoxAccountNum.NewLine = false;
            this.verifyBoxAccountNum.Size = new System.Drawing.Size(100, 23);
            this.verifyBoxAccountNum.TabIndex = 40;
            this.verifyBoxAccountNum.TextV = "";
            this.verifyBoxAccountNum.Validated += new System.EventHandler(this.verifyBoxAccountNum_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(119, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 12);
            this.label7.TabIndex = 41;
            this.label7.Text = "返戻先宛名（口座名義）";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 39;
            this.label2.Text = "口座番号";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 74);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1018, 480);
            this.scrollPictureControl1.TabIndex = 19;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelHnum.ResumeLayout(false);
            this.panelHnum.PerformLayout();
            this.panelAcc.ResumeLayout(false);
            this.panelAcc.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelHnum;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private System.Windows.Forms.Label labelOCR;
        private System.Windows.Forms.ToolTip toolTipOCR;
        private VerifyBox verifyBoxHnumM;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Panel panelAcc;
        private VerifyBox verifyBoxAccountName;
        private VerifyBox verifyBoxAccountNum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private VerifyBox verifyBoxClinicAddress;
        private VerifyBox verifyBoxClinicZip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private VerifyBox verifyBoxHname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonZipSearch;
    }
}