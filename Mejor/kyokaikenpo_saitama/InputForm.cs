﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.kyokaikenpo_saitama
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(100, 0);    
        

        //最下部に座標変更
        Point posAcc = new Point(100, 1800);
        //Point posAcc = new Point(100, 1300);

        Control[] ymControls, accControls;

        DataTable dtacc = new DataTable();


        /// <summary>
        /// 住所検索フォーム用
        /// </summary>
        private PostalCodeForm pcForm;
        private List<PostalCode> selectedHistory = new List<PostalCode>();
        private string prevTodofuken;
        private string prevSikutyoson;


        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            #region コントロールグループ化
            ymControls = new Control[] { verifyBoxY, verifyBoxM, verifyBoxHnum, verifyBoxHnumM, verifyBoxHname, };
            accControls = new Control[] { verifyBoxAccountNum, verifyBoxAccountName, verifyBoxClinicAddress, verifyBoxClinicZip, };

            
            #endregion

            #region 右側パネルのコントロールEnter時イベント追加
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);


          
                        //20201110165008 furukawa st ////////////////////////
                        //PastDataのロード方式に変更
           // CreateAccountDataList();
                        //20201110165008 furukawa ed ////////////////////////

       

            #region 左パネルのリスト
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            #endregion

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            panelHnum.Visible = false;            

            #region aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            #endregion

            focusBack(false);
        }

        #region リスト変更時、表示申請書を変更する
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }
        #endregion



        
                //20201110165052 furukawa st ////////////////////////
                //PastDataのロード方式に変更のため個々で作らない

        ///// <summary>
        ///// 口座名義取得
        ///// </summary>
        //private void CreateAccountDataList()
        //{
        //    DB.Command cmd = DB.Main.CreateCmd("select baccnumber,baccname from application where trim(baccname)<>'' " +
        //        "group by baccnumber,baccname order by baccnumber ");
        //    var l = cmd.TryExecuteReaderList();
        //    dtacc.Columns.Add("baccnum");
        //    dtacc.Columns.Add("baccname");

        //    for (int r = 0; r < l.Count; r++)
        //    {
        //        DataRow dr = dtacc.NewRow();
        //        dr["baccnum"] = l[r][0].ToString();
        //        dr["baccname"] = l[r][1].ToString();

        //        dtacc.Rows.Add(dr);

        //    }
        //    cmd.Dispose();
        //}

                //20201110165052 furukawa ed ////////////////////////

    
        #region テキストボックスにカーソルが入ったとき座標を変更
        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;

            if (ymControls.Contains(t)) 
            {
                scrollPictureControl1.ScrollPosition = posYM;                
            }
            else if (accControls.Contains(t))
            {
                //20201110155215 furukawa st ////////////////////////
                //拡大不要

                //scrollPictureControl1.Ratio = 0.8f;//先に倍率を変更しないと座標が適用されてない？
                //20201110155215 furukawa ed ////////////////////////

                scrollPictureControl1.ScrollPosition = posAcc;                
            }


            //住所欄のカーソル位置を最後に
            if (t == verifyBoxClinicAddress) verifyBoxClinicAddress.Select(verifyBoxClinicAddress.Text.Length, 0);
        }

        #endregion

        #region 登録処理
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }
        #endregion

        #region 登録ボタン
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }
        #endregion

        #region ショートカットキー
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
            else if (e.KeyCode == Keys.F5) buttonZipSearch.PerformClick();//20201112093003 furukawa F5で住所検索
                                                                          

        }
        #endregion

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, year < 1 );
            
            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || month>12);

            //被保険者番号
            string strverifyBoxHnum = verifyBoxHnum.Text.Trim();
            setStatus(verifyBoxHnum, strverifyBoxHnum.Length > 12);


            //被保険者記号
            string strverifyBoxHMark = verifyBoxHnumM.Text.Trim();
            setStatus(verifyBoxHnumM, strverifyBoxHMark.Length > 10);

            //被保険者名
            string strverifyBoxHname = verifyBoxHname.Text.Trim();


            //施術所郵便番号
            string strverifyBoxClinicZip = verifyBoxClinicZip.Text.Trim();
            setStatus(verifyBoxClinicZip, strverifyBoxClinicZip.Length > 7);


            //施術所住所
            string strverifyBoxClinicAdd = verifyBoxClinicAddress.Text.Trim();

            //口座名義
            string strverifyBoxAccName = verifyBoxAccountName.Text.Trim();

            //口座番号
            string strverifyBoxAccNum = verifyBoxAccountNum.Text.Trim();
            setStatus(verifyBoxAccountNum, strverifyBoxAccNum.Length > 10 || !int.TryParse(strverifyBoxAccNum,out int tmp));


            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }
        
            
            //appへ値の反映
            
            //施術年
            app.MediYear = year;

            //施術月
            app.MediMonth = month;


            //被保険者記号/番号
            app.HihoNum = $"{strverifyBoxHMark}-{strverifyBoxHnum}";

            //被保険者名
            app.HihoName = strverifyBoxHname;

            //施術所郵便番号
            app.ClinicZip = strverifyBoxClinicZip;

            //施術所住所
            app.ClinicAdd = strverifyBoxClinicAdd;

            //口座名義
            app.AccountName = strverifyBoxAccName;

            //口座番号
            app.AccountNumber = strverifyBoxAccNum;

            //20201130115010 furukawa st ////////////////////////
            //スキャンタイプをscanから取得            
            app.AppType = scan.AppType;
            //20201130115010 furukawa ed ////////////////////////


            return true;
        }




        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                resetInputData(app);
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //表示リセット
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setValues(app);
            }
            else
            {


                //OCRデータがあれば、部位のみ挿入
                if (!string.IsNullOrWhiteSpace(app.OcrData))
                {
                    var ocr = app.OcrData.Split(',');
              
                }
            }
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="app"></param>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    labelImageName.Text = fn + " )";
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //通常表示


                    //20201110160211 furukawa st ////////////////////////
                    //拡大率修正                    
                    scrollPictureControl1.Ratio = 0.4f;
                    //scrollPictureControl1.Ratio = 0.6f;
                    //20201110160211 furukawa ed ////////////////////////
                    scrollPictureControl1.SetImage(img, fn);
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                //被保険者番号
                var hn = app.HihoNum.Split('-');
                if (hn.Length == 2)
                {
                    setValue(verifyBoxHnumM, hn[0], firstTime, nv);
                    setValue(verifyBoxHnum, hn[1], firstTime, nv);
                }
                else
                {
                    setValue(verifyBoxHnumM, string.Empty, firstTime, nv);
                    setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);
                }

                //申請書               

                //診療和暦年
                setValue(verifyBoxY, app.MediYear, firstTime, nv);

                //診療和暦月
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);

                //被保険者名
                setValue(verifyBoxHname, app.HihoName.ToString(), firstTime, nv);

                //施術所郵便番号
                setValue(verifyBoxClinicZip, app.ClinicZip.ToString(), firstTime, nv);

                //施術所住所
                setValue(verifyBoxClinicAddress, app.ClinicAdd.ToString(), firstTime, nv);

                //口座名義
                setValue(verifyBoxAccountName, app.AccountName.ToString(), firstTime, nv);

                //口座番号
                setValue(verifyBoxAccountNum, app.AccountNumber.ToString(), firstTime, nv);

          
            }
        }

       

        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text.Length == 2 && 
                (verifyBoxY.Text == "--" || verifyBoxY.Text == "++"))
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelHnum.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
            }
            else
            {
                //申請書の場合
                verifyBoxM.Visible = true;
                labelM.Visible = true;
                panelHnum.Visible = true;

            }
        }

        #region 画像操作
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }
        #endregion

        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            focusBack(false);
        }

     

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymControls.Contains(t)) posYM = pos;
            
            else if (accControls.Contains(t)) posAcc = pos;
            

        }


        /// <summary>
        /// 口座番号入力後、口座名義選択画面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxAccountNum_Validated(object sender, EventArgs e)
        {

            //20201125135418 furukawa st ////////////////////////
            //口座名義は入れないレコードもあるので一旦止める


                    //20201110165148 furukawa st ////////////////////////
                    //PastDataのロード方式に変更のため個々で作らない




                    //現在のAppが1回目入力の場合のみ自動入力
                    //var app = (App)bsApp.Current;
                    //if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return;


                    //PastData.AccountData ad = new PastData.AccountData();
                    //ad = PastData.AccountData.SelectRecord($"baccnumber='{verifyBoxAccountNum.Text.Trim()}'");
                    //if (ad == null) return;
                    //verifyBoxAccountName.Text = ad.baccname;




                    #region old

                    //DataRow[] drs = dtacc.Select($"trim(baccnum)='{verifyBoxAccountNum.Text}'");
                    //if (drs.Count() == 0) return;

                    //SelectForm frm = new SelectForm(verifyBoxAccountName);

                    //foreach (DataRow dr in drs)
                    //{
                    //    frm.listBox.Items.Add(dr["baccname"].ToString());
                    //}

                    //frm.ShowDialog();
                    #endregion


                    //20201110165148 furukawa ed ////////////////////////


            //20201125135418 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 郵便番号検索ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonZipSearch_Click(object sender, EventArgs e)
        {
            if (pcForm == null || pcForm.IsDisposed)
            {
                void setPostal(PostalCode pc)
                {
                    if (pc == null) return;
                    verifyBoxClinicZip.Text = pc.Postal_Code.ToString("0000000");
                    verifyBoxClinicAddress.Text = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    prevTodofuken = pc.Todofuken;
                    prevSikutyoson = pc.Sichokuson;
                    verifyBoxClinicAddress.Focus();
                    verifyBoxClinicAddress.Select(verifyBoxClinicAddress.Text.Length, 0);
                    selectedHistory.Add(pc);
                }

                var x = panelRight.Location.X + panelAcc.Location.X - PostalCodeForm.FORM_WIDTH;
                var y = this.Height - 820;
                pcForm = new PostalCodeForm(setPostal, selectedHistory, x, y, prevTodofuken, prevSikutyoson);
            }
            if (!pcForm.Visible) pcForm.Show(this);
            else pcForm.MoveDefaultFocus();
        }

        /// <summary>
        /// 郵便番号入力後住所の自動入力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxClinicZip_Leave(object sender, EventArgs e)
        {
            //郵便番号と住所入力欄があれば自動で設定する
            if (int.TryParse(verifyBoxClinicZip.Text, out int zipcode))
            {
                var pc = PostalCode.GetPostalCode(zipcode);
                if (pc != null)
                {
                    var add = $"{pc.Todofuken}{pc.Sichokuson}{pc.Choiki}";
                    var curAdd = verifyBoxClinicAddress.Text;
                    if (!curAdd.StartsWith(add)) verifyBoxClinicAddress.Text = add;
                }
                else verifyBoxClinicAddress.Text = "";
            }
        }

        private void verifyBoxHnum_Validated(object sender, EventArgs e)
        {
            //20201125144750 furukawa st ////////////////////////
            //ベリファイ時は出さない
            
            var app = (App)bsApp.Current;
            if (app == null) return;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime) return;
            //20201125144750 furukawa ed ////////////////////////


            PastData.PersonalData data = new PastData.PersonalData();

            //20201125114712 furukawa st ////////////////////////
            //選択リストを出したくないので1行のみ抽出
            
            data = PastData.PersonalData.SelectRecordOne($"hnum='{verifyBoxHnumM.Text.Trim()}-{verifyBoxHnum.Text.Trim()}'");
            //data = PastData.PersonalData.SelectRecord($"hnum='{verifyBoxHnumM.Text.Trim()}-{verifyBoxHnum.Text.Trim()}'");
            //20201125114712 furukawa ed ////////////////////////

            if (data == null) return;
            verifyBoxHname.Text = data.hname;

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }
    }
}
