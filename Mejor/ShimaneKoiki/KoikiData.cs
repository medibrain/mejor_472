﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.ShimaneKoiki
{
    class KoikiData
    {
        public int Did { get; private set; }
        public int AppYM { get; private set; }
        public int MediYM { get; private set; }
        public string InsNum { get; private set; } = string.Empty;
        public string Num { get; private set; } = string.Empty;
        public string Name { get; private set; } = string.Empty;
        public string Kana { get; private set; } = string.Empty;
        public int AppType { get; private set; }
        public DateTime StartDate { get; private set; }
        public int Days { get; private set; }
        public int Total { get; private set; }
        public int Ratio { get; private set; }
        public int Seikyu { get; private set; }
        public int Futan { get; private set; }
        public string DrNum { get; private set; } = string.Empty;
        public string ReceNum { get; private set; } = string.Empty;
        public string Zip { get; private set; } = string.Empty;
        public string Add { get; private set; } = string.Empty;
        public string DestName { get; private set; } = string.Empty;
        public string DestKana { get; private set; } = string.Empty;
        public string DestZip { get; private set; } = string.Empty;
        public string DestAdd { get; private set; } = string.Empty;
        public int Aid { get; private set; }

        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
        public int MediHYM => DateTimeEx.GetHsYearFromAd(MediYM / 100, MediYM % 100) * 100 + MediYM % 100;
        public int MediHY => DateTimeEx.GetHsYearFromAd(MediYM / 100, MediYM % 100);
        //public int MediHYM => DateTimeEx.GetHsYearFromAd(MediYM / 100) * 100 + MediYM % 100;
        //public int MediHY => DateTimeEx.GetHsYearFromAd(MediYM / 100);
        //20190424191358_2019/04/22 GetHsYearFromAd令和対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

        public int MediM => MediYM % 100;

        public static bool Import(string fileName, int ady, int m)
        {
            using (var wf = new WaitForm())
            {
                wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;
                wf.ShowDialogOtherTask();
                wf.LogPrint("インポートを開始します");

                wf.LogPrint("CSVファイルを読み込み中です…");
                var l = CommonTool.CsvImportUTF8(fileName);

                wf.LogPrint("データベースへ書き込み中です…");
                wf.SetMax(l.Count - 1);

                var sql = "INSERT INTO koikidata( " +
                    "appym, mediym, insnum, num, name, kana, apptype, startdate, " +
                    "days, total, ratio, seikyu, futan, drnum, recenum, zip, add, " +
                    "destname, destkana, destzip, destadd, aid) " +
                    "VALUES( " +
                    ":appym, :mediym, :insnum, :num, :name, :kana, :apptype, :startdate, " +
                    ":days, :total, :ratio, :seikyu, :futan, :drnum, :recenum, :zip, :add, " +
                    ":destname, :destkana, :destzip, :destadd, :aid);";

                using (var tran = DB.Main.CreateTransaction())
                using (var cmd = DB.Main.CreateCmd(sql, tran))
                {
                    cmd.Parameters.Add("appym", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("mediym", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("insnum", NpgsqlDbType.Text);
                    cmd.Parameters.Add("num", NpgsqlDbType.Text);
                    cmd.Parameters.Add("name", NpgsqlDbType.Text);
                    cmd.Parameters.Add("kana", NpgsqlDbType.Text);
                    cmd.Parameters.Add("apptype", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("startdate", NpgsqlDbType.Date);
                    cmd.Parameters.Add("days", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("total", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("ratio", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("seikyu", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("futan", NpgsqlDbType.Integer);
                    cmd.Parameters.Add("drnum", NpgsqlDbType.Text);
                    cmd.Parameters.Add("recenum", NpgsqlDbType.Text);
                    cmd.Parameters.Add("zip", NpgsqlDbType.Text);
                    cmd.Parameters.Add("add", NpgsqlDbType.Text);
                    cmd.Parameters.Add("destname", NpgsqlDbType.Text);
                    cmd.Parameters.Add("destkana", NpgsqlDbType.Text);
                    cmd.Parameters.Add("destzip", NpgsqlDbType.Text);
                    cmd.Parameters.Add("destadd", NpgsqlDbType.Text);
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer);

                    try
                    {
                        //取込開始 ヘッダなし
                        for (int i = 0; i < l.Count; i++)
                        {
                            wf.InvokeValue = i;
                            if (l[i].Length < 11) continue;

                            int mym, atype, days, total, ratio, seikyu, futan;
                            int.TryParse(l[i][6], out mym);
                            int.TryParse(l[i][3], out atype);
                            int.TryParse(l[i][8], out days);
                            int.TryParse(l[i][9], out ratio);
                            int.TryParse(l[i][10], out total);
                            int.TryParse(l[i][11], out seikyu);
                            int.TryParse(l[i][12], out futan);
                            mym = DateTimeEx.GetAdYearMonthFromJyymm(mym);
                            DateTime sdt = DateTimeEx.GetDateFromJstr7(l[i][7].Trim());

                            cmd.Parameters["appym"].Value = ady * 100 + m;
                            cmd.Parameters["mediym"].Value = mym;
                            cmd.Parameters["insnum"].Value = l[i][0].Trim();
                            cmd.Parameters["num"].Value = l[i][1].Trim();
                            cmd.Parameters["name"].Value = l[i][13].Trim();
                            cmd.Parameters["kana"].Value = l[i][14].Trim();
                            cmd.Parameters["apptype"].Value = atype;
                            cmd.Parameters["startdate"].Value = sdt;
                            cmd.Parameters["days"].Value = days;
                            cmd.Parameters["total"].Value = total;
                            cmd.Parameters["ratio"].Value = ratio;
                            cmd.Parameters["seikyu"].Value = seikyu;
                            cmd.Parameters["futan"].Value = futan;
                            cmd.Parameters["drnum"].Value = l[i][2] + l[i][4];
                            cmd.Parameters["recenum"].Value = l[i][5].Trim();
                            cmd.Parameters["zip"].Value = l[i][16].Trim();
                            cmd.Parameters["add"].Value = l[i][17].Trim() + l[i][18].Trim() + l[i][19].Trim();
                            cmd.Parameters["destname"].Value = l[i][24].Trim();
                            cmd.Parameters["destkana"].Value = l[i][25].Trim();
                            cmd.Parameters["destzip"].Value = l[i][20].Trim();
                            cmd.Parameters["destadd"].Value = l[i][21].Trim() + l[i][22].Trim() + l[i][23].Trim();
                            cmd.Parameters["aid"].Value = 0;

                            if (!cmd.TryExecuteNonQuery())
                            {
                                tran.Rollback();
                                return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                        System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                        tran.Rollback();
                        return false;
                    }

                    tran.Commit();
                }

                System.Windows.Forms.MessageBox.Show("インポートが完了しました");
                return true;
            }
        }

        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <returns></returns>
        public static List<KoikiData> SerchByInput(int appAdY, int appM, int adY, int m, string hnum, int total)
        {
            var sql = "SELECT " +
                "did, appym, mediym, insnum, num, name, kana, apptype, startdate, " +
                "days, total, ratio, seikyu, futan, drnum, recenum, zip, add, " +
                "destname, destkana, destzip, destadd, aid " +
                "FROM koikidata " +
                "WHERE num=:num AND mediym=:mediym AND total=:total AND appym=:appym;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("num", NpgsqlDbType.Text).Value = hnum;
                cmd.Parameters.Add("mediym", NpgsqlDbType.Integer).Value = adY * 100 + m;
                cmd.Parameters.Add("total", NpgsqlDbType.Integer).Value = total;
                cmd.Parameters.Add("appym", NpgsqlDbType.Integer).Value = appAdY * 100 + appM;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;
                var list = new List<KoikiData>();

                foreach (var item in res)
                {
                    var dat = createDataFromSQL_Record(item);
                    list.Add(dat);
                }

                return list;
            }
        }

        /// <summary>
        /// sqlから出たデータを整理し、クラスインスタンスを作成します
        /// </summary>
        /// <param name="objs"></param>
        /// <returns></returns>
        private static KoikiData createDataFromSQL_Record(object[] objs)
        {
            var dat = new KoikiData();
            dat.Did = (int)objs[0];
            dat.AppYM = (int)objs[1];
            dat.MediYM = (int)objs[2];
            dat.InsNum = (string)objs[3];
            dat.Num = (string)objs[4];
            dat.Name = (string)objs[5];
            dat.Kana = (string)objs[6];
            dat.AppType = (int)objs[7];
            dat.StartDate = (DateTime)objs[8];
            dat.Days = (int)objs[9];
            dat.Total = (int)objs[10];
            dat.Ratio = (int)objs[11];
            dat.Seikyu = (int)objs[12];
            dat.Futan = (int)objs[13];
            dat.DrNum = (string)objs[14];
            dat.ReceNum = (string)objs[15];
            dat.Zip = (string)objs[16];
            dat.Add = (string)objs[17];
            dat.DestName = (string)objs[18];
            dat.DestKana = (string)objs[19];
            dat.DestZip = (string)objs[20];
            dat.DestAdd = (string)objs[21];
            dat.Aid = (int)objs[22];
            return dat;
        }

        /// <summary>
        /// koikidataテーブル中のdidに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int did, int aid, DB.Transaction tran = null)
        {
            if (tran == null)
            {
                using (tran = DB.Main.CreateTransaction())
                using (var dcmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=:aid WHERE did=:did;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("did", NpgsqlDbType.Integer).Value = did;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            tran.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    tran.Rollback();
                    return false;
                }
            }
            else
            {
                using (var dcmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=:aid WHERE did=:did;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("did", NpgsqlDbType.Integer).Value = did;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// koikidataテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        public static List<KoikiData> GetNotMatchDatas(int cym)
        {
            var sql = "SELECT " +
                "did, appym, mediym, insnum, num, name, kana, apptype, startdate, " +
                "days, total, ratio, seikyu, futan, drnum, recenum, zip, add, " +
                "destname, destkana, destzip, destadd, aid " +
                "FROM koikidata " +
                "WHERE appym=:appym AND aid=0;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("appym", NpgsqlDbType.Integer).Value = cym;


                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;
                var list = new List<KoikiData>();

                foreach (var item in res)
                {
                    var dat = createDataFromSQL_Record(item);
                    list.Add(dat);
                }

                return list;
            }
        }

        public static KoikiData Select(int did)
        {
            var sql = "SELECT " +
                "did, appym, mediym, insnum, num, name, kana, apptype, startdate, " +
                "days, total, ratio, seikyu, futan, drnum, recenum, zip, add, " +
                "destname, destkana, destzip, destadd, aid " +
                "FROM koikidata " +
                "WHERE did=:did;";

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("did", NpgsqlDbType.Integer).Value = did;

                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count != 1) return null;
                return createDataFromSQL_Record(res[0]);
            }
        }

        public static List<KoikiData> SelectAym(int cym)
        {
            var sql = "SELECT " +
                "did, appym, mediym, insnum, num, name, kana, apptype, startdate, " +
                "days, total, ratio, seikyu, futan, drnum, recenum, zip, add, " +
                "destname, destkana, destzip, destadd, aid " +
                "FROM koikidata " +
                "WHERE appym=:appym;";

            var l = new List<KoikiData>();

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                cmd.Parameters.Add("appym", NpgsqlDbType.Integer).Value = cym;

                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                foreach (var item in res)
                {
                    l.Add(createDataFromSQL_Record(item));
                }
            }
            return l;
        }

        public static List<KoikiData> SelectAll()
        {
            var sql = "SELECT " +
                "did, appym, mediym, insnum, num, name, kana, apptype, startdate, " +
                "days, total, ratio, seikyu, futan, drnum, recenum, zip, add, " +
                "destname, destkana, destzip, destadd, aid " +
                "FROM koikidata;";

            var l = new List<KoikiData>();

            using (var cmd = DB.Main.CreateCmd(sql))
            {
                var res = cmd.TryExecuteReaderList();
                if (res == null) return null;

                foreach (var item in res)
                {
                    l.Add(createDataFromSQL_Record(item));
                }
            }
            return l;
        }


        public static void AutoMatching(int chy, int cm)
        {
            

        }
    }
}
