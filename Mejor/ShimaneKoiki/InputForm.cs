﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using NpgsqlTypes;

namespace Mejor.ShimaneKoiki
{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;


        /// <summary>
        /// レセタイプ変数
        /// </summary>
        private APP_TYPE currentAppType;
        


        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        //point(横位置,縦位置);
        Point posYM = new Point(0, 0);
        Point posHosCode = new Point(400, 1900);
        Point posHnum = new Point(400, 0);
        
        Point posFusho = new Point(100, 300);
        Point posCost = new Point(400, 1900);
        Point posDays = new Point(400, 300);
        Point posNumbering = new Point(300, 0);

        //20190920163952 furukawa st ////////////////////////
        //医療機関コード、開始終了日、同意年月日等位置
        
        Point posIryoKikan = new Point(100, 0);
        Point posStEd = new Point(100, 800);
        Point posDoui=new Point(200,1900);
        //20190920163952 furukawa ed ////////////////////////



        Control[] ymControls, hnumControls,  dayControls, costControls,
            fushoControls1, hosControls,               
            IryoKikanControls,StEdControls,DouiControls;//20190920160100 furukawa 医療機関コード、開始終了日、同意年月日等追加


        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();

            ymControls = new Control[] { verifyBoxY, verifyBoxM };                  //施術年月
            hnumControls = new Control[] { verifyBoxHnum, };                        //被保険者番号
            dayControls = new Control[] { verifyBoxDays, };                         //診療実日数
            costControls = new Control[] { verifyBoxTotal, verifyBoxCharge,  };     //合計金額、請求金額
            fushoControls1 = new Control[] { verifyBoxF1Y, verifyBoxF1M, verifyBoxFushoCount, };//初検、負傷数
            hosControls = new Control[] { verifyBoxDrCode, };                       //柔整師登録番号
            

            //20190920160853 furukawa st ////////////////////////
            //医療機関コード、開始終了日、同意年月日等追加
            
            IryoKikanControls = new Control[] { verifyBoxIryokikan1, verifyBoxIryokikan2, };            //医療機関コード

            //20191206123351 furukawa st ////////////////////////
            //診療実日数も開始日終了日と同じグループにして座標を同じくする
            
            StEdControls = new Control[] { verifyBoxStart,verifyBoxFinish, verifyBoxDays, };             //開始日終了日、診療実日数
            //StEdControls = new Control[] { verifyBoxStart, verifyBoxFinish, };                           //開始日終了日
            //20191206123351 furukawa ed ////////////////////////


            DouiControls = new Control[] { checkBoxVisitKasan, vbDouiG,vbDouiY,vbDouiM,vbDouiD,};       //同意年月日、往療加算
            //20190920160853 furukawa ed ////////////////////////

            

            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if (item is TextBox)
                    {
                        item.Enter += item_Enter;
                    }
                    func(item);
                }
            });
            func(panelRight);
            
            
            //20190920164442 furukawa st ////////////////////////
            //各項目にイベント割当
     
            //func(pDoui);
            
            //20190920164442 furukawa ed ////////////////////////

           
            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = App.GetAppsGID(this.scanGroup.GroupID);

            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }

            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.HihoPref)].HeaderText = "県";
            dataGridViewPlist.Columns[nameof(App.Sex)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Sex)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.Sex)].HeaderText = "性";
            dataGridViewPlist.Columns[nameof(App.Birthday)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Birthday)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Birthday)].HeaderText = "生年月日";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 50;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "ナンバリング";
            dataGridViewPlist.Columns[nameof(App.Numbering)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            if (!firstTime) this.Text += "ベリファイ入力モード";

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //上の入力欄
            panelUpper.Visible = false;
            //下の入力欄
            panelTotal.Visible = false;
            //施術月
            verifyBoxM.Visible = false;
            labelM.Visible = false;
        
            

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
            verifyBoxY.Focus();

        }



        //20190910174221 furukawa st ////////////////////////
        //申請書種別で入力項目変更
        
        private void EnableControl(APP_TYPE mode)
        {
            

            //柔整で使用

            //負傷数
            labelFusyoCnt.Visible = mode == APP_TYPE.柔整 ? true : false;
            verifyBoxFushoCount.Visible = mode == APP_TYPE.柔整 ? true : false;

            //柔整師登録番号
            lblDrCode.Visible = mode == APP_TYPE.柔整 ? true : false;
            verifyBoxDrCode.Visible = mode == APP_TYPE.柔整 ? true : false;
            lblDrCode01.Visible = mode == APP_TYPE.柔整 ? true : false;




            //20190920161747 furukawa st ////////////////////////
            //あはき専用コントロール制御


            //医療機関コード１，２
            lblHosCode1.Visible = mode == APP_TYPE.柔整 ? false : true;
            lblHosCode2.Visible  = mode == APP_TYPE.柔整 ? false : true;            
            verifyBoxIryokikan1.Visible = mode == APP_TYPE.柔整 ? false : true;
            verifyBoxIryokikan2.Visible = mode == APP_TYPE.柔整 ? false : true;

            //保険者番号
            lblinum.Visible = mode == APP_TYPE.柔整 ? false : true;
            lblinumhead.Visible = mode == APP_TYPE.柔整 ? false : true;
            verifyBoxinum.Visible = mode == APP_TYPE.柔整 ? false : true;

            //給付割合
            lblRatio.Visible = mode == APP_TYPE.柔整 ? false : true;
            lblRatioNum.Visible = mode == APP_TYPE.柔整 ? false : true;
            verifyBoxKyufuRate.Visible = mode == APP_TYPE.柔整 ? false : true;


            //開始終了日
            lblstart.Visible = mode == APP_TYPE.柔整 ? false : true;
            lblfinish.Visible = mode == APP_TYPE.柔整 ? false : true;
            verifyBoxStart.Visible = mode == APP_TYPE.柔整 ? false : true;
            verifyBoxFinish.Visible = mode == APP_TYPE.柔整 ? false : true;


            //20191001142827 furukawa st ////////////////////////
            //診療実日数は両方使う
            
            //診療実日数
            //labelDays.Visible = mode == APP_TYPE.柔整 ? false : true;
            //verifyBoxDays.Visible = mode == APP_TYPE.柔整 ? false : true;
            //20191001142827 furukawa ed ////////////////////////



            //加算あり
            checkBoxVisitKasan.Visible = mode == APP_TYPE.柔整 ? false : true;

            //同意年月日
            pDoui.Visible = mode == APP_TYPE.柔整 ? false : true;


            //20190920161747 furukawa ed ////////////////////////


        }

        //20190910174221 furukawa ed ////////////////////////




        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
                        
            //レセタイプ判定            
            currentAppType = scanGroup.AppType;
            
            setApp(app);
            focusBack(false);
        }

        void item_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;
            if (t.BackColor == SystemColors.Info) t.BackColor = Color.LightCyan;

            if (ymControls.Contains(t)) scrollPictureControl1.ScrollPosition = posYM;
            else if (hnumControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHnum;

            //20191206124145 furukawa st ////////////////////////
            //開始終了日グループでやるので不要
            //else if (dayControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDays;
            //20191206124145 furukawa ed ////////////////////////

            else if (costControls.Contains(t)) scrollPictureControl1.ScrollPosition = posCost;
            else if (fushoControls1.Contains(t)) scrollPictureControl1.ScrollPosition = posFusho;
            else if (hosControls.Contains(t)) scrollPictureControl1.ScrollPosition = posHosCode;

            //20190920164253 furukawa st ////////////////////////
            //医療機関コード、開始終了日、同意年月日等位置            
            else if (IryoKikanControls.Contains(t)) scrollPictureControl1.ScrollPosition = posIryoKikan;
            else if (StEdControls.Contains(t)) scrollPictureControl1.ScrollPosition = posStEd;
            else if (DouiControls.Contains(t)) scrollPictureControl1.ScrollPosition = posDoui;

            //20190920164253 furukawa ed ////////////////////////

        }

        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        private void buttonRegist_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        /// <summary>
        /// 入力チェック：バッジ
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkBatch(App app)
        {
            hasError = false;

            ////バッチ番号
            //setStatus(verifyBoxBatch, verifyBoxBatch.Text.Trim().Length != Common.BatchLength);

            ////口座番号
            //setStatus(verifyBoxBank, verifyBoxBank.Text.Trim().Length != 7);

            ////合計枚数
            //int count = verifyBoxCount.GetIntValue();
            //setStatus(verifyBoxCount, count < 1);

            ////鍼灸マッサージ時、2～5ケタまたは10桁 記入ない場合は++
            //var drNumber = string.Empty;
            //if (scan.AppType == APP_TYPE.鍼灸 || scan.AppType == APP_TYPE.あんま
            //        || Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_23AICHI
            //        || Insurer.CurrrentInsurer.EnumInsID == InsurerID.GAKKO_11SAITAMA)
            //{
            //    drNumber = verifyBoxShinkyuDr.Text.Trim();
            //    setStatus(verifyBoxShinkyuDr,
            //        drNumber != "++" && !(2 <= drNumber.Length && drNumber.Length <= 5) && drNumber.Length != 10);
            //}

            //if (hasError)
            //{
            //    MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
            //        "赤で示されたデータをご確認ください。登録できません。\r\n",
            //        "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            //    return false;
            //}

            app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
            app.MediMonth = 0;
            app.HihoNum = string.Empty;
            app.Family = 0;
            app.HihoType = 0;
            app.Sex = 0;
            app.Birthday = DateTime.MinValue;
            
            //app.CountedDays = count;

            app.Total = 0;
            app.Charge = 0;
            app.Partial = 0;

            //app.DrNum = drNumber;
            //app.Numbering = verifyBoxBatch.Text.Trim();
            //app.AccountNumber = verifyBoxBank.Text.Trim();
            app.AppType = APP_TYPE.バッチ;
            return true;
        }

        /// <summary>
        /// 入力チェック：申請書
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool checkApp(App app)
        {
            hasError = false;
            
            #region 入力チェック
            //柔整あはき共通

            //年
            int sejutuYear = verifyBoxY.GetIntValue();
            setStatus(verifyBoxY, sejutuYear < 1 || 31 < sejutuYear);

            //月
            int sejutuMonth = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, sejutuMonth < 1 || 12 < sejutuMonth);

            //被保険者番号 2文字以上かつ英数字
            var r = Regex.IsMatch(verifyBoxHnum.Text, "^[0-9A-Z]{2,}$");
            setStatus(verifyBoxHnum, !r);

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //請求金額
            int charge = verifyBoxCharge.GetIntValue();
            setStatus(verifyBoxCharge, charge < 10 | total < charge);

            //請求金額が合計金額の7～10割の金額と合致すればOK
            bool payError = false;

            if ((int)(total * 70 / 100) != charge &&
                (int)(total * 80 / 100) != charge &&
                (int)(total * 90 / 100) != charge &&
                (int)(total * 100 / 100) != charge)
            {
                payError = true;
            }

            //診療日数
            int sinryoDays = verifyBoxDays.GetIntValue();
            setStatus(verifyBoxDays, sinryoDays < 1 || 31 < sinryoDays);


            //初検年月
            DateTime shoken = DateTime.MinValue;
            int sY = verifyBoxF1Y.GetIntValue();
            int sM = verifyBoxF1M.GetIntValue();
        
            sY = DateTimeEx.GetAdYearFromHs(sY * 100 + sM);

            if (!DateTimeEx.IsDate(sY, sM, 1))
            {
                setStatus(verifyBoxF1Y, true);
                setStatus(verifyBoxF1M, true);
            }
            else
            {
                setStatus(verifyBoxF1Y, false);
                setStatus(verifyBoxF1M, false);
                shoken = new DateTime(sY, sM, 1);
            }
            


            
            int intRatio = 0;//給付割合
            int stDate = verifyBoxStart.GetIntValue();//開始日
            int edDate = verifyBoxFinish.GetIntValue();//終了日

            string strClinicCode1 = verifyBoxIryokikan1.Text;//医療機関コード１
            string strClinicCode2 = verifyBoxIryokikan2.Text;//医療機関コード２


            //同意年月日用
            int douiG = vbDouiG.GetIntValue();
            int douiY = vbDouiY.GetIntValue();
            int douiM = vbDouiM.GetIntValue();
            int douiD = vbDouiD.GetIntValue();
            DateTime dtDoui = DateTime.MinValue;



            //あはきのみ
            if (scan.AppType == APP_TYPE.あんま ||
                scan.AppType == APP_TYPE.鍼灸)
            {

                //医療機関コード
                setStatus(verifyBoxIryokikan1, strClinicCode1.Length != 10);

                //医療機関コード2
                setStatus(verifyBoxIryokikan2, strClinicCode2.Length != 10);

                //給付割合
                int.TryParse(verifyBoxKyufuRate.Text,out intRatio);
                
                //20191004091247 furukawa st ////////////////////////
                //給付割合は７，９いずれか。１０や０はあり得ない 2019/10/03伸作さん                
                setStatus(verifyBoxKyufuRate, intRatio != 7 && intRatio!=9);
                        //setStatus(verifyBoxKyufuRate, intRatio < 7 || intRatio > 10);
                //20191004091247 furukawa ed ////////////////////////

                //開始日                
                setStatus(verifyBoxStart, stDate < 1 || stDate > 31);
                //終了日            
                setStatus(verifyBoxFinish, stDate < 1 || stDate > 31);

                //同意年月日

                //20191001120959 furukawa st ////////////////////////
                //平成未満をエラーとする                
                //setStatus(vbDouiG, douiG < 1 || douiG > 5);
                setStatus(vbDouiG, douiG < 4 || douiG > 5);
                //20191001120959 furukawa ed ////////////////////////


                setStatus(vbDouiY, douiY < 1 || douiY > 64);
                setStatus(vbDouiM, douiM < 1 || douiM > 12);
                setStatus(vbDouiD, douiD < 1 || douiD > 31);


            }



       
            
            //柔整師番号
            var drCode = verifyBoxDrCode.Text.Trim();
        
            //負傷数
            int cntFusyosu = 0;

            //柔整のみ
            if (scan.AppType == APP_TYPE.柔整)
            {
                //負傷数
                cntFusyosu = int.Parse(verifyBoxFushoCount.Text);
                setStatus(verifyBoxFushoCount, cntFusyosu < 1 || 5 < cntFusyosu);


                //柔整師登録番号
                int.TryParse(drCode, out int drIntCode);

                if (drCode.Length == 0)
                {
                    //空欄
                    setStatus(verifyBoxDrCode, false);
                }
                else if (drCode.Length < 6 && 0 < drIntCode)
                {
                    //地方公務員共済組合協議会番号1-5桁
                    setStatus(verifyBoxDrCode, false);
                }
                else if (drCode.Length == 10 && (drCode[0] == '0' || drCode[0] == '1') && 0 < drIntCode)
                {
                    //通常の柔整師番号
                    //1文字目は協=0か契=1のどちらかのみ
                    setStatus(verifyBoxDrCode, false);
                }
                else
                {
                    setStatus(verifyBoxDrCode,true);
                }
            }



            //チェックでエラーが検出されたらnullを返す
            if (hasError)
            {
                MessageBox.Show("申請書 データを再確認してください。\r\n\r\n" +
                    "赤で示されたデータをご確認ください。登録できません。\r\n",
                    "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                return false;
            }

            //金額でのエラーがあれば確認
            if (payError)
            {
              
                verifyBoxTotal.BackColor = Color.GreenYellow;
                verifyBoxCharge.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("本家区分・合計金額・請求金額のいずれか、" +
                    "または複数に入力ミスがある可能性があります。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }

            //柔整で柔整登録番号がなければ確認
            if (scan.Note2 == string.Empty && drCode.Length == 0)
            {
                verifyBoxDrCode.BackColor = Color.GreenYellow;
                var res = MessageBox.Show("柔整師登録番号が指定されていません。このまま登録してもよろしいですか？", "",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (res != DialogResult.OK) return false;
            }
          

            #endregion

            #region appに反映


            //値の反映
            app.MediYear = sejutuYear;                                      //施術年
            app.MediMonth = sejutuMonth;                                    //施術月

            int MediYearAD = DateTimeEx.GetAdYearFromHs(sejutuYear * 100 + sejutuMonth);//20191001101209 furukawa 施術年西暦


            //保険者番号
            app.InsNum = lblinumhead.Text + verifyBoxinum.Text;

            //給付割合
            app.Ratio = intRatio;


            //20191004095558 furukawa st ////////////////////////
            //区分コード自動判定            
            app.TaggedDatas.GeneralString1 = intRatio == 9 ? "08" : "00";
            //20191004095558 furukawa ed ////////////////////////



            //医療機関コード1=上の医療機関コード
            app.ClinicNum = strClinicCode1;

            //医療機関コード2=下の医療機関コード=支払先コード
            app.PayCode = strClinicCode2;

            app.HihoNum = verifyBoxHnum.Text.Trim();                        //被保険者番号
            app.HihoType = 0;


            app.FushoFirstDate1 = shoken;                               //初検日




            //開始日
            DateTime startDate = DateTime.MinValue;
            if (verifyBoxStart.Text != string.Empty)
            {
                //20191001101355 furukawa st ////////////////////////
                //開始年月日西暦
                
                        //施術開始日
                        //startDate = new DateTime(sejutuYear, sejutuMonth, int.Parse(verifyBoxStart.Text));
                startDate = new DateTime(MediYearAD, sejutuMonth, int.Parse(verifyBoxStart.Text));
                //20191001101355 furukawa ed ////////////////////////

            }
            //終了日
            DateTime finishDate = DateTime.MinValue;
            if (verifyBoxFinish.Text != string.Empty)
            {
                //20191001101425 furukawa st ////////////////////////
                //終了年月日西暦
                
                        //施術終了日   
                        //finishDate = new DateTime(sejutuYear, sejutuMonth, int.Parse(verifyBoxFinish.Text));
                finishDate = new DateTime(MediYearAD, sejutuMonth, int.Parse(verifyBoxFinish.Text));
                //20191001101425 furukawa ed ////////////////////////
            }
            app.FushoStartDate1 = startDate;
            app.FushoFinishDate1 = finishDate;
                

          
            //負傷数登録（application.fusyoは自動計算のみで、直接登録できない)
            app.TaggedDatas.count = cntFusyosu;



            app.CountedDays = sinryoDays;                                   //診療実日数
            app.Total = total;                                              //合計金額
            app.Charge = charge;                                            //請求金額

            //新規継続判断 施術年と初検年 施術月と初検月 が同じ場合は新規
            app.NewContType = 
                DateTimeEx.GetAdYearFromHs(sejutuYear * 100 + sejutuMonth) == shoken.Year && shoken.Month == sejutuMonth ? NEW_CONT.新規 : NEW_CONT.継続;

            app.DrNum = drCode;                                         //柔整師登録番号
            
            app.AccountNumber = string.Empty;                           //口座番号
            app.AppType = scan.AppType;                                 //申請書種類

            
            app.Distance = checkBoxVisit.Checked ? 999 : 0;//往療
            app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;//往療加算あり



            //同意年月日
            if (vbDouiG.Text.Trim() != string.Empty &&
                vbDouiY.Text.Trim() != string.Empty &&
                vbDouiM.Text.Trim() != string.Empty &&
                vbDouiD.Text.Trim() != string.Empty)
            {
                int douiADY = 0;
                douiADY = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(douiG.ToString() + douiY.ToString("00") + douiM.ToString("00"))) / 100;
                app.TaggedDatas.DouiDate = new DateTime(douiADY, douiM, douiD);
            }
            
            #endregion

            return true;
            
        }

        

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;

            //verifyBoxNumbering.Text = verifyBoxNumbering.Text.Trim().PadLeft(Common.NumberingLength, '0');
            verifyBoxHnum.Text = verifyBoxHnum.Text.ToUpper();

            if (verifyBoxY.Text == "**")
            {
                //バッジの場合
                if (!checkBatch(app))
                {
                    focusBack(true);
                    return false;
                }
            }
            else if (verifyBoxY.Text == "--")
            {
                //続紙の場合
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.MediMonth = 0;
                app.HihoNum = string.Empty;
                app.Family = 0;
                app.Sex = 0;
                app.Birthday = DateTime.MinValue;
                app.CountedDays = 0;
                app.Total = 0;
                app.Charge = 0;
                app.Partial = 0;
                app.DrNum = string.Empty;

                //20200312111534 furukawa st ////////////////////////
                //続紙から申請書に修正する時があるので、ナンバリングは空白にしない
                //app.Numbering = string.Empty;
                //20200312111534 furukawa ed ////////////////////////

                app.AccountNumber = string.Empty;
                app.AppType = APP_TYPE.続紙;
            }
            else if (verifyBoxY.Text == "++")
            {
                //白バッジ、その他の場合
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.MediMonth = 0;
                app.HihoNum = string.Empty;
                app.Family = 0;
                app.Sex = 0;
                app.Birthday = DateTime.MinValue;
                app.CountedDays = 0;
                app.Total = 0;
                app.Charge = 0;
                app.Partial = 0;
                app.DrNum = string.Empty;

                //20200312111654 furukawa st ////////////////////////
                //続紙から申請書に修正する時があるので、ナンバリングは空白にしない

                // app.Numbering = string.Empty;
                //20200312111654 furukawa ed ////////////////////////

                app.AccountNumber = string.Empty;
                app.AppType = APP_TYPE.不要;
            }
            else
            {
                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;

                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false; //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;
                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }


        /// <summary>
        /// 次のAppを表示します
        /// </summary>
        /// <param name="app"></param>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            //入力ユーザー表示
            labelInputerName.Text = "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

            //App_Flagのチェック

            //20190722131945 furukawa st ////////////////////////
            //未入力フラグ時もOCRの値をセットする
            
            if (app.StatusFlagCheck(StatusFlag.入力済) || app.StatusFlagCheck(StatusFlag.未処理))
            //if (app.StatusFlagCheck(StatusFlag.入力済))
            //20190722131945 furukawa ed ////////////////////////

            {
                setValues(app);
            }
            else if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                var ocr = app.OcrData.Split(',');
                verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                verifyBoxF5.Text = Fusho.GetFusho5(ocr);
            }

            //void setEnable(TextBox t, bool b)
            //{
            //    t.Enabled = b;
            //    t.BackColor = b ? SystemColors.Info : SystemColors.Control;
            //}

            ////鍼灸時はバッチシートで柔整師番号を入力しているため、グレーアウト
            //if (scan.AppType != APP_TYPE.柔整)
            //{
            //    setEnable(verifyBoxF1, false);
            //    setEnable(verifyBoxF1Y, false);
            //    setEnable(verifyBoxF1M, false);
            //    setEnable(verifyBoxF2, false);
            //    setEnable(verifyBoxF3, false);
            //    setEnable(verifyBoxF4, false);
            //    setEnable(verifyBoxF5, false);

            //    setEnable(verifyBoxDrCode, false);
            //}


            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        /// <param name="r"></param>
        private void setImage(App a)
        {
            string fn = a.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }

                labelImageName.Text = fn;

                scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posNumbering;
                //scrollPictureControl1.AutoScrollPosition = verifyBoxY.Text != "**" ? posYM : posBatch;
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }



        /// <summary>
        /// 入力済みのデータをセットします
        /// </summary>
        private void setValues(App app)
        {
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);
            var ca = 0;// new CheckZenkokuGakkoInputAall(app);

            //OCRチェックが済んだ画像の場合
            if (app.MediYear == (int)APP_SPECIAL_CODE.バッチ)
            {
                setValue(verifyBoxY, "**", firstTime, nv);

                //setValue(verifyBoxBatch, app.Numbering, firstTime, nv);
                //setValue(verifyBoxCount, app.CountedDays, firstTime, nv);
                //setValue(verifyBoxBank, app.AccountNumber, firstTime, nv);
                //setValue(verifyBoxShinkyuDr, app.DrNum == string.Empty ? "++" : app.DrNum, firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.続紙)
            {
                setValue(verifyBoxY, "--", firstTime, nv);
            }
            else if (app.MediYear == (int)APP_SPECIAL_CODE.不要)
            {
                setValue(verifyBoxY, "++", firstTime, nv);
            }
            else
            {
                SetData(app, nv);
                
                #region appからデータ取得 旧コード
                /*
                

                
                 
                //申請書
                //施術年月
                setValue(verifyBoxY, app.MediYear, firstTime, nv);
                setValue(verifyBoxM, app.MediMonth, firstTime, nv);


                //被保険者番号
                setValue(verifyBoxHnum, app.HihoNum, firstTime, nv);

                //診療日数
                setValue(verifyBoxDays, app.CountedDays, firstTime, nv);

                //合計金額
                setValue(verifyBoxTotal, app.Total, firstTime, nv);// && ca.ErrAtotal);
    
                //請求金額
                setValue(verifyBoxCharge, app.Charge, firstTime, nv);//  && ca.ErrAcharge);                               

                //負傷数
                setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);

                //柔整師番号
                //柔整以外は不要
               setValue(verifyBoxDrCode, app.DrNum, firstTime, nv && app.AppType == APP_TYPE.柔整);

                //往療


                //20190722113319 furukawa st ////////////////////////
                //往療有無をベリファイ入力後使えなくする

                //if (app.Distance == 999) checkBoxVisit.Checked = true;
                setValue(checkBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);
                //20190722113319 furukawa ed ////////////////////////



                //setValue(verifyBoxFamily, app.Family.ToString(), firstTime, nv);
                //setValue(verifyCheckBoxSai, app.HihoType == (int)HTYPE_SPECIAL_CODE.災害一部負担支払猶予, firstTime, nv);
                //setValue(verifyBoxSex, app.Sex, firstTime, nv && ca.ErrBirth);
                //setValue(verifyBoxBE, DateTimeEx.GetEraNumber(app.Birthday), firstTime, nv && ca.ErrBirth);
                //setValue(verifyBoxBY, DateTimeEx.GetJpYear(app.Birthday), firstTime, nv && ca.ErrBirth);
                //setValue(verifyBoxBM, app.Birthday.Month, firstTime, nv && ca.ErrBirth);
                //setValue(verifyBoxBD, app.Birthday.Day, firstTime, nv && ca.ErrBirth);
                //setValue(verifyBoxFutan, app.Partial, firstTime, nv);
                //setValue(verifyBoxNumbering, app.Numbering, firstTime, nv);



                //負傷名 初検日はベリファイなし
                //setValue(verifyBoxF1, app.FushoName1, firstTime, false);
                //setValue(verifyBoxF2, app.FushoName2, firstTime, false);
                //setValue(verifyBoxF3, app.FushoName3, firstTime, false);
                //setValue(verifyBoxF4, app.FushoName4, firstTime, false);
                //setValue(verifyBoxF5, app.FushoName5, firstTime, false);



                //20190722112435 furukawa st ////////////////////////
                //初検年月をベリファイ入力にする                
                setValue(verifyBoxF1Y, app.FushoDate1.Year, firstTime, nv);
                setValue(verifyBoxF1M, app.FushoDate1.Month, firstTime, nv);
                //20190722112435 furukawa ed ////////////////////////
                


                

            */
                #endregion

            }
            missCounterReset();
        }



        /// <summary>
        /// 入力欄へのロード
        /// </summary>
        /// <param name="app"></param>
        /// <param name="nv"></param>
        private void SetData(App app, bool nv)
        {
            #region OCRからデータ取得し、なければappを入れる
            Ocr.OcrDatas ocrdata = new Ocr.OcrDatas(app);

            //20190722182820 furukawa st ////////////////////////
            //被保番は0またはemptyのときがある


            //被保険者番号              
           
            
            //20200529174611 furukawa st ////////////////////////
            //OCRからの値は殆ど不正確なので削除
            
            setValue(verifyBoxHnum,app.HihoNum, firstTime, nv);
            //setValue(verifyBoxHnum, 
            //    app.HihoNum.Trim() == "0" || app.HihoNum.Trim()==string.Empty ? 
            //    ocrdata.HihoNum : app.HihoNum, firstTime, nv);
            //setValue(verifyBoxHnum,app.HihoNum == "0" ? ocrdata.HihoNum : app.HihoNum, firstTime, nv);
            //20190722182820 furukawa ed ////////////////////////
            //20200529174611 furukawa ed ////////////////////////


            //20191004103217 furukawa st ////////////////////////
            //診療実日数は精度が低いため除外

            //診療日数
            //setValue(verifyBoxDays, app.CountedDays == 0 ? ocrdata.Days : app.CountedDays, firstTime, nv);
            //20191004103217 furukawa ed ////////////////////////



            //20200529180635 furukawa st ////////////////////////
            //OCRからの値は殆ど不正確なので削除
            
            setValue(verifyBoxTotal, app.Total, firstTime, nv);//合計金額
            setValue(verifyBoxCharge, app.Charge, firstTime, nv);//請求金額

                //合計金額
                //setValue(verifyBoxTotal, app.Total == 0 ? ocrdata.Total : app.Total, firstTime, nv);// && ca.ErrAtotal);

                //請求金額
                //setValue(verifyBoxCharge, app.Charge == 0 ? ocrdata.Charge : app.Charge, firstTime, nv);//  && ca.ErrAcharge);                               
            //20200529180635 furukawa ed ////////////////////////


            //柔整師番号
            //柔整以外は不要
            //OCRでは不正確な場合が多いので手入力とする
            //setValue(verifyBoxDrCode, app.DrNum == string.Empty ? ocrdata.DrCode: app.DrNum , firstTime, nv && app.AppType == APP_TYPE.柔整);

            //初検年月
            DateTime tmp = app.FushoFirstDate1 == DateTime.MinValue ? ocrdata.FirstDate : app.FushoFirstDate1;
            if (tmp != DateTime.MinValue)
            {
                int intwareki = DateTimeEx.GetIntJpDateWithEraNumber(tmp);//和暦年月に変換

                //int変換は前ゼロ落とすため
                setValue(verifyBoxF1Y, int.Parse(intwareki.ToString().Substring(1, 2)), firstTime, nv);
                setValue(verifyBoxF1M, int.Parse(intwareki.ToString().Substring(3, 2)), firstTime, nv);
            }




            #endregion

            #region appからデータ取得
            
            //申請書           
            //施術年月            
            //ここで申請書の種類が変わるので手入力とする
            setValue(verifyBoxY, app.MediYear, firstTime, nv);
            setValue(verifyBoxM, app.MediMonth, firstTime, nv);


            //負傷数
            setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);

            //柔整師番号
            //柔整以外は不要
            setValue(verifyBoxDrCode, app.DrNum, firstTime, nv && app.AppType == APP_TYPE.柔整);

            //往療


            //20190722113319 furukawa st ////////////////////////
            //往療有無をベリファイ入力後使えなくする

            //if (app.Distance == 999) checkBoxVisit.Checked = true;
            setValue(checkBoxVisit, app.Distance == 999 ? true : false, firstTime, nv);
            //20190722113319 furukawa ed ////////////////////////


            //20191004112104 furukawa st ////////////////////////
            //診療実日数はappから取得
            
            setValue(verifyBoxDays, app.CountedDays, firstTime, nv);
            //20191004112104 furukawa ed ////////////////////////


            //柔整の場合のみ
            if (scan.AppType == APP_TYPE.柔整)
            {
                //負傷数                
            }

            //あはきの場合のみ
            if (scan.AppType == APP_TYPE.あんま || scan.AppType == APP_TYPE.鍼灸)
            {
                //医療機関コード１、医療機関コード２、開始日、終了日、保険者番号、給付割合、同意年月日、往療料加算あり
                setValue(verifyBoxIryokikan1, app.ClinicNum, firstTime, nv);
                setValue(verifyBoxIryokikan2, app.PayCode, firstTime, nv);


                setValue(verifyBoxStart, app.FushoStartDate1 == DateTime.MinValue ? string.Empty : app.FushoStartDate1.Day.ToString(), firstTime, nv);
                setValue(verifyBoxFinish, app.FushoFinishDate1 == DateTime.MinValue ? string.Empty : app.FushoFinishDate1.Day.ToString(), firstTime, nv);

                //保険者番号後ろ4桁を入れる
                setValue(verifyBoxinum, app.InsNum!=string.Empty ? app.InsNum.Substring(4):string.Empty, firstTime, nv);                
                setValue(verifyBoxKyufuRate, app.Ratio, firstTime, nv);

                DateTime dt = DateTime.MinValue;
                if (app.TaggedDatas.DouiDate != DateTime.MinValue)
                {
                    dt = app.TaggedDatas.DouiDate;
                   
                    setValue(vbDouiG, DateTimeEx.GetEraNumber(dt).ToString(), firstTime, nv);
                    setValue(vbDouiY, DateTimeEx.GetJpYear(dt).ToString(), firstTime, nv);
                    setValue(vbDouiM, dt.Month.ToString(), firstTime, nv);
                    setValue(vbDouiD, dt.Day.ToString(), firstTime, nv);

                }

                //往療料加算
                setValue(checkBoxVisitKasan, app.VisitAdd == 999 ? true : false, firstTime, nv);

              
            }

            #endregion
            
        }


        /// <summary>
        /// 請求年への入力で画像の種類を判別し、入力項目を調整します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            if (verifyBoxY.Text == "**")
            {
                //赤バッジの場合
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
                panelUpper.Visible = false;


             
                //scrollPictureControl1.ScrollPosition = posBatch;
            }
            else if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text.Trim().Length == 0)
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                panelTotal.Visible = false;
                verifyBoxM.Visible = false;
                labelM.Visible = false;
                panelUpper.Visible = false;

            }
            else
            {
                //申請書の場合
                panelTotal.Visible = true;
                verifyBoxM.Visible = true;
                labelM.Visible = true;
                panelUpper.Visible = true;

                //初検日年月
                lblF1.Visible = true;
                lblF1Y.Visible = true;
                verifyBoxF1Y.Visible = true;
                verifyBoxF1M.Visible = true;
                //verifyBoxF1Y.Enabled = true;
                //verifyBoxF1M.Enabled = true;

                //20191001142938 furukawa st ////////////////////////
                //診療実日数は両方使う
                
                labelDays.Visible = true;
                verifyBoxDays.Visible = true;
                //20191001142938 furukawa ed ////////////////////////



                EnableControl(scan.AppType);
                

            }
        }



        private void inputForm_Shown(object sender, EventArgs e)
        {
            panelLeft.Width = this.Width - 1028;
            verifyBoxY.Focus();
        }

        private void fushoVerifyBox_TextChanged(object sender, EventArgs e)
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        #region 画像関係
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            if (app == null) return;
            setImage(app);
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            if (!(ActiveControl is TextBox)) return;

            var t = (TextBox)ActiveControl;
            var pos = scrollPictureControl1.ScrollPosition;


            if (ymControls.Contains(t)) posYM = pos;
            else if (hnumControls.Contains(t)) posHnum = pos;

            //20191206124248 furukawa st ////////////////////////
            //開始終了日グループでやるので不要
            //else if (dayControls.Contains(t)) posDays = pos;
            //20191206124248 furukawa ed ////////////////////////


            else if (costControls.Contains(t)) posCost = pos;
            else if (fushoControls1.Contains(t)) posFusho = pos;
            else if (hosControls.Contains(t)) posHosCode = pos;
            //else if (numberControls.Contains(t)) posNumbering = pos;

            //20190920170325 furukawa st ////////////////////////
            //医療機関コード、開始終了日、同意年月日等位置
            
            else if (IryoKikanControls.Contains(t)) posIryoKikan = pos;
            else if (StEdControls.Contains(t)) posStEd = pos;
            else if (DouiControls.Contains(t)) posDoui = pos;
            //20190920170325 furukawa ed ////////////////////////




        }

        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            if (app == null) return;
            var fn = app.GetImageFullPath();

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion


    }
}
