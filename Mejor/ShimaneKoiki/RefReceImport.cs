﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.ShimaneKoiki

{
    class RefReceImport
    {
        public int RrImportID { get; private set; }
        public DateTime ImportDate { get; private set; }
        public int UserID { get; set; }
        public string FileName { get; set; }

        /// <summary>
        /// 新しいインポート情報を作成します
        /// 失敗した場合nullが返ります
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public static RefReceImport Create(string fileName, DB.Transaction tran)
        {
            var sql = "INSERT INTO refreceimport (importdate, userid, filename) " +
                "VALUES (@importdate, @userid, @filename) RETURNING rrimportid;";

            try
            {
                var rri = new RefReceImport();
                rri.FileName = fileName;
                rri.UserID = User.CurrentUser.UserID;
                rri.ImportDate = DateTime.Today;

                var ids = DB.Main.Query<int>(sql, rri, tran);
                rri.RrImportID = ids.First();
                return rri;
            }
            catch
            {
                return null;
            }
        }
    }
}
