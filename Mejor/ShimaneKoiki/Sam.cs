﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor.ShimaneKoiki
{
    class Sam
    {
        App app;
        int number;
        string drNum => app.DrNum.PadLeft(10, '0');

        public Sam(App app)
        {
            this.app = app;
            this.number = 0;
        }
        public Sam(App app, int number)
        {
            this.app = app;
            this.number = number;
        }

        public static byte[] CreateHeader()
        {
            //2.ヘッダレコード
            var s = "10000000D16201320000" +
                string.Empty.PadLeft(8, ' ');
             
            s += DateTime.Now.ToString("yyyyMMddHHmmss");
            s = s.PadRight(1000);
            return Encoding.UTF8.GetBytes(s);
        }

        public static byte[] CreateFooter(int count)
        {
            //4.トレイラレコード
            var s = "39999999";
            s += count.ToString("0000000");
            s = s.PadRight(1000);
            return Encoding.UTF8.GetBytes(s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exportDt">出力日</param>
        /// <param name="dtUketuke">受付日</param>
        /// <param name="dtKettei">決定日</param>
        /// <param name="strdtSisyutu">支出日</param>
        /// <param name="recNo">レコードの連番</param>
        /// <param name="wf">waitform</param>
        /// <returns></returns>
        /// 
        
        //20191107132741 furukawa st ////////////////////////
        //支給年月日も手入力になったので引数追加
        public byte[] CreateRecord(DateTime exportDt,string strdtUketuke,string strdtKettei,string strdtSisyutu ,int recNo, WaitForm wf)
        //public byte[] CreateRecord(DateTime exportDt, string strdtUketuke, string strdtKettei, int recNo, WaitForm wf)
        //20191107132741 furukawa ed ////////////////////////
        {
            var bl = new List<byte>();

            void addUTF8(string str) => bl.AddRange(Encoding.UTF8.GetBytes(str));
            void addUTF16(string str) => bl.AddRange(Encoding.BigEndianUnicode.GetBytes(str));
            void addGyymmddUTF8(DateTime dt) => 
                addUTF8((dt.IsNullDate() ? 0 : DateTimeEx.GetIntJpDateWithEraNumber(dt)).ToString("0000000"));



            //3.データレコード
            addUTF8("2");                                  //3-1 英数 1 「2」を設定する。
            addUTF8(recNo.ToString().PadLeft(7,'0'));          //3-2 英数 7 「0000001」から連番を設定する。


            //島根広域あはき用SAMファイルのレコード
            //5.インターフェースデータ

            //numberingのパターンでない場合は、ダミーデータを入れて判別しやすくしておく
            if (app.Numbering==string.Empty) app.Numbering = "dummy".PadRight(30, 'x');

            addUTF8(app.Numbering);                                         //1 レセプト管理番号  3byte →   やっぱり画像登録時に連番でつけて最終の値をテーブルに持たせる。   画像登録中はインクリメントするだけ→やった

            DateTime dtYM = new DateTime(int.Parse(app.YM.ToString().Substring(0, 4)), int.Parse(app.YM.ToString().Substring(4, 2)), 1);
            addUTF8(DateTimeEx.GetIntJpDateWithEraNumber(dtYM).ToString().Substring(0, 5));                         //2 診療年月  5byte

            addUTF8(app.ClinicNum.Substring(0, 2));                             //3 都道府県番号  2byte
            addUTF8(app.ClinicNum.Substring(2, 1));                             //4 点数表コード  1byte
            addUTF8(app.ClinicNum.Substring(3));                                //5 医療機関等コード  7byte
            addUTF8("1");                                                   //6 保険種別  1byte


            if(app.Ratio==9) addUTF8("08");//給付割合＝９０％＝9割＝外来9割
            if(app.Ratio==7) addUTF8("00");//給付割合＝７０％＝7割＝外来7割
            //addUTF8("08?00");                                               //7 区分コード  2byte※要確認　給付割合から判定

            addUTF8(app.InsNum);                                            //8 保険者番号  8byte
            addUTF8(app.HihoNum.ToString().PadLeft(8, '0'));                 //9 被保険者番号  8byte
            addUTF8((app.Ratio * 10).ToString().PadLeft(3, '0'));              //10 給付割合  3byte


            //7が鍼、8あんま
            //20191107193544 furukawa st ////////////////////////
            //比較間違い            
            addUTF8(app.AppType.ToString() == APP_TYPE.鍼灸.ToString() ? "06" : "05");            //11 療養費区分コード  2byte
            //addUTF8(app.AppType.ToString() == "7" ? "06" : "05");            //11 療養費区分コード  2byte
            //20191107193544 furukawa ed ////////////////////////


            //string strStartdate1 = DateTimeEx.ToJDateStr(app.FushoStartDate1);                      //12 施術開始年月日  7byte
            string strStartdate1 = DateTimeEx.GetIntJpDateWithEraNumber(app.FushoStartDate1).ToString();
            addUTF8(strStartdate1==DateTime.MinValue.ToString() ? "0000000" : strStartdate1);

            //string strFinishdate1 = DateTimeEx.ToJDateStr(app.FushoFinishDate1);                    //13 施術終了年月日  7byte
            string strFinishdate1 = DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFinishDate1).ToString();
            addUTF8(strFinishdate1 == DateTime.MinValue.ToString() ? "0000000" : strFinishdate1);


            //14～19　月別診療年月
            addUTF8(string.Empty.ToString().PadLeft(5, '0'));//14　月別診療年月
            addUTF8(string.Empty.ToString().PadLeft(5, '0'));//15　月別診療年月
            addUTF8(string.Empty.ToString().PadLeft(5, '0'));//16　月別診療年月
            addUTF8(string.Empty.ToString().PadLeft(5, '0'));//17　月別診療年月
            addUTF8(string.Empty.ToString().PadLeft(5, '0'));//18　月別診療年月
            addUTF8(string.Empty.ToString().PadLeft(5, '0'));//19　月別診療年月

            addUTF8(app.CountedDays == 0 ? "00" : app.CountedDays.ToString("00"));                  //20 診療実日数  2byte


            //21～26　月別診療日日数
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));//21　月別診療日日数
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));//22　月別診療日日数
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));//23　月別診療日日数
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));//24　月別診療日日数
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));//25　月別診療日日数
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));//26　月別診療日日数

            addUTF8(app.Charge.ToString().PadLeft(8, '0'));             //27 請求金額  8byte  請求金額　伸作さんOK
            addUTF8(app.Charge.ToString().PadLeft(8, '0'));             //28 決定金額  8byte　請求金額　伸作さんOK
            addUTF8(app.Total.ToString().PadLeft(8, '0'));              //29 費用額（合計）  8byte

            //30～35 月別費用金額
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //30 月別費用金額
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //31 月別費用金額
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //32 月別費用金額
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //33 月別費用金額
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //34 月別費用金額
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //35 月別費用金額

            addUTF8(app.Partial.ToString().PadLeft(8, '0'));            //36 負担金額  8byte

            addUTF8(string.Empty.ToString().PadLeft(3, '0'));           //37 食事療養費 回数  3byte
            addUTF8(string.Empty.ToString().PadLeft(6, '0'));           //38 食事療養費 決定金額  6byte
            addUTF8(string.Empty.ToString().PadLeft(6, '0'));           //39 食事療養費 標準負担額  6byte

            addUTF8(string.Empty.ToString().PadLeft(8, ' '));           //40 負担者番号 英数 8 8			半角スペース8文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(7, ' '));           //41 受給者番号 英数 7 7			半角スペース7文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));           //42 日数 英数 2 2			「00」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(7, '0'));           //43 請求金額 英数 7 7			「0000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(7, '0'));           //44 決定金額 英数 7 7			「0000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //45 公費対象負担金額 英数 8 8			「00000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //46 公費患者負担額 英数 8 8			「00000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(3, '0'));           //47 食事回数 英数 3 3			「000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(6, '0'));           //48 食事決定金額 英数 6 6			「000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(6, '0'));           //49 食事標準負担額 英数 6 6			「000000」を設定する。

            addUTF8(string.Empty.ToString().PadLeft(8, ' '));           //50 負担者番号 英数 8 8			半角スペース8文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(7, ' '));           //51 受給者番号 英数 7 7			半角スペース7文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(2, '0'));           //52 日数 英数 2 2						「00」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(7, '0'));           //53 請求金額 英数 7 7                    「0000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(7, '0'));           //54 決定金額 英数 7 7                    「0000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //55 公費対象負担金額 英数 8 8            「00000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //56 公費患者負担額 英数 8 8              「00000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(3, '0'));           //57 食事回数 英数 3 3                    「000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(6, '0'));           //58 食事決定金額 英数 6 6                「000000」を設定する。
            addUTF8(string.Empty.ToString().PadLeft(6, '0'));           //59 食事標準負担額 英数 6 6              「000000」を設定する。



            //20191101121008 furukawa st ////////////////////////
            //医療機関コード１，２の比較フィールドを修正
            
            addUTF8(app.ClinicNum == app.PayCode ? "2" : "3");        //60 支払先区分コード  1byte drnumとaccountnumberが違う場合は３
                                 //addUTF8(app.DrNum == app.AccountNumber ? "2" : "3");        //60 支払先区分コード  1byte drnumとaccountnumberが違う場合は３
            //20191101121008 furukawa ed ////////////////////////




            //支払先区分コードが3:受領委任（団体）の場合は、申請書摘要欄または欄外に青字で10ケタの連番を記載しますので、それを入力ください。
            //（番号の前に団を〇囲みで記入いたします。）
            //支払先区分コードが2:受領委任の場合は半角スペース7文字→61-63で10文字？を設定ください。

            //20191101121050 furukawa st ////////////////////////
            //医療機関コード１，２の比較フィールドを修正
            
            if (app.ClinicNum != app.PayCode)
            //if (app.DrNum != app.AccountNumber)
            //20191101121050 furukawa ed ////////////////////////


            {
                addUTF8(app.PayCode.ToString().Substring(0, 2));      //61 支払先都道府県番号  2byte
                addUTF8(app.PayCode.ToString().Substring(2, 1));      //62 支払先点数表コード  1byte
                addUTF8(app.PayCode.ToString().Substring(3));         //63 支払先医療機関番号  7byte
            }
            else
            {
                addUTF8(string.Empty.ToString().PadLeft(10, ' '));
            }


            addUTF8(strdtUketuke);                                      //64 受付年月日  7byte
            addUTF8(strdtKettei);                                       //65 決定年月日  7byte             

            //20191107132936 furukawa st ////////////////////////
            //支給年月日も手入力になったので引数から取得
            
            addUTF8(strdtSisyutu);           //66 支出年月日  7byte 
                //addUTF8(string.Empty.ToString().PadLeft(7, '0'));           //66 支出年月日  7byte 
            //20191107132936 furukawa ed ////////////////////////


            addUTF8(app.Charge.ToString().PadLeft(8, '0'));             //67 支給決定額  7byte




            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //68 充当額　英数 8 8 仕様通り0を8バイト
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //69 支給調整額　英数 8 8 仕様通り0を8バイト
            addUTF8(string.Empty.ToString().PadLeft(8, '0'));           //70 支給額　英数 8 8  仕様通り0を8バイト
            addUTF8(string.Empty.ToString().PadLeft(7, ' '));           //71 郵便番号 英数 7 7									半角スペース7文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(16, ' '));           //72 電話番号 英数 16 16                                  半角スペース16文字を設定する。
            addUTF16(string.Empty.ToString().PadLeft(30, '　'));           //73 氏名（漢字） 漢字 30 60                              全角スペース30文字を設定する。
            addUTF16(string.Empty.ToString().PadLeft(100, '　'));           //74 住所（漢字） 漢字 100 200                            全角スペース100文字を設定する。
            addUTF16(string.Empty.ToString().PadLeft(15, '　'));           //75 被保険者との関係 漢字 15 30                          全角スペース15文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(1, ' '));           //76 金融機関区分コード 英数 1 1                          半角スペース1文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(4, ' '));           //77 金融機関コード 英数 4 4                              半角スペース4文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(3, ' '));           //78 金融機関店舗コード 英数 3 3                          半角スペース3文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(1, ' '));           //79 預金種別コード 英数 1 1                              半角スペース1文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(13, ' '));           //80 口座番号 英数 13 13                                  半角スペース13文字を設定する。
            addUTF16(string.Empty.ToString().PadLeft(100, '　'));           //81 口座名義人氏名（カナ） 漢字 100 200                  全角スペース100文字を設定する。
            addUTF8(string.Empty.ToString().PadLeft(47, ' '));           //82 予備 英数 47 47                                      半角スペース47文字を設定する。








            /*
            addUTF8("2" + number.ToString("0000000"));
            addUTF8((drNum + app.Numbering).PadLeft(30, '0'));  //レセプト管理番号(医療機関番号+ナンバリング)
            addUTF8(DateTimeEx.GetEraNumberYearFromYYYYMM(app.YM).ToString("000"));
            addUTF8((app.YM % 100).ToString("00"));
            addUTF8(drNum);
            addUTF8("1");
            addUTF8(app.Family < 0 ? "00" : app.Family.ToString("00"));
            addUTF8(app.InsNum.PadLeft(8, '0').Substring(0, 8));
            addUTF8(app.HihoNum.PadLeft(8, '0').Substring(0, 8));
            addUTF8((app.Ratio * 10).ToString("000"));
            addUTF8(app.AppType == APP_TYPE.鍼灸 ? "06" :
                app.AppType == APP_TYPE.あんま ? "05" :
                throw new Exception("鍼灸/あんま以外の申請書が指定されています"));
            addGyymmddUTF8(app.FushoStartDate1);
            addGyymmddUTF8(app.FushoFinishDate1);
            addUTF8(new string('0', 30));
            addUTF8(app.CountedDays.ToString("00"));
            addUTF8(new string('0', 12));
            addUTF8(app.Charge.ToString("00000000"));
            addUTF8(app.Charge.ToString("00000000"));
            addUTF8(app.Total.ToString("00000000"));
            addUTF8(new string('0', 48));
            addUTF8(app.Partial.ToString("00000000"));
            addUTF8(new string('0', 15));       //食事
            addUTF8(new string(' ', 15));       //公費1 スペース部
            addUTF8(new string('0', 47));       //公費1 0埋め部
            addUTF8(new string(' ', 15));       //公費2 スペース部
            addUTF8(new string('0', 47));       //公費2 0埋め部
            //var dr = dic.ContainsKey(drNum) ? dic[drNum] : null;
            //if (dr == null) wf.LogPrint($"施術師情報が見つかりません " +
            //    $"AID:{app.Aid} ナンバリング:{ app.Numbering} 施術師コード:{app.DrNum}");
            //addUTF8(dr == null ? "0" : dr.PayType == Doctor.PAY_TYPE.団体 ? "3" : "2");  //支払先区分　要設定
            addUTF8(new string(' ', 10));
            addGyymmddUTF8(exportDt);           //受付日 通常翌月25日
            addUTF8(new string('0', 7));        //決定年月日
            addUTF8(new string('0', 7));        //支出年月日
            addUTF8(app.Charge.ToString("00000000"));
            addUTF8(new string(' ', 24));       //未使用
            addUTF8(new string(' ', 23));       //申請者 英数部
            addUTF16(new string('　', 145));    //申請者 漢字部
            addUTF8(new string(' ', 22));       //振込先 英数部
            addUTF16(new string('　', 100));    //振込先 漢字部
            addUTF8(new string(' ', 47));       //予備
            */
            return bl.ToArray();
        }

        public static bool CheckNumbering(List<App> list, WaitForm wf)
        {
            var error = false;
            list.Sort((x, y) => x.Aid.CompareTo(y.Aid));

            int num1 = 0, num2 = 0, num3 = 0;
            App app1 = null, app2 = null, app3 = null;
            var dic = new Dictionary<string, List<int>>();

            foreach (var item in list)
            {
                if (item.AppType < APP_TYPE.NULL) continue;
                if (!dic.ContainsKey(item.Numbering)) dic.Add(item.Numbering, new List<int>());
                dic[item.Numbering].Add(item.Aid);

                if (!int.TryParse(item.Numbering, out int temp))
                {
                    wf.LogPrint($"数値にできないナンバリングがあります　AID:{item.Aid}　　ナンバリング:{item.Numbering}");
                    error = true;
                }

                app1 = app2;
                app2 = app3;
                app3 = item;
                num1 = num2;
                num2 = num3;
                num3 = temp;

                if ((num1 != 0 && Math.Abs(num2 - num1) > 8) || (num2 != 0 && Math.Abs(num2 - num3) > 8))
                {
                    wf.LogPrint($"前後との比較で確認の必要なナンバリングがあります　" +
                        $"AID:{app2.Aid}　ナンバリング:{app2.Numbering}　前:{app1.Numbering}　後:{app3.Numbering}");
                    error = true;
                }
            }

            foreach (var item in dic)
            {
                if(item.Value.Count>1)
                {
                    wf.LogPrint($"重複しているナンバリングがあります　" +
                        $"ナンバリング:{item.Key}　AID:{string.Join(",", item.Value)}");
                    error = true;
                }
            }
            return error;
        }
    }
}
