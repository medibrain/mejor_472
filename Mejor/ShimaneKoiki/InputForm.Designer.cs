﻿using System;

namespace Mejor.ShimaneKoiki
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelImageName = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelWholeImage = new System.Windows.Forms.Panel();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.userControlImage1 = new UserControlImage();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelUpper = new System.Windows.Forms.Panel();
            this.lblinumhead = new System.Windows.Forms.Label();
            this.verifyBoxHnum = new Mejor.VerifyBox();
            this.lblRatioNum = new System.Windows.Forms.Label();
            this.lblRatio = new System.Windows.Forms.Label();
            this.lblHosCode2 = new System.Windows.Forms.Label();
            this.verifyBoxKyufuRate = new Mejor.VerifyBox();
            this.lblHosCode1 = new System.Windows.Forms.Label();
            this.lblinum = new System.Windows.Forms.Label();
            this.verifyBoxinum = new Mejor.VerifyBox();
            this.verifyBoxIryokikan2 = new Mejor.VerifyBox();
            this.verifyBoxIryokikan1 = new Mejor.VerifyBox();
            this.scrollPictureControl1 = new Mejor.ScrollPictureControl();
            this.buttonBack = new System.Windows.Forms.Button();
            this.verifyBoxM = new Mejor.VerifyBox();
            this.verifyBoxY = new Mejor.VerifyBox();
            this.panelTotal = new System.Windows.Forms.Panel();
            this.pDoui = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.vbDouiG = new Mejor.VerifyBox();
            this.lblDoui = new System.Windows.Forms.Label();
            this.vbDouiD = new Mejor.VerifyBox();
            this.lblDouiM = new System.Windows.Forms.Label();
            this.lblDouiD = new System.Windows.Forms.Label();
            this.lblDouiY = new System.Windows.Forms.Label();
            this.vbDouiM = new Mejor.VerifyBox();
            this.vbDouiY = new Mejor.VerifyBox();
            this.checkBoxVisitKasan = new Mejor.VerifyCheckBox();
            this.lblstart = new System.Windows.Forms.Label();
            this.lblDrCode = new System.Windows.Forms.Label();
            this.lblDrCode01 = new System.Windows.Forms.Label();
            this.verifyBoxDays = new Mejor.VerifyBox();
            this.verifyBoxDrCode = new Mejor.VerifyBox();
            this.verifyBoxStart = new Mejor.VerifyBox();
            this.labelFusyoCnt = new System.Windows.Forms.Label();
            this.verifyBoxFinish = new Mejor.VerifyBox();
            this.checkBoxVisit = new Mejor.VerifyCheckBox();
            this.lblfinish = new System.Windows.Forms.Label();
            this.verifyBoxFushoCount = new Mejor.VerifyBox();
            this.labelDays = new System.Windows.Forms.Label();
            this.verifyBoxF5 = new Mejor.VerifyBox();
            this.verifyBoxF4 = new Mejor.VerifyBox();
            this.verifyBoxF3 = new Mejor.VerifyBox();
            this.verifyBoxCharge = new Mejor.VerifyBox();
            this.verifyBoxTotal = new Mejor.VerifyBox();
            this.verifyBoxF2 = new Mejor.VerifyBox();
            this.verifyBoxF1M = new Mejor.VerifyBox();
            this.verifyBoxF1Y = new Mejor.VerifyBox();
            this.verifyBoxF1 = new Mejor.VerifyBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblF1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblF1M = new System.Windows.Forms.Label();
            this.lblF1Y = new System.Windows.Forms.Label();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelCharge = new System.Windows.Forms.Label();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panelLeft.SuspendLayout();
            this.panelWholeImage.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelUpper.SuspendLayout();
            this.panelTotal.SuspendLayout();
            this.pDoui.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonRegist.Location = new System.Drawing.Point(566, 695);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(90, 23);
            this.buttonRegist.TabIndex = 255;
            this.buttonRegist.Text = "登録 (PgUp)";
            this.buttonRegist.UseVisualStyleBackColor = true;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(144, 38);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(17, 12);
            this.labelY.TabIndex = 3;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(194, 38);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(29, 12);
            this.labelM.TabIndex = 5;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(229, 10);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(77, 12);
            this.labelHnum.TabIndex = 4;
            this.labelHnum.Text = "被保険者番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(82, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "和暦";
            // 
            // labelImageName
            // 
            this.labelImageName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImageName.AutoSize = true;
            this.labelImageName.Location = new System.Drawing.Point(164, 701);
            this.labelImageName.Name = "labelImageName";
            this.labelImageName.Size = new System.Drawing.Size(64, 12);
            this.labelImageName.TabIndex = 1;
            this.labelImageName.Text = "ImageName";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.panelWholeImage);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeft.Location = new System.Drawing.Point(217, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(104, 721);
            this.panelLeft.TabIndex = 1;
            // 
            // panelWholeImage
            // 
            this.panelWholeImage.Controls.Add(this.buttonImageChange);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateL);
            this.panelWholeImage.Controls.Add(this.buttonImageRotateR);
            this.panelWholeImage.Controls.Add(this.buttonImageFill);
            this.panelWholeImage.Controls.Add(this.labelImageName);
            this.panelWholeImage.Controls.Add(this.userControlImage1);
            this.panelWholeImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelWholeImage.Location = new System.Drawing.Point(0, 0);
            this.panelWholeImage.Name = "panelWholeImage";
            this.panelWholeImage.Size = new System.Drawing.Size(104, 721);
            this.panelWholeImage.TabIndex = 2;
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(78, 696);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 23);
            this.buttonImageChange.TabIndex = 12;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(5, 696);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateL.TabIndex = 11;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(41, 696);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 23);
            this.buttonImageRotateR.TabIndex = 10;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(-196, 696);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 23);
            this.buttonImageFill.TabIndex = 6;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // userControlImage1
            // 
            this.userControlImage1.AutoScroll = true;
            this.userControlImage1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControlImage1.Location = new System.Drawing.Point(0, 0);
            this.userControlImage1.Margin = new System.Windows.Forms.Padding(4);
            this.userControlImage1.Name = "userControlImage1";
            this.userControlImage1.Size = new System.Drawing.Size(104, 721);
            this.userControlImage1.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelUpper);
            this.panelRight.Controls.Add(this.scrollPictureControl1);
            this.panelRight.Controls.Add(this.buttonBack);
            this.panelRight.Controls.Add(this.verifyBoxM);
            this.panelRight.Controls.Add(this.verifyBoxY);
            this.panelRight.Controls.Add(this.panelTotal);
            this.panelRight.Controls.Add(this.labelInputerName);
            this.panelRight.Controls.Add(this.label3);
            this.panelRight.Controls.Add(this.label8);
            this.panelRight.Controls.Add(this.labelY);
            this.panelRight.Controls.Add(this.buttonRegist);
            this.panelRight.Controls.Add(this.labelM);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(324, 0);
            this.panelRight.MinimumSize = new System.Drawing.Size(660, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(1020, 721);
            this.panelRight.TabIndex = 2;
            // 
            // panelUpper
            // 
            this.panelUpper.Controls.Add(this.lblinumhead);
            this.panelUpper.Controls.Add(this.verifyBoxHnum);
            this.panelUpper.Controls.Add(this.lblRatioNum);
            this.panelUpper.Controls.Add(this.labelHnum);
            this.panelUpper.Controls.Add(this.lblRatio);
            this.panelUpper.Controls.Add(this.lblHosCode2);
            this.panelUpper.Controls.Add(this.verifyBoxKyufuRate);
            this.panelUpper.Controls.Add(this.lblHosCode1);
            this.panelUpper.Controls.Add(this.lblinum);
            this.panelUpper.Controls.Add(this.verifyBoxinum);
            this.panelUpper.Controls.Add(this.verifyBoxIryokikan2);
            this.panelUpper.Controls.Add(this.verifyBoxIryokikan1);
            this.panelUpper.Location = new System.Drawing.Point(235, 3);
            this.panelUpper.Name = "panelUpper";
            this.panelUpper.Size = new System.Drawing.Size(700, 69);
            this.panelUpper.TabIndex = 20;
            // 
            // lblinumhead
            // 
            this.lblinumhead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblinumhead.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblinumhead.Location = new System.Drawing.Point(3, 26);
            this.lblinumhead.Name = "lblinumhead";
            this.lblinumhead.Size = new System.Drawing.Size(45, 23);
            this.lblinumhead.TabIndex = 95;
            this.lblinumhead.Text = "3932";
            this.lblinumhead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxHnum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxHnum.Location = new System.Drawing.Point(231, 26);
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.NewLine = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(104, 23);
            this.verifyBoxHnum.TabIndex = 25;
            this.verifyBoxHnum.TextV = "";
            // 
            // lblRatioNum
            // 
            this.lblRatioNum.AutoSize = true;
            this.lblRatioNum.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRatioNum.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblRatioNum.Location = new System.Drawing.Point(184, 31);
            this.lblRatioNum.Name = "lblRatioNum";
            this.lblRatioNum.Size = new System.Drawing.Size(35, 12);
            this.lblRatioNum.TabIndex = 58;
            this.lblRatioNum.Text = "7 or 9";
            // 
            // lblRatio
            // 
            this.lblRatio.AutoSize = true;
            this.lblRatio.Location = new System.Drawing.Point(150, 10);
            this.lblRatio.Name = "lblRatio";
            this.lblRatio.Size = new System.Drawing.Size(53, 12);
            this.lblRatio.TabIndex = 59;
            this.lblRatio.Text = "給付割合";
            // 
            // lblHosCode2
            // 
            this.lblHosCode2.AutoSize = true;
            this.lblHosCode2.Location = new System.Drawing.Point(522, 10);
            this.lblHosCode2.Name = "lblHosCode2";
            this.lblHosCode2.Size = new System.Drawing.Size(88, 12);
            this.lblHosCode2.TabIndex = 99;
            this.lblHosCode2.Text = "医療機関コード２";
            // 
            // verifyBoxKyufuRate
            // 
            this.verifyBoxKyufuRate.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKyufuRate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxKyufuRate.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxKyufuRate.Location = new System.Drawing.Point(152, 26);
            this.verifyBoxKyufuRate.Name = "verifyBoxKyufuRate";
            this.verifyBoxKyufuRate.NewLine = false;
            this.verifyBoxKyufuRate.Size = new System.Drawing.Size(26, 23);
            this.verifyBoxKyufuRate.TabIndex = 20;
            this.verifyBoxKyufuRate.TextV = "";
            // 
            // lblHosCode1
            // 
            this.lblHosCode1.AutoSize = true;
            this.lblHosCode1.Location = new System.Drawing.Point(406, 10);
            this.lblHosCode1.Name = "lblHosCode1";
            this.lblHosCode1.Size = new System.Drawing.Size(88, 12);
            this.lblHosCode1.TabIndex = 98;
            this.lblHosCode1.Text = "医療機関コード１";
            // 
            // lblinum
            // 
            this.lblinum.AutoSize = true;
            this.lblinum.Location = new System.Drawing.Point(39, 10);
            this.lblinum.Name = "lblinum";
            this.lblinum.Size = new System.Drawing.Size(65, 12);
            this.lblinum.TabIndex = 94;
            this.lblinum.Text = "保険者番号";
            this.lblinum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxinum
            // 
            this.verifyBoxinum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxinum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxinum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxinum.Location = new System.Drawing.Point(47, 26);
            this.verifyBoxinum.Name = "verifyBoxinum";
            this.verifyBoxinum.NewLine = false;
            this.verifyBoxinum.Size = new System.Drawing.Size(90, 23);
            this.verifyBoxinum.TabIndex = 10;
            this.verifyBoxinum.TextV = "";
            // 
            // verifyBoxIryokikan2
            // 
            this.verifyBoxIryokikan2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxIryokikan2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxIryokikan2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxIryokikan2.Location = new System.Drawing.Point(524, 26);
            this.verifyBoxIryokikan2.Name = "verifyBoxIryokikan2";
            this.verifyBoxIryokikan2.NewLine = false;
            this.verifyBoxIryokikan2.Size = new System.Drawing.Size(104, 23);
            this.verifyBoxIryokikan2.TabIndex = 50;
            this.verifyBoxIryokikan2.TextV = "";
            // 
            // verifyBoxIryokikan1
            // 
            this.verifyBoxIryokikan1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxIryokikan1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxIryokikan1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxIryokikan1.Location = new System.Drawing.Point(408, 26);
            this.verifyBoxIryokikan1.Name = "verifyBoxIryokikan1";
            this.verifyBoxIryokikan1.NewLine = false;
            this.verifyBoxIryokikan1.Size = new System.Drawing.Size(104, 23);
            this.verifyBoxIryokikan1.TabIndex = 30;
            this.verifyBoxIryokikan1.TextV = "";
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scrollPictureControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Location = new System.Drawing.Point(0, 74);
            this.scrollPictureControl1.Margin = new System.Windows.Forms.Padding(4);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 126);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(1020, 480);
            this.scrollPictureControl1.TabIndex = 8;
            this.scrollPictureControl1.TabStop = false;
            this.scrollPictureControl1.ImageScrolled += new System.EventHandler(this.scrollPictureControl1_ImageScrolled);
            // 
            // buttonBack
            // 
            this.buttonBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonBack.Location = new System.Drawing.Point(470, 695);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(90, 23);
            this.buttonBack.TabIndex = 95;
            this.buttonBack.TabStop = false;
            this.buttonBack.Text = "戻る (PgDn)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxM.Location = new System.Drawing.Point(159, 27);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.NewLine = false;
            this.verifyBoxM.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxM.TabIndex = 1;
            this.verifyBoxM.TextV = "";
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxY.Location = new System.Drawing.Point(111, 27);
            this.verifyBoxY.MaxLength = 2;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.NewLine = false;
            this.verifyBoxY.Size = new System.Drawing.Size(33, 23);
            this.verifyBoxY.TabIndex = 0;
            this.verifyBoxY.TextV = "";
            this.verifyBoxY.TextChanged += new System.EventHandler(this.verifyBoxY_TextChanged);
            // 
            // panelTotal
            // 
            this.panelTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panelTotal.Controls.Add(this.pDoui);
            this.panelTotal.Controls.Add(this.checkBoxVisitKasan);
            this.panelTotal.Controls.Add(this.lblstart);
            this.panelTotal.Controls.Add(this.lblDrCode);
            this.panelTotal.Controls.Add(this.lblDrCode01);
            this.panelTotal.Controls.Add(this.verifyBoxDays);
            this.panelTotal.Controls.Add(this.verifyBoxDrCode);
            this.panelTotal.Controls.Add(this.verifyBoxStart);
            this.panelTotal.Controls.Add(this.labelFusyoCnt);
            this.panelTotal.Controls.Add(this.verifyBoxFinish);
            this.panelTotal.Controls.Add(this.checkBoxVisit);
            this.panelTotal.Controls.Add(this.lblfinish);
            this.panelTotal.Controls.Add(this.verifyBoxFushoCount);
            this.panelTotal.Controls.Add(this.labelDays);
            this.panelTotal.Controls.Add(this.verifyBoxF5);
            this.panelTotal.Controls.Add(this.verifyBoxF4);
            this.panelTotal.Controls.Add(this.verifyBoxF3);
            this.panelTotal.Controls.Add(this.verifyBoxCharge);
            this.panelTotal.Controls.Add(this.verifyBoxTotal);
            this.panelTotal.Controls.Add(this.verifyBoxF2);
            this.panelTotal.Controls.Add(this.verifyBoxF1M);
            this.panelTotal.Controls.Add(this.verifyBoxF1Y);
            this.panelTotal.Controls.Add(this.verifyBoxF1);
            this.panelTotal.Controls.Add(this.label22);
            this.panelTotal.Controls.Add(this.label21);
            this.panelTotal.Controls.Add(this.label20);
            this.panelTotal.Controls.Add(this.label11);
            this.panelTotal.Controls.Add(this.lblF1);
            this.panelTotal.Controls.Add(this.label9);
            this.panelTotal.Controls.Add(this.lblF1M);
            this.panelTotal.Controls.Add(this.lblF1Y);
            this.panelTotal.Controls.Add(this.labelTotal);
            this.panelTotal.Controls.Add(this.labelCharge);
            this.panelTotal.Location = new System.Drawing.Point(0, 550);
            this.panelTotal.Name = "panelTotal";
            this.panelTotal.Size = new System.Drawing.Size(1020, 130);
            this.panelTotal.TabIndex = 30;
            // 
            // pDoui
            // 
            this.pDoui.Controls.Add(this.label12);
            this.pDoui.Controls.Add(this.vbDouiG);
            this.pDoui.Controls.Add(this.lblDoui);
            this.pDoui.Controls.Add(this.vbDouiD);
            this.pDoui.Controls.Add(this.lblDouiM);
            this.pDoui.Controls.Add(this.lblDouiD);
            this.pDoui.Controls.Add(this.lblDouiY);
            this.pDoui.Controls.Add(this.vbDouiM);
            this.pDoui.Controls.Add(this.vbDouiY);
            this.pDoui.Location = new System.Drawing.Point(649, 60);
            this.pDoui.Name = "pDoui";
            this.pDoui.Size = new System.Drawing.Size(310, 51);
            this.pDoui.TabIndex = 135;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(65, 5);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 24);
            this.label12.TabIndex = 84;
            this.label12.Text = "4:平成\r\n5:令和";
            // 
            // vbDouiG
            // 
            this.vbDouiG.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiG.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiG.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiG.Location = new System.Drawing.Point(107, 5);
            this.vbDouiG.MaxLength = 2;
            this.vbDouiG.Name = "vbDouiG";
            this.vbDouiG.NewLine = false;
            this.vbDouiG.Size = new System.Drawing.Size(28, 23);
            this.vbDouiG.TabIndex = 61;
            this.vbDouiG.TextV = "";
            // 
            // lblDoui
            // 
            this.lblDoui.AutoSize = true;
            this.lblDoui.Location = new System.Drawing.Point(5, 5);
            this.lblDoui.Name = "lblDoui";
            this.lblDoui.Size = new System.Drawing.Size(41, 24);
            this.lblDoui.TabIndex = 63;
            this.lblDoui.Text = "同意\r\n年月日";
            // 
            // vbDouiD
            // 
            this.vbDouiD.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiD.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiD.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiD.Location = new System.Drawing.Point(244, 5);
            this.vbDouiD.MaxLength = 2;
            this.vbDouiD.Name = "vbDouiD";
            this.vbDouiD.NewLine = false;
            this.vbDouiD.Size = new System.Drawing.Size(28, 23);
            this.vbDouiD.TabIndex = 68;
            this.vbDouiD.TextV = "";
            // 
            // lblDouiM
            // 
            this.lblDouiM.AutoSize = true;
            this.lblDouiM.Location = new System.Drawing.Point(224, 14);
            this.lblDouiM.Name = "lblDouiM";
            this.lblDouiM.Size = new System.Drawing.Size(17, 12);
            this.lblDouiM.TabIndex = 65;
            this.lblDouiM.Text = "月";
            // 
            // lblDouiD
            // 
            this.lblDouiD.AutoSize = true;
            this.lblDouiD.Location = new System.Drawing.Point(275, 14);
            this.lblDouiD.Name = "lblDouiD";
            this.lblDouiD.Size = new System.Drawing.Size(17, 12);
            this.lblDouiD.TabIndex = 68;
            this.lblDouiD.Text = "日";
            // 
            // lblDouiY
            // 
            this.lblDouiY.AutoSize = true;
            this.lblDouiY.Location = new System.Drawing.Point(172, 14);
            this.lblDouiY.Name = "lblDouiY";
            this.lblDouiY.Size = new System.Drawing.Size(17, 12);
            this.lblDouiY.TabIndex = 64;
            this.lblDouiY.Text = "年";
            // 
            // vbDouiM
            // 
            this.vbDouiM.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiM.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiM.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiM.Location = new System.Drawing.Point(194, 5);
            this.vbDouiM.MaxLength = 2;
            this.vbDouiM.Name = "vbDouiM";
            this.vbDouiM.NewLine = false;
            this.vbDouiM.Size = new System.Drawing.Size(28, 23);
            this.vbDouiM.TabIndex = 63;
            this.vbDouiM.TextV = "";
            // 
            // vbDouiY
            // 
            this.vbDouiY.BackColor = System.Drawing.SystemColors.Info;
            this.vbDouiY.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vbDouiY.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.vbDouiY.Location = new System.Drawing.Point(143, 5);
            this.vbDouiY.MaxLength = 2;
            this.vbDouiY.Name = "vbDouiY";
            this.vbDouiY.NewLine = false;
            this.vbDouiY.Size = new System.Drawing.Size(28, 23);
            this.vbDouiY.TabIndex = 62;
            this.vbDouiY.TextV = "";
            // 
            // checkBoxVisitKasan
            // 
            this.checkBoxVisitKasan.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisitKasan.CheckedV = false;
            this.checkBoxVisitKasan.Location = new System.Drawing.Point(432, 24);
            this.checkBoxVisitKasan.Name = "checkBoxVisitKasan";
            this.checkBoxVisitKasan.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisitKasan.Size = new System.Drawing.Size(87, 24);
            this.checkBoxVisitKasan.TabIndex = 76;
            this.checkBoxVisitKasan.Text = "加算有り";
            this.checkBoxVisitKasan.UseVisualStyleBackColor = false;
            // 
            // lblstart
            // 
            this.lblstart.AutoSize = true;
            this.lblstart.Location = new System.Drawing.Point(293, 59);
            this.lblstart.Name = "lblstart";
            this.lblstart.Size = new System.Drawing.Size(41, 12);
            this.lblstart.TabIndex = 36;
            this.lblstart.Text = "開始日";
            // 
            // lblDrCode
            // 
            this.lblDrCode.AutoSize = true;
            this.lblDrCode.Location = new System.Drawing.Point(429, 55);
            this.lblDrCode.Name = "lblDrCode";
            this.lblDrCode.Size = new System.Drawing.Size(89, 12);
            this.lblDrCode.TabIndex = 21;
            this.lblDrCode.Text = "柔整師登録番号";
            // 
            // lblDrCode01
            // 
            this.lblDrCode01.AutoSize = true;
            this.lblDrCode01.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblDrCode01.Location = new System.Drawing.Point(553, 72);
            this.lblDrCode01.Name = "lblDrCode01";
            this.lblDrCode01.Size = new System.Drawing.Size(37, 24);
            this.lblDrCode01.TabIndex = 23;
            this.lblDrCode01.Text = "[協]: 0\r\n[契]: 1";
            // 
            // verifyBoxDays
            // 
            this.verifyBoxDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDays.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDays.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDays.Location = new System.Drawing.Point(595, 73);
            this.verifyBoxDays.MaxLength = 2;
            this.verifyBoxDays.Name = "verifyBoxDays";
            this.verifyBoxDays.NewLine = false;
            this.verifyBoxDays.Size = new System.Drawing.Size(40, 23);
            this.verifyBoxDays.TabIndex = 130;
            this.verifyBoxDays.TextV = "";
            // 
            // verifyBoxDrCode
            // 
            this.verifyBoxDrCode.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrCode.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxDrCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxDrCode.Location = new System.Drawing.Point(431, 72);
            this.verifyBoxDrCode.Name = "verifyBoxDrCode";
            this.verifyBoxDrCode.NewLine = false;
            this.verifyBoxDrCode.Size = new System.Drawing.Size(118, 23);
            this.verifyBoxDrCode.TabIndex = 120;
            this.verifyBoxDrCode.TextV = "";
            // 
            // verifyBoxStart
            // 
            this.verifyBoxStart.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxStart.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxStart.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxStart.Location = new System.Drawing.Point(295, 73);
            this.verifyBoxStart.MaxLength = 2;
            this.verifyBoxStart.Name = "verifyBoxStart";
            this.verifyBoxStart.NewLine = false;
            this.verifyBoxStart.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxStart.TabIndex = 113;
            this.verifyBoxStart.TextV = "";
            // 
            // labelFusyoCnt
            // 
            this.labelFusyoCnt.AutoSize = true;
            this.labelFusyoCnt.Location = new System.Drawing.Point(292, 12);
            this.labelFusyoCnt.Name = "labelFusyoCnt";
            this.labelFusyoCnt.Size = new System.Drawing.Size(41, 12);
            this.labelFusyoCnt.TabIndex = 35;
            this.labelFusyoCnt.Text = "負傷数";
            // 
            // verifyBoxFinish
            // 
            this.verifyBoxFinish.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFinish.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFinish.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFinish.Location = new System.Drawing.Point(343, 73);
            this.verifyBoxFinish.MaxLength = 2;
            this.verifyBoxFinish.Name = "verifyBoxFinish";
            this.verifyBoxFinish.NewLine = false;
            this.verifyBoxFinish.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxFinish.TabIndex = 114;
            this.verifyBoxFinish.TextV = "";
            // 
            // checkBoxVisit
            // 
            this.checkBoxVisit.BackColor = System.Drawing.SystemColors.Info;
            this.checkBoxVisit.CheckedV = false;
            this.checkBoxVisit.Location = new System.Drawing.Point(335, 24);
            this.checkBoxVisit.Name = "checkBoxVisit";
            this.checkBoxVisit.Padding = new System.Windows.Forms.Padding(7, 3, 0, 0);
            this.checkBoxVisit.Size = new System.Drawing.Size(87, 24);
            this.checkBoxVisit.TabIndex = 75;
            this.checkBoxVisit.Text = "往料有り";
            this.checkBoxVisit.UseVisualStyleBackColor = false;
            // 
            // lblfinish
            // 
            this.lblfinish.AutoSize = true;
            this.lblfinish.Location = new System.Drawing.Point(341, 59);
            this.lblfinish.Name = "lblfinish";
            this.lblfinish.Size = new System.Drawing.Size(41, 12);
            this.lblfinish.TabIndex = 38;
            this.lblfinish.Text = "終了日";
            // 
            // verifyBoxFushoCount
            // 
            this.verifyBoxFushoCount.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxFushoCount.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxFushoCount.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxFushoCount.Location = new System.Drawing.Point(294, 26);
            this.verifyBoxFushoCount.MaxLength = 2;
            this.verifyBoxFushoCount.Name = "verifyBoxFushoCount";
            this.verifyBoxFushoCount.NewLine = false;
            this.verifyBoxFushoCount.Size = new System.Drawing.Size(28, 23);
            this.verifyBoxFushoCount.TabIndex = 70;
            this.verifyBoxFushoCount.TextV = "";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(595, 59);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(53, 12);
            this.labelDays.TabIndex = 24;
            this.labelDays.Text = "診療日数";
            // 
            // verifyBoxF5
            // 
            this.verifyBoxF5.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF5.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF5.Location = new System.Drawing.Point(954, 102);
            this.verifyBoxF5.Name = "verifyBoxF5";
            this.verifyBoxF5.NewLine = false;
            this.verifyBoxF5.Size = new System.Drawing.Size(20, 23);
            this.verifyBoxF5.TabIndex = 14;
            this.verifyBoxF5.TextV = "";
            this.verifyBoxF5.Visible = false;
            this.verifyBoxF5.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF4
            // 
            this.verifyBoxF4.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF4.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF4.Location = new System.Drawing.Point(906, 102);
            this.verifyBoxF4.Name = "verifyBoxF4";
            this.verifyBoxF4.NewLine = false;
            this.verifyBoxF4.Size = new System.Drawing.Size(20, 23);
            this.verifyBoxF4.TabIndex = 12;
            this.verifyBoxF4.TextV = "";
            this.verifyBoxF4.Visible = false;
            this.verifyBoxF4.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF3
            // 
            this.verifyBoxF3.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF3.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF3.Location = new System.Drawing.Point(954, 61);
            this.verifyBoxF3.Name = "verifyBoxF3";
            this.verifyBoxF3.NewLine = false;
            this.verifyBoxF3.Size = new System.Drawing.Size(20, 23);
            this.verifyBoxF3.TabIndex = 10;
            this.verifyBoxF3.TextV = "";
            this.verifyBoxF3.Visible = false;
            this.verifyBoxF3.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxCharge
            // 
            this.verifyBoxCharge.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxCharge.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxCharge.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxCharge.Location = new System.Drawing.Point(98, 73);
            this.verifyBoxCharge.Name = "verifyBoxCharge";
            this.verifyBoxCharge.NewLine = false;
            this.verifyBoxCharge.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxCharge.TabIndex = 110;
            this.verifyBoxCharge.TextV = "";
            // 
            // verifyBoxTotal
            // 
            this.verifyBoxTotal.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTotal.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxTotal.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxTotal.Location = new System.Drawing.Point(9, 73);
            this.verifyBoxTotal.Name = "verifyBoxTotal";
            this.verifyBoxTotal.NewLine = false;
            this.verifyBoxTotal.Size = new System.Drawing.Size(83, 23);
            this.verifyBoxTotal.TabIndex = 100;
            this.verifyBoxTotal.TextV = "";
            // 
            // verifyBoxF2
            // 
            this.verifyBoxF2.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF2.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF2.Location = new System.Drawing.Point(906, 61);
            this.verifyBoxF2.Name = "verifyBoxF2";
            this.verifyBoxF2.NewLine = false;
            this.verifyBoxF2.Size = new System.Drawing.Size(20, 23);
            this.verifyBoxF2.TabIndex = 8;
            this.verifyBoxF2.TextV = "";
            this.verifyBoxF2.Visible = false;
            this.verifyBoxF2.TextChanged += new System.EventHandler(this.fushoVerifyBox_TextChanged);
            // 
            // verifyBoxF1M
            // 
            this.verifyBoxF1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1M.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1M.Location = new System.Drawing.Point(52, 25);
            this.verifyBoxF1M.MaxLength = 2;
            this.verifyBoxF1M.Name = "verifyBoxF1M";
            this.verifyBoxF1M.NewLine = false;
            this.verifyBoxF1M.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1M.TabIndex = 41;
            this.verifyBoxF1M.TextV = "";
            // 
            // verifyBoxF1Y
            // 
            this.verifyBoxF1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1Y.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1Y.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.verifyBoxF1Y.Location = new System.Drawing.Point(9, 25);
            this.verifyBoxF1Y.MaxLength = 2;
            this.verifyBoxF1Y.Name = "verifyBoxF1Y";
            this.verifyBoxF1Y.NewLine = false;
            this.verifyBoxF1Y.Size = new System.Drawing.Size(27, 23);
            this.verifyBoxF1Y.TabIndex = 40;
            this.verifyBoxF1Y.TextV = "";
            // 
            // verifyBoxF1
            // 
            this.verifyBoxF1.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxF1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.verifyBoxF1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.verifyBoxF1.Location = new System.Drawing.Point(954, 19);
            this.verifyBoxF1.Name = "verifyBoxF1";
            this.verifyBoxF1.NewLine = false;
            this.verifyBoxF1.Size = new System.Drawing.Size(20, 23);
            this.verifyBoxF1.TabIndex = 5;
            this.verifyBoxF1.TextV = "";
            this.verifyBoxF1.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(954, 87);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 12);
            this.label22.TabIndex = 13;
            this.label22.Text = "負傷名5";
            this.label22.Visible = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(906, 87);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 12);
            this.label21.TabIndex = 11;
            this.label21.Text = "負傷名4";
            this.label21.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(954, 46);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 12);
            this.label20.TabIndex = 9;
            this.label20.Text = "負傷名3";
            this.label20.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(906, 46);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 7;
            this.label11.Text = "負傷名2";
            this.label11.Visible = false;
            // 
            // lblF1
            // 
            this.lblF1.AutoSize = true;
            this.lblF1.Location = new System.Drawing.Point(7, 10);
            this.lblF1.Name = "lblF1";
            this.lblF1.Size = new System.Drawing.Size(29, 12);
            this.lblF1.TabIndex = 2;
            this.lblF1.Text = "初検";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(954, 4);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 0;
            this.label9.Text = "負傷名1";
            this.label9.Visible = false;
            // 
            // lblF1M
            // 
            this.lblF1M.AutoSize = true;
            this.lblF1M.Location = new System.Drawing.Point(79, 36);
            this.lblF1M.Name = "lblF1M";
            this.lblF1M.Size = new System.Drawing.Size(17, 12);
            this.lblF1M.TabIndex = 6;
            this.lblF1M.Text = "月";
            // 
            // lblF1Y
            // 
            this.lblF1Y.AutoSize = true;
            this.lblF1Y.Location = new System.Drawing.Point(36, 36);
            this.lblF1Y.Name = "lblF1Y";
            this.lblF1Y.Size = new System.Drawing.Size(17, 12);
            this.lblF1Y.TabIndex = 4;
            this.lblF1Y.Text = "年";
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(9, 57);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(53, 12);
            this.labelTotal.TabIndex = 15;
            this.labelTotal.Text = "合計金額";
            // 
            // labelCharge
            // 
            this.labelCharge.AutoSize = true;
            this.labelCharge.Location = new System.Drawing.Point(98, 57);
            this.labelCharge.Name = "labelCharge";
            this.labelCharge.Size = new System.Drawing.Size(53, 12);
            this.labelCharge.TabIndex = 17;
            this.labelCharge.Text = "請求金額";
            // 
            // labelInputerName
            // 
            this.labelInputerName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelInputerName.Location = new System.Drawing.Point(272, 695);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(186, 23);
            this.labelInputerName.TabIndex = 13;
            this.labelInputerName.Text = "1\r\n2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(11, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "続紙: --\r\n不要: ++";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(321, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 721);
            this.splitter2.TabIndex = 40;
            this.splitter2.TabStop = false;
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 721);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panelRight);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InputForm";
            this.Text = "OCR Check";
            this.Shown += new System.EventHandler(this.inputForm_Shown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.Controls.SetChildIndex(this.panelRight, 0);
            this.Controls.SetChildIndex(this.splitter2, 0);
            this.Controls.SetChildIndex(this.panelLeft, 0);
            this.panelLeft.ResumeLayout(false);
            this.panelWholeImage.ResumeLayout(false);
            this.panelWholeImage.PerformLayout();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelUpper.ResumeLayout(false);
            this.panelUpper.PerformLayout();
            this.panelTotal.ResumeLayout(false);
            this.panelTotal.PerformLayout();
            this.pDoui.ResumeLayout(false);
            this.pDoui.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Label labelImageName;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelWholeImage;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Label labelCharge;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelDays;
        private UserControlImage userControlImage1;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Label lblDrCode;
        private System.Windows.Forms.Panel panelTotal;
        private System.Windows.Forms.Label lblDrCode01;
        private System.Windows.Forms.Label lblF1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblF1M;
        private System.Windows.Forms.Label lblF1Y;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxF5;
        private VerifyBox verifyBoxF4;
        private VerifyBox verifyBoxF3;
        private VerifyBox verifyBoxCharge;
        private VerifyBox verifyBoxTotal;
        private VerifyBox verifyBoxDays;
        private VerifyBox verifyBoxF2;
        private VerifyBox verifyBoxF1;
        private VerifyBox verifyBoxDrCode;
        private VerifyBox verifyBoxF1M;
        private VerifyBox verifyBoxF1Y;
        private System.Windows.Forms.Button buttonBack;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label labelFusyoCnt;
        private VerifyBox verifyBoxFushoCount;
        private VerifyCheckBox checkBoxVisit;
        private VerifyBox verifyBoxFinish;
        private VerifyBox verifyBoxStart;
        private System.Windows.Forms.Label lblstart;
        private System.Windows.Forms.Label lblfinish;
        private VerifyBox verifyBoxKyufuRate;
        private System.Windows.Forms.Label lblRatio;
        private System.Windows.Forms.Label lblRatioNum;
        private VerifyBox verifyBoxinum;
        private System.Windows.Forms.Label lblinum;
        private VerifyCheckBox checkBoxVisitKasan;
        private System.Windows.Forms.Panel pDoui;
        private System.Windows.Forms.Label label12;
        private VerifyBox vbDouiG;
        private System.Windows.Forms.Label lblDoui;
        private VerifyBox vbDouiD;
        private System.Windows.Forms.Label lblDouiM;
        private System.Windows.Forms.Label lblDouiD;
        private System.Windows.Forms.Label lblDouiY;
        private VerifyBox vbDouiM;
        private VerifyBox vbDouiY;
        private System.Windows.Forms.Label lblHosCode2;
        private System.Windows.Forms.Label lblHosCode1;
        private VerifyBox verifyBoxIryokikan2;
        private VerifyBox verifyBoxIryokikan1;
        private System.Windows.Forms.Label lblinumhead;
        private System.Windows.Forms.Panel panelUpper;
    }
}