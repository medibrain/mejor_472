﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading.Tasks;

namespace Mejor.ShimaneKoiki
{
    class ViewerDataExport
    {
        public static bool ExportViewerData(int cym,out string strPath)
        {
            string log = string.Empty;
            string fileName;

            //2019/10/24furukawa追加
            strPath = string.Empty;

            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }
            
            var dir = Path.GetDirectoryName(fileName);

            strPath = dir;

            var dataFile = dir + "\\" + cym.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            fileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cym);

                //20190930140934 furukawa st ////////////////////////
                //広域からのデータはないので削除

                /*
                wf.LogPrint("提供データを取得しています");
                var rrs = KoikiData.SelectAym(cym);
                var dic = new Dictionary<int, KoikiData>();
                rrs.ForEach(rr => { if (rr.Aid != 0) dic.Add(rr.Aid, rr); });
                */
                //20190930140934 furukawa ed ////////////////////////



                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                int receCount = 0;
                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerData.Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];
                        var vd = createViewData(app);

                        sw.WriteLine(vd.CreateCsvLine());
                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }


                //20191004144302 furukawa st ////////////////////////
                //照会データの出力
                

                //更新データが照会データのこと？→あってる（2019/10/04伸作さん
                //なぜ６ヶ月前？→保険者によって半年ごとに作業する場合があるので

                wf.LogPrint("更新データの作成中です");
                if (!ViewerUpdateData.Export(DateTimeEx.Int6YmAddMonth(cym, -6), cym, dir, wf))
                {
                    wf.LogPrint("更新データの出力に失敗しました。");
                    return false;
                }

                //20191004144302 furukawa ed ////////////////////////



                if (!ViewerData.CreateInfo(fileName, Insurer.CurrrentInsurer.InsurerName, cym, receCount))
                    return false;

                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\nビューアデータ出力数   :{receCount}";
                log += lastMsg;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                using (var sw = new StreamWriter(logFn, false, Encoding.UTF8)) sw.Write(log);
                wf.Dispose();
            }
            return true;
        }











        private static ViewerData createViewData(App app)
        {
            var vd = new ViewerData();
            
            vd.AID = app.Aid;
            vd.CYM = app.CYM;                               //処理年月
            vd.YM = app.YM;                                 //施術年//施術月(診療年月)
            vd.AppType = app.AppType;                       //申請書タイプ
            vd.Num = app.HihoNum;                           //被保険者番号
                            
            int.TryParse(app.FushoFirstDate1.ToString("yyyyMMdd"), out int tmp);
            vd.ShokenDate = tmp;                            //初検日

            
            //負傷数
            if (app.TaggedDatas.count>=1) vd.Fusho1 = "1";
            if (app.TaggedDatas.count >= 2) vd.Fusho2 = "1";
            if (app.TaggedDatas.count >= 3) vd.Fusho3 = "1";
            if (app.TaggedDatas.count >= 4) vd.Fusho4 = "1";
            if (app.TaggedDatas.count >= 5) vd.Fusho5 = "1";

            
            vd.NewCont = app.NewContType;
            vd.Total = app.Total;                           //合計金額
            vd.Charge = app.Charge;                         //請求金額
            vd.Days = app.CountedDays;                      //診療実日数
            vd.VisitFee = app.Distance == 999;              //往療料有無
            vd.DrNum = app.DrNum;                           //柔整師登録番号

            vd.ImageFile = app.Aid.ToString() + ".tif";     //画像ファイル名

            

            return vd;
        }

        private static bool createNotMatchCsv(KoikiData k, StreamWriter sw)
        {
            var sl = new string[16];

            sl[0] = k.Num;
            sl[1] = k.Name;
            sl[2] = k.Kana;
            sl[3] = k.Zip;
            sl[4] = k.Add;
            sl[5] = k.DestZip;
            sl[6] = k.DestAdd;
            sl[7] = k.DestName;
            sl[8] = k.DrNum;
            sl[9] = string.Empty;
            sl[10] = string.Empty;
            sl[11] = k.MediYM.ToString("000000");
            sl[12] = k.AppYM.ToString("000000");
            sl[13] = k.Days.ToString();
            sl[14] = k.Total.ToString();
            sl[15] = k.ReceNum;

            try
            {
                sw.WriteLine("\"" + string.Join("\",\"", sl) + "\"");
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\nCSVデータの書き込みに失敗しました");
                return false;
            }

            return true;
        }
    }
}
