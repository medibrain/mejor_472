﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor.ShimaneKoiki
{
    public partial class ExportForm : Form
    {
        public  string dtvalKettei = string.Empty;
        public  string dtvalUketuke = string.Empty;
        public string dtvalSisyutu = string.Empty;
        public  DialogResult ret;

        public ExportForm()
        {
            InitializeComponent();
        }

        private void btnAbort_Click(object sender, EventArgs e)
        {
            ret = DialogResult.Cancel;
            Close();
        }

        private void btnOutput_Click(object sender, EventArgs e)
        {
            //20191107132230 furukawa st ////////////////////////
            //支給年月日も手入力になったのでチェック
            
            if (txtUketuke.Text.Length != 7 || txtKettei.Text.Length != 7 || txtSisyutu.Text.Length!=7)
                    //if (txtUketuke.Text.Length != 7 || txtKettei.Text.Length != 7)
            //20191107132230 furukawa ed ////////////////////////
            {
                MessageBox.Show("4310430型で入力してください");
                ret = DialogResult.Cancel;
                return;
            }


            dtvalKettei = txtKettei.Text;
            dtvalUketuke = txtUketuke.Text;
            dtvalSisyutu = txtSisyutu.Text;
            ret = DialogResult.OK;
            Close();
        }

        private void ExportForm_Load(object sender, EventArgs e)
        {
            int ym = DateTime.Now.Year * 100 + DateTime.Now.Month;
            
            txtKettei.Text = (DateTimeEx.GetEraNumberYearFromYYYYMM(ym) * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day).ToString();
            txtUketuke.Text = (DateTimeEx.GetEraNumberYearFromYYYYMM(ym) * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day).ToString();

        }
    }
}
