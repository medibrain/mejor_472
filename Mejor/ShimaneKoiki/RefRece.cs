﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.ShimaneKoiki
{
    class RefRece
    {
        [DB.DbAttribute.PrimaryKey]
        public int RrID { get; set; }
        public int ImportID { get; set; }
        public int CYM { get; set; }
        public int YM { get; set; }
        public string InsNum { get; set; } = string.Empty;
        public string InsName { get; set; } = string.Empty;
        public string Num { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Kana { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Add { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdd { get; set; } = string.Empty;
        public SEX Sex { get; set; }
        public DateTime Birth { get; set; }
        public string DrNum { get; set; } = string.Empty;
        public string DrName { get; set; } = string.Empty;
        public string ClinicNum { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public int Distance100 { get; set; }
        public int Days { get; set; }
        public int Total { get; set; }
        public int Charge { get; set; }
        public int Partial { get; set; }
        public APP_TYPE AppType { get; set; }
        public int Aid { get; set; }

        private static void readJyushinFile(string fileName, List<RefRece> list)
        {
            //被保番をキー
            var dic = new Dictionary<string, List<RefRece>>();
            list.ForEach(rr =>
            {
                if (!dic.ContainsKey(rr.Num)) dic.Add(rr.Num, new List<RefRece>());
                dic[rr.Num].Add(rr);
            });

            var csv = CommonTool.CsvImportShiftJis(fileName);

            for (int i = 0; i < csv.Count; i++)
            {
                if (csv[i].Length < 28) continue;
                int cym, ym, birth, days, total, partial, charge, appType;
                if (!int.TryParse(csv[i][27], out cym)) continue;
                if (!int.TryParse(csv[i][15], out ym)) continue;
                if (!int.TryParse(csv[i][3], out appType)) continue;
                int.TryParse(csv[i][9], out birth);
                int.TryParse(csv[i][11], out days);
                int.TryParse(csv[i][12], out total);
                int.TryParse(csv[i][13], out charge);
                int.TryParse(csv[i][14], out partial);

                //1か月プラス
                cym = cym % 100 == 12 ? cym + 89 : cym + 1;

                var rr = new RefRece();
                rr.CYM = DateTimeEx.GetAdYearMonthFromJyymm(cym);
                rr.YM = DateTimeEx.GetAdYearMonthFromJyymm(ym);
                rr.Num = csv[i][7].Trim();
                rr.Kana = csv[i][8].Trim();
                rr.Sex = csv[i][10].Trim() == "女" ? SEX.女 : SEX.男;
                rr.Birth = DateTimeEx.GetDateFromJInt7(birth);
                rr.DrNum = csv[i][16].Trim();
                rr.ClinicNum = csv[i][25].Trim();
                rr.Days = days;
                rr.Total = total;
                rr.Partial = partial;
                rr.Charge = charge;
                rr.AppType = appType == 1 ? APP_TYPE.鍼灸 : appType == 2 ? APP_TYPE.あんま : APP_TYPE.NULL;

                if (dic.ContainsKey(rr.Num) &&
                    dic[rr.Num].Any(r => r.CYM == rr.CYM && r.YM == rr.YM && r.DrNum == rr.DrNum && r.Total == rr.Total))
                    continue;

                list.Add(rr);
            }
        }

        /// <summary>
        /// 62Tファイルを読み込み、申請情報を作成します
        /// </summary>
        /// <param name="list"></param>
        private static List<RefRece> read62T(string fileName)
        {
            var l = new List<RefRece>();
            var csv = CommonTool.CsvImportUTF8(fileName);

            foreach (var item in csv)
            {
                int ym, cym, charge, partial, total, birth, sex;
                if (!int.TryParse(item[10], out ym)) continue;
                if (!int.TryParse(item[23], out cym)) continue;
                if (!int.TryParse(item[53], out sex)) continue;
                if (!int.TryParse(item[52], out birth)) continue;
                if (!int.TryParse(item[42].Substring(1), out charge)) continue;
                if (!int.TryParse(item[92], out total)) continue;
                if (!int.TryParse(item[93], out partial)) continue;

                var rr = new RefRece();
                rr.YM = ym;
                rr.CYM = cym / 100; //日付まであるため下2ケタカット
                rr.InsNum = item[2].Trim();
                rr.InsName = item[3].Trim();
                rr.Num = item[4];
                rr.Name = item[51];
                rr.Zip = item[61].Trim();
                rr.Add = item[58].Trim() + item[59].Trim() + item[60].Trim();
                rr.DestZip = item[66].Trim();
                rr.DestAdd = item[63].Trim() + item[64].Trim() + item[65].Trim();
                rr.Sex = sex == 2 ? SEX.女 : SEX.男;
                rr.Birth = birth.ToDateTime();
                rr.Total = total;
                rr.Charge = charge;
                rr.Partial = partial;
                rr.DrNum = item[46] + item[47] + item[48] + item[49];
                rr.AppType = item[94].Contains("はり") ? APP_TYPE.鍼灸 : item[94].Contains("あんま") ? APP_TYPE.あんま : APP_TYPE.NULL;
                l.Add(rr);
            }

            return l;
        }

        /// <summary>
        /// ２つの62Tデータを統合します
        /// </summary>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <returns></returns>
        private static List<RefRece> Marge62T(List<RefRece> list1, List<RefRece> list2)
        {
            var dic = new Dictionary<string, List<RefRece>>();

            foreach (var item in list1)
            {
                if (!dic.ContainsKey(item.Num)) dic.Add(item.Num, new List<RefRece>());
                dic[item.Num].Add(item);
            }

            foreach (var item in list2)
            {
                if (!dic.ContainsKey(item.Num)) dic.Add(item.Num, new List<RefRece>());
                if (dic[item.Num].Exists(r =>
                    r.YM == item.YM && r.CYM == item.CYM && r.Total == item.Total
                    && r.DrNum == item.DrNum && r.AppType == item.AppType)) continue;

                dic[item.Num].Add(item);
            }

            var l = new List<RefRece>();
            foreach (var item in dic.Values)
            {
                l.AddRange(item);
            }
            return l;
        }

        /// <summary>
        /// 施術所台帳ファイルを読み込み、施術師名と施術所名を取得します
        /// </summary>
        /// <param name="list"></param>
        private static void readClinic(string fileName, List<RefRece> list)
        {
            var dic = new Dictionary<string, string[]>();
            var l = CommonTool.CsvImportShiftJis(fileName);

            //ファイルの内容を登録番号で整理
            foreach (var item in l)
            {
                if (!dic.ContainsKey(item[1])) dic.Add(item[1],item);
                else dic[item[1]] = item;
            }

            //RefReceに情報を追加
            foreach (var item in list)
            {
                if (!dic.ContainsKey(item.DrNum))
                    continue;
                var r = dic[item.DrNum];
                item.DrName = r[2].Trim();
                item.ClinicName = r[7].Trim();
            }
        }

        private static void marge(List<RefRece> main, List<RefRece> sub)
        {
            //被保番をキー
            var dic = new Dictionary<string, List<RefRece>>();
            sub.ForEach(rr =>
            {
                if (!dic.ContainsKey(rr.Num)) dic.Add(rr.Num, new List<RefRece>());
                dic[rr.Num].Add(rr);
            });

            foreach (var item in main)
            {
                if (!dic.ContainsKey(item.Num))
                    continue;

                var rrs = dic[item.Num].Where(r =>
                    r.CYM == item.CYM && r.YM == item.YM && r.Total == item.Total && r.AppType == item.AppType);
                if (rrs.Count() == 0)
                    continue;
                if (rrs.Count() > 1)
                {
                    rrs = rrs.Where(r => r.DrNum == item.DrNum);
                    if (rrs.Count() != 1)
                        continue;
                }

                var rr = rrs.First();

                item.Kana = rr.Kana;
                item.DrNum = rr.DrNum;
                item.ClinicNum = rr.ClinicNum;
                item.Days = rr.Days;
            }
        }

        /// <summary>
        /// 鍼灸往療/あんまマッサージ往療の各csvからデータを読み込み、距離情報を取得します。
        /// また、62Tになかった施術情報も追加します
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="list"></param>
        private static void readOryo(string fileName, List<RefRece> list)
        {

        }

        public static bool Import(string dirName)
        {
            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("インポートを開始します");

            try
            {
                var files = System.IO.Directory.GetFiles(dirName);
                var jyushinFiles = files.Where(s => System.IO.Path.GetFileName(s).Contains("受診者別一覧"));
                var baseFiles = files.Where(s => System.IO.Path.GetFileName(s).Contains("KD08F062T"));
                var drFiles = files.Where(s => System.IO.Path.GetFileName(s).Contains("施術所台帳"));

                if (baseFiles.Count() == 0)
                {
                    System.Windows.Forms.MessageBox.Show("62Tファイルが見つかりません。" +
                        "指定したフォルダのファイルを確認してください。インポートを中止します。");
                    return false;
                }

                if (baseFiles.Count() == 1)
                {
                    var res = System.Windows.Forms.MessageBox.Show("62Tファイルが1ファイルのみです。" +
                        "このままインポートをつづけてもよろしいですか？", "",
                        System.Windows.Forms.MessageBoxButtons.OKCancel,
                        System.Windows.Forms.MessageBoxIcon.Question);
                    if (res != System.Windows.Forms.DialogResult.OK) return false;
                }

                if (jyushinFiles.Count() < 1)
                {
                    System.Windows.Forms.MessageBox.Show("受信者別一覧ファイルが見つかりません。" +
                        "指定したフォルダのファイルを確認してください。インポートを中止します。");
                    return false;
                }

                if (drFiles.Count() != 1)
                {
                    System.Windows.Forms.MessageBox.Show("施術所台帳ファイルが見つからない、または複数存在します。" +
                        "指定したフォルダのファイルを確認してください。インポートを中止します。");
                    return false;
                }

                wf.LogPrint("62Tファイルを読み込み開始します");
                var list = new List<RefRece>();
                foreach (var item in baseFiles)
                {
                    wf.LogPrint($"[{System.IO.Path.GetFileName(item)}]ファイルを読み込み中です");
                    var l = read62T(item);
                    list = Marge62T(l, list);
                }
                var subList = new List<RefRece>();
                foreach (var item in jyushinFiles)
                {
                    wf.LogPrint($"[{System.IO.Path.GetFileName(item)}]ファイルを読み込み中です");
                    readJyushinFile(item, subList);
                }

                wf.LogPrint("62Tと受信者別一覧を関連付けています");
                marge(list, subList);

                wf.LogPrint("施術所台帳から施術師名と施術所名を関連付けています");
                readClinic(drFiles.First(), list);

                wf.LogPrint("データベースに登録しています");
                using (var tran = DB.Main.CreateTransaction())
                {
                    var rri = RefReceImport.Create(dirName, tran);

                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].RrID = rri.RrImportID * 1000000 + i + 1;
                        list[i].ImportID = rri.RrImportID;
                    }

                    wf.SetMax(list.Count);
                    wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    //500ごとに登録
                    var c = 0;
                    var list100 = new List<RefRece>();
                    for (int i = 0; i < list.Count; i++)
                    {
                        c++;
                        list100.Add(list[i]);
                        if (c == 100)
                        {
                            if (!DB.Main.Inserts(list100, tran))
                            {
                                tran.Rollback();
                                return false;
                            }
                            wf.InvokeValue = i + 1;
                            list100.Clear();
                            c = 0;
                        }
                    }

                    //最終500未満登録
                    if (!DB.Main.Inserts(list100, tran))
                    {
                        tran.Rollback();
                        return false;
                    }

                    tran.Commit();
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            System.Windows.Forms.MessageBox.Show("インポートが完了しました");
            return true;
        }

        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInput(int cyyyymm, int yyyymm, string num, int total, APP_TYPE appType)
        {
            var l = DB.Main.Select<RefRece>(
                new { cym = cyyyymm, ym = yyyymm, num = num, total = total, apptype = (int)appType });
            return l.ToList();
        }

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cyyyymm, string num, int total)
        {
            var l = DB.Main.Select<RefRece>(
                new { cym = cyyyymm, num = num, total = total});
            return l.ToList();
        }

        /// <summary>
        /// refreceテーブル中のdidに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int did, int aid, DB.Transaction tran = null)
        {
            if (tran == null)
            {
                using (tran = DB.Main.CreateTransaction())
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = did;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            tran.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    tran.Rollback();
                    return false;
                }
            }
            else
            {
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = did;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery()) return true;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// refreceテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int cyyyymm)
        {
            var l = DB.Main.Select<RefRece>(new { cym = cyyyymm });
            return l.ToList();
        }

        /// <summary>
        /// 指定された年月のデータをすべて取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> SelectCym(int cyyyymm)
        {
            var l = DB.Main.Select<RefRece>(new { cym = cyyyymm });
            return l.ToList();
        }
        public static List<RefRece> SelectAll ()
        {
            var l = DB.Main.SelectAll<RefRece>();
            return l.ToList();
        }

        /// <summary>
        /// 指定された年月のデータをすべて取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> SelectRrID(int rrid)
        {
            var l = DB.Main.Select<RefRece>(new { rrid = rrid });
            return l.ToList();
        }

    }
}