﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Mejor.ShimaneKoiki
{
    class ExportAHK
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cyyyymm">処理年月西暦</param>
        /// <param name="strPath">柔整データのフォルダ</param>
        /// <param name="strdtKettei">決定年月日</param>
        /// <param name="strdtUketuke">受付年月日</param>
        /// <param name="strdtSisyutu">支出年月日</param>
        /// <returns></returns>
        /// 

        //20191107132605 furukawa st ////////////////////////
        //支給年月日も手入力になったので引数追加
        public static bool ExportSAMData(int cyyyymm, string strPath,string  strdtKettei,string  strdtUketuke,string strdtSisyutu)
                //public static bool ExportSAMData(int cyyyymm, string strPath, string strdtKettei, string strdtUketuke)
        //20191107132605 furukawa ed ////////////////////////

        {
            string fileName;
            string samName;

            //20191024142150 furukawa st ////////////////////////
            //柔整と同じパスにAHKを追加して別フォルダとして出力

            var dir = strPath + "ahk";
            if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);
            fileName = dir + "\\Info.txt";


                        //using (var f = new SaveFileDialog())
                        //{
                        //    f.FileName = "Info.txt";                
                        //    if (f.ShowDialog() != DialogResult.OK) return false;
                        //    fileName = f.FileName;
                        //}

            //var dir = System.IO.Path.GetDirectoryName(fileName)+"ahk";

            //20191024142150 furukawa ed ////////////////////////

            var dataFile = dir + "\\" + cyyyymm.ToString() + ".csv";
            var imgDir = dir + "\\Img";

            samName = dir + $"\\ファイル名.SAM_{DateTime.Today.ToString("yyyyMMdd")}";
            //京都広域のファイル名samName = dir + $"\\JKD08M0010201_KD08F001N.SAM_{DateTime.Today.ToString("yyyyMMdd")}";

            //System.IO.Directory.CreateDirectory(imgDir);

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();

                //あはき（aapptype=7,8)のみ取得
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetAppsWithWhere($"where a.cym={cyyyymm} and a.aapptype in (7,8) and a.ayear>0");

                //20191025143903 furukawa st ////////////////////////
                //件数0の場合ぬける

                if (apps.Count == 0)
                {
                    MessageBox.Show("あはき申請書がありません");
                    return false;
                }
                //20191025143903 furukawa ed ////////////////////////


                wf.LogPrint("データを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;


                //SAMファイル用
                FileStream fs = new FileStream(samName, FileMode.Create);

                byte[] bHdr = Sam.CreateHeader();
                fs.Write(bHdr, 0, bHdr.Length);

                int receCount = 1;
                int i = 0;

                //続紙等飛ばす
                while (apps[i].YM < 0) i++;
                
                while (i < apps.Count)
                {
                    

                    wf.InvokeValue = i;
                    var app = apps[i];

                    if (apps[i].YM < 0)
                    {
                        i++;
                        continue;                        
                    }

                    if (!app.StatusFlagCheck(StatusFlag.返戻))
                    {
                        var sum = new Sam(app);

                        byte[] b;

                        //20191107135759 furukawa st ////////////////////////
                        //支給年月日も手入力になったので引数追加
                        
                        b = sum.CreateRecord(DateTime.Now, strdtUketuke, strdtKettei,strdtSisyutu,receCount, wf);
                        //b = sum.CreateRecord(DateTime.Now, strdtUketuke, strdtKettei, receCount, wf);
                        //20191107135759 furukawa ed ////////////////////////

                        if (b.Length != 1000) wf.LogPrint($"バイト数の確認が必要です AID:{app.Aid}");
                        
                        //20200224135650 furukawa st ////////////////////////
                        //連番がダミーの時にaidを表示
                        
                        if (b.ToString().Contains("dummy")) wf.LogPrint($"連番がダミーです AID:{app.Aid}");
                        //20200224135650 furukawa ed ////////////////////////


                        fs.Write(b, 0, b.Length);
                    }
                    i++;
                    receCount++;
                }
                wf.InvokeValue = i;

                byte[] bFtr = Sam.CreateFooter(receCount-1);
                fs.Write(bFtr, 0, bFtr.Length);

                fs.Close();
                MessageBox.Show("出力が終了しました");
                return true;

            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                wf.Cancel = true;
                return false;
            }
            finally
            {
                wf.Dispose();                
            }
            

        }
    }
}