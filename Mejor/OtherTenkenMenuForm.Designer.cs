﻿namespace Mejor
{
    partial class OtherTenkenMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(227, 20);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(183, 48);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "---";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.SystemColors.Control;
            this.btn1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn1.Location = new System.Drawing.Point(38, 20);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(183, 48);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "---";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Location = new System.Drawing.Point(227, 182);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(183, 48);
            this.button8.TabIndex = 7;
            this.button8.Text = "----";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(227, 74);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(183, 48);
            this.button4.TabIndex = 3;
            this.button4.Text = "----";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(227, 128);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(183, 48);
            this.button6.TabIndex = 5;
            this.button6.Text = "----";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.Location = new System.Drawing.Point(38, 182);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(183, 48);
            this.button7.TabIndex = 6;
            this.button7.Text = "----";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(38, 128);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(183, 48);
            this.button5.TabIndex = 4;
            this.button5.Text = "----";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(38, 74);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(183, 48);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "---";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.button3_Click);
            // 
            // OtherTenkenMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 250);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.btn3);
            this.MaximizeBox = false;
            this.Name = "OtherTenkenMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "点検メニュー";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn3;
    }
}