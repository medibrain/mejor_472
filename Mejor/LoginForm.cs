﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mejor
{
    public partial class LoginForm : Form
    {
        public int LoginUserID = 0;
        public static List<User> uList = new List<User>();

        //フォームが表示されるタイミングでUserリストを作成
        public LoginForm()
        {
            InitializeComponent();


            //20210217092434 furukawa st ////////////////////////
            //フォーム色を環境に合わす            
            CommonTool.setFormColor(this);
            //20210217092434 furukawa ed ////////////////////////

            //フォームが表示されたタイミングでIDボックスにカーソルを移動
            textBoxUID.Select();
            
            User.GetUsers();
            uList = User.GetList();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int id;
            string pass;
            int.TryParse(textBoxUID.Text, out id);
            pass = textBoxPass.Text;

            foreach (var item in uList)
            {
                if (item.UserID == id && item.Pass == pass && item.Enable)
                {

                    //medivery使用可否
                    Medivery.MediveryXML.GetUseList();

                    //ユーザーIDとパスワードが一致したら返り値を設定
                    this.LoginUserID = id;
                    this.DialogResult = DialogResult.OK;
                    return;
                }
            }
            this.LoginUserID = 0;
            MessageBox.Show("正しいユーザーID、パスワードを入力してください",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            //MessageBox.Show("正しいユーザーID、パスワードを入力してください");
            textBoxUID.Focus();
        }

        //ユーザーIDが入力されたら、それに対応するユーザー名を表示
        private void textBoxUID_Leave(object sender, EventArgs e)
        {
            foreach (var item in uList)
            {
                int id;
                int.TryParse(textBoxUID.Text, out id);
                if (item.UserID == id)
                {
                    //20211014141610 furukawa st ////////////////////////
                    //無効のユーザはログインできなくする

                    if (item.Enable == false)
                    {
                        textBoxPass.Enabled = false;
                        labelUsername.Text = string.Empty;
                        MessageBox.Show("このユーザは無効です");
                        textBoxUID.Focus();
                        return;
                    }
                    else
                    {

                        textBoxPass.Enabled = true;
                        textBoxPass.Focus();
                        //20211014141610 furukawa ed ////////////////////////


                        //20211224102701 furukawa st ////////////////////////
                        //ユーザID対応のユーザ名が出たらユーザ名が発覚するので削除
                        //       labelUsername.Text = item.Name;
                        //20211224102701 furukawa ed ////////////////////////
                        return;
                    }
                }
            }

            labelUsername.Text = "";
        }

        
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                //20211224102754 furukawa st ////////////////////////
                //コントロールの順序を入れ替えたので削除
                
                SendKeys.Send("{TAB}");


                //if (textBoxPass.Focused)
                //{
                //   // buttonOK.PerformClick();
                //}
                //else
                //{
                    //SendKeys.Send("{TAB}");
                //}

                //20211224102754 furukawa ed ////////////////////////

                e.Handled = true;
            }
        }

        private void textBoxPass_Leave(object sender, EventArgs e)
        {
            //20211224102910 furukawa st ////////////////////////
            //ユーザID、パスワードが合致したらユーザ名表示
            
            int id;
            string pass;
            int.TryParse(textBoxUID.Text, out id);
            pass = textBoxPass.Text;

            foreach (var item in uList)
            {
                if (item.UserID == id && item.Pass == pass && item.Enable)
                {
                    labelUsername.Text = item.Name;
                    labelBelong.Text = item.belong;
                    return;
                }
                else
                {
                    labelUsername.Text = String.Empty;
                }
            }

            //foreach (var item in uList)
            //{
            //    int id;
            //    int.TryParse(textBoxUID.Text, out id);
            //    if (item.UserID == id)
            //    {

            //        if (item.Enable == false)
            //        {
            //            textBoxPass.Enabled = false;
            //            labelUsername.Text = string.Empty;
            //            MessageBox.Show("このユーザは無効です");
            //            textBoxUID.Focus();
            //            return;
            //        }
            //        else
            //        {




            //            labelUsername.Text = item.Name;
            //            return;
            //        }
            //    }
            //}

            //labelUsername.Text = "";

            //20211224102910 furukawa ed ////////////////////////
        }
    }
}
