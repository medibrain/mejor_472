﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mejor
{
    public partial class InputFormCore : Form
    {
        protected Label labelInfo => _labelInfo;
        protected DataGridView dataGridViewPlist => _dataGridViewPlist;
        protected Panel panelInfo => _panelInfo;
        protected Panel panelMatchCheckInfo => _panelMatchCheckInfo;
        protected RadioButton radioButtonOverlap => _radioButtonOverlap;
        protected RadioButton radioButtonNotMatch => _radioButtonNotMatch;


        //20200812180938 furukawa st ////////////////////////
        /// <summary>
        /// 入力開始時刻の変数
        /// </summary>
        protected DateTime dtstart_core = DateTime.MinValue;
        //20200812180938 furukawa ed ////////////////////////


        private Verify.VerifyMissCounter missCounter;
        private TotalCalcForm calcForm;
        private BindingSource bs;
        private NextImageForm nextImageForm = null;

        public InputFormCore()
        {
            InitializeComponent();

            KeyPreview = true;
            KeyDown += InputFormCore_KeyDown;
            KeyPress += InputFormCore_KeyPress;
            Shown += InputFormCore_Shown;
            FormClosed += InputFormCore_FormClosed;
            _dataGridViewPlist.DataSourceChanged += _dataGridViewPlist_DataSourceChanged;

        }

        private void _dataGridViewPlist_DataSourceChanged(object sender, EventArgs e)
        {
            if (!(_dataGridViewPlist.DataSource is BindingSource bs)) return;
            this.bs = bs;
            bs.CurrentChanged += Bs_CurrentChanged;
            Bs_CurrentChanged(bs, e);
        }

        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            if (nextImageForm != null)
            {
                nextImageForm.Dispose();
                nextImageForm = null;
            }
            
            App app = (App)bs.Current;
            inputMemoControl1.SetApp(app);

            //20200812181348 furukawa st ////////////////////////
            //申請書が変わった時点で入力開始時刻の取得            
            dtstart_core = DateTime.Now;
            //20200812181348 furukawa ed ////////////////////////

            if (ocrForm?.Visible ?? false)
            {
                ocrForm.App = app;
            }
        }

        private void InputFormCore_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (calcForm != null && !calcForm.IsDisposed)
                calcForm.Dispose();
        }

        private ScanGroup _sg;
        protected Scan scan { get; private set; }
        protected ScanGroup scanGroup
        {
            get => _sg;
            set
            {
                _sg = value;
                scan = Scan.Select(_sg.ScanID);
                _labelSeikyu.Text = $"処理月: {scanGroup.cyear}/{scanGroup.cmonth}";
                _labelNote.Text = "Note: " + scanGroup.note1.ToString();
                _labelScanID.Text = "ScanID: " + scanGroup.ScanID.ToString();
                _labelGroupID.Text = "GroupID: " + scanGroup.GroupID.ToString();

            }
        }
        protected List<Control> inputControls { get; private set; } = new List<Control>();

        /// <summary>
        /// 入力コントロールのあるパネル
        /// 更新すべきかどうか判断するために指定します
        /// </summary>
        protected virtual Control inputPanel { get; }

        /// <summary>
        /// リセット後にデータ変化があるかどうかを取得します
        /// </summary>
        protected bool dataChanged { get; private set; } = true;

        /// <summary>
        /// リセット後にデータ変化があることを強制指定します
        /// </summary>
        protected void setDataChanged(bool b) => dataChanged = b;

        /// <summary>
        /// DataChangedプロパティをリセットします
        /// </summary>
        protected void changedReset(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            dataChanged = false;
        }

        /// <summary>
        /// 最初の入力フィールド、または最初のエラーフィールドをフォーカスします
        /// </summary>
        /// <param name="errorOnly"></param>
        protected void focusBack(bool errorOnly)
        {
            if (errorOnly)
            {
                foreach (var item in inputControls)
                {
                    if (!item.Visible) continue;
                    if (!item.Enabled) continue;
                    if (!item.TabStop) continue;
                    if (item.BackColor == SystemColors.Info) continue;
                    item.Focus();
                    if (item is TextBox) ((TextBox)item).SelectAll();
                    return;
                }
            }
            else
            {
                foreach (var item in inputControls)
                {
                    if (!item.Visible) continue;
                    if (!item.Enabled) continue;
                    if (!item.TabStop) continue;
                    item.Focus();
                    if (item is TextBox) ((TextBox)item).SelectAll();
                    return;
                }
            }
        }

        /// <summary>
        /// 入力チェック時にエラーがあるかどうかを設定/取得
        /// </summary>
        protected bool hasError = false;

        /// <summary>
        /// 指定されたエラーに合わせて、テキストボックスの表示を調整します
        /// </summary>
        protected void setStatus(IVerifiable v, bool isError)
        {
            if (v is Control == false) return;
            if (!((Control)v).Enabled) return;
            v.SetErrorView(isError);
            hasError |= isError;
        }

        /// <summary>
        /// 負傷1入力欄のチェックを針の番号も考慮して行ないます
        /// </summary>
        /// <param name="vb"></param>
        protected void fusho1Check(VerifyBox vb)
        {
            //必ず2文字以上、もしくは1文字で数字の1～7
            vb.Text = Utility.Abc123BracketToHalf(vb.Text);
            int hariID = vb.GetIntValue();
            if (vb.Text.Trim().Length == 1)
            {
                if (hariID < 1 || 7 < hariID)
                {
                    hasError = true;
                    setStatus(vb, true);
                }
                else
                {
                    setStatus(vb, false);
                }
            }
            else if (vb.Enabled && vb.Text.Trim().Length < 2)
            {
                hasError = true;
                setStatus(vb, true);
            }
            else
            {
                setStatus(vb, false);
            }
        }

        /// <summary>
        ///  負傷2以降の入力欄のチェックを行ないます
        /// </summary>
        /// <param name="vb"></param>
        protected void fushoCheck(VerifyBox vb)
        {
            //完全に空、もしくは2文字以上
            vb.Text = Utility.Abc123BracketToHalf(vb.Text);
            int hariID = vb.GetIntValue();
            if (vb.Text.Trim().Length == 1)
            {
                if (hariID < 1 || 7 < hariID)
                {
                    hasError = true;
                    setStatus(vb, true);
                }
                else
                {
                    setStatus(vb, false);
                }
            }
            else if (vb.Enabled && 0 < vb.Text.Trim().Length && vb.Text.Trim().Length < 2)
            {
                hasError = true;
                setStatus(vb, true);
            }
            else
            {
                setStatus(vb, false);
            }
        }


        #region 日付変換
        /// <summary>
        /// 各テキストボックスを指定して和暦日付から西暦日付を取得します
        /// </summary>
        /// <param name="eraBox"></param>
        /// <param name="jYearBox"></param>
        /// <param name="monthBox"></param>
        /// <param name="DayBox"></param>
        /// <returns></returns>
        protected DateTime getDate(VerifyBox eraBox, VerifyBox jYearBox, VerifyBox monthBox, VerifyBox DayBox)
        {
            int era = eraBox.GetIntValue();
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            int d = DayBox.GetIntValue();

            //20190816112617 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更
            
                    //y = DateTimeEx.GetAdYearFromEraYear(era, y);            
            y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(era.ToString() + y.ToString("00") + m.ToString("00")))/100;
            //20190816112617 furukawa ed ////////////////////////

            return DateTimeEx.IsDate(y, m, d) ? new DateTime(y, m, d) : DateTime.MinValue;
        }

        /// <summary>
        /// 各テキストボックスを指定して和暦日付から西暦日付を取得します
        /// </summary>
        ///<param name="eraNumber">数字</param>
        /// <param name="jYearBox">verifybox年</param>
        /// <param name="monthBox">verifybox月</param>
        /// <param name="DayBox">verifybox日</param>
        /// <returns></returns>
        protected DateTime getDate(int eraNumber, VerifyBox jYearBox, VerifyBox monthBox, VerifyBox DayBox)
        {
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            int d = DayBox.GetIntValue();

            //20190816112755 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更
            
                    //y = DateTimeEx.GetAdYearFromEraYear(eraNumber, y);
            y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(eraNumber.ToString() + y.ToString("00") + m.ToString("00")))/100;
            //20190816112755 furukawa ed ////////////////////////


            return DateTimeEx.IsDate(y, m, d) ? new DateTime(y, m, d) : DateTime.MinValue;
        }
        #endregion



        /// <summary>
        ///20191211125913 furukawa 初検日の妥当性チェック。本日より20年以上前はエラーとする
        /// </summary>
        /// <param name="eraBox">和暦番号VerifyBoxコントロール</param>
        /// <param name="jYearBox">和暦年VerifyBoxコントロール</param>
        /// <param name="monthBox">月VerifyBoxコントロール</param>
        /// <param name="DayBox">日VerifyBoxコントロール</param>
        /// <returns>0=OK,1=エラー,2=警告</returns>
        protected int dateCheckNengoDate(VerifyBox eraBox, VerifyBox jYearBox, VerifyBox monthBox, VerifyBox DayBox)
        {
            int era = eraBox.GetIntValue();
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            int d = DayBox.GetIntValue();

            //負の数はエラー返し
            if (y < 0 || m < 0 || d < 0)
            {
                //負の値がある場合はすべてエラーとする

                setStatus(eraBox, true);
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                setStatus(DayBox, true);

                return 1;
            }


            //月がないためGetAdYearMonthFromJyymmに変更
            y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(era.ToString() + y.ToString("00") + m.ToString("00"))) / 100;

            if (!DateTimeEx.IsDate(y, m, d))
            {
                //日付でない場合
                setStatus(eraBox, true);
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                setStatus(DayBox, true);
                return 1;
            }
            else
            {
                if (y < DateTime.Now.Year - 30)
                {
                    //日付だが30年以上前の場合は警告
                    eraBox.BackColor = Color.LightGreen;
                    jYearBox.BackColor = Color.LightGreen;
                    monthBox.BackColor = Color.LightGreen;
                    DayBox.BackColor = Color.LightGreen;
                    return 2;
                }
                else
                {
                    //日付で30年以内の場合はOK
                    setStatus(eraBox, false);
                    setStatus(jYearBox, false);
                    setStatus(monthBox, false);
                    setStatus(DayBox, false);

                    return 0;
                }

                
            }
        }


        #region 日付入力チェック

        //20200625154820 furukawa st ////////////////////////
        //日付なし版

        /// <summary>
        /// 和暦の入力チェックを実施。正常であれば西暦を返す
        /// </summary>
        /// <param name="eraBox"></param>
        /// <param name="jYearBox"></param>
        /// <param name="monthBox"></param>        
        /// <param name="flgBlankOK">空欄許可フラグ true=許可</param>
        /// <returns>エラー＝DateTime.MinValue、正常＝西暦年月日</returns>
        /// 

        //20200803130949 furukawa st ////////////////////////
        //全空欄の場合OKのフラグを付ける        
        protected DateTime dateCheck(VerifyBox eraBox, VerifyBox jYearBox, VerifyBox monthBox,bool flgBlankOK=false)
            //protected DateTime dateCheck(VerifyBox eraBox, VerifyBox jYearBox, VerifyBox monthBox)
        //20200803130949 furukawa ed ////////////////////////
        {
            int era = eraBox.GetIntValue();
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            int d = 1;

            //20200803131041 furukawa st ////////////////////////
            //全項目－9（空欄）の場合のみOKとする
            
            if (flgBlankOK)
            {
                if (era < 0 && y < 0 && m < 0 )
                {
                    //全項目空欄時のみOK
                    setStatus(eraBox, false);
                    setStatus(jYearBox, false);
                    setStatus(monthBox, false);
                    return DateTime.MinValue;
                }
            }
            //20200803131041 furukawa ed ////////////////////////


            //負の数はエラー返し
            if (y < 0 || m < 0 || d < 0)
            {
                //負の値がある場合はすべてエラーとする

                setStatus(eraBox, true);
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                return DateTime.MinValue;
            }

            y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(era.ToString() + y.ToString("00") + m.ToString("00"))) / 100;

            if (!DateTimeEx.IsDate(y, m, d))
            {
                setStatus(eraBox, true);
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                
                return DateTime.MinValue;
            }
            else
            {
                setStatus(eraBox, false);
                setStatus(jYearBox, false);
                setStatus(monthBox, false);
                
                return new DateTime(y, m, d);
            }
        }


        /// <summary>
        /// 和暦の入力チェックを実施。正常であれば西暦を返す
        /// </summary>
        /// <param name="eraBox"></param>
        /// <param name="jYearBox"></param>
        /// <param name="monthBox"></param>
        /// <param name="DayBox"></param>
        /// <param name="flgBlankOK">空欄許可フラグ true=許可</param>
        /// <returns>エラー＝DateTime.MinValue、正常＝西暦年月日</returns>
        
            
            
            
        //20200803130534 furukawa st ////////////////////////
        //全空欄の場合OKのフラグを付ける
        
        protected DateTime dateCheck(VerifyBox eraBox, VerifyBox jYearBox, VerifyBox monthBox, VerifyBox DayBox,bool flgBlankOK=false)
                //protected DateTime dateCheck(VerifyBox eraBox, VerifyBox jYearBox, VerifyBox monthBox, VerifyBox DayBox)
        //20200803130534 furukawa ed ////////////////////////

        {
            int era = eraBox.GetIntValue();
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            int d = DayBox.GetIntValue();

            //20200803130625 furukawa st ////////////////////////
            //全項目－9（空欄）の場合のみOKとする
            
            if (flgBlankOK)
            {
                if (era < 0 && y < 0 && m < 0 && d < 0)
                {
                    //全項目空欄時のみOK
                    setStatus(eraBox, false);
                    setStatus(jYearBox, false);
                    setStatus(monthBox, false);
                    setStatus(DayBox, false);
                    return DateTime.MinValue;
                }
            }
            //20200803130625 furukawa ed ////////////////////////


            //20190824224423 furukawa st ////////////////////////
            //負の数はエラー返し

            if (y < 0 || m < 0 || d < 0)
            {
                //20190905200342 furukawa st ////////////////////////
                //負の値がある場合はすべてエラーとする
                
                setStatus(eraBox, true);
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                setStatus(DayBox, true);
                //20190905200342 furukawa ed ////////////////////////

                return DateTime.MinValue;
            }
            //20190824224423 furukawa ed ////////////////////////


            //20190816113017 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更

            //y = DateTimeEx.GetAdYearFromEraYear(era, y);
            y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(era.ToString() + y.ToString("00") + m.ToString("00")))/100;
            //20190816113017 furukawa ed ////////////////////////

            if (!DateTimeEx.IsDate(y, m, d))
            {
                setStatus(eraBox, true);
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                setStatus(DayBox, true);
                return DateTime.MinValue;
            }
            else
            {
                setStatus(eraBox, false);
                setStatus(jYearBox, false);
                setStatus(monthBox, false);
                setStatus(DayBox, false);
                return new DateTime(y, m, d);
            }
        }

        /// <summary>
        /// 和暦の入力チェックを実施。正常であれば西暦を返す
        /// </summary>
        /// <param name="eraNumber"></param>
        /// <param name="jYearBox"></param>
        /// <param name="monthBox"></param>
        /// <param name="DayBox"></param>
        /// <returns></returns>
        protected DateTime dateCheck(int eraNumber, VerifyBox jYearBox, VerifyBox monthBox, VerifyBox DayBox)
        {
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            int d = DayBox.GetIntValue();


            //20190816113430 furukawa st ////////////////////////
            //チェックをGetAdYearMonthFromJyymmに統一

            y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(eraNumber.ToString() + y.ToString("00") + m.ToString("00")))/100;


                    ////20190807175118 furukawa st ////////////////////////
                    ////令和対応            


                    //if (eraNumber == 4 && y == 31 && m < 5) eraNumber = 4; //43104->43104
                    //else if (eraNumber == 4 && y == 31 && m >= 5)
                    //{
                    //    eraNumber = 5;  //43105->53105
                    //    y -= 30;        //53105->50105
                    //}

                    //else if (eraNumber == 4 && y == 1 && m >= 5) eraNumber = 5;//40105->50105
                    //else if (eraNumber == 4 && (y >= 2 && y < 20)) eraNumber = 5;//40201->50201 平成は20年以降とする
                    ////20190807175118 furukawa ed ////////////////////////


                    //y = DateTimeEx.GetAdYearFromEraYear(eraNumber, y);
            //20190816113430 furukawa ed ////////////////////////


            if (!DateTimeEx.IsDate(y, m, d))
            {
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
                setStatus(DayBox, true);
                return DateTime.MinValue;
            }
            else
            {
                setStatus(jYearBox, false);
                setStatus(monthBox, false);
                setStatus(DayBox, false);
                return new DateTime(y, m, d);
            }
        }

        /// <summary>
        /// 和暦の入力チェックを実施。正常であれば西暦を返す
        /// </summary>
        /// <param name="eraNumber"></param>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <param name="DayBox"></param>
        /// <returns></returns>
        protected DateTime dateCheck(int eraNumber, int jy, int m, VerifyBox DayBox)
        {
            int d = DayBox.GetIntValue();

            //20190816113820 furukawa st ////////////////////////
            //月がないためGetAdYearMonthFromJyymmに変更
            
                //int y = DateTimeEx.GetAdYearFromEraYear(eraNumber, jy);

            int y = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(eraNumber.ToString() + jy.ToString("00") + m.ToString("00")))/100;
            //20190816113820 furukawa ed ////////////////////////


            if (!DateTimeEx.IsDate(y, m, d))
            {
                setStatus(DayBox, true);
                return DateTime.MinValue;
            }
            else
            {
                setStatus(DayBox, false);
                return new DateTime(y, m, d);
            }
        }



        //20190414090054 furukawa 年月版追加

        /// <summary>
        /// 和暦年月の入力チェックを行ないます（日なし）
        /// </summary>
        /// <param name="jYearBox">和暦年テキストボックス</param>
        /// <param name="monthBox">月テキストボックス</param>
        /// <param name="d">日付</param>
        /// <returns></returns>
        
        //20191105163256 furukawa st ////////////////////////
        //年月日チェック用に日付の引数を追加
        
        protected DateTime dateCheck( VerifyBox jYearBox, VerifyBox monthBox,int d=1)
                //protected DateTime dateCheck(VerifyBox jYearBox, VerifyBox monthBox)

        //20191105163256 furukawa ed ////////////////////////
        {
            int y = jYearBox.GetIntValue();
            int m = monthBox.GetIntValue();
            //int d = 1;//20191105163417 furukawa 引数変更に伴い削除
            

            //20190416171805 furukawa st ////////////////////////
            //年の値がない場合は最小値返す

            if (y <= 0) return DateTime.MinValue;
            if (m <= 0) return DateTime.MinValue;
            //20190416171805 furukawa ed ////////////////////////



            //20190816114200 furukawa st ////////////////////////
            //チェックをGetAdYearMonthFromJyymmに統一
            


                        //int eraNumber = 0;
            
                        ////20190425203640 furukawa st ////////////////////////
                        ////令和対応
                        //eraNumber = DateTimeEx.GetEraNumber(new DateTime(
                        //                DateTimeEx.GetAdYearFromHs(int.Parse(y.ToString().PadLeft(2,'0')+ m.ToString().PadLeft(2,'0'))), m, d));
                        ////eraNumber = DateTimeEx.GetEraNumber(new DateTime(DateTimeEx.GetAdYearFromHs(y), m, d));
                        ////20190425203640 furukawa ed ////////////////////////



                        //y = DateTimeEx.GetAdYearFromEraYear(eraNumber, y);


            y = DateTimeEx.GetAdYearFromHs(int.Parse(y.ToString("00") + m.ToString("00")));


            //20190816114200 furukawa ed ////////////////////////


            if (!DateTimeEx.IsDate(y, m, d))
            {
                setStatus(jYearBox, true);
                setStatus(monthBox, true);
            
                return DateTime.MinValue;
            }
            else
            {
                setStatus(jYearBox, false);
                setStatus(monthBox, false);
            
                return new DateTime(y, m, d);
            }
        }

        #endregion


        protected void showInputErrorMessage()
        {
            MessageBox.Show("入力エラーがあります。\r\n" +
                "赤で表示されている部分をご確認ください。", "入力エラー",
                 MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        protected void showVerifyErrorMessage()
        {
            MessageBox.Show("入力結果が一致していません。\r\n" +
                "赤で表示されている部分をご確認ください。", "入力エラー",
                 MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }


        /// <summary>
        /// すべてのiVerifiableコントロールをクリアします
        /// </summary>
        /// <param name="c"></param>
        protected void iVerifiableAllClear(Control c)
        {
            foreach (Control item in c.Controls)
            {
                if (item is IVerifiable)
                {
                    ((IVerifiable)item).AllClear();
                }
                iVerifiableAllClear(item);
            }
        }

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box"></param>
        /// <param name="value"></param>
        /// <param name="verified"></param>
        protected void setVerifyVal(VerifyBox box, string value, bool needVerifyInput)
        {
            if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.TextV = value;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Text = value;
            }
        }

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box"></param>
        /// <param name="value"></param>
        /// <param name="needVerifyInput"></param>
        /// <param name="length">0埋め時の必要文字数</param>
        protected void setVerifyVal(VerifyBox box, int value, bool needVerifyInput, int length = 0)
        {
            var s = value == 0 ? string.Empty : value.ToString();
            if (length > 0) s = s.PadLeft(length, '0');

            if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.TextV = s;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Text = s;
            }
        }

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box"></param>
        /// <param name="check"></param>
        /// <param name="needVerifyInput">true=ベリファイ必要</param>
        protected void setVerifyVal(VerifyCheckBox box, bool check, bool needVerifyInput)
        {
            if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.CheckedV = check;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Checked = check;
            }
        }

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box">verifybox</param>
        /// <param name="value">表示する値</param>
        /// <param name="needVerifyInput">true=ベリファイ必要</param>
        protected void setValue(VerifyBox box, string value, bool first, bool needVerifyInput)
        {
            if (first)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.Text = value;
            }
            else if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.TextV = value;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Text = value;
            }
        }



        //20200626112654 furukawa st ////////////////////////
        //日付テキストボックス一括セット用。年号、日はなくてもOK
        
        
        /// <summary>
        /// 日付テキストボックスに値を一括セット
        /// </summary>
        /// <param name="val">入れる値</param>
        /// <param name="firstTime">1回目入力フラグ</param>
        /// <param name="nv">ベリファイ必要フラグ</param>
        /// <param name="vbY">年</param>
        /// <param name="vbM">月</param>
        /// <param name="vbG">年号 nullable</param>
        /// <param name="vbD">日 nullable</param>
        protected void setDateValue(DateTime val,bool firstTime,bool nv, VerifyBox vbY, VerifyBox vbM, VerifyBox vbG=null,  VerifyBox vbD=null)
        {
            DateTime dt = DateTime.MinValue;
            if (val != DateTime.MinValue)
            {
                dt = val;

                if (vbG != null) setValue(vbG, DateTimeEx.GetEraNumber(dt).ToString(), firstTime, nv);
                setValue(vbY, DateTimeEx.GetJpYear(dt).ToString(), firstTime, nv);
                setValue(vbM, dt.Month.ToString(), firstTime, nv);
                if (vbD != null) setValue(vbD, dt.Day.ToString(), firstTime, nv);
            }

        }


     

        /// <summary>
        /// ベリファイモード時のデータをセットします（府県コード初回入力時のみ使用）
        /// </summary>
        /// <param name="box">verifybox</param>
        /// <param name="value">表示する値</param>
        /// <param name="needVerifyInput">true=ベリファイ必要</param>
        protected void setValueFukencode(VerifyBox box, string value, bool first, bool needVerifyInput)
            {
                 if (first)
                 {
                     box.Enabled = true;
                     box.BackColor = SystemColors.Info;
                     box.Text = value;
                 }
                 else if (needVerifyInput)
                {
                    box.Enabled = true;
                    box.BackColor = SystemColors.Info;
                    box.TextV = value;
                
                }
                else
                {
                    box.Enabled = true;
                    box.BackColor = SystemColors.Info;
                    box.Text = value;
                    /*
                    box.Enabled = true;
                    box.BackColor = SystemColors.Control;
                    box.Text = value;*/
                }
            }


        

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box">verifybox</param>
        /// <param name="value">表示する値</param>
        /// <param name="needVerifyInput">true=ベリファイ必要</param>
        /// <param name="length">0埋め時の必要文字数</param>
        protected void setValue(VerifyBox box, int value, bool first, bool needVerifyInput, int length = 0)
        {
            var s = value == 0 ? string.Empty : value.ToString();
            if (length > 0) s = s.PadLeft(length, '0');

            if (first)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.Text = s;
            }
            else if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.TextV = s;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Text = s;
            }
        }

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box">verifybox</param>
        /// <param name="value">表示する値</param>
        /// <param name="needVerifyInput">true=ベリファイ必要</param>
        /// <param name="length">0埋め時の必要文字数</param>
        protected void setValue1000(VerifyBox box, int value, bool first, bool needVerifyInput, int length = 0)
        {
            var s = value == 0 ? string.Empty : (((decimal)value) / 1000m).ToString();
            if (length > 0) s = s.PadLeft(length, '0');

            if (first)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.Text = s;
            }
            else if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.TextV = s;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Text = s;
            }
        }

        /// <summary>
        /// ベリファイモード時のデータをセットします
        /// </summary>
        /// <param name="box">verifybox</param>
        /// <param name="value">表示する値</param>
        /// <param name="needVerifyInput">true=ベリファイ必要</param>
        protected void setValue(VerifyCheckBox box, bool check, bool first, bool needVerifyInput)
        {
            if(first)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.Checked = check;
            }
            else if (needVerifyInput)
            {
                box.Enabled = true;
                box.BackColor = SystemColors.Info;
                box.CheckedV = check;
            }
            else
            {
                box.Enabled = false;
                box.BackColor = SystemColors.Control;
                box.Checked = check;
            }
        }

        private void InputFormCore_Shown(object sender, EventArgs e)
        {
            var vs = new List<IVerifiable>();
            if (inputPanel == null) return;
            void act(Control c)
            {
                if (c is IVerifiable v) vs.Add(v);
                if (c is TextBox || c is CheckBox) inputControls.Add(c);

                if (c is TextBox tb) tb.TextChanged += ValueChanged;
                if (c is CheckBox cb) cb.CheckedChanged += ValueChanged;
                if (c is DataGridView dgv)
                {
                    dgv.CurrentCellChanged += ValueChanged;
                    dgv.CellValueChanged += ValueChanged;
                }

                var l = new List<Control>();
                foreach (Control item in c.Controls) l.Add(item);
                l.Sort((x, y) => x.TabIndex.CompareTo(y.TabIndex));
                foreach (var item in l) act(item);
            }
            act(inputPanel);
            
            missCounter = new Verify.VerifyMissCounter(vs);

            //20190424093443 furukawa st ////////////////////////
            //本番サーバでない場合、フォーム背景色をThistleにする
            CommonTool.setFormColor(this);
            //20190424093443 furukawa ed ////////////////////////

        }

        private void InputFormCore_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) e.Handled = true;
        }

        private void InputFormCore_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");

            //20200217094856 furukawa st ////////////////////////
            //入力項目にフォーカスを戻すため、InputMemoControlにコントロールを渡す。但しInputMemoControl自体は除く
            if (ActiveControl != inputMemoControl1) inputMemoControl1.returnControl = ActiveControl;
            //20200217094856 furukawa ed ////////////////////////


            //20200205174147 furukawa InputMemoControlへキーボードで飛びたい
            if (e.KeyCode == Keys.Insert) inputMemoControl1.textBoxMemo.Focus();
                                                                                
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            dataChanged = true;
        }

        private void _dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var bs = (BindingSource)dataGridViewPlist.DataSource;
            var glist = (List<App>)bs.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == "Aid")
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bs.ResetBindings(false);
        }

        int cid = User.CurrentUser?.UserID * 100000 ?? 0;
        /// <summary>
        /// ベリファイ入力をチェックします  エラーがある場合、false
        /// </summary>
        /// <returns></returns>
        protected bool checkVerify()
        {
            bool err = false;

            foreach (var item in inputControls)
            {
                if (item is IVerifiable vc) err |= vc.CheckVerify();
            }

            var bs = (BindingSource)dataGridViewPlist.DataSource;
            var app = (App)bs.Current;
            missCounter.Check(app.Aid);

            if (err)
            {
                showVerifyErrorMessage();
                focusBack(true);
            }
            else
            {
                missCounter.MissLogWrite(app.Aid, ++cid);
            }

            return !err;
        }

        /// <summary>
        /// ミスカウントをリセットします
        /// </summary>
        protected void missCounterReset()
        {
            missCounter?.Reset();
        }

        /// <summary>
        /// ベリファイ入力時、1回目入力のミス回数を取得します
        /// </summary>
        protected int firstMissCount => missCounter.firstMissCount;

        /// <summary>
        /// ベリファイ入力時、2回目入力のミス回数を取得します
        /// </summary>
        protected int secondMissCount => missCounter.secondMissCount;

        protected void resetInputData(App app)
        {
            app.MediMonth = 0;
            app.HihoNum = string.Empty;
            app.HihoPref = 0;
            app.HihoType = 0;
            app.HihoName = string.Empty;
            app.HihoZip = string.Empty;
            app.HihoAdd = string.Empty;
            app.PersonName = string.Empty;
            app.Sex = 0;
            app.Birthday = DateTimeEx.DateTimeNull;
            app.ClinicNum = string.Empty;
            app.Family = 0;
            app.Ratio = 0;
            app.FushoName1 = string.Empty;
            app.FushoDate1 = DateTimeEx.DateTimeNull;
            app.FushoFirstDate1 = DateTimeEx.DateTimeNull;
            app.FushoStartDate1 = DateTimeEx.DateTimeNull;
            app.FushoFinishDate1 = DateTimeEx.DateTimeNull;
            app.FushoDays1 = 0;
            app.FushoCourse1 = 0;
            app.FushoName2 = string.Empty;
            app.FushoDate2 = DateTimeEx.DateTimeNull;
            app.FushoFirstDate2 = DateTimeEx.DateTimeNull;
            app.FushoStartDate2 = DateTimeEx.DateTimeNull;
            app.FushoFinishDate2 = DateTimeEx.DateTimeNull;
            app.FushoDays2 = 0;
            app.FushoCourse2 = 0;
            app.FushoName3 = string.Empty;
            app.FushoDate3 = DateTimeEx.DateTimeNull;
            app.FushoFirstDate3 = DateTimeEx.DateTimeNull;
            app.FushoStartDate3 = DateTimeEx.DateTimeNull;
            app.FushoFinishDate3 = DateTimeEx.DateTimeNull;
            app.FushoDays3 = 0;
            app.FushoCourse3 = 0;
            app.FushoName4 = string.Empty;
            app.FushoDate4 = DateTimeEx.DateTimeNull;
            app.FushoFirstDate4 = DateTimeEx.DateTimeNull;
            app.FushoStartDate4 = DateTimeEx.DateTimeNull;
            app.FushoFinishDate4 = DateTimeEx.DateTimeNull;
            app.FushoDays4 = 0;
            app.FushoCourse4 = 0;
            app.FushoName5 = string.Empty;
            app.FushoDate5 = DateTimeEx.DateTimeNull;
            app.FushoFirstDate5 = DateTimeEx.DateTimeNull;
            app.FushoStartDate5 = DateTimeEx.DateTimeNull;
            app.FushoFinishDate5 = DateTimeEx.DateTimeNull;
            app.FushoDays5 = 0;
            app.FushoCourse5 = 0;
            app.NewContType = NEW_CONT.Null;
            app.Distance = 0;
            app.VisitTimes = 0;
            app.VisitFee = 0;
            app.VisitAdd = 0;
            app.DrNum = string.Empty;
            app.ClinicZip = string.Empty;
            app.ClinicAdd = string.Empty;
            app.ClinicName = string.Empty;
            app.ClinicTel = string.Empty;
            app.DrName = string.Empty;
            app.DrKana = string.Empty;
            app.Total = 0;
            app.Partial = 0;
            app.Charge = 0;
            app.CountedDays = 0;
            app.Numbering = string.Empty;
            app.AccountNumber = string.Empty;
            app.Bui = 0;
            app.RrID = 0;
            app.PayCode = string.Empty;
            app.ShokaiCode = string.Empty;
            app.ComNum = string.Empty;
            app.GroupNum = string.Empty;
        }

        private void 複数月計算ツールToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (calcForm == null || calcForm.IsDisposed) calcForm = new TotalCalcForm();
            if (!calcForm.Visible) calcForm.Show(this);
        }

        private void 次画像参照ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (nextImageForm != null)
            {
                nextImageForm.Dispose();
                nextImageForm = null;
            }

            var current = bs.Current;
            if (current==null || !(current is App app)) return;
            nextImageForm = new NextImageForm(app);
            nextImageForm.Show(this);
        }

        private void 前画像参照ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (nextImageForm != null)
            {
                nextImageForm.Dispose();
                nextImageForm = null;
            }

            var current = bs.Current;
            if (current == null || !(current is App app)) return;
            nextImageForm = new NextImageForm(app, false);
            nextImageForm.Show(this);
        }

        Ocr.OcrViewForm ocrForm;
        private void oCR情報ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var current = bs.Current;
            if (current == null || !(current is App app)) return;

            if (ocrForm == null || ocrForm.IsDisposed)
            {
                ocrForm = new Ocr.OcrViewForm(app);
                ocrForm.Show(this);
            }
            else
            {
                ocrForm.App = app;
                if (!ocrForm.Visible) ocrForm.Show(this);
            }
        }
    }
}
