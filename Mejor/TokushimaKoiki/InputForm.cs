﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.TokushimaKoiki
{
    public partial class InputForm : InputFormCore
    {
        private BindingSource bsApp;
        private BindingSource bsRefReces;
        private InputMode inputMode;
        protected override Control inputPanel => panelRight;
        int cym;

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point posYM = new Point(80, 30);
        Point posHnum = new Point(800, 60);
        Point posNew = new Point(800, 1100);
        Point posVisit = new Point(350, 1200);
        Point posTotal = new Point(800, 1400);
        Point posBui = new Point(80, 750);

        /// <summary>
        /// 通常の入力・ベリファイ時のコンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="mode"></param>
        public InputForm(InputMode mode, ScanGroup sGroup, int aid = 0)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode == InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            panelInfo.Visible = true;
            panelMatchWhere.Visible = false;
            panelMatchCheckInfo.Visible = false;
        
            scanGroup = sGroup;
            var list = new List<App>();
            list = App.GetAppsGID(scanGroup.GroupID);

            //フラグ順にソート
            list.Sort((x, y) =>
                x.InputOrderNumber == y.InputOrderNumber ?
                x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));

            //Appリストを作成
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            initializeGridView();

            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);
        }

        /// <summary>
        /// マッチングチェックの際のコンストラクタ
        /// </summary>
        /// <param name="iname"></param>
        /// <param name="mode"></param>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        public InputForm(InputMode mode, int cym)
        {
            InitializeComponent();
            Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;
            inputMode = mode;
            if (mode != InputMode.MatchCheck)
                throw new Exception("指定されたモードとコンストラクタが矛盾しています");

            this.cym = cym;
            labelInfo.Text = $"{cym.ToString("0000年00月")}分チェック";

            panelInfo.Visible = false;
            panelMatchWhere.Visible = true;
            panelMatchCheckInfo.Visible = true;

            //Appリスト
            var list = new List<App>();
            bsApp = new BindingSource();
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            //各グリッド表示調整
            initializeGridView();

            //初回のみ手動セット
            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;
            if (app != null) setApp(app);

            radioButtonOverlap.CheckedChanged += RadioButton_CheckedChanged;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (!panelMatchCheckInfo.Visible)
                throw new Exception("マッチチェックパネル非表示中にマッチチェックモードが変更されました");

            //データリストを作成
            var list = new List<App>();

            var f = new WaitFormSimple();
            try
            {
                Task.Factory.StartNew(() => f.ShowDialog());
                if (radioButtonOverlap.Checked)
                {
                    list = MatchingApp.GetOverlapApp(cym);
                    list.Sort((x, y) => x.Numbering == y.Numbering ?
                        x.Aid.CompareTo(y.Aid) : x.Numbering.CompareTo(y.Numbering));
                }
                else
                {
                    list = MatchingApp.GetNotMatchApp(cym);
                    list.Sort((x, y) => x.Aid.CompareTo(y.Aid));
                }
            }
            finally
            {
                f.InvokeCloseDispose();
            }

            bsApp.DataSource = list;

            if (list.Count == 0)
            {
                MessageBox.Show((radioButtonOverlap.Checked ? "重複" : "マッチなし") +
                    "エラーデータはありません", "",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            dataGridViewPlist.DataSource = bsApp;
            verifyBoxY.Focus();
        }

        private void initializeGridView()
        {
            //Appデータ欄
            foreach (DataGridViewColumn c in dataGridViewPlist.Columns) c.Visible = false;
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 40;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Numbering)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Numbering)].HeaderText = "突合ID";
            
            //広域データ欄
            bsRefReces = new BindingSource();
            bsRefReces.DataSource = new List<RefRece>();
            dataGridKoikiData.DataSource = bsRefReces;

            dataGridKoikiData.DefaultCellStyle.SelectionForeColor = Color.Black;
            dataGridKoikiData.DefaultCellStyle.SelectionBackColor = Utility.GridSelectColor;

            foreach (DataGridViewColumn c in dataGridKoikiData.Columns) c.Visible = false;
            dataGridKoikiData.Columns[nameof(RefRece.RrID)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.RrID)].Width = 60;
            dataGridKoikiData.Columns[nameof(RefRece.YM)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.YM)].Width = 60;
            dataGridKoikiData.Columns[nameof(RefRece.YM)].HeaderText = "年月";
            dataGridKoikiData.Columns[nameof(RefRece.Num)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.Num)].Width = 70;
            dataGridKoikiData.Columns[nameof(RefRece.Num)].HeaderText = "被保番";
            dataGridKoikiData.Columns[nameof(RefRece.Name)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.Name)].Width = 95;
            dataGridKoikiData.Columns[nameof(RefRece.Name)].HeaderText = "氏名";
            dataGridKoikiData.Columns[nameof(RefRece.ClinicNum)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.ClinicNum)].Width = 80;
            dataGridKoikiData.Columns[nameof(RefRece.ClinicNum)].HeaderText = "施術所番号";
            dataGridKoikiData.Columns[nameof(RefRece.ClinicName)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.ClinicName)].Width = 100;
            dataGridKoikiData.Columns[nameof(RefRece.ClinicName)].HeaderText = "施術所";
            dataGridKoikiData.Columns[nameof(RefRece.Total)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.Total)].Width = 50;
            dataGridKoikiData.Columns[nameof(RefRece.Total)].HeaderText = "合計";
            dataGridKoikiData.Columns[nameof(RefRece.AID)].Visible = true;
            dataGridKoikiData.Columns[nameof(RefRece.AID)].Width = 60;

            //pgupで選択行が変わるのを防ぐ
            dataGridKoikiData.KeyDown += (sender, e) => e.Handled = e.KeyCode == Keys.PageUp;
        }

        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;

            if (app == null)
            {
                clearApp();
                return;
            }

            setApp(app);
            if (app.StatusFlagCheck(StatusFlag.自動マッチ済)) verifyBoxNewCont.Focus();
            else verifyBoxY.Focus();
        }

        //フォーム表示時
        private void FormOCRCheck_Shown(object sender, EventArgs e)
        {
            if (inputMode == InputMode.MatchCheck)
            {
                radioButtonOverlap.Checked = true;
                if (dataGridViewPlist.RowCount == 0) radioButtonNotMatch.Checked = true;
                if (dataGridViewPlist.RowCount == 0) return;
            }
            else
            {
                if (dataGridViewPlist.RowCount == 0)
                {
                    MessageBox.Show("表示すべきデータがありません", "",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    return;
                }
            }
            
            verifyBoxY.Focus();
        }

        //終了ボタン
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }

        private void regist()
        {
            if (!updateDbApp()) return;

            int ri = dataGridViewPlist.CurrentRow.Index;
            if (ri == dataGridViewPlist.RowCount - 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }

        //合計金額まで入力したら、広域データとのマッチングを行う
        private void verifyBoxTotal_Leave(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectRefRece(app);
        }

        private void selectRefRece(App app)
        {
            bsRefReces.Clear();

            int y, m, total;
            int.TryParse(verifyBoxY.Text, out y);
            int.TryParse(verifyBoxM.Text, out m);
            int.TryParse(verifyBoxTotal.Text, out total);
            if (y == 0 || m == 0 || total == 0)
            {
                bsRefReces.ResetBindings(false);
                return;
            }

            List<RefRece> l;
            if (inputMode == InputMode.MatchCheck)
            {
                //マッチチェック時
                var num = verifyBoxHnum.Text.Trim();
                l = RefRece.SerchForMatching(cym, num, total);
                if (checkBoxNumber.Checked) l = l.FindAll(a => a.Num == num);
                if (checkBoxTotal.Checked) l = l.FindAll(a => a.Total == total);
            }
            else
            {
                //入力,ベリファイ時

                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
                int cym = DateTimeEx.GetAdYearFromHs(scanGroup.cyear * 100 + scanGroup.cmonth) * 100 + scanGroup.cmonth;
                int ym = DateTimeEx.GetAdYearFromHs(y * 100 + m) * 100 + m;
                //int cym = DateTimeEx.GetAdYearFromHs(scanGroup.cyear) * 100 + scanGroup.cmonth;
                //int ym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;
                //20190424191119_2019/04/07 GetAdYearFromHS変更対応＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊

                l = RefRece.SerchByInput(cym, ym, verifyBoxHnum.Text.Trim(), total);
            }

            if (l != null) bsRefReces.DataSource = l;
            bsRefReces.ResetBindings(false);
            
            if (l == null || l.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }
            else if (l.Count == 1 && inputMode != InputMode.MatchCheck)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                for (int i = 0; i < dataGridKoikiData.RowCount; i++)
                {
                    string rrid = dataGridKoikiData[nameof(RefRece.RrID) , i].Value.ToString();
                    if (app.Numbering == rrid)
                    {
                        //既に一意の広域データとマッチング済みの場合。
                        labelMacthCheck.BackColor = Color.Cyan;
                        labelMacthCheck.Text = "マッチングOK";
                        labelMacthCheck.Visible = true;
                        dataGridKoikiData.CurrentCell = dataGridKoikiData[0, i];
                        return;
                    }
                }

                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
                dataGridKoikiData.CurrentCell = null;
            }
        }

        /// <summary>
        /// 入力内容をチェックします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //年
            int year = verifyBoxY.GetIntValue();
            //setStatus(verifyBoxY, year < app.ChargeYear - 7 || app.ChargeYear < year);

            //月
            int month = verifyBoxM.GetIntValue();
            setStatus(verifyBoxM, month < 1 || 12 < month);

            //被保険者番号 3文字以上かつ数字に直せること
            long hnumTemp;
            setStatus(verifyBoxHnum, verifyBoxHnum.Text.Length < 2 ||
                !long.TryParse(verifyBoxHnum.Text, out hnumTemp));

            //合計金額
            int total = verifyBoxTotal.GetIntValue();
            setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            //新規・継続の抜けチェック
            int newCont = verifyBoxNewCont.GetIntValue();
            setStatus(verifyBoxNewCont, newCont != 1 && newCont != 2);

            //負傷名チェック
            fusho1Check(verifyBoxF1);
            fushoCheck(verifyBoxF2);
            fushoCheck(verifyBoxF3);
            fushoCheck(verifyBoxF4);
            fushoCheck(verifyBoxF5);

            //往療料
            bool visit = verifyCheckBoxVisit.Checked;

            //ここまでのチェックで必須エラーが検出されたらnullを返す
            if (hasError)
            {
                showInputErrorMessage();
                return false;
            }

            //ここから値の反映
            var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
            app.MediYear = year;
            app.MediMonth = month;
            app.HihoNum = verifyBoxHnum.Text;
            app.Total = total;
            app.Distance = visit ? 999 : 0;
            app.AppType = appsg.AppType;

            //新規・継続
            app.NewContType = newCont == 1 ? NEW_CONT.新規 : NEW_CONT.継続;

            //部位
            app.FushoName1 = verifyBoxF1.Text.Trim();
            app.FushoName2 = verifyBoxF2.Text.Trim();
            app.FushoName3 = verifyBoxF3.Text.Trim();
            app.FushoName4 = verifyBoxF4.Text.Trim();
            app.FushoName5 = verifyBoxF5.Text.Trim();

            //広域データからのデータコピー
            var rr = (RefRece)dataGridKoikiData.CurrentRow.DataBoundItem;
            app.PersonName = rr.Name;
            app.HihoZip = rr.Zip;
            app.HihoAdd = rr.Add;
            app.DrNum = rr.ClinicNum;
            app.CountedDays = rr.Days;
            app.ClinicName = rr.ClinicName;
            app.HihoZip = rr.DestZip == "" ? rr.Zip : rr.DestZip;
            app.HihoAdd = rr.DestAdd == "" ? rr.Add : rr.DestZip;
            app.Numbering = rr.RrID.ToString();
            app.RrID = rr.RrID;
            app.ComNum = rr.ComNum;

            app.TaggedDatas.Kana = rr.Kana;
            app.TaggedDatas.DestZip = rr.DestZip;
            app.TaggedDatas.DestAdd = rr.DestAdd;
            app.TaggedDatas.DestName = rr.DestName;
            app.TaggedDatas.Kana = rr.Kana;
            return true;
        }

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            var setSpecialApp = new Action<App>(a =>
                {
                    a.MediMonth = 0;
                    a.HihoNum = string.Empty;
                    a.Total = 0;
                    a.Distance = 0;
                    a.NewContType = 0;

                    //20210210172455 furukawa st ////////////////////////
                    //先に設定しているのに、ここで続紙、エラー等を全て0にしてしまっているので削除
                    //  a.AppType = APP_TYPE.NULL;
                    //20210210172455 furukawa ed ////////////////////////

                    a.FushoName1 = string.Empty;
                    a.FushoName2 = string.Empty;
                    a.FushoName3 = string.Empty;
                    a.FushoName4 = string.Empty;
                    a.FushoName5 = string.Empty;
                    a.PersonName = string.Empty;
                    a.ClinicName = string.Empty;
                    a.DrNum = string.Empty;
                    a.CountedDays = 0;
                    a.DrName = string.Empty;
                    a.HihoZip = string.Empty;
                    a.HihoAdd = string.Empty;
                    a.Numbering = string.Empty;
                    a.RrID = 0;
                });

            if (verifyBoxY.Text == "--")
            {
                //続紙
                app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                app.AppType = APP_TYPE.続紙;
                setSpecialApp(app);
            }
            else if (verifyBoxY.Text == "++")
            {
                //不要
                app.MediYear = (int)APP_SPECIAL_CODE.不要;
                app.AppType = APP_TYPE.不要;
                setSpecialApp(app);
            }
            else if (verifyBoxY.Text == "**")
            {
                //エラー
                app.MediYear = (int)APP_SPECIAL_CODE.エラー;
                app.AppType = APP_TYPE.エラー;
                setSpecialApp(app);
            }
            else
            {
                //申請書の場合
                if (dataGridKoikiData.CurrentCell == null)
                {
                    //20220511095547 furukawa st ////////////////////////
                    //メッセージ変更
                    
                    if (MessageBox.Show("マッチングデータがありません。登録しますか？",
                           Application.ProductName,
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Exclamation,
                           MessageBoxDefaultButton.Button2) != DialogResult.Yes) return false;

                    //      MessageBox.Show("対象となる広域データが選択されていません。\r\n" +
                    //          "対象となるマッチングデータがない場合は、年に「**」を入力し、エラーとして下さい。",
                    //          "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    //      return false;
                    //20220511095547 furukawa ed ////////////////////////
                }

                var oldDidStr = app.Numbering;

                if (!checkApp(app))
                {
                    focusBack(true);
                    return false;
                }


                //20210210164721 furukawa st ////////////////////////
                //後の処理で同じことをやってるので削除

                //      //データベースへ反映
                //      app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput);

                //      //広域データ更新
                //      var kd = (RefRece)dataGridKoikiData.CurrentRow.DataBoundItem;
                //      if (kd.AID != app.Aid) RefRece.AidUpdate(kd.RrID, app.Aid);

                //20210210164721 furukawa ed ////////////////////////


            }

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                //入力ログ
                //20200806111633 furukawa 一申請書の入力時間計測を正確にするため関数置換
                if (app.Ufirst == 0 && !InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran))
                    return false;

                //データ記録
                app.Update(User.CurrentUser.UserID, App.UPDATE_TYPE.FirstInput);

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;

                //広域データ更新
                if (app.RrID == 0)
                {
                    RefRece.AidDelete(app.Aid, tran);
                }
                else
                {
                    var kd = (RefRece)dataGridKoikiData.CurrentRow.DataBoundItem;
                    if (kd.AID != app.Aid) RefRece.AidUpdate(kd.RrID, app.Aid, tran);
                }
                jyuTran.Commit();
                tran.Commit();
            }
            
            return true;
        }

        private void clearApp()
        {
            //画像クリア
            try
            {
                userControlImage1.Clear();
                scrollPictureControl1.Clear();
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }

            //情報クリア
            iVerifiableAllClear(panelRight);
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            bsRefReces.Clear();
            bsRefReces.ResetBindings(false);
        }


        /// <summary>
        /// Appを表示します
        /// </summary>
        private void setApp(App app)
        {
            //画像の表示
            setImage(app);

            //全クリア
            iVerifiableAllClear(panelRight);

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //App_Flagのチェック
            labelAppStatus.Text = app.InputStatus.ToString();
            if (app.StatusFlagCheck(StatusFlag.入力済))
            {
                //既にチェック済みの画像はデータベースからデータ表示
                setInputedApp(app);
                labelAppStatus.BackColor = Color.Cyan;
            }
            else if (app.StatusFlagCheck(StatusFlag.自動マッチ済))
            {
                //マッチング有のデータについては一部広域からのデータを表示
                setNoInputAppWithOcr(app);
                labelAppStatus.BackColor = Color.Yellow;
            }
            else
            {
                //一度もチェックしておらず、かつ広域データとのマッチングできない画像はOCRデータからデータ表示
                setNoInputApp(app);
                labelAppStatus.BackColor = Color.Red;
            }

            //広域データ表示
            selectRefRece(app);

            labelUser.Text = $"入力：{User.GetUserName(app.Ufirst)}";
            changedReset(app);
        }

        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// 未チェック、およびマッチング無しの場合、OCRデータから入力欄にフィルします
        /// </summary>
        private void setNoInputApp(App app)
        {
            //OCRデータが存在する場合
            if (!string.IsNullOrWhiteSpace(app.OcrData))
            {
                try
                {
                    var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                    if (string.IsNullOrEmpty(appsg.note2))
                    {
                        var ocr = app.OcrData.Split(',');
                        //OCRデータがあれば、部位のみ挿入
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);

                        //新規/継続
                        if (ocr.Length > 127)
                        {
                            if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                            if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                }
            }
        }

        /// <summary>
        /// マッチング有りの場合：広域データとOCRデータの両方から入力欄にフィルします
        /// </summary>
        private void setNoInputAppWithOcr(App app)
        {
            try
            {
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();
                verifyBoxHnum.Text = app.HihoNum;
                verifyBoxTotal.Text = app.Total.ToString();

                var appsg = scanGroup ?? ScanGroup.SelectWithScanData(app.GroupID);
                if (string.IsNullOrEmpty(appsg.note2))
                {
                    var ocr = app.OcrData.Split(',');
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        verifyBoxF1.Text = Fusho.GetFusho1(ocr);
                        verifyBoxF2.Text = Fusho.GetFusho2(ocr);
                        verifyBoxF3.Text = Fusho.GetFusho3(ocr);
                        verifyBoxF4.Text = Fusho.GetFusho4(ocr);
                        verifyBoxF5.Text = Fusho.GetFusho5(ocr);
                    }

                    //新規/継続
                    if (ocr.Length > 127)
                    {
                        if (ocr[126] != "0") verifyBoxNewCont.Text = "1";
                        if (ocr[127] != "0") verifyBoxNewCont.Text = "2";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
            }
        }


        /// <summary>
        /// チェック済みの画像の場合、データベースから入力欄にフィルします
        /// </summary>
        private void setInputedApp(App app)
        {
            if (app.MediYear == -3)
            {
                //続紙
                verifyBoxY.Text = "--";
            }
            else if (app.MediYear == -4)
            {
                //白バッジ、その他
                verifyBoxY.Text = "++";
            }
            else if (app.MediYear == -999)
            {
                //エラー
                verifyBoxY.Text = "**";
            }
            else
            {
                //申請書
                //申請書年月
                verifyBoxY.Text = app.MediYear.ToString();
                verifyBoxM.Text = app.MediMonth.ToString();

                //被保険者番号
                verifyBoxHnum.Text = app.HihoNum;

                //合計金額
                verifyBoxTotal.Text = app.Total.ToString();

                verifyBoxF1.Text = app.FushoName1;
                verifyBoxF2.Text = app.FushoName2;
                verifyBoxF3.Text = app.FushoName3;
                verifyBoxF4.Text = app.FushoName4;
                verifyBoxF5.Text = app.FushoName5;

                //新規/継続
                verifyBoxNewCont.Text =
                    app.NewContType == NEW_CONT.新規 ? "1" :
                    app.NewContType == NEW_CONT.継続 ? "2" : "";

                //往療
                verifyCheckBoxVisit.Checked = app.Distance == 999;

                //突合情報
                int oldDid = 0;
                int.TryParse(app.Numbering, out oldDid);
            }
        }


        /// <summary>
        /// 請求年への入力で、画像の種類を判別します
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            var setCanInput = new Action<TextBox, bool>((t, b) =>
                {
                    t.ReadOnly = !b;
                    t.TabStop = b;
                    if (!b)
                        t.BackColor = SystemColors.Menu;
                    else
                        t.BackColor = SystemColors.Info;
                });

            if (verifyBoxY.Text == "--" || verifyBoxY.Text == "++" || verifyBoxY.Text == "**")
            {
                //続紙、白バッジ、その他の場合、入力項目は無い
                setCanInput(verifyBoxM, false);
                setCanInput(verifyBoxHnum, false);
                setCanInput(verifyBoxTotal, false);
                setCanInput(verifyBoxNewCont, false);
                setCanInput(verifyBoxF1, false);
                setCanInput(verifyBoxF2, false);
                setCanInput(verifyBoxF3, false);
                setCanInput(verifyBoxF4, false);
                setCanInput(verifyBoxF5, false);
                verifyBoxNewCont.TabStop = false;

                verifyCheckBoxVisit.BackColor = SystemColors.Menu;
                verifyCheckBoxVisit.Enabled = false;
            }
            else
            {
                //申請書の場合
                setCanInput(verifyBoxM, true);
                setCanInput(verifyBoxHnum, true);
                setCanInput(verifyBoxTotal, true);
                setCanInput(verifyBoxNewCont, true);
                setCanInput(verifyBoxF1, true);
                setCanInput(verifyBoxF2, true);
                setCanInput(verifyBoxF3, true);
                setCanInput(verifyBoxF4, true);
                setCanInput(verifyBoxF5, true);
                verifyBoxNewCont.TabStop = true;

                verifyCheckBoxVisit.BackColor = SystemColors.Info;
                verifyCheckBoxVisit.Enabled = true;
                buiTabStopAdjust();
            }
        }

        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            string newFileName;
            using (var f = new OpenFileDialog())
            {
                f.FileName = "*.tif";
                f.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
                f.Title = "新しい画像ファイルを選択してください";

                if (f.ShowDialog() != DialogResult.OK) return;
                newFileName = f.FileName;
            }

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBox_Enter(object sender, EventArgs e)
        {
            Point p;

            if (sender == verifyBoxY) p = posYM;
            else if (sender == verifyBoxM) p = posYM;
            else if (sender == verifyBoxHnum) p = posHnum;
            else if (sender == verifyBoxTotal) p = posTotal;
            else if (sender == verifyBoxNewCont) p = posNew;
            else if (sender == verifyBoxF1) p = posBui;
            else if (sender == verifyBoxF2) p = posBui;
            else if (sender == verifyBoxF3) p = posBui;
            else if (sender == verifyBoxF4) p = posBui;
            else if (sender == verifyBoxF5) p = posBui;
            else if (sender == verifyCheckBoxVisit) p = posVisit;
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        private void fushoTextBox_TextChanged(object sender, EventArgs e)
        {
            buiTabStopAdjust();
        }

        private void buiTabStopAdjust()
        {
            verifyBoxF3.TabStop = verifyBoxF2.Text != string.Empty;
            verifyBoxF4.TabStop = verifyBoxF3.Text != string.Empty;
            verifyBoxF5.TabStop = verifyBoxF4.Text != string.Empty;
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (ri <= 0) return;
            dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri - 1];
        }

        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            var pos = scrollPictureControl1.ScrollPosition;
            if (verifyBoxY.Focused == true) posYM = pos;
            else if (verifyBoxM.Focused == true) posYM = pos;
            else if (verifyBoxHnum.Focused == true) posHnum = pos;
            else if (verifyBoxNewCont.Focused == true) posNew = pos;
            else if (verifyBoxTotal.Focused == true) posTotal = pos;
            else if (verifyBoxF1 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF2 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF3 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF4 == this.ActiveControl) posBui = pos;
            else if (verifyBoxF5 == this.ActiveControl) posBui = pos;
        }

        private void checkBoxMatch_CheckedChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            selectRefRece(app);
        }
    }
}
