﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NpgsqlTypes;

namespace Mejor.TokushimaKoiki
{
    class RefRece
    {
        public int RrID { get; set; }
        public int ImportID { get; set; }
        public int CYM { get; set; }
        public int YM { get; set; }
        public string Num { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string Kana { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public string Add { get; set; } = string.Empty;
        public string DestZip { get; set; } = string.Empty;
        public string DestAdd { get; set; } = string.Empty;
        public string DestName { get; set; } = string.Empty;
        public string ClinicNum { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public int Days { get; set; }
        public int Total { get; set; }
        public string ComNum { get; set; } = string.Empty;
        public int AID { get; set; }

        /// <summary>
        /// CSVファイルを読み込み、申請情報を作成します
        /// </summary>
        /// <param name="list"></param>
        private static List<RefRece> readCSV(string fileName)
        {
            var l = new List<RefRece>();
            var csv = CommonTool.CsvImportUTF8(fileName);

            foreach (var item in csv)
            {
                int ym, cym, total, days;
                if (!int.TryParse(item[11], out ym)) continue;
                if (!int.TryParse(item[12], out cym)) continue;
                if (!int.TryParse(item[13], out days)) continue;
                if (!int.TryParse(item[14], out total)) continue;

                var rr = new RefRece();
                rr.YM = ym;
                rr.CYM = cym;
                rr.Num = item[0];
                rr.Name = item[1];
                rr.Kana = item[2];
                rr.Zip = item[3].Trim();
                rr.Add = item[4].Trim();
                rr.DestZip = item[5].Trim();
                rr.DestAdd = item[6].Trim();
                rr.DestName = item[7].Trim();
                rr.ClinicNum = item[8].Trim();
                rr.ClinicName = item[9].Trim();
                rr.Days = days;
                rr.Total = total;
                rr.ComNum = item[15].Trim();
                l.Add(rr);
            }

            return l;
        }

        public static bool Import(string fileName)
        {
            var wf = new WaitForm();
            wf.ShowDialogOtherTask();
            wf.LogPrint("インポートを開始します");

            try
            {
                wf.LogPrint("CSVファイルを読み込み中です");
                var list = readCSV(fileName);

                wf.LogPrint("データベースに登録しています");
                using (var tran = DB.Main.CreateTransaction())
                {
                    var rri = RefReceImport.Create(fileName, tran);

                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].RrID = rri.RrImportID * 1000000 + i + 1;
                        list[i].ImportID = rri.RrImportID;
                    }

                    var sql = "INSERT INTO refrece( " +
                        "rrid, importid, cym, ym, num, name, kana, zip, add, destzip, " +
                        "destadd, destname, clinicnum, clinicname, days, total, comnum, aid) " +
                        "VALUES (" +
                        "@rrid, @importid, @cym, @ym, @num, @name, @kana, @zip, @add, @destzip, " +
                        "@destadd, @destname, @clinicnum, @clinicname, @days, @total, @comnum, @aid);";

                    wf.SetMax(list.Count);
                    wf.BarStyle = System.Windows.Forms.ProgressBarStyle.Continuous;

                    //500ごとに登録
                    var c = 0;
                    var list100 = new List<RefRece>();
                    for (int i = 0; i < list.Count; i++)
                    {
                        c++;
                        list100.Add(list[i]);
                        if (c == 100)
                        {
                            if (!DB.Main.Excute(sql, list100, tran))
                            {
                                tran.Rollback();
                                return false;
                            }
                            wf.InvokeValue = i + 1;
                            list100.Clear();
                            c = 0;
                        }
                    }

                    //最終500未満登録
                    if (!DB.Main.Excute(sql, list100, tran))
                    {
                        tran.Rollback();
                        return false;
                    }

                    tran.Commit();
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                System.Windows.Forms.MessageBox.Show("インポートに失敗しました");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
            System.Windows.Forms.MessageBox.Show("インポートが完了しました");
            return true;
        }

        private const string SELECT_CLAUSE =
            "SELECT rrid, importid, cym, ym, num, name, kana, zip, add, destzip, " +
            "destadd, destname, clinicnum, clinicname, days, total, comnum, aid " +
            "FROM refrece";

        /// <summary>
        /// 診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchByInput(int cyyyymm, int yyyymm, string num, int total)
        {
            var sql = SELECT_CLAUSE +
                " WHERE cym=@cym AND ym=@ym AND num=@num AND total=@total;";

            var l = DB.Main.Query<RefRece>(sql,
                new { cym = cyyyymm, ym = yyyymm, num = num, total = total});
            return l.ToList();
        }

        /// <summary>
        /// マッチング作業のため診療年月と被保番、合計金額でデータを抽出します
        /// </summary>
        /// <param name="cy"></param>
        /// <param name="cm"></param>
        /// <param name="y"></param>
        /// <param name="m"></param>
        /// <param name="hnum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static List<RefRece> SerchForMatching(int cyyyymm, string num, int total)
        {
            var sql = SELECT_CLAUSE +
                " WHERE cym=@cym AND (num=@num OR total=@total);";

            var l = DB.Main.Query<RefRece>(sql,
                new { cym = cyyyymm, num = num, total = total});
            return l.ToList();
        }

        /// <summary>
        /// koikidataテーブル中のdidに該当するレコードにaidを追記します
        /// </summary>
        /// <param name="did"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidUpdate(int did, int aid, DB.Transaction tran = null)
        {
            if (tran == null)
            {
                using (tran = DB.Main.CreateTransaction())
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = did;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery())
                        {
                            tran.Commit();
                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    tran.Rollback();
                    return false;
                }
            }
            else
            {
                //using (var dcmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=0 WHERE aid=:aid;", tran))
                //using (var cmd = DB.Main.CreateCmd("UPDATE koikidata SET aid=:aid WHERE rrid=:rrid;", tran))
                using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
                using (var cmd = DB.Main.CreateCmd("UPDATE refrece SET aid=:aid WHERE rrid=:rrid;", tran))
                {
                    try
                    {
                        dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                        cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = did;

                        if (dcmd.TryExecuteNonQuery() && cmd.TryExecuteNonQuery()) return true;
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWriteWithMsg(ex);
                    }
                    return false;
                }
            }
        }

        /// <summary>
        /// koikidataテーブルから指定されたAIDの情報を削除します
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static bool AidDelete(int aid, DB.Transaction tran)
        {
            using (var dcmd = DB.Main.CreateCmd("UPDATE refrece SET aid=0 WHERE aid=:aid;", tran))
            {
                dcmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                return dcmd.TryExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// 現時点でマッチングのないデータを取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> GetNotMatchDatas(int cyyyymm)
        {
            var sql = SELECT_CLAUSE +
                " WHERE cym=@cym AND aid = 0 ORDER BY rrid;";

            var l = DB.Main.Query<RefRece>(sql, new { cym = cyyyymm });
            return l.ToList();
        }

        /// <summary>
        /// 指定された年月のデータをすべて取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static List<RefRece> Select(int cyyyymm)
        {
            var sql = SELECT_CLAUSE +
                " WHERE cym=@cym ORDER BY rrid;";

            var l = DB.Main.Query<RefRece>(sql, new { cym = cyyyymm });
            return l.ToList();
        }

        public static List<RefRece> SelectAll()
        {
            var sql = SELECT_CLAUSE +
                " ORDER BY rrid;";

            var l = DB.Main.Query<RefRece>(sql);
            return l.ToList();
        }

        /// <summary>
        /// 指定されたAIDのデータを取得します。単一です。
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static RefRece SelectByAID(int aid)
        {
            var sql = SELECT_CLAUSE +
                " WHERE aid=@aid ORDER BY rrid;";

            var l = DB.Main.Query<RefRece>(sql, new { aid = aid });
            return l.ToList()[0];
        }
    }
}
