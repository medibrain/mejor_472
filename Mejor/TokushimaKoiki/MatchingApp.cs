﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mejor.TokushimaKoiki
{
    class MatchingApp
    {
        public static List<App> GetNotMatchApp(int cym)
        {
            var where = "FROM application AS a " +
                "LEFT OUTER JOIN refrece AS r ON a.aid = r.aid " +
                "WHERE r.aid IS NULL " +
                $"AND a.cym={cym} " +
                "AND (a.ayear>0 OR a.ayear=-999)";

            return App.InspectSelect(where);
        }

        public static List<App> GetOverlapApp(int cym)
        {
            var where = "FROM application AS a " +
                "WHERE a.numbering IN( " +
                    "SELECT a2.numbering FROM application AS a2 " +
                    $"WHERE a2.cym={cym} " +
                    "AND a2.ayear>0 " +
                    "GROUP BY a2.numbering HAVING COUNT(a2.numbering) > 1) ";

            return App.InspectSelect(where);
        }
    }
}
