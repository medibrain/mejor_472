﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Mejor.TokushimaKoiki
{
    class Export
    {
        public static bool ListExport(List<App> list, string fileName)
        {
            var scans = list.GroupBy(a => a.ScanID).Select(g => g.Key);

            try
            {
                using (var wf = new WaitForm())
                using (var sw = new StreamWriter(fileName, false, Encoding.UTF8))
                {
                    wf.Max = list.Count;
                    wf.BarStyle = ProgressBarStyle.Continuous;
                    wf.ShowDialogOtherTask();
                    wf.LogPrint("リストを出力しています…");

                    var h = "AID,処理年,処理月,No.,照会ID,記号番号,被保険者名,施術年,施術月,実日数,合計," +
                        "施術所番号,施術所名,宛名氏名,郵便番号,住所,電算番号,点検結果,照会理由,過誤理由,再審査理由,点検メモ";
                    sw.WriteLine(h);
                    var ss = new List<string>();

                    int no = 0;
                    string insNoStr = ((int)InsurerID.TOKUSHIMA_KOIKI).ToString();
                    foreach (var item in list)
                    {

                        var kd = RefRece.SelectByAID(item.Aid);
                        if (kd == null)
                        {
                            throw new Exception("広域からのデータがない、またはOCR確認が済んでいない申請書が照会対象として指定されています" +
                            "\r\nID:" + item.Aid.ToString());
                        }

                        no++;
                        var ymStr = (item.ChargeYear * 100 + item.ChargeMonth).ToString("0000");
                        ss.Add(item.Aid.ToString());
                        ss.Add(item.ChargeYear.ToString());
                        ss.Add(item.ChargeMonth.ToString());
                        ss.Add(no.ToString());
                        ss.Add(insNoStr + ymStr + no.ToString("0000"));
                        ss.Add(kd.Num);
                        ss.Add(kd.Name);
                        ss.Add(item.MediYear.ToString());
                        ss.Add(item.MediMonth.ToString());
                        ss.Add(item.CountedDays.ToString());
                        ss.Add(item.Total.ToString());
                        ss.Add(kd.ClinicNum);
                        ss.Add(kd.ClinicName);
                        ss.Add(kd.DestName == string.Empty ? kd.Name : kd.DestName);
                        ss.Add(kd.DestAdd == string.Empty ? kd.Zip : kd.DestZip);
                        ss.Add(kd.DestAdd == string.Empty ? kd.Add : kd.DestAdd);
                        ss.Add(kd.ComNum);
                        ss.Add(item.InspectInfo);
                        ss.Add(item.ShokaiReasonStr);
                        ss.Add(item.KagoReasonStr);
                        ss.Add(item.SaishinsaReasonStr);
                        ss.Add(item.MemoInspect);
                        sw.WriteLine(string.Join(",", ss));
                        ss.Clear();

                        wf.InvokeValue++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                MessageBox.Show("出力に失敗しました");
                return false;
            }
            MessageBox.Show("出力が終了しました");
            return true;
        }

        public static bool ExportViewerData(int cyyyymm)
        {
            string log = string.Empty;
            string fileName;
            using (var f = new SaveFileDialog())
            {
                f.FileName = "Info.txt";
                f.Filter = "Info.txt|Infoファイル";
                if (f.ShowDialog() != DialogResult.OK) return false;
                fileName = f.FileName;
            }

            var dir = Path.GetDirectoryName(fileName);
            var dataFile = dir + "\\" + cyyyymm.ToString() + ".csv";
            var imgDir = dir + "\\Img";
            Directory.CreateDirectory(imgDir);
            fileName = $"{dir}\\Info.txt";

            var wf = new WaitForm();
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("申請書を取得しています");
                var apps = App.GetApps(cyyyymm);

                wf.LogPrint("提供データを取得しています");
                var rrs = RefRece.Select(cyyyymm);
                var dic = new Dictionary<int, RefRece>();
                rrs.ForEach(rr => { if (rr.AID != 0) dic.Add(rr.AID, rr); });

                wf.LogPrint("Viewerデータを作成しています");
                wf.SetMax(apps.Count);
                wf.BarStyle = ProgressBarStyle.Continuous;

                int receCount = 0;
                using (var sw = new StreamWriter(dataFile, false, Encoding.UTF8))
                {
                    sw.WriteLine(ViewerData.Header);
                    var tiffs = new List<string>();
                    //先頭申請書外対策
                    int i = 0;
                    while (apps[i].YM < 0) i++;

                    //高速コピーのため
                    var fc = new TiffUtility.FastCopy();

                    while (i < apps.Count)
                    {
                        wf.InvokeValue = i;
                        var app = apps[i];
                        if (!dic.ContainsKey(app.Aid))
                        {
                            log += $"突合データがない申請書があります AID:{app.Aid}\r\n";
                            for (i++; i < apps.Count; i++) if (apps[i].YM > 0) break;
                            continue;
                        }

                        var rr = dic[app.Aid];
                        var vd = createViewData(app, rr);

                        sw.WriteLine(vd.CreateCsvLine());
                        tiffs.Add(app.GetImageFullPath());

                        //画像
                        for (i++; i < apps.Count; i++)
                        {
                            if (apps[i].YM > 0) break;
                            if (apps[i].YM == (int)APP_SPECIAL_CODE.続紙)
                                tiffs.Add(apps[i].GetImageFullPath());
                        }

                        TiffUtility.MargeOrCopyTiff(fc, tiffs, imgDir + "\\" + app.Aid.ToString() + ".tif");
                        tiffs.Clear();
                        receCount++;
                    }
                }

                if (!ViewerData.CreateInfo(fileName, Insurer.CurrrentInsurer.InsurerName, cyyyymm, receCount))
                    return false;

                string lastMsg = DateTime.Now.ToString() +
                    $"\r\nデータ出力処理を終了しました。\r\n" +
                    $"\r\n広域からのデータ総数   :{rrs.Count}" +
                    $"\r\nビューアデータ出力数   :{receCount}";
                log += lastMsg;
                log += $"{DateTime.Now}\r\n更新データの出力を終了しました。";
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                string logFn = dir + $"\\ExportLog_{DateTime.Now.ToString("yyMMdd-HHmmss")}.txt";
                using (var sw = new StreamWriter(logFn, false, Encoding.UTF8)) sw.Write(log);
                wf.Dispose();
            }
            return true;
        }

        private static ViewerData createViewData(App app, RefRece rr)
        {
            var vd = new ViewerData();
            vd.AID = app.Aid;
            vd.RrID = rr.RrID;
            vd.CYM = app.CYM;
            vd.YM = app.YM;
            vd.AppType = app.AppType;
            vd.Num = rr.Num;
            vd.Name = rr.Name;
            vd.Kana = rr.Kana;
            vd.Sex = SEX.Null;
            vd.Birth = 0;
            vd.ShokenDate = 0;
            vd.StartDate = 0;
            vd.Family = 0;
            if (app.AppType == APP_TYPE.鍼灸)
            {
                var fsFunc = new Func<string, string>(s =>
                {
                    int hid;
                    int.TryParse(s, out hid);
                    return (0 < hid && hid < 8) ? ((HARI_BYOMEI)hid).ToString() : s;
                });
                vd.Fusho1 = fsFunc(app.FushoName1);
                vd.Fusho2 = fsFunc(app.FushoName2);
                vd.Fusho3 = fsFunc(app.FushoName3);
                vd.Fusho4 = fsFunc(app.FushoName4);
                vd.Fusho5 = fsFunc(app.FushoName5);
            }
            else
            {
                vd.Fusho1 = app.FushoName1;
                vd.Fusho2 = app.FushoName2;
                vd.Fusho3 = app.FushoName3;
                vd.Fusho4 = app.FushoName4;
                vd.Fusho5 = app.FushoName5;
            }
            vd.AnmaCount = 0;
            vd.AnmaBody = false;
            vd.AnmaRightUpper = false;
            vd.AnmaLeftUpper = false;
            vd.AnmaRightLower = false;
            vd.AnmaLeftLower = false;
            vd.NewCont = app.NewContType;
            vd.Total = rr.Total;
            vd.Ratio = app.Ratio;
            vd.Partial = 0;
            vd.Charge = 0;
            vd.Days = rr.Days;
            vd.VisitFee = app.Distance == 999;
            vd.DrNum = string.Empty;
            vd.DrName = string.Empty;
            vd.ClinicNum = rr.ClinicNum;
            vd.ClinicName = rr.ClinicName;
            vd.GroupNum = string.Empty;
            vd.GroupName = string.Empty;
            vd.Zip = rr.Zip;
            vd.Adds = rr.Add;
            vd.DestName = rr.DestName;
            vd.DestKana = string.Empty;
            vd.DestZip = rr.DestZip;
            vd.DestAdds = rr.DestAdd;
            vd.Numbering = string.Empty;
            vd.ComNum = rr.ComNum;
            vd.ImageFile = app.Aid.ToString() + ".tif";

            return vd;
        }
    }
}
