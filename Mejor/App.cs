﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;
using System.Windows.Forms;
using System.Diagnostics;
using System.Linq;
using NPOI.SS.UserModel;

namespace Mejor
{

    //20190312134638 furukawa st ////////////////////////
    //大阪広域用分類追加

    /*public enum APP_SPECIAL_CODE { 総括票 = -1, 続紙 = -3, 不要 = -4, 同意書 = -5, 往療内訳 = -6, バッチ = -7, バッチ2 = -8, 長期 = -9,
        エラー = -999 }
        */
    /// <summary>
    /// ayearに入る申請書以外のコード
    /// </summary>
    public enum APP_SPECIAL_CODE
    {
        総括票 = -1, 続紙 = -3, 不要 = -4, 同意書 = -5, 往療内訳 = -6, バッチ = -7, バッチ2 = -8, 長期 = -9,
        同意書裏 = -11, 施術報告書 = -21, 状態記入書 = -31,その他=-99,//20220525175439 furukawa その他追加
                                                     
        エラー = -999
    }
    //20190312134638 furukawa ed ////////////////////////



    public enum HTYPE_SPECIAL_CODE { 災害一部負担支払猶予 = -1 }



    //20190312134748 furukawa st ////////////////////////
    //大阪広域用分類追加


    /*
    public enum APP_TYPE { NULL = 0, 医科 = 1, 柔整 = 6, 鍼灸 = 7, あんま = 8,
        複合 = 999,
        総括票 = -1, 続紙 = -3, 不要 = -4, 同意書 = -5, 往療内訳 = -6, バッチ = -7, バッチ2 = -8, 長期 = -9,
        エラー = -999}
        */
    /// <summary>
    /// aapptypeに入るコード
    /// </summary>
    public enum APP_TYPE
    {
        NULL = 0, 医科 = 1, 柔整 = 6, 鍼灸 = 7, あんま = 8,
        複合 = 999,
        総括票 = -1, 続紙 = -3, 不要 = -4, 同意書 = -5, 往療内訳 = -6, バッチ = -7, バッチ2 = -8, 長期 = -9,
        施術報告書 = -11, 状態記入書 = -13, 同意書裏 = -14,その他=-99,//20220525171537 furukawa その他追加
                                                     
        エラー = -999
    }
    //20190312134748 furukawa ed ////////////////////////



    public enum SEX { Null = 0, 男 = 1, 女 = 2 }
    public enum HARI_BYOMEI { Null = 0, 神経痛 = 1, リウマチ = 2, 頚腕症候群 = 3, 五十肩 = 4, 腰痛症 = 5, 頸椎捻挫後遺症 = 6, その他 = 7 }
    public enum NEW_CONT { Null = 0, 新規 = 1, 継続 = 2 }

    /// <summary>
    /// 申請書の状態を表すフラグ　必要なフラグがあれば連絡下さい(松本)
    /// </summary>
    [Flags]
    public enum StatusFlag
    {
        未処理 = 0x0,
        入力済 = 0x1, ベリファイ済 = 0x2, 自動マッチ済 = 0x4, 入力時エラー = 0x8,
        点検対象 = 0x10, 点検済 = 0x20, 返戻 = 0x40, 支払保留 = 0x80,
        照会対象 = 0x100, 保留 = 0x200, 過誤 = 0x400, 再審査 = 0x800,
        往療点検対象 = 0x1000, 往療疑義 = 0x2000, /*往療疑義なし = 0x4000,*/ 往療点検済 = 0x8000,
        追加入力済 = 0x1_0000, 追加ベリ済 = 0x2_0000, 拡張入力済 = 0x4_0000, 拡張ベリ済 = 0x8_0000,
        架電対象 = 0x10_0000, 支払済 = 0x20_0000, 処理1 = 0x40_0000, 処理2 = 0x80_0000,
        処理3 = 0x100_0000, 処理4 = 0x200_0000, 処理5 = 0x400_0000,
        独自処理1 = 0x1000_0000, 独自処理2 = 0x2000_0000,//20200522183245 furukawa 保険者個別機能用追加 9.2でも行けそう
        外部1回目済 = 0x4000_0000, //外部2回目済 = 0x800_0000, //20221216 ito プロジェクトK入力対応、これ以上増える場合は点検関連は別フラグにすべき。
    }//シャープ健保の場合、処理4は医科レセに被保番と診療年月が合致したもの

    public class App
    {
        #region member
        [DB.DbAttribute.PrimaryKey]
        public int Aid { get; set; }
        public int ScanID { get; set; }
        public int GroupID { get; set; }

        public int MediYear { get; set; }
        public int MediMonth { get; set; }
        public string InsNum { get; set; } = string.Empty;

        //被保険者情報
        public string HihoNum { get; set; } = string.Empty;
        public int HihoPref { get; set; }
        public int HihoType { get; set; }
        public string HihoName { get; set; } = string.Empty;
        public string HihoZip { get; set; } = string.Empty;
        public string HihoAdd { get; set; } = string.Empty;
        public string PersonName { get; set; } = string.Empty;
        public int Sex { get; set; }
        public DateTime Birthday { get; set; }

        public string ClinicNum { get; set; } = string.Empty;
        private int Single { get; set; }
        public int Family { get; set; }
        public int Ratio { get; set; }
        public string PublcExpense { get; set; } = string.Empty;    //公費番号

        private string ImageFile { get; set; } = string.Empty;
        public int ChargeYear { get; private set; }//処理年
        public int ChargeMonth { get; private set; }//処理月


        //負傷関連情報
        public string FushoName1 { get; set; } = string.Empty;      //負傷名
        public DateTime FushoDate1 { get; set; }                    //初診日（最初に受診した日付）
        public DateTime FushoFirstDate1 { get; set; }               //初検日（最初にその負傷を認めた日）
        public DateTime FushoStartDate1 { get; set; }               //施術開始年月日（その負傷の治療を開始した日）
        public DateTime FushoFinishDate1 { get; set; }              //施術終了年月日（その負傷の治療を終了した日）
        public int FushoDays1 { get; set; }                         //実日数（診療に要した日数）
        public int FushoCourse1 { get; set; }                       //転帰
        public string FushoName2 { get; set; } = string.Empty;
        public DateTime FushoDate2 { get; set; }
        public DateTime FushoFirstDate2 { get; set; }
        public DateTime FushoStartDate2 { get; set; }
        public DateTime FushoFinishDate2 { get; set; }
        public int FushoDays2 { get; set; }
        public int FushoCourse2 { get; set; }
        public string FushoName3 { get; set; } = string.Empty;
        public DateTime FushoDate3 { get; set; }
        public DateTime FushoFirstDate3 { get; set; }
        public DateTime FushoStartDate3 { get; set; }
        public DateTime FushoFinishDate3 { get; set; }
        public int FushoDays3 { get; set; }
        public int FushoCourse3 { get; set; }
        public string FushoName4 { get; set; } = string.Empty;
        public DateTime FushoDate4 { get; set; }
        public DateTime FushoFirstDate4 { get; set; }
        public DateTime FushoStartDate4 { get; set; }
        public DateTime FushoFinishDate4 { get; set; }
        public int FushoDays4 { get; set; }
        public int FushoCourse4 { get; set; }
        public string FushoName5 { get; set; } = string.Empty;
        public DateTime FushoDate5 { get; set; }
        public DateTime FushoFirstDate5 { get; set; }
        public DateTime FushoStartDate5 { get; set; }
        public DateTime FushoFinishDate5 { get; set; }
        public int FushoDays5 { get; set; }
        public int FushoCourse5 { get; set; }


        //新規継続区分
        public NEW_CONT NewContType { get; set; }

        //往療関連
        public int Distance { get; set; }
        public int VisitTimes { get; set; }
        public int VisitFee { get; set; }
        public int VisitAdd { get; set; }


        //施術し関連情報
        public string DrNum { get; set; } = string.Empty;
        public string ClinicZip { get; set; } = string.Empty;
        public string ClinicAdd { get; set; } = string.Empty;
        public string ClinicName { get; set; } = string.Empty;
        public string ClinicTel { get; set; } = string.Empty;
        public string DrName { get; set; } = string.Empty;
        public string DrKana { get; set; } = string.Empty;

        //口座関連
        public int AccountType { get; set; }
        public string BankName { get; set; } = string.Empty;
        public int BankType { get; set; }
        public string BankBranch { get; set; } = string.Empty;
        public int BankBranchType { get; set; }
        public string AccountName { get; set; } = string.Empty;
        public string AccountKana { get; set; } = string.Empty;
        public string AccountNumber { get; set; } = string.Empty;


        //お金関連
        public int Total { get; set; }
        public int Partial { get; set; }
        public int Charge { get; set; }
        public int CountedDays { get; set; }
        public string Numbering { get; set; } = string.Empty;

        //レセプト種別
        public APP_TYPE AppType { get; set; }



        //補足・付随
        public int Ufirst { get; set; }
        public int Usecond { get; set; }
        public int Uinquiry { get; set; }
        public int Bui { get; set; }//あんま部位




        public int CYM { get; set; }
        public int YM { get; set; }
        public StatusFlag StatusFlags { get; private set; }
        public ShokaiReason ShokaiReason { get; private set; }
        public int RrID { get; set; }



        public int AdditionalUid1 { get; set; }
        public int AdditionalUid2 { get; set; }
        public string MemoShokai { get; set; } = string.Empty;
        public string MemoInspect { get; set; } = string.Empty;
        public string Memo { get; set; } = string.Empty;
        public string PayCode { get; set; } = string.Empty;
        public string ShokaiCode { get; set; } = string.Empty;
        public string OcrData { get; set; } = string.Empty;
        public int UfirstEx { get; set; }
        public int UsecondEx { get; set; }

        public KagoReasons KagoReasons { get; set; }//現行


        public SaishinsaReasons SaishinsaReasons { get; set; }
        public HenreiReasons HenreiReasons { get; set; }
        public string ComNum { get; set; }
        public string GroupNum { get; set; }
        public string OutMemo { get; set; } = string.Empty;
        public TaggedDatas TaggedDatas { get; private set; }

        public string strKagoReasonsXML { get; set; }//試験用xml文字列取得



        public string JCYM => DateTimeEx.GetInitialEraJpYearMonth(CYM);
        public string JYM => DateTimeEx.GetInitialEraJpYearMonth(YM);


        //データベース外
        public int ViewIndex { get; set; }
        public bool Select { get; set; } = false;

        public int FushoCount =>
            !string.IsNullOrWhiteSpace(FushoName5) ? 5 :
            !string.IsNullOrWhiteSpace(FushoName4) ? 4 :
            !string.IsNullOrWhiteSpace(FushoName3) ? 3 :
            !string.IsNullOrWhiteSpace(FushoName2) ? 2 :
            !string.IsNullOrWhiteSpace(FushoName1) ? 1 : 0;

        public StatusFlag InputStatus =>
            StatusFlagCheck(StatusFlag.ベリファイ済) ? StatusFlag.ベリファイ済 :
            StatusFlagCheck(StatusFlag.入力済) ? StatusFlag.入力済 :
            StatusFlagCheck(StatusFlag.自動マッチ済) ? StatusFlag.自動マッチ済 :
            StatusFlagCheck(StatusFlag.外部1回目済) ? StatusFlag.外部1回目済 ://20221216 ito プロジェクトK入力対応
            StatusFlag.未処理;

        public StatusFlag InputStatusEx =>
            StatusFlagCheck(StatusFlag.拡張ベリ済) ? StatusFlag.拡張ベリ済 :
            StatusFlagCheck(StatusFlag.拡張入力済) ? StatusFlag.拡張入力済 :
            StatusFlagCheck(StatusFlag.ベリファイ済) ? StatusFlag.ベリファイ済 :
            StatusFlagCheck(StatusFlag.入力済) ? StatusFlag.入力済 :
            StatusFlagCheck(StatusFlag.自動マッチ済) ? StatusFlag.自動マッチ済 :
            StatusFlag.未処理;


        //20200522174744 furukawa st ////////////////////////
        //保険者個別機能


        public StatusFlag InputStatusSP =>
            StatusFlagCheck(StatusFlag.独自処理2) ? StatusFlag.独自処理2 :
            StatusFlagCheck(StatusFlag.独自処理1) ? StatusFlag.独自処理1 :
          StatusFlagCheck(StatusFlag.拡張ベリ済) ? StatusFlag.拡張ベリ済 :
          StatusFlagCheck(StatusFlag.拡張入力済) ? StatusFlag.拡張入力済 :
          StatusFlagCheck(StatusFlag.ベリファイ済) ? StatusFlag.ベリファイ済 :
          StatusFlagCheck(StatusFlag.入力済) ? StatusFlag.入力済 :
          StatusFlagCheck(StatusFlag.自動マッチ済) ? StatusFlag.自動マッチ済 :
          StatusFlag.未処理;

        //20200522174744 furukawa ed ////////////////////////


        //20200608190800 furukawa st ////////////////////////
        //宮城県国保専用のステータス

        public StatusFlag InputStatusSP2 =>
                      StatusFlagCheck(StatusFlag.追加ベリ済) ? StatusFlag.追加ベリ済 :
          StatusFlagCheck(StatusFlag.追加入力済) ? StatusFlag.追加入力済 :
                      StatusFlagCheck(StatusFlag.拡張ベリ済) ? StatusFlag.拡張ベリ済 :
          StatusFlagCheck(StatusFlag.拡張入力済) ? StatusFlag.拡張入力済 :
            StatusFlagCheck(StatusFlag.独自処理2) ? StatusFlag.独自処理2 :
            StatusFlagCheck(StatusFlag.独自処理1) ? StatusFlag.独自処理1 :
          StatusFlagCheck(StatusFlag.ベリファイ済) ? StatusFlag.ベリファイ済 :
          StatusFlagCheck(StatusFlag.入力済) ? StatusFlag.入力済 :
          StatusFlagCheck(StatusFlag.自動マッチ済) ? StatusFlag.自動マッチ済 :
          StatusFlag.未処理;
        //20200608190800 furukawa ed ////////////////////////


        public string InputStatusAdd =>
            StatusFlagCheck(StatusFlag.追加ベリ済) ? "追加ベリ済" :
            StatusFlagCheck(StatusFlag.追加入力済) ? "追加入力済" :
            string.Empty;

        public string ShokaiHenreiStatus =>
            StatusFlagCheck(StatusFlag.照会対象 | StatusFlag.返戻) ? "照会/返戻" :
            StatusFlagCheck(StatusFlag.照会対象) ? "照会" :
            StatusFlagCheck(StatusFlag.返戻) ? "返戻" :
            StatusFlagCheck(StatusFlag.支払保留) ? "払保留" :
            string.Empty;

        public string TenkenResult =>
            StatusFlagCheck(StatusFlag.過誤) ? "過誤" :
            StatusFlagCheck(StatusFlag.再審査) ? "再審査" :
            StatusFlagCheck(StatusFlag.保留) ? "保留" :
            StatusFlagCheck(StatusFlag.点検済) ? "申出なし" :
            StatusFlagCheck(StatusFlag.点検対象) ? "未点検" :
            string.Empty;

        public string OryoResult =>
            StatusFlagCheck(StatusFlag.往療疑義) ? "往療疑義" :
            StatusFlagCheck(StatusFlag.往療点検済) ? "往療疑義なし" :
            StatusFlagCheck(StatusFlag.往療点検済) ? "往療未点検済" :
            string.Empty;


        //20201008172439 furukawa KagoReasons_xmlの初期化文字列        
        /// <summary>
        /// KagoReason_xmlの初期化文字列
        /// </summary>
        public string KagoReasons_xml_InitString =
            "<kagoreasons>\r" +
            "<なし>false</なし>\r" +
            "<署名違い>false</署名違い>\r" +
            "<筆跡違い>false</筆跡違い>\r" +
            "<家族同一筆跡>false</家族同一筆跡>\r" +
            "<その他>false</その他>\r" +
            "<原因なし>false</原因なし>\r" +
            "<原因なし1>false</原因なし1>\r" +
            "<原因なし2>false</原因なし2>\r" +
            "<原因なし3>false</原因なし3>\r" +
            "<原因なし4>false</原因なし4>\r" +
            "<原因なし5>false</原因なし5>\r" +
            "<負傷原因相違>false</負傷原因相違>\r" +
            "<負傷原因相違1>false</負傷原因相違1>\r" +
            "<負傷原因相違2>false</負傷原因相違2>\r" +
            "<負傷原因相違3>false</負傷原因相違3>\r" +
            "<負傷原因相違4>false</負傷原因相違4>\r" +
            "<負傷原因相違5>false</負傷原因相違5>\r" +
            "<長期理由なし>false</長期理由なし>\r" +
            "<長期理由なし1>false</長期理由なし1>\r" +
            "<長期理由なし2>false</長期理由なし2>\r" +
            "<長期理由なし3>false</長期理由なし3>\r" +
            "<長期理由なし4>false</長期理由なし4>\r" +
            "<長期理由なし5>false</長期理由なし5>\r" +
            "<長期理由相違>false</長期理由相違>\r" +
            "<長期理由相違1>false</長期理由相違1>\r" +
            "<長期理由相違2>false</長期理由相違2>\r" +
            "<長期理由相違3>false</長期理由相違3>\r" +
            "<長期理由相違4>false</長期理由相違4>\r" +
            "<長期理由相違5>false</長期理由相違5>\r" +
            "<療養費請求権の消滅時効>false</療養費請求権の消滅時効>\r" +
            "<本家区分誤り>false</本家区分誤り>\r" +
            "<保険者相違>false</保険者相違>\r" +
            "<往療理由記載なし>false</往療理由記載なし>\r" +
            "<往療16km以上>false</往療16km以上>\r" +
            "<同意書>false</同意書>\r" +
            "<同意書添付なし>false</同意書添付なし>\r" +
            "<同意書期限切れ>false</同意書期限切れ>\r" +
            "<施術報告書>false</施術報告書>\r" +
            "<施術報告書添付なし>false</施術報告書添付なし>\r" +
            "<施術報告書記載不備>false</施術報告書記載不備>\r" +
            "<施術継続理由状態記入書>false</施術継続理由状態記入書>\r" +
            "<施術継続理由状態記入書添付なし>false</施術継続理由状態記入書添付なし>\r" +
            "<施術継続理由状態記入書記載不備>false</施術継続理由状態記入書記載不備>\r" +

            //20210201160459 furukawa st ////////////////////////
            //往療内訳書追加            
            "<往療内訳書>false</往療内訳書>\r" +
            "<往療内訳書添付なし>false</往療内訳書添付なし>\r" +
            "<往療内訳書記載不備>false</往療内訳書記載不備>\r" +
            //20210201160459 furukawa ed ////////////////////////

            "<独自>false</独自>\r" +
            "<独自1>false</独自1>\r" +
            "<独自2>false</独自2>\r" +
            "<独自3>false</独自3>\r" +
            "<独自4>false</独自4>\r" +
            "<独自5>false</独自5>\r" +
            "</kagoreasons>";




        /// <summary>
        /// 過誤理由設定値
        /// </summary>
        public enum FlgValue {
            add,
            del,
            other,
            check,

        }

        #endregion

        #region 過誤設定
        /// <summary>
        /// 過誤理由設定
        /// </summary>
        /// <param name="strFlag">どのフラグか</param>
        /// <param name="flg">追加削除等</param>
        /// <param name="Value">自由設定値</param>
        /// <returns></returns>
        public bool KagoReasons_xml(string strFlag, FlgValue flg, string Value = "")
        {
            string strTag = string.Empty;
            string strpattern = $"<{strFlag}>.+</{strFlag}>";


            if (strFlag == KagoReasons_Member.初期化.ToString())
            {
                strKagoReasonsXML = KagoReasons_xml_InitString;

                return true;
            }


            if (flg != FlgValue.check)
            {
                switch (flg)
                {
                    case FlgValue.add:
                        strTag = $"<{strFlag}>true</{strFlag}>";
                        break;
                    case FlgValue.del:
                        strTag = $"<{strFlag}>false</{strFlag}>";
                        break;
                    case FlgValue.other:
                        strTag = $"<{strFlag}>{Value}</{strFlag}>";
                        break;

                }

                strKagoReasonsXML = System.Text.RegularExpressions.Regex.Replace(strKagoReasonsXML, strpattern, strTag);

                return true;
            }
            else
            {
                strpattern = $"<{strFlag}>true</{strFlag}>";
                if (System.Text.RegularExpressions.Regex.IsMatch(strKagoReasonsXML, strpattern)) return true;
                else return false;
            }
        }






        /// <summary>
        /// 過誤理由文字列　フラグがあれば、そのフラグに対応する文字列を返す
        /// </summary>
        public string KagoReasonStr
        {

            get
            {

                //20201008172548 furukawa st ////////////////////////
                //KagoReasons_xmlを判定して文字列設定

                var l = new List<string>();
                if (KagoReasonsCheck_xml(KagoReasons_Member.原因なし))
                {
                    var f = (KagoReasonsCheck_xml(KagoReasons_Member.原因なし1) ? "①" : "") +
                       (KagoReasonsCheck_xml(KagoReasons_Member.原因なし2) ? "②" : "") +
                       (KagoReasonsCheck_xml(KagoReasons_Member.原因なし3) ? "③" : "") +
                       (KagoReasonsCheck_xml(KagoReasons_Member.原因なし4) ? "④" : "") +
                       (KagoReasonsCheck_xml(KagoReasons_Member.原因なし5) ? "⑤" : "");
                    l.Add("負傷原因なし" + f);
                }
                if (KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし))
                {
                    var f = (KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし1) ? "①" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし2) ? "②" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし3) ? "③" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし4) ? "④" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由なし5) ? "⑤" : "");
                    l.Add("長期理由なし" + f);
                }
                if (KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違))
                {
                    var f = (KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違1) ? "①" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違2) ? "②" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違3) ? "③" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違4) ? "④" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.負傷原因相違5) ? "⑤" : "");
                    l.Add("負傷原因相違" + f);
                }
                if (KagoReasonsCheck_xml(KagoReasons_Member.署名違い)) l.Add("署名違い");
                if (KagoReasonsCheck_xml(KagoReasons_Member.筆跡違い)) l.Add("筆跡違い");
                if (KagoReasonsCheck_xml(KagoReasons_Member.家族同一筆跡)) l.Add("家族同一筆跡");
                if (KagoReasonsCheck_xml(KagoReasons_Member.その他)) l.Add("その他");

                if (KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違))
                {
                    var f = (KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違1) ? "①" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違2) ? "②" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違3) ? "③" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違4) ? "④" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.長期理由相違5) ? "⑤" : "");
                    l.Add("長期理由相違" + f);
                }

                if (KagoReasonsCheck_xml(KagoReasons_Member.療養費請求権の消滅時効)) l.Add("療養費請求権の消滅時効");
                if (KagoReasonsCheck_xml(KagoReasons_Member.本家区分誤り)) l.Add("本家区分誤り");
                if (KagoReasonsCheck_xml(KagoReasons_Member.保険者相違)) l.Add("保険者相違");
                if (KagoReasonsCheck_xml(KagoReasons_Member.往療理由記載なし)) l.Add("往療理由記載なし");
                if (KagoReasonsCheck_xml(KagoReasons_Member.往療16km以上)) l.Add("往療16km以上");

                if (KagoReasonsCheck_xml(KagoReasons_Member.同意書)) l.Add("同意書");

                //20210201150606 furukawa st ////////////////////////
                //単語が重なっていたのを削除

                if (KagoReasonsCheck_xml(KagoReasons_Member.同意書添付なし)) l.Add("添付なし");

                //20210531170144 furukawa st ////////////////////////
                //期限切れでなく不備に変更               
                if (KagoReasonsCheck_xml(KagoReasons_Member.同意書期限切れ)) l.Add("記載不備");
                //      if (KagoReasonsCheck_xml(KagoReasons_Member.同意書不備)) l.Add("期限切れ");
                //20210531170144 furukawa ed ////////////////////////


                //if (KagoReasonsCheck_xml(KagoReasons_Member.同意書添付なし)) l.Add("同意書添付なし");
                //if (KagoReasonsCheck_xml(KagoReasons_Member.同意書期限切れ)) l.Add("同意書期限切れ");
                //20210201150606 furukawa ed ////////////////////////



                if (KagoReasonsCheck_xml(KagoReasons_Member.施術報告書)) l.Add("施術報告書");

                //20210201150658 furukawa st ////////////////////////
                //単語が重なっていたのを削除

                if (KagoReasonsCheck_xml(KagoReasons_Member.施術報告書添付なし)) l.Add("添付なし");
                if (KagoReasonsCheck_xml(KagoReasons_Member.施術報告書記載不備)) l.Add("記載不備");
                //if (KagoReasonsCheck_xml(KagoReasons_Member.施術報告書添付なし)) l.Add("施術報告書添付なし");
                //if (KagoReasonsCheck_xml(KagoReasons_Member.施術報告書記載不備)) l.Add("施術報告書記載不備");
                //20210201150658 furukawa ed ////////////////////////


                if (KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書)) l.Add("施術継続理由状態記入書");

                //20210201150715 furukawa st ////////////////////////
                //単語が重なっていたのを削除

                if (KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書添付なし)) l.Add("添付なし");
                if (KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書記載不備)) l.Add("記載不備");
                //if (KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書添付なし)) l.Add("施術継続理由状態記入書添付なし");
                //if (KagoReasonsCheck_xml(KagoReasons_Member.施術継続理由状態記入書記載不備)) l.Add("施術継続理由状態記入書記載不備");
                //20210201150715 furukawa ed ////////////////////////



                //20210201155222 furukawa st ////////////////////////
                //KagoReasons_xmlを判定して文字列設定

                if (KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書)) l.Add("往療内訳書");
                if (KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書添付なし)) l.Add("添付なし");
                if (KagoReasonsCheck_xml(KagoReasons_Member.往療内訳書記載不備)) l.Add("記載不備");
                //20210201155222 furukawa ed ////////////////////////



                if (KagoReasonsCheck_xml(KagoReasons_Member.独自))
                {
                    var f = (KagoReasonsCheck_xml(KagoReasons_Member.独自1) ? "①" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.独自2) ? "②" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.独自3) ? "③" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.独自4) ? "④" : "") +
                        (KagoReasonsCheck_xml(KagoReasons_Member.独自5) ? "⑤" : "");
                    l.Add("独自" + f);
                }

                #region 更新前のコード
                //var l = new List<string>();
                //if (KagoReasonsCheck(KagoReasons.原因なし))
                //{
                //    var f = (KagoReasonsCheck(KagoReasons.原因なし1) ? "①" : "") +
                //       (KagoReasonsCheck(KagoReasons.原因なし2) ? "②" : "") +
                //       (KagoReasonsCheck(KagoReasons.原因なし3) ? "③" : "") +
                //       (KagoReasonsCheck(KagoReasons.原因なし4) ? "④" : "") +
                //       (KagoReasonsCheck(KagoReasons.原因なし5) ? "⑤" : "");
                //    l.Add("負傷原因なし" + f);
                //}
                //if (KagoReasonsCheck(KagoReasons.長期理由なし))
                //{
                //    var f = (KagoReasonsCheck(KagoReasons.長期理由なし1) ? "①" : "") +
                //        (KagoReasonsCheck(KagoReasons.長期理由なし2) ? "②" : "") +
                //        (KagoReasonsCheck(KagoReasons.長期理由なし3) ? "③" : "") +
                //        (KagoReasonsCheck(KagoReasons.長期理由なし4) ? "④" : "") +
                //        (KagoReasonsCheck(KagoReasons.長期理由なし5) ? "⑤" : "");
                //    l.Add("長期理由なし" + f);
                //}
                //if (KagoReasonsCheck(KagoReasons.負傷原因相違))
                //{
                //    var f = (KagoReasonsCheck(KagoReasons.負傷原因相違1) ? "①" : "") +
                //        (KagoReasonsCheck(KagoReasons.負傷原因相違2) ? "②" : "") +
                //        (KagoReasonsCheck(KagoReasons.負傷原因相違3) ? "③" : "") +
                //        (KagoReasonsCheck(KagoReasons.負傷原因相違4) ? "④" : "") +
                //        (KagoReasonsCheck(KagoReasons.負傷原因相違5) ? "⑤" : "");
                //    l.Add("負傷原因相違" + f);
                //}
                //if (KagoReasonsCheck(KagoReasons.署名違い)) l.Add("署名違い");
                //if (KagoReasonsCheck(KagoReasons.筆跡違い)) l.Add("筆跡違い");
                //if (KagoReasonsCheck(KagoReasons.家族同一筆跡)) l.Add("家族同一筆跡");
                //if (KagoReasonsCheck(KagoReasons.その他)) l.Add("その他");
                #endregion



                //20201008172548 furukawa ed ////////////////////////








                return string.Join(" ", l);
            }
        }

        #endregion

        #region 再審査設定
        /// <summary>
        /// 再審査理由文字列
        /// </summary>
        public string SaishinsaReasonStr
        {
            get
            {
                if (SaishinsaReasonsCheck(SaishinsaReasons.初検料))
                {
                    var f = (SaishinsaReasonsCheck(SaishinsaReasons.負傷1) ? "①" : "") +
                        (SaishinsaReasonsCheck(SaishinsaReasons.負傷2) ? "②" : "") +
                        (SaishinsaReasonsCheck(SaishinsaReasons.負傷3) ? "③" : "") +
                        (SaishinsaReasonsCheck(SaishinsaReasons.負傷4) ? "④" : "") +
                        (SaishinsaReasonsCheck(SaishinsaReasons.負傷5) ? "⑤" : "");
                    f = string.IsNullOrEmpty(f) ? "" : "" + f;

                    var tenki =
                        SaishinsaReasonsCheck(SaishinsaReasons.転帰なし) ? "転帰なし" :
                        SaishinsaReasonsCheck(SaishinsaReasons.中止) ? "中止" : "";

                    var fr = SaishinsaReasonsCheck(SaishinsaReasons.同一負傷名) ? "同一負傷" :
                        SaishinsaReasonsCheck(SaishinsaReasons.別負傷名) ? "別負傷" : "";

                    return "初検料疑義 先月:" + f + tenki + "　当月:" + fr;
                }
                else if (SaishinsaReasonsCheck(SaishinsaReasons.その他))
                {
                    return "その他";
                }
                else return string.Empty;
            }
        }
        #endregion

        #region 照会理由設定
        public string ShokaiReasonStr
        {
            get
            {
                var l = new List<string>();
                if (ShokaiReasonCheck(ShokaiReason.合計金額)) l.Add("合計金額");
                if (ShokaiReasonCheck(ShokaiReason.施術日数)) l.Add("施術日数");
                if (ShokaiReasonCheck(ShokaiReason.施術期間)) l.Add("施術期間");
                if (ShokaiReasonCheck(ShokaiReason.部位数)) l.Add("部位数");
                if (ShokaiReasonCheck(ShokaiReason.疑義施術所)) l.Add("疑義施術所");
                if (ShokaiReasonCheck(ShokaiReason.その他)) l.Add("その他");
                return string.Join(" ", l);
            }
        }

        public string ShokaiResultStr =>
            !StatusFlagCheck(StatusFlag.照会対象) ? string.Empty :
            ShokaiReasonCheck(ShokaiReason.相違あり) ? "相違あり" :
            ShokaiReasonCheck(ShokaiReason.相違なし) ? "相違なし" :
            ShokaiReasonCheck(ShokaiReason.宛所なし) ? "宛所なし" :
            "未返信";

        #endregion

        /// <summary>
        /// 点検詳細
        /// </summary>
        public string InspectDescription
        {
            get
            {
                var l = new List<string>();
                if (!string.IsNullOrWhiteSpace(KagoReasonStr)) l.Add("過誤理由:" + KagoReasonStr);
                if (!string.IsNullOrWhiteSpace(SaishinsaReasonStr)) l.Add("再審査理由:" + SaishinsaReasonStr);
                if (!string.IsNullOrWhiteSpace(MemoInspect)) l.Add("点検メモ:" + MemoInspect);
                return string.Join(" * ", l);
            }
        }

        /// <summary>
        /// 返戻理由文字列
        /// </summary>
        public string HenreiReasonStr
        {
            get
            {
                var l = new List<string>();
                if (HenreiReasonsCheck(HenreiReasons.負傷部位))
                {
                    var f = (HenreiReasonsCheck(HenreiReasons.負傷部位1) ? "①" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷部位2) ? "②" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷部位3) ? "③" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷部位4) ? "④" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷部位5) ? "⑤" : "");
                    l.Add("負傷部位" + f);
                }
                if (HenreiReasonsCheck(HenreiReasons.負傷原因))
                {
                    var f = (HenreiReasonsCheck(HenreiReasons.負傷原因1) ? "①" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷原因2) ? "②" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷原因3) ? "③" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷原因4) ? "④" : "") +
                        (HenreiReasonsCheck(HenreiReasons.負傷原因5) ? "⑤" : "");
                    l.Add("負傷原因" + f);
                }
                if (HenreiReasonsCheck(HenreiReasons.けが外)) l.Add("けが以外");
                if (HenreiReasonsCheck(HenreiReasons.受療日数)) l.Add("受療日数");
                if (HenreiReasonsCheck(HenreiReasons.負傷時期)) l.Add("負傷時期");
                if (HenreiReasonsCheck(HenreiReasons.過誤)) l.Add("過誤");
                if (HenreiReasonsCheck(HenreiReasons.勤務中)) l.Add("勤務中");
                if (HenreiReasonsCheck(HenreiReasons.その他)) l.Add("その他");
                return string.Join(" ", l);
            }
        }

        /// <summary>
        /// 点検結果　(過誤/再審査/保留/往療疑義)
        /// </summary>
        public string InspectInfo
        {
            get
            {
                var ss = new List<string>();
                if (StatusFlagCheck(StatusFlag.点検対象))
                {
                    if (StatusFlagCheck(StatusFlag.過誤)) ss.Add("過誤");
                    if (StatusFlagCheck(StatusFlag.再審査)) ss.Add("再審査");
                    if (StatusFlagCheck(StatusFlag.保留)) ss.Add("保留");
                }
                else if (StatusFlagCheck(StatusFlag.往療点検対象))
                {
                    if (StatusFlagCheck(StatusFlag.往療疑義)) ss.Add("往療疑義");
                }
                return string.Join(" / ", ss);
            }
        }


        public int InputOrderNumber =>
            StatusFlagCheck(StatusFlag.ベリファイ済) ? 4 :
            StatusFlagCheck(StatusFlag.入力済) ? 3 :
            StatusFlagCheck(StatusFlag.自動マッチ済) ? 2 : 1;



        #region ステータスフラグ
        public void StatusFlagSet(StatusFlag flag) => StatusFlags |= flag;
        public void StatusFlagRemove(StatusFlag flag) => StatusFlags &= ~flag;
        public bool StatusFlagCheck(StatusFlag flag) => (StatusFlags & flag) == flag;
        #endregion

        #region 照会理由
        public void ShokaiReasonOverwrite(ShokaiReason reason) => ShokaiReason = reason;
        public void ShokaiReasonSet(ShokaiReason reason) => ShokaiReason |= reason;
        public void ShokaiReasonRemove(ShokaiReason reason) => ShokaiReason &= ~reason;
        public bool ShokaiReasonCheck(ShokaiReason reason) => (ShokaiReason & reason) == reason;
        #endregion

        #region 過誤理由
        public void KagoReasonsSet_xml(KagoReasons_Member flag) => KagoReasons_xml(flag.ToString(), App.FlgValue.add);
        public void KagoReasonsRemove_xml(KagoReasons_Member flag) => KagoReasons_xml(flag.ToString(), App.FlgValue.del);
        public bool KagoReasonsCheck_xml(KagoReasons_Member flag) => KagoReasons_xml(flag.ToString(), App.FlgValue.check);
        public void KagoReasonsSet(KagoReasons flag) => KagoReasons |= flag;
        public void KagoReasonsRemove(KagoReasons flag) => KagoReasons &= ~flag;
        public bool KagoReasonsCheck(KagoReasons flag) => (KagoReasons & flag) == flag;
        #endregion

        #region 再審査理由
        public void SaishinsaReasonsSet(SaishinsaReasons flag) => SaishinsaReasons |= flag;
        public void SaishinsaReasonsRemove(SaishinsaReasons flag) => SaishinsaReasons &= ~flag;
        public bool SaishinsaReasonsCheck(SaishinsaReasons flag) => (SaishinsaReasons & flag) == flag;
        #endregion

        #region 返戻理由
        public void HenreiReasonsSet(HenreiReasons flag) => HenreiReasons |= flag;
        public void HenreiReasonsRemove(HenreiReasons flag) => HenreiReasons &= ~flag;
        public bool HenreiReasonsCheck(HenreiReasons flag) => (HenreiReasons & flag) == flag;
        #endregion



        //20201019110609 furukawa st ////////////////////////
        //新規継続の判定

        /// <summary>
        /// 新規継続判定
        /// </summary>
        /// <param name="app">app</param>
        /// <param name="txtMediY">施術年コントロール</param>
        /// <param name="txtMediM">施術月コントロール</param>
        /// <param name="txtFirstY">初検年コントロール</param>
        /// <param name="txtFirstM">初検月コントロール</param>
        public static bool CheckNewContinue(App app, VerifyBox txtMediY, VerifyBox txtMediM, VerifyBox txtFirstY, VerifyBox txtFirstM)
        {
            if (!int.TryParse(txtMediY.Text.Trim(), out int mediY)) { MessageBox.Show($"{txtMediY.Text}が数値ではありません"); return false; }
            if (!int.TryParse(txtMediM.Text.Trim(), out int mediM)) { MessageBox.Show($"{txtMediM.Text}が数値ではありません"); return false; }
            if (!int.TryParse(txtFirstY.Text.Trim(), out int firstY)) { MessageBox.Show($"{txtFirstY.Text}が数値ではありません"); return false; }
            if (!int.TryParse(txtFirstM.Text.Trim(), out int firstM)) { MessageBox.Show($"{txtFirstM.Text}が数値ではありません"); return false; }

            app.NewContType =
                DateTimeEx.GetAdYearFromHs(mediY * 100 + mediM) == firstY &&
                firstM == mediM ? NEW_CONT.新規 : NEW_CONT.継続;
            return true;
        }
        //20201019110609 furukawa ed ////////////////////////






        /// <summary>
        /// SELECTをかける時に期待するSELECT文はCreateAppFromRecordで使用する
        /// 順番である必要があるので、ここで定義。FROM application as aを忘れずに
        /// </summary>
        private const string SELECT_CLAUSE =
            "a.aid, a.scanid, a.groupid, a.ayear, a.amonth, a.inum, a.hnum, a.hpref, a.htype, a.hname, " +
            "a.hzip, a.haddress, a.pname, a.psex, a.pbirthday, a.sid, a.asingle, a.afamily, a.aratio, " +
            "a.publcexpense, a.aimagefile, a.achargeyear, a.achargemonth, " +
            "a.iname1, a.idate1, a.ifirstdate1, a.istartdate1, a.ifinishdate1, a.idays1, a.icourse1, " +
            "a.iname2, a.idate2, a.ifirstdate2, a.istartdate2, a.ifinishdate2, a.idays2, a.icourse2, " +
            "a.iname3, a.idate3, a.ifirstdate3, a.istartdate3, a.ifinishdate3, a.idays3, a.icourse3, " +
            "a.iname4, a.idate4, a.ifirstdate4, a.istartdate4, a.ifinishdate4, a.idays4, a.icourse4, " +
            "a.iname5, a.idate5, a.ifirstdate5, a.istartdate5, a.ifinishdate5, a.idays5, a.icourse5, " +
            "a.fchargetype, a.fdistance, a.fvisittimes, a.fvisitfee, a.fvisitadd, " +
            "a.sregnumber, a.szip, a.saddress, a.sname, a.stel, a.sdoctor, a.skana, " +
            "a.bacctype, a.bname, a.btype, a.bbranch, a.bbranchtype, a.baccname, a.bkana, a.baccnumber, " +
            "a.atotal, a.apartial, a.acharge, a.acounteddays, a.numbering, a.aapptype, " +
            "a.ufirst, a.usecond, a.uinquiry, a.bui, a.cym, a.ym, a.statusflags, " +
            "a.shokaireason, a.rrid, a.additionaluid1, a.additionaluid2, a.memo_shokai, " +
            "a.memo_inspect, a.memo, a.paycode, a.shokaicode, a.ufirstex, a.usecondex, a.ocrdata, " +
            "a.KagoReasons, " +//試験xml用
            "a.saishinsareasons, a.henreireasons, a.comnum, a.groupnum, a.outmemo, " +
            "a.taggeddatas ," +
            "a.kagoreasons_xml ";//試験xml用



        //20201116184718 furukawa st ////////////////////////
        //申請書区別用ページ番号カウントをemptytext2に入れる

        /// <summary>
        /// 画像登録により、あらゆる画像データ1枚につき1レコードをapplicationテーブルに追加する
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="scanID"></param>
        /// <param name="groupID"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="fileName"></param>
        /// <param name="pageno">申請書区別用ページ</param>
        /// <returns></returns>
        public static bool ImageEntry(int aid, int scanID, int groupID, int cYear, int cMonth, string fileName,
            DB.Transaction tran, int pageno, string strOrigFileName)
        //public static bool ImageEntry(int aid, int scanID, int groupID, int cYear, int cMonth, string fileName, DB.Transaction tran, int pageno, string strOrigFileName)
        {
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応
            var cym = DateTimeEx.GetAdYearFromHs(cYear * 100 + cMonth) * 100 + cMonth;
            //var cym = DateTimeEx.GetAdYearFromHs(cYear) * 100 + cMonth;

            //20210603194559 furukawa st ////////////////////////
            //画像登録時にauxに基本情報だけ登録            
            if (!Application_AUX.Insert(aid, cym, scanID, groupID, fileName, strOrigFileName, pageno)) return false;
            //20210603194559 furukawa ed ////////////////////////

            using (var cmd = DB.Main.CreateCmd("INSERT INTO application " +
                "(aid, scanid, groupid, emptytext2, achargeyear, achargemonth, aimagefile, cym)" +
                "VALUES(:aid, :sid, :gid, :emptytext2, :cy, :cm, :aif, :cym)", tran))
            {
                try
                {
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                    cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = scanID;
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;

                    //20220221154440 furukawa st ////////////////////////
                    //NPGSQLバージョン更新で型を合わせる必要がある
                    
                    cmd.Parameters.Add("emptytext2", NpgsqlDbType.Integer).Value = pageno;
                    //      cmd.Parameters.Add("emptytext2", NpgsqlDbType.Integer).Value = pageno.ToString();
                    //20220221154440 furukawa ed ////////////////////////


                    cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = cYear;
                    cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = cMonth;
                    cmd.Parameters.Add("aif", NpgsqlDbType.Text).Value = fileName;
                    cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                    return cmd.TryExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }
            }
        }




        /// <summary>
        /// 画像登録により、あらゆる画像データ1枚につき1レコードをapplicationテーブルに追加する
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="scanID"></param>
        /// <param name="groupID"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool ImageEntry(int aid, int scanID, int groupID, int cYear, int cMonth, string fileName,
            DB.Transaction tran, string strOrigFileName)
        //public static bool ImageEntry(int aid, int scanID, int groupID, int cYear, int cMonth, string fileName, DB.Transaction tran)
        {
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応
            var cym = DateTimeEx.GetAdYearFromHs(cYear * 100 + cMonth) * 100 + cMonth;
            //var cym = DateTimeEx.GetAdYearFromHs(cYear) * 100 + cMonth;

       
            //20210603194447 furukawa st ////////////////////////
            //画像登録時にauxに基本情報だけ登録            
            if (!Application_AUX.Insert(aid, cym, scanID, groupID, fileName, strOrigFileName, 0)) return false;
            //20210603194447 furukawa ed ////////////////////////

            using (var cmd = DB.Main.CreateCmd("INSERT INTO application " +
                "(aid, scanid, groupid, achargeyear, achargemonth, aimagefile, cym)" +
                "VALUES(:aid, :sid, :gid, :cy, :cm, :aif, :cym)", tran))
            {
                try
                {
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                    cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = scanID;
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;
                    cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = cYear;
                    cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = cMonth;
                    cmd.Parameters.Add("aif", NpgsqlDbType.Text).Value = fileName;
                    cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                    return cmd.TryExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    return false;
                }
            }
        }


        //20201113132506 furukawa 引数に申請書区別用ページ番号カウントを追加
        /// <summary>
        /// 画像登録により、あらゆる画像データ1枚につき1レコードをapplicationテーブルに追加する
        /// ナンバリング情報をともなう
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="scanID"></param>
        /// <param name="groupID"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="fileName"></param>
        /// <param name="numbering"></param>
        /// <param name="pageno">申請書区別用ページ番号</param>
        /// <returns></returns>
        public static bool ImageEntryWithNumbering(int aid, int scanID, int groupID, int cYear,
            int cMonth, string fileName, string numbering, int pageno, string strOrigFilename)
        //public static bool ImageEntryWithNumbering(int aid, int scanID, int groupID, int cYear,
        //int cMonth, string fileName, string numbering,int pageno)
        {
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応
            var cym = DateTimeEx.GetAdYearFromHs(cYear * 100 + cMonth) * 100 + cMonth;
            //var cym = DateTimeEx.GetAdYearFromHs(cYear) * 100 + cMonth;

      
            //20210603194628 furukawa st ////////////////////////
            //画像登録時にauxに基本情報だけ登録
            if (!Application_AUX.Insert(aid, cym, scanID, groupID, fileName, strOrigFilename, pageno)) return false;
            //20210603194628 furukawa ed ////////////////////////




            using (var cmd = DB.Main.CreateCmd("INSERT INTO application " +
                "(aid, scanid, groupid, emptytext2, achargeyear, achargemonth, aimagefile, numbering, cym)" +
                "VALUES(:aid, :sid, :gid, :emptytext2, :cy, :cm, :aif, :nb, :cym)"))
            {
                try
                {
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                    cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = scanID;
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;

                    //20201113132625 furukawa st ////////////////////////
                    //申請書区別用ページ番号カウントをemptytext2に入れる

                    
                    //20220221154604 furukawa st ////////////////////////
                    //NPGSQLバージョン更新で型を合わせる必要がある
                    
                    cmd.Parameters.Add("emptytext2", NpgsqlDbType.Integer).Value = pageno;
                    //      cmd.Parameters.Add("emptytext2", NpgsqlDbType.Integer).Value = pageno.ToString();
                    //20220221154604 furukawa ed ////////////////////////


                    //20201113132625 furukawa ed ////////////////////////

                    cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = cYear;
                    cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = cMonth;
                    cmd.Parameters.Add("aif", NpgsqlDbType.Text).Value = fileName;
                    cmd.Parameters.Add("nb", NpgsqlDbType.Text).Value = numbering;
                    cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;
                }
            }
        }


   

        /// <summary>
        /// 画像登録により、あらゆる画像データ1枚につき1レコードをapplicationテーブルに追加する
        /// ナンバリング情報をともなう
        /// </summary>
        /// <param name="aid"></param>
        /// <param name="scanID"></param>
        /// <param name="groupID"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <param name="fileName"></param>
        /// <param name="numbering"></param>
        /// <returns></returns>
        public static bool ImageEntryWithNumbering(int aid, int scanID, int groupID, int cYear, int cMonth, string fileName,
            string numbering, string strOrigFileName)
        //public static bool ImageEntryWithNumbering(int aid, int scanID, int groupID, int cYear, int cMonth, string fileName, string numbering)
        {
            //20190424191119_2019/04/07 GetAdYearFromHS変更対応
            var cym = DateTimeEx.GetAdYearFromHs(cYear * 100 + cMonth) * 100 + cMonth;
            //var cym = DateTimeEx.GetAdYearFromHs(cYear) * 100 + cMonth;

      
            //20210603194652 furukawa st ////////////////////////
            //画像登録時にauxに基本情報だけ登録
            if (!Application_AUX.Insert(aid, cym, scanID, groupID, fileName, strOrigFileName, 0)) return false;
            //20210603194652 furukawa ed ////////////////////////



            using (var cmd = DB.Main.CreateCmd("INSERT INTO application " +
                "(aid, scanid, groupid, achargeyear, achargemonth, aimagefile, numbering, cym)" +
                "VALUES(:aid, :sid, :gid, :cy, :cm, :aif, :nb, :cym)"))
            {
                try
                {
                    cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                    cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = scanID;
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;
                    cmd.Parameters.Add("cy", NpgsqlDbType.Integer).Value = cYear;
                    cmd.Parameters.Add("cm", NpgsqlDbType.Integer).Value = cMonth;
                    cmd.Parameters.Add("aif", NpgsqlDbType.Text).Value = fileName;
                    cmd.Parameters.Add("nb", NpgsqlDbType.Text).Value = numbering;
                    cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                    return cmd.TryExecuteNonQuery();
                }
                catch
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// aIdで一枚の申請書を抽出
        /// </summary>
        /// <param name="aid"></param>
        /// <returns></returns>
        public static App GetApp(int aid)
        {
            using (var cmd = DB.Main.CreateCmd(
                "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.aid=:aid"))
            {
                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = aid;
                var res = cmd.TryExecuteReaderList();
                if (res == null || res.Count == 0) return null;

                return CreateAppFromRecord(res[0]);
            }
        }







        //20190313132638 furukawa st ////////////////////////
        //保険者によってソート順が違うため引数追加


        /// <summary>
        /// 請求年月6桁で複数の申請書を抽出
        /// </summary>
        /// <param name="cym">処理年月</param>
        /// <param name="insID">保険者(デフォルトは0)</param>
        /// <returns></returns>
        public static List<App> GetApps(int cym, InsurerID insID = 0)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE cym=:cym " +
                "ORDER BY ";

            switch (insID)
            {
                case InsurerID.NAGOYA_KOUWAN:
                    sql += "a.paycode,a.afamily";
                    break;

                default:
                    sql += "a.aid";
                    break;
            }


            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
            });
        }


        /*
        /// <summary>
        /// 請求年月6桁で複数の申請書を抽出
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static List<App> GetApps(int cym)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE cym=:cym " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
            });
        }*/

        //20190313132638 furukawa ed ////////////////////////





        /// <summary>
        /// WHERE句を指定して複数の申請書を抽出
        /// </summary>
        /// <param name="year"></param>
        /// <param name="where">where句 ※WHEREを含み、各カラムにaをつける</param>
        /// <returns></returns>
        public static List<App> GetAppsWithWhere(string where)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                where + " " +
                "ORDER BY a.aid limit 10";

            return GetAppFromSql(sql, null);
        }

        /// <summary>
        /// WHERE句を指定して複数の申請書を抽出
        /// </summary>
        /// <param name="year"></param>
        /// <param name="where">where句 ※WHEREを含み、各カラムにaをつける</param>
        /// <returns></returns>
        public static List<App> GetAppsWithLimit(string where, int limit, bool asc)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                $"{where} ORDER BY a.aid {(asc ? "" : "DESC ")}" +
                $"LIMIT {limit}";

            var l = GetAppFromSql(sql, null);
            l.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            return l;
        }

        /// <summary>
        /// スキャンIDで複数の申請書を抽出
        /// </summary>
        /// <param name="scanID"></param>
        /// <returns></returns>
        public static List<App> GetAppsSID(int scanID)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.scanid=:scanid " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
                {
                    cmd.Parameters.Add("scanid", NpgsqlDbType.Integer).Value = scanID;
                });
        }

        /// <summary>
        /// 被保番で過去半年分と次月のApp一覧を取得します
        /// </summary>
        /// <param name="hnum"></param>
        /// <param name="cYear"></param>
        /// <param name="cMonth"></param>
        /// <returns></returns>
        public static List<App> GetAppsFamily(string hnum, int cym)
        {
            int sym = DateTimeEx.Int6YmAddMonth(cym, -6);
            int eym = DateTimeEx.Int6YmAddMonth(cym, 1);

            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.hnum=:hnum ";

            sql += "AND a.cym BETWEEN :sym AND :eym " +
                "ORDER BY aid";

            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = hnum;
                cmd.Parameters.Add("sym", NpgsqlDbType.Integer).Value = sym;
                cmd.Parameters.Add("eym", NpgsqlDbType.Integer).Value = eym;
            });
        }

        /// <summary>
        /// グループIDで複数の申請書を抽出
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static List<App> GetAppsGID(int groupID)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.groupid=:gid " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
                {
                    cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;
                });
        }

        /// <summary>
        /// SQLが吐き出したobjectを元に、Appを作成します
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static App CreateAppFromRecord(object[] obj)
        {
            int i = 0;
            var a = new App();
            a.Aid = (int)obj[i++];
            a.ScanID = (int)obj[i++];
            a.GroupID = (int)obj[i++];
            a.MediYear = (int)obj[i++];
            a.MediMonth = (int)obj[i++];
            a.InsNum = (string)obj[i++];
            a.HihoNum = (string)obj[i++];
            a.HihoPref = (int)obj[i++];
            a.HihoType = (int)obj[i++];
            a.HihoName = (string)obj[i++];
            a.HihoZip = (string)obj[i++];
            a.HihoAdd = (string)obj[i++];
            a.PersonName = (string)obj[i++];
            a.Sex = (int)obj[i++];
            a.Birthday = (DateTime)obj[i++];
            a.ClinicNum = (string)obj[i++];
            a.Single = (int)obj[i++];
            a.Family = (int)obj[i++];
            a.Ratio = (int)obj[i++];
            a.PublcExpense = (string)obj[i++];
            a.ImageFile = (string)obj[i++];
            a.ChargeYear = (int)obj[i++];
            a.ChargeMonth = (int)obj[i++];
            a.FushoName1 = (string)obj[i++];
            a.FushoDate1 = (DateTime)obj[i++];
            a.FushoFirstDate1 = (DateTime)obj[i++];
            a.FushoStartDate1 = (DateTime)obj[i++];
            a.FushoFinishDate1 = (DateTime)obj[i++];
            a.FushoDays1 = (int)obj[i++];
            a.FushoCourse1 = (int)obj[i++];
            a.FushoName2 = (string)obj[i++];
            a.FushoDate2 = (DateTime)obj[i++];
            a.FushoFirstDate2 = (DateTime)obj[i++];
            a.FushoStartDate2 = (DateTime)obj[i++];
            a.FushoFinishDate2 = (DateTime)obj[i++];
            a.FushoDays2 = (int)obj[i++];
            a.FushoCourse2 = (int)obj[i++];
            a.FushoName3 = (string)obj[i++];
            a.FushoDate3 = (DateTime)obj[i++];
            a.FushoFirstDate3 = (DateTime)obj[i++];
            a.FushoStartDate3 = (DateTime)obj[i++];
            a.FushoFinishDate3 = (DateTime)obj[i++];
            a.FushoDays3 = (int)obj[i++];
            a.FushoCourse3 = (int)obj[i++];
            a.FushoName4 = (string)obj[i++];
            a.FushoDate4 = (DateTime)obj[i++];
            a.FushoFirstDate4 = (DateTime)obj[i++];
            a.FushoStartDate4 = (DateTime)obj[i++];
            a.FushoFinishDate4 = (DateTime)obj[i++];
            a.FushoDays4 = (int)obj[i++];
            a.FushoCourse4 = (int)obj[i++];
            a.FushoName5 = (string)obj[i++];
            a.FushoDate5 = (DateTime)obj[i++];
            a.FushoFirstDate5 = (DateTime)obj[i++];
            a.FushoStartDate5 = (DateTime)obj[i++];
            a.FushoFinishDate5 = (DateTime)obj[i++];
            a.FushoDays5 = (int)obj[i++];
            a.FushoCourse5 = (int)obj[i++];
            a.NewContType = (NEW_CONT)(int)obj[i++];
            a.Distance = (int)obj[i++];
            a.VisitTimes = (int)obj[i++];
            a.VisitFee = (int)obj[i++];
            a.VisitAdd = (int)obj[i++];

            a.DrNum = (string)obj[i++];
            a.ClinicZip = (string)obj[i++];
            a.ClinicAdd = (string)obj[i++];
            a.ClinicName = (string)obj[i++];
            a.ClinicTel = (string)obj[i++];
            a.DrName = (string)obj[i++];
            a.DrKana = (string)obj[i++];
            a.AccountType = (int)obj[i++];
            a.BankName = (string)obj[i++];
            a.BankType = (int)obj[i++];
            a.BankBranch = (string)obj[i++];
            a.BankBranchType = (int)obj[i++];
            a.AccountName = (string)obj[i++];
            a.AccountKana = (string)obj[i++];
            a.AccountNumber = (string)obj[i++];
            a.Total = (int)obj[i++];
            a.Partial = (int)obj[i++];
            a.Charge = (int)obj[i++];
            a.CountedDays = (int)obj[i++];
            a.Numbering = (string)obj[i++];
            a.AppType = (APP_TYPE)(int)obj[i++];
            a.Ufirst = (int)obj[i++];
            a.Usecond = (int)obj[i++];
            a.Uinquiry = (int)obj[i++];
            a.Bui = (int)obj[i++];
            a.CYM = (int)obj[i++];
            a.YM = (int)obj[i++];
            a.StatusFlags = (StatusFlag)(int)obj[i++];
            a.ShokaiReason = (ShokaiReason)(int)obj[i++];
            a.RrID = (int)obj[i++];

            a.AdditionalUid1 = (int)obj[i++];
            a.AdditionalUid2 = (int)obj[i++];
            a.MemoShokai = (string)obj[i++];
            a.MemoInspect = (string)obj[i++];
            a.Memo = (string)obj[i++];
            a.PayCode = (string)obj[i++];
            a.ShokaiCode = (string)obj[i++];
            a.UfirstEx = (int)obj[i++];
            a.UsecondEx = (int)obj[i++];
            a.OcrData = (string)obj[i++];

            a.KagoReasons = (KagoReasons)(int)obj[i++];
            //i++;
            a.SaishinsaReasons = (SaishinsaReasons)(int)obj[i++];
            a.HenreiReasons = (HenreiReasons)(int)obj[i++];
            a.ComNum = (string)obj[i++];
            a.GroupNum = (string)obj[i++];
            a.OutMemo = (string)obj[i++];
            a.TaggedDatas = new TaggedDatas((string)obj[i++]);
            a.strKagoReasonsXML = obj[i++].ToString();

            return a;
        }


        //20200524105129 furukawa st ////////////////////////
        //保険者個別機能用フラグ追加

        public enum UPDATE_TYPE
        {
            Null = 0,
            FirstInput = 1, SecondInput = 2, Both = 3,
            FirstInputEx = 11, SecondInputEx = 12, BothEx = 13,
            Ikatotu = 14,
            Sp1 = 20, Sp2 = 21,               //20200524105129 furukawa 保険者個別機能用フラグ追加

            //20200607172245 furukawa st //宮城県国保　リスト抽出区分追加
            Extra1 = 31, Extra2 = 32,
            ListInput1 = 41, ListInput2 = 42,
            //20200607172245 furukawa ed ////////////////////////

            GaibuFirst = 51, GaibuSecond = 52,  //20221216 ito プロジェクトK入力対応
        }

        //20190510170659 furukawa st //医科突合フラグ更新　追加
        //public enum UPDATE_TYPE
        //{
        //    Null = 0, FirstInput = 1, SecondInput = 2, Both = 3,
        //    FirstInputEx = 11, SecondInputEx = 12, BothEx = 13, Ikatotu = 14,
        //}

        //public enum UPDATE_TYPE
        //{
        //    Null = 0, FirstInput = 1, SecondInput = 2, Both = 3,
        //    FirstInputEx = 11, SecondInputEx = 12, BothEx = 13
        //}
        //20190510170659 furukawa ed ////////////////////////
        //20200524105129 furukawa ed ////////////////////////



        /// <summary>
        /// 申請書レコードの更新（現在の更新タイプから、ステータスフラグを更新する）
        /// </summary>
        /// <param name="updateUserID"></param>
        /// <param name="updateType">現在の更新タイプ</param>
        /// <returns></returns>
        public bool Update(int updateUserID, UPDATE_TYPE updateType, DB.Transaction tran = null)
        {
            if (updateType == UPDATE_TYPE.FirstInput)
            {
                Ufirst = updateUserID;
                StatusFlagSet(StatusFlag.入力済);
                StatusFlagRemove(StatusFlag.ベリファイ済);
            }
            else if (updateType == UPDATE_TYPE.SecondInput)
            {
                Usecond = updateUserID;
                StatusFlagSet(StatusFlag.ベリファイ済);
            }
            else if (updateType == UPDATE_TYPE.Both)
            {
                Ufirst = updateUserID;
                Usecond = updateUserID;
                StatusFlagSet(StatusFlag.入力済 | StatusFlag.ベリファイ済);
            }
            else if (updateType == UPDATE_TYPE.FirstInputEx)
            {
                UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.拡張入力済);
                StatusFlagRemove(StatusFlag.拡張ベリ済);
            }
            else if (updateType == UPDATE_TYPE.SecondInputEx)
            {
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.拡張ベリ済);
            }
            else if (updateType == UPDATE_TYPE.BothEx)
            {
                UfirstEx = updateUserID;
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.拡張入力済 | StatusFlag.拡張ベリ済);
            }

            //20190510170913 furukawa st //医科突合フラグ更新処理
            else if (updateType == UPDATE_TYPE.Ikatotu)
            {
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.処理4);
            }
            //20190510170913 furukawa ed ////////////////////////

            //20200524105247 furukawa st //保険者個別機能用ステータス
            else if (updateType == UPDATE_TYPE.Sp1)
            {
                //20200617154536 furukawa st //独自処理のユーザを追加ユーザとして登録
                AdditionalUid1 = updateUserID;
                //UfirstEx = updateUserID;
                //20200617154536 furukawa ed ////////////////////////

                StatusFlagSet(StatusFlag.独自処理1);
                StatusFlagRemove(StatusFlag.独自処理2);
            }
            else if (updateType == UPDATE_TYPE.Sp2)
            {
                //20200617154617 furukawa st //独自処理のユーザを追加ユーザとして登録
                AdditionalUid2 = updateUserID;
                //UsecondEx = updateUserID;
                //20200617154617 furukawa ed ////////////////////////

                StatusFlagSet(StatusFlag.独自処理2);
            }
            //20200524105247 furukawa ed ////////////////////////

            //20200608164226 furukawa st //宮城県国保　患者照会入力ステータス
            //患者照会
            else if (updateType == UPDATE_TYPE.ListInput1)
            {
                TaggedDatas.GeneralString2 = updateUserID.ToString();
                //UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.独自処理1);
                StatusFlagRemove(StatusFlag.独自処理2);
            }
            else if (updateType == UPDATE_TYPE.ListInput2)
            {
                TaggedDatas.GeneralString3 = updateUserID.ToString();
                //UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.独自処理2);
            }

            //施術所照会
            else if (updateType == UPDATE_TYPE.Extra1)
            {
                TaggedDatas.GeneralString4 = updateUserID.ToString();
                //UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.追加入力済);
                StatusFlagRemove(StatusFlag.追加入力済);
            }
            else if (updateType == UPDATE_TYPE.Extra2)
            {
                TaggedDatas.GeneralString5 = updateUserID.ToString();
                //UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.追加入力済);
            }
            //20200608164226 furukawa ed ////////////////////////

            //20221216 ito st プロジェクトK入力
            else if (updateType == UPDATE_TYPE.GaibuFirst)
            {
                Ufirst = updateUserID;  //南さんより、1回目入力者の名前は外部1の人で良い
                StatusFlagSet(StatusFlag.外部1回目済);
            }
            else if (updateType == UPDATE_TYPE.GaibuSecond)
            {
                StatusFlagSet(StatusFlag.入力済);// .外部2回目済);
            }
            //20221216 ito end


            var sql = "UPDATE application SET " +
                "scanid=:scanid, groupid=:groupid, ayear=:ayear, amonth=:amonth, inum=:inum, hnum=:hnum, hpref=:hpref, " +
                "htype=:htype, hname=:hname, hzip=:hzip, haddress=:haddress, " +
                "pname=:pname, psex=:psex, pbirthday=:pbirthday, sid=:sid, asingle=:asingle, " +
                "afamily=:afamily, aratio=:aratio, publcexpense=:publcexpense, aimagefile=:aimagefile, " +
                "achargeyear=:achargeyear, achargemonth=:achargemonth, " +
                "iname1=:iname1, idate1=:idate1, ifirstdate1=:ifirstdate1, istartdate1=:istartdate1, " +
                "ifinishdate1=:ifinishdate1, idays1=:idays1, icourse1=:icourse1, " +
                "iname2=:iname2, idate2=:idate2, ifirstdate2=:ifirstdate2, istartdate2=:istartdate2, " +
                "ifinishdate2=:ifinishdate2, idays2=:idays2, icourse2=:icourse2, " +
                "iname3=:iname3, idate3=:idate3, ifirstdate3=:ifirstdate3, istartdate3=:istartdate3, " +
                "ifinishdate3=:ifinishdate3, idays3=:idays3, icourse3=:icourse3, " +
                "iname4=:iname4, idate4=:idate4, ifirstdate4=:ifirstdate4, istartdate4=:istartdate4, " +
                "ifinishdate4=:ifinishdate4, idays4=:idays4, icourse4=:icourse4, " +
                "iname5=:iname5, idate5=:idate5, ifirstdate5=:ifirstdate5, istartdate5=:istartdate5, " +
                "ifinishdate5=:ifinishdate5, idays5=:idays5, icourse5=:icourse5, " +
                "fchargetype=:fchargetype, fdistance=:fdistance, fvisittimes=:fvisittimes, fvisitfee=:fvisitfee, " +
                "fvisitadd=:fvisitadd, sregnumber=:sregnumber, szip=:szip, saddress=:saddress, sname=:sname, " +
                "stel=:stel, sdoctor=:sdoctor, skana=:skana, " +
                "bacctype=:bacctype, bname=:bname, btype=:btype, bbranch=:bbranch, " +
                "bbranchtype=:bbranchtype, baccname=:baccname, bkana=:bkana, baccnumber=:baccnumber, " +
                "atotal=:atotal, apartial=:apartial, acharge=:acharge, " +
                "acounteddays=:acounteddays, numbering=:numbering, aapptype=:aapptype, " +
                "ufirst=:ufirst, usecond=:usecond, uinquiry=:uinquiry, bui=:bui, " +
                "cym=:cym, ym=:ym, statusflags=:statusflags, shokaireason=:shokaireason, " +
                "rrid=:rrid, additionaluid1=:additionaluid1, " +
                "additionaluid2=:additionaluid2, memo_shokai=:memoshokai, memo_inspect=:memoinspect, " +
                "memo=:memo, paycode=:paycode, shokaicode=:shokaicode, ufirstex=:ufirstex, usecondex=:usecondex, " +
                "KagoReasons=:KagoReasons, " +//現行
                "saishinsareasons=:saishinsareasons, henreireasons=:henreireasons, " +
                "comnum=:comnum, groupnum=:groupnum, outmemo=:outmemo, taggeddatas=:taggeddatas " +
                ",KagoReasons_xml=:KagoReasons_xml " +//20201011145640 furukawa 過誤フラグ追加に伴ってxml列追加

                "WHERE aid=:aid;";

            using (var cmd = tran == null ? DB.Main.CreateCmd(sql) : DB.Main.CreateCmd(sql, tran))
            {
                var c = this;
                cmd.Parameters.Add("scanid", NpgsqlDbType.Integer).Value = c.ScanID;
                cmd.Parameters.Add("groupid", NpgsqlDbType.Integer).Value = c.GroupID;
                cmd.Parameters.Add("ayear", NpgsqlDbType.Integer).Value = c.MediYear;
                cmd.Parameters.Add("amonth", NpgsqlDbType.Integer).Value = c.MediMonth;
                cmd.Parameters.Add("inum", NpgsqlDbType.Text).Value = c.InsNum;
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = c.HihoNum;
                cmd.Parameters.Add("hpref", NpgsqlDbType.Integer).Value = c.HihoPref;
                cmd.Parameters.Add("htype", NpgsqlDbType.Integer).Value = c.HihoType;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = c.HihoName;
                cmd.Parameters.Add("hzip", NpgsqlDbType.Text).Value = c.HihoZip;
                cmd.Parameters.Add("haddress", NpgsqlDbType.Text).Value = c.HihoAdd;

                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = c.PersonName;
                cmd.Parameters.Add("psex", NpgsqlDbType.Integer).Value = c.Sex;
                cmd.Parameters.Add("pbirthday", NpgsqlDbType.Date).Value = c.Birthday;

                cmd.Parameters.Add("sid", NpgsqlDbType.Text).Value = c.ClinicNum;
                cmd.Parameters.Add("asingle", NpgsqlDbType.Integer).Value = c.Single;
                cmd.Parameters.Add("afamily", NpgsqlDbType.Integer).Value = c.Family;
                cmd.Parameters.Add("aratio", NpgsqlDbType.Integer).Value = c.Ratio;
                cmd.Parameters.Add("publcexpense", NpgsqlDbType.Text).Value = c.PublcExpense;
                cmd.Parameters.Add("aimagefile", NpgsqlDbType.Text).Value = c.ImageFile;
                cmd.Parameters.Add("achargeyear", NpgsqlDbType.Integer).Value = c.ChargeYear;
                cmd.Parameters.Add("achargemonth", NpgsqlDbType.Integer).Value = c.ChargeMonth;

                cmd.Parameters.Add("iname1", NpgsqlDbType.Text).Value = c.FushoName1;
                cmd.Parameters.Add("idate1", NpgsqlDbType.Date).Value = c.FushoDate1;
                cmd.Parameters.Add("ifirstdate1", NpgsqlDbType.Date).Value = c.FushoFirstDate1;
                cmd.Parameters.Add("istartdate1", NpgsqlDbType.Date).Value = c.FushoStartDate1;
                cmd.Parameters.Add("ifinishdate1", NpgsqlDbType.Date).Value = c.FushoFinishDate1;
                cmd.Parameters.Add("idays1", NpgsqlDbType.Integer).Value = c.FushoDays1;
                cmd.Parameters.Add("icourse1", NpgsqlDbType.Integer).Value = c.FushoCourse1;
                cmd.Parameters.Add("iname2", NpgsqlDbType.Text).Value = c.FushoName2;
                cmd.Parameters.Add("idate2", NpgsqlDbType.Date).Value = c.FushoDate2;
                cmd.Parameters.Add("ifirstdate2", NpgsqlDbType.Date).Value = c.FushoFirstDate2;
                cmd.Parameters.Add("istartdate2", NpgsqlDbType.Date).Value = c.FushoStartDate2;
                cmd.Parameters.Add("ifinishdate2", NpgsqlDbType.Date).Value = c.FushoFinishDate2;
                cmd.Parameters.Add("idays2", NpgsqlDbType.Integer).Value = c.FushoDays2;
                cmd.Parameters.Add("icourse2", NpgsqlDbType.Integer).Value = c.FushoCourse2;
                cmd.Parameters.Add("iname3", NpgsqlDbType.Text).Value = c.FushoName3;
                cmd.Parameters.Add("idate3", NpgsqlDbType.Date).Value = c.FushoDate3;
                cmd.Parameters.Add("ifirstdate3", NpgsqlDbType.Date).Value = c.FushoFirstDate3;
                cmd.Parameters.Add("istartdate3", NpgsqlDbType.Date).Value = c.FushoStartDate3;
                cmd.Parameters.Add("ifinishdate3", NpgsqlDbType.Date).Value = c.FushoFinishDate3;
                cmd.Parameters.Add("idays3", NpgsqlDbType.Integer).Value = c.FushoDays3;
                cmd.Parameters.Add("icourse3", NpgsqlDbType.Integer).Value = c.FushoCourse3;
                cmd.Parameters.Add("iname4", NpgsqlDbType.Text).Value = c.FushoName4;
                cmd.Parameters.Add("idate4", NpgsqlDbType.Date).Value = c.FushoDate4;
                cmd.Parameters.Add("ifirstdate4", NpgsqlDbType.Date).Value = c.FushoFirstDate4;
                cmd.Parameters.Add("istartdate4", NpgsqlDbType.Date).Value = c.FushoStartDate4;
                cmd.Parameters.Add("ifinishdate4", NpgsqlDbType.Date).Value = c.FushoFinishDate4;
                cmd.Parameters.Add("idays4", NpgsqlDbType.Integer).Value = c.FushoDays4;
                cmd.Parameters.Add("icourse4", NpgsqlDbType.Integer).Value = c.FushoCourse4;
                cmd.Parameters.Add("iname5", NpgsqlDbType.Text).Value = c.FushoName5;
                cmd.Parameters.Add("idate5", NpgsqlDbType.Date).Value = c.FushoDate5;
                cmd.Parameters.Add("ifirstdate5", NpgsqlDbType.Date).Value = c.FushoFirstDate5;
                cmd.Parameters.Add("istartdate5", NpgsqlDbType.Date).Value = c.FushoStartDate5;
                cmd.Parameters.Add("ifinishdate5", NpgsqlDbType.Date).Value = c.FushoFinishDate5;
                cmd.Parameters.Add("idays5", NpgsqlDbType.Integer).Value = c.FushoDays5;
                cmd.Parameters.Add("icourse5", NpgsqlDbType.Integer).Value = c.FushoCourse5;


                //20210615104723 furukawa st //新規継続は、負傷名1が治癒した場合、負傷名2基準となり、入力していない場合は判定できないのでやめる
                //      20210521141552 furukawa st //新規継続自動判定
                //      if (c.MediYear > 0 && c.MediYear<=64 && c.MediMonth > 0)//申請書のみ 新規継続判定
                //      {
                //          //診療年月
                //          int intmediymAD = DateTimeEx.GetAdYearFromHs(c.MediYear * 100 + c.MediMonth) * 100 + c.MediMonth;

                //          //比較のため診療年月の1日にする
                //          DateTime dtmediAD = DateTimeEx.ToDateTime(intmediymAD * 100 + 1);

                //          //負傷1の初検日と診療年月を比較、年月とも同じ場合、新規とする
                //          if (c.FushoFirstDate1.Year == dtmediAD.Year && c.FushoFirstDate1.Month == dtmediAD.Month) c.NewContType = NEW_CONT.新規;
                //          else c.NewContType = NEW_CONT.継続;
                //      }
                //      else
                //      {
                //          c.NewContType = NEW_CONT.Null;
                //      }
                //      20210521141552 furukawa ed ////////////////////////
                //20210615104723 furukawa ed ////////////////////////

                //20220214134655 furukawa st ////////////////////////
                //NPGSQL2.2.7から4.0.12への変更により型変換が厳密に
                cmd.Parameters.Add("fchargetype", NpgsqlDbType.Integer).Value = (int)c.NewContType;
                //cmd.Parameters.Add("fchargetype", NpgsqlDbType.Integer).Value = c.NewContType;
                //20220214134655 furukawa ed ////////////////////////

                cmd.Parameters.Add("fdistance", NpgsqlDbType.Integer).Value = c.Distance;
                cmd.Parameters.Add("fvisittimes", NpgsqlDbType.Integer).Value = c.VisitTimes;
                cmd.Parameters.Add("fvisitfee", NpgsqlDbType.Integer).Value = c.VisitFee;
                cmd.Parameters.Add("fvisitadd", NpgsqlDbType.Integer).Value = c.VisitAdd;
                cmd.Parameters.Add("sregnumber", NpgsqlDbType.Text).Value = c.DrNum;
                cmd.Parameters.Add("szip", NpgsqlDbType.Text).Value = c.ClinicZip;
                cmd.Parameters.Add("saddress", NpgsqlDbType.Text).Value = c.ClinicAdd;
                cmd.Parameters.Add("sname", NpgsqlDbType.Text).Value = c.ClinicName;
                cmd.Parameters.Add("stel", NpgsqlDbType.Text).Value = c.ClinicTel;
                cmd.Parameters.Add("sdoctor", NpgsqlDbType.Text).Value = c.DrName;
                cmd.Parameters.Add("skana", NpgsqlDbType.Text).Value = c.DrKana;
                cmd.Parameters.Add("bacctype", NpgsqlDbType.Integer).Value = c.AccountType;
                cmd.Parameters.Add("bname", NpgsqlDbType.Text).Value = c.BankName;
                cmd.Parameters.Add("btype", NpgsqlDbType.Integer).Value = c.BankType;
                cmd.Parameters.Add("bbranch", NpgsqlDbType.Text).Value = c.BankBranch;
                cmd.Parameters.Add("bbranchtype", NpgsqlDbType.Integer).Value = c.BankBranchType;
                cmd.Parameters.Add("baccname", NpgsqlDbType.Text).Value = c.AccountName;
                cmd.Parameters.Add("bkana", NpgsqlDbType.Text).Value = c.AccountKana;
                cmd.Parameters.Add("baccnumber", NpgsqlDbType.Text).Value = c.AccountNumber;

                cmd.Parameters.Add("atotal", NpgsqlDbType.Integer).Value = c.Total;
                cmd.Parameters.Add("apartial", NpgsqlDbType.Integer).Value = c.Partial;
                cmd.Parameters.Add("acharge", NpgsqlDbType.Integer).Value = c.Charge;
                cmd.Parameters.Add("acounteddays", NpgsqlDbType.Integer).Value = c.CountedDays;
                cmd.Parameters.Add("numbering", NpgsqlDbType.Text).Value = c.Numbering;
                cmd.Parameters.Add("aapptype", NpgsqlDbType.Integer).Value = (int)c.AppType;
                cmd.Parameters.Add("ufirst", NpgsqlDbType.Integer).Value = c.Ufirst;
                cmd.Parameters.Add("usecond", NpgsqlDbType.Integer).Value = c.Usecond;
                cmd.Parameters.Add("uinquiry", NpgsqlDbType.Integer).Value = c.Uinquiry;

                cmd.Parameters.Add("bui", NpgsqlDbType.Integer).Value = c.Bui;

                //20190425190415 furukawa st //令和対応
                int cym = DateTimeEx.GetAdYearFromHs(ChargeYear * 100 + ChargeMonth) * 100 + ChargeMonth;
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym; //(ChargeYear + 1988) * 100 + ChargeMonth;
                int ym = DateTimeEx.GetAdYearFromHs(MediYear * 100 + MediMonth) * 100 + MediMonth;

                //20190716150824 furukawa st //1未満の場合はayearをそのまま、それ以外は年月を入れる
                cmd.Parameters.Add("ym", NpgsqlDbType.Integer).Value = MediYear < 1 ? MediYear : ym;
                //          cmd.Parameters.Add("ym", NpgsqlDbType.Integer).Value = ym;// MediYear < 1 ? MediYear : (MediYear + 1988) * 100 + MediMonth;
                //20190716150824 furukawa ed ////////////////////////
                //           cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = (ChargeYear + 1988) * 100 + ChargeMonth;
                //           cmd.Parameters.Add("ym", NpgsqlDbType.Integer).Value = MediYear < 1 ? MediYear : (MediYear + 1988) * 100 + MediMonth;
                //20190425190415 furukawa ed ////////////////////////

                cmd.Parameters.Add("statusflags", NpgsqlDbType.Integer).Value = (int)c.StatusFlags;
                cmd.Parameters.Add("shokaireason", NpgsqlDbType.Integer).Value = (int)c.ShokaiReason;
                cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = c.RrID;

                cmd.Parameters.Add("additionaluid1", NpgsqlDbType.Integer).Value = c.AdditionalUid1;
                cmd.Parameters.Add("additionaluid2", NpgsqlDbType.Integer).Value = c.AdditionalUid2;
                cmd.Parameters.Add("memoshokai", NpgsqlDbType.Text).Value = c.MemoShokai;
                cmd.Parameters.Add("memoinspect", NpgsqlDbType.Text).Value = c.MemoInspect;
                cmd.Parameters.Add("memo", NpgsqlDbType.Text).Value = c.Memo;
                cmd.Parameters.Add("paycode", NpgsqlDbType.Text).Value = c.PayCode;
                cmd.Parameters.Add("shokaicode", NpgsqlDbType.Text).Value = c.ShokaiCode;
                cmd.Parameters.Add("ufirstex", NpgsqlDbType.Integer).Value = c.UfirstEx;
                cmd.Parameters.Add("usecondex", NpgsqlDbType.Integer).Value = c.UsecondEx;
                cmd.Parameters.Add("KagoReasons", NpgsqlDbType.Integer).Value = (int)c.KagoReasons;//従来の過誤理由
                cmd.Parameters.Add("saishinsareasons", NpgsqlDbType.Integer).Value = (int)c.SaishinsaReasons;
                cmd.Parameters.Add("henreireasons", NpgsqlDbType.Integer).Value = (int)c.HenreiReasons;
                cmd.Parameters.Add("comnum", NpgsqlDbType.Text).Value = c.ComNum;
                cmd.Parameters.Add("groupnum", NpgsqlDbType.Text).Value = c.GroupNum;
                cmd.Parameters.Add("outmemo", NpgsqlDbType.Text).Value = c.OutMemo;
                cmd.Parameters.Add("taggeddatas", NpgsqlDbType.Text).Value = TaggedDatas.CreateDbStr();

                //20201011150006 furukawa st //過誤フラグ追加に伴ってxml列追加
                cmd.Parameters.Add("KagoReasons_xml", NpgsqlDbType.Xml).Value = c.strKagoReasonsXML == string.Empty ? string.Empty : c.strKagoReasonsXML;
                //cmd.Parameters.Add("KagoReasons_xml", NpgsqlDbType.Xml).Value = c.strKagoReasonsXML == string.Empty ? null : c.strKagoReasonsXML;
                //20201011150006 furukawa ed ////////////////////////

                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = c.Aid;

                return cmd.TryExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// 20210629165655 furukawa 続紙と申請書の関連付け
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool CreateRelation(int cym)
        {
            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            try
            {
                //20220604115058 furukawa st //グループIDが飛んでいる対策のストアドに変更
                if (!CreateStoredProcedure(wf, "ParentRelation3.sql"))
                //      20210708182355 furukawa st //ストアド用sqlファイルを引数に
                //      if (!CreateStoredProcedure(wf, "ParentRelation2.sql"))
                //      //        if (!CreateStoredProcedure(wf))
                //      20210708182355 furukawa ed ////////////////////////
                //20220604115058 furukawa ed ////////////////////////
                {
                        System.Windows.Forms.MessageBox.Show($"ストアド作成失敗に失敗しました。処理を中止します");
                    return false;
                }
                if (!UpdateZokushi(cym,wf)) 
                {
                    System.Windows.Forms.MessageBox.Show($"続紙と申請書の関連付けに失敗しました。処理を中止します");
                    return false;
                }
                //MessageBox.Show("終了しました");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }
        //20210629165655 furukawa ed ////////////////////////


        //20220829_1 ito st ////////////////////////
        public static bool CreateRelationQuary(int cym)
        {

            WaitForm wf = new WaitForm();
            wf.ShowDialogOtherTask();

            string sql = "WITH tmp as (" +
                $"SELECT aid, aapptype," +
                $" CASE " +
                $"  WHEN aapptype >= 0 THEN 0 " +
                $"  WHEN aapptype = -4 THEN - 1 " +
                $"  WHEN aapptype = -7 THEN - 1 " +
                $"  WHEN aapptype = -8 THEN - 1 " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 1) over(order by aid) > 0 THEN LAG(aid, 1) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 2) over(order by aid) > 0 THEN LAG(aid, 2) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 3) over(order by aid) > 0 THEN LAG(aid, 3) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 4) over(order by aid) > 0 THEN LAG(aid, 4) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 5) over(order by aid) > 0 THEN LAG(aid, 5) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 6) over(order by aid) > 0 THEN LAG(aid, 6) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 7) over(order by aid) > 0 THEN LAG(aid, 7) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 8) over(order by aid) > 0 THEN LAG(aid, 8) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 9) over(order by aid) > 0 THEN LAG(aid, 9) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 10) over(order by aid) > 0 THEN LAG(aid, 10) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 11) over(order by aid) > 0 THEN LAG(aid, 11) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 12) over(order by aid) > 0 THEN LAG(aid, 12) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 13) over(order by aid) > 0 THEN LAG(aid, 13) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 14) over(order by aid) > 0 THEN LAG(aid, 14) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 15) over(order by aid) > 0 THEN LAG(aid, 15) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 16) over(order by aid) > 0 THEN LAG(aid, 16) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 17) over(order by aid) > 0 THEN LAG(aid, 17) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 18) over(order by aid) > 0 THEN LAG(aid, 18) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 19) over(order by aid) > 0 THEN LAG(aid, 19) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 20) over(order by aid) > 0 THEN LAG(aid, 20) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 21) over(order by aid) > 0 THEN LAG(aid, 21) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 22) over(order by aid) > 0 THEN LAG(aid, 22) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 23) over(order by aid) > 0 THEN LAG(aid, 23) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 24) over(order by aid) > 0 THEN LAG(aid, 24) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 25) over(order by aid) > 0 THEN LAG(aid, 25) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 26) over(order by aid) > 0 THEN LAG(aid, 26) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 27) over(order by aid) > 0 THEN LAG(aid, 27) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 28) over(order by aid) > 0 THEN LAG(aid, 28) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 29) over(order by aid) > 0 THEN LAG(aid, 29) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 30) over(order by aid) > 0 THEN LAG(aid, 30) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 31) over(order by aid) > 0 THEN LAG(aid, 31) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 32) over(order by aid) > 0 THEN LAG(aid, 32) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 33) over(order by aid) > 0 THEN LAG(aid, 33) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 34) over(order by aid) > 0 THEN LAG(aid, 34) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 35) over(order by aid) > 0 THEN LAG(aid, 35) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 36) over(order by aid) > 0 THEN LAG(aid, 36) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 37) over(order by aid) > 0 THEN LAG(aid, 37) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 38) over(order by aid) > 0 THEN LAG(aid, 38) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 39) over(order by aid) > 0 THEN LAG(aid, 39) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 40) over(order by aid) > 0 THEN LAG(aid, 40) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 41) over(order by aid) > 0 THEN LAG(aid, 41) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 42) over(order by aid) > 0 THEN LAG(aid, 42) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 43) over(order by aid) > 0 THEN LAG(aid, 43) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 44) over(order by aid) > 0 THEN LAG(aid, 44) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 45) over(order by aid) > 0 THEN LAG(aid, 45) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 46) over(order by aid) > 0 THEN LAG(aid, 46) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 47) over(order by aid) > 0 THEN LAG(aid, 47) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 48) over(order by aid) > 0 THEN LAG(aid, 48) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 49) over(order by aid) > 0 THEN LAG(aid, 49) over(order by aid) " +
                $"  WHEN aapptype< 0 and LAG(aapptype, 50) over(order by aid) > 0 THEN LAG(aid, 50) over(order by aid) " +
                $"  ELSE -1" +
                $" END as parent_aid " +
                $"FROM application WHERE cym = {cym}) " +

                $"UPDATE application_aux as a " +
                $"SET parentaid = tmp.parent_aid " +
                $"FROM tmp " +
                $"WHERE a.aid = tmp.aid and cym = {cym}";

            var cmd = DB.Main.CreateCmd(sql);
            try
            {

                if (cmd.TryExecuteNonQuery())
                {
                    MessageBox.Show(cym + " の続紙関連付けを完了しました");
                }
                return true;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
                wf.Dispose();
            }
        }
        //20220829_1 ito ed /////


        //20210629165737 furukawa st //全DBに作成しておくとメンテが手間なのでここで作成
        //20210708182317 furukawa st //ストアド用sqlファイルを引数に

        /// <summary>
        /// ストアドを現在の保険者のDBに作成
        /// </summary>
        /// <param name="wf"></param>
        /// <returns></returns>
        /// 
        private static bool CreateStoredProcedure(WaitForm wf,string strStored)
        //private static bool CreateStoredProcedure(WaitForm wf)
        //20210708182317 furukawa ed ////////////////////////
        {
            wf.LogPrint("ストアド作成中");
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();

            //20210708182419 furukawa st //バッチに渡す引数にストアドファイル名追加
            string strarg = $" {DB.getDBHost()} {Settings.dbPort} {DB.GetMainDBName()} {strStored}";
            //string strarg = $" {DB.getDBHost()} {Settings.dbPort} {DB.GetMainDBName()}";
            //20210708182419 furukawa ed ////////////////////////

            try
            {
                psi.Arguments = strarg;

                psi.FileName = Application.StartupPath + "\\Stored\\create_stored.bat";
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;

                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = psi;

                p.Start();
                p.WaitForExit(8000);
                //p.WaitForExit(5000);

                wf.LogPrint("ストアド作成完了");

                return true;

            }
            catch (Exception ex) 
            { 
                
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
           
        }
        //20210629165737 furukawa ed ////////////////////////


        //20210629165816 furukawa st //ストアドを走らせて続紙と申請書の関連付け実行。クライアントからやると遅すぎて終わらない

        /// <summary>
        /// ParentRelationストアドを走らせる
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool UpdateZokushi(int cym,WaitForm wf)
        {
            string strdb = string.Empty;
            strdb = DB.GetMainDBName();
            DB.SetMainDBName("common");
            DB.SetMainDBName(strdb);
            
            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("続紙と申請書を関連付けています");
        
                DB.Command cmd = new DB.Command(DB.Main, $"select ParentRelation({cym})");
                var cnt = cmd.TryExecuteScalar();
                string tmp = cnt.ToString();

                wf.LogPrint($"{tmp}件　終了");
                
                MessageBox.Show($"{tmp}件　終了");
                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
            }
        }
        //20210629165816 furukawa ed ////////////////////////


        //20210708184917 furukawa st ////////////////////////
        //バッチシートと申請書を紐付け
        /// <summary>
        /// バッチシートと申請書を紐付け
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static bool RelateBatch(int cym,WaitForm wf)
        {
                       
            try
            {       
                if (!CreateStoredProcedure(wf, "BatchSheetRelation.sql"))         
                {
                    System.Windows.Forms.MessageBox.Show($"ストアド作成失敗に失敗しました。処理を中止します");
                    return false;
                }
                if (!UpdateBatchSheet(cym, wf))
                {
                    System.Windows.Forms.MessageBox.Show($"バッチシートと申請書の関連付けに失敗しました。処理を中止します");
                    return false;
                }

                MessageBox.Show("終了しました");
                
                return true;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
              
            }
        }


        /// <summary>
        /// バッチシートと申請書を関連付け
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool UpdateBatchSheet(int cym, WaitForm wf)
        {
            string strdb = string.Empty;
            strdb = DB.GetMainDBName();

            try
            {
                wf.ShowDialogOtherTask();
                wf.LogPrint("バッチシートと申請書を関連付けています");

                DB.Command cmd = new DB.Command(DB.Main, $"select BatchSheetRelation({cym})");
                var cnt = cmd.TryExecuteScalar();
                string tmp = cnt.ToString();

                wf.LogPrint($"{tmp}件　終了");

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{ex.Message} ");
                return false;
            }
            finally
            {
            }
        }
        //20210708184917 furukawa ed ////////////////////////

        //20200608173511 furukawa st ////////////////////////
        //保険者個別用の更新処理

        /// <summary>
        /// 申請書レコードの更新[保険者個別]（現在の更新タイプから、ステータスフラグを更新する）
        /// </summary>
        /// <param name="updateUserID"></param>
        /// <param name="updateType">現在の更新タイプ</param>
        /// <param name="tran"></param>
        /// <param name="ins">保険者別個別機能を使う場合保険者を指定</param>
        /// <returns></returns>
        public bool Update_SP(int updateUserID, UPDATE_TYPE updateType, DB.Transaction tran = null,int ins=0)
        {
            if (updateType == UPDATE_TYPE.FirstInput)
            {
                Ufirst = updateUserID;
                StatusFlagSet(StatusFlag.入力済);
                StatusFlagRemove(StatusFlag.ベリファイ済);
            }
            else if (updateType == UPDATE_TYPE.SecondInput)
            {
                Usecond = updateUserID;
                StatusFlagSet(StatusFlag.ベリファイ済);
            }
            else if (updateType == UPDATE_TYPE.Both)
            {
                Ufirst = updateUserID;
                Usecond = updateUserID;
                StatusFlagSet(StatusFlag.入力済 | StatusFlag.ベリファイ済);
            }
            else if (updateType == UPDATE_TYPE.FirstInputEx)
            {
                UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.拡張入力済);
                StatusFlagRemove(StatusFlag.拡張ベリ済);
            }
            else if (updateType == UPDATE_TYPE.SecondInputEx)
            {
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.拡張ベリ済);
            }
            else if (updateType == UPDATE_TYPE.BothEx)
            {
                UfirstEx = updateUserID;
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.拡張入力済 | StatusFlag.拡張ベリ済);
            }
            //20190510170913 furukawa st //医科突合フラグ更新処理
            else if (updateType == UPDATE_TYPE.Ikatotu)
            {
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.処理4);
            }
            //20190510170913 furukawa ed ////////////////////////

            //20200524105247 furukawa st //保険者個別機能用ステータス
            else if (updateType == UPDATE_TYPE.Sp1 && ins == (int)InsurerID.NAGOYASHI_KOKUHO)
            {
                UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.独自処理1);
                StatusFlagRemove(StatusFlag.独自処理2);
            }
            else if (updateType == UPDATE_TYPE.Sp2 && ins == (int)InsurerID.NAGOYASHI_KOKUHO)
            {
                UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.独自処理2);
            }
            //20200524105247 furukawa ed ////////////////////////

            //20200608164226 furukawa st //宮城県国保　患者照会入力ステータス
            //患者照会
            else if (updateType == UPDATE_TYPE.ListInput1 && ins==(int)InsurerID.MIYAGI_KOKUHO)
            {
                TaggedDatas.GeneralString2 = updateUserID.ToString();
                //UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.独自処理1);
                StatusFlagRemove(StatusFlag.独自処理2);
            }
            else if (updateType == UPDATE_TYPE.ListInput2 && ins == (int)InsurerID.MIYAGI_KOKUHO)
            {
                TaggedDatas.GeneralString3 = updateUserID.ToString();
                //UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.独自処理2);
            }

            //施術所照会
            else if (updateType == UPDATE_TYPE.Extra1 && ins == (int)InsurerID.MIYAGI_KOKUHO)
            {
                TaggedDatas.GeneralString4 = updateUserID.ToString();
                //UfirstEx = updateUserID;
                StatusFlagSet(StatusFlag.追加入力済);
                StatusFlagRemove(StatusFlag.追加ベリ済);
            }
            else if (updateType == UPDATE_TYPE.Extra2 && ins == (int)InsurerID.MIYAGI_KOKUHO)
            {
                TaggedDatas.GeneralString5 = updateUserID.ToString();
                //UsecondEx = updateUserID;
                StatusFlagSet(StatusFlag.追加ベリ済);
            }
            //20200608164226 furukawa ed ////////////////////////


            #region sql
            var sql = "UPDATE application SET " +
                "scanid=:scanid, groupid=:groupid, ayear=:ayear, amonth=:amonth, inum=:inum, hnum=:hnum, hpref=:hpref, " +
                "htype=:htype, hname=:hname, hzip=:hzip, haddress=:haddress, " +
                "pname=:pname, psex=:psex, pbirthday=:pbirthday, sid=:sid, asingle=:asingle, " +
                "afamily=:afamily, aratio=:aratio, publcexpense=:publcexpense, aimagefile=:aimagefile, " +
                "achargeyear=:achargeyear, achargemonth=:achargemonth, " +
                "iname1=:iname1, idate1=:idate1, ifirstdate1=:ifirstdate1, istartdate1=:istartdate1, " +
                "ifinishdate1=:ifinishdate1, idays1=:idays1, icourse1=:icourse1, " +
                "iname2=:iname2, idate2=:idate2, ifirstdate2=:ifirstdate2, istartdate2=:istartdate2, " +
                "ifinishdate2=:ifinishdate2, idays2=:idays2, icourse2=:icourse2, " +
                "iname3=:iname3, idate3=:idate3, ifirstdate3=:ifirstdate3, istartdate3=:istartdate3, " +
                "ifinishdate3=:ifinishdate3, idays3=:idays3, icourse3=:icourse3, " +
                "iname4=:iname4, idate4=:idate4, ifirstdate4=:ifirstdate4, istartdate4=:istartdate4, " +
                "ifinishdate4=:ifinishdate4, idays4=:idays4, icourse4=:icourse4, " +
                "iname5=:iname5, idate5=:idate5, ifirstdate5=:ifirstdate5, istartdate5=:istartdate5, " +
                "ifinishdate5=:ifinishdate5, idays5=:idays5, icourse5=:icourse5, " +
                "fchargetype=:fchargetype, fdistance=:fdistance, fvisittimes=:fvisittimes, fvisitfee=:fvisitfee, " +
                "fvisitadd=:fvisitadd, sregnumber=:sregnumber, szip=:szip, saddress=:saddress, sname=:sname, " +
                "stel=:stel, sdoctor=:sdoctor, skana=:skana, " +
                "bacctype=:bacctype, bname=:bname, btype=:btype, bbranch=:bbranch, " +
                "bbranchtype=:bbranchtype, baccname=:baccname, bkana=:bkana, baccnumber=:baccnumber, " +
                "atotal=:atotal, apartial=:apartial, acharge=:acharge, " +
                "acounteddays=:acounteddays, numbering=:numbering, aapptype=:aapptype, " +
                "ufirst=:ufirst, usecond=:usecond, uinquiry=:uinquiry, bui=:bui, " +
                "cym=:cym, ym=:ym, statusflags=:statusflags, shokaireason=:shokaireason, " +
                "rrid=:rrid, additionaluid1=:additionaluid1, " +
                "additionaluid2=:additionaluid2, memo_shokai=:memoshokai, memo_inspect=:memoinspect, " +
                "memo=:memo, paycode=:paycode, shokaicode=:shokaicode, ufirstex=:ufirstex, usecondex=:usecondex, " +
                "KagoReasons=:KagoReasons, saishinsareasons=:saishinsareasons, henreireasons=:henreireasons, " +
                "comnum=:comnum, groupnum=:groupnum, outmemo=:outmemo, taggeddatas=:taggeddatas " +
                ",kagoreasons_xml=:kagoreasons_xml " +//20201010200734 furukawakagoreasons_xml追加
                "WHERE aid=:aid;";
            
            #endregion

            using (var cmd = tran == null ? DB.Main.CreateCmd(sql) : DB.Main.CreateCmd(sql, tran))
            {
                var c = this;
                cmd.Parameters.Add("scanid", NpgsqlDbType.Integer).Value = c.ScanID;
                cmd.Parameters.Add("groupid", NpgsqlDbType.Integer).Value = c.GroupID;
                cmd.Parameters.Add("ayear", NpgsqlDbType.Integer).Value = c.MediYear;
                cmd.Parameters.Add("amonth", NpgsqlDbType.Integer).Value = c.MediMonth;
                cmd.Parameters.Add("inum", NpgsqlDbType.Text).Value = c.InsNum;
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = c.HihoNum;
                cmd.Parameters.Add("hpref", NpgsqlDbType.Integer).Value = c.HihoPref;
                cmd.Parameters.Add("htype", NpgsqlDbType.Integer).Value = c.HihoType;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = c.HihoName;
                cmd.Parameters.Add("hzip", NpgsqlDbType.Text).Value = c.HihoZip;
                cmd.Parameters.Add("haddress", NpgsqlDbType.Text).Value = c.HihoAdd;

                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = c.PersonName;
                cmd.Parameters.Add("psex", NpgsqlDbType.Integer).Value = c.Sex;
                cmd.Parameters.Add("pbirthday", NpgsqlDbType.Date).Value = c.Birthday;

                cmd.Parameters.Add("sid", NpgsqlDbType.Text).Value = c.ClinicNum;
                cmd.Parameters.Add("asingle", NpgsqlDbType.Integer).Value = c.Single;
                cmd.Parameters.Add("afamily", NpgsqlDbType.Integer).Value = c.Family;
                cmd.Parameters.Add("aratio", NpgsqlDbType.Integer).Value = c.Ratio;
                cmd.Parameters.Add("publcexpense", NpgsqlDbType.Text).Value = c.PublcExpense;
                cmd.Parameters.Add("aimagefile", NpgsqlDbType.Text).Value = c.ImageFile;
                cmd.Parameters.Add("achargeyear", NpgsqlDbType.Integer).Value = c.ChargeYear;
                cmd.Parameters.Add("achargemonth", NpgsqlDbType.Integer).Value = c.ChargeMonth;

                cmd.Parameters.Add("iname1", NpgsqlDbType.Text).Value = c.FushoName1;
                cmd.Parameters.Add("idate1", NpgsqlDbType.Date).Value = c.FushoDate1;
                cmd.Parameters.Add("ifirstdate1", NpgsqlDbType.Date).Value = c.FushoFirstDate1;
                cmd.Parameters.Add("istartdate1", NpgsqlDbType.Date).Value = c.FushoStartDate1;
                cmd.Parameters.Add("ifinishdate1", NpgsqlDbType.Date).Value = c.FushoFinishDate1;
                cmd.Parameters.Add("idays1", NpgsqlDbType.Integer).Value = c.FushoDays1;
                cmd.Parameters.Add("icourse1", NpgsqlDbType.Integer).Value = c.FushoCourse1;
                cmd.Parameters.Add("iname2", NpgsqlDbType.Text).Value = c.FushoName2;
                cmd.Parameters.Add("idate2", NpgsqlDbType.Date).Value = c.FushoDate2;
                cmd.Parameters.Add("ifirstdate2", NpgsqlDbType.Date).Value = c.FushoFirstDate2;
                cmd.Parameters.Add("istartdate2", NpgsqlDbType.Date).Value = c.FushoStartDate2;
                cmd.Parameters.Add("ifinishdate2", NpgsqlDbType.Date).Value = c.FushoFinishDate2;
                cmd.Parameters.Add("idays2", NpgsqlDbType.Integer).Value = c.FushoDays2;
                cmd.Parameters.Add("icourse2", NpgsqlDbType.Integer).Value = c.FushoCourse2;
                cmd.Parameters.Add("iname3", NpgsqlDbType.Text).Value = c.FushoName3;
                cmd.Parameters.Add("idate3", NpgsqlDbType.Date).Value = c.FushoDate3;
                cmd.Parameters.Add("ifirstdate3", NpgsqlDbType.Date).Value = c.FushoFirstDate3;
                cmd.Parameters.Add("istartdate3", NpgsqlDbType.Date).Value = c.FushoStartDate3;
                cmd.Parameters.Add("ifinishdate3", NpgsqlDbType.Date).Value = c.FushoFinishDate3;
                cmd.Parameters.Add("idays3", NpgsqlDbType.Integer).Value = c.FushoDays3;
                cmd.Parameters.Add("icourse3", NpgsqlDbType.Integer).Value = c.FushoCourse3;
                cmd.Parameters.Add("iname4", NpgsqlDbType.Text).Value = c.FushoName4;
                cmd.Parameters.Add("idate4", NpgsqlDbType.Date).Value = c.FushoDate4;
                cmd.Parameters.Add("ifirstdate4", NpgsqlDbType.Date).Value = c.FushoFirstDate4;
                cmd.Parameters.Add("istartdate4", NpgsqlDbType.Date).Value = c.FushoStartDate4;
                cmd.Parameters.Add("ifinishdate4", NpgsqlDbType.Date).Value = c.FushoFinishDate4;
                cmd.Parameters.Add("idays4", NpgsqlDbType.Integer).Value = c.FushoDays4;
                cmd.Parameters.Add("icourse4", NpgsqlDbType.Integer).Value = c.FushoCourse4;
                cmd.Parameters.Add("iname5", NpgsqlDbType.Text).Value = c.FushoName5;
                cmd.Parameters.Add("idate5", NpgsqlDbType.Date).Value = c.FushoDate5;
                cmd.Parameters.Add("ifirstdate5", NpgsqlDbType.Date).Value = c.FushoFirstDate5;
                cmd.Parameters.Add("istartdate5", NpgsqlDbType.Date).Value = c.FushoStartDate5;
                cmd.Parameters.Add("ifinishdate5", NpgsqlDbType.Date).Value = c.FushoFinishDate5;
                cmd.Parameters.Add("idays5", NpgsqlDbType.Integer).Value = c.FushoDays5;
                cmd.Parameters.Add("icourse5", NpgsqlDbType.Integer).Value = c.FushoCourse5;

                cmd.Parameters.Add("fchargetype", NpgsqlDbType.Integer).Value = c.NewContType;
                cmd.Parameters.Add("fdistance", NpgsqlDbType.Integer).Value = c.Distance;
                cmd.Parameters.Add("fvisittimes", NpgsqlDbType.Integer).Value = c.VisitTimes;
                cmd.Parameters.Add("fvisitfee", NpgsqlDbType.Integer).Value = c.VisitFee;
                cmd.Parameters.Add("fvisitadd", NpgsqlDbType.Integer).Value = c.VisitAdd;
                cmd.Parameters.Add("sregnumber", NpgsqlDbType.Text).Value = c.DrNum;
                cmd.Parameters.Add("szip", NpgsqlDbType.Text).Value = c.ClinicZip;
                cmd.Parameters.Add("saddress", NpgsqlDbType.Text).Value = c.ClinicAdd;
                cmd.Parameters.Add("sname", NpgsqlDbType.Text).Value = c.ClinicName;
                cmd.Parameters.Add("stel", NpgsqlDbType.Text).Value = c.ClinicTel;
                cmd.Parameters.Add("sdoctor", NpgsqlDbType.Text).Value = c.DrName;
                cmd.Parameters.Add("skana", NpgsqlDbType.Text).Value = c.DrKana;
                cmd.Parameters.Add("bacctype", NpgsqlDbType.Integer).Value = c.AccountType;
                cmd.Parameters.Add("bname", NpgsqlDbType.Text).Value = c.BankName;
                cmd.Parameters.Add("btype", NpgsqlDbType.Integer).Value = c.BankType;
                cmd.Parameters.Add("bbranch", NpgsqlDbType.Text).Value = c.BankBranch;
                cmd.Parameters.Add("bbranchtype", NpgsqlDbType.Integer).Value = c.BankBranchType;
                cmd.Parameters.Add("baccname", NpgsqlDbType.Text).Value = c.AccountName;
                cmd.Parameters.Add("bkana", NpgsqlDbType.Text).Value = c.AccountKana;
                cmd.Parameters.Add("baccnumber", NpgsqlDbType.Text).Value = c.AccountNumber;

                cmd.Parameters.Add("atotal", NpgsqlDbType.Integer).Value = c.Total;
                cmd.Parameters.Add("apartial", NpgsqlDbType.Integer).Value = c.Partial;
                cmd.Parameters.Add("acharge", NpgsqlDbType.Integer).Value = c.Charge;
                cmd.Parameters.Add("acounteddays", NpgsqlDbType.Integer).Value = c.CountedDays;
                cmd.Parameters.Add("numbering", NpgsqlDbType.Text).Value = c.Numbering;
                cmd.Parameters.Add("aapptype", NpgsqlDbType.Integer).Value = (int)c.AppType;
                cmd.Parameters.Add("ufirst", NpgsqlDbType.Integer).Value = c.Ufirst;
                cmd.Parameters.Add("usecond", NpgsqlDbType.Integer).Value = c.Usecond;
                cmd.Parameters.Add("uinquiry", NpgsqlDbType.Integer).Value = c.Uinquiry;

                cmd.Parameters.Add("bui", NpgsqlDbType.Integer).Value = c.Bui;

                //20190425190415 furukawa st //令和対応
                int cym = DateTimeEx.GetAdYearFromHs(ChargeYear * 100 + ChargeMonth) * 100 + ChargeMonth;
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym; //(ChargeYear + 1988) * 100 + ChargeMonth;
                int ym = DateTimeEx.GetAdYearFromHs(MediYear * 100 + MediMonth) * 100 + MediMonth;

                //20190716150824 furukawa st //1未満の場合はayearをそのまま、それ以外は年月を入れる
                cmd.Parameters.Add("ym", NpgsqlDbType.Integer).Value = MediYear < 1 ? MediYear : ym;
                //cmd.Parameters.Add("ym", NpgsqlDbType.Integer).Value = ym;// MediYear < 1 ? MediYear : (MediYear + 1988) * 100 + MediMonth;
                //20190716150824 furukawa ed ////////////////////////

                //cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = (ChargeYear + 1988) * 100 + ChargeMonth;
                //cmd.Parameters.Add("ym", NpgsqlDbType.Integer).Value = MediYear < 1 ? MediYear : (MediYear + 1988) * 100 + MediMonth;
                //20190425190415 furukawa ed ////////////////////////

                cmd.Parameters.Add("statusflags", NpgsqlDbType.Integer).Value = (int)c.StatusFlags;
                cmd.Parameters.Add("shokaireason", NpgsqlDbType.Integer).Value = (int)c.ShokaiReason;
                cmd.Parameters.Add("rrid", NpgsqlDbType.Integer).Value = c.RrID;

                cmd.Parameters.Add("additionaluid1", NpgsqlDbType.Integer).Value = c.AdditionalUid1;
                cmd.Parameters.Add("additionaluid2", NpgsqlDbType.Integer).Value = c.AdditionalUid2;
                cmd.Parameters.Add("memoshokai", NpgsqlDbType.Text).Value = c.MemoShokai;
                cmd.Parameters.Add("memoinspect", NpgsqlDbType.Text).Value = c.MemoInspect;
                cmd.Parameters.Add("memo", NpgsqlDbType.Text).Value = c.Memo;
                cmd.Parameters.Add("paycode", NpgsqlDbType.Text).Value = c.PayCode;
                cmd.Parameters.Add("shokaicode", NpgsqlDbType.Text).Value = c.ShokaiCode;
                cmd.Parameters.Add("ufirstex", NpgsqlDbType.Integer).Value = c.UfirstEx;
                cmd.Parameters.Add("usecondex", NpgsqlDbType.Integer).Value = c.UsecondEx;

                cmd.Parameters.Add("KagoReasons", NpgsqlDbType.Integer).Value = (int)c.KagoReasons;

                //20201010200900 furukawa st //kagoreasons_xmlに変更
                            //20201012104222 furukawa st //から文字の場合nullにしないとxml型違いdb登録ではじかれる
                            cmd.Parameters.Add("KagoReasons_xml", NpgsqlDbType.Xml).Value = c.strKagoReasonsXML == string.Empty ? null : c.strKagoReasonsXML;
                            //cmd.Parameters.Add("KagoReasons_xml", NpgsqlDbType.Integer).Value = strKagoReasonsXML;                            
                            //20201012104222 furukawa ed ////////////////////////
                //20201010200900 furukawa ed ////////////////////////

                cmd.Parameters.Add("saishinsareasons", NpgsqlDbType.Integer).Value = (int)c.SaishinsaReasons;
                cmd.Parameters.Add("henreireasons", NpgsqlDbType.Integer).Value = (int)c.HenreiReasons;
                cmd.Parameters.Add("comnum", NpgsqlDbType.Text).Value = c.ComNum;
                cmd.Parameters.Add("groupnum", NpgsqlDbType.Text).Value = c.GroupNum;
                cmd.Parameters.Add("outmemo", NpgsqlDbType.Text).Value = c.OutMemo;
                cmd.Parameters.Add("taggeddatas", NpgsqlDbType.Text).Value = TaggedDatas.CreateDbStr();

                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = c.Aid;

                return cmd.TryExecuteNonQuery();
            }
        }
        //20200608173511 furukawa ed ////////////////////////


        /// <summary>
        /// 点検用Update
        /// </summary>
        /// <returns></returns>
        public bool UpdateINQ()
        {
            using (var cmd = DB.Main.CreateCmd("UPDATE application SET " +
                "haddress =:haddress, pname=:pname, uinquiry=:uinquiry, statusflags=:statusflags, " +
                "shokaireason=:shokaireason, memo_shokai=:memoshokai, memo_inspect=:memoinspect, memo=:memo, " +
                "KagoReasons=:KagoReasons, " +//従来の過誤フラグ
                "saishinsareasons=:saishinsareasons, henreireasons=:henreireasons, " +
                "outmemo=:outmemo, taggeddatas=:taggeddatas " +
                ",kagoreasons_xml=:kagoreasons_xml " +//20201011145715 furukawa 過誤フラグ追加に伴ってxml列追加
                                                      
                "WHERE aid=:aid"))
            {
                var c = this;
                cmd.Parameters.Add("haddress", NpgsqlDbType.Text).Value = c.HihoAdd;
                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = c.PersonName;
                cmd.Parameters.Add("uinquiry", NpgsqlDbType.Integer).Value = c.Uinquiry;
                cmd.Parameters.Add("statusflags", NpgsqlDbType.Integer).Value = (int)c.StatusFlags;
                cmd.Parameters.Add("shokaireason", NpgsqlDbType.Integer).Value = (int)c.ShokaiReason;
                cmd.Parameters.Add("memoshokai", NpgsqlDbType.Text).Value = c.MemoShokai;
                cmd.Parameters.Add("memoinspect", NpgsqlDbType.Text).Value = c.MemoInspect;
                cmd.Parameters.Add("memo", NpgsqlDbType.Text).Value = c.Memo;

                cmd.Parameters.Add("KagoReasons", NpgsqlDbType.Integer).Value = (int)c.KagoReasons;

                //20201011145807 furukawa st //過誤フラグ追加に伴ってxml列追加
                //20220215105516 furukawa st //NPGSQL2.2.7から4.0.12への変更により型変換が厳密に
                cmd.Parameters.Add("KagoReasons_xml", NpgsqlDbType.Xml).Value = c.strKagoReasonsXML == string.Empty ? "" : c.strKagoReasonsXML;
                //          20201012103836 furukawa st //から文字の場合nullにしないとxml型違いdb登録ではじかれる
                //          cmd.Parameters.Add("kagoreasons_xml", NpgsqlDbType.Xml).Value = strKagoReasonsXML;
                //          cmd.Parameters.Add("KagoReasons_xml", NpgsqlDbType.Xml).Value = c.strKagoReasonsXML == string.Empty ? null : c.strKagoReasonsXML;
                //          20201012103836 furukawa ed ////////////////////////
                //20220215105516 furukawa ed ////////////////////////
                //20201011145807 furukawa ed ////////////////////////

                cmd.Parameters.Add("saishinsareasons", NpgsqlDbType.Integer).Value = (int)c.SaishinsaReasons;
                cmd.Parameters.Add("henreireasons", NpgsqlDbType.Integer).Value = (int)c.HenreiReasons;
                cmd.Parameters.Add("outmemo", NpgsqlDbType.Text).Value = c.OutMemo;
                cmd.Parameters.Add("taggeddatas", NpgsqlDbType.Text).Value = c.TaggedDatas.CreateDbStr();

                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = c.Aid;

                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// 追加入力（Medivery）用Update
        /// 入力者情報付加
        /// </summary>
        /// <returns></returns>
        public bool UpdateMedivery(UPDATE_TYPE upType, DB.Transaction tran = null)
        {
            if (upType == UPDATE_TYPE.FirstInput)
            {
                AdditionalUid1 = User.CurrentUser.UserID;
            }
            else if (upType == UPDATE_TYPE.SecondInput)
            {
                AdditionalUid2 = User.CurrentUser.UserID;
            }
            else if (upType == UPDATE_TYPE.Both)
            {
                AdditionalUid1 = User.CurrentUser.UserID;
                AdditionalUid2 = User.CurrentUser.UserID;
            }

            var sql = "UPDATE application SET " +
                "pname=:pname, hname=:hname, hzip=:hzip, haddress=:haddress, " +
                "sname=:sname, fvisittimes=:fvisittimes, baccname=:baccname, baccnumber=:baccnumber, " +
                "pbirthday=:pbirthday, hnum=:hnum, ayear=:ayear, amonth=:amonth, " +
                "acounteddays=:acounteddays, atotal=:atotal, acharge=:acharge, " +
                "apartial=:apartial, sdoctor=:sdoctor, statusflags=:statusflags, " +
                "additionaluid1=:auid1, additionaluid2=:auid2 " +
                "WHERE aid=:aid";

            using (var cmd = tran == null ? DB.Main.CreateCmd(sql) : DB.Main.CreateCmd(sql, tran))
            {
                var c = this;
                cmd.Parameters.Add("pname", NpgsqlDbType.Text).Value = c.PersonName;
                cmd.Parameters.Add("hname", NpgsqlDbType.Text).Value = c.HihoName;
                cmd.Parameters.Add("hzip", NpgsqlDbType.Text).Value = c.HihoZip;
                cmd.Parameters.Add("haddress", NpgsqlDbType.Text).Value = c.HihoAdd;
                cmd.Parameters.Add("sname", NpgsqlDbType.Text).Value = c.ClinicName;
                cmd.Parameters.Add("fvisittimes", NpgsqlDbType.Integer).Value = c.VisitTimes;
                cmd.Parameters.Add("baccname", NpgsqlDbType.Text).Value = c.AccountName;
                cmd.Parameters.Add("baccnumber", NpgsqlDbType.Text).Value = c.AccountNumber;
                cmd.Parameters.Add("pbirthday", NpgsqlDbType.Date).Value = c.Birthday;
                cmd.Parameters.Add("hnum", NpgsqlDbType.Text).Value = c.HihoNum;
                cmd.Parameters.Add("ayear", NpgsqlDbType.Integer).Value = c.MediYear;
                cmd.Parameters.Add("amonth", NpgsqlDbType.Integer).Value = c.MediMonth;
                cmd.Parameters.Add("acounteddays", NpgsqlDbType.Integer).Value = c.CountedDays;
                cmd.Parameters.Add("atotal", NpgsqlDbType.Integer).Value = c.Total;
                cmd.Parameters.Add("acharge", NpgsqlDbType.Integer).Value = c.Charge;
                cmd.Parameters.Add("apartial", NpgsqlDbType.Integer).Value = c.Partial;
                cmd.Parameters.Add("sdoctor", NpgsqlDbType.Text).Value = c.DrName;
                cmd.Parameters.Add("statusflags", NpgsqlDbType.Integer).Value = (int)c.StatusFlags;
                cmd.Parameters.Add("auid1", NpgsqlDbType.Integer).Value = c.AdditionalUid1;
                cmd.Parameters.Add("auid2", NpgsqlDbType.Integer).Value = c.AdditionalUid2;

                cmd.Parameters.Add("aid", NpgsqlDbType.Integer).Value = c.Aid;

                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// ScanId単位で申請書を削除
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static bool DeleteBySID(int sid)
        {
            using (var cmd = DB.Main.CreateCmd("DELETE FROM application " +
                "WHERE scanid=:sid"))
            {
                cmd.Parameters.Add("sid", NpgsqlDbType.Integer).Value = sid;
                var res = cmd.TryExecuteReaderList();

                return cmd.TryExecuteNonQuery();
            }
        }

        /// <summary>
        /// 点検抽出用・「FROM」～「WHERE」キーワードを含む句を指定します
        /// </summary>
        /// <param name="sqlParam">デフォルトのSELECT以降のSQL文
        /// FROM application AS a WHERE ～</param>
        /// <returns></returns>
        public static List<App> InspectSelect(string sqlParam)
        {
            string sql = "SELECT " + SELECT_CLAUSE + sqlParam +
                " ORDER BY a.aid;";

            return App.GetAppFromSql(sql, null);
        }


        /// <summary>
        /// 申請書を区分ごとにカウントします
        /// </summary>
        public static void InspectCount(int cym, out int all, out int kago, out int oryogigi, out int saishinsa, out int syokai, out int autodetect, out int autodetectOryo)
        {
            all = 0;
            kago = 0;
            saishinsa = 0;
            oryogigi = 0;
            syokai = 0;
            autodetect = 0;
            autodetectOryo = 0;

            using (var cmd = DB.Main.CreateCmd("SELECT statusflags, COUNT(aid) FROM application " +
                "WHERE cym=:cym GROUP BY statusflags;"))
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;

                var l = cmd.TryExecuteReaderList();
                foreach (var item in l)
                {
                    int c = (int)(long)item[1];
                    all += c;
                    StatusFlag sf = (StatusFlag)(int)item[0];
                    //未チェック = 0, チェック済 = 1, 点検済み = 2, マッチング有 = 3, マッチング無 = 4, ベリファイ済 = 5 }
                    if (sf.HasFlag(StatusFlag.過誤)) kago += c;
                    else if (sf.HasFlag(StatusFlag.再審査)) saishinsa += c;
                    else if (sf.HasFlag(StatusFlag.往療疑義)) oryogigi += c;
                    else if (sf.HasFlag(StatusFlag.照会対象)) syokai += c;
                    else if (sf.HasFlag(StatusFlag.往療点検対象)) autodetectOryo += c;
                    else if (sf.HasFlag(StatusFlag.点検対象)) autodetect += c;
                }
            }
        }

        /// <summary>
        /// 請求年と月から、指定したフラグを含む申請書を一覧で取得します。
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public static List<App> GetAppsByStatusFlag(int cym, StatusFlag flag)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.cym=:cym AND a.statusflags&:statusflags!=0" +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("cym", NpgsqlDbType.Integer).Value = cym;
                cmd.Parameters.Add("statusflags", NpgsqlDbType.Integer).Value = (int)flag;
            });
        }

        
        /// <summary>
        ///20201119121247 furukawa settings.xmlではなくjyusei.insurerから取得
        /// 画像ファイルのscanフォルダのパスを取得
        /// </summary>
        /// <param name="DBName"></param>
        /// <returns></returns>
        public static string GetImageFolderPath(string DBName)
        {
            return DB.dicDB[DBName].strScanPath;
        }


        /// <summary>
        /// 画像ファイルのフルパスを取得します
        /// </summary>
        /// <returns></returns>
        public string GetImageFullPath(string DBName)
        {
            //20200910194055 furukawa st ////////////////////////
            //画像パスはinsurerテーブルから取得する（AWS段階的移行対応
            
            return DB.dicDB[DBName].strScanPath + "\\" + DBName + "\\" + ScanID.ToString() + "\\" + ImageFile;

            //return Settings.ImageFolder +
            //    "\\" + DBName + "\\" + ScanID.ToString() + "\\" + ImageFile;
            //20200910194055 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 画像ファイルのフルパスを取得します
        /// </summary>
        /// <returns></returns>
        public string GetImageFullPath()
        {
            var DBName = DB.GetMainDBName();

            //20200910194204 furukawa st //画像パスはinsurerテーブルから取得する（AWS段階的移行対応
            return DB.dicDB[DBName].strScanPath + "\\" + DBName + "\\" + ScanID.ToString() + "\\" + ImageFile;
            //return Settings.ImageFolder + "\\" + DBName + "\\" + ScanID.ToString() + "\\" + ImageFile;
            //20200910194204 furukawa ed ////////////////////////
        }

        /// <summary>
        /// AppインスタンスデータをDBから取得する
        /// </summary>
        /// <param name="sql">applicationテーブルをクエリするデータ</param>
        /// <returns>取得したデータ</returns>
        public static List<App> GetAppFromSql(string sql, Action<DB.Command> procParams)
        {
            var l = new List<App>();

            using (var cmd = DB.Main.CreateLongTimeoutCmd(sql))
            {
                // パラメータ処理
                procParams?.Invoke(cmd);
                var res = cmd.TryExecuteReaderList();
                if (res != null)
                {
                    foreach (var item in res)
                    {
                        l.Add(CreateAppFromRecord(item));
                    }
                }
            }

            return l;
        }

        /// <summary>
        /// メモ(Memoプロパティ)とステータスのみ更新します
        /// </summary>
        /// <returns></returns>
        public bool MemoAndFlagsUpdate()
        {
            var sql = $"UPDATE application SET memo=@memo, statusflags=@flag WHERE aid=@aid";
            return DB.Main.Excute(sql, new { memo = Memo, flag= StatusFlags, aid = Aid });
        }

        // 医療機関コード補正用。APP_TYPEとは異なる為マッピング用
        public int GetAppTypeInt()
        {
            var result = 0;
            switch (this.AppType)
            {
                case APP_TYPE.医科:
                    result = 1;
                    break;
                case APP_TYPE.鍼灸:
                    result = 7;
                    break;
                case APP_TYPE.柔整:
                    result = 8;
                    break;
                case APP_TYPE.あんま:
                    result = 9;
                    break;
            }
            return result;
        }


        /// <summary>
        /// 20221216 ito 外部入力2回目用
        /// </summary>
        /// <param name="groupID"></param>
        /// <returns></returns>
        public static List<App> GetAppsGID2(int groupID)
        {
            string sql = "SELECT " + SELECT_CLAUSE +
                "FROM application AS a " +
                "WHERE a.groupid=:gid " + "AND aapptype = 6 " +
                "ORDER BY a.aid";

            return GetAppFromSql(sql, (cmd) =>
            {
                cmd.Parameters.Add("gid", NpgsqlDbType.Integer).Value = groupID;
            });
        }


    }


    public partial class hihoInfo
    {
        public string hihoName { get; set; } = string.Empty;
        public string hihoNum { get; set; } = string.Empty;
        public int cym { get; set; } = 0;

        public static List<hihoInfo> GetHihoInfo()
        {
            List<hihoInfo> lst = new List<hihoInfo>();
            //aidが大きいレコードを取得（後ろの方が新しいと仮定する）
            DB.Command cmd = new DB.Command(DB.Main, "select distinct hnum,hname,aid from application  order by aid desc");
            var  l =cmd.TryExecuteReaderList();

            for (int r = 0; r < l.Count; r++)
            {
                hihoInfo h = new hihoInfo();
                h.hihoNum = l[r][0].ToString();
                h.hihoName = l[r][1].ToString();
                h.cym = int.Parse(l[r][2].ToString());
                lst.Add(h);
            }
            cmd.Dispose();
            return lst;
        }
    }

    //20200804171412 furukawa ////////////////////////
    //最新AIDの被保険者/受療者情報を取得
    /// <summary>
    /// 被保険者・受療者情報
    /// </summary>
    public partial class APP_HihoPersonInfo
    {
        public int aid { get; set; } = 0;
        public string hihoName { get; set; } = string.Empty;
        public string hihoNum { get; set; } = string.Empty;
        public string hihoZip { get; set; } = string.Empty;
        public string hihoAddress { get; set; } = string.Empty;
        public string pname { get; set; } = string.Empty;
        public int psex { get; set; } = 0;
        public DateTime pbirthday { get; set; } =DateTime.MinValue;


        /// <summary>
        /// 最新AIDの被保険者/受療者情報を取得
        /// </summary>
        /// <returns>被保険者/受療者情報</returns>
        public static List<APP_HihoPersonInfo> GetHihoPersonInfo()
        {
            List<APP_HihoPersonInfo> lst = new List<APP_HihoPersonInfo>();
            //aidが大きいレコードを取得（後ろの方が新しいと仮定する）
            DB.Command cmd = new DB.Command(DB.Main, "select distinct aid,hnum,hname,hzip,haddress,pname,psex,pbirthday from application order by aid desc");
            var l = cmd.TryExecuteReaderList();

            for (int r = 0; r < l.Count; r++)
            {
                APP_HihoPersonInfo h = new APP_HihoPersonInfo();
                h.aid = int.Parse(l[r][0].ToString());
                h.hihoNum = l[r][1].ToString();
                h.hihoName = l[r][2].ToString();
                h.hihoZip = l[r][3].ToString();
                h.hihoAddress = l[r][4].ToString();
                h.pname = l[r][5].ToString();
                h.psex = l[r][6].ToString() == string.Empty ?  0 : int.Parse(l[r][6].ToString());
                h.pbirthday = !(DateTime.TryParse(l[r][7].ToString(),out DateTime dtpbirthday)) ? DateTime.MinValue: dtpbirthday;

                lst.Add(h);
            }
            cmd.Dispose();
            return lst;
        }
    }

    //20200616110449 furukawa 柔整師登録記号番号で施術所情報取得

    /// <summary>
    /// 施術所情報
    /// </summary>
    public partial class APP_ClinicInfo
    {
        public string strClinicNum { get; set; } = string.Empty;
        public string strClinicName { get; set; } = string.Empty;
        public string strDrNum { get; set; } = string.Empty;
        public string strDrName { get; set; } = string.Empty;
        public string strDrNameKana { get; set; } = string.Empty;
        public string strClinicZip { get; set; } = string.Empty;
        public string strClinicAddress { get; set; } = string.Empty;
        public string strClinicTel { get; set; } = string.Empty;

        public static List<APP_ClinicInfo> lstClinicInfo = new List<APP_ClinicInfo>();
        public static Dictionary<string, APP_ClinicInfo> dicClinicInfoDrNum = new Dictionary<string, APP_ClinicInfo>();
        public static Dictionary<string, APP_ClinicInfo> dicClinicInfoClinicNum = new Dictionary<string, APP_ClinicInfo>();

        /// <summary>
        /// 施術所・柔整師情報取得
        /// </summary>
        /// <returns>List<APP_ClinicInfo></returns>
        public static List<APP_ClinicInfo> CreateClinicInfoList()
        {
            DB.Command cmd = new DB.Command(DB.Main, "select distinct aid,sregnumber,sdoctor,skana,sid,szip,saddress,sname,stel from application order by aid desc");
            var l = cmd.TryExecuteReaderList();
            for(int r =0;r<l.Count;r++)
            {
                APP_ClinicInfo c = new APP_ClinicInfo();
                
                c.strDrNum = l[r][1].ToString();
                c.strDrName = l[r][2].ToString();
                c.strDrNameKana = l[r][3].ToString();

                c.strClinicNum = l[r][4].ToString();
                c.strClinicZip = l[r][5].ToString();
                c.strClinicAddress = l[r][6].ToString();
                c.strClinicName = l[r][7].ToString();
                c.strClinicTel = l[r][8].ToString();

                lstClinicInfo.Add(c);

            }
            return lstClinicInfo;
        }

        /// <summary>
        /// 柔整師登録記号番号で施術所情報取得
        /// </summary>
        /// <param name="cym"></param>
        public static void CreateClinicInfoList(int cym)
        {
            //20200621105437 furukawa st //柔整師登録記号番号がないレコードは無視
            string strsql = $" where a.cym={cym} and a.sregnumber<>''";
            //string strsql = $" where a.cym={cym}";
            //20200621105437 furukawa ed ////////////////////////

            List<App> lstapp =App.GetAppsWithWhere(strsql);
            
            foreach (App a in lstapp)
            {
                APP_ClinicInfo c = new APP_ClinicInfo();

                c.strClinicName = a.ClinicName;
                c.strClinicNum = a.ClinicNum;
                c.strDrName = a.DrName;
                c.strDrNum = a.DrNum;
                c.strDrNameKana = a.DrKana;
                c.strClinicZip = a.ClinicZip;
                c.strClinicAddress = a.ClinicAdd;
                c.strClinicTel = a.ClinicTel;

                c.strClinicName = a.ClinicName;
                c.strClinicNum = a.ClinicNum;
                c.strDrName = a.DrName;
                c.strDrNum = a.DrNum;
                c.strDrNameKana = a.DrKana;
                c.strClinicZip = a.ClinicZip;
                c.strClinicAddress = a.ClinicAdd;
                c.strClinicTel = a.ClinicTel;

                lstClinicInfo.Add(c);
                try
                {
                    dicClinicInfoDrNum.Add(a.DrNum, c);
                    dicClinicInfoClinicNum.Add(a.ClinicNum, c);
                }
                catch (Exception ex)
                {
                    //重複エラー無視
                }
            }
        }

        //20200713182316 furukawa st ////////////////////////
        //医療機関情報dictionary取得

        /// <summary>
        /// 施術所名、医療機関コード、施術師名、柔整師登録記号番号取得
        /// </summary>
        /// <param name="cym">cym</param>
        /// <returns>Dictionary<string, ClinicInfo></returns>
        public static Dictionary<string, APP_ClinicInfo> GetClinicInfoDrNumDic(int cym)
        {
            //柔整師登録記号番号がないレコードは無視
            string strsql = $" where a.sregnumber<>''";

            List<App> lstapp = App.GetAppsWithWhere(strsql);

            foreach (App a in lstapp)
            {
                APP_ClinicInfo c = new APP_ClinicInfo();
                c.strClinicName = a.ClinicName;
                c.strClinicNum = a.ClinicNum;
                c.strDrName = a.DrName;
                c.strDrNum = a.DrNum;

                c.strDrNameKana = a.DrKana;
                c.strClinicZip = a.ClinicZip;
                c.strClinicAddress = a.ClinicAdd;
                c.strClinicTel = a.ClinicTel;
                lstClinicInfo.Add(c);

                try
                {
                    dicClinicInfoDrNum.Add(a.DrNum, c);
                    dicClinicInfoClinicNum.Add(a.ClinicNum, c);
                }
                catch (Exception ex)
                {
                    //重複エラー無視
                }
            }
            return dicClinicInfoDrNum;
        }
        //20200713182316 furukawa ed ////////////////////////


        /// <summary>
        /// 柔整師登録記号番号から取得
        /// </summary>
        /// <param name="strDrNum">柔整師登録記号番号</param>
        /// <returns></returns>
        public static APP_ClinicInfo selectFromDrNum(string strDrNum)
        {
            foreach (APP_ClinicInfo c in dicClinicInfoDrNum.Values)
            {
                //20200701154656 furukawa st //判定間違い
                if (c.strDrNum == strDrNum)
                //if(dicClinicInfoDrNum.ContainsKey(strDrNum))
                //20200701154656 furukawa ed ////////////////////////
                {
                    return c;
                }
            }
            return new APP_ClinicInfo();//nullエラー回避
        }
    }
}