﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.ChibaKoikiAHK

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        /// <summary>
        /// 提供データ(支給決定データ)
        /// </summary>
        List<imp_chargedata> lstChargeData = new List<imp_chargedata>();

        /// <summary>
        /// 提供データ（被保険者マスタ個人情報のみ）
        /// </summary>
        List<imp_hihomaster> lstHihoMaster_pd = new List<imp_hihomaster>();

        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();          

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum,  };

            //20220701133503 furukawa st ////////////////////////
            //座標制御コントロールが漏れてた
            

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal,checkBoxVisit,checkBoxVisitKasan,checkBoxKofu, };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1 ,verifyBoxF1FirstD,};


            //      //合計、往療、前回支給
            //      totalConts = new Control[] { verifyBoxTotal, };

            //      //初検日
            //      firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1 };

            //20220701133503 furukawa ed ////////////////////////

            #endregion


            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //初回表示時
            if (!InitControl(app, verifyBoxY)) return;
            
            if (app != null)
            {
                
                //提供データ取得
                lstChargeData = imp_chargedata.selectall(app.CYM);

                //20220412133759 furukawa st ////////////////////////
                //件数が100万と多いため初期ロードを中止、登録時に抽出に変更

                //  lstHihoMaster_pd = imp_hihomaster.selectpd(app.CYM);
                //20220412133759 furukawa ed ////////////////////////


                setApp(app);
            }        

            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            //20210520151626 furukawa st ////////////////////////
            //種類に応じたセンシティブ制御

            App app = (App)bsApp.Current;
            
            InitControl(app, verifyBoxY);
            //20210520151626 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// センシティブ制御 20210520151408 furukawa センシティブ制御まとめた
        /// 柔整通常入力なし、鍼灸のみ傷病名1
        /// </summary>
        /// <param name="app"></param>
        /// <param name="verifyboxY"></param>
        /// <returns></returns>
        private bool InitControl(App app,VerifyBox verifyboxY)
        {
          
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };


            phnum.Visible = false;
            
            panelTotal.Visible = false;
            dgv.Visible = false;
            verifyBoxF1.Visible = false;
            labelF1.Visible = false;

            //AppがNull（入力前）のときはscanのを採用
            APP_TYPE type = app.AppType == APP_TYPE.NULL ? scan.AppType : app.AppType;
           

            if (type == APP_TYPE.柔整)
            {
                MessageBox.Show("あはきのみ入力可能です");
                return false;
            }

            //未入力・入力後表示前
            if (verifyboxY.Text.Trim() == string.Empty)
            {
                switch (type)
                {
                    case APP_TYPE.長期:
                    case APP_TYPE.続紙:
                    case APP_TYPE.不要:
                    case APP_TYPE.エラー:
                    case APP_TYPE.同意書裏:
                    case APP_TYPE.施術報告書:
                    case APP_TYPE.状態記入書:
                        break;

                    case APP_TYPE.同意書:
                        
                        break;

                    case APP_TYPE.柔整:
                        //通常入力無し
                        break;

                    case APP_TYPE.鍼灸:
                        verifyBoxF1.Visible = true;
                        labelF1.Visible = true;

                        phnum.Visible = true;
                        
                        panelTotal.Visible = true;
                        dgv.Visible = true;

                        break;

                    case APP_TYPE.あんま:

                        phnum.Visible =  true;
                        
                        panelTotal.Visible = true;
                        dgv.Visible = true;

                        break;
                    default:
                        break;
                }
            }

            //登録後、再表示時
            else
            {

                switch (verifyboxY.Text.Trim())
                {
                    case clsInputKind.長期:
                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.施術報告書:
                    case clsInputKind.状態記入書:
                        break;

                    case clsInputKind.施術同意書:
                        
                        break;

                    default:

                 
                        switch (type)
                        {
                            case APP_TYPE.あんま:
                                phnum.Visible = true;
                                
                                panelTotal.Visible = true;
                                dgv.Visible = true;
                                break;
                            case APP_TYPE.鍼灸:
                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                phnum.Visible = true;
                                
                                panelTotal.Visible = true;
                                dgv.Visible = true;


                                break;
                            default:
                                //20211021121136 furukawa st ////////////////////////
                                //aapptypeがあんまでも鍼でもないが、数字が入った場合＝申請書なのにそれ以外と誤入力し修正したと想定する
                                
                                
                                if (type!=APP_TYPE.あんま && 
                                    type != APP_TYPE.鍼灸 && 
                                    int.TryParse(verifyboxY.Text.Trim(),out int tmp))
                                {
                                    switch (scan.AppType)
                                    {
                                        case APP_TYPE.あんま:
                                            phnum.Visible = true;
                                            
                                            panelTotal.Visible = true;
                                            dgv.Visible = true;
                                            break;
                                        case APP_TYPE.鍼灸:
                                            verifyBoxF1.Visible = true;
                                            labelF1.Visible = true;

                                            phnum.Visible = true;
                                            
                                            panelTotal.Visible = true;
                                            dgv.Visible = true;
                                            break;
                                    }
                                }
                                //20211021121136 furukawa ed ////////////////////////
                                break;
                        }

                        break;
                }
            }

            return true;
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {

            dgv.Columns[nameof(imp_chargedata.f000_importid)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.f001_insnum)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f002_insname)].Width = 100;
            dgv.Columns[nameof(imp_chargedata.f003_hihonum)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f004_shikyukind)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.f005_shikyukindnum)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.f006_mediym)].Width = 60;
            dgv.Columns[nameof(imp_chargedata.f007_ketteiymd)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f008_ketteiymdw)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f009_shishutsuymd)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f010_shishutsuymdw)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f011_charge)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f012_clinicnum)].Width = 100;
            dgv.Columns[nameof(imp_chargedata.f013_clinicname)].Width = 100;
            dgv.Columns[nameof(imp_chargedata.f014_hihoname)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f015_hihopref)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.f016_hihocity)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f017_hihoadd)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f018_hihozip)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.f019_destname)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f020_destpref)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f021_destcity)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.f022_destadd)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f023_destzip)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f024_clinicnum)].Width = 100;
            dgv.Columns[nameof(imp_chargedata.f025_total)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f026_partial)].Width = 80;
            dgv.Columns[nameof(imp_chargedata.f027_biko)].Width = 40;
            dgv.Columns[nameof(imp_chargedata.cym)].Width = 60;

            dgv.Columns[nameof(imp_chargedata.f000_importid)].HeaderText = "インポートID";
            dgv.Columns[nameof(imp_chargedata.f001_insnum)].HeaderText = "保険者番号";
            dgv.Columns[nameof(imp_chargedata.f002_insname)].HeaderText = "保険者名";
            dgv.Columns[nameof(imp_chargedata.f003_hihonum)].HeaderText = "被保険者番号";
            dgv.Columns[nameof(imp_chargedata.f004_shikyukind)].HeaderText="支給種別（文言）"; 
            dgv.Columns[nameof(imp_chargedata.f005_shikyukindnum)].HeaderText = "支給整理番号";
            dgv.Columns[nameof(imp_chargedata.f006_mediym)].HeaderText = "診療年月";
            dgv.Columns[nameof(imp_chargedata.f007_ketteiymd)].HeaderText = "支給決定年月日";
            dgv.Columns[nameof(imp_chargedata.f008_ketteiymdw)].HeaderText = "支給決定年月日（日付形式）";
            dgv.Columns[nameof(imp_chargedata.f009_shishutsuymd)].HeaderText = "支出年月日";
            dgv.Columns[nameof(imp_chargedata.f010_shishutsuymdw)].HeaderText = "支出年月日（日付形式）";
            dgv.Columns[nameof(imp_chargedata.f011_charge)].HeaderText = "支給金額";
            dgv.Columns[nameof(imp_chargedata.f012_clinicnum)].HeaderText = "支払先医療機関コード";
            dgv.Columns[nameof(imp_chargedata.f013_clinicname)].HeaderText = "支払先医療機関名称";
            dgv.Columns[nameof(imp_chargedata.f014_hihoname)].HeaderText = "氏名（漢字）";
            dgv.Columns[nameof(imp_chargedata.f015_hihopref)].HeaderText = "現住所ー都道府県名（漢字）";
            dgv.Columns[nameof(imp_chargedata.f016_hihocity)].HeaderText = "現住所ー市区町村名（漢字）";
            dgv.Columns[nameof(imp_chargedata.f017_hihoadd)].HeaderText = "現住所ー住所（漢字）";
            dgv.Columns[nameof(imp_chargedata.f018_hihozip)].HeaderText = "〒";
            dgv.Columns[nameof(imp_chargedata.f019_destname)].HeaderText = "送付先氏名（漢字）";
            dgv.Columns[nameof(imp_chargedata.f020_destpref)].HeaderText = "送付先住所ー都道府県名（漢字）";
            dgv.Columns[nameof(imp_chargedata.f021_destcity)].HeaderText = "送付先住所ー市区町村名（漢字）";
            dgv.Columns[nameof(imp_chargedata.f022_destadd)].HeaderText = "送付先住所（漢字）";
            dgv.Columns[nameof(imp_chargedata.f023_destzip)].HeaderText = "送付先郵便番号";
            dgv.Columns[nameof(imp_chargedata.f024_clinicnum)].HeaderText = "医療機関コード";
            dgv.Columns[nameof(imp_chargedata.f025_total)].HeaderText = "費用額";
            dgv.Columns[nameof(imp_chargedata.f026_partial)].HeaderText = "一部負担額";
            dgv.Columns[nameof(imp_chargedata.f027_biko)].HeaderText = "備考（漢字）";
            dgv.Columns[nameof(imp_chargedata.cym)].HeaderText = "メホール請求年月";


            dgv.Columns[nameof(imp_chargedata.f000_importid)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f001_insnum)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f002_insname)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f003_hihonum)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f004_shikyukind)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f005_shikyukindnum)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f006_mediym)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f007_ketteiymd)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f008_ketteiymdw)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f009_shishutsuymd)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f010_shishutsuymdw)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f011_charge)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f012_clinicnum)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f013_clinicname)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f014_hihoname)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f015_hihopref)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f016_hihocity)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f017_hihoadd)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f018_hihozip)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f019_destname)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f020_destpref)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f021_destcity)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f022_destadd)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f023_destzip)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.f024_clinicnum)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f025_total)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f026_partial)].Visible = true;
            dgv.Columns[nameof(imp_chargedata.f027_biko)].Visible = false;
            dgv.Columns[nameof(imp_chargedata.cym)].Visible = true;


        }


        /// <summary>
        /// 提供情報グリッド
        /// </summary>
        private void createGrid(App app=null)
        {

            List<imp_chargedata> lstimp = new List<imp_chargedata>();


            //string strhmark = verifyBoxHmark.Text.Trim();
            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetInt100Value()  + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

    
            foreach (imp_chargedata item in lstChargeData)
            {
                //合致したレコードをグリッドに入れる
                if (item.f006_mediym == intymad.ToString() &&
                    item.f003_hihonum == strhnum &&
                    item.f025_total == intTotal.ToString())

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;

            //0件の場合グリッド作らない
            if (lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
                return;
            }

            dgv.DataSource = lstimp;

            InitGrid();

            
            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();


            if (app!=null && app.RrID!=0)                
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {                                        
                    if (r.Cells["f000_importid"].Value.ToString() == app.RrID.ToString())
                    {
                        r.Selected = true;
                    }

                    //提供データIDと違う場合の処理抜け
                    else
                    {
                        r.Selected = false;
                    }
                 
                }
            }


       

            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }

            
            //マッチングOK条件に選択行が1行の場合も追加
            
            else if (lstimp.Count == 1 || dgv.SelectedRows.Count==1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        #endregion



        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            ////表示初期化
            //phnum.Visible = false;
            //panelTotal.Visible = false;            
            //pZenkai.Enabled = false;
            //dgv.Visible = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = 
                "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

          //  InitControl(scan.AppType);


            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証番号
                    setValue(verifyBoxHnum, Microsoft.VisualBasic.Strings.StrConv(app.HihoNum,VbStrConv.Narrow), firstTime, nv);

               
                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE,verifyBoxF1FirstD);

                    //負傷名1
                    setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
              
                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //実日数
                    setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                    //往療あり
                    checkBoxVisit.Checked = app.Distance == 0 ? false : true;
                    //往療加算
                    checkBoxVisitKasan.Checked = app.VisitAdd == 0 ? false : true;

                    //交付料あり
                    checkBoxKofu.Checked = app.TaggedDatas.flgKofuUmu;

                    //グリッド選択する
                    createGrid(app);

                    break;
            }
        }


        #endregion

        #region 各種更新

        private bool RegistImportData(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {

                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(imp_chargedata.f000_importid)].Value.ToString()); //ID


                    //被保険者マスタの個人情報を取得
                    imp_hihomaster hiho = new imp_hihomaster();

                    //20220412133858 furukawa st ////////////////////////
                    //件数が100万と多いため初期ロードを中止、登録時に抽出に変更
                    
                    hiho = imp_hihomaster.select(app.CYM, app.HihoNum);

                    //  foreach(imp_hihomaster item in lstHihoMaster_pd)
                    //  {
                    //      if (item.f001_hihonum == app.HihoNum)
                    //      //if (item.f000_importid == app.RrID)
                    //      {
                    //          hiho.f000_importid = app.RrID;
                    //          hiho.f001_hihonum = item.f001_hihonum;
                    //          hiho.f002_hihokana = item.f002_hihokana;
                    //          hiho.birthad = item.birthad;
                    //          hiho.f004_hihogender = item.f004_hihogender;
                    //          hiho.f005_setainum1 = item.f005_setainum1;
                    //          break;
                    //      }
                    //  }
                    //20220412133858 furukawa ed ////////////////////////


                    //入力を優先させる                        
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                      //被保険者証番号

                    app.HihoName= dgv.CurrentRow.Cells[nameof(imp_chargedata.f014_hihoname)].Value.ToString();  //被保険者名
                    app.InsNum = dgv.CurrentRow.Cells[nameof(imp_chargedata.f001_insnum)].Value.ToString();  //保険者番号
                    app.PersonName = dgv.CurrentRow.Cells[nameof(imp_chargedata.f014_hihoname)].Value.ToString();  //千葉広域あはきの場合、受療者＝被保険者

                    app.Sex = int.Parse(hiho.f004_hihogender);                                                   //性別
                    app.Birthday = hiho.birthad;                                                                 //生年月日西暦

                    app.ClinicName = dgv.CurrentRow.Cells[nameof(imp_chargedata.f013_clinicname)].Value.ToString();         //医療機関名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(imp_chargedata.f024_clinicnum)].Value.ToString();           //医療機関番号
                    
                    app.Total = int.Parse(dgv.CurrentRow.Cells[nameof(imp_chargedata.f025_total)].Value.ToString());        //合計
                    app.Charge = int.Parse(dgv.CurrentRow.Cells[nameof(imp_chargedata.f011_charge)].Value.ToString());      //請求
                    app.Partial = int.Parse(dgv.CurrentRow.Cells[nameof(imp_chargedata.f026_partial)].Value.ToString());    //一部負担金
                    app.TaggedDatas.GeneralString1 = hiho.f005_setainum1.ToString();                                        //世帯番号

                    //20220412134809 furukawa st ////////////////////////
                    //カナ追加    

                    //20220701141410 furukawa st ////////////////////////
                    //後ろスペース取る
                    
                    app.TaggedDatas.Kana = hiho.f002_hihokana.Trim();
                    //      app.TaggedDatas.Kana = hiho.f002_hihokana;
                    //20220701141410 furukawa ed ////////////////////////

                    //20220412134809 furukawa ed ////////////////////////

                    //20220701132509 furukawa st ////////////////////////
                    //null判定では空文字時が拾えない

                    string pref = dgv.CurrentRow.Cells[nameof(imp_chargedata.f015_hihopref)].Value == null ? string.Empty : dgv.CurrentRow.Cells[nameof(imp_chargedata.f015_hihopref)].Value.ToString().Trim();
                    string city = dgv.CurrentRow.Cells[nameof(imp_chargedata.f016_hihocity)].Value == null ? string.Empty : dgv.CurrentRow.Cells[nameof(imp_chargedata.f016_hihocity)].Value.ToString().Trim();
                    string hihoadd = dgv.CurrentRow.Cells[nameof(imp_chargedata.f017_hihoadd)].Value == null ? string.Empty : dgv.CurrentRow.Cells[nameof(imp_chargedata.f017_hihoadd)].Value.ToString().Trim();

                    app.HihoAdd = pref + city + hihoadd;

                    //app.HihoAdd =
                    //    dgv.CurrentRow.Cells[nameof(imp_chargedata.f015_hihopref)].Value!=null ? dgv.CurrentRow.Cells[nameof(imp_chargedata.f015_hihopref)].Value.ToString().Trim():string.Empty +
                    //    dgv.CurrentRow.Cells[nameof(imp_chargedata.f016_hihocity)].Value!=null ? dgv.CurrentRow.Cells[nameof(imp_chargedata.f016_hihocity)].Value.ToString().Trim():string.Empty +
                    //    dgv.CurrentRow.Cells[nameof(imp_chargedata.f017_hihoadd)].Value!=null ? dgv.CurrentRow.Cells[nameof(imp_chargedata.f017_hihoadd)].Value.ToString().Trim():string.Empty;                  //被保険者住所
                    //20220701132509 furukawa ed ////////////////////////


                    app.HihoZip= dgv.CurrentRow.Cells[nameof(imp_chargedata.f018_hihozip)].Value.ToString().Trim();         //被保険者郵便番号

                    //20220701132629 furukawa st ////////////////////////
                    //null判定では空文字時が拾えない
                    
                    string destpref = dgv.CurrentRow.Cells[nameof(imp_chargedata.f020_destpref)].Value == null ? string.Empty : dgv.CurrentRow.Cells[nameof(imp_chargedata.f020_destpref)].Value.ToString().Trim();
                    string destcity = dgv.CurrentRow.Cells[nameof(imp_chargedata.f021_destcity)].Value == null ? string.Empty : dgv.CurrentRow.Cells[nameof(imp_chargedata.f021_destcity)].Value.ToString().Trim();
                    string destadd = dgv.CurrentRow.Cells[nameof(imp_chargedata.f022_destadd)].Value == null ? string.Empty : dgv.CurrentRow.Cells[nameof(imp_chargedata.f022_destadd)].Value.ToString().Trim();

                    app.TaggedDatas.DestAdd = destpref + destcity + destadd;

                    //app.TaggedDatas.DestAdd =
                    //    dgv.CurrentRow.Cells[nameof(imp_chargedata.f020_destpref)].Value!=null ? dgv.CurrentRow.Cells[nameof(imp_chargedata.f020_destpref)].Value.ToString().Trim() : string.Empty +
                    //    dgv.CurrentRow.Cells[nameof(imp_chargedata.f021_destcity)].Value!=null ? dgv.CurrentRow.Cells[nameof(imp_chargedata.f021_destcity)].Value.ToString().Trim() :string.Empty+
                    //    dgv.CurrentRow.Cells[nameof(imp_chargedata.f022_destadd)].Value !=null ? dgv.CurrentRow.Cells[nameof(imp_chargedata.f022_destadd)].Value.ToString().Trim():string.Empty;                    //送付先住所                        
                    //20220701132629 furukawa ed ////////////////////////


                    app.TaggedDatas.DestZip = dgv.CurrentRow.Cells[nameof(imp_chargedata.f023_destzip)].Value.ToString();       //送付先郵便番号

                    //20220701141500 furukawa st ////////////////////////
                    //後ろスペース取る                    
                    app.TaggedDatas.DestName = dgv.CurrentRow.Cells[nameof(imp_chargedata.f019_destname)].Value.ToString().Trim();     //送付先氏名
                    //      app.TaggedDatas.DestName = dgv.CurrentRow.Cells[nameof(imp_chargedata.f019_destname)].Value.ToString();     //送付先氏名
                    //20220701141500 furukawa ed ////////////////////////


                    //拡張入力でやる
                    //app.AccountNumber = dgv.CurrentRow.Cells[nameof(imp_chargedata.f102accnumber)].Value.ToString();                          //口座番号
                    //app.AccountKana = dgv.CurrentRow.Cells[nameof(imp_chargedata.f103acckana)].Value.ToString();                              //口座カナ
                    //app.AccountName = dgv.CurrentRow.Cells[nameof(imp_chargedata.f104acckanji)].Value.ToString();                             //口座名


                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells[nameof(imp_chargedata.f006_mediym)].Value.ToString().Substring(0, 4)) &&
                        app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells[nameof(imp_chargedata.f006_mediym)].Value.ToString().Substring(4, 2)))
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }

                    
                }
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;
            }

        }

        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証番号   
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);

                    //負傷名1
                    string f1 = verifyBoxF1.Text.Trim();
                    
                    //20220412134109 furukawa st ////////////////////////
                    //負傷名1は鍼のみチェック
                    
                    if (scan.AppType == APP_TYPE.鍼灸)
                    {
                        setStatus(verifyBoxF1, f1.Trim() == string.Empty);
                    }
                    //20220412134109 furukawa ed ////////////////////////


                    //初検日1
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM,verifyBoxF1FirstD);

                    //実日数
                    int counteddays = verifyBoxCountedDays.GetIntValue();
                    setStatus(verifyBoxCountedDays, counteddays < 1);

                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                   
                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月

                    app.HihoNum =hnumN;                //被保険者証番号                                       

                    //申請書種別
                    app.AppType = scan.AppType;

                    //初検日
                    app.FushoFirstDate1 = f1FirstDt;

                    //負傷名1 
                    if (verifyBoxF1.Visible) app.FushoName1 = verifyBoxF1.Text.Trim();
                
                    //合計
                    app.Total = total;

                    //実日数
                    app.CountedDays = counteddays;

                    //往療あり
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;
                    //往療加算
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;
                    //交付料あり
                    app.TaggedDatas.flgKofuUmu = checkBoxKofu.Checked;

                    //提供データの適用
                    if (!RegistImportData(app)) return false;


                    if (app.AppType == APP_TYPE.柔整 || app.AppType == APP_TYPE.あんま || app.AppType == APP_TYPE.鍼灸)
                    {

                        if (dgv.Rows.Count == 0 || dgv.SelectedRows.Count==0)
                            
                        {
                            if (MessageBox.Show("マッチングなしですがよろしいですか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }
                    

                    #endregion


                    break;

            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            createGrid();
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
   

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
            //else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
