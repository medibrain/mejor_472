﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.ChibaKoikiAHK

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName_anmamaster = frm.strFileAnmaMaster;
            string strFileName_harimaster = frm.strFileHariMaster;
            string strFileName_hihomaster = frm.strFileHihoMaster;
            string strFileName_sikyu = frm.strFileSikyuData;

            if (strFileName_anmamaster == string.Empty &&
                strFileName_harimaster == string.Empty &&
                strFileName_hihomaster == string.Empty &&
                strFileName_sikyu == string.Empty) return;



            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                
                if (strFileName_anmamaster != string.Empty)
                {
                    wf.LogPrint("提供データ_施術所マスタあんまインポート");
                    if (!dataImport_AHK.dataImport_AHK_ClinicMaster(_cym,9, wf, strFileName_anmamaster))
                    {
                        wf.LogPrint("提供データ_施術所マスタあんまインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_施術所マスタあんまインポート失敗",
                            System.Windows.Forms.Application.ProductName,
                            System.Windows.Forms.MessageBoxButtons.OK,
                            System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_施術所マスタあんまインポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_施術所マスタあんまのファイルが指定されていないため処理しない");
                }

                if (strFileName_harimaster != string.Empty)
                {
                    wf.LogPrint("提供データ_施術所マスタ鍼灸インポート");
                    if (!dataImport_AHK.dataImport_AHK_ClinicMaster(_cym,7, wf, strFileName_harimaster))
                    {
                        wf.LogPrint("提供データ_施術所マスタ鍼灸インポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_施術所マスタ鍼灸インポート失敗"
                            ,System.Windows.Forms.Application.ProductName
                            ,System.Windows.Forms.MessageBoxButtons.OK
                            ,System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_施術所マスタ鍼灸インポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_施術所マスタ鍼灸のファイルが指定されていないため処理しない");
                }


                if (strFileName_hihomaster != string.Empty)
                {
                    wf.LogPrint("提供データ_被保険者マスタインポート");
                    if (!dataImport_AHK.dataImport_AHK_HihoMaster(_cym, wf, strFileName_hihomaster))
                    {
                        wf.LogPrint("提供データ_被保険者マスタインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_被保険者マスタインポート失敗"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_被保険者マスタインポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_被保険者マスタのファイルが指定されていないため処理しない");
                }



                if (strFileName_sikyu != string.Empty)
                {
                    wf.LogPrint("提供データ_支給決定データインポート");
                    if (!dataImport_AHK.dataImport_AHK_ChargeData(_cym,  wf, strFileName_sikyu))
                    {
                        wf.LogPrint("提供データ_支給決定データインポート失敗");
                        System.Windows.Forms.MessageBox.Show("提供データ_支給決定データインポート失敗"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    wf.LogPrint("提供データ_支給決定データインポート終了");
                }
                else
                {
                    wf.LogPrint("提供データ_支給決定データのファイルが指定されていないため処理しない");
                }



                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name +  "\r\n" + ex.Message);
                return;
            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    partial class dataImport_AHK
    {
   
        /// <summary>
        /// Excel
        /// </summary>
        static NPOI.XSSF.UserModel.XSSFWorkbook wb;


        static NPOI.HSSF.UserModel.HSSFWorkbook wbhs;
        
        /// <summary>
        /// あんま・鍼灸施術所マスタインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_ClinicMaster(int _cym, int tensuhyo,WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            int intcym = _cym;

            
            //xlsファイル
            System.IO.FileStream fs = new System.IO.FileStream(strFileName,System.IO.FileMode.Open);
            wbhs = new NPOI.HSSF.UserModel.HSSFWorkbook(fs);
            NPOI.SS.UserModel.ISheet ws = wbhs.GetSheet("検索用");


            //wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            //NPOI.SS.UserModel.ISheet ws = wb.GetSheet("検索用");


            if (ws == null)
            {
                System.Windows.Forms.MessageBox.Show("「検索用」シートがありません。取り込めません。ファイルの中身を確認してください"
                      , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                return false;
            }

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_clinicmaster where cym={intcym} and f003_tensuhyo='{tensuhyo}'", tran);
            if (!cmd.TryExecuteNonQuery()) return false;

            
            int col = 0;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし

                //最終行を最大値とする                
                wf.SetMax(ws.LastRowNum);

                //列数確認
                NPOI.SS.UserModel.IRow rowheader = ws.GetRow(0);
                if (rowheader.Cells.Count - 1 != 17)
                {
                    System.Windows.Forms.MessageBox.Show("列数が17ではありません。取り込めません。ファイルの中身を確認してください"
                        , System.Windows.Forms.Application.ProductName
                        , System.Windows.Forms.MessageBoxButtons.OK
                        , System.Windows.Forms.MessageBoxIcon.Exclamation);


                    return false;
                }

                //登録しない行はスキップ
                bool flgskip = false;

                for (int r = 0; r <= ws.LastRowNum; r++)
                {
                    imp_clinicmaster imp = new imp_clinicmaster();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);              

                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    


                    for (int c = 0; c < row.LastCellNum; c++)
                    {

                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //2列目が数値に変換できない場合は見出し行と見なし飛ばす                        
                        //ループを抜けないと行が終わらない                        
                        if (c == 1)
                        {
                            if (!int.TryParse(getCellValueToString(cell).ToString(), out int tmp))

                            {
                                flgskip = true;
                                break;
                            }
                            else
                            {
                                flgskip = false;
                            }
                        }




                        #region 値の取込

                        if (c == 0) imp.f001_clinicnum = getCellValueToString(cell).ToString().PadLeft(10,'0');                 //医療機関コード;
                        if (c == 1) imp.f002_prefno = getCellValueToString(cell);                    //県ｺｰﾄﾞ;
                        if (c == 2) imp.f003_tensuhyo = getCellValueToString(cell);                  //点数表№;
                        if (c == 3) imp.f004_cityno = getCellValueToString(cell);                    //市町村別連番;
                        if (c == 4) imp.f005_clinictel = getCellValueToString(cell);                 //医療機関電話番号;
                        if (c == 5) imp.f006_clinickana = getCellValueToString(cell);                //医療機関名（全角カナ）;
                        if (c == 6) imp.f007_clinicname = getCellValueToString(cell);                //医療機関名（漢字）;
                        if (c == 7) imp.f008_cliniczip = getCellValueToString(cell);                 //医療機関郵便番号;
                        if (c == 8) imp.f009_clinicadd = getCellValueToString(cell);                 //医療機関住所（漢字）;
                        if (c == 9) imp.f010_bankcode = getCellValueToString(cell).ToString().PadLeft(4, '0');                  //金融機関コード;
                        if (c == 10) imp.f011_bankshopcode = getCellValueToString(cell).ToString().PadLeft(3, '0');             //金融機関店舗コード;
                        if (c == 11) imp.f012_kind = getCellValueToString(cell);                     //預金種別コード;
                        if (c == 12) imp.f013_accountnum = getCellValueToString(cell).ToString().PadLeft(7, '0');               //口座番号;
                        if (c == 13) imp.f014_accountname = getCellValueToString(cell);              //口座名義人氏名（全角カナ）;
                        if (c == 14) imp.f015_dantaicode = getCellValueToString(cell).ToString() ==string.Empty ? string.Empty : getCellValueToString(cell).ToString().PadLeft(10, '0');               //団体コード;
                        if (c == 15) imp.f016_daihyo = getCellValueToString(cell);                   //代表者名;
                        if (c == 16) imp.f017_opendate = getCellValueToString(cell);                 //開設日;
                        if (c == 16) imp.cym = intcym;                                               //メホール請求年月;


                        #endregion


                    }

               
                    if (!flgskip)
                    {
                        wf.InvokeValue++;
                        wf.LogPrint($"医療機関コード: {imp.f001_clinicnum}");

                        if (!DB.Main.Insert<imp_clinicmaster>(imp, tran)) return false;
                    }

                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {
                wbhs.Close();
                //wb.Close();
                fs.Close();
            }

        }


        /// <summary>
        /// 支給決定ファイルインポート
        /// </summary>
        /// <param name="_cym"></param>
        /// <param name="wf"></param>
        /// <param name="strFileName"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_ChargeData(int _cym,  WaitForm wf, string strFileName)
        {

            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            int intcym = _cym;

            //20220426154307 furukawa st ////////////////////////
            //シート名チェックを提出用データに変更　2022/04/26林田さん
            
            wb = new NPOI.XSSF.UserModel.XSSFWorkbook(strFileName);
            NPOI.SS.UserModel.ISheet ws = wb.GetSheet("提供用データ");
            if (ws == null)
            {
                System.Windows.Forms.MessageBox.Show("「提供用データ」シートがありません。取り込めません。ファイルの中身を確認してください"
                      , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                return false;
            }

            //NPOI.SS.UserModel.ISheet ws = wb.GetSheet("支給決定(全部)");
            //if (ws == null)
            //{
            //    System.Windows.Forms.MessageBox.Show("「支給決定(全部)」シートがありません。取り込めません。ファイルの中身を確認してください"
            //          , System.Windows.Forms.Application.ProductName
            //                , System.Windows.Forms.MessageBoxButtons.OK
            //                , System.Windows.Forms.MessageBoxIcon.Exclamation);

            //    return false;
            //}
            //20220426154307 furukawa ed ////////////////////////

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_chargedata where cym={intcym} ", tran);
            cmd.TryExecuteNonQuery();

            wf.InvokeValue = 0;
            wf.LogPrint($"{intcym}削除");

            int col = 0;

            try
            {
                wf.LogPrint("データ取得");

                //npoiで開きデータ取りだし
                //最終行を最大値とする                
                wf.SetMax(ws.LastRowNum);

                //登録しない行はスキップ
                bool flgskip = false;

                NPOI.SS.UserModel.IRow rowheader = ws.GetRow(0);
                if (rowheader.Cells.Count != 27)
                {
                    System.Windows.Forms.MessageBox.Show("列数が27ではありません。取り込めません。ファイルの中身を確認してください"
                        , System.Windows.Forms.Application.ProductName
                        , System.Windows.Forms.MessageBoxButtons.OK
                        , System.Windows.Forms.MessageBoxIcon.Exclamation);

                    return false;
                }


                for (int r = 0; r <= ws.LastRowNum; r++)
                {
                    //3行目からデータ
                    if (r < 2) continue;

                    imp_chargedata imp = new imp_chargedata();

                    NPOI.SS.UserModel.IRow row = ws.GetRow(r);
                  
                    imp.cym = intcym;//	メホール上の処理年月 メホール管理用    


                    for (int c = 0; c < row.LastCellNum; c++)
                    {

                        if (CommonTool.WaitFormCancelProcess(wf)) return false;

                        NPOI.SS.UserModel.ICell cell = row.GetCell(c, NPOI.SS.UserModel.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        //2列目が数値に変換できない場合は見出し行と見なし飛ばす                        
                        //ループを抜けないと行が終わらない                        
                        if (c == 10)
                        {
                            if (!int.TryParse(getCellValueToString(cell).ToString(), out int tmp))
                            {
                                flgskip = true;
                                break;
                            }
                            else
                            {
                                flgskip = false;
                            }
                        }


                        //20220701181236 furukawa st ////////////////////////
                        //後ろスペース取る
                        
                        if (c == 0) imp.f001_insnum = getCellValueToString(cell).Trim();
                        if (c == 1) imp.f002_insname = getCellValueToString(cell).Trim();
                        if (c == 2) imp.f003_hihonum = getCellValueToString(cell).Trim();
                        if (c == 3) imp.f004_shikyukind = getCellValueToString(cell).Trim();
                        if (c == 4) imp.f005_shikyukindnum = getCellValueToString(cell).Trim();
                        if (c == 5) imp.f006_mediym = getCellValueToString(cell).Trim();
                        if (c == 6) imp.f007_ketteiymd = getCellValueToString(cell).Trim();
                        if (c == 7) imp.f008_ketteiymdw = getCellValueToString(cell).Trim();
                        if (c == 8) imp.f009_shishutsuymd = getCellValueToString(cell).Trim();
                        if (c == 9) imp.f010_shishutsuymdw = getCellValueToString(cell).Trim();
                        if (c == 10) imp.f011_charge = getCellValueToString(cell).Trim();
                        if (c == 11) imp.f012_clinicnum = getCellValueToString(cell).Trim();
                        if (c == 12) imp.f013_clinicname = getCellValueToString(cell).Trim();
                        if (c == 13) imp.f014_hihoname = getCellValueToString(cell).Trim();
                        if (c == 14) imp.f015_hihopref = getCellValueToString(cell).Trim();
                        if (c == 15) imp.f016_hihocity = getCellValueToString(cell).Trim();
                        if (c == 16) imp.f017_hihoadd = getCellValueToString(cell).Trim();
                        if (c == 17) imp.f018_hihozip = getCellValueToString(cell).Trim();
                        if (c == 18) imp.f019_destname = getCellValueToString(cell).Trim();
                        if (c == 19) imp.f020_destpref = getCellValueToString(cell).Trim();
                        if (c == 20) imp.f021_destcity = getCellValueToString(cell).Trim();
                        if (c == 21) imp.f022_destadd = getCellValueToString(cell).Trim();
                        if (c == 22) imp.f023_destzip = getCellValueToString(cell).Trim();
                        if (c == 23) imp.f024_clinicnum = getCellValueToString(cell).Trim();
                        if (c == 24) imp.f025_total = getCellValueToString(cell).Trim();
                        if (c == 25) imp.f026_partial = getCellValueToString(cell).Trim();
                        if (c == 26) imp.f027_biko = getCellValueToString(cell).Trim();

                        //      if (c == 0) imp.f001_insnum = getCellValueToString(cell);
                        //      if (c == 1) imp.f002_insname = getCellValueToString(cell);
                        //      if (c == 2) imp.f003_hihonum = getCellValueToString(cell);
                        //      if (c == 3) imp.f004_shikyukind = getCellValueToString(cell);
                        //      if (c == 4) imp.f005_shikyukindnum = getCellValueToString(cell);
                        //      if (c == 5) imp.f006_mediym = getCellValueToString(cell);
                        //      if (c == 6) imp.f007_ketteiymd = getCellValueToString(cell);
                        //      if (c == 7) imp.f008_ketteiymdw = getCellValueToString(cell);
                        //      if (c == 8) imp.f009_shishutsuymd = getCellValueToString(cell);
                        //      if (c == 9) imp.f010_shishutsuymdw = getCellValueToString(cell);
                        //      if (c == 10) imp.f011_charge = getCellValueToString(cell);
                        //      if (c == 11) imp.f012_clinicnum = getCellValueToString(cell);
                        //      if (c == 12) imp.f013_clinicname = getCellValueToString(cell);
                        //      if (c == 13) imp.f014_hihoname = getCellValueToString(cell);
                        //      if (c == 14) imp.f015_hihopref = getCellValueToString(cell);
                        //      if (c == 15) imp.f016_hihocity = getCellValueToString(cell);
                        //      if (c == 16) imp.f017_hihoadd = getCellValueToString(cell);
                        //      if (c == 17) imp.f018_hihozip = getCellValueToString(cell);
                        //      if (c == 18) imp.f019_destname = getCellValueToString(cell);
                        //      if (c == 19) imp.f020_destpref = getCellValueToString(cell);
                        //      if (c == 20) imp.f021_destcity = getCellValueToString(cell);
                        //      if (c == 21) imp.f022_destadd = getCellValueToString(cell);
                        //      if (c == 22) imp.f023_destzip = getCellValueToString(cell);
                        //      if (c == 23) imp.f024_clinicnum = getCellValueToString(cell);
                        //      if (c == 24) imp.f025_total = getCellValueToString(cell);
                        //      if (c == 25) imp.f026_partial = getCellValueToString(cell);
                        //      if (c == 26) imp.f027_biko = getCellValueToString(cell);
                        //20220701181236 furukawa ed ////////////////////////

                        if (c == 26) imp.cym = intcym;
                        

                    }

                    wf.InvokeValue++;
                    wf.LogPrint($"支給決定データインポート 被保険者証番号: {imp.f003_hihonum}");

                    if(!flgskip) if (!DB.Main.Insert<imp_chargedata>(imp, tran)) return false;
                }

                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + col + "\r\n" + ex.Message);
                tran.Rollback();
                return false;
            }
            finally
            {

                wb.Close();
            }

        }


        /// <summary>
        /// copyコマンドでインポート
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="cym"></param>
        /// <returns></returns>
        private static bool DirectImport(string strFileName,int cym)
        {
            StringBuilder sb = new StringBuilder();
            sb.Clear();
            sb.AppendLine($"copy imp_hihomaster(");
            sb.AppendLine($"f001_hihonum, f002_hihokana, f003_hihobirth, f004_hihogender, f005_setainum1, ");
            sb.AppendLine($"f006_getdate1, f007_losedate1, f008_insnum1, f009_futan1, f010_tekiyodate1, f011_futan2, f012_tekiyodate2 )");
            sb.AppendLine($" from ");
            sb.AppendLine($" stdin with csv NULL AS ' ' encoding 'utf-8'  ");
           
            if (!CommonTool.CsvDirectImportToTable(strFileName, sb.ToString()))
            {
                System.Windows.Forms.MessageBox.Show("CSV取込失敗",
                    System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }

            return true;

        }


        /// <summary>
        /// 被保険者マスタ
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        public static bool dataImport_AHK_HihoMaster(int _cym, WaitForm wf,string strFileName)
        {
            int intcym = _cym;

            //20220411101143 furukawa st ////////////////////////
            //copyインポート時に不具合が起きるためutf8以外受け付けない
            
            System.Text.Encoding e = CommonTool.CSVEncodingCheck(strFileName);
            if (e != System.Text.Encoding.UTF8)
            {
                System.Windows.Forms.MessageBox.Show("文字コードがUTF8ではありません。変換してください",
                      System.Windows.Forms.Application.ProductName,
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Exclamation);
                return false;
            }
            //20220411101143 furukawa ed ////////////////////////

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from imp_hihomaster where cym={intcym}", tran);
            wf.LogPrint($"{intcym}削除");
            cmd.TryExecuteNonQuery();

            

            try
            {
                //20220411101045 furukawa st ////////////////////////
                //件数が100万と多いためcopyでインポートに変更
                

                wf.LogPrint($"{intcym}CSVインポート");
                if(!DirectImport(strFileName, _cym))return false;
       
                //あまりやりたくないが100万と件数が多いためsqlで即席の和暦西暦変換
                wf.LogPrint($"{intcym}生年月日変換中...");
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($" update imp_hihomaster set birthad=");
                sb.AppendLine($" case substr(f003_hihobirth,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f003_hihobirth,2,2) as int) || '-' || substr(f003_hihobirth,4,2) || '-' || substr(f003_hihobirth,6,2) as date)");
                sb.AppendLine($" else cast('0001-01-01' as date) end,");
                sb.AppendLine($" getdatead1=");
                sb.AppendLine($" case substr(f006_getdate1,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f006_getdate1,2,2) as int) || '-' || substr(f006_getdate1,4,2) || '-' || substr(f006_getdate1,6,2) as date)");
                sb.AppendLine($" else cast('0001-01-01' as date) end,");
                sb.AppendLine($" losedatead1=");
                sb.AppendLine($" case substr(f007_losedate1,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f007_losedate1,2,2) as int) || '-' || substr(f007_losedate1,4,2) || '-' || substr(f007_losedate1,6,2) as date)");
                sb.AppendLine($" else cast('0001-01-01' as date) end,");
                sb.AppendLine($" tekiyodatead1=");
                sb.AppendLine($" case substr(f010_tekiyodate1,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f010_tekiyodate1,2,2) as int) || substr(f010_tekiyodate1,4,2) as int)");
                sb.AppendLine($" else 0 end,");
                sb.AppendLine($" tekiyodatead2=");
                sb.AppendLine($" case substr(f012_tekiyodate2,1,1)");
                sb.AppendLine($" when '2' then cast(1911+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" when '3' then cast(1925+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" when '4' then cast(1988+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" when '5' then cast(2018+cast(substr(f012_tekiyodate2,2,2) as int) || substr(f012_tekiyodate2,4,2) as int)");
                sb.AppendLine($" else 0 end, ");

                sb.AppendLine($" cym={intcym} ");

                sb.AppendLine($" where cym=0");

                cmd = DB.Main.CreateCmd(sb.ToString(), tran);
                cmd.TryExecuteNonQuery();


                #region old
                //CSV内容ロード
                //List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                //wf.LogPrint("CSVロード");
                // wf.SetMax(lst.Count);

                //wf.LogPrint($"{intcym}ロード");
                //cmd = DB.Main.CreateCmd($"select * from imp_hihomaster where cym={intcym}");
                //var lst = cmd.TryExecuteReaderList();
                //wf.SetMax(lst.Count);

                //////クラスのリストに登録
                //foreach (var tmp in lst)
                //{

                //    //if (tmp.Length != 12)
                //    //{
                //    //    System.Windows.Forms.MessageBox.Show("列数が12ではありません。取り込めません。ファイルの中身を確認してください"
                //    //       , System.Windows.Forms.Application.ProductName
                //    //       , System.Windows.Forms.MessageBoxButtons.OK
                //    //       , System.Windows.Forms.MessageBoxIcon.Exclamation);

                //    //    return false;
                //    //}


                //    //try
                //    //{
                //    //    int.Parse(tmp[11].ToString().Replace(",", ""));
                //    //}
                //    //catch
                //    //{
                //    //    wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                //    //    continue;
                //    //}

                //    imp_hihomaster cls = new imp_hihomaster();

                //    //cls.f001_hihonum = tmp[0];       //被保険者番号;
                //    //cls.f002_hihokana = tmp[1];       //カナ氏名;
                //    //cls.f003_hihobirth = tmp[2].ToString();       //生年月日;
                //    // cls.f004_hihogender = tmp[3];       //性別;
                //    // cls.f005_setainum1 = tmp[4];       //世帯番号１;
                //    //cls.f006_getdate1 = tmp[5].ToString();       //資格取得年月日１;
                //    //cls.f007_losedate1 = tmp[6].ToString();       //資格喪失年月日１;
                //    // cls.f008_insnum1 = tmp[7];       //保険者番号１;
                //    // cls.f009_futan1 = tmp[8];             //負担区分１;
                //    //cls.f010_tekiyodate1 = tmp[9].ToString();       //適用年月１;
                //    // cls.f011_futan2 = tmp[10];            //負担区分2;
                //    //cls.f012_tekiyodate2 = tmp[11].ToString();       //適用年月2;



                //    if (tmp[1].ToString() == string.Empty) continue;

                //    cls.f003_hihobirth = tmp[3].ToString();       //生年月日;                                                           
                //    cls.f006_getdate1 = tmp[6].ToString();       //資格取得年月日１;
                //    cls.f007_losedate1 = tmp[7].ToString();       //資格喪失年月日１;
                //    cls.f010_tekiyodate1 = tmp[10].ToString();       //適用年月１;
                //    cls.f012_tekiyodate2 = tmp[12].ToString();       //適用年月2;


                //    //管理項目

                //    cls.cym = intcym;//メホール請求年月管理用;

                //    //適用年月西暦1;
                //    if (!int.TryParse(cls.f010_tekiyodate1, out int intTekiyodate1)) cls.tekiyodatead1 = 0;
                //    else cls.tekiyodatead1 = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f010_tekiyodate1));

                //    //適用年月西暦2;
                //    if (!int.TryParse(cls.f012_tekiyodate2, out int intTekiyodate2)) cls.tekiyodatead2 = 0;
                //    else cls.tekiyodatead2 = DateTimeEx.GetAdYearMonthFromJyymm(int.Parse(cls.f012_tekiyodate2));

                //    cls.birthad = DateTimeEx.GetDateFromJstr7(cls.f003_hihobirth);      //生年月日西暦;
                //    cls.getdatead1 = DateTimeEx.GetDateFromJstr7(cls.f006_getdate1);    //取得日西暦;
                //    cls.losedatead1 = DateTimeEx.GetDateFromJstr7(cls.f007_losedate1);  //喪失日西暦;


                //    if (!DB.Main.Update<imp_hihomaster>(cls, tran)) return false;
                //    //if (!DB.Main.Insert<imp_hihomaster>(cls, tran)) return false;


                //    wf.LogPrint($"被保険者マスタ  被保険者証番号: {tmp[1].ToString()}");
                //    wf.InvokeValue++;

                //}


                //wf.LogPrint("CSVロード終了");
                #endregion


                //20220411101045 furukawa ed ////////////////////////


                tran.Commit();
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                
            }

        }



        /// <summary>
        /// 提供データでAppを更新
        /// </summary>
        /// <param name="cym">メホール処理年月</param>
        /// <returns></returns>
        private static bool UpdateApp(int cym)
        {
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendLine(" update application set ");
            sb.AppendLine("  ayear			= cast(substr(ahk.f029shinryoym,2,2) as int) ");
            sb.AppendLine(" ,amonth			= cast(substr(ahk.f029shinryoym,4,2) as int) ");
            sb.AppendLine(" ,inum			= ahk.f012insnum	");
            sb.AppendLine(" ,hnum			= ahk.hihonum_narrow	");
            sb.AppendLine(" ,hname			= ahk.f019hihokanji	");
            sb.AppendLine(" ,psex			= cast(ahk.f020pgender	as int) ");
            sb.AppendLine(" ,pbirthday		= ahk.pbirthdayad	");
            sb.AppendLine(" ,afamily		= cast(ahk.f046family as int) ");
            sb.AppendLine(" ,aratio			= cast(ahk.f045ratio as int)/10		"); 

            sb.AppendLine(" ,istartdate1	= ahk.startdate1ad ");
            sb.AppendLine(" ,ifinishdate1	= ahk.finishdate1ad ");

            sb.AppendLine(" ,baccname		= ahk.f104acckanji	");
            sb.AppendLine(" ,bkana		    = ahk.f103acckana	");
            sb.AppendLine(" ,baccnumber		= ahk.f102accnumber	");

            sb.AppendLine(" ,atotal			= cast(ahk.f090total as int) ");
            sb.AppendLine(" ,apartial		= cast(ahk.f093partial as int) ");
            sb.AppendLine(" ,acharge		= cast(ahk.f092charge as int) ");

            sb.AppendLine(" ,aapptype		= case ahk.f043apptype ");
            sb.AppendLine($"                     when '04' then {(int)APP_TYPE.あんま} ");
            sb.AppendLine($"                     when '05' then {(int)APP_TYPE.鍼灸} ");
            sb.AppendLine(" end ");

            sb.AppendLine(" ,comnum			= ahk.f002comnum");

            sb.AppendLine(",taggeddatas");
            sb.AppendLine("       = 'GeneralString1:\"' || ahk.f021pnum || '\"' ");
            sb.AppendLine("       || 'GeneralString2:\"' || ahk.f022atenanum || '\"' ");

            sb.AppendLine(" from dataimport_ahk ahk  ");

            sb.AppendLine(" where ");
            sb.AppendLine($" application.comnum=ahk.f002comnum and application.cym={cym}");



            DB.Command cmd = new DB.Command(DB.Main, sb.ToString(),true);

            try
            {
                cmd.TryExecuteNonQuery();
                return true;

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// セルの値を取得
        /// </summary>
        /// <param name="cell">セル</param>
        /// <returns>string型</returns>
        private static string getCellValueToString(NPOI.SS.UserModel.ICell cell)
        {
            switch (cell.CellType)
            {                
                case NPOI.SS.UserModel.CellType.String:
                    return cell.StringCellValue;
                case NPOI.SS.UserModel.CellType.Numeric:
                    return cell.NumericCellValue.ToString();
                    
                case NPOI.SS.UserModel.CellType.Formula:
                    if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.String)
                    {
                        return cell.StringCellValue;
                    }
                    else if (cell.CachedFormulaResultType == NPOI.SS.UserModel.CellType.Numeric)
                    {
                        return cell.NumericCellValue.ToString();
                       
                    }
                    else return string.Empty;
                    
                default:
                    return string.Empty;

            }


        }



    }


}
