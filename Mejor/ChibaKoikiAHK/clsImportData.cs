﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mejor.ChibaKoikiAHK
{
   
    /// <summary>
    /// 施術所マスタ
    /// </summary>
    public partial class imp_clinicmaster
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int    f000_importid { get; set; } = 0;                        //インポートID;
        public string f001_clinicnum { get; set; } = string.Empty;            //医療機関コード;
        public string f002_prefno { get; set; } = string.Empty;               //県ｺｰﾄﾞ;
        public string f003_tensuhyo { get; set; } = string.Empty;             //点数表№;
        public string f004_cityno { get; set; } = string.Empty;               //市町村別連番;
        public string f005_clinictel { get; set; } = string.Empty;            //医療機関電話番号;
        public string f006_clinickana { get; set; } = string.Empty;           //医療機関名（全角カナ）;
        public string f007_clinicname { get; set; } = string.Empty;           //医療機関名（漢字）;
        public string f008_cliniczip { get; set; } = string.Empty;            //医療機関郵便番号;
        public string f009_clinicadd { get; set; } = string.Empty;            //医療機関住所（漢字）;
        public string f010_bankcode { get; set; } = string.Empty;             //金融機関コード;
        public string f011_bankshopcode { get; set; } = string.Empty;         //金融機関店舗コード;
        public string f012_kind { get; set; } = string.Empty;                 //預金種別コード;
        public string f013_accountnum { get; set; } = string.Empty;           //口座番号;
        public string f014_accountname { get; set; } = string.Empty;          //口座名義人氏名（全角カナ）;
        public string f015_dantaicode { get; set; } = string.Empty;           //団体コード;
        public string f016_daihyo { get; set; } = string.Empty;               //代表者名;
        public string f017_opendate { get; set; } = string.Empty;             //開設日;
        public int cym { get; set; } = 0;                                     //メホール請求年月;


        /// <summary>
        /// 全件取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<imp_clinicmaster> selectall(int cym)
        {
            List<imp_clinicmaster> lst = new List<imp_clinicmaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_clinicmaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_clinicmaster imp = new imp_clinicmaster();

                    imp.f001_clinicnum = item[1].ToString();
                    imp.f002_prefno = item[2].ToString();
                    imp.f003_tensuhyo = item[3].ToString();
                    imp.f004_cityno = item[4].ToString();
                    imp.f005_clinictel = item[5].ToString();
                    imp.f006_clinickana = item[6].ToString();
                    imp.f007_clinicname = item[7].ToString();
                    imp.f008_cliniczip = item[8].ToString();
                    imp.f009_clinicadd = item[9].ToString();
                    imp.f010_bankcode = item[10].ToString();
                    imp.f011_bankshopcode = item[11].ToString();
                    imp.f012_kind = item[12].ToString();
                    imp.f013_accountnum = item[13].ToString();
                    imp.f014_accountname = item[14].ToString();
                    imp.f015_dantaicode = item[15].ToString();
                    imp.f016_daihyo = item[16].ToString();
                    imp.f017_opendate = item[17].ToString();
                    imp.cym = int.Parse(item[18].ToString());


                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }


    }

    /// <summary>
    /// 被保険者マスタ
    /// </summary>
    public partial class imp_hihomaster
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                            //インポートID;
        public string f001_hihonum { get; set; } = string.Empty;               //被保険者番号;
        public string f002_hihokana { get; set; } = string.Empty;              //カナ氏名;
        public string f003_hihobirth { get; set; } = string.Empty;             //生年月日;
        public string f004_hihogender { get; set; } = string.Empty;            //性別;
        public string f005_setainum1 { get; set; } = string.Empty;             //世帯番号１;
        public string f006_getdate1 { get; set; } = string.Empty;              //資格取得年月日１;
        public string f007_losedate1 { get; set; } = string.Empty;             //資格喪失年月日１;
        public string f008_insnum1 { get; set; } = string.Empty;               //保険者番号１;
        public string f009_futan1 { get; set; } = string.Empty;                //負担区分１;
        public string f010_tekiyodate1 { get; set; } = string.Empty;           //適用年月１;
        public string f011_futan2 { get; set; } = string.Empty;                //負担区分2;
        public string f012_tekiyodate2 { get; set; } = string.Empty;           //適用年月2;
        public int cym { get; set; } = 0;                                      //メホール請求年月;
        public DateTime birthad { get; set; } = DateTime.MinValue;             //生年月日西暦;
        public DateTime getdatead1 { get; set; } = DateTime.MinValue;          //取得日西暦;
        public DateTime losedatead1 { get; set; } = DateTime.MinValue;         //喪失日西暦;
        public int tekiyodatead1 { get; set; } = 0;                            //適用年月西暦1;
        public int tekiyodatead2 { get; set; } = 0;                            //適用年月西暦2;

        /// <summary>
        /// 全件取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<imp_hihomaster> selectall(int cym)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_hihomaster where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();

                    imp.f001_hihonum = item[1].ToString();
                    imp.f002_hihokana = item[2].ToString();
                    imp.f003_hihobirth = item[3].ToString();
                    imp.f004_hihogender = item[4].ToString();
                    imp.f005_setainum1 = item[5].ToString();
                    imp.f006_getdate1 = item[6].ToString();
                    imp.f007_losedate1 = item[7].ToString();
                    imp.f008_insnum1 = item[8].ToString();
                    imp.f009_futan1 = item[9].ToString();
                    imp.f010_tekiyodate1 = item[10].ToString();
                    imp.f011_futan2 = item[11].ToString();
                    imp.f012_tekiyodate2 = item[12].ToString();
                    imp.cym = int.Parse(item[13].ToString());
                    imp.birthad = DateTime.Parse(item[14].ToString());
                    imp.getdatead1 = DateTime.Parse(item[15].ToString());
                    imp.losedatead1 = DateTime.Parse(item[16].ToString());
                    imp.tekiyodatead1 = int.Parse(item[17].ToString());
                    imp.tekiyodatead2 = int.Parse(item[18].ToString());


                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }

        /// <summary>
        /// 20220412133945 furukawa cymと被保険者証番号で1件抽出        
        /// </summary>
        /// <param name="cym"></param>
        /// <param name="strHihonum"></param>
        /// <returns></returns>
        public static imp_hihomaster select (int cym,string strHihonum)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($"  f001_hihonum");
            sb.AppendLine($" ,f002_hihokana");
            sb.AppendLine($" ,f003_hihobirth");
            sb.AppendLine($" ,f004_hihogender");
            sb.AppendLine($" ,f005_setainum1");
            sb.AppendLine($" ,cym");
            sb.AppendLine($" ,birthad");
            sb.AppendLine($" from imp_hihomaster where cym={cym} and f001_hihonum='{strHihonum}'");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();
                //20220412143820 furukawa st ////////////////////////
                //レコードがない場合最小値返し
                
                if (l.Count == 0)
                {
                    imp_hihomaster imp = new imp_hihomaster();
                    imp.f001_hihonum = string.Empty;
                    imp.f002_hihokana = string.Empty;
                    imp.f003_hihobirth = "0001-01-01";
                    imp.f004_hihogender = "0";
                    imp.f005_setainum1 = string.Empty;

                    imp.cym = cym;
                    imp.birthad = DateTime.MinValue;
                    return imp;
                }
                //20220412143820 furukawa ed ////////////////////////

                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();

                    imp.f001_hihonum = item[0].ToString();
                    imp.f002_hihokana = item[1].ToString();
                    imp.f003_hihobirth = item[2].ToString();
                    imp.f004_hihogender = item[3].ToString();
                    imp.f005_setainum1 = item[4].ToString();

                    imp.cym = int.Parse(item[5].ToString());
                    imp.birthad = DateTime.Parse(item[6].ToString());

                    lst.Add(imp);
                }
                return (imp_hihomaster)lst[0];
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
            
        }

        /// <summary>
        /// 資格情報を除いた個人情報部だけを取得
        /// </summary>
        /// <param name="cym"></param>
        /// <returns></returns>
        public static List<imp_hihomaster> selectpd(int cym)
        {
            List<imp_hihomaster> lst = new List<imp_hihomaster>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select  ");
            sb.AppendLine($"  f001_hihonum");
            sb.AppendLine($" ,f002_hihokana");
            sb.AppendLine($" ,f003_hihobirth");
            sb.AppendLine($" ,f004_hihogender");
            sb.AppendLine($" ,f005_setainum1");
            sb.AppendLine($" ,cym");
            sb.AppendLine($" ,birthad");
            sb.AppendLine($" from imp_hihomaster where cym={cym} ");

            sb.AppendLine($" group by ");
            sb.AppendLine($"  f001_hihonum");
            sb.AppendLine($" ,f002_hihokana");
            sb.AppendLine($" ,f003_hihobirth");
            sb.AppendLine($" ,f004_hihogender");
            sb.AppendLine($" ,f005_setainum1");
            sb.AppendLine($" ,cym");
            sb.AppendLine($" ,birthad");
            sb.AppendLine($" order by f001_hihonum;");

            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_hihomaster imp = new imp_hihomaster();

                    imp.f001_hihonum = item[0].ToString();
                    imp.f002_hihokana = item[1].ToString();
                    imp.f003_hihobirth = item[2].ToString();
                    imp.f004_hihogender = item[3].ToString();
                    imp.f005_setainum1 = item[4].ToString();
                  
                    imp.cym = int.Parse(item[5].ToString());
                    imp.birthad = DateTime.Parse(item[6].ToString());
                  
                    lst.Add(imp);
                }

                return lst;
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }

    /// <summary>
    /// 支給決定データ
    /// </summary>
    public partial class imp_chargedata
    {
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                            //インポートID;
        public string f001_insnum { get; set; } = string.Empty;                //保険者番号;
        public string f002_insname { get; set; } = string.Empty;               //保険者名;
        public string f003_hihonum { get; set; } = string.Empty;               //被保険者番号;
        public string f004_shikyukind { get; set; } = string.Empty;            //支給種別（文言）;
        public string f005_shikyukindnum { get; set; } = string.Empty;         //支給整理番号;
        public string f006_mediym { get; set; } = string.Empty;                //診療年月;
        public string f007_ketteiymd { get; set; } = string.Empty;             //支給決定年月日;
        public string f008_ketteiymdw { get; set; } = string.Empty;            //支給決定年月日（日付形式）;
        public string f009_shishutsuymd { get; set; } = string.Empty;          //支出年月日;
        public string f010_shishutsuymdw { get; set; } = string.Empty;         //支出年月日（日付形式）;
        public string f011_charge { get; set; } = string.Empty;                //支給金額;
        public string f012_clinicnum { get; set; } = string.Empty;             //支払先医療機関コード;
        public string f013_clinicname { get; set; } = string.Empty;            //支払先医療機関名称;
        public string f014_hihoname { get; set; } = string.Empty;              //氏名（漢字）;
        public string f015_hihopref { get; set; } = string.Empty;              //現住所ー都道府県名（漢字）;
        public string f016_hihocity { get; set; } = string.Empty;              //現住所ー市区町村名（漢字）;
        public string f017_hihoadd { get; set; } = string.Empty;               //現住所ー住所（漢字）;
        public string f018_hihozip { get; set; } = string.Empty;               //〒;
        public string f019_destname { get; set; } = string.Empty;              //送付先氏名（漢字）;
        public string f020_destpref { get; set; } = string.Empty;              //送付先住所ー都道府県名（漢字）;
        public string f021_destcity { get; set; } = string.Empty;              //送付先住所ー市区町村名（漢字）;
        public string f022_destadd { get; set; } = string.Empty;               //送付先住所（漢字）;
        public string f023_destzip { get; set; } = string.Empty;               //送付先郵便番号;
        public string f024_clinicnum { get; set; } = string.Empty;             //医療機関コード;
        public string f025_total { get; set; } = string.Empty;                 //費用額;
        public string f026_partial { get; set; } = string.Empty;               //一部負担額;
        public string f027_biko { get; set; } = string.Empty;                  //備考（漢字）;
        public int cym { get; set; } = 0;                                      //メホール請求年月;


        /// <summary>
        /// 支給決定データ取得
        /// </summary>
        /// <param name="cym">メホール請求年月</param>
        /// <returns></returns>
        public static List<imp_chargedata> selectall(int cym)
        {
            List<imp_chargedata> lst = new List<imp_chargedata>();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"select * from imp_chargedata where cym={cym} order by f000_importid;");
            DB.Command cmd = DB.Main.CreateCmd(sb.ToString());
            try
            {
                var l = cmd.TryExecuteReaderList();

                foreach (var item in l)
                {
                    imp_chargedata imp = new imp_chargedata();
                    imp.f000_importid = int.Parse(item[0].ToString());
                    imp.f001_insnum = item[1].ToString();
                    imp.f002_insname = item[2].ToString();
                    imp.f003_hihonum = item[3].ToString();
                    imp.f004_shikyukind = item[4].ToString();
                    imp.f005_shikyukindnum = item[5].ToString();
                    imp.f006_mediym = item[6].ToString();
                    imp.f007_ketteiymd = item[7].ToString();
                    imp.f008_ketteiymdw = item[8].ToString();
                    imp.f009_shishutsuymd = item[9].ToString();
                    imp.f010_shishutsuymdw = item[10].ToString();
                    imp.f011_charge = item[11].ToString();
                    imp.f012_clinicnum = item[12].ToString();
                    imp.f013_clinicname = item[13].ToString();
                    imp.f014_hihoname = item[14].ToString();
                    imp.f015_hihopref = item[15].ToString();
                    imp.f016_hihocity = item[16].ToString();
                    imp.f017_hihoadd = item[17].ToString();
                    imp.f018_hihozip = item[18].ToString();
                    imp.f019_destname = item[19].ToString();
                    imp.f020_destpref = item[20].ToString();
                    imp.f021_destcity = item[21].ToString();
                    imp.f022_destadd = item[22].ToString();
                    imp.f023_destzip = item[23].ToString();
                    imp.f024_clinicnum = item[24].ToString();
                    imp.f025_total = item[25].ToString();
                    imp.f026_partial = item[26].ToString();
                    imp.f027_biko = item[27].ToString();
                    imp.cym = int.Parse(item[28].ToString());




                    lst.Add(imp);
                }

                return lst;
            }
            catch(Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }
}
