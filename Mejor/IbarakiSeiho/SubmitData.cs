﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

namespace Mejor.IbarakiSeiho
{
    class SubmitData
    {
        App app;

        SubmitData(App app) => this.app = app;

        public static bool Export(int cym)
        {
            string dirName = string.Empty, logName = string.Empty;
            using (var f = new System.Windows.Forms.SaveFileDialog())
            {
                f.FileName = $"Export{DateTime.Now.ToString("yyyyMMddHHmmss")}.log";
                if (f.ShowDialog() != System.Windows.Forms.DialogResult.OK) return true;
                logName = f.FileName;
                dirName = System.IO.Path.GetDirectoryName(f.FileName);
            }


            var wf = new WaitForm();
            wf.ShowDialogOtherTask();

            wf.LogPrint("申請書を取得しています");
            var list = App.GetApps(cym);
            wf.LogPrint("データを出力しています");

            try
            {
                wf.InvokeValue = 0;

                var info = dirName + "\\0_COMMON001\\00_INFORMATION.CSV";
                var recode = dirName + "\\0_COMMON001\\99_PECULIARTEXTINFO_JYU.CSV";
                var img = dirName + "\\1_MEDICAL001\\";

                System.IO.Directory.CreateDirectory(dirName + "\\0_COMMON001");
                System.IO.Directory.CreateDirectory(dirName + "\\9_JUDOTHERAPY");
                var imageNames = new List<string>();
                int count = 0;

                using (var sw = new System.IO.StreamWriter(recode, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    var fc = new TiffUtility.FastCopy();

                    for (int i = 0; i < list.Count; i++)
                    {
                        //先頭不要等回避
                        for (; i < list.Count && list[i].YM < 1; i++) ;

                        var de = new SubmitData(list[i]);

                        //テキストデータ書き出し
                        sw.WriteLine(de.createMN());
                        sw.WriteLine(de.createSA());
                        sw.WriteLine(de.createNJ());
                        imageNames.Add(de.app.GetImageFullPath());

                        string imageName = dirName + "\\9_JUDOTHERAPY\\" +
                            "000" + DateTimeEx.GetGyymmFromAdYM(list[i].CYM).ToString() +
                            "000" + list[i].Numbering + ".tif";

                        //続紙抽出
                        for (; i < list.Count - 1; i++)
                        {
                            wf.InvokeValue++;
                            if (list[i + 1].YM > 0) break;
                            if (list[i + 1].YM == (int)APP_SPECIAL_CODE.続紙)
                                imageNames.Add(list[i + 1].GetImageFullPath());
                        }

                        //画像保存
                        if (!TiffUtility.MargeOrCopyTiff(fc, imageNames, imageName))
                        {
                            wf.LogPrint($"{imageName}の保存に失敗しました");
                        }
                        imageNames.Clear();
                        count++;
                    }
                }

                int jym = DateTimeEx.GetGyymmFromAdYM(list.Max(item => item.CYM));
                jym--;
                if (jym % 100 == 0) jym -= 88;
                createInfoFile(info, count, jym);
                wf.LogPrint("出力が完了しました");
            }
            catch(Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                wf.LogPrint("エラーが発生しました:"+ex.Message);
            }
            finally
            {
                wf.LogSave(logName);
                wf.Dispose();
            }

            return true;
        }

        private static bool createInfoFile(string fileName, int count, int MediGYYMM)
        {
            try
            {
                using (var sw = new System.IO.StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.WriteLine($"保険者名,{Insurer.CurrrentInsurer.FormalName}");
                    sw.WriteLine($"保険者番号,{Insurer.CurrrentInsurer.InsNumber}");
                    sw.WriteLine($"診療年月,{MediGYYMM}");
                    sw.WriteLine($"作成年月日,{DateTime.Today.ToString("yyyy/MM/dd")}");
                    sw.WriteLine();
                    sw.WriteLine("医科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("DPC  (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("歯科 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine("調剤 (レセ電/紙レセ/コード/画像) ,0,0,0,0");
                    sw.WriteLine($"柔整 (レセ電/紙レセ/コード/画像) ,0,{count},0,{count}");
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }
        
        private string createMN()
        {
            var l = new string[15];
            l[0] = "10";
            l[1] = "MN";
            l[2] = app.Numbering;
            l[3] = DateTimeEx.GetGyymmFromAdYM(app.YM).ToString();
            l[4] = "6";
            l[5] = app.HihoPref.ToString();
            l[6] = "9";
            l[7] = DateTimeEx.GetGyymmFromAdYM(app.CYM).ToString();
            l[8] = "000" + app.Numbering;
            l[9] = "000" + DateTimeEx.GetGyymmFromAdYM(app.CYM).ToString() +
                "000" + app.Numbering;
            l[10] = "2";
            l[11] = string.Empty;
            l[12] = string.Empty;
            l[13] = string.Empty;
            l[14] = string.Empty;

            return string.Join(",", l);
        }

        private string createSA()
        {
            string toWide(string s) => Strings.StrConv(s, VbStrConv.Wide);
            var l = new string[42];
            l[0] = "20";
            l[1] = "SA";
            l[2] = app.AppType == APP_TYPE.柔整 ? "1" :
                app.AppType == APP_TYPE.鍼灸 ? "7" : "";
            l[3] = "1";
            l[4] = app.Family.ToString();   //todo:
            l[5] = string.Empty;
            l[6] = string.Empty;
            l[7] = Insurer.CurrrentInsurer.InsNumber;
            l[8] = toWide(app.HihoNum.Split('-').Length != 2 ? "" : app.HihoNum.Split('-')[0]);
            l[9] = toWide(app.HihoNum.Split('-').Length != 2 ? app.HihoNum : app.HihoNum.Split('-')[1]);
            l[10] = string.Empty;
            l[11] = string.Empty;
            l[12] = string.Empty;
            l[13] = string.Empty;
            l[14] = string.Empty;
            l[15] = string.Empty;
            l[16] = string.Empty;
            l[17] = string.Empty;
            l[18] = string.Empty;
            l[19] = string.Empty;
            l[20] = string.Empty;
            l[21] = app.PersonName;
            l[22] = ((int)app.Sex).ToString(); ;
            l[23] = DateTimeEx.GetIntJpDateWithEraNumber(app.Birthday).ToString();
            l[24] = string.Empty;
            l[25] = string.Empty;
            l[26] = string.Empty;
            l[27] = string.Empty;
            l[28] = string.Empty;
            l[29] = string.Empty;
            l[30] = string.Empty;
            l[31] = string.Empty;
            l[32] = string.Empty;
            l[33] = string.Empty;
            l[34] = string.Empty;
            l[35] = app.CountedDays.ToString();
            l[36] = string.Empty;
            l[37] = string.Empty;
            l[38] = string.Empty;
            l[39] = string.Empty;
            l[40] = string.Empty;
            l[41] = string.Empty;

            return string.Join(",", l);
        }

        private string createNJ()
        {
            var l = new string[29];
            l[0] = "30";
            l[1] = "NJ";
            l[2] = string.Empty;
            l[3] = app.Family == 2 ? "1" : app.Family == 6 ? "2" : "";
            l[4] = app.Total.ToString();
            l[5] = DateTimeEx.GetIntJpDateWithEraNumber(app.FushoFirstDate1).ToString();
            for (int i = 6; i < l.Length; i++) l[i] = string.Empty;

            return string.Join(",", l);
        }
    }
}
