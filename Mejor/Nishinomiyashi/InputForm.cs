﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using NpgsqlTypes;

namespace Mejor.Nishinomiyashi

{
    public partial class InputForm : InputFormCore
    {
        private bool firstTime;//1回目入力フラグ
        private BindingSource bsApp = new BindingSource();
        protected override Control inputPanel => panelRight;

        #region 座標

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100

        /// <summary>
        /// 施術年月位置
        /// </summary>
        Point posYM = new Point(80, 0);

        /// <summary>
        /// 被保険者番号位置
        /// </summary>
        Point posHnum = new Point(800,0);

        /// <summary>
        /// 被保険者性別位置
        /// </summary>
        Point posPerson = new Point(800, 0);

        /// <summary>
        /// 合計金額位置
        /// </summary>
        Point posTotal = new Point(1800, 2060);
        Point posTotalAHK = new Point(1000, 1000);

        /// <summary>
        /// 負傷名位置
        /// </summary>
        Point posBuiDate = new Point(800, 0);   

        /// <summary>
        /// 公費位置
        /// </summary>
        Point posKohi=new Point(80, 0);


        /// <summary>
        /// 被保険者名
        /// </summary>
        Point posHname = new Point(0, 2000);

        /// <summary>
        /// 申請日
        /// </summary>
        Point posShinsei = new Point(1000, 1000);

        /// <summary>
        /// ヘッダコントロール位置
        /// </summary>
        Point posHeader = new Point(80, 500);
        #endregion

        Control[] ymConts, hnumConts, totalConts, firstDateConts, douiConts;

        /// <summary>
        /// ヘッダの番号を控えておく
        /// </summary>
        string strNumbering = string.Empty;


        /// <summary>
        /// 提供データ
        /// </summary>
        List<dataImport_jyusei> lstImport = new List<dataImport_jyusei>();
        
        #region コンストラクタ
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="sGroup"></param>
        /// <param name="firstTime"></param>
        /// <param name="aid"></param>
        public InputForm(ScanGroup sGroup, bool firstTime, int aid = 0)
        {
            InitializeComponent();          

            #region コントロールグループ化
            //施術年月                       
            ymConts = new Control[] { verifyBoxY, verifyBoxM };

            //被保険者番号
            hnumConts = new Control[] { verifyBoxHnum, verifyBoxFushoCount, verifyBoxFamily, };

            //合計、往療、前回支給
            totalConts = new Control[] { verifyBoxTotal, };

            //初検日
            firstDateConts = new Control[] { verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1 };


            #endregion

          
            this.scanGroup = sGroup;
            this.firstTime = firstTime;
            var list = new List<App>();

            
            #region 左リスト
            //GIDで検索
            list = App.GetAppsGID(scanGroup.GroupID);

            //データリストを作成
            bsApp.DataSource = list;
            dataGridViewPlist.DataSource = bsApp;

            for (int j = 1; j < dataGridViewPlist.ColumnCount; j++)
            {
                dataGridViewPlist.Columns[j].Visible = false;
            }
            dataGridViewPlist.Columns[nameof(App.Aid)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.Aid)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.Aid)].HeaderText = "ID";
            dataGridViewPlist.Columns[nameof(App.MediYear)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediYear)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediYear)].HeaderText = "年";
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.MediMonth)].HeaderText = "月";
            dataGridViewPlist.Columns[nameof(App.AppType)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.AppType)].Width = 25;
            dataGridViewPlist.Columns[nameof(App.AppType)].HeaderText = "種";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.InputStatus)].HeaderText = "Flag";
            dataGridViewPlist.Columns[nameof(App.InputStatus)].DisplayIndex = 1;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Visible = true;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].Width = 70;
            dataGridViewPlist.Columns[nameof(App.HihoNum)].HeaderText = "被保番";
            dataGridViewPlist.Columns[nameof(App.HihoNum)].DisplayIndex = 2;

            this.Text += " - Insurer: " + Insurer.CurrrentInsurer.InsurerName;

            //panelTotal.Visible = false;
            //panelHnum.Visible = false;
            
            #endregion


            //aid指定時、その申請書をカレントにする
            if (aid != 0) bsApp.Position = list.FindIndex(x => x.Aid == aid);

            bsApp.CurrentChanged += BsApp_CurrentChanged;
            var app = (App)bsApp.Current;

            //初回表示時
            if (!InitControl(app, verifyBoxY)) return;
            
            if (app != null)
            {
                
                //提供データ取得
                lstImport = dataImport_jyusei.selectAll(app.CYM);
                setApp(app);
            }        

            focusBack(false);

        }
        #endregion

        #region オブジェクトイベント

        /// <summary>
        /// 左側のリストの現在行が変わった場合
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BsApp_CurrentChanged(object sender, EventArgs e)
        {
            var app = (App)bsApp.Current;
            setApp(app);
            focusBack(false);
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            regist();
        }

        private void InputForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonUpdate.PerformClick();
            else if (e.KeyCode == Keys.PageDown) buttonBack.PerformClick();
        }
        
        //全体表示ボタン
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            userControlImage1.SetPictureBoxFill();
        }


        //フォーム表示時
        private void InputForm_Shown(object sender, EventArgs e)
        {
            focusBack(false);
        }

        /// <summary>
        /// 戻るボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBack_Click(object sender, EventArgs e)
        {
            bsApp.MovePrevious();
        }


        /// <summary>
        /// 請求年への入力で、用紙の種類にあった入力項目にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void verifyBoxY_TextChanged(object sender, EventArgs e)
        {
            //種類に応じたセンシティブ制御

            App app = (App)bsApp.Current;
            
            InitControl(app, verifyBoxY);
        }

        /// <summary>
        /// 左のデータ一覧のソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewPlist_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var glist = (List<App>)bsApp.DataSource;
            var name = dataGridViewPlist.Columns[e.ColumnIndex].Name;

            if (name == nameof(App.Aid))
            {
                glist.Sort((x, y) => x.Aid.CompareTo(y.Aid));
            }
            else if (name == nameof(App.InputStatus))
            {
                //フラグ順にソート
                glist.Sort((x, y) =>
                    x.InputOrderNumber == y.InputOrderNumber ?
                    x.Aid.CompareTo(y.Aid) : x.InputOrderNumber.CompareTo(y.InputOrderNumber));
            }
            else if (name == nameof(App.HihoNum))
            {
                glist.Sort((y, x) => x.HihoNum.CompareTo(y.HihoNum));
            }

            bsApp.ResetBindings(false);
        }


        /// <summary>
        /// センシティブ制御 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="verifyboxY"></param>
        /// <returns></returns>
        private bool InitControl(App app,VerifyBox verifyboxY)
        {
          
            Control[] ignoreControls = new Control[] { labelHs, labelYear,
                verifyBoxY, labelInputerName, };

            labelMacthCheck.Visible = false;

            phnum.Visible = false;
            pDoui.Visible = false;
            panelTotal.Visible = false;
            dgv.Visible = false;
            panelF1FirstDay.Visible = false;

            //負傷名1
            verifyBoxF1.Visible = false;
            labelF1.Visible = false;

            //負傷数
            verifyBoxFushoCount.Visible = false;
            labelFusyoCnt.Visible = false;

            //請求金額
            verifyBoxCharge.Visible = false;
            labelCharge.Visible = false;

            //往療
            checkBoxVisit.Visible = false;
            checkBoxVisitKasan.Visible = false;

            //実日数
            labelCountedDays.Visible = false;
            verifyBoxCountedDays.Visible = false;

            //AppがNull（入力前）のときはscanのを採用
            APP_TYPE type = app.AppType == APP_TYPE.NULL ? scan.AppType : app.AppType;
           

            //未入力・入力後表示前
            if (verifyboxY.Text.Trim() == string.Empty)
            {
                switch (type)
                {
                    case APP_TYPE.長期:
                    case APP_TYPE.続紙:
                    case APP_TYPE.不要:
                    case APP_TYPE.エラー:
                    case APP_TYPE.同意書裏:
                    case APP_TYPE.施術報告書:
                    case APP_TYPE.状態記入書:
                        break;

                    case APP_TYPE.同意書:
                        //pDoui.Visible = true;
                        break;

                    case APP_TYPE.柔整:
                        labelMacthCheck.Visible = true;
                        phnum.Visible = true;                        
                        panelTotal.Visible = true;
                        dgv.Visible = true;
                        labelFusyoCnt.Visible = true;
                        verifyBoxFushoCount.Visible = true;
                        break;

                    case APP_TYPE.鍼灸:
                        phnum.Visible = true;                        
                        panelTotal.Visible = true;

                        verifyBoxF1.Visible = true;
                        labelF1.Visible = true;

                        panelF1FirstDay.Visible = true;

                        checkBoxVisitKasan.Visible = true;
                        checkBoxVisit.Visible = true;
                        labelCharge.Visible = true;
                        verifyBoxCharge.Visible = true;
                        labelCountedDays.Visible = true;
                        verifyBoxCountedDays.Visible = true;

                        checkBoxVisit.Visible = true;
                        checkBoxVisitKasan.Visible = true;
                        labelCountedDays.Visible = true;
                        verifyBoxCountedDays.Visible = true;

                        break;

                    case APP_TYPE.あんま:

                        phnum.Visible =  true;       
                        panelTotal.Visible = true;

                        panelF1FirstDay.Visible = true;

                        checkBoxVisitKasan.Visible = true;
                        checkBoxVisit.Visible = true;
                        labelCharge.Visible = true;
                        verifyBoxCharge.Visible = true;
                        labelCountedDays.Visible = true;
                        verifyBoxCountedDays.Visible = true;
                        checkBoxVisit.Visible = true;
                        checkBoxVisitKasan.Visible = true;
                        labelCountedDays.Visible = true;
                        verifyBoxCountedDays.Visible = true;

                        break;
                    default:
                        break;
                }
            }

            //登録後、再表示時
            else
            {

                switch (verifyboxY.Text.Trim())
                {
                    case clsInputKind.長期:
                    case clsInputKind.続紙:
                    case clsInputKind.不要:
                    case clsInputKind.エラー:
                    case clsInputKind.施術同意書裏:
                    case clsInputKind.施術報告書:
                    case clsInputKind.状態記入書:
                        break;

                    case clsInputKind.施術同意書:
                        pDoui.Visible = true;
                        break;

                    default:

                 
                        switch (type)
                        {
                            case APP_TYPE.柔整:
                                labelMacthCheck.Visible = true;
                                phnum.Visible = true;
                                panelTotal.Visible = true;
                                dgv.Visible = true;
                                labelFusyoCnt.Visible = true;
                                verifyBoxFushoCount.Visible = true;
                                break;

                            case APP_TYPE.鍼灸:
                                phnum.Visible = true;
                                panelTotal.Visible = true;

                                verifyBoxF1.Visible = true;
                                labelF1.Visible = true;

                                panelF1FirstDay.Visible = true;

                                checkBoxVisitKasan.Visible = true;
                                checkBoxVisit.Visible = true;
                                labelCharge.Visible = true;
                                verifyBoxCharge.Visible = true;
                                labelCountedDays.Visible = true;
                                verifyBoxCountedDays.Visible = true;
                                checkBoxVisit.Visible = true;
                                checkBoxVisitKasan.Visible = true;
                                labelCountedDays.Visible = true;
                                verifyBoxCountedDays.Visible = true;

                                break;

                            case APP_TYPE.あんま:

                                phnum.Visible = true;
                                panelTotal.Visible = true;

                                panelF1FirstDay.Visible = true;

                                checkBoxVisitKasan.Visible = true;
                                checkBoxVisit.Visible = true;
                                labelCharge.Visible = true;
                                verifyBoxCharge.Visible = true;
                                labelCountedDays.Visible = true;
                                verifyBoxCountedDays.Visible = true;
                                checkBoxVisit.Visible = true;
                                checkBoxVisitKasan.Visible = true;
                                labelCountedDays.Visible = true;
                                verifyBoxCountedDays.Visible = true;

                                break;

                        }

                        break;
                }
            }

            return true;
        }


        /// <summary>
        /// 提供情報グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            dgv.Columns[nameof(dataImport_jyusei.f000_importid)].Width = 80;		//インポートID
            dgv.Columns[nameof(dataImport_jyusei.f001_inspage)].Width = 60;			//保険者ページ数
            dgv.Columns[nameof(dataImport_jyusei.f002_allpage)].Width = 60;			//総ページ数
            dgv.Columns[nameof(dataImport_jyusei.f003_shinsayear)].Width = 60;		//審査年
            dgv.Columns[nameof(dataImport_jyusei.f004_shinsamonth)].Width = 60;		//審査月
            dgv.Columns[nameof(dataImport_jyusei.f005_menjo)].Width = 60;			//免除
            dgv.Columns[nameof(dataImport_jyusei.f006_insnum)].Width = 60;			//保険者番号
            dgv.Columns[nameof(dataImport_jyusei.f007_insname)].Width = 60;			//保険者名
            dgv.Columns[nameof(dataImport_jyusei.f008_clinicnum)].Width = 80;		//機関番号
            dgv.Columns[nameof(dataImport_jyusei.f009_personname)].Width = 100;		//受診者氏名
            dgv.Columns[nameof(dataImport_jyusei.f010_futannum)].Width = 60;		//公費負担者番号
            dgv.Columns[nameof(dataImport_jyusei.f011_futankbn)].Width = 60;		//負担区分
            dgv.Columns[nameof(dataImport_jyusei.f012_startdate)].Width = 60;		//施術開始年月日
            dgv.Columns[nameof(dataImport_jyusei.f013_kbn1)].Width = 60;			//複数月区分
            dgv.Columns[nameof(dataImport_jyusei.f014_total)].Width = 60;			//決定金額
            dgv.Columns[nameof(dataImport_jyusei.f015_biko1)].Width = 60;			//備考1
            dgv.Columns[nameof(dataImport_jyusei.f016_kbn2)].Width = 60;			//修正区分
            dgv.Columns[nameof(dataImport_jyusei.f017_mediy)].Width = 60;			//施療年
            dgv.Columns[nameof(dataImport_jyusei.f018_medim)].Width = 60;			//施療月
            dgv.Columns[nameof(dataImport_jyusei.f019_clinicname)].Width = 120;		//機関名
            dgv.Columns[nameof(dataImport_jyusei.f020_hihonum)].Width = 80;			//被保険者番号
            dgv.Columns[nameof(dataImport_jyusei.f021_birthgw)].Width = 60;			//生年月日年号
            dgv.Columns[nameof(dataImport_jyusei.f022_birthyw)].Width = 60;			//生年月日年
            dgv.Columns[nameof(dataImport_jyusei.f023_birthmw)].Width = 60;			//生年月日月
            dgv.Columns[nameof(dataImport_jyusei.f024_birthdw)].Width = 60;			//生年月日日
            dgv.Columns[nameof(dataImport_jyusei.f025_code)].Width = 60;			//住民コード
            dgv.Columns[nameof(dataImport_jyusei.f026_gender)].Width = 60;			//性別
            dgv.Columns[nameof(dataImport_jyusei.f027_jukyunum)].Width = 60;		//受給者番号
            dgv.Columns[nameof(dataImport_jyusei.f028_ratio)].Width = 60;			//負担割合
            dgv.Columns[nameof(dataImport_jyusei.f029_firstdatew)].Width = 60;		//初検年月日
            dgv.Columns[nameof(dataImport_jyusei.f030_finishdatew)].Width = 60;		//施術終了年月日
            dgv.Columns[nameof(dataImport_jyusei.f031_counteddays)].Width = 60;		//実日数
            dgv.Columns[nameof(dataImport_jyusei.f032_partial)].Width = 60;			//一部負担金
            dgv.Columns[nameof(dataImport_jyusei.f033_biko2)].Width = 60;			//備考2
            dgv.Columns[nameof(dataImport_jyusei.f034_comnum)].Width = 60;			//全国共通キー
            dgv.Columns[nameof(dataImport_jyusei.cym)].Width = 80;				    //メホール請求年月
            dgv.Columns[nameof(dataImport_jyusei.shinsaymad)].Width = 80;			//審査年月西暦
            dgv.Columns[nameof(dataImport_jyusei.startdatead)].Width = 100;			//開始日西暦
            dgv.Columns[nameof(dataImport_jyusei.mediymad)].Width = 80;				//施術年月西暦
            dgv.Columns[nameof(dataImport_jyusei.birthdayad)].Width = 100;			//生年月日西暦
            dgv.Columns[nameof(dataImport_jyusei.firstdatead)].Width = 100;			//初検日西暦
            dgv.Columns[nameof(dataImport_jyusei.finishdatead)].Width = 100;		//終了日西暦
            dgv.Columns[nameof(dataImport_jyusei.totalint)].Width = 80;				//決定金額数値


            dgv.Columns[nameof(dataImport_jyusei.f000_importid)].HeaderText = "インポートID";			//インポートID
            dgv.Columns[nameof(dataImport_jyusei.f001_inspage)].HeaderText = "保険者ページ数";			//保険者ページ数
            dgv.Columns[nameof(dataImport_jyusei.f002_allpage)].HeaderText = "総ページ数";				//総ページ数
            dgv.Columns[nameof(dataImport_jyusei.f003_shinsayear)].HeaderText = "審査年";				//審査年
            dgv.Columns[nameof(dataImport_jyusei.f004_shinsamonth)].HeaderText = "審査月";				//審査月
            dgv.Columns[nameof(dataImport_jyusei.f005_menjo)].HeaderText = "免除";						//免除
            dgv.Columns[nameof(dataImport_jyusei.f006_insnum)].HeaderText = "保険者番号";				//保険者番号
            dgv.Columns[nameof(dataImport_jyusei.f007_insname)].HeaderText = "保険者名";				//保険者名
            dgv.Columns[nameof(dataImport_jyusei.f008_clinicnum)].HeaderText = "機関番号";				//機関番号
            dgv.Columns[nameof(dataImport_jyusei.f009_personname)].HeaderText = "受診者氏名";			//受診者氏名
            dgv.Columns[nameof(dataImport_jyusei.f010_futannum)].HeaderText = "公費負担者番号";			//公費負担者番号
            dgv.Columns[nameof(dataImport_jyusei.f011_futankbn)].HeaderText = "負担区分";				//負担区分
            dgv.Columns[nameof(dataImport_jyusei.f012_startdate)].HeaderText = "施術開始年月日";		//施術開始年月日
            dgv.Columns[nameof(dataImport_jyusei.f013_kbn1)].HeaderText = "複数月区分";					//複数月区分
            dgv.Columns[nameof(dataImport_jyusei.f014_total)].HeaderText = "決定金額";					//決定金額
            dgv.Columns[nameof(dataImport_jyusei.f015_biko1)].HeaderText = "備考1";						//備考1
            dgv.Columns[nameof(dataImport_jyusei.f016_kbn2)].HeaderText = "修正区分";					//修正区分
            dgv.Columns[nameof(dataImport_jyusei.f017_mediy)].HeaderText = "施療年";					//施療年
            dgv.Columns[nameof(dataImport_jyusei.f018_medim)].HeaderText = "施療月";					//施療月
            dgv.Columns[nameof(dataImport_jyusei.f019_clinicname)].HeaderText = "機関名";				//機関名
            dgv.Columns[nameof(dataImport_jyusei.f020_hihonum)].HeaderText = "被保険者番号";			//被保険者番号
            dgv.Columns[nameof(dataImport_jyusei.f021_birthgw)].HeaderText = "生年月日年号";			//生年月日年号
            dgv.Columns[nameof(dataImport_jyusei.f022_birthyw)].HeaderText = "生年月日年";				//生年月日年
            dgv.Columns[nameof(dataImport_jyusei.f023_birthmw)].HeaderText = "生年月日月";				//生年月日月
            dgv.Columns[nameof(dataImport_jyusei.f024_birthdw)].HeaderText = "生年月日日";				//生年月日日
            dgv.Columns[nameof(dataImport_jyusei.f025_code)].HeaderText = "住民コード";					//住民コード
            dgv.Columns[nameof(dataImport_jyusei.f026_gender)].HeaderText = "性別";						//性別
            dgv.Columns[nameof(dataImport_jyusei.f027_jukyunum)].HeaderText = "受給者番号";				//受給者番号
            dgv.Columns[nameof(dataImport_jyusei.f028_ratio)].HeaderText = "負担割合";					//負担割合
            dgv.Columns[nameof(dataImport_jyusei.f029_firstdatew)].HeaderText = "初検年月日";			//初検年月日
            dgv.Columns[nameof(dataImport_jyusei.f030_finishdatew)].HeaderText = "施術終了年月日";		//施術終了年月日
            dgv.Columns[nameof(dataImport_jyusei.f031_counteddays)].HeaderText = "実日数";				//実日数
            dgv.Columns[nameof(dataImport_jyusei.f032_partial)].HeaderText = "一部負担金";				//一部負担金
            dgv.Columns[nameof(dataImport_jyusei.f033_biko2)].HeaderText = "備考2";						//備考2
            dgv.Columns[nameof(dataImport_jyusei.f034_comnum)].HeaderText = "全国共通キー";				//全国共通キー
            dgv.Columns[nameof(dataImport_jyusei.cym)].HeaderText = "メホール請求年月";				    //メホール請求年月
            dgv.Columns[nameof(dataImport_jyusei.shinsaymad)].HeaderText = "審査年月西暦";				//審査年月西暦
            dgv.Columns[nameof(dataImport_jyusei.startdatead)].HeaderText = "開始日西暦";				//開始日西暦
            dgv.Columns[nameof(dataImport_jyusei.mediymad)].HeaderText = "施術年月西暦";				//施術年月西暦
            dgv.Columns[nameof(dataImport_jyusei.birthdayad)].HeaderText = "生年月日西暦";				//生年月日西暦
            dgv.Columns[nameof(dataImport_jyusei.firstdatead)].HeaderText = "初検日西暦";				//初検日西暦
            dgv.Columns[nameof(dataImport_jyusei.finishdatead)].HeaderText = "終了日西暦";				//終了日西暦
            dgv.Columns[nameof(dataImport_jyusei.totalint)].HeaderText = "決定金額数値";				//決定金額数値


            dgv.Columns[nameof(dataImport_jyusei.f000_importid)].Visible = true;                       //インポートID
            dgv.Columns[nameof(dataImport_jyusei.f001_inspage)].Visible = false;                        //保険者ページ数
            dgv.Columns[nameof(dataImport_jyusei.f002_allpage)].Visible = false;                        //総ページ数
            dgv.Columns[nameof(dataImport_jyusei.f003_shinsayear)].Visible = false;                     //審査年
            dgv.Columns[nameof(dataImport_jyusei.f004_shinsamonth)].Visible = false;                    //審査月
            dgv.Columns[nameof(dataImport_jyusei.f005_menjo)].Visible = false;                          //免除
            dgv.Columns[nameof(dataImport_jyusei.f006_insnum)].Visible = false;							//保険者番号
            dgv.Columns[nameof(dataImport_jyusei.f007_insname)].Visible = false;						//保険者名
            dgv.Columns[nameof(dataImport_jyusei.f008_clinicnum)].Visible = true;						//機関番号
            dgv.Columns[nameof(dataImport_jyusei.f009_personname)].Visible = true;						//受診者氏名
            dgv.Columns[nameof(dataImport_jyusei.f010_futannum)].Visible = false;						//公費負担者番号
            dgv.Columns[nameof(dataImport_jyusei.f011_futankbn)].Visible = false;						//負担区分
            dgv.Columns[nameof(dataImport_jyusei.f012_startdate)].Visible = false;						//施術開始年月日
            dgv.Columns[nameof(dataImport_jyusei.f013_kbn1)].Visible = false;							//複数月区分
            dgv.Columns[nameof(dataImport_jyusei.f014_total)].Visible = true;							//決定金額
            dgv.Columns[nameof(dataImport_jyusei.f015_biko1)].Visible = false;							//備考1
            dgv.Columns[nameof(dataImport_jyusei.f016_kbn2)].Visible = false;							//修正区分
            dgv.Columns[nameof(dataImport_jyusei.f017_mediy)].Visible = false;							//施療年
            dgv.Columns[nameof(dataImport_jyusei.f018_medim)].Visible = false;							//施療月
            dgv.Columns[nameof(dataImport_jyusei.f019_clinicname)].Visible = true;						//機関名
            dgv.Columns[nameof(dataImport_jyusei.f020_hihonum)].Visible = true;						//被保険者番号
            dgv.Columns[nameof(dataImport_jyusei.f021_birthgw)].Visible = false;						//生年月日年号
            dgv.Columns[nameof(dataImport_jyusei.f022_birthyw)].Visible = false;						//生年月日年
            dgv.Columns[nameof(dataImport_jyusei.f023_birthmw)].Visible = false;						//生年月日月
            dgv.Columns[nameof(dataImport_jyusei.f024_birthdw)].Visible = false;						//生年月日日
            dgv.Columns[nameof(dataImport_jyusei.f025_code)].Visible = false;							//住民コード
            dgv.Columns[nameof(dataImport_jyusei.f026_gender)].Visible = false;							//性別
            dgv.Columns[nameof(dataImport_jyusei.f027_jukyunum)].Visible = false;						//受給者番号
            dgv.Columns[nameof(dataImport_jyusei.f028_ratio)].Visible = false;							//負担割合
            dgv.Columns[nameof(dataImport_jyusei.f029_firstdatew)].Visible = false;						//初検年月日
            dgv.Columns[nameof(dataImport_jyusei.f030_finishdatew)].Visible = false;					//施術終了年月日
            dgv.Columns[nameof(dataImport_jyusei.f031_counteddays)].Visible = false;					//実日数
            dgv.Columns[nameof(dataImport_jyusei.f032_partial)].Visible = false;						//一部負担金
            dgv.Columns[nameof(dataImport_jyusei.f033_biko2)].Visible = false;							//備考2
            dgv.Columns[nameof(dataImport_jyusei.f034_comnum)].Visible = false;							//全国共通キー
            dgv.Columns[nameof(dataImport_jyusei.cym)].Visible = true;								    //メホール請求年月
            dgv.Columns[nameof(dataImport_jyusei.shinsaymad)].Visible = true;							//審査年月西暦
            dgv.Columns[nameof(dataImport_jyusei.startdatead)].Visible = true;							//開始日西暦
            dgv.Columns[nameof(dataImport_jyusei.mediymad)].Visible = true;							//施術年月西暦
            dgv.Columns[nameof(dataImport_jyusei.birthdayad)].Visible = true;							//生年月日西暦
            dgv.Columns[nameof(dataImport_jyusei.firstdatead)].Visible = true;							//初検日西暦
            dgv.Columns[nameof(dataImport_jyusei.finishdatead)].Visible = true;						//終了日西暦
            dgv.Columns[nameof(dataImport_jyusei.totalint)].Visible = true;							//決定金額数値


        }


        /// <summary>
        /// 提供情報グリッド
        /// </summary>
        private void createGrid(App app=null)
        {

            List<dataImport_jyusei> lstimp = new List<dataImport_jyusei>();


            //string strhmark = verifyBoxHmark.Text.Trim();
            string strhnum = verifyBoxHnum.Text.Trim();
            int intTotal = verifyBoxTotal.GetIntValue();
            int intymad = DateTimeEx.GetAdYearFromHs(verifyBoxY.GetIntValue() * 100 + verifyBoxM.GetIntValue()) * 100 + verifyBoxM.GetIntValue();

            foreach (dataImport_jyusei item in lstImport)
            {

                if (item.mediymad == intymad &&
                    item.f020_hihonum == strhnum &&
                    item.totalint == intTotal)

                {
                    lstimp.Add(item);
                }
            }

            dgv.DataSource = null;

            //0件の場合グリッド作らない
            if (lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
                return;
            }

            dgv.DataSource = lstimp;

            InitGrid();

            
            //複数行の場合は選択行をクリアしないと、勝手に1行目が選択された状態になる
            if (lstimp.Count > 1) dgv.ClearSelection();


            if (app!=null && app.RrID.ToString() != string.Empty && app.ComNum !=string.Empty)                
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {

                    //合致条件はレセプト全国共通キーにする（rridだとNextValなので再取込したときに面倒
                    string strcomnum = r.Cells[nameof(dataImport_jyusei.f034_comnum)].Value.ToString();

                    //エクセル編集等で指数とかになってcomnumが潰れている場合rridで合致させる
                    if (System.Text.RegularExpressions.Regex.IsMatch(strcomnum, ".+[E+]"))
                    {

                        if (r.Cells["f000_importid"].Value.ToString() == app.RrID.ToString())
                        {
                            r.Selected = true;
                        }

                        //提供データIDと違う場合の処理抜け
                        else
                        {
                            r.Selected = false;
                        }
                    }
                    else
                    {

                        if (strcomnum == app.ComNum)
                        {
                            //レセプト全国共通キーで合致している場合AND複数候補AND1回目入力（未処理）時、誤って一番上で登録してしまうのを防ぐため自動選択しない                            
                            if (app.StatusFlags == StatusFlag.未処理 && lstimp.Count > 1) r.Selected = false;
                            else r.Selected = true;
                        }
                        //提供データIDと違う場合の処理抜け                        
                        else
                        {
                            r.Selected = false;
                        }
                    }

                }
            }


       

            if (lstimp == null || lstimp.Count == 0)
            {
                labelMacthCheck.BackColor = Color.Pink;
                labelMacthCheck.Text = "マッチング無し";
                labelMacthCheck.Visible = true;
            }

            
            //マッチングOK条件に選択行が1行の場合も追加
            
            else if (lstimp.Count == 1 || dgv.SelectedRows.Count==1)
            {
                labelMacthCheck.BackColor = Color.Cyan;
                labelMacthCheck.Text = "マッチングOK";
                labelMacthCheck.Visible = true;
            }
            else
            {
                labelMacthCheck.BackColor = Color.Yellow;
                labelMacthCheck.Text = "マッチング未確定\r\n選択して下さい。";
                labelMacthCheck.Visible = true;
            }


        }


        #endregion



        #region 各種ロード

        /// <summary>
        /// 現在選択されている行のAppを表示します
        /// </summary>
        private void setApp(App app)
        {
            //全クリア
            iVerifiableAllClear(panelRight);

            ////表示初期化
            //phnum.Visible = false;
            //panelTotal.Visible = false;            
            //pZenkai.Enabled = false;
            //dgv.Visible = false;

            //提供データグリッド初期化
            dgv.DataSource = null;

            //マッチングチェック用
            labelMacthCheck.Text = "";
            labelMacthCheck.BackColor = SystemColors.Control;
            labelMacthCheck.Visible = false;

            //入力ユーザー表示
            labelInputerName.Text = 
                "入力1:  " + User.GetUserName(app.Ufirst) +
                "\r\n入力2:  " + User.GetUserName(app.Usecond);

          //  InitControl(scan.AppType);


            if (!firstTime)
            {
                //ベリファイ入力時
                setValues(app);
            }
            else
            {
                if (app.StatusFlagCheck(StatusFlag.入力済))
                {
                    setValues(app);
                }
                else
                {
                    //OCRデータがあれば、部位のみ挿入
                    if (!string.IsNullOrWhiteSpace(app.OcrData))
                    {
                        var ocr = app.OcrData.Split(',');
                    }
                }
            }
       

            //画像の表示
            setImage(app);
            changedReset(app);
        }


        /// <summary>
        /// フォーム上の各画像の表示
        /// </summary>
        private void setImage(App app)
        {
            string fn = app.GetImageFullPath();

            try
            {
                using (var fs = new System.IO.FileStream(fn, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                using (var img = Image.FromStream(fs,false,false))
                {
                    //全体表示
                    userControlImage1.SetImage(img, fn);
                    userControlImage1.SetPictureBoxFill();

                    //拡大表示                    
                    scrollPictureControl1.Ratio = 0.4f;
                    scrollPictureControl1.SetImage(img, fn);
                    scrollPictureControl1.ScrollPosition = posYM;
                }
            }
            catch
            {
                MessageBox.Show("画像表示でエラーが発生しました");
                return;
            }
        }

        /// <summary>
        /// テーブルからテキストボックスに値を入れる
        /// </summary>
        /// <param name="app">appクラス</param>
        private void setValues(App app)
        {
            if (!app.StatusFlagCheck(StatusFlag.入力済)) return;
            var nv = !app.StatusFlagCheck(StatusFlag.ベリファイ済);


            switch (app.MediYear)
            {
                case (int)APP_SPECIAL_CODE.続紙:
                    setValue(verifyBoxY, clsInputKind.続紙, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.不要:
                    setValue(verifyBoxY, clsInputKind.不要, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.バッチ:
                    setValue(verifyBoxY, clsInputKind.エラー, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.同意書:
                    setValue(verifyBoxY, clsInputKind.施術同意書, firstTime, nv);

                    //DouiDate:同意年月日                
                    setDateValue(app.TaggedDatas.DouiDate, firstTime, nv, vbDouiY, vbDouiM, vbDouiG, vbDouiD);


                    break;

                case (int)APP_SPECIAL_CODE.同意書裏:
                    setValue(verifyBoxY, clsInputKind.施術同意書裏, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.施術報告書:
                    setValue(verifyBoxY, clsInputKind.施術報告書, firstTime, nv);
                    break;

                case (int)APP_SPECIAL_CODE.状態記入書:
                    setValue(verifyBoxY, clsInputKind.状態記入書, firstTime, nv);
                    break;

                default:
                    
                    //申請書


                    //和暦年
                    //和暦月
                    setValue(verifyBoxY, app.MediYear.ToString(), firstTime, nv);
                    setValue(verifyBoxM, app.MediMonth.ToString(), firstTime, nv);


                    //被保険者証番号
                    setValue(verifyBoxHnum, Microsoft.VisualBasic.Strings.StrConv(app.HihoNum,VbStrConv.Narrow), firstTime, nv);


                    //初検日1
                    setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE, verifyBoxF1FirstD);

                    //負傷名1
                    setValue(verifyBoxF1, app.FushoName1, firstTime, nv);
              
                    //合計金額
                    setValue(verifyBoxTotal, app.Total.ToString(), firstTime, nv);

                    //請求金額
                    setValue(verifyBoxCharge, app.Charge.ToString(), firstTime, nv);

                    //実日数
                    setValue(verifyBoxCountedDays, app.CountedDays, firstTime, nv);

                    //部位数
                    setValue(verifyBoxFushoCount, app.TaggedDatas.count, firstTime, nv);

                    //性別
                    setValue(verifyBoxSex, app.Sex, firstTime, nv);

                    //生年月日
                    setDateValue(app.Birthday, firstTime, nv, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthE, verifyBoxBirthD);

                    //本人家族 本人家族x100+本家区分
                    setValue(verifyBoxFamily, app.Family.ToString().Substring(0,1), firstTime, nv);

                    //往療あり
                    setValue(checkBoxVisit, app.Distance == 999, firstTime, nv);

                    //往療加算
                    setValue(checkBoxVisitKasan, app.VisitAdd == 999, firstTime, nv);


                    //グリッド選択する
                    //createGrid(app.ComNum.ToString());
                    if (app.AppType == APP_TYPE.柔整) createGrid(app);

                    break;
            }
        }


        #endregion

        #region 各種更新

        private bool ResistImportData(App app)
        {
            try
            {
                if (dgv.Rows.Count > 0)
                {

                    app.RrID = int.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f000_importid)].Value.ToString()); //ID
                    
                    int tmpRatio = 0;
                    app.Ratio = int.TryParse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f028_ratio)].Value.ToString(), out tmpRatio) == false ? 0 : tmpRatio;  //給付割合                        

                    //入力を優先させる                        
                    //app.HihoNum= dgv.CurrentRow.Cells["hihonum_nr"].Value.ToString();                      //被保険者証番号
                    
                    app.InsNum = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f006_insnum)].Value.ToString();                                  //保険者番号
                    app.TaggedDatas.InsName = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f007_insname)].Value.ToString();                    //保険者名

                    //入力画面優先
                    //app.Sex = int.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f026_gender)].Value.ToString());                          //性別 

                    app.Birthday = DateTime.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.birthdayad)].Value.ToString());                 //生年月日西暦
                    app.ClinicName = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f019_clinicname)].Value.ToString();                          //医療機関名
                    app.ClinicNum = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f008_clinicnum)].Value.ToString();                            //医療機関番号
                    app.ComNum = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f034_comnum)].Value.ToString();                                  //レセプト全国共通キー
                    app.CountedDays = int.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f031_counteddays)].Value.ToString());             //実日数
                    app.FushoFirstDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.firstdatead)].Value.ToString());         //初検日1
                    app.FushoStartDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.startdatead)].Value.ToString());         //開始日1
                    app.FushoFinishDate1 = DateTime.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.finishdatead)].Value.ToString());       //終了日1
                    app.PersonName= dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f009_personname)].Value.ToString();                           //受療者名
                    
                    app.Total = int.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.totalint)].Value.ToString());                           //合計

                    int.TryParse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f032_partial)].Value.ToString(), out int intPartial);
                    app.Partial = intPartial;                       //一部負担金

                    app.TaggedDatas.GeneralString1 = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f025_code)].Value.ToString();        //住民コード
                    
                    app.TaggedDatas.JukyuNum= dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f027_jukyunum)].Value.ToString();               //公費受給者番号
                    app.TaggedDatas.KouhiNum = dgv.CurrentRow.Cells[nameof(dataImport_jyusei.f010_futannum)].Value.ToString();              //公費負担者番号

                   
                    //新規継続
                    //初検日と診療年月が同じ場合は新規
                    if (app.FushoFirstDate1.Year == int.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.mediymad)].Value.ToString().Substring(0, 4)) &&
                        app.FushoFirstDate1.Month == int.Parse(dgv.CurrentRow.Cells[nameof(dataImport_jyusei.mediymad)].Value.ToString().Substring(4, 2)))
                    {
                        app.NewContType = NEW_CONT.新規;
                    }
                    else
                    {
                        app.NewContType = NEW_CONT.継続;
                    }


                }
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return false;
            }

        }

        /// <summary>
        /// 入力内容をチェックします+appクラスへの反映
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns>エラーの場合null</returns>
        private bool checkApp(App app)
        {
            hasError = false;

            //申請書以外
            switch (verifyBoxY.Text)
            {
                case clsInputKind.エラー:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.バッチ;
                    app.AppType = APP_TYPE.バッチ;
                    break;

                case clsInputKind.続紙:
                    //続紙
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.続紙;
                    app.AppType = APP_TYPE.続紙;
                    break;

                case clsInputKind.不要:
                    //不要
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.不要;
                    app.AppType = APP_TYPE.不要;
                    break;

                case clsInputKind.施術同意書裏:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書裏;
                    app.AppType = APP_TYPE.同意書裏;
                    break;

                case clsInputKind.施術報告書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.施術報告書;
                    app.AppType = APP_TYPE.施術報告書;
                    break;

                case clsInputKind.状態記入書:

                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.状態記入書;
                    app.AppType = APP_TYPE.状態記入書;
                    break;

                case clsInputKind.施術同意書:
                    resetInputData(app);
                    app.MediYear = (int)APP_SPECIAL_CODE.同意書;
                    app.AppType = APP_TYPE.同意書;

                    //DouiDate:同意年月日               
                    DateTime dtDouiym = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

                    //DouiDate:同意年月日
                    app.TaggedDatas.DouiDate = dtDouiym;
                    app.TaggedDatas.flgSejutuDouiUmu = dtDouiym == DateTime.MinValue ? false : true;


                    break;

                default:
                    //申請書

                    #region 入力チェック
                    //和暦月
                    int month = verifyBoxM.GetIntValue();
                    setStatus(verifyBoxM, month < 1 || 12 < month);

                    //和暦年
                    int year = verifyBoxY.GetIntValue();
                    setStatus(verifyBoxY, year < 1 || 31 < year);
                    int adYear = DateTimeEx.GetAdYearFromHs(year * 100 + month);

                    //被保険者証番号   
                    string hnumN = verifyBoxHnum.Text.Trim();
                    setStatus(verifyBoxHnum, hnumN==string.Empty);

                    //性別
                    int intsex = verifyBoxSex.GetIntValue();
                    setStatus(verifyBoxSex, !new int[] { 1, 2 }.Contains(intsex));

                    //生年月日
                    DateTime birthday = dateCheck(verifyBoxBirthE, verifyBoxBirthY, verifyBoxBirthM, verifyBoxBirthD);

                 
                    //合計金額
                    int total = verifyBoxTotal.GetIntValue();
                    setStatus(verifyBoxTotal, total < 100 || total > 200000);

                    //負傷数(柔整
                    int FushoCount = verifyBoxFushoCount.GetIntValue();
                    if (verifyBoxFushoCount.Visible) setStatus(verifyBoxFushoCount, FushoCount < 0);

                    //負傷名１(はりのみ
                    string strF1 = verifyBoxF1.Text.Trim();                    
                    if(verifyBoxF1.Visible) setStatus(verifyBoxF1, strF1 == string.Empty);


                    //初検日1(あんま、はり
                    DateTime f1FirstDt = DateTimeEx.DateTimeNull;
                    if (panelF1FirstDay.Visible) f1FirstDt = dateCheck(verifyBoxF1FirstE, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstD);

                    //請求金額(あんま、はり
                    int charge = verifyBoxCharge.GetIntValue();                    
                    if(verifyBoxCharge.Visible) setStatus(verifyBoxCharge, charge < 0 || charge > 200000);

                    //実日数(あんま、はり
                    int counteddays = verifyBoxCountedDays.GetIntValue();                    
                    if(verifyBoxCountedDays.Visible) setStatus(verifyBoxCountedDays, counteddays < 0 || counteddays > 31);

                  


                    //ここまでのチェックで必須エラーが検出されたらfalse
                    if (hasError)
                    {
                        showInputErrorMessage();
                        return false;
                    }

                    /*
                    //合計金額：請求金額：本家区分のトリプルチェック
                    //金額でのエラーがあればいったん登録中断
                    bool ratioError = (int)(total * ratio / 10) != seikyu;
                    if (ratioError && ratio == 8) ratioError = (int)(total * 9 / 10) != seikyu;
                    if (ratioError)
                    {
                        verifyBoxTotal.BackColor = Color.GreenYellow;
                        verifyBoxCharge.BackColor = Color.GreenYellow;
                        var res = MessageBox.Show("給付割合・合計金額・請求金額のいずれか、" +
                            "または複数に入力ミスがある可能性があります。このまま登録しますか？", "",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        if (res != System.Windows.Forms.DialogResult.OK) return false;
                    }*/

                    #endregion

                    #region Appへの反映

                    //ここから値の反映
                    app.MediYear = year;                //施術年
                    app.MediMonth = month;              //施術月

                    app.HihoNum =hnumN;                //被保険者証番号                                       
                   
                    app.AppType = scan.AppType;     //申請書種別

                    app.Birthday = birthday;                //生年月日
                    app.Sex = intsex;                       //性別

                    app.Total = total;                 //合計

                    if (verifyBoxFushoCount.Visible) app.TaggedDatas.count = FushoCount;     //負傷数

                    if (panelF1FirstDay.Visible) app.FushoFirstDate1 = f1FirstDt;        //初検日
                    
                    if (verifyBoxF1.Visible) app.FushoName1 = verifyBoxF1.Text.Trim();      //負傷名1 鍼灸のみ

                    if (verifyBoxCharge.Visible) app.Charge = charge;                 //請求金額
                    if (verifyBoxCountedDays.Visible) app.CountedDays = counteddays;  //実日数

                    //往療あり
                    app.Distance = checkBoxVisit.Checked ? 999 : 0;
                    //往療加算
                    app.VisitAdd = checkBoxVisitKasan.Checked ? 999 : 0;

                    //提供データの適用
                    if (!ResistImportData(app)) return false;


                    if (app.AppType == APP_TYPE.柔整)
                    {

                        if (dgv.Rows.Count == 0 || dgv.SelectedRows.Count==0)
                            
                        {
                            if (MessageBox.Show("マッチングなしですがよろしいですか？",
                                Application.ProductName,
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question,
                                MessageBoxDefaultButton.Button2) == DialogResult.No)
                            {
                                return false;
                            }
                        }
                    }
                    

                    #endregion


                    break;

            }
          

            return true;
        }

     

        /// <summary>
        /// Appの種類を判別し、データをチェック、OKならデータベースをアップデートします
        /// </summary>
        /// <returns></returns>
        private bool updateDbApp()
        {
            var app = (App)bsApp.Current;
            if (app == null) return false;

            //2回目入力＆ベリファイ済みは何もしない
            if (!firstTime && app.StatusFlagCheck(StatusFlag.ベリファイ済)) return true;


            
            if (!checkApp(app))
            {
                focusBack(true);
                return false;
            }

            //ベリファイチェック
            if (!firstTime && !checkVerify()) return false;

            //データベースへ反映
            var db = new DB("jyusei");
            using (var jyuTran = db.CreateTransaction())
            using (var tran = DB.Main.CreateTransaction())
            {
                var ut = firstTime ? App.UPDATE_TYPE.FirstInput : App.UPDATE_TYPE.SecondInput;
               
                if (firstTime && app.Ufirst == 0)
                {
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.First, 0, DateTime.Now - dtstart_core, jyuTran)) return false;
                }
                else if (!firstTime && app.Usecond == 0)
                {
                    if (!InputLog.FirstMissLogWrite(app.Aid, firstMissCount, jyuTran)) return false;
                    if (!InputLog.LogWriteTs(app, INPUT_TYPE.Second, secondMissCount, DateTime.Now - dtstart_core, jyuTran)) return false;
                }

                if (!app.Update(User.CurrentUser.UserID, ut, tran)) return false;

                //20211012101218 furukawa AUXにApptype登録
                if (!Application_AUX.Update(app.Aid, app.AppType, tran, app.RrID.ToString())) return false;


                jyuTran.Commit();
                tran.Commit();
                return true;
            }
        }

        private void verifyBoxTotal_Validated(object sender, EventArgs e)
        {
            if (scan.AppType == APP_TYPE.柔整) createGrid();
        }

        private void cbZenkai_CheckedChanged(object sender, EventArgs e)
        {
            pZenkai.Enabled = cbZenkai.Checked;
        }

       

        /// <summary>
        /// DB更新
        /// </summary>
        private void regist()
        {
            if (dataChanged && !updateDbApp()) return;
            int ri = dataGridViewPlist.CurrentCell.RowIndex;
            if (dataGridViewPlist.RowCount <= ri + 1)
            {
                if (MessageBox.Show("最終データの処理が終了しました。入力を終了しますか？", "処理確認",
                      MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                      != System.Windows.Forms.DialogResult.OK)
                {
                    dataGridViewPlist.CurrentCell = null;
                    dataGridViewPlist.CurrentCell = dataGridViewPlist[0, ri];
                    return;
                }
                this.Close();
                return;
            }
            bsApp.MoveNext();
        }

        #endregion


        #region 画像関連
        /// <summary>
        /// 画像ファイルの回転
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(true);
            var app = (App)bsApp.Current;
            setImage(app);
        }
   

        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            userControlImage1.ImageRotate(false);
            var app = (App)bsApp.Current;
            setImage(app);
        }

        /// <summary>
        /// 画像ファイルの差し替え
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.FileName = "*.tif";
            ofd.Filter = "tifファイル(*.tiff;*.tif)|*.tiff;*.tif";
            ofd.Title = "新しい画像ファイルを選択してください";

            if (ofd.ShowDialog() != DialogResult.OK) return;
            string newFileName = ofd.FileName;

            var app = (App)bsApp.Current;
            string fn = app.GetImageFullPath(DB.GetMainDBName());

            try
            {
                System.IO.File.Copy(newFileName, fn, true);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex + "\r\n\r\n" + newFileName + " から\r\n" +
                    fn + " へのファイル差替に失敗しました");
            }

            setImage(app);
        }

        #endregion

        #region 画像座標関係

        /// <summary>
        /// フォーカス位置によって画像の表示位置を制御
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void item_Enter(object sender, EventArgs e)
        {
            Point p;

            if (ymConts.Contains(sender)) p = posYM;
            else if (hnumConts.Contains(sender)) p = posHnum;                       
            else if (totalConts.Contains(sender)) p = posTotalAHK;
            else if (firstDateConts.Contains(sender)) p = posBuiDate;
            
            else return;

            scrollPictureControl1.ScrollPosition = p;
        }

        /// <summary>
        /// 入力項目によって画像位置を修正したら、その位置を覚えておく
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scrollPictureControl1_ImageScrolled(object sender, EventArgs e)
        {
            //入力項目によって画像位置を修正したら、その位置を控え、デフォルトのpos変数に代入する
            var pos = scrollPictureControl1.ScrollPosition;

            if (ymConts.Any(c => c.Focused)) posYM = pos;
            else if (hnumConts.Any(c => c.Focused)) posHnum = pos;
            
            else if (totalConts.Any(c => c.Focused)) posTotal = pos;
            else if (firstDateConts.Any(c => c.Focused)) posBuiDate = pos;
            //else if (douiConts.Any(c => c.Focused)) posHname = pos;
            
        }
        #endregion


    
       
    }      
}
