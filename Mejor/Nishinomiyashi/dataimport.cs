﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Mejor.Nishinomiyashi

{
    class dataImport
    {
        /// <summary>
        /// インポートメイン
        /// </summary>
        /// <param name="_cym">メホール請求年月</param>
        public static void import_main(int _cym)
        {
            frmImport frm = new frmImport(_cym);
            frm.ShowDialog();

            string strFileName = frm.strFileBaseData;
         

            WaitForm wf = new WaitForm();

            wf.ShowDialogOtherTask();
            try
            {
                if (strFileName != string.Empty)
                {
                    wf.LogPrint("データインポート");
                    if (!dataImport_jyusei.dataImport_main(_cym, wf, strFileName))
                    {
                        wf.LogPrint("データインポート異常終了");
                        System.Windows.Forms.MessageBox.Show("データインポート異常終了"
                            , System.Windows.Forms.Application.ProductName
                            , System.Windows.Forms.MessageBoxButtons.OK
                            , System.Windows.Forms.MessageBoxIcon.Exclamation);
                        return;
                    }
                    
                    wf.LogPrint("データインポート終了");
                }
                else
                {
                    wf.LogPrint("データのファイルが指定されていないため処理しない");
                }

       

                System.Windows.Forms.MessageBox.Show("終了");
            }
            catch(Exception ex)
            {

            }
            finally
            {
                wf.Dispose();
            }
        }
    }



    /// <summary>
    /// 柔整データインポート
    /// </summary>
    partial class dataImport_jyusei
    {

        #region テーブル構造
        [DB.DbAttribute.PrimaryKey]
        [DB.DbAttribute.Serial]
        public int f000_importid { get; set; } = 0;                                 //インポートID;

        public string f001_inspage { get; set; } = string.Empty;                    //保険者ページ数;
        public string f002_allpage { get; set; } = string.Empty;                    //総ページ数;
        public string f003_shinsayear { get; set; } = string.Empty;                 //審査年;
        public string f004_shinsamonth { get; set; } = string.Empty;                //審査月;
        public string f005_menjo { get; set; } = string.Empty;                      //免除;
        public string f006_insnum { get; set; } = string.Empty;                     //保険者番号;
        public string f007_insname { get; set; } = string.Empty;                    //保険者名;
        public string f008_clinicnum { get; set; } = string.Empty;                  //機関番号;
        public string f009_personname { get; set; } = string.Empty;                 //受診者氏名;
        public string f010_futannum { get; set; } = string.Empty;                   //公費負担者番号;
        public string f011_futankbn { get; set; } = string.Empty;                   //負担区分;
        public string f012_startdate { get; set; } = string.Empty;                  //施術開始年月日;
        public string f013_kbn1 { get; set; } = string.Empty;                       //複数月区分;
        public string f014_total { get; set; } = string.Empty;                      //決定金額;
        public string f015_biko1 { get; set; } = string.Empty;                      //備考1;
        public string f016_kbn2 { get; set; } = string.Empty;                       //修正区分;
        public string f017_mediy { get; set; } = string.Empty;                      //施療年;
        public string f018_medim { get; set; } = string.Empty;                      //施療月;
        public string f019_clinicname { get; set; } = string.Empty;                 //機関名;
        public string f020_hihonum { get; set; } = string.Empty;                    //被保険者番号;
        public string f021_birthgw { get; set; } = string.Empty;                    //生年月日年号;
        public string f022_birthyw { get; set; } = string.Empty;                    //生年月日年;
        public string f023_birthmw { get; set; } = string.Empty;                    //生年月日月;
        public string f024_birthdw { get; set; } = string.Empty;                    //生年月日日;
        public string f025_code { get; set; } = string.Empty;                       //住民コード;
        public string f026_gender { get; set; } = string.Empty;                     //性別;
        public string f027_jukyunum { get; set; } = string.Empty;                   //受給者番号;
        public string f028_ratio { get; set; } = string.Empty;                      //負担割合;
        public string f029_firstdatew { get; set; } = string.Empty;                 //初検年月日;
        public string f030_finishdatew { get; set; } = string.Empty;                //施術終了年月日;
        public string f031_counteddays { get; set; } = string.Empty;                //実日数;
        public string f032_partial { get; set; } = string.Empty;                    //一部負担金;
        public string f033_biko2 { get; set; } = string.Empty;                      //備考2;
        public string f034_comnum { get; set; } = string.Empty;                     //全国共通キー;
        public int cym { get; set; } = 0;                                           //メホール請求年月;
        public int shinsaymad { get; set; } = 0;                                    //審査年月西暦;
        public DateTime startdatead { get; set; } = DateTime.MinValue;              //開始日西暦;
        public int mediymad { get; set; } = 0;                                      //施術年月西暦;
        public DateTime birthdayad { get; set; } = DateTime.MinValue;               //生年月日西暦;
        public DateTime firstdatead { get; set; } = DateTime.MinValue;              //初検日西暦;
        public DateTime finishdatead { get; set; } = DateTime.MinValue;             //終了日西暦;
        public int totalint { get; set; } = 0;                                      //決定金額数値;

        #endregion


        static int intcym = 0;

        List<dataImport_jyusei> lstImp = new List<dataImport_jyusei>();

        /// <summary>
        /// データインポート
        /// </summary>
        /// <returns></returns>
        public static bool dataImport_main(int _cym,WaitForm wf,string strFileName)
        {
            
            List<App> lstApp = new List<App>();
            lstApp = App.GetApps(_cym);
            intcym = _cym;

            try
            {
                wf.LogPrint("データ取得");
                
                //dataimport_jyuseiに登録
                if(!EntryCSVData(strFileName, wf)) return false;

                return true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
           

        }



        /// <summary>
        /// CSV取込
        /// </summary>
        /// <param name="strFileName"></param>
        /// <param name="wf"></param>
        /// <returns></returns>
        private static bool EntryCSVData(string strFileName, WaitForm wf)
        {

            DB.Transaction tran = new DB.Transaction(DB.Main);
            DB.Command cmd = new DB.Command($"delete from dataimport_jyusei where cym={intcym}", tran);
            cmd.TryExecuteNonQuery();

            wf.LogPrint($"{intcym}削除");

            try
            {
                //CSV内容ロード
                List<string[]> lst = CommonTool.CsvImportMultiCode(strFileName);

                wf.LogPrint("CSVロード");
                wf.SetMax(lst.Count);

                if (lst[0].Length != 34)
                {
                    System.Windows.Forms.MessageBox.Show("列数が34ではありません。取り込めません。ファイルの中身を確認してください"
                                            , System.Windows.Forms.Application.ProductName
                                            , System.Windows.Forms.MessageBoxButtons.OK
                                            , System.Windows.Forms.MessageBoxIcon.Exclamation);

                    return false;
                }

                //クラスのリストに登録
                foreach (string[] tmp in lst)
                {
                    //20220509101105 furukawa st ////////////////////////
                    //キャンセルプロセス
                    
                    if (CommonTool.WaitFormCancelProcess(wf, tran))
                    {
                        cmd.Dispose();
                        return false;
                    }
                    //20220509101105 furukawa ed ////////////////////////


                    try
                    {
                        //被保番を判断材料とする
                        int.Parse(tmp[19].ToString());
                    }
                    catch
                    {
                        wf.LogPrint($"{wf.Value + 1}行目：文字列行なので飛ばす");
                        continue;
                    }

                    dataImport_jyusei cls = new dataImport_jyusei();

                    cls.f001_inspage = tmp[0];			  //保険者ページ数
                    cls.f002_allpage = tmp[1];			  //総ページ数
                    cls.f003_shinsayear = tmp[2];		  //審査年
                    cls.f004_shinsamonth = tmp[3];		  //審査月
                    cls.f005_menjo = tmp[4];			  //免除
                    cls.f006_insnum = tmp[5];			  //保険者番号
                    cls.f007_insname = tmp[6];			  //保険者名
                    cls.f008_clinicnum = tmp[7];		  //機関番号
                    cls.f009_personname = tmp[8];		  //受診者氏名
                    cls.f010_futannum = tmp[9];			  //公費負担者番号
                    cls.f011_futankbn = tmp[10];		  //負担区分
                    cls.f012_startdate = tmp[11];		  //施術開始年月日
                    cls.f013_kbn1 = tmp[12];			  //複数月区分
                    cls.f014_total = tmp[13];			  //決定金額
                    cls.f015_biko1 = tmp[14];			  //備考1
                    cls.f016_kbn2 = tmp[15];			  //修正区分
                    cls.f017_mediy = tmp[16];			  //施療年
                    cls.f018_medim = tmp[17];			  //施療月
                    cls.f019_clinicname = tmp[18];		  //機関名
                    cls.f020_hihonum = tmp[19];			  //被保険者番号
                    cls.f021_birthgw = tmp[20];			  //生年月日年号
                    cls.f022_birthyw = tmp[21];			  //生年月日年
                    cls.f023_birthmw = tmp[22];			  //生年月日月
                    cls.f024_birthdw = tmp[23];			  //生年月日日
                    cls.f025_code = tmp[24];			  //住民コード
                    cls.f026_gender = tmp[25];			  //性別
                    cls.f027_jukyunum = tmp[26];		  //受給者番号
                    cls.f028_ratio = tmp[27];			  //負担割合
                    cls.f029_firstdatew = tmp[28];		  //初検年月日
                    cls.f030_finishdatew = tmp[29];		  //施術終了年月日
                    cls.f031_counteddays = tmp[30];		  //実日数
                    cls.f032_partial = tmp[31];			  //一部負担金
                    cls.f033_biko2 = tmp[32];			  //備考2
                    cls.f034_comnum = tmp[33];			  //全国共通キー




                    //管理項目
                    string strErr = string.Empty;
                    cls.cym = intcym;//メホール請求年月管理用;

                    //審査年月西暦;
                    string strShinsaym_w = cls.f003_shinsayear.PadLeft(2, '0') + cls.f004_shinsamonth.PadLeft(2, '0');
                    if (int.Parse(strShinsaym_w) == 0)
                    {
                        strErr += "審査年月 変換に失敗しました";
                        cls.shinsaymad = 0;
                    }
                    else
                    {
                        cls.shinsaymad = DateTimeEx.GetAdYearFromHs(int.Parse(strShinsaym_w)) * 100 + int.Parse(strShinsaym_w.Substring(2, 2));
                    }


                    //施術年月西暦
                    string strmediym_w = cls.f017_mediy.PadLeft(2, '0') + cls.f018_medim.PadLeft(2, '0');
                    if (int.Parse(strmediym_w) == 0)
                    {
                        strErr += "施術年月 変換に失敗しました";
                        cls.mediymad = 0;
                    }
                    else
                    {
                        cls.mediymad = DateTimeEx.GetAdYearFromHs(int.Parse(strmediym_w)) * 100 + int.Parse(strmediym_w.Substring(2, 2));
                    }

                    //生年月日西暦
                    if (cls.f021_birthgw == string.Empty)
                    {
                        strErr += "生年月日 変換に失敗しました";
                        cls.birthdayad = DateTime.MinValue;
                    }
                    else
                    {
                        //20220509094300 furukawa st ////////////////////////
                        //平30型を西暦に変換
                        
                        int intBirthYearAD =DateTimeEx.GetAdYearFromInitial1(cls.f021_birthgw + cls.f022_birthyw.PadLeft(2, '0'));
                        //  DateTimeEx.TryGetYear(cls.f021_birthgw + cls.f022_birthyw.PadLeft(2, '0'), out int intBirthYearAD);
                        //20220509094300 furukawa ed ////////////////////////


                        string strBirthMonthDay = cls.f023_birthmw.PadLeft(2, '0') + cls.f024_birthdw.PadLeft(2, '0');
                        cls.birthdayad = DateTimeEx.ToDateTime(intBirthYearAD * 10000 + int.Parse(strBirthMonthDay));

                    }


                    //開始年月日西暦
                    if (cls.f012_startdate == string.Empty)
                    {
                        strErr += "開始年月日 変換に失敗しました";
                        cls.startdatead = DateTime.MinValue;
                    }
                    else
                    {
                        string strStartDate = cls.f012_startdate.Replace(".", "");
                        int intStartDateAD = DateTimeEx.GetAdYearFromHs(int.Parse(strStartDate.Substring(0, 4))) * 10000 + int.Parse(strStartDate.Substring(2));
                        cls.startdatead = DateTimeEx.ToDateTime(intStartDateAD);
                    }

                    //初検日
                    if (cls.f029_firstdatew == string.Empty)
                    {
                        strErr += "初検年月日 変換に失敗しました";
                        cls.firstdatead = DateTime.MinValue;
                    }
                    else
                    {
                        string strFirstDate = cls.f029_firstdatew.Replace(".", "");
                        int intFirstDateAD = DateTimeEx.GetAdYearFromHs(int.Parse(strFirstDate.Substring(0, 4))) * 10000 + int.Parse(strFirstDate.Substring(2));
                        cls.firstdatead = DateTimeEx.ToDateTime(intFirstDateAD);
                    }

                    //終了日
                    if (cls.f030_finishdatew == string.Empty)
                    {
                        strErr += "終了年月日 変換に失敗しました";
                        cls.finishdatead = DateTime.MinValue;
                    }
                    else
                    {
                        string strFinishDate = cls.f030_finishdatew.Replace(".", "");
                        int intFinishDateAD = DateTimeEx.GetAdYearFromHs(int.Parse(strFinishDate.Substring(0, 4))) * 10000 + int.Parse(strFinishDate.Substring(2));
                        cls.finishdatead = DateTimeEx.ToDateTime(intFinishDateAD);
                    }

                    //決定金額数値
                    cls.totalint = cls.f014_total=="0" || cls.f014_total == string.Empty? 0:int.Parse(cls.f014_total.Replace(",", ""));

                  
                    if (strErr != string.Empty)
                    {
                        System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{strErr}");
                        tran.Rollback();
                        return false;
                    }

                    
                    //トランザクションにしないと遅すぎ                    
                    if (!DB.Main.Insert<dataImport_jyusei>(cls, tran)) return false;
                    

                    wf.LogPrint($"レセプトデータ  レセプト全国共通キー:{cls.f034_comnum}");
                    wf.InvokeValue++;

                }


                wf.LogPrint("CSVロード終了");


                tran.Commit();

                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show($"{System.Reflection.MethodBase.GetCurrentMethod().Name}\r\n{wf.Value + 1}行目:{ex.Message}");
                tran.Rollback();
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

        }


        public static List<dataImport_jyusei> selectAll(int cym)
        {
            DB.Command cmd = DB.Main.CreateCmd($"select * from dataimport_jyusei where cym={cym}");
            List<dataImport_jyusei> lst = new List<dataImport_jyusei>();

            try
            {
                var rList=cmd.TryExecuteReaderList();
                foreach(var item in rList)
                {
                    dataImport_jyusei imp = new dataImport_jyusei();
                    imp.f000_importid = int.Parse(item[0].ToString());

                    imp.f001_inspage = item[1].ToString();
                    imp.f002_allpage = item[2].ToString();
                    imp.f003_shinsayear = item[3].ToString();
                    imp.f004_shinsamonth = item[4].ToString();
                    imp.f005_menjo = item[5].ToString();
                    imp.f006_insnum = item[6].ToString();
                    imp.f007_insname = item[7].ToString();
                    imp.f008_clinicnum = item[8].ToString();
                    imp.f009_personname = item[9].ToString();
                    imp.f010_futannum = item[10].ToString();
                    imp.f011_futankbn = item[11].ToString();
                    imp.f012_startdate = item[12].ToString();
                    imp.f013_kbn1 = item[13].ToString();
                    imp.f014_total = item[14].ToString();
                    imp.f015_biko1 = item[15].ToString();
                    imp.f016_kbn2 = item[16].ToString();
                    imp.f017_mediy = item[17].ToString();
                    imp.f018_medim = item[18].ToString();
                    imp.f019_clinicname = item[19].ToString();
                    imp.f020_hihonum = item[20].ToString();
                    imp.f021_birthgw = item[21].ToString();
                    imp.f022_birthyw = item[22].ToString();
                    imp.f023_birthmw = item[23].ToString();
                    imp.f024_birthdw = item[24].ToString();
                    imp.f025_code = item[25].ToString();
                    imp.f026_gender = item[26].ToString();
                    imp.f027_jukyunum = item[27].ToString();
                    imp.f028_ratio = item[28].ToString();
                    imp.f029_firstdatew = item[29].ToString();
                    imp.f030_finishdatew = item[30].ToString();
                    imp.f031_counteddays = item[31].ToString();
                    imp.f032_partial = item[32].ToString();
                    imp.f033_biko2 = item[33].ToString();
                    imp.f034_comnum = item[34].ToString();

                    imp.cym = int.Parse(item[35].ToString());
                    imp.shinsaymad = int.Parse(item[36].ToString());
                    imp.startdatead = DateTime.Parse(item[37].ToString());
                    imp.mediymad = int.Parse(item[38].ToString());
                    imp.birthdayad = DateTime.Parse(item[39].ToString());
                    imp.firstdatead = DateTime.Parse(item[40].ToString());
                    imp.finishdatead = DateTime.Parse(item[41].ToString());
                    imp.totalint = int.Parse(item[42].ToString());

                    lst.Add(imp);

                }



                return lst;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return null;
            }
            finally
            {
                cmd.Dispose();
            }
        }
    }

}
